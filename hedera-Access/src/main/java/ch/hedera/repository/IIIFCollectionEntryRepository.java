/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - IIIFCollectionEntryRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.hedera.controller.AccessController;
import ch.hedera.model.access.IIIFCollectionEntry;

@Repository
@ConditionalOnBean(AccessController.class)
public interface IIIFCollectionEntryRepository extends SolidifyRepository<IIIFCollectionEntry> {

  List<IIIFCollectionEntry> findByProjectShortName(String projectShortName);

  Optional<IIIFCollectionEntry> findByProjectShortNameAndId(String projectShortName, String id);

  @Query("SELECT COUNT(c) FROM IIIFCollectionEntry c WHERE c.projectShortName = :projectShortName")
  long countByProject(String projectShortName);

  @Modifying
  @Query("DELETE FROM IIIFCollectionEntry c WHERE c.projectShortName = :projectShortName")
  void deleteByProject(String projectShortName);
}
