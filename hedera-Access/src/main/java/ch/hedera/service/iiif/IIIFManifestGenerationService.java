/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - IIIFManifestGenerationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.iiif;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.expr.nodevalue.NodeValueNode;
import org.apache.jena.sparql.syntax.ElementBind;
import org.apache.jena.sparql.syntax.ElementGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AccessController;
import ch.hedera.model.iiif.IIIFAnnotation;
import ch.hedera.model.iiif.IIIFAnnotationPage;
import ch.hedera.model.iiif.IIIFCanvas;
import ch.hedera.model.iiif.IIIFImage;
import ch.hedera.model.iiif.IIIFManifest;
import ch.hedera.model.iiif.IIIFManifestItem;
import ch.hedera.model.iiif.IIIFMetadataItem;
import ch.hedera.model.iiif.IIIFMultiLanguageText;
import ch.hedera.model.iiif.IIIFPageDTO;
import ch.hedera.model.iiif.IIIFRequiredStatement;
import ch.hedera.model.iiif.IIIFResource;
import ch.hedera.model.iiif.IIIFService;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.ResourceName;
import ch.hedera.service.triplestore.TriplestoreResourceService;
import ch.hedera.util.SparqlBuilderTool;

@Service
@ConditionalOnBean(AccessController.class)
public class IIIFManifestGenerationService {
  private static final Logger logger = LoggerFactory.getLogger(IIIFManifestGenerationService.class);

  private static final String PRESENTATION_CONTEXT = "https://iiif.io/api/presentation/3/context.json";
  private static final String IMAGE_CONTEXT = "https://iiif.io/api/image/3/context.json";
  private static final String SPARQL_ERROR = "Unable to read SPARQL result";
  private static final String LEVEL2_PROFILE = "level2";
  private static final String MANIFEST_TYPE = "Manifest";
  private static final String CANVAS_TYPE = "Canvas";
  private static final String IMAGE_TYPE = "Image";
  private static final String JPEG_FORMAT = "image/jpeg";
  private static final String ANNOTATION_TYPE = "Annotation";
  private static final String ANNOTATION_PAGE_TYPE = "AnnotationPage";
  private static final String PAINTING_MOTIVATION = "painting";

  private final String ldBaseUri;
  private final String manifestBaseUrl;

  private final String iiifDefaultParameters;

  private final TriplestoreResourceService triplestoreResourceService;

  public IIIFManifestGenerationService(HederaProperties config, TriplestoreResourceService triplestoreResourceService) {
    this.ldBaseUri = config.getModule().getLinkedData().getPublicUrl() + "/";
    this.manifestBaseUrl = config.getModule().getIIIF().getPublicUrl() + "/" + ResourceName.MANIFEST + "/";
    this.iiifDefaultParameters = config.getIIIF().getDefaultParameters();
    this.triplestoreResourceService = triplestoreResourceService;
  }

  public List<String> listManifestResearchObjectUri(Project project, Query listManifestQuery) {
    final String csvResult = this.triplestoreResourceService.executeQueryAndReturnCSV(project.getShortName(), listManifestQuery);
    try (final CSVReader csvReader = new CSVReader(new StringReader(csvResult))) {
      final List<String[]> results = csvReader.readAll();
      final List<String[]> resultsWithoutColumnName = results.subList(1, results.size());
      return resultsWithoutColumnName.stream().map(array -> array[0]).toList();
    } catch (CsvException | IOException e) {
      throw new SolidifyRuntimeException(SPARQL_ERROR, e);
    }
  }

  public List<IIIFManifestItem> listManifestUrl(Project project) {
    if (project.getIiifManifestResearchObjectType() == null) {
      return Collections.emptyList();
    }
    return this.listManifestUrl(project, project.getIiifManifestSparqlQuery());
  }

  public List<IIIFManifestItem> listManifestUrl(Project project, String listManifestQuery) {
    final Map<String, String> iiifManifests = this.getManifestLabels(project.getShortName(), listManifestQuery);
    final List<IIIFManifestItem> manifestItems = new ArrayList<>();

    for (Entry<String, String> entry : iiifManifests.entrySet()) {
      IIIFManifestItem item = new IIIFManifestItem();
      item.setId(this.manifestBaseUrl + project.getShortName() + "/" + entry.getKey());
      item.setLabel(entry.getValue());
      manifestItems.add(item);
    }
    return manifestItems;
  }

  public IIIFManifest generateManifest(Project project, String manifestId) {
    final Map<String, List<String>> manifestValues = this.getManifestValues(project, manifestId);
    if (manifestValues.isEmpty()) {
      return null;
    }
    final List<IIIFPageDTO> iiifPageDtoList = this.getIIIFPages(manifestValues);
    return this.generateIIIFManifest(
            this.manifestBaseUrl + project.getShortName() + "/" + manifestId,
            manifestValues, project.getCopyrightHolder(), iiifPageDtoList);
  }

  private Map<String, List<String>> getManifestValues(Project project, String manifestId) {
    // Add a bind statement to the beginning of the SPARQL query with the manifestId
    final Query queryObj = SparqlBuilderTool.createQuery(project.getIiifManifestSparqlQuery());
    final String researchObjectURI = this.ldBaseUri + project.getShortName()
            + "/" + project.getIiifManifestResearchObjectType().getName()
            + "/" + manifestId;
    final Node researchObjectNode = NodeFactory.createURI(researchObjectURI);
    final ElementBind bind = new ElementBind(Var.alloc(HederaConstants.MANIFEST_RESOURCE_SUBJECT_VAR), new NodeValueNode(researchObjectNode));
    final ElementGroup originalWhereClause = (ElementGroup) queryObj.getQueryPattern();
    final ElementGroup newWhereClause = new ElementGroup();
    newWhereClause.addElement(bind);
    originalWhereClause.getElements().forEach(newWhereClause::addElement);
    queryObj.setQueryPattern(newWhereClause);
    logger.debug("projectShortName : {}", project.getShortName());
    if (logger.isDebugEnabled()) {
      logger.debug("Get manifest values with the following SPARQL request :\n{}", queryObj.serialize());
    }
    final String csvResult = this.triplestoreResourceService.executeQueryAndReturnCSV(project.getShortName(), queryObj);

    return this.extractManifestValues(csvResult);
  }

  /**
   * Return a map of manifest labels
   * The IIIF Manifest SPARQL query defined at the project level is run once to get all values
   *
   * @param project
   * @return a map with manifest ID as key and manifest label as value
   */
  public Map<String, String> getManifestLabels(String projectShortName, String sparqlQuery) {
    if (sparqlQuery == null) {
      return Collections.emptyMap();
    }
    final Query queryObj = QueryFactory.create(sparqlQuery);
    // Keep only SPARQL projection variables related to the manifest i.e. starting with the METADATA_LABEL
    queryObj.getProjectVars().clear();
    queryObj.addResultVar(Var.alloc(HederaConstants.MANIFEST_RESOURCE_SUBJECT_VAR));
    queryObj.addResultVar(Var.alloc(HederaConstants.MANIFEST_LABEL_VAR));
    if (logger.isDebugEnabled()) {
      logger.debug("Get manifest labels with the following SPARQL request :\n{}", queryObj.serialize());
    }
    final String csvResult = this.triplestoreResourceService.executeQueryAndReturnCSV(projectShortName, queryObj);
    final Map<String, String> manifestLabels = new HashMap<>();
    try (final CSVReader csvReader = new CSVReader(new StringReader(csvResult))) {
      final List<String[]> records = csvReader.readAll();
      final int nbRecords = records.size() - 1;
      if (nbRecords < 1) {
        logger.warn("The IIIF Manifest SPARQL query returned no result");
        return new HashMap<>();
      }
      for (int currentRow = 1; currentRow <= nbRecords; currentRow++) {
        final String resourceSubject = records.get(currentRow)[0];
        final String manifestId = resourceSubject.substring(resourceSubject.lastIndexOf('/') + 1);
        final String manifestLabel = records.get(currentRow)[1];
        manifestLabels.put(manifestId, manifestLabel);
      }
    } catch (CsvException | IOException e) {
      throw new SolidifyRuntimeException(SPARQL_ERROR, e);
    }
    return manifestLabels;
  }

  public IIIFManifest generateIIIFManifest(
          String manifestId,
          Map<String, List<String>> manifestValues,
          String copyrightHolder,
          List<IIIFPageDTO> iiifPageDtoList) {
    if (logger.isDebugEnabled()) {
      showIIIFGenerationDebugInformation(manifestId, manifestValues, copyrightHolder, iiifPageDtoList);
    }
    final IIIFManifest iiifManifest = new IIIFManifest();
    final String manifestLabel = manifestValues.get(HederaConstants.MANIFEST_LABEL_VAR).get(0);
    iiifManifest.setContext(List.of(PRESENTATION_CONTEXT));
    iiifManifest.setId(manifestId);
    iiifManifest.setType(MANIFEST_TYPE);
    final IIIFMultiLanguageText manifestLabelMultiLang = new IIIFMultiLanguageText();
    manifestLabelMultiLang.addText(HederaConstants.MANIFEST_DEFAULT_LANGUAGE, List.of(manifestLabel));
    iiifManifest.setLabel(manifestLabelMultiLang);
    iiifManifest.setMetadata(this.getManifestMetadata(manifestValues));
    final IIIFMultiLanguageText requiredStatementLabel = new IIIFMultiLanguageText();
    final IIIFMultiLanguageText requiredStatementValue = new IIIFMultiLanguageText();
    requiredStatementLabel.addText(HederaConstants.MANIFEST_DEFAULT_LANGUAGE, List.of("Copyright"));
    requiredStatementValue.addText(HederaConstants.MANIFEST_DEFAULT_LANGUAGE, List.of(copyrightHolder));
    final IIIFRequiredStatement requiredStatement = new IIIFRequiredStatement();
    requiredStatement.setLabel(requiredStatementLabel);
    requiredStatement.setValue(requiredStatementValue);
    iiifManifest.setRequiredStatement(requiredStatement);

    List<IIIFCanvas> canvasList = new ArrayList<>();
    iiifManifest.setCanvases(canvasList);
    int pageNumber = 1;
    for (IIIFPageDTO pageDTO : iiifPageDtoList) {
      canvasList.add(this.generatePageMetadata(manifestId, pageDTO, pageNumber));
      pageNumber++;
    }

    return iiifManifest;
  }

  private IIIFCanvas generatePageMetadata(String manifestId, IIIFPageDTO pageDTO, int pageNumber) {
    String imageBaseUrl = pageDTO.getPageId();
    String canvasId = manifestId + "/canvas/p" + pageNumber;
    String resourceId = imageBaseUrl + this.iiifDefaultParameters;
    IIIFCanvas canvas = new IIIFCanvas();
    canvas.setId(canvasId);
    canvas.setType(CANVAS_TYPE);
    IIIFMultiLanguageText multiLanguageLabel = new IIIFMultiLanguageText();
    multiLanguageLabel.addText(HederaConstants.MANIFEST_DEFAULT_LANGUAGE, List.of(pageDTO.getPageLabel()));
    canvas.setLabel(multiLanguageLabel);
    canvas.setHeight(pageDTO.getHeight());
    canvas.setWidth(pageDTO.getWidth());
    List<IIIFAnnotationPage> annotationPages = new ArrayList<>();
    canvas.setAnnotationPages(annotationPages);

    IIIFAnnotationPage annotationPage = new IIIFAnnotationPage();
    annotationPages.add(annotationPage);
    annotationPage.setId(manifestId + "/annopage/p" + pageNumber);
    annotationPage.setType(ANNOTATION_PAGE_TYPE);

    List<IIIFAnnotation> annotations = new ArrayList<>();
    annotationPage.setAnnotations(annotations);

    IIIFAnnotation annotation = new IIIFAnnotation();
    annotations.add(annotation);
    annotation.setId(manifestId + "/annopage/p" + pageNumber + "/a1");
    annotation.setType(ANNOTATION_TYPE);
    annotation.setTarget(canvasId);

    IIIFImage image = new IIIFImage();
    annotation.setImage(image);
    annotation.setMotivation(PAINTING_MOTIVATION);

    image.setId(imageBaseUrl);
    image.setType(IMAGE_TYPE);

    IIIFResource iiifResource = new IIIFResource();
    image.setResource(iiifResource);
    iiifResource.setId(resourceId);
    iiifResource.setType(IMAGE_TYPE);
    iiifResource.setFormat(JPEG_FORMAT);
    iiifResource.setHeight(pageDTO.getHeight());
    iiifResource.setWidth(pageDTO.getWidth());

    IIIFService service = new IIIFService();
    iiifResource.setService(service);
    service.setContext(IMAGE_CONTEXT);
    service.setId(pageDTO.getPageId());
    service.setProfile(LEVEL2_PROFILE);

    return canvas;
  }

  private Map<String, List<String>> extractManifestValues(String csvResult) {
    try (final CSVReader csvReader = new CSVReader(new StringReader(csvResult))) {
      final List<String[]> records = csvReader.readAll();
      final int nbRecords = records.size() - 1;
      if (nbRecords < 1) {
        logger.warn("The IIIF Manifest SPARQL query returned no result");
        return new HashMap<>();
      }
      final Map<String, List<String>> manifestValues = new HashMap<>();
      int currentCol = 0;
      for (String variableName : records.get(0)) {
        List<String> listValues = new ArrayList<>();
        for (int currentRow = 1; currentRow <= nbRecords; currentRow++) {
          listValues.add(records.get(currentRow)[currentCol]);
        }
        manifestValues.put(variableName, listValues);
        currentCol++;
      }
      return manifestValues;
    } catch (CsvException | IOException e) {
      throw new SolidifyRuntimeException(SPARQL_ERROR, e);
    }
  }

  private IIIFMetadataItem[] getManifestMetadata(Map<String, List<String>> manifestValues) {
    final List<IIIFMetadataItem> iiifMetadataItemList = new ArrayList<>();
    for (Map.Entry<String, List<String>> entry : manifestValues.entrySet()) {
      if (entry.getKey().startsWith(HederaConstants.MANIFEST_METADATA_LABEL_PREFIX)) {
        final IIIFMetadataItem iiifMetadataItem = new IIIFMetadataItem();
        final IIIFMultiLanguageText label = new IIIFMultiLanguageText();
        final IIIFMultiLanguageText value = new IIIFMultiLanguageText();
        final String iiifLabel = entry.getKey().replace(HederaConstants.MANIFEST_METADATA_LABEL_PREFIX, "").replace("_", " ");
        label.addText(HederaConstants.MANIFEST_DEFAULT_LANGUAGE, List.of(iiifLabel));
        value.addText(HederaConstants.MANIFEST_DEFAULT_LANGUAGE, List.of(entry.getValue().get(0)));
        iiifMetadataItem.setLabel(label);
        iiifMetadataItem.setValue(value);
        iiifMetadataItemList.add(iiifMetadataItem);
      }
    }
    return iiifMetadataItemList.toArray(new IIIFMetadataItem[0]);
  }

  private List<IIIFPageDTO> getIIIFPages(Map<String, List<String>> manifestValues) {
    // Check if there is an image at least
    final List<String> imageList = manifestValues.get(HederaConstants.MANIFEST_IMG_SUBJECT_VAR);
    if (imageList.isEmpty()
            || (imageList.size() == 1 && StringTool.isNullOrEmpty(imageList.get(0)))) {
      return Collections.emptyList();
    }
    // For each image
    final int nbPages = imageList.size();
    final List<IIIFPageDTO> iiifPageDtoList = new ArrayList<>();
    for (int i = 0; i < nbPages; i++) {
      IIIFPageDTO pageDTO = new IIIFPageDTO();
      pageDTO.setPageId(manifestValues.get(HederaConstants.MANIFEST_IMG_IIIF_URL_VAR).get(i));
      pageDTO.setHeight(Integer.parseInt(manifestValues.get(HederaConstants.MANIFEST_IMG_HEIGHT_VAR).get(i)));
      pageDTO.setWidth(Integer.parseInt(manifestValues.get(HederaConstants.MANIFEST_IMG_WIDTH_VAR).get(i)));
      pageDTO.setPageLabel(Integer.toString(i + 1));
      iiifPageDtoList.add(pageDTO);
    }
    return iiifPageDtoList;
  }

  private void showIIIFGenerationDebugInformation(String manifestId, Map<String, List<String>> manifestValues, String copyrightHolder,
          List<IIIFPageDTO> iiifPageDtoList) {
    logger.debug("Generating manifest {}", manifestId);
    logger.debug("copyrightHolder={}", copyrightHolder);
    for(Map.Entry<String, List<String>> entry : manifestValues.entrySet()) {
      logger.debug("Values for {} :", entry.getKey());
      for (String value : entry.getValue()) {
        logger.debug(" - {} ", value);
      }
    }
    logger.debug("Using {} pages :", iiifPageDtoList.size());
    for(IIIFPageDTO page : iiifPageDtoList) {
      logger.debug(" - pageId={}, pageLabel={}, height={}, width={}", page.getPageId(), page.getPageLabel(), page.getHeight(), page.getWidth());
    }
  }
}
