/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - IIIFImageGenerationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.iiif;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Objects;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.TrustedRestClientTool;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AccessController;
import ch.hedera.model.dto.IIIFResponseDto;

@Service
@ConditionalOnBean(AccessController.class)
public class IIIFImageGenerationService {
  private static final String SLASH_PERCENT_CODE = "%2F";
  private static final String ID_FIELD_IIIF_V2 = "@id";
  private static final String ID_FIELD_IIIF_V3 = "id";

  private final TrustedRestClientTool trustedRestClientTool;
  private final String iiifImageUrl;
  private final URI iiifPublicUrl;

  public IIIFImageGenerationService(HederaProperties config, TrustedRestClientTool trustedRestClientTool) throws URISyntaxException {
    this.trustedRestClientTool = trustedRestClientTool;
    this.iiifImageUrl = config.getIIIF().getImageUrl();
    this.iiifPublicUrl = new URI(config.getModule().getIIIF().getPublicUrl());
  }

  public IIIFResponseDto downloadImage(String protocolVersion, String endingPath) throws URISyntaxException {
    final String encodedPath = this.encodeIdentifier(endingPath);
    final String imageUrl = this.iiifImageUrl + "/" + protocolVersion + "/" + encodedPath;
    // Send the request using RestTemplate and retrieve the response
    // encoded URL can use the method that has a URI as the first parameter to avoid double encoding
    final ResponseEntity<byte[]> response = this.trustedRestClientTool.getClient().getForEntity(new URI(imageUrl), byte[].class);
    final HttpHeaders headers = response.getHeaders();
    final String contentType = Objects.requireNonNull(headers.getContentType()).toString();
    return new IIIFResponseDto(Objects.requireNonNull(response.getBody()), contentType);
  }

  public IIIFResponseDto downloadInfoJson(String protocolVersion, String endingPath) throws URISyntaxException {
    final String encodedPath = this.encodeIdentifier(endingPath);
    final String imageUrl = this.iiifImageUrl + "/" + protocolVersion + "/" + encodedPath;
    final String infoJson = new RestTemplateBuilder()
            .defaultHeader("X-Forwarded-Proto", this.iiifPublicUrl.getScheme())
            .defaultHeader("X-Forwarded-Host", this.iiifPublicUrl.getHost())
            .defaultHeader("X-Forwarded-Port", String.valueOf(this.iiifPublicUrl.getPort()))
            .build().getForEntity(new URI(imageUrl), String.class).getBody();
    if (infoJson == null) {
      throw new SolidifyRuntimeException("IIIF server is responding to info.json request with an empty body");
    }
    final String transormedInfoJson = this.percentDecodeSlash(infoJson);
    return new IIIFResponseDto(transormedInfoJson.getBytes(), MediaType.APPLICATION_JSON_VALUE);
  }

  private String percentDecodeSlash(String infoJson) {
    final ObjectMapper objectMapper = new ObjectMapper();
    try {
      final JsonNode jsonNode = objectMapper.readTree(infoJson);
      if (jsonNode.isObject()) {
        final ObjectNode objectNode = (ObjectNode) jsonNode;
        if (objectNode.get(ID_FIELD_IIIF_V2) != null) {
          final String idValue = objectNode.get(ID_FIELD_IIIF_V2).textValue();
          objectNode.put(ID_FIELD_IIIF_V2, idValue.replace(SLASH_PERCENT_CODE, "/"));
        } else if (objectNode.get(ID_FIELD_IIIF_V3) != null) {
          final String idValue = objectNode.get(ID_FIELD_IIIF_V3).textValue();
          objectNode.put(ID_FIELD_IIIF_V3, idValue.replace(SLASH_PERCENT_CODE, "/"));
        }
        return objectMapper.writeValueAsString(objectNode);
      } else {
        throw new SolidifyRuntimeException("Invalid info.json : " + infoJson);
      }
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("Unable to process info.json : " + infoJson);
    }
  }

  /**
   * Encode the identifier to be compatible with iiif server url standard
   * {identifier}/{region}/{size}/{rotation}/{quality}.{format}
   *
   * @param url
   * @return encoded identifer ( replace / with %2F for identifier only )
   */
  private String encodeIdentifier(String url) {
    String[] splitUrlAsArray = url.split("/");
    // Check array size to avoid issues with small arrays
    if (splitUrlAsArray.length < 3) {
      throw new SolidifyRuntimeException("error: the url used for downloading image is incorrect");
    }

    String[] filteredArray;
    // we use api image information
    if (url.endsWith(HederaConstants.IIIF_IMAGE_INFO)) {
      // Create a new array to hold the desired elements except, first, second and the last
      filteredArray = new String[splitUrlAsArray.length - 1];
    } else {
      // we api image presentation
      filteredArray = new String[splitUrlAsArray.length - 4]; // hold the desired elements except, first, second and 4 other elements
                                                              // ({region}/{size}/{rotation}/{quality}.{format})
    }
    // Copy elements from the original array and keep only the elements needed
    System.arraycopy(splitUrlAsArray, 0, filteredArray, 0, filteredArray.length);
    StringBuilder result = new StringBuilder(String.join(SLASH_PERCENT_CODE, filteredArray));
    String lastElement = filteredArray[filteredArray.length - 1];
    int lastIndex = Arrays.asList(splitUrlAsArray).indexOf(lastElement) + 1;
    for (int i = lastIndex; i < splitUrlAsArray.length; ++i) {
      result.append("/");
      result.append(splitUrlAsArray[i]);
    }

    return result.toString();
  }

}
