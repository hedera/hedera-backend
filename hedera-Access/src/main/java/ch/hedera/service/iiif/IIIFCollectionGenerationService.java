/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - IIIFCollectionGenerationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.iiif;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AccessController;
import ch.hedera.model.iiif.IIIFCollection;
import ch.hedera.model.iiif.IIIFCollectionItem;
import ch.hedera.model.iiif.IIIFManifestItem;
import ch.hedera.model.iiif.IIIFMultiLanguageText;
import ch.hedera.model.settings.IIIFCollectionSettings;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.ResourceName;
import ch.hedera.service.rest.trusted.TrustedIIIFCollectionSettingsRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class IIIFCollectionGenerationService {

  private final IIIFManifestGenerationService iiifManifestService;
  private final TrustedIIIFCollectionSettingsRemoteResourceService iiifCollectionSettingsRemoteResourceService;
  private final String collectionBaseUrl;

  public IIIFCollectionGenerationService(
          IIIFManifestGenerationService iiifManifestService,
          TrustedIIIFCollectionSettingsRemoteResourceService iiifCollectionSettingsRemoteResourceService,
          HederaProperties hederaProperties) {
    this.iiifManifestService = iiifManifestService;
    this.iiifCollectionSettingsRemoteResourceService = iiifCollectionSettingsRemoteResourceService;
    this.collectionBaseUrl = hederaProperties.getModule().getIIIF().getPublicUrl() + "/" + ResourceName.COLLECTION + "/";
  }

  public List<IIIFCollection> getProjectCollectionList(Project project) {
    final List<IIIFCollection> collectionList = new ArrayList<>();
    for (IIIFCollectionSettings collectionSettings : this.iiifCollectionSettingsRemoteResourceService
            .getIIIFCollectionSettings(project.getResId())) {
      collectionList.add(this.getCollection(project, collectionSettings));
    }
    // Add default project collection
    collectionList.add(this.getProjectDefaultCollection(project));
    return collectionList;
  }

  public IIIFCollection getProjectDefaultCollection(Project project) {
    final List<IIIFManifestItem> manifestUrlList = this.iiifManifestService.listManifestUrl(project);
    return this.getCollection(project.getShortName(), HederaConstants.IIIF_PROJECT_DEFAULT_COLLECTION, project.getName(), manifestUrlList);
  }

  public IIIFCollection getCollection(Project project, String collectionName) {
    if (HederaConstants.IIIF_PROJECT_DEFAULT_COLLECTION.equals(collectionName)) {
      return this.getProjectDefaultCollection(project);
    }
    final IIIFCollectionSettings iiifCollectionSettings = this.iiifCollectionSettingsRemoteResourceService
            .getIIIFCollectionSettings(project.getResId(), collectionName)
            .orElseThrow(() -> new SolidifyResourceNotFoundException("No settings defined for collection " + collectionName + " in project "
                    + project.getShortName()));
    return this.getCollection(project, iiifCollectionSettings);
  }

  public IIIFCollection getCollection(String projectShortName, String collectionName, String collectionDescription,
          List<IIIFManifestItem> manifestUrlList) {
    final List<IIIFCollectionItem> collectionItemList = new ArrayList<>();
    for (IIIFManifestItem manifestUrl : manifestUrlList) {
      final IIIFCollectionItem collectionItem = new IIIFCollectionItem();
      collectionItem.setId(manifestUrl.getId());
      final IIIFMultiLanguageText manifestLabelMultiLang = new IIIFMultiLanguageText();
      manifestLabelMultiLang.addText(HederaConstants.MANIFEST_DEFAULT_LANGUAGE, List.of(manifestUrl.getLabel()));
      collectionItem.setLabel(manifestLabelMultiLang);
      collectionItemList.add(collectionItem);
    }
    final IIIFCollection collection = new IIIFCollection();
    collection.setId(this.collectionBaseUrl + projectShortName + "/" + collectionName);
    final IIIFMultiLanguageText collectionDescriptionMultiLang = new IIIFMultiLanguageText();
    if (collectionDescription == null) {
      collectionDescription = "";
    }
    collectionDescriptionMultiLang.addText(HederaConstants.MANIFEST_DEFAULT_LANGUAGE, List.of(collectionDescription));
    collection.setLabel(collectionDescriptionMultiLang);
    collection.setItems(collectionItemList.toArray(new IIIFCollectionItem[0]));
    return collection;

  }

  private IIIFCollection getCollection(Project project, IIIFCollectionSettings iiifCollectionSettings) {
    final List<IIIFManifestItem> manifestUrlList = this.iiifManifestService.listManifestUrl(project,
            iiifCollectionSettings.getIiifCollectionSparqlQuery());
    return this.getCollection(project.getShortName(), iiifCollectionSettings.getName(), iiifCollectionSettings.getDescription(),
            manifestUrlList);
  }
}
