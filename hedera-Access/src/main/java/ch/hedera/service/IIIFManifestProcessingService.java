/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - IIIFManifestProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.service.MessageService;

import ch.hedera.business.IIIFManifestService;
import ch.hedera.controller.AccessController;
import ch.hedera.model.access.IIIFManifestEntry;
import ch.hedera.model.iiif.IIIFManifestItem;
import ch.hedera.model.settings.Project;
import ch.hedera.service.iiif.IIIFManifestGenerationService;

@Service
@ConditionalOnBean(AccessController.class)
public class IIIFManifestProcessingService extends HederaService {

  private static final Logger log = LoggerFactory.getLogger(IIIFManifestProcessingService.class);

  private final IIIFManifestGenerationService iiifManifestGenerationService;

  private final IIIFManifestService iiifManifestService;

  public IIIFManifestProcessingService(
          MessageService messageService,
          IIIFManifestService iiifManifestService,
          IIIFManifestGenerationService iiifManifestGenerationService) {
    super(messageService);
    this.iiifManifestService = iiifManifestService;
    this.iiifManifestGenerationService = iiifManifestGenerationService;
  }

  @Transactional
  public void removeIIIFManifests(Project project) {
    final long count = this.iiifManifestService.countByProject(project.getShortName());
    this.iiifManifestService.deleteByProject(project.getShortName());
    log.info("{} IIIF manifest(s) deleted for project '{}'", count, project.getShortName());
  }

  @Transactional
  public void generateIIIFManifests(Project project) {
    long count = 0;
    final List<IIIFManifestItem> manifestItemList = this.iiifManifestGenerationService.listManifestUrl(project);
    log.info("Number of manifest URLs detected: {}", manifestItemList.size());
    for (IIIFManifestItem manifestItem : manifestItemList) {
      final String manifestId = manifestItem.getId().substring(manifestItem.getId().lastIndexOf("/") + 1);
      final ch.hedera.model.iiif.IIIFManifest iiifManifest = this.iiifManifestGenerationService.generateManifest(project, manifestId);
      if (iiifManifest != null) {
        IIIFManifestEntry manifest = new IIIFManifestEntry();
        manifest.setProjectShortName(project.getShortName());
        manifest.setId(manifestId);
        manifest.setUrl(manifestItem.getId());
        manifest.setName(manifestItem.getLabel());
        manifest.setContent(iiifManifest);
        this.iiifManifestService.save(manifest);
        count++;
      }
    }
    log.info("{} IIIF manifest(s) generated for project '{}'", count, project.getShortName());
  }

}
