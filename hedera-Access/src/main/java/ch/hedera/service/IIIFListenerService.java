/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - IIIFListenerService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.hedera.controller.AccessController;
import ch.hedera.message.IIIFMessage;
import ch.hedera.model.settings.Project;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class IIIFListenerService extends HederaService {

  private static final Logger log = LoggerFactory.getLogger(IIIFListenerService.class);

  private final TrustedProjectRemoteResourceService projectRemoteResourceService;

  private final IIIFManifestProcessingService iiifManifestProcessingService;

  private final IIIFCollectionProcessingService iiifCollectionProcessingService;

  public IIIFListenerService(
          MessageService messageService,
          TrustedProjectRemoteResourceService projectRemoteResourceService,
          IIIFManifestProcessingService iiifManifestProcessingService,
          IIIFCollectionProcessingService iiifCollectionProcessingService) {
    super(messageService);
    this.projectRemoteResourceService = projectRemoteResourceService;
    this.iiifManifestProcessingService = iiifManifestProcessingService;
    this.iiifCollectionProcessingService = iiifCollectionProcessingService;
  }

  @JmsListener(destination = "${hedera.queue.iiif}")
  public void processProjectData(IIIFMessage iiifMessage) {
    log.trace("Reading message {}", iiifMessage);
    try {
      final Project project = this.projectRemoteResourceService.getProjectByShortNameWithCache(iiifMessage.getResId());
      if (StringTool.isNullOrEmpty(project.getIiifManifestSparqlQuery())) {
        throw new SolidifyCheckingException(this.messageService.get("access.project.error.no_iiif_manifest_query"));
      }
      if (project.getIiifManifestResearchObjectType() == null) {
        throw new SolidifyCheckingException(this.messageService.get("access.project.error.no_iiif_research_object_type"));
      }
      log.info("IIIF refresh starting for project '{}'", project.getShortName());

      // IIIF Manifests
      this.iiifManifestProcessingService.removeIIIFManifests(project);
      this.iiifManifestProcessingService.generateIIIFManifests(project);

      // IIIF Collections
      this.iiifCollectionProcessingService.removeIIIFCollections(project);
      this.iiifCollectionProcessingService.generateIIIFCollections(project);

      log.info("IIIF refresh completed for project '{}'", project.getShortName());
    } catch (HttpClientErrorException.NotFound | SolidifyResourceNotFoundException e) {
      log.error("Project '{}' not found", iiifMessage.getResId(), e);
    } catch (SolidifyCheckingException e) {
      log.error("Missing configuration for project '{}': {}", iiifMessage.getResId(), e.getMessage(), e);
    } catch (SolidifyProcessingException e) {
      log.error("Error in IIIF refreshin for project '{}': {}", iiifMessage.getResId(), e.getMessage(), e);
    } catch (Exception e) {
      log.error("Error with project '{}' in IIIF refresh: {}", iiifMessage.getResId(), e.getMessage(), e);
    }
  }

}
