/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - OntologySitemapService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.sitemap;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.service.sitemap.SolidifySitemapService;

import ch.hedera.business.PublicOntologyService;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AccessController;
import ch.hedera.model.access.PublicOntology;
import ch.hedera.rest.ResourceName;

@Service
@ConditionalOnBean(AccessController.class)
public class OntologySitemapService extends SolidifySitemapService {

  private final PublicOntologyService ontologyService;

  public OntologySitemapService(
          SolidifyProperties solidifyConfig,
          HederaProperties config,
          PublicOntologyService ontologyService) {
    super(solidifyConfig, "browse/ontology/detail/", "");
    this.ontologyService = ontologyService;
  }

  @Override
  public String getName() {
    return ResourceName.ONTOLOGY;
  }

  @Override
  public long getItemsTotal() {
    Page<PublicOntology> result = this.getInternalOntologies(PageRequest.of(0, 1));
    return result.getTotalElements();

  }

  @Override
  public List<String> getItemsFrom(int from) {
    int page = from / this.getPageSize();
    Pageable pageable = PageRequest.of(page, this.getPageSize());
    Page<PublicOntology> result = this.getInternalOntologies(pageable);
    return result.getContent().stream().map(o -> this.getItemPartialUrl(o.getResId())).toList();
  }

  private Page<PublicOntology> getInternalOntologies(Pageable pageable) {
    final PublicOntology onto = new PublicOntology();
    onto.setExternal(false);
    return this.ontologyService.findAll(onto, pageable);
  }
}
