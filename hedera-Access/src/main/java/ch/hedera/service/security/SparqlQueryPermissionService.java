/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - SparqlQueryPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.security;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.hedera.controller.AccessController;
import ch.hedera.controller.HederaControllerAction;
import ch.hedera.model.ProjectAwareResource;
import ch.hedera.model.settings.Project;
import ch.hedera.service.rest.trusted.TrustedPersonRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class SparqlQueryPermissionService extends AbstractPermissionWithProjectService {
  private final TrustedProjectRemoteResourceService projectService;

  public SparqlQueryPermissionService(TrustedPersonRemoteResourceService trustedPersonRemoteResourceService,
          TrustedProjectRemoteResourceService projectService) {
    super(trustedPersonRemoteResourceService);
    this.projectService = projectService;
  }

  @Override
  ProjectAwareResource getExistingResource(String resId) {
    return this.projectService.getProjectByShortNameWithCache(resId);
  }

  @Override
  protected boolean isAllowedToPerformActionOnResource(String personId, ProjectAwareResource existingResource, HederaControllerAction action) {
    if (Boolean.TRUE.equals(((Project)existingResource).isAccessPublic())) {
        return true;
    } else {
      return super.isAllowedToPerformActionOnResource(personId, existingResource, action);
     }
  }

  @Override
  protected boolean isManagerAllowed(ProjectAwareResource existingResource, HederaControllerAction action) {
    return action == HederaControllerAction.EXEC_QUERY;
  }

  @Override
  protected boolean isCreatorAllowed(ProjectAwareResource existingResource, HederaControllerAction action) {
    return action == HederaControllerAction.EXEC_QUERY;
  }

  @Override
  protected boolean isVisitorAllowed(ProjectAwareResource existingResource, HederaControllerAction action) {
    return action == HederaControllerAction.EXEC_QUERY;
  }

}
