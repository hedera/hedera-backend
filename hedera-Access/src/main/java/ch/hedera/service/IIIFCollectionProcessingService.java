/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - IIIFCollectionProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import java.net.MalformedURLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.service.MessageService;

import ch.hedera.HederaConstants;
import ch.hedera.business.IIIFCollectionService;
import ch.hedera.controller.AccessController;
import ch.hedera.model.access.IIIFCollectionEntry;
import ch.hedera.model.settings.Project;
import ch.hedera.service.iiif.IIIFCollectionGenerationService;

@Service
@ConditionalOnBean(AccessController.class)
public class IIIFCollectionProcessingService extends HederaService {

  private static final Logger log = LoggerFactory.getLogger(IIIFCollectionProcessingService.class);

  private final IIIFCollectionGenerationService iiifCollectionGenerationService;

  private final IIIFCollectionService iiifCollectionService;

  public IIIFCollectionProcessingService(
          MessageService messageService,
          IIIFCollectionService iiifCollectionService,
          IIIFCollectionGenerationService iiifCollectionGenerationService) {
    super(messageService);
    this.iiifCollectionService = iiifCollectionService;
    this.iiifCollectionGenerationService = iiifCollectionGenerationService;
  }

  @Transactional
  public void removeIIIFCollections(Project project) {
    final long count = this.iiifCollectionService.countByProject(project.getShortName());
    this.iiifCollectionService.deleteByProject(project.getShortName());
    log.info("{} IIIF collection(s) deleted for project '{}'", count, project.getShortName());
  }

  @Transactional
  public void generateIIIFCollections(Project project) throws MalformedURLException {
    long count = 0;
    for (ch.hedera.model.iiif.IIIFCollection iiifCollection : this.iiifCollectionGenerationService.getProjectCollectionList(project)) {
      IIIFCollectionEntry collection = new IIIFCollectionEntry();
      collection.setProjectShortName(project.getShortName());
      collection.setId(iiifCollection.getId().substring(iiifCollection.getId().lastIndexOf("/") + 1));
      collection.setUrl(iiifCollection.getId());
      collection.setName(iiifCollection.getLabel().getMultiLanguageMap().get(HederaConstants.MANIFEST_DEFAULT_LANGUAGE).get(0));
      collection.setItemNumber(Long.valueOf(iiifCollection.getItems().length));
      collection.setContent(iiifCollection);
      try {
        this.iiifCollectionService.save(collection);
      } catch (RuntimeException e) {
        log.error("Unable to save IIIFCollectionEntry {} from IIIFCollection {}", collection, iiifCollection, e);
      }
      count++;
    }
    log.info("{} IIIF collection(s) generated for project '{}'", count, project.getShortName());
  }

}
