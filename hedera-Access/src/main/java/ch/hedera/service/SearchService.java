/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - SearchService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.model.oai.OAIRecord;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchConditionType;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.OAIMetadataService;
import ch.unige.solidify.service.OAIRecordService;
import ch.unige.solidify.service.OAISearchService;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaXmlNamespacePrefixMapper;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AccessController;
import ch.hedera.model.index.ResearchObjectMetadata;

@Service
@ConditionalOnBean(AccessController.class)
public class SearchService extends HederaService
        implements OAISearchService, OAIMetadataService, OAIRecordService {

  private final String publicIndexName;
  private final String privateIndexName;

  private IndexingService<ResearchObjectMetadata> indexSearchingService;

  private MetadataService metadataService;

  public SearchService(
          MessageService messageService,
          HederaProperties config,
          IndexingService<ResearchObjectMetadata> searchService,
          MetadataService metadataService) {
    super(messageService);
    this.publicIndexName = config.getIndexing().getIndexName();
    this.privateIndexName = config.getIndexing().getPrivateIndexName();
    this.indexSearchingService = searchService;
    this.metadataService = metadataService;
  }

  @Override
  public RestCollection<OAIRecord> searchOAIRecords(String from, String until, String setQuery, Pageable pageable) {
    List<SearchCondition> searchConditionList = this.getSearchParameters(null, from, until, setQuery);
    // if (!pageable.getSort().isSorted()) {
    // pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("subject"));
    // }
    FacetPage<ResearchObjectMetadata> researchObjectList = this.indexSearchingService.search(this.publicIndexName, searchConditionList, null,
            pageable, null);
    final List<OAIRecord> oaiRecordList = new ArrayList<>();
    for (final ResearchObjectMetadata researchObject : researchObjectList.getContent()) {
      oaiRecordList.add(researchObject);
    }
    return new RestCollection<>(oaiRecordList, new RestCollectionPage(researchObjectList.getNumber(), researchObjectList.getNumberOfElements(),
            researchObjectList.getTotalPages(), researchObjectList.getTotalElements()));
  }

  @Override
  public boolean isInSet(String resId, String setQuery, Pageable pageable) {
    final String query = "_id:" + resId;
    List<SearchCondition> searchConditionList = this.getSearchParameters(query, null, null, setQuery);
    FacetPage<ResearchObjectMetadata> result = this.indexSearchingService.search(this.publicIndexName, searchConditionList, null, pageable,
            null);
    return (result.getContent().size() == 1);
  }

  @Override
  public OAIRecord getRecord(String oaiId) {
    return this.indexSearchingService.findOne(this.publicIndexName, oaiId);
  }

  @Override
  public Object getOAIMetadata(String referenceMetadata) throws JAXBException {
    return this.metadataService.getResearchItem(referenceMetadata);
  }

  @Override
  public List<Class<?>> getOaiXmlClasses() {
    return Arrays.asList(
            ch.hedera.model.xml.hedera.v1.repository.RepositoryInfo.class,
            ch.hedera.model.xml.hedera.v1.researchObject.ResearchObject.class,
            ch.hedera.model.xml.hedera.v1.researchObject.ResearchDataFile.class);
  }

  @Override
  public Marshaller getMarshaller(JAXBContext jaxbContext) throws JAXBException {
    final Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    marshaller.setProperty(OAIConstants.NAMESPACE_MAPPER_PROPERTY, new HederaXmlNamespacePrefixMapper());
    return marshaller;
  }

  private List<SearchCondition> getSearchParameters(String query, String from, String to, String set) {
    List<SearchCondition> parameters = new ArrayList<>();
    if (!StringTool.isNullOrEmpty(query)) {
      SearchCondition querySearch = new SearchCondition(SearchConditionType.QUERY);
      querySearch.setValue(query);
      parameters.add(querySearch);
    }
    if (!StringTool.isNullOrEmpty(from) || !StringTool.isNullOrEmpty(to)) {
      SearchCondition dateRange = new SearchCondition(SearchConditionType.RANGE);
      dateRange.setField("objectDate");
      if (!StringTool.isNullOrEmpty(from)) {
        dateRange.setLowerValue(from);
      }
      if (!StringTool.isNullOrEmpty(to)) {
        dateRange.setUpperValue(to);
      }
      parameters.add(dateRange);
    }
    if (!StringTool.isNullOrEmpty(set)) {
      SearchCondition setSearch = new SearchCondition(SearchConditionType.QUERY);
      setSearch.setValue(set);
      parameters.add(setSearch);
    }
    return parameters;
  }

}
