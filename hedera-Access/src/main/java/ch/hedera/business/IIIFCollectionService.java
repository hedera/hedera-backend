/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - IIIFCollectionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.StringTool;

import ch.hedera.controller.AccessController;
import ch.hedera.model.access.IIIFCollectionEntry;
import ch.hedera.repository.IIIFCollectionEntryRepository;
import ch.hedera.specification.IIIFCollectionEntrySpecification;

@Service
@ConditionalOnBean(AccessController.class)
public class IIIFCollectionService extends ResourceService<IIIFCollectionEntry> implements IIIFEntryServiceInterface<IIIFCollectionEntry> {

  private final IIIFCollectionEntryRepository iiifCollectionEntryRepository;

  public IIIFCollectionService(IIIFCollectionEntryRepository iiifCollectionEntryRepository) {
    this.iiifCollectionEntryRepository = iiifCollectionEntryRepository;
  }

  public List<IIIFCollectionEntry> listByProject(String projectshortName) {
    return this.iiifCollectionEntryRepository.findByProjectShortName(projectshortName);
  }

  public long countByProject(String projectshortName) {
    return this.iiifCollectionEntryRepository.countByProject(projectshortName);
  }

  public void deleteByProject(String projectshortName) {
    this.iiifCollectionEntryRepository.deleteByProject(projectshortName);
  }

  @Override
  public IIIFCollectionEntry get(String projectshortName, String id) {
    return this.iiifCollectionEntryRepository.findByProjectShortNameAndId(projectshortName, id).orElseThrow();
  }

  @Override
  public SolidifySpecification<IIIFCollectionEntry> getSpecification(IIIFCollectionEntry resource) {
    return new IIIFCollectionEntrySpecification(resource);
  }

  @Override
  protected void validateItemSpecificRules(IIIFCollectionEntry item, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(item.getUrl())) {
      try {
        new URL(item.getUrl());
      } catch (MalformedURLException e) {
        errors.addError(new FieldError(item.getClass().getSimpleName(), "url", e.getMessage()));
      }
    }
  }

}
