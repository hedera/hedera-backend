/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - AccessProjectService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.NoSqlResourceService;
import ch.unige.solidify.util.StringTool;

import ch.hedera.model.access.PublicProject;
import ch.hedera.model.settings.Project;
import ch.hedera.service.rest.abstractservice.ProjectRemoteResourceService;

public abstract class AccessProjectService extends NoSqlResourceService<PublicProject> {

  private final ProjectRemoteResourceService projectRemoteResourceService;

  public AccessProjectService(ProjectRemoteResourceService projectRemoteResourceService) {
    this.projectRemoteResourceService = projectRemoteResourceService;
  }

  @Override
  public boolean delete(PublicProject t) {
    throw new UnsupportedOperationException();
  }

  /**
   * This method returns all the authorized projects for the person authenticated where the person has a role adding all the public projects.
   */
  @Override
  public List<PublicProject> findAll(PublicProject search) {
    final RestCollection<Project> result = this.projectRemoteResourceService
            .getAuthorizedProjectList(StringTool.convertToQueryString(search), PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE));
    final List<PublicProject> list = new ArrayList<>();
    for (final Project project : result.getData()) {
      list.add(new PublicProject(project));
    }
    return list;
  }

  /**
   * This method returns all the authorized projects for the person authenticated where the person has a role adding all the public projects.
   */
  @Override
  public Page<PublicProject> findAll(PublicProject search, Pageable pageable) {
    final RestCollection<Project> result = this.projectRemoteResourceService.getAuthorizedProjectList(
            StringTool.convertToQueryString(search), pageable);
    return this.getPublicPage(result, pageable);
  }

  /**
   * This method returns all the public projects without checking if the person has a role in it.
   */
  public Page<PublicProject> findOnlyPublic(PublicProject search, Pageable pageable) {
    search.setAccessPublic(true);
    final RestCollection<Project> result = this.projectRemoteResourceService.getProjectList(
            StringTool.convertToQueryString(search), pageable);
    return this.getPublicPage(result, pageable);
  }

  private Page<PublicProject> getPublicPage(RestCollection<Project> projectCollection, Pageable pageable) {
    final List<PublicProject> list = new ArrayList<>();
    for (final Project project : projectCollection.getData()) {
      list.add(new PublicProject(project));
    }
    return new PageImpl<>(list, pageable, projectCollection.getPage().getTotalItems());
  }

  @Override
  public PublicProject findOne(String id) {
    Project project = this.projectRemoteResourceService.getProjectByShortNameOrById(id);
    return new PublicProject(project);
  }

  @Override
  public PublicProject save(PublicProject t) {
    throw new UnsupportedOperationException();
  }

  @Override
  public PublicProject update(PublicProject t) {
    throw new UnsupportedOperationException();
  }

}
