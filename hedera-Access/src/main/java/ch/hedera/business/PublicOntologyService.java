/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - PublicOntologyService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.NoSqlResourceService;
import ch.unige.solidify.util.StringTool;

import ch.hedera.controller.AccessController;
import ch.hedera.model.access.PublicOntology;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.service.rest.trusted.TrustedOntologyRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class PublicOntologyService extends NoSqlResourceService<PublicOntology> {

  private final TrustedOntologyRemoteResourceService ontolgyRemoteResourceService;

  public PublicOntologyService(TrustedOntologyRemoteResourceService ontolgyRemoteResourceService) {
    this.ontolgyRemoteResourceService = ontolgyRemoteResourceService;
  }

  @Override
  public boolean delete(PublicOntology t) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Page<PublicOntology> findAll(PublicOntology search, Pageable pageable) {
    final RestCollection<Ontology> result = this.ontolgyRemoteResourceService.getList(StringTool.convertToQueryString(search), pageable);
    final List<PublicOntology> list = new ArrayList<>();
    for (final Ontology onto : result.getData()) {
      list.add(new PublicOntology(onto));
    }
    return new PageImpl<>(list, pageable, result.getPage().getTotalItems());
  }

  @Override
  public List<PublicOntology> findAll(PublicOntology search) {
    final RestCollection<Ontology> result = this.ontolgyRemoteResourceService.findAll(PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE));
    final List<PublicOntology> list = new ArrayList<>();
    for (final Ontology onto : result.getData()) {
      list.add(new PublicOntology(onto));
    }
    return list;
  }

  @Override
  public PublicOntology findOne(String id) {
    return new PublicOntology(this.ontolgyRemoteResourceService.findOneWithCache(id));
  }

  @Override
  public PublicOntology save(PublicOntology t) {
    throw new UnsupportedOperationException();
  }

  @Override
  public PublicOntology update(PublicOntology t) {
    throw new UnsupportedOperationException();
  }

  public void download(String id) {
    this.ontolgyRemoteResourceService.downloadOntology(id);
  }
}
