/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - ResearchObjectService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.util.List;
import java.util.Optional;

import org.apache.jena.query.Query;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.util.StringTool;

import ch.hedera.controller.AccessController;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.model.triplestore.SparqlResultRow;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedResearchObjectTypeRemoteResourceService;
import ch.hedera.service.triplestore.TriplestoreResourceService;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlQueryTool;

@Service
@ConditionalOnBean(AccessController.class)
public class ResearchObjectService {

  TriplestoreResourceService triplestoreResourceService;
  TrustedResearchObjectTypeRemoteResourceService trustedResearchObjectTypeRemoteResourceService;
  TrustedProjectRemoteResourceService trustedProjectRemoteResourceService;

  private enum SparqlQueryType {
    LIST, DETAIL
  }

  public ResearchObjectService(TriplestoreResourceService triplestoreResourceService,
          TrustedResearchObjectTypeRemoteResourceService trustedResearchObjectTypeRemoteResourceService,
          TrustedProjectRemoteResourceService trustedProjectRemoteResourceService) {
    this.triplestoreResourceService = triplestoreResourceService;
    this.trustedResearchObjectTypeRemoteResourceService = trustedResearchObjectTypeRemoteResourceService;
    this.trustedProjectRemoteResourceService = trustedProjectRemoteResourceService;
  }

  public Page<SparqlResultRow> listResearchObjects(String projectShortName, String researchObjectTypeName, Pageable pageable) {
    final Query sparqlQuery = this.getSparqlQuery(projectShortName, researchObjectTypeName, SparqlQueryType.LIST);
    return this.triplestoreResourceService.executeQueryForList(projectShortName, sparqlQuery, pageable);
  }

  public Page<SparqlResultRow> getResearchObject(String projectShortName, String researchObjectTypeName, String researchObjectId) {
    final Query sparqlQueryForDetail = this.getSparqlQuery(projectShortName, researchObjectTypeName, SparqlQueryType.DETAIL);
    return this.triplestoreResourceService.executeQueryForDetail(projectShortName, researchObjectId, sparqlQueryForDetail);
  }

  /**
   * Get a research object type's SPARQL query used to get the list or the details.
   * It also checks that the research object type is associated to the project.
   *
   * @param projectShortName
   * @param researchObjectTypeName
   * @param queryType
   * @return
   */
  private Query getSparqlQuery(String projectShortName, String researchObjectTypeName, SparqlQueryType queryType) {
    final Project project = this.trustedProjectRemoteResourceService.getProjectByShortNameWithCache(projectShortName);
    final List<ResearchObjectType> researchObjectTypeList = this.trustedProjectRemoteResourceService.getAllResearchObjectTypes(projectShortName);
    final Optional<ResearchObjectType> researchObjectType = researchObjectTypeList.stream()
            .filter(rot -> rot.getName().equals(researchObjectTypeName))
            .findFirst();
    if (researchObjectType.isEmpty()) {
      throw new SolidifyResourceNotFoundException("ResearchObjectType not found: " + researchObjectTypeName);
    }
    ResearchObjectType objectType = null;
    // Test if special research object type: Research Data File or IIIF Manifest
    if (project.getResearchDataFileResearchObjectType().getResId().equals(researchObjectType.get().getResId())
            || project.getIiifManifestResearchObjectType().getResId().equals(researchObjectType.get().getResId())) {
      objectType = researchObjectType.get();
    } else {
      objectType = this.trustedResearchObjectTypeRemoteResourceService.findOneByProjectWithCache(project.getResId(),
              researchObjectType.get().getResId());
    }
    if (queryType == SparqlQueryType.LIST) {
      if (StringTool.isNullOrEmpty(objectType.getSparqlListQuery())) {
        return SparqlQueryTool.queryToListSubjectsOfResearchObjectType(SparqlBuilderTool.SUBJECT_VARIABLE, objectType);
      }
      return SparqlBuilderTool.createQuery(objectType.getSparqlListQuery());
    } else {
      if (StringTool.isNullOrEmpty(objectType.getSparqlDetailQuery())) {
        return SparqlQueryTool.queryToListSubjectsOfResearchObjectType(SparqlBuilderTool.SUBJECT_VARIABLE, objectType);
      }
      return SparqlBuilderTool.createQuery(objectType.getSparqlDetailQuery());
    }
  }
}
