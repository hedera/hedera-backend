/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - ResourceByProjectReadOnlyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.BooleanClauseType;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchConditionType;
import ch.unige.solidify.rest.Tool;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.model.settings.Project;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;

public abstract class ResourceByProjectReadOnlyController extends SolidifyController implements ControllerWithHateoasHome {

  private final String publicIndexName;
  private final String privateIndexName;

  protected final TrustedProjectRemoteResourceService projectRemoteService;
  protected final IndexingService<ResearchObjectMetadata> searchService;

  protected ResourceByProjectReadOnlyController(
          HederaProperties config,
          TrustedProjectRemoteResourceService projectRemoteService,
          IndexingService<ResearchObjectMetadata> searchService) {
    this.publicIndexName = config.getIndexing().getIndexName();
    this.privateIndexName = config.getIndexing().getPrivateIndexName();
    this.projectRemoteService = projectRemoteService;
    this.searchService = searchService;
  }

  @GetMapping
  public HttpEntity<RestCollection<ResearchObjectMetadata>> list(@PathVariable String projectShortName,
          @ModelAttribute ResearchObjectMetadata search, Pageable pageable) {
    Project project = this.projectRemoteService.getProjectByShortNameOrById(projectShortName);
    final Page<ResearchObjectMetadata> listItem = this.searchService.search(this.getIndex(project),
            this.buildSearchByProjectShortNameConditions(project.getShortName()), null, pageable, null);
    return this.page2Collection(projectShortName, listItem, pageable);
  }

  @GetMapping("/{hederaId}")
  public HttpEntity<ResearchObjectMetadata> get(@PathVariable String projectShortName, @PathVariable String hederaId) {
    Project projectByShortName = this.projectRemoteService.getProjectByShortNameWithCache(projectShortName);
    // Find by hederaId
    ResearchObjectMetadata t = this.findByHederaId(projectByShortName, hederaId);
    if (t == null) {
      // Find by resId
      t = this.searchService.findOne(this.getIndex(projectByShortName), hederaId);
    }
    if (t != null) {
      this.addLinks(t, projectShortName);
      return new ResponseEntity<>(t, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  protected ResearchObjectMetadata findByHederaId(Project project, String hederaId) {
    FacetPage<ResearchObjectMetadata> researchObjectList = this.searchService.search(this.getIndex(project),
            this.buildSearchByHederaIdConditions(project.getShortName(), hederaId), null,
            Pageable.ofSize(1), null);

    return researchObjectList.getContent().stream()
            .filter(rs -> rs.getHederaId().equals(hederaId))
            .findFirst()
            .orElse(null);
  }

  protected List<SearchCondition> buildSearchByProjectShortNameConditions(String projectShortName) {
    SearchCondition projectCondition = this.createTermSearchCondition(
            HederaConstants.PROJECT_SHORT_NAME_INDEX_FIELD + HederaConstants.INDEXING_KEYWORD,
            projectShortName);

    SearchCondition requestedCondition = this
            .createNestedSearchCondition(List.of(this.createItemTypeCondition(), projectCondition));

    return List.of(requestedCondition);
  }

  protected List<SearchCondition> buildSearchByHederaIdConditions(String projectShortName, String hederaId) {
    SearchCondition hederaIdCondition = this.createTermSearchCondition(
            HederaConstants.INDEX_FIELD_HEDERA_ID,
            hederaId);

    SearchCondition projectCondition = this.createTermSearchCondition(
            HederaConstants.PROJECT_SHORT_NAME_INDEX_FIELD + HederaConstants.INDEXING_KEYWORD,
            projectShortName);

    SearchCondition requestedCondition = this
            .createNestedSearchCondition(List.of(this.createItemTypeCondition(), projectCondition, hederaIdCondition));

    return List.of(requestedCondition);
  }

  protected abstract SearchCondition createItemTypeCondition();

  protected SearchCondition createTermSearchCondition(String fieldName, String fieldValue) {
    SearchCondition searchCondition = new SearchCondition();
    searchCondition.setBooleanClauseType(BooleanClauseType.MUST);
    searchCondition.setType(SearchConditionType.TERM);
    searchCondition.setField(fieldName);
    searchCondition.setValue(fieldValue);
    return searchCondition;
  }

  protected SearchCondition createNestedSearchCondition(List<SearchCondition> conditions) {
    SearchCondition searchCondition = new SearchCondition();
    searchCondition.setBooleanClauseType(BooleanClauseType.MUST);
    searchCondition.setType(SearchConditionType.NESTED_BOOLEAN);
    for (SearchCondition condition : conditions) {
      searchCondition.getNestedConditions().add(condition);
    }
    return searchCondition;
  }

  protected String getIndex(Project project) {
    return Boolean.TRUE.equals(project.isAccessPublic()) ? this.publicIndexName : this.privateIndexName;
  }

  protected void addLinks(ResearchObjectMetadata item, String projectName) {
    item.removeLinks();
    item.addLinksForAccess(linkTo(this.getClass(), projectName));
  }

  protected HttpEntity<RestCollection<ResearchObjectMetadata>> page2Collection(String projectName, Page<ResearchObjectMetadata> listItem,
          Pageable pageable) {
    for (final ResearchObjectMetadata t : listItem) {
      this.addLinks(t, projectName);
    }
    final RestCollection<ResearchObjectMetadata> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkTo(this.getClass(), pageable).withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass(), pageable)).toUriComponentsBuilder())
            .withRel(ActionName.MODULE));
    // this.addOthersLinks(collection);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

}
