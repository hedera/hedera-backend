/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - SitemapController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.controller.SolidifySitemapController;

import ch.hedera.config.HederaProperties;
import ch.hedera.service.sitemap.OntologySitemapService;
import ch.hedera.service.sitemap.ProjectSitemapService;

@RestController
@ConditionalOnBean(AccessController.class)
public class SitemapController extends SolidifySitemapController {

  public SitemapController(
          SolidifyProperties solidifyConfig,
          HederaProperties config,
          ProjectSitemapService projectSitemapService,
          OntologySitemapService ontologySitemapService) {
    super(solidifyConfig, config.getParameters().getPortalHomePage());
    this.getSitemapServiceList().add(projectSitemapService);
    this.getSitemapServiceList().add(ontologySitemapService);
  }

  @Override
  protected List<String> getExtraUrls() {
    return Collections.emptyList();
  }

}
