/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - AccessController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller;

import java.io.IOException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.business.OAIMetadataPrefixService;
import ch.unige.solidify.business.OAISetService;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.controller.ModuleController;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.security.TrustedUserPermissions;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.message.IIIFMessage;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;

@RestController
@ConditionalOnProperty(prefix = "hedera.module.access", name = "enable")
@RequestMapping(UrlPath.ACCESS)
public class AccessController extends ModuleController {

  private final TrustedProjectRemoteResourceService projectRemoteResourceService;

  AccessController(HederaProperties config,
          OAIMetadataPrefixService oaiMetadataPrefixService,
          OAISetService oaiSetService,
          TrustedProjectRemoteResourceService projectRemoteResourceService) {
    super(ModuleName.ACCESS);
    this.projectRemoteResourceService = projectRemoteResourceService;
    if (config.getData().isInit()) {
      // OAI-PMH
      try {
        // Init OAI Metadata Prefix
        oaiMetadataPrefixService.initDefaultData();
        // OAI Metadata Prefix: hedera
        oaiMetadataPrefixService.createIfNotExists(
                HederaConstants.OAI_HEDERA,
                true,
                HederaConstants.OAI_HEDERA_NAME,
                HederaConstants.OAI_HEDERA_NAME + " v1.0",
                HederaConstants.HEDERA_RESEARCH_OBJECT_SCHEMA_1,
                null,
                HederaConstants.HEDERA_SCHEMA_1,
                HederaConstants.HEDERA_NAMESPACE_1);
        // Update OAI DC
        oaiMetadataPrefixService.updateOaiDc("hedera2oai_dc.xsl");
      } catch (IOException e) {
        throw new SolidifyRuntimeException(e.getMessage(), e);
      }
      // OAI Set for all projects
      oaiSetService.createIfNotExists(HederaConstants.OAI_PROJECT, "Projects", "Set for all project", "researchProject.id:*");
      // OAI Set for all research objects
      oaiSetService.createIfNotExists(HederaConstants.OAI_RESEARCH_OBJECT, "Research Objects", "Set for all research objects (RDF)",
              "itemType:ResearchObject");
      // OAI Set for all research data file
      oaiSetService.createIfNotExists(HederaConstants.OAI_RESEARCH_DATA_FILE, "Research Data Files", "Set for all research data files",
              "itemType:ResearchDataFile");
      // OAI Set for a RDF tyoe
      oaiSetService.createIfNotExists(HederaConstants.OAI_DIGITAL_OBJECT, "RDF Digital Objects", "Set for RDF subjects (D1_Digital_Object)",
              "researchObjectType.rdfType:D1_Digital_Object");
    }
  }

  @TrustedUserPermissions
  @PostMapping("/" + HederaActionName.IIIF_REFRESH)
  public ResponseEntity<Void> refreshIIIFAll() {
    for (Project project : this.projectRemoteResourceService.getProjectList("hasData=true", null).getData()) {
      SolidifyEventPublisher.getPublisher().publishEvent(new IIIFMessage(project.getShortName()));
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @TrustedUserPermissions
  @PostMapping("/" + HederaActionName.IIIF_REFRESH + "/{projectShortName}")
  public ResponseEntity<Void> refreshIIIF(@PathVariable String projectShortName) {
    SolidifyEventPublisher.getPublisher().publishEvent(new IIIFMessage(projectShortName));
    return new ResponseEntity<>(HttpStatus.OK);
  }

}
