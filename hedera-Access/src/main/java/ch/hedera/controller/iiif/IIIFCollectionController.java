/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - IIIFCollectionController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.iiif;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.EveryonePermissions;

import ch.hedera.business.IIIFCollectionService;
import ch.hedera.controller.AccessController;
import ch.hedera.model.access.IIIFCollectionEntry;
import ch.hedera.model.iiif.IIIFCollection;
import ch.hedera.rest.UrlPath;

@EveryonePermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.IIIF_COLLECTION)
public class IIIFCollectionController extends IIIFItemController<IIIFCollectionService, IIIFCollectionEntry, IIIFCollection> {

  public IIIFCollectionController(IIIFCollectionService iiifCollectionService) {
    super(iiifCollectionService);
  }

  @Override
  @SuppressWarnings("java:S5122")
  @CrossOrigin(origins = "*")
  @GetMapping("/{projectShortName}")
  @PreAuthorize("@IIIFImagePermissionService.isAllowed(#projectShortName, 'EXEC_QUERY')")
  public HttpEntity<RestCollection<IIIFCollectionEntry>> list(@PathVariable String projectShortName, @ModelAttribute IIIFCollectionEntry search,
          Pageable pageable) {
    return super.list(projectShortName, search, pageable);
  }

  @Override
  @SuppressWarnings("java:S5122")
  @CrossOrigin(origins = "*")
  @GetMapping("/{projectShortName}" + SolidifyConstants.URL_ID)
  @PreAuthorize("@IIIFImagePermissionService.isAllowed(#projectShortName, 'EXEC_QUERY')")
  public HttpEntity<IIIFCollection> get(@PathVariable String projectShortName, @PathVariable("id") String collectionName) {
    return super.get(projectShortName, collectionName);
  }

}
