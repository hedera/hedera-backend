/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - IIIFImageController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.iiif;

import static ch.hedera.HederaConstants.IIIF_IMAGE_INFO;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URISyntaxException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.util.StringTool;

import ch.hedera.controller.AccessController;
import ch.hedera.model.dto.IIIFResponseDto;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.iiif.IIIFImageGenerationService;

@EveryonePermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.IIIF_PROXY)
public class IIIFImageController extends SolidifyController {
  private final IIIFImageGenerationService iiifImageService;

  public IIIFImageController(IIIFImageGenerationService iiifImageService) {
    this.iiifImageService = iiifImageService;
  }

  @SuppressWarnings("java:S5122")
  @GetMapping(value = { "/{protocolVersion:\\d}/{projectShortName}/**" })
  @PreAuthorize("@IIIFImagePermissionService.isAllowed(#projectShortName, 'EXEC_QUERY')"
          + "|| @downloadTokenPermissionService.isAllowed(#projectShortName, T(ch.hedera.model.security.DownloadTokenType).IIIF)")
  public ResponseEntity<StreamingResponseBody> executeIIIFRequest(HttpServletRequest request, HttpServletResponse response,
          @PathVariable String protocolVersion,
          @PathVariable String projectShortName) throws URISyntaxException {
    String endingUrlPath = this.extractEndingPathWithSemiColon(request, projectShortName);
    if (!StringTool.isNullOrEmpty(request.getQueryString())) {
      endingUrlPath += "?" + request.getQueryString();
    }
    IIIFResponseDto iiifResponseDto;
    if (request.getRequestURI().endsWith(IIIF_IMAGE_INFO)) {
      iiifResponseDto = this.iiifImageService.downloadInfoJson(protocolVersion, endingUrlPath);
    } else {
      iiifResponseDto = this.iiifImageService.downloadImage(protocolVersion, endingUrlPath);
    }
    return this.buildDownloadResponseEntity(iiifResponseDto, response);
  }

  private ResponseEntity<StreamingResponseBody> buildDownloadResponseEntity(IIIFResponseDto iiifResponseDto, HttpServletResponse response) {
    HttpHeaders respHeaders = new HttpHeaders();
    respHeaders.setContentType(MediaType.valueOf(iiifResponseDto.getContentType()));
    // Don't set ACCESS_CONTROL_ALLOW_ORIGIN if already set by SimpleCorsFilter
    if (response.getHeaders(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN).isEmpty()) {
      respHeaders.set(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
    }
    InputStream inputStream = new ByteArrayInputStream(iiifResponseDto.getContent());
    StreamingResponseBody responseBody = outputStream -> {
      byte[] data = new byte[SolidifyConstants.BUFFER_SIZE];
      int numberOfBytesToWrite;
      while ((numberOfBytesToWrite = inputStream.read(data, 0, data.length)) != -1) {
        outputStream.write(data, 0, numberOfBytesToWrite);
      }
      inputStream.close();
    };
    return new ResponseEntity<>(responseBody, respHeaders, HttpStatus.OK);
  }

  private String extractEndingPathWithSemiColon(HttpServletRequest request, String projectShortName) {
    //extractEndingPath method in SolidifyController does not handle semicolon in URL
    String url = request.getRequestURL().toString();
    return url.substring(url.indexOf(projectShortName));
  }
}
