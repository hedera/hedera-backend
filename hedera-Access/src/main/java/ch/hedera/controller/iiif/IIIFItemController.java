/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - IIIFItemController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.iiif;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.service.ResourceService;

import ch.hedera.business.IIIFEntryServiceInterface;
import ch.hedera.model.access.IIIFEntryInterface;

@RootPermissions
public abstract class IIIFItemController<S extends ResourceService<T> & IIIFEntryServiceInterface<T>, T extends Resource & IIIFEntryInterface<V>, V>
        extends SolidifyController
        implements ControllerWithHateoasHome {

  protected S itemService;

  protected IIIFItemController(S itemService) {
    this.itemService = itemService;
  }

  protected HttpEntity<V> get(String projectShortName, String id) {
    T item = this.itemService.get(projectShortName, id);
    return new ResponseEntity<>(item.getContent(), HttpStatus.OK);
  }

  protected HttpEntity<RestCollection<T>> list(String projectShortName, T search, Pageable pageable) {
    search.setProjectShortName(projectShortName);
    final Specification<T> spec = this.itemService.getSpecification(search);
    final Page<T> listItem = this.itemService.findAll(spec, pageable);
    this.setResourceLinks(listItem);
    final RestCollection<T> collection = this.setCollectionLinks(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  protected void addLinks(T t) {
    t.removeLinks();
    t.addLinks(linkTo(this.getClass()), true, true);
  }

  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    // Do nothing
    // Could be overridden by subclass
  }

  protected RestCollection<T> setCollectionLinks(Page<T> listItem, Pageable pageable) {
    final RestCollection<T> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkTo(this.getClass()).withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass())).toUriComponentsBuilder())
            .withRel(ActionName.MODULE));

    this.addSortLinks(linkTo(this.getClass()), collection);
    this.addPageLinks(linkTo(this.getClass()), collection, pageable);
    this.addOthersLinks(collection);
    return collection;
  }

  protected RestCollection<T> setCollectionLinksForMethod(Page<T> listItem, Pageable pageable, WebMvcLinkBuilder linkBuilder) {
    final RestCollection<T> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkBuilder.withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass())).toUriComponentsBuilder())
            .withRel(ActionName.MODULE));
    this.addSortLinks(linkBuilder, collection);
    this.addPageLinks(linkBuilder, collection, pageable);
    this.addOthersLinks(collection);
    return collection;
  }

  protected void setResourceLinks(Page<T> listItem) {
    if (listItem == null) {
      return;
    }
    for (final T t : listItem) {
      this.addLinks(t);
    }
  }

}
