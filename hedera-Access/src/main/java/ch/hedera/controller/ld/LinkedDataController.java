/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - LinkedDataController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.ld;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.Query;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.BooleanClauseType;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchConditionType;
import ch.unige.solidify.security.EveryonePermissions;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.config.HederaRepositoryDescription;
import ch.hedera.controller.AccessController;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.model.settings.Project;
import ch.hedera.model.xml.hedera.v1.researchObject.ItemType;
import ch.hedera.rest.ResourceName;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;
import ch.hedera.service.triplestore.TriplestoreResourceService;
import ch.hedera.util.RdfTool;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlConstructTool;
import ch.hedera.util.SparqlQueryTool;

@EveryonePermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.LINKED_DATA_PROXY)
public class LinkedDataController extends SolidifyController implements ControllerWithHateoasHome {

  private static final String REDIRECT = "redirect:";

  private final String ldBaseURi;
  private final String projectAccessUrl;
  private final String publicIndexName;
  private final String privateIndexName;
  private final String researchObjectHomePage;

  private final TrustedProjectRemoteResourceService projectRemoteService;
  private final IndexingService<ResearchObjectMetadata> searchService;
  private final TriplestoreResourceService triplestoreResourceService;

  public LinkedDataController(HederaProperties config,
          HederaRepositoryDescription repositoryDescription,
          TrustedProjectRemoteResourceService projectRemoteService,
          IndexingService<ResearchObjectMetadata> searchService,
          TriplestoreResourceService triplestoreResourceService) {
    super();
    this.ldBaseURi = config.getModule().getLinkedData().getPublicUrl() + "/";
    this.projectAccessUrl = config.getModule().getAccess().getPublicUrl() + "/" + ResourceName.PROJECT;
    this.publicIndexName = config.getIndexing().getIndexName();
    this.privateIndexName = config.getIndexing().getPrivateIndexName();
    this.researchObjectHomePage = repositoryDescription.getArchiveHomePage();
    this.projectRemoteService = projectRemoteService;
    this.searchService = searchService;
    this.triplestoreResourceService = triplestoreResourceService;
  }

  @SuppressWarnings("java:S5122")
  @CrossOrigin(origins = "*")
  @GetMapping
  public ModelAndView redirectProjectList(Pageable pageable) {
    return new ModelAndView(REDIRECT + this.projectAccessUrl
            + "?" + ActionName.PAGE + "=" + pageable.getPageNumber()
            + "&" + ActionName.SIZE + "=" + pageable.getPageSize());
  }

  @SuppressWarnings("java:S5122")
  @CrossOrigin(origins = "*")
  @GetMapping("/" + HederaConstants.URL_PROJECT + "/" + ResourceName.DETAIL)
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public ModelAndView redirectProject(@PathVariable String projectShortName) {
    return new ModelAndView(REDIRECT + this.projectAccessUrl + "/" + projectShortName);
  }

  @GetMapping("/" + HederaConstants.URL_PROJECT)
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public HttpEntity<List<String>> listResearchObjectType(@PathVariable String projectShortName) {
    final Query query = SparqlQueryTool.queryToListResearchObjectTypes(projectShortName, SparqlBuilderTool.QUERY_VARIABLE);
    final List<String> list = this.executeQuery(projectShortName, query, SparqlBuilderTool.QUERY_VARIABLE);
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @GetMapping(path = "/" + HederaConstants.URL_PROJECT + "/{researchObjectTypeName}")
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public HttpEntity<List<String>> listResearchObjects(@PathVariable String projectShortName, @PathVariable String researchObjectTypeName) {
    final Query query = SparqlQueryTool.queryToListResearchObjectsOfTypes(projectShortName, researchObjectTypeName,
            SparqlBuilderTool.QUERY_VARIABLE);
    final List<String> list = this.executeQuery(projectShortName, query, SparqlBuilderTool.QUERY_VARIABLE);
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @GetMapping(path = "/" + HederaConstants.URL_PROJECT + "/{researchObjectTypeName}/{hederaId}", consumes = "application/hal+json")
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public ModelAndView redirectResearchObject(@PathVariable String projectShortName, @PathVariable String researchObjectTypeName,
          @PathVariable String hederaId) {
    final Project projectByShortName = this.projectRemoteService.getProjectByShortNameWithCache(projectShortName);
    String resourceType = ResourceName.RESEARCH_OBJECT;
    List<ResearchObjectMetadata> researchObjectMetadatalist = this.findByHederaId(projectByShortName, hederaId);
    if (researchObjectMetadatalist.size() == 1
            && ItemType.RESEARCH_DATA_FILE == researchObjectMetadatalist.get(0).getItemType()) {
      resourceType = ResourceName.RESEARCH_DATA_FILE;
    }
    return new ModelAndView(REDIRECT + this.projectAccessUrl
            + "/" + projectShortName
            + "/" + resourceType
            + "/" + hederaId);
  }

  @GetMapping("/" + HederaConstants.URL_PROJECT + "/{researchObjectTypeName}/{hederaId}/" + ActionName.DOWNLOAD)
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public ModelAndView redirectResearchObjectDownload(@PathVariable String projectShortName, @PathVariable String researchObjectTypeName,
          @PathVariable String hederaId) {
    return new ModelAndView(REDIRECT + this.projectAccessUrl
            + "/" + projectShortName
            + "/" + ResourceName.RESEARCH_DATA_FILE
            + "/" + hederaId
            + "/" + ActionName.DOWNLOAD);
  }

  @GetMapping(path = "/" + HederaConstants.URL_PROJECT + "/{researchObjectTypeName}/{hederaId}")
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public ModelAndView redirectResearchObjectToPortal(@PathVariable String projectShortName, @PathVariable String researchObjectTypeName,
          @PathVariable String hederaId) {
    return new ModelAndView(REDIRECT + this.researchObjectHomePage + hederaId);
  }

  @GetMapping(path = "/" + HederaConstants.URL_PROJECT
          + "/{researchObjectTypeName}/{hederaId}", consumes = HederaConstants.APPLICATION_JSON_LD_MIME_TYPE)
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public HttpEntity<String> getResearchObjectRdfAsJsonLd(@PathVariable String projectShortName, @PathVariable String researchObjectTypeName,
          @PathVariable String hederaId) {
    final String rdfStr = this.getSubjectRdf(projectShortName, researchObjectTypeName, hederaId, RdfFormat.JSON_LD);
    return new ResponseEntity<>(rdfStr, HttpStatus.OK);
  }

  @GetMapping(path = "/" + HederaConstants.URL_PROJECT + "/{researchObjectTypeName}/{hederaId}", consumes = { MediaType.APPLICATION_XML_VALUE,
          HederaConstants.APPLICATION_RDF_XML_MIME_TYPE })
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public HttpEntity<String> getResearchObjectRdfAsXml(@PathVariable String projectShortName, @PathVariable String researchObjectTypeName,
          @PathVariable String hederaId) {
    final String rdfStr = this.getSubjectRdf(projectShortName, researchObjectTypeName, hederaId, RdfFormat.RDF_XML);
    return new ResponseEntity<>(rdfStr, HttpStatus.OK);
  }

  @GetMapping(path = "/" + HederaConstants.URL_PROJECT + "/{researchObjectTypeName}/{hederaId}", consumes = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public HttpEntity<String> getResearchObjectRdfAsJson(@PathVariable String projectShortName, @PathVariable String researchObjectTypeName,
          @PathVariable String hederaId) {
    final String rdfStr = this.getSubjectRdf(projectShortName, researchObjectTypeName, hederaId, RdfFormat.RDF_JSON);
    return new ResponseEntity<>(rdfStr, HttpStatus.OK);
  }

  @GetMapping(path = "/" + HederaConstants.URL_PROJECT
          + "/{researchObjectTypeName}/{hederaId}", consumes = HederaConstants.APPLICATION_N_TRIPLES_MIME_TYPE)
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public HttpEntity<String> getResearchObjectRdfAsNTriples(@PathVariable String projectShortName, @PathVariable String researchObjectTypeName,
          @PathVariable String hederaId) {
    final String rdfStr = this.getSubjectRdf(projectShortName, researchObjectTypeName, hederaId, RdfFormat.N_TRIPLES);
    return new ResponseEntity<>(rdfStr, HttpStatus.OK);
  }

  @GetMapping(path = "/" + HederaConstants.URL_PROJECT
          + "/{researchObjectTypeName}/{hederaId}", consumes = HederaConstants.TEXT_TURTLE_MIME_TYPE)
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public HttpEntity<String> getResearchObjectRdfAsTurtle(@PathVariable String projectShortName, @PathVariable String researchObjectTypeName,
          @PathVariable String hederaId) {
    final String rdfStr = this.getSubjectRdf(projectShortName, researchObjectTypeName, hederaId, RdfFormat.TURTLE);
    return new ResponseEntity<>(rdfStr, HttpStatus.OK);
  }

  @GetMapping(path = "/" + HederaConstants.URL_PROJECT + "/{researchObjectTypeName}/{hederaId}", consumes = HederaConstants.TEXT_N3_MIME_TYPE)
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public HttpEntity<String> getResearchObjectRdfAsN3(@PathVariable String projectShortName, @PathVariable String researchObjectTypeName,
          @PathVariable String hederaId) {
    final String rdfStr = this.getSubjectRdf(projectShortName, researchObjectTypeName, hederaId, RdfFormat.N3);
    return new ResponseEntity<>(rdfStr, HttpStatus.OK);
  }

  private List<String> executeQuery(String projectShortName, Query query, String variable) {
    final List<String> list = new ArrayList<>();
    final ResultSet resultSet = this.triplestoreResourceService.executeQuery(projectShortName, query);
    while (resultSet.hasNext()) {
      QuerySolution solution = resultSet.nextSolution();
      list.add(solution.get(variable).toString());
    }
    return list;
  }

  private String getSubjectRdf(String projectShortName, @PathVariable String researchObjectTypeName, String hederaId, RdfFormat rdfFormat) {
    final String subjectUri = this.ldBaseURi + projectShortName + "/" + researchObjectTypeName + "/" + hederaId;
    final Query query = SparqlConstructTool.constructSubjectPropertiesTriples(subjectUri, SparqlBuilderTool.SUBJECT_VARIABLE,
            SparqlBuilderTool.PROPERTY_VARIABLE, SparqlBuilderTool.VALUE_VARIABLE);
    final Model subjectModel = this.triplestoreResourceService.executeQueryToGetModel(projectShortName, query);
    try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      subjectModel.write(outputStream, RdfTool.getJenaLanguage(rdfFormat));
      return outputStream.toString();
    } catch (IOException e) {
      throw new SolidifyProcessingException("Unable to transform RDF data", e);
    }
  }

  private List<ResearchObjectMetadata> findByHederaId(Project project, String hederaId) {
    FacetPage<ResearchObjectMetadata> researchObjectList = this.searchService.search(this.getIndex(project),
            this.buildSearchByHederaIdConditions(project.getShortName(), hederaId), null,
            Pageable.ofSize(3), null);
    return researchObjectList.getContent();
  }

  private List<SearchCondition> buildSearchByHederaIdConditions(String projectShortName, String hederaId) {
    SearchCondition hederaIdCondition = this.createTermSearchCondition(
            HederaConstants.INDEX_FIELD_HEDERA_ID,
            hederaId);

    SearchCondition projectCondition = this.createTermSearchCondition(
            HederaConstants.PROJECT_SHORT_NAME_INDEX_FIELD + HederaConstants.INDEXING_KEYWORD,
            projectShortName);

    SearchCondition requestedCondition = this
            .createNestedSearchCondition(List.of(projectCondition, hederaIdCondition));

    return List.of(requestedCondition);
  }

  private SearchCondition createTermSearchCondition(String fieldName, String fieldValue) {
    SearchCondition searchCondition = new SearchCondition();
    searchCondition.setBooleanClauseType(BooleanClauseType.MUST);
    searchCondition.setType(SearchConditionType.TERM);
    searchCondition.setField(fieldName);
    searchCondition.setValue(fieldValue);
    return searchCondition;
  }

  private SearchCondition createNestedSearchCondition(List<SearchCondition> conditions) {
    SearchCondition searchCondition = new SearchCondition();
    searchCondition.setBooleanClauseType(BooleanClauseType.MUST);
    searchCondition.setType(SearchConditionType.NESTED_BOOLEAN);
    for (SearchCondition condition : conditions) {
      searchCondition.getNestedConditions().add(condition);
    }
    return searchCondition;
  }

  private String getIndex(Project project) {
    return Boolean.TRUE.equals(project.isAccessPublic()) ? this.publicIndexName : this.privateIndexName;
  }
}
