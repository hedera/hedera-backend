/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - ProjectController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.access;

import java.nio.file.Path;
import java.security.Principal;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.service.HttpRequestInfoProvider;
import ch.unige.solidify.util.FileTool;

import ch.hedera.HederaConstants;
import ch.hedera.business.PublicProjectService;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AccessController;
import ch.hedera.controller.AccessNoSqlResourceReadOnlyController;
import ch.hedera.model.access.PublicProject;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;

@EveryonePermissions
@RestController("accessProjectController")
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_PROJECT)
public class ProjectController extends AccessNoSqlResourceReadOnlyController<PublicProject> {

  private final TrustedProjectRemoteResourceService projectRemoteResourceService;
  private final HttpRequestInfoProvider httpRequestInfoProvider;

  public ProjectController(
          PublicProjectService noSqlResourceService,
          HederaProperties config,
          TrustedProjectRemoteResourceService projectRemoteResourceService,
          HttpRequestInfoProvider httpRequestInfoProvider) {
    super(noSqlResourceService, config);
    this.projectRemoteResourceService = projectRemoteResourceService;
    this.httpRequestInfoProvider = httpRequestInfoProvider;
  }

  @Override
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public HttpEntity<PublicProject> get(@PathVariable(name = SolidifyConstants.URL_ID_FIELD) String projectShortName) {
    return super.get(projectShortName);
  }

  @Override
  public HttpEntity<RestCollection<PublicProject>> list(@ModelAttribute PublicProject search, Pageable pageable) {
    final Principal principal = this.httpRequestInfoProvider.getPrincipal();
    if (principal == null) {
      Page<PublicProject> publicProjectPage = ((PublicProjectService) this.noSqlResourceService).findOnlyPublic(search, pageable);
      return this.page2Collection(publicProjectPage, pageable);
    }
    return super.list(search, pageable);
  }

  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'DOWNLOAD_FILE')"
          + "|| @downloadTokenPermissionService.isAllowed(#projectShortName, T(ch.hedera.model.security.DownloadTokenType).PROJECT)")
  @GetMapping(HederaConstants.URL_PROJECT + "/" + HederaActionName.DOWNLOAD_LOGO)
  public HttpEntity<StreamingResponseBody> downloadLogo(@PathVariable String projectShortName) {
    final PublicProject project = this.noSqlResourceService.findOne(projectShortName);
    if (project == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    try {
      // Check if thumbnail exists
      Path thumbnailPath = this.getThumbnailPath(projectShortName);
      // Check if thumbnail already download
      if (!FileTool.checkFile(thumbnailPath)) {
        this.projectRemoteResourceService.downloadProjectLogo(project.getResId(), thumbnailPath);
      }
      return this.getThumbnail(thumbnailPath);
    } catch (HttpClientErrorException.NotFound e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

}
