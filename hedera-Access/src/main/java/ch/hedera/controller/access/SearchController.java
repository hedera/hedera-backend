/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - SearchController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.access;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.rest.BooleanClauseType;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchConditionType;
import ch.unige.solidify.rest.SearchOperator;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.service.HttpRequestInfoProvider;
import ch.unige.solidify.service.IndexFieldAliasInfoProvider;

import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AccessController;
import ch.hedera.controller.HederaIndexDataSearchController;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.rest.propagate.PropagateProjectRemoteResourceService;

@RestController
@EveryonePermissions
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_RESEARCH_OBJECT)
public class SearchController extends HederaIndexDataSearchController<ResearchObjectMetadata> {
  private static final String ACCESS_PUBLIC_FIELD = "researchProject.accessPublic";
  private static final String PROJECT_ID_FIELD = "researchProject.id.keyword";
  private final HttpRequestInfoProvider httpRequestInfoProvider;
  private final PropagateProjectRemoteResourceService projectRemoteResourceService;
  private final String publicIndexName;
  private final String privateIndexName;

  protected SearchController(
          HederaProperties config,
          IndexingService<ResearchObjectMetadata> searchService,
          IndexFieldAliasInfoProvider indexFieldAliasInfoProvider,
          HttpRequestInfoProvider httpRequestInfoProvider,
          PropagateProjectRemoteResourceService projectRemoteResourceService) {
    super(searchService, indexFieldAliasInfoProvider, config.getIndexing().getIndexName());
    this.httpRequestInfoProvider = httpRequestInfoProvider;
    this.publicIndexName = config.getIndexing().getIndexName();
    this.privateIndexName = config.getIndexing().getPrivateIndexName();
    this.projectRemoteResourceService = projectRemoteResourceService;
  }

  @TrustedUserPermissions
  @Override
  public HttpEntity<RestCollection<ResearchObjectMetadata>> list(@ModelAttribute ResearchObjectMetadata search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<ResearchObjectMetadata> get(@PathVariable String id) {
    // Create a filter with the id provided as parameter
    List<SearchCondition> searchConditions = new ArrayList<>();
    SearchCondition idCondition = new SearchCondition(SearchConditionType.TERM, "_id", id);
    searchConditions.add(idCondition);

    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    RestCollection<ResearchObjectMetadata> restCollection = super.searchPost(null, null, searchConditions, pageable).getBody();
    if (restCollection != null && !restCollection.getData().isEmpty()) {
      return new HttpEntity<>(restCollection.getData().get(0));
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @Override
  public HttpEntity<RestCollection<ResearchObjectMetadata>> search(
          @RequestParam(required = false) String query,
          @RequestParam(required = false) String conditions,
          Pageable pageable) {
    List<SearchCondition> searchConditions = new ArrayList<>();
    return super.searchPost(query, conditions, searchConditions, pageable);
  }

  @Override
  public HttpEntity<RestCollection<ResearchObjectMetadata>> searchPost(
          @RequestParam(required = false) String query,
          @RequestParam(required = false) String conditions,
          @RequestBody(required = false) List<SearchCondition> searchConditions,
          Pageable pageable) {
    return super.searchPost(query, conditions, searchConditions, pageable);
  }

  @Override
  protected List<SearchCondition> beforeSearch(List<SearchCondition> searchConditions) {
    final Principal principal = this.httpRequestInfoProvider.getPrincipal();
    if (principal == null) {
      this.setIndexName(this.publicIndexName);
    } else {
      this.setIndexName(this.privateIndexName);
      this.addFilterByPrivateProjectsAndAccessPublicFields(searchConditions, principal);
    }
    return searchConditions;
  }

  private void addFilterByPrivateProjectsAndAccessPublicFields(List<SearchCondition> searchConditions, Principal principal) {
    // Add two filter nested: one for each project and another to filter by project with access public combined with an OR
    // (BooleanClauseType.SHOULD)
    SearchCondition nestedCondition = new SearchCondition();
    nestedCondition.setBooleanClauseType(BooleanClauseType.MUST);
    nestedCondition.setType(SearchConditionType.NESTED_BOOLEAN);

    // Add filter to search for each private authorized projects
    List<Project> authorizedProjects = this.projectRemoteResourceService.getAuthorizedProjects(principal);
    List<String> idsPrivateProject = authorizedProjects.stream().filter(p -> !p.isAccessPublic()).map(Project::getResId).toList();
    if (!idsPrivateProject.isEmpty()) {
      SearchCondition projectIdsCondition = new SearchCondition(SearchConditionType.TERM, PROJECT_ID_FIELD, idsPrivateProject);
      projectIdsCondition.setSearchOperator(SearchOperator.OR);
      projectIdsCondition.setBooleanClauseType(BooleanClauseType.SHOULD);
      nestedCondition.getNestedConditions().add(projectIdsCondition);

    }
    // Add filter to search in public projects.
    SearchCondition accessPublicCondition = new SearchCondition();
    accessPublicCondition.setField(ACCESS_PUBLIC_FIELD);
    accessPublicCondition.setValue(Boolean.TRUE.toString());
    accessPublicCondition.setType(SearchConditionType.TERM);
    accessPublicCondition.setBooleanClauseType(BooleanClauseType.SHOULD);
    nestedCondition.getNestedConditions().add(accessPublicCondition);

    searchConditions.add(nestedCondition);
  }
}
