/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - ProjectRdfResearchObjectTypeController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.access;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.EveryonePermissions;

import ch.hedera.HederaConstants;
import ch.hedera.business.ResearchObjectService;
import ch.hedera.controller.AccessController;
import ch.hedera.model.triplestore.SparqlResultRow;
import ch.hedera.rest.ResourceName;
import ch.hedera.rest.UrlPath;

@EveryonePermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_PROJECT + "/" + HederaConstants.URL_PROJECT)
public class ProjectRdfResearchObjectTypeController {

  private final ResearchObjectService researchObjectService;

  public ProjectRdfResearchObjectTypeController(ResearchObjectService researchObjectService) {
    this.researchObjectService = researchObjectService;
  }

  @GetMapping(value = { "/{researchObjectTypeName}/" + ResourceName.RESEARCH_OBJECT })
  public ResponseEntity<RestCollection<SparqlResultRow>> executeQueryForList(@PathVariable String projectShortName,
          @PathVariable String researchObjectTypeName, Pageable pageable) {
    Page<SparqlResultRow> datasetEntryList = this.researchObjectService.listResearchObjects(projectShortName, researchObjectTypeName, pageable);
    return new ResponseEntity<>(this.toRestCollection(datasetEntryList), HttpStatus.OK);
  }

  @PostMapping(value = { "/{researchObjectTypeName}/" + ResourceName.RESEARCH_OBJECT }, consumes = MediaType.TEXT_PLAIN_VALUE)
  public ResponseEntity<RestCollection<SparqlResultRow>> executeQueryForDetail(@PathVariable String projectShortName,
          @PathVariable String researchObjectTypeName, @RequestBody String researchObjectId) {
    Page<SparqlResultRow> datasetEntryProperties = this.researchObjectService.getResearchObject(projectShortName, researchObjectTypeName,
            researchObjectId);
    return new ResponseEntity<>(this.toRestCollection(datasetEntryProperties), HttpStatus.OK);
  }

  private RestCollection<SparqlResultRow> toRestCollection(Page<SparqlResultRow> resultsPage) {
    return new RestCollection<>(resultsPage, resultsPage.getPageable());
  }
}
