/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - AccessDownloadTokenController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.access;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.model.security.DownloadToken;
import ch.unige.solidify.repository.DownloadTokenRepository;
import ch.unige.solidify.rest.ActionName;

import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AccessController;
import ch.hedera.controller.HederaDownloadTokenController;
import ch.hedera.model.security.DownloadTokenType;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ResourceName;
import ch.hedera.rest.UrlPath;

@RestController
@ConditionalOnBean(AccessController.class)
public class AccessDownloadTokenController extends HederaDownloadTokenController {

  private final String accessModulePath;
  private final String iiifModulePath;

  public AccessDownloadTokenController(
          DownloadTokenRepository downloadTokenRepository,
          SolidifyProperties solidifyConfig,
          HederaProperties config) throws URISyntaxException {
    super(solidifyConfig, downloadTokenRepository);
    this.accessModulePath = new URI(config.getModule().getAccess().getPublicUrl()).getPath();
    this.iiifModulePath = new URI(config.getModule().getIIIF().getPublicUrl()).getPath();
  }

  /**
   * DownloadToken can be created only for public projects or for private project in which the user has a role. The action used is
   * DOWNLOAD that is used for all downloads in access module.
   */
  @GetMapping(UrlPath.ACCESS_PROJECT + "/{projectShortName}/" + ActionName.DOWNLOAD_TOKEN)
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'DOWNLOAD')")
  public ResponseEntity<DownloadToken> getTokenForProject(@PathVariable String projectShortName) {
    final String cookiePath = this.accessModulePath
            + "/" + ResourceName.PROJECT
            + "/" + projectShortName
            + "/" + HederaActionName.DOWNLOAD_LOGO;
    return this.getToken(projectShortName, DownloadTokenType.PROJECT, cookiePath);
  }

  @GetMapping(UrlPath.ACCESS_PROJECT_RESEARCH_DATA_FILE + "/{fileName}/" + ActionName.DOWNLOAD_TOKEN)
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'DOWNLOAD')")
  public ResponseEntity<DownloadToken> getTokenForResearchDataFile(@PathVariable String projectShortName, @PathVariable String fileName) {
    final String cookiePath = this.accessModulePath
            + "/" + ResourceName.PROJECT
            + "/" + projectShortName
            + "/" + ResourceName.RESEARCH_DATA_FILE
            + "/" + fileName
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(projectShortName, DownloadTokenType.RESEARCH_DATA_FILE, cookiePath);
  }

  @GetMapping(UrlPath.IIIF_PROXY + "/{protocolVersion:\\d}/{projectShortName}/" + ActionName.DOWNLOAD_TOKEN)
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'DOWNLOAD')")
  public ResponseEntity<DownloadToken> getTokenForIiif(@PathVariable String protocolVersion, @PathVariable String projectShortName) {
    final String cookiePath = this.iiifModulePath + "/" + protocolVersion + "/" + projectShortName;
    return this.getToken(projectShortName, DownloadTokenType.IIIF, cookiePath);
  }
}
