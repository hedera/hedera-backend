/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - ProjectResearchDataFileController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.access;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import jakarta.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.security.EveryonePermissions;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.config.HederaProperties.Storage;
import ch.hedera.controller.AccessController;
import ch.hedera.controller.ResourceByProjectReadOnlyController;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.settings.Project;
import ch.hedera.model.xml.hedera.v1.researchObject.ItemType;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedResearchDataFileRemoteResourceService;
import ch.hedera.service.storage.ProjectStorage;

@EveryonePermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_PROJECT_RESEARCH_DATA_FILE)
public class ProjectResearchDataFileController extends ResourceByProjectReadOnlyController {

  private final TrustedResearchDataFileRemoteResourceService researchDataFileRemoteService;
  private final Storage storageConfig;

  protected ProjectResearchDataFileController(HederaProperties config, TrustedProjectRemoteResourceService projectRemoteService,
          IndexingService<ResearchObjectMetadata> searchService, TrustedResearchDataFileRemoteResourceService researchDataFileRemoteService) {
    super(config, projectRemoteService, searchService);
    this.storageConfig = config.getStorage();
    this.researchDataFileRemoteService = researchDataFileRemoteService;
  }

  @Override
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public HttpEntity<ResearchObjectMetadata> get(@PathVariable String projectShortName, @PathVariable String hederaId) {
    return super.get(projectShortName, hederaId);
  }

  @Override
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'LIST_FILES')")
  public HttpEntity<RestCollection<ResearchObjectMetadata>> list(@PathVariable String projectShortName,
          @ModelAttribute ResearchObjectMetadata search,
          Pageable pageable) {
    return super.list(projectShortName, search, pageable);
  }

  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'DOWNLOAD_FILE')"
          + "|| @downloadTokenPermissionService.isAllowed(#projectShortName, T(ch.hedera.model.security.DownloadTokenType).RESEARCH_DATA_FILE)")
  @GetMapping("/**")
  public HttpEntity<StreamingResponseBody> download(@PathVariable String projectShortName, HttpServletRequest request) throws IOException {
    Project projectByShortName = this.projectRemoteService.getProjectByShortNameWithCache(projectShortName);
    final String[] requestPaths = this.extractEndingPath(request).split("/");

    // Check action
    if (requestPaths.length < 2 || !requestPaths[requestPaths.length - 1].equals(ActionName.DOWNLOAD)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    ResearchObjectMetadata researchObjectMetadata = null;
    if (requestPaths.length == 2) {
      // One parameter => hederaId or filename without relative location
      final String id = requestPaths[0];
      researchObjectMetadata = this.findByHederaId(projectByShortName, id);
      if (researchObjectMetadata == null) {
        researchObjectMetadata = this.findByBusinessId(projectByShortName, "/", id);
      }
    } else {
      // several paramaters => filename with relative location
      final String fileName = requestPaths[requestPaths.length - 2];
      StringBuilder relativeLocation = new StringBuilder();
      for (int i = 0; i < requestPaths.length - 2; i++) {
        relativeLocation.append("/").append(requestPaths[i]);
      }
      researchObjectMetadata = this.findByBusinessId(projectByShortName, relativeLocation.toString(), fileName);
    }
    if (researchObjectMetadata == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    ResearchDataFile researchDataFile = this.researchDataFileRemoteService.findOne(researchObjectMetadata.getResId());
    // Get the filename and convert it to a stream
    ProjectStorage projectStorage = ProjectStorage.getProjectStorage(this.storageConfig, projectByShortName);
    InputStream is = projectStorage.getResearchDataFile(researchDataFile);
    return this.buildDownloadResponseEntity(is, researchObjectMetadata.getFilename(), researchObjectMetadata.getMimeType(),
            researchDataFile.getFileSize());
  }

  private ResearchObjectMetadata findByBusinessId(Project project, String relativeLocation, String fileName) {
    FacetPage<ResearchObjectMetadata> researchObjectList = this.searchService.search(this.getIndex(project),
            this.buildSearchByBusinessIdConditions(project.getShortName(), relativeLocation, fileName), null,
            Pageable.ofSize(1), null);

    return researchObjectList.getContent().stream()
            .filter(rs -> rs.getProjectId().equals(project.getProjectId()) && rs.getFilename().equals(fileName)
                    && rs.getRelativeLocation().equals(relativeLocation))
            .findFirst()
            .orElse(null);
  }

  private List<SearchCondition> buildSearchByBusinessIdConditions(String projectShortName, String relativeLocation, String fileName) {
    SearchCondition fileNameCondition = this.createTermSearchCondition(
            HederaConstants.INDEX_FIELD_FILENAME + HederaConstants.INDEXING_KEYWORD,
            fileName);
    SearchCondition relativeCondition = this.createTermSearchCondition(
            HederaConstants.INDEX_FIELD_RELATIVE_LOCATION + HederaConstants.INDEXING_KEYWORD,
            relativeLocation);

    SearchCondition projectCondition = this.createTermSearchCondition(
            HederaConstants.PROJECT_SHORT_NAME_INDEX_FIELD + HederaConstants.INDEXING_KEYWORD,
            projectShortName);

    SearchCondition requestedCondition = this.createNestedSearchCondition(List.of(projectCondition, fileNameCondition, relativeCondition));

    return List.of(requestedCondition);
  }

  @Override
  protected SearchCondition createItemTypeCondition() {
    return this.createTermSearchCondition(HederaConstants.ITEM_TYPE_INDEX_FIELD + HederaConstants.INDEXING_KEYWORD,
            ItemType.RESEARCH_DATA_FILE.value());
  }

}
