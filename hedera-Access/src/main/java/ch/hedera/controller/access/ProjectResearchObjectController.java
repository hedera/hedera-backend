/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - ProjectResearchObjectController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.access;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.security.EveryonePermissions;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AccessController;
import ch.hedera.controller.ResourceByProjectReadOnlyController;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.model.xml.hedera.v1.researchObject.ItemType;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;

@EveryonePermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_PROJECT_RESEARCH_OBJECT)
public class ProjectResearchObjectController extends ResourceByProjectReadOnlyController {

  protected ProjectResearchObjectController(HederaProperties config, TrustedProjectRemoteResourceService projectRemoteService,
          IndexingService<ResearchObjectMetadata> searchService) {
    super(config, projectRemoteService, searchService);
  }

  @Override
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'GET')")
  public HttpEntity<ResearchObjectMetadata> get(@PathVariable String projectShortName, @PathVariable String hederaId) {
    return super.get(projectShortName, hederaId);
  }

  @Override
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProjectName(#projectShortName, 'LIST')")
  public HttpEntity<RestCollection<ResearchObjectMetadata>> list(@PathVariable String projectShortName,
          @ModelAttribute ResearchObjectMetadata search, Pageable pageable) {
    return super.list(projectShortName, search, pageable);
  }

  @Override
  protected SearchCondition createItemTypeCondition() {
    return this.createTermSearchCondition(HederaConstants.ITEM_TYPE_INDEX_FIELD + HederaConstants.INDEXING_KEYWORD,
            ItemType.RESEARCH_OBJECT.value());
  }

}
