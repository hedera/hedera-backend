/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - SparqlQueryProxyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.sparql;

import java.util.Set;

import org.apache.jena.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.validation.ValidationError;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AccessController;
import ch.hedera.model.humanities.RmlFormat;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.rest.fallback.FallbackProjectRemoteResourceService;
import ch.hedera.service.triplestore.TriplestoreResourceService;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlTool;

@EveryonePermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.SPARQL_PROXY)
public class SparqlQueryProxyController {
  private static final Logger logger = LoggerFactory.getLogger(SparqlQueryProxyController.class);

  private final TriplestoreResourceService triplestoreResourceService;
  private final FallbackProjectRemoteResourceService projectRemoteResourceService;

  private String sparqlProxyUrl;

  public SparqlQueryProxyController(TriplestoreResourceService triplestoreResourceService,
          FallbackProjectRemoteResourceService projectRemoteResourceService,
          HederaProperties config) {
    this.triplestoreResourceService = triplestoreResourceService;
    this.projectRemoteResourceService = projectRemoteResourceService;
    this.sparqlProxyUrl = config.getModule().getSparql().getPublicUrl();
  }

  @GetMapping(value = "/{projectName}", params = HederaConstants.QUERY_PARAM)
  @PreAuthorize("@sparqlQueryPermissionService.isAllowed(#projectName, 'EXEC_QUERY')")
  public ResponseEntity<String> executeGetQuery(@PathVariable String projectName,
          @RequestParam(name = HederaConstants.QUERY_PARAM) String queryStr,
          @RequestHeader(HttpHeaders.ACCEPT) String accept) {
    return this.executeQuery(queryStr, projectName, accept);
  }

  @PostMapping(value = "/{projectName}", consumes = MediaType.TEXT_PLAIN_VALUE)
  @PreAuthorize("@sparqlQueryPermissionService.isAllowed(#projectName, 'EXEC_QUERY')")
  public ResponseEntity<String> executePostQuery(@PathVariable String projectName,
          @RequestBody String queryStr,
          @RequestHeader(HttpHeaders.ACCEPT) String accept) {
    return this.executeQuery(queryStr, projectName, accept);
  }

  @PostMapping(value = "/{projectName}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  @PreAuthorize("@sparqlQueryPermissionService.isAllowed(#projectName, 'EXEC_QUERY')")
  public ResponseEntity<String> executeUrlEncodedQuery(@PathVariable String projectName,
          @RequestBody MultiValueMap<String, String> parametersMap,
          @RequestHeader(HttpHeaders.ACCEPT) String accept) {
    if (parametersMap.containsKey(HederaConstants.QUERY_PARAM)) {
      final String queryStr = parametersMap.getFirst(HederaConstants.QUERY_PARAM);
      return this.executeQuery(queryStr, projectName, accept);
    } else {
      throw new SolidifyValidationException(new ValidationError("Missing query param in body"));
    }
  }

  private ResponseEntity<String> executeQuery(String queryStr, String projectShortName, String accept) {
    // check if request is a delete
    if (SparqlTool.isDeleteOrUpdateQuery(queryStr)) {
      logger.error("Sparql query is of forbidden type UPDATE or DELETE");
      return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
    }

    if (SparqlTool.isFederatedQuery(queryStr)) {
      Set<String> endpoints = SparqlTool.extractEndpoints(queryStr);
      logger.debug("Sparql query is a federate type, endpoint list: {}", endpoints);
      for (String endpoint : endpoints) {
        if (endpoint.contains(this.sparqlProxyUrl)) {
          logger.error("Sparql federate query not allow to use our service: {}", this.sparqlProxyUrl);
          return new ResponseEntity<>(HttpStatus.FORBIDDEN); // don't allow query that point to our sparql proxy
        }
      }
    }

    RmlFormat format;
    switch (accept) {
      case MediaType.TEXT_XML_VALUE, HederaConstants.APPLICATION_SPARQL_RESULT_XML_MIME_TYPE, HederaConstants.APPLICATION_RDF_XML_MIME_TYPE -> format = RmlFormat.XML;
      case HederaConstants.TEXT_CSV_MIME_TYPE -> format = RmlFormat.CSV;
      default -> format = RmlFormat.JSON;
    }

    final Project project = this.projectRemoteResourceService.getProjectByShortNameWithCache(projectShortName);
    String result;
    String contentType;
    if (project != null) {
      final Query query = SparqlBuilderTool.createQuery(queryStr);
      switch (format) {
        case CSV -> {
          result = this.triplestoreResourceService.executeQueryAndReturnCSV(project.getShortName(), query);
          contentType = MediaType.TEXT_PLAIN_VALUE;
        }
        case XML -> {
          result = this.triplestoreResourceService.executeQueryAndReturnXML(project.getShortName(), query);
          contentType = MediaType.TEXT_XML_VALUE;
        }
        default -> {
          result = this.triplestoreResourceService.executeQueryAndReturnJSON(project.getShortName(), query);
          contentType = MediaType.APPLICATION_JSON_VALUE;
        }
      }
      return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, contentType).body(result);
    } else {
      logger.error("Project not found !!");
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

}
