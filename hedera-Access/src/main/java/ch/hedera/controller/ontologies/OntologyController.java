/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - OntologyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.ontologies;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.NoSqlResourceReadOnlyController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.service.NoSqlResourceService;
import ch.unige.solidify.util.FileTool;

import ch.hedera.business.PublicOntologyService;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AccessController;
import ch.hedera.model.access.PublicOntology;
import ch.hedera.rest.UrlPath;

@EveryonePermissions
@RestController("ldOntologyController")
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ONTOLOGY_PROXY)
public class OntologyController extends NoSqlResourceReadOnlyController<PublicOntology> {

  private static final Logger log = LoggerFactory.getLogger(OntologyController.class);

  private final String ontologyLocation;

  public OntologyController(HederaProperties config, NoSqlResourceService<PublicOntology> noSqlResourceService) {
    super(noSqlResourceService);
    this.ontologyLocation = config.getOntologyLocation();
  }

  @Override
  @GetMapping(path = SolidifyConstants.URL_ID, consumes = { MediaTypes.HAL_JSON_VALUE })
  public HttpEntity<PublicOntology> get(@PathVariable String id) {
    return super.get(id);
  }

  @GetMapping(path = SolidifyConstants.URL_ID)
  public HttpEntity<String> getOntologyContent(@PathVariable String id) throws IOException {
    final Path ontoFile = Path.of(this.ontologyLocation, id);
    if (!FileTool.checkFile(ontoFile)) {
      ((PublicOntologyService) this.noSqlResourceService).download(id);
    }
    return new ResponseEntity<>(FileTool.toString(ontoFile), HttpStatus.OK);
  }

  @Override
  public HttpEntity<RestCollection<PublicOntology>> list(@ModelAttribute PublicOntology search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  protected void addLinks(PublicOntology ontology) {
    super.addLinks(ontology);
    if (ontology.getUrl() == null) {
      try {
        Optional<Link> selfLink = ontology.getLink(IanaLinkRelations.SELF);
        if (selfLink.isPresent()) {
          ontology.setUrl(new URL(selfLink.get().getHref()));
        }
      } catch (MalformedURLException e) {
        log.warn("Cannot get the SELF HATEOS link");
      }
    }
  }
}
