/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - OaiPmhTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.xml.bind.JAXBException;

import org.mockito.Mock;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Pageable;

import com.fasterxml.jackson.core.JsonProcessingException;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.model.oai.OAIMetadataPrefix;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.OAIService;
import ch.unige.solidify.test.oai.OAIServiceTest;
import ch.unige.solidify.util.FileTool;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.config.HederaRepositoryDescription;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.model.xml.hedera.v1.researchObject.ObjectType;
import ch.hedera.service.MetadataService;
import ch.hedera.service.SearchService;

public class OaiPmhTest extends OAIServiceTest {

  private Path testFolder = Paths.get("src", "test", "resources").toAbsolutePath();

  protected String indexName;
  protected HederaProperties config;
  protected HederaRepositoryDescription repositoryDescription;
  protected MetadataService metadataService;
  protected SearchService searchService;

  @Mock
  private IndexingService<ResearchObjectMetadata> searchingService;

  @Override
  protected void localSetup() {
    this.config = new HederaProperties(new SolidifyProperties(new GitInfoProperties()));
    this.config.getModule().getLinkedData().setUrl("https://hedera.unige.ch/ld");
    this.indexName = this.config.getIndexing().getIndexName();
    this.repositoryDescription = new HederaRepositoryDescription(this.gitInfoProperties);
    this.repositoryDescription.setArchiveHomePage(HOST_URL + "/");
    // Git Info Service
    this.gitInfoProperties.getBuild().setVersion("1.x");
    // MetadataService
    this.metadataService = new MetadataService(this.messageService);
    // Search Service
    this.searchService = new SearchService(this.messageService, this.config, this.searchingService, this.metadataService);
    // OAI Service
    this.oaiService = new OAIService(
            this.oaiConfig,
            this.repositoryDescription,
            this.oaiMetadataPrefixService,
            this.oaiSetService,
            this.searchService,
            this.searchService,
            this.searchService);

    // Mock Service
    try {
      for (String id : this.getIdentifierList()) {
        ResearchObjectMetadata researchObjectMetadata = this.getResearchObjectMetadata(id);
        when(this.searchingService.findOne(this.indexName, id)).thenReturn(researchObjectMetadata);
      }
      FacetPage<ResearchObjectMetadata> researchObjectList = this.getResearchObjectMetadataList();
      when(this.searchingService.search(any(String.class), anyList(), isNull(), any(Pageable.class), isNull(FieldsRequest.class)))
              .thenReturn(researchObjectList);
    } catch (IOException | JAXBException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  @Override
  protected String[] getMetadataList() {
    return new String[] { OAIConstants.OAI_DC, HederaConstants.OAI_HEDERA };
  }

  @Override
  protected List<String> getIdentifierList() {
    List<String> list = new ArrayList<>();
    list.add("research-object");
    list.add("research-data-file");
    return list;
  }

  private ResearchObjectMetadata getResearchObjectMetadata(String id) throws JsonProcessingException, JAXBException, MalformedURLException {
    String jsonObject;
    if (id.equals("research-object")) {
      jsonObject = this.metadataService.getMetadataInJson(this.getProject(), this.getResearchObjectType(), id, id, ObjectType.MANAGED,
              OffsetDateTime.now(), "sourceId", this.getProperties(id));
    } else {
      ResearchDataFile researchDataFile = this.getResearchDataFile(id);
      jsonObject = this.metadataService.getMetadataInJson(researchDataFile);
    }
    return new ResearchObjectMetadata(this.indexName, id, jsonObject);
  }

  private Map<String, Object> getProperties(String id) {
    Map<String, Object> properties = new HashMap<>();
    properties.put(id + "field1", id + "value1");
    properties.put(id + "field2", id + "value2");
    return properties;
  }

  private ResearchDataFile getResearchDataFile(String id) throws MalformedURLException {
    ResearchDataFile researchDataFile = new ResearchDataFile();
    researchDataFile.setResId(id);
    researchDataFile.getLastUpdate().setWhen(OffsetDateTime.now());
    researchDataFile.setRelativeLocation("/");
    researchDataFile.setFileName("research-data.jpg");
    researchDataFile.setMimeType("image/jpeg");
    researchDataFile.setProject(this.getProject());
    researchDataFile.setResearchObjectType(this.getResearchObjectType());
    researchDataFile.setMetadata(this.getProperties(id));
    researchDataFile.setAccessibleFrom(DataAccessibilityType.IIIF);
    final Path image = this.getFilePath(this.testFolder, "geneve.jpg");
    researchDataFile.setSourcePath(image.toUri());
    researchDataFile.setFileSize(FileTool.getSize(image.toUri()));
    return researchDataFile;
  }

  private ResearchObjectType getResearchObjectType() throws MalformedURLException {
    ResearchObjectType rot = new ResearchObjectType();
    rot.setResId("rot");
    rot.setName("digital-objet");
    rot.setRdfType("D1");
    rot.setOntology(this.getOntology());
    return rot;
  }

  private Ontology getOntology() throws MalformedURLException {
    Ontology onto = new Ontology();
    onto.setResId("onto");
    onto.setName("CIDOC-CRM");
    onto.setVersion("7.1.2");
    onto.setFormat(RdfFormat.RDF_XML);
    onto.setBaseUri(URI.create("http://www.cidoc-crm.org/cidoc-crm/"));
    onto.setUrl(new URL("https://www.cidoc-crm.org/Version/version-7.1.2"));
    return onto;
  }

  private Project getProject() {
    Project p = new Project();
    p.setResId("projectId");
    p.setName("project");
    p.setShortName(p.getName());
    p.setKeywords(List.of("keyword1", "Keywrod2"));
    p.setOpeningDate(LocalDate.now());
    p.setAccessPublic(Boolean.TRUE);
    return p;
  }

  private FacetPage<ResearchObjectMetadata> getResearchObjectMetadataList() throws IOException, JAXBException {
    List<ResearchObjectMetadata> list = new ArrayList<>();
    for (String id : this.getIdentifierList()) {
      list.add(this.getResearchObjectMetadata(id));
    }
    return new FacetPage<>(list);
  }

  @Override
  protected String getMetadataXmlTransformation() throws IOException {
    ClassPathResource xslResource = new ClassPathResource(SolidifyConstants.XSL_HOME + "/hedera2oai_dc.xsl");
    return FileTool.toString(xslResource.getInputStream());
  }

  @Override
  protected String getReferenceOAIMetadataPrefixName() {
    return HederaConstants.OAI_HEDERA;
  }

  @Override
  protected OAIMetadataPrefix getReferenceOAIMetadataPrefix() throws MalformedURLException {
    OAIMetadataPrefix oaiMetadataPrefix = new OAIMetadataPrefix();
    oaiMetadataPrefix.setPrefix(HederaConstants.OAI_HEDERA);
    oaiMetadataPrefix.setName(HederaConstants.OAI_HEDERA_NAME);
    oaiMetadataPrefix.setReference(true);
    oaiMetadataPrefix.setSchemaUrl(new URL(HederaConstants.HEDERA_SCHEMA_1));
    oaiMetadataPrefix.setSchemaNamespace(HederaConstants.HEDERA_NAMESPACE_1);
    oaiMetadataPrefix.setEnabled(true);
    return oaiMetadataPrefix;
  }

  private Path getFilePath(Path parentPath, String filename) {
    final Path file = parentPath.resolve(filename);
    assertNotNull(file);
    assertTrue(FileTool.checkFile(file));
    return file;
  }
}
