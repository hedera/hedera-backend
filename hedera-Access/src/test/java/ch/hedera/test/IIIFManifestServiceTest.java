/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Access - IIIFManifestServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.util.JSONTool;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.model.iiif.IIIFManifest;
import ch.hedera.model.iiif.IIIFMetadataItem;
import ch.hedera.model.iiif.IIIFPageDTO;
import ch.hedera.service.iiif.IIIFManifestGenerationService;
import ch.hedera.service.triplestore.TriplestoreResourceService;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
class IIIFManifestServiceTest {

  private static final String TEST_PUBLISHER = "Springer";
  private static final String TEST_LOCATION = "Berlin";

  @Mock
  private TriplestoreResourceService triplestoreResourceService;

  private final HederaProperties config;

  private final IIIFManifestGenerationService iiifManifestService;

  protected IIIFManifestServiceTest() {
    this.config = new HederaProperties(new SolidifyProperties(new GitInfoProperties()));
    this.config.getModule().getIIIF().setUrl("https://hedera.unige.ch/iiif");
    this.config.getModule().getLinkedData().setUrl("https://hedera.unige.ch/ld");
    this.iiifManifestService = new IIIFManifestGenerationService(this.config, this.triplestoreResourceService);
  }

  @Test
  void generationTest() {
    final IIIFManifest iiifManifest = this.generateIIIFManifest();
    assertTrue(this.isMetadataPresent(iiifManifest, "publisher", TEST_PUBLISHER));
    assertTrue(this.isMetadataPresent(iiifManifest, "location", TEST_LOCATION));

    final ObjectMapper objectMapper = new ObjectMapper();
    assertDoesNotThrow(() -> objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(iiifManifest));
  }

  @Test
  void serializeTest() {
    final IIIFManifest iiifManifest = this.generateIIIFManifest();
    assertDoesNotThrow(() -> JSONTool.convert2JsonString(iiifManifest));
  }

  @Test
  void deserializeTest() {
    final IIIFManifest iiifManifest = this.generateIIIFManifest();
    final String iiifManifestStr = JSONTool.convert2JsonString(iiifManifest);
    assertDoesNotThrow(() -> new ObjectMapper().readValue(iiifManifestStr, IIIFManifest.class));
  }

  private IIIFManifest generateIIIFManifest() {
    final String manifestId = "123";
    final String copyrightHolder = "Unige";
    final Map<String, List<String>> manifestValues = new HashMap<>();
    manifestValues.put("label", Arrays.asList("My manifest label"));
    manifestValues.put("manifest_md_publisher", Arrays.asList(TEST_PUBLISHER));
    manifestValues.put("manifest_md_location", Arrays.asList(TEST_LOCATION));

    final IIIFPageDTO page1 = new IIIFPageDTO("toto.jpg", "1", 200, 300);
    final IIIFPageDTO page2 = new IIIFPageDTO("titi.jpg", "2", 200, 300);
    final IIIFPageDTO page3 = new IIIFPageDTO("tata.jpg", "3", 200, 300);
    final List<IIIFPageDTO> pageList = Arrays.asList(page1, page2, page3);

    return this.iiifManifestService.generateIIIFManifest(manifestId, manifestValues, copyrightHolder, pageList);
  }

  private boolean isMetadataPresent(IIIFManifest iiifManifest, String label, String value) {
    for (IIIFMetadataItem item : iiifManifest.getMetadata()) {
      if (label.equals(item.getLabel().getMultiLanguageMap().get(HederaConstants.MANIFEST_DEFAULT_LANGUAGE).get(0)) &&
              value.equals(item.getValue().getMultiLanguageMap().get(HederaConstants.MANIFEST_DEFAULT_LANGUAGE).get(0))) {
        return true;
      }
    }
    return false;
  }
}
