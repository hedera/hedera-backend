/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - RdfToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.rdf;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.util.FileTool;

import ch.hedera.model.RdfFormat;
import ch.hedera.util.RdfTool;

@ExtendWith(SpringExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class RdfToolTest {

  protected Path sourceFolder = Paths.get("src", "test", "resources").toAbsolutePath();

  @Order(10)
  @ParameterizedTest
  @MethodSource("validFileList")
  void wellformedRdf(String filename, RdfFormat rdfFormat) throws IOException {
    final Path file = this.getDataFile(filename);
    RdfTool.wellformed(FileTool.getInputStream(file), rdfFormat);
  }

  @Order(20)
  @ParameterizedTest
  @MethodSource("validFileList")
  void validateRdf(String filename, RdfFormat rdfFormat) throws IOException {
    final Path file = this.getDataFile(filename);
    RdfTool.validate(FileTool.getInputStream(file), rdfFormat);
  }

  protected Path getDataFile(String filename) {
    final Path file = this.sourceFolder.resolve(filename);
    assertNotNull(file);
    assertTrue(FileTool.checkFile(file));
    return file;
  }

  private static Stream<Arguments> validFileList() {
    return Stream.of(
            Arguments.of("Moreno-001.ttl", RdfFormat.TURTLE),
            Arguments.of("ludus-data-1.0.ttl", RdfFormat.TURTLE),
            Arguments.of("pliegos-1.0.ttl", RdfFormat.TURTLE),
            Arguments.of("CRMdig_v3.2.1.rdfs", RdfFormat.RDF_XML),
            Arguments.of("rdf-sample.xml", RdfFormat.RDF_XML),
            Arguments.of("bayeux.nt", RdfFormat.N_TRIPLES));
  }

}
