/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - SparqlQueryToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.rdf;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.Syntax;
import org.apache.jena.rdf.model.Model;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.util.FileTool;

import ch.hedera.util.RdfTool;
import ch.hedera.util.SparqlQueryTool;

@ExtendWith(SpringExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SparqlQueryToolTest {

  private static final Logger log = LoggerFactory.getLogger(SparqlQueryToolTest.class);

  protected Path sourceFolder = Paths.get("src", "test", "resources").toAbsolutePath();

  @Order(10)
  @Test
  void printQuery() {
    final Query query = SparqlQueryTool.queryToListResearchObjectTypes("project", SparqlQueryTool.QUERY_VARIABLE);
    assertNotNull(query);
    log.info("\nQuery.toString():\n{}\n", query);
    log.info("\nQuery.serialiaze():\n{}\n", query.serialize());
    log.info("\nQuery.serialiaze(syntaxSPARQL):\n{}\n", query.serialize(Syntax.syntaxSPARQL));
  }

  @Order(20)
  @Test
  void testRdfTypeQuery() {
    Model model = RdfTool.getRdfModel(this.getDataFile("Moreno-001.ttl"));
    final Query localQuery = SparqlQueryTool.queryToListRdfTypes("?t");
    log.info("Query to list RDF type:\n {}", localQuery);
    int count = 0;
    try (QueryExecution localExecution = QueryExecutionFactory.create(localQuery, model)) {
      ResultSet resultSet = localExecution.execSelect();
      while (resultSet.hasNext()) {
        QuerySolution solution = resultSet.nextSolution();
        count++;
        log.info("{}) {}", count, solution.get("?t"));
      }
    }
    assertEquals(10, count);
  }

  @Order(20)
  @Test
  void testSubjectQuery() {
    Model model = RdfTool.getRdfModel(this.getDataFile("Moreno-001.ttl"));
    final Query typeQuery = SparqlQueryTool.queryToListRdfTypes("?t");
    int count = 0;
    try (QueryExecution typeExecution = QueryExecutionFactory.create(typeQuery, model)) {
      ResultSet typeResultSet = typeExecution.execSelect();
      while (typeResultSet.hasNext()) {
        QuerySolution result = typeResultSet.nextSolution();
        count++;
        this.listSubjects(model, result.get("?t").toString(), false);
      }
    }
    assertEquals(10, count);
  }

  @Order(30)
  @Test
  void testSubjectPropertiesQuery() {
    Model model = RdfTool.getRdfModel(this.getDataFile("Moreno-001.ttl"));
    final Query typeQuery = SparqlQueryTool.queryToListRdfTypes("?t");
    int typeCount = 0;
    try (QueryExecution typeExecution = QueryExecutionFactory.create(typeQuery, model)) {
      ResultSet typeResultSet = typeExecution.execSelect();
      while (typeResultSet.hasNext()) {
        QuerySolution result = typeResultSet.nextSolution();
        typeCount++;
        this.listSubjects(model, result.get("?t").toString(), true);
      }
    }
    assertEquals(10, typeCount);
  }

  private void listSubjects(Model model, String rdfType, boolean withProperties) {
    final Query listQuery = SparqlQueryTool.queryToListSubjectsOfRdfType("?s", rdfType);
    log.info("Query to list subjects:\n {}", listQuery);
    int count = 0;
    try (QueryExecution listExecution = QueryExecutionFactory.create(listQuery, model)) {
      ResultSet listResultSet = listExecution.execSelect();
      while (listResultSet.hasNext()) {
        QuerySolution solution = listResultSet.nextSolution();
        count++;
        log.info("{}) {}", count, solution.get("?s"));
        if (withProperties) {
          this.listSubjectProperties(model, solution.get("?s").toString());
        }
      }
    }
    log.info("{}: {} subject(s)", rdfType, count);
    assertNotEquals(0, count);
  }

  private void listSubjectProperties(Model model, String subject) {
    final Query listQuery = SparqlQueryTool.queryToListSubjectProperties(subject, "?p", "?o");
    log.info("Query to list subject properties:\n {}", listQuery);
    int count = 0;
    try (QueryExecution listExecution = QueryExecutionFactory.create(listQuery, model)) {
      ResultSet listResultSet = listExecution.execSelect();
      while (listResultSet.hasNext()) {
        QuerySolution solution = listResultSet.nextSolution();
        count++;
        log.info("{}) {} = {}", count, solution.get("?p"), solution.get("?o"));
      }
    }
    log.info("{}: {} properties", subject, count);
    assertNotEquals(0, count);
  }

  protected Path getDataFile(String filename) {
    final Path file = this.sourceFolder.resolve(filename);
    assertNotNull(file);
    assertTrue(FileTool.checkFile(file));
    return file;
  }

}
