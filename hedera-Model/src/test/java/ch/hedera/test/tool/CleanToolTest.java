/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - CleanToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.tool;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

import ch.hedera.HederaConstants;
import ch.hedera.tool.CleanTool;

class CleanToolTest {

  @Test
  void cleanRorIdTest() {
    assertNull(CleanTool.cleanRorId(null));
    assertEquals("", CleanTool.cleanRorId(""));

    String rorId = "01swzsf04";
    for (String rorPrefix : HederaConstants.getRorPrefixList()) {
      assertEquals(rorId, CleanTool.cleanRorId(rorPrefix + rorId));
    }

    assertEquals(rorId, CleanTool.cleanRorId(rorId));
    assertEquals("any value", CleanTool.cleanRorId("any value"));
    assertEquals("https://unige.ch/" + rorId, CleanTool.cleanRorId("https://unige.ch/" + rorId));
  }
}
