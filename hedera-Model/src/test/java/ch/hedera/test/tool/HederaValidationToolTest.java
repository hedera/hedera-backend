/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - HederaValidationToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.tool;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import ch.hedera.tool.HederaValidationTool;

public class HederaValidationToolTest {

  @ParameterizedTest
  @ValueSource(strings = { "filename.jpg", "filename123.jpg", "fileName.jpg", "file-name.jpg", "file_name.jpg", "file(name).jpg",
          "file.name.jpg", "fi(le.na)me.jpg", "fi-le.name.jpg", "f-i-l-e.name.jpg" })
  void isValidFileNameTest(String fileName) {
    assertTrue(HederaValidationTool.isValidFileName(fileName));
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = { "", "file!name.jpg", "filen$ame.jpg", "filena?me.jpg", "filenamçe.jpg", "filena[me].jpg", "file/name.jpg",
          "  filename.jpg  ", "/filename.jpg", "/test/filename.jpg", "filename.jpg/test" })
  void isInvalidFileNameTest(String fileName) {
    assertFalse(HederaValidationTool.isValidFileName(fileName));
  }

  @ParameterizedTest
  @ValueSource(strings = { "/relative/location", "/relative/location/sub-folder", "/relative/locat(ion)", "/relat.ive/loca.tion",
          "/relative/location/123-test", "/RELATIVE/location/123-test", "/relative/location/123_test", "/relative/location/test_test" })
  void isValidRelativeLocationTest(String relativeLocation) {
    assertTrue(HederaValidationTool.isValidRelativeLocation(relativeLocation));
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = { "", "/relative/loca;tion", "/relative/loca[tion", "/relative/loca!tion", "/relative/loca?tion", "/relative/loca$tion",
          "  /relative/location  " })
  void isInvalidRelativeLocationTest(String relativeLocation) {
    assertFalse(HederaValidationTool.isValidRelativeLocation(relativeLocation));
  }
}
