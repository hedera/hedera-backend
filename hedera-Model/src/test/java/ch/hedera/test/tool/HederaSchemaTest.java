/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - HederaSchemaTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.tool;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.JSONTool;
import ch.unige.solidify.util.XMLTool;

import ch.hedera.adapter.ItemTypeJsonSerializer;
import ch.hedera.adapter.MetadataListJsonSerializer;
import ch.hedera.adapter.ObjectTypeJsonSerializer;
import ch.hedera.model.xml.hedera.v1.researchObject.ItemType;
import ch.hedera.model.xml.hedera.v1.researchObject.Metadata;
import ch.hedera.model.xml.hedera.v1.researchObject.MetadataList;
import ch.hedera.model.xml.hedera.v1.researchObject.ObjectType;
import ch.hedera.model.xml.hedera.v1.researchObject.ResearchDataFile;
import ch.hedera.model.xml.hedera.v1.researchObject.ResearchObject;
import ch.hedera.model.xml.hedera.v1.researchObject.ResearchProject;

class HederaSchemaTest {

  private Path schemaFolder = Paths.get("src", "main", "resources", "schemas").toAbsolutePath();
  private Path xsltFolder = Paths.get("src", "main", "resources", "xslt").toAbsolutePath();
  private Path testFolder = Paths.get("src", "test", "resources").toAbsolutePath();

  @ParameterizedTest
  @ValueSource(strings = { "hedera-repository-1.0.xsd", "hedera-research-object-1.0.xsd" })
  void wellformedTest(String file) {
    final Path filePath = this.getFilePath(this.schemaFolder, file);
    assertDoesNotThrow(() -> XMLTool.wellformed(filePath));
  }

  @ParameterizedTest
  @ValueSource(strings = { "hedera-research-object.xml", "hedera-research-data-file-http.xml", "hedera-research-data-file-https.xml" })
  void validTest(String file) {
    final Path schemaPath = this.getFilePath(this.schemaFolder, "hedera-research-object-1.0.xsd");
    final Path filePath = this.getFilePath(this.testFolder, file);
    assertDoesNotThrow(() -> XMLTool.validate(FileTool.toString(schemaPath), FileTool.toString(filePath)));
  }

  @ParameterizedTest
  @ValueSource(strings = { "hedera-research-object.xml", "hedera-research-data-file-http.xml", "hedera-research-data-file-https.xml" })
  void oaiTest(String file) throws IOException {
    final Path xsltPath = this.getFilePath(this.xsltFolder, "hedera2oai_dc.xsl");
    final Path filePath = this.getFilePath(this.testFolder, file);
    final Path resultPath = XMLTool.getOutputFile(filePath);
    assertDoesNotThrow(() -> XMLTool.transform(filePath, xsltPath));
    assertTrue(FileTool.checkFile(resultPath));
    assertDoesNotThrow(() -> XMLTool.wellformed(resultPath));
    final List<URL> schemaUrls = new ArrayList<>();
    schemaUrls.add(new ClassPathResource("schemas/oai-dc.xsd").getURL());
    schemaUrls.add(new ClassPathResource("schemas/simpledc20021212.xsd").getURL());
    assertDoesNotThrow(() -> XMLTool.validate(schemaUrls, resultPath));
    assertDoesNotThrow(() -> FileTool.deleteFile(resultPath));
  }

  @ParameterizedTest
  @ValueSource(strings = { "hedera-research-object.xml", "hedera-research-data-file-http.xml", "hedera-research-data-file-https.xml" })
  void unmarshalTest(String file) throws JAXBException, JsonProcessingException {
    final Path filePath = this.getFilePath(this.testFolder, file);
    final Unmarshaller unmarshaller = this.getJaxbContext().createUnmarshaller();
    final Marshaller marshaller = this.getJaxbContext().createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    // XML => Object
    Object researchItem = unmarshaller.unmarshal(filePath.toFile());
    if (file.contains("object")) {
      assertTrue(researchItem instanceof ResearchObject);
    } else {
      assertTrue(researchItem instanceof ResearchDataFile);
    }
    // Object => XML
    marshaller.marshal(researchItem, System.out);
    // Object => JSON
    final String json = this.convert2String(researchItem);
    System.out.print(json);
    JSONTool.convertJson2Map(json);
  }

  @Test
  void jsonTest() throws JAXBException, JsonProcessingException {
    final ResearchObject researchObject = this.createResearchObject();
    final Marshaller marshaller = this.getJaxbContext().createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    // Object => XML
    assertDoesNotThrow(() -> marshaller.marshal(researchObject, System.out));
    // Object => JSON
    assertDoesNotThrow(() -> System.out.print(this.convert2String(researchObject)));
  }

  private ResearchObject createResearchObject() {
    final ResearchObject ro = new ResearchObject();
    ro.setHederaId("objectId");
    ro.setItemType(ItemType.RESEARCH_OBJECT);
    ro.setObjectType(ObjectType.OTHER);
    ro.setObjectDate(OffsetDateTime.now());
    ro.setUri("objectUri");
    ro.setResearchProject(this.createResearchProject());
    ro.setMetadataList(new MetadataList());
    Metadata metadata1 = new Metadata();
    metadata1.setKey("w");
    metadata1.setValue("1200");
    ro.getMetadataList().getMetadata().add(metadata1);
    Metadata metadata2 = new Metadata();
    metadata2.setKey("h");
    metadata2.setValue("1000");
    ro.getMetadataList().getMetadata().add(metadata2);
    return ro;
  }

  private ResearchProject createResearchProject() {
    final ResearchProject p = new ResearchProject();
    p.setId("1");
    p.setName("project");
    p.setShortName("project");
    p.setOpeningDate(LocalDate.now());
    p.setClosingDate(LocalDate.now());
    p.getKeywords().add("keyword1");
    p.getKeywords().add("keyword2");
    return p;
  }

  private Path getFilePath(Path parentPath, String filename) {
    final Path file = parentPath.resolve(filename);
    assertNotNull(file);
    assertTrue(FileTool.checkFile(file));
    return file;
  }

  private JAXBContext getJaxbContext() throws JAXBException {
    return JAXBContext.newInstance(ResearchObject.class, ResearchDataFile.class);
  }

  private String convert2String(Object o) throws JsonProcessingException {
    final ObjectMapper mapper = new ObjectMapper();
    SimpleModule metadataModule = new SimpleModule();
    metadataModule.addSerializer(new MetadataListJsonSerializer());
    metadataModule.addSerializer(new ItemTypeJsonSerializer());
    metadataModule.addSerializer(new ObjectTypeJsonSerializer());
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.registerModule(new JavaTimeModule());
    mapper.registerModule(metadataModule);
    return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);
  }
}
