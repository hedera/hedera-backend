/* Nov.2
   Rename of research object to research object type
 */
ALTER TABLE project_research_objects to project_research_object_types;
ALTER TABLE project_research_object_types RENAME COLUMN research_object_id TO reserch_object_type_id;
ALTER TABLE research_objects to research_object_types;
ALTER TABLE ontology_research_objects to ontology_research_object_types;
ALTER TABLE ontology_research_object_types RENAME COLUMN research_object_id TO reserch_object_type_id;

/* Nov, 2023
   add column for accessPublic to project
 */
ALTER TABLE project
    ADD COLUMN `accessPublic` varchar(5) not null;
/*
  Nov 3
  Delete relation many to many between ontology and research object type
 */
DROP TABLE ontology_research_objects;
ALTER TABLE research_object_type
    ADD COLUMN ontology_id VARCHAR(50) DEFAULT NULL;
ALTER TABLE research_object_type
    ADD CONSTRAINT research_object_type_ontology FOREIGN KEY ontology_id REFERENCES (ontology, res_id);
/*
 Nov 13
 Delete tables: ArchiveThumbnail, ArchiveThumbnailLogo
 Add new column iiif_manifest_sparql_query to project table
 */
DROP TABLE archive_thumbnail;
DROP TABLE archive_thumbnail_logo;

ALTER TABLE project
    ADD COLUMN iiif_manifest_sparql_query varchar(4096) null;

/*
  Dec 7
  Add logo column into SourceDataset
 */
ALTER TABLE source_dataset
    ADD column logo_id varchar(255) null;

/*
 Dec 15
 Delete status import column from source dataset
 */
ALTER TABLE source_dataset_file DROP COLUMN status_import;
ALTER TABLE rdf_dataset_file
    ADD column status_import varchar(255) DEFAULT NULL;

/*
  Dec 18
  Change statusImport to status
 */
ALTER TABLE source_dataset_file RENAME column statusImport to status;

/*
 Dec 22
 Delete unique constraint for name
 */
ALTER TABLE source_dataset DROP INDEX `UK_lgfa213m0qxr04vntai986iva`;
ALTER TABLE source_dataset ADD CONSTRAINT UNIQUE KEY `name_projectId` (`name`, `project_id`);
/*
 Jan 12
 Update ontology format
 */
 /* Update existing ontologies format */
UPDATE ontology_name_version set format='RDFXML' where name='CRMdig' and version='3.2.1';
UPDATE ontology_name_version set format='RDFXML' where name='RiC-O' and version='1.0-beta';
UPDATE ontology_name_version set format='RDFXML' where name='CIDOC CRM' and version='5.1.2';
UPDATE ontology_name_version set format='RDFXML' where name='RiC-O' and version='0.2';
UPDATE ontology_name_version set format='RDFXML' where name='CIDOC CRM' and version='7.1.2';
UPDATE ontology_name_version set format='RDFXML' where name='CIDOC CRM' and version='6.2.1';


/*
 Feb 21
 Add enum values for RdfDatasetFile
 */
ALTER TABLE rdf_dataset_file MODIFY COLUMN status_import ENUM('NOT_IMPORTED','TO_IMPORT','IMPORTING','INDEXING','TO_REPLACE','REPLACING','IMPORTED','TO_REIMPORT','IN_ERROR', 'TO_REMOVE', 'REMOVING');

/*
 Apr 17
 Change enum values for download_token
 */
ALTER TABLE download_token MODIFY COLUMN type ENUM('PROJECT');

/*
 Apr 30
 Change enum values for download_token
 */
ALTER TABLE download_token MODIFY COLUMN type ENUM('PROJECT', 'RESEARCH_DATA_FILE', 'IIIF');
