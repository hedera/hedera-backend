<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:hedera="http://www.unige.ch/hedera/v1" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
	exclude-result-prefixes="hedera">

	<xsl:output method="xml" omit-xml-declaration="yes" indent="yes" encoding="UTF-8" />

	<xsl:param name="archiveUrl" />
	<xsl:param name="resId" />

	<xsl:strip-space elements="*" />

	<xsl:template match="hedera:subject">
		<xsl:element name="dc:title">
			<xsl:value-of select="." />
		</xsl:element>
	</xsl:template>

	<xsl:template match="hedera:researchProject">
		<xsl:element name="dc:subject">
			[<xsl:value-of select="@id" />] <xsl:value-of select="hedera:name" /> (<xsl:value-of select="hedera:description" />)
		</xsl:element>
	</xsl:template>

	<xsl:template match="hedera:researchObjectType">
		<xsl:element name="dc:type">
			[<xsl:value-of select="@rdfType" />] <xsl:value-of select="hedera:name" /> (<xsl:value-of select="hedera:description" />) from <xsl:value-of select="hedera:ontology/hedera:name" /> ontology (<xsl:value-of select="hedera:ontology/hedera:version" />)
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="hedera:metadataList">
		<xsl:for-each select="hedera:metadata">
			<xsl:element name="dc:description">
				<xsl:value-of select="@key" /> = <xsl:value-of select="." />
			</xsl:element>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="hedera:fileName">
		<xsl:element name="dc:description">
			<xsl:value-of select="../hedera:relativeLocation" /><xsl:value-of select="." />[<xsl:value-of select="../hedera:accessibleFrom" />]
		</xsl:element>
	</xsl:template>

	<xsl:template match="hedera:ResearchObject|hedera:ResearchDataFile">
		<xsl:element name="oai_dc:dc">
			<xsl:attribute name="xsi:schemaLocation">http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd</xsl:attribute>
			<xsl:namespace name="dc">http://purl.org/dc/elements/1.1/</xsl:namespace>
			<dc:identifier><xsl:value-of select="@hederaId" /></dc:identifier>
			<dc:identifier><xsl:value-of select="$archiveUrl" /><xsl:value-of select="$resId" /></dc:identifier>
			
			<dc:date><xsl:value-of select="@objectDate" /></dc:date>
			<dc:type><xsl:value-of select="@itemType" /></dc:type>
			<xsl:choose>
				<xsl:when test="@mimeType">
					<dc:format><xsl:value-of select="@mimeType" /></dc:format>
				</xsl:when>
				<xsl:when test="@fileSize">
					<dc:format>size=<xsl:value-of select="@fileSize" /></dc:format>
				</xsl:when>
				<xsl:when test="@sourceId">
					<dc:format>source<xsl:value-of select="@sourceId" /></dc:format>
				</xsl:when>
			</xsl:choose>
			<xsl:apply-templates select="hedera:subject" />
			<xsl:apply-templates select="hedera:metadataList" />
			<xsl:apply-templates select="hedera:fileName" />
			<xsl:apply-templates select="hedera:researchProject" />
			<xsl:apply-templates select="hedera:researchObjectType" />
		</xsl:element>
	</xsl:template>

	<xsl:template name="node_to_string">
		<xsl:param name="delimiter" select="' '" />
		<xsl:for-each select="descendant::text()">
			<xsl:value-of select="string( . )" />
			<xsl:if test="position() != last()">
				<xsl:value-of select="$delimiter" />
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>
