/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - RorInfo.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RorInfo {

  public static class Country {
    @JsonProperty("country_code")
    private String code;
    @JsonProperty("country_name")
    private String name;

    public String getCode() {
      return this.code;
    }

    public void setCode(String code) {
      this.code = code;
    }

    public String getName() {
      return this.name;
    }

    public void setName(String name) {
      this.name = name;
    }
  }

  public static class Identifier {
    @JsonProperty("FundRef")
    private IdentifierDetail crossrefFunder;
    @JsonProperty("GRID")
    private IdentifierSimple grid;
    @JsonProperty("ISNI")
    private IdentifierDetail isni;
    @JsonProperty("Wikidata")
    private IdentifierDetail wikidata;

    public IdentifierDetail getCrossrefFunder() {
      return this.crossrefFunder;
    }

    public void setCrossrefFunder(IdentifierDetail crossrefFunder) {
      this.crossrefFunder = crossrefFunder;
    }

    public IdentifierSimple getGrid() {
      return this.grid;
    }

    public void setGrid(IdentifierSimple grid) {
      this.grid = grid;
    }

    public IdentifierDetail getIsni() {
      return this.isni;
    }

    public void setIsni(IdentifierDetail isni) {
      this.isni = isni;
    }

    public IdentifierDetail getWikidata() {
      return this.wikidata;
    }

    public void setWikidata(IdentifierDetail wikidata) {
      this.wikidata = wikidata;
    }

  }

  public static class IdentifierDetail {
    private String[] all;
    private String preferred;

    public String[] getAll() {
      return this.all;
    }

    public void setAll(String[] all) {
      this.all = all;
    }

    public String getPreferred() {
      return this.preferred;
    }

    public void setPreferred(String preferred) {
      this.preferred = preferred;
    }

  }

  public static class IdentifierSimple {
    private String all;
    private String preferred;

    public String getAll() {
      return this.all;
    }

    public void setAll(String all) {
      this.all = all;
    }

    public String getPreferred() {
      return this.preferred;
    }

    public void setPreferred(String preferred) {
      this.preferred = preferred;
    }

  }

  private String id;
  private String name;
  private String[] acronyms;
  private Country country;

  @JsonProperty("links")
  private String[] webSites;

  @JsonProperty("external_ids")
  private Identifier externalIds;

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String[] getAcronyms() {
    return this.acronyms;
  }

  public void setAcronyms(String[] acronyms) {
    this.acronyms = acronyms;
  }

  public Country getCountry() {
    return this.country;
  }

  public void setCountry(Country country) {
    this.country = country;
  }

  public Identifier getExternalIds() {
    return this.externalIds;
  }

  public void setExternalIds(Identifier externalIds) {
    this.externalIds = externalIds;
  }

  public String[] getWebSites() {
    return this.webSites;
  }

  public void setWebSites(String[] webSites) {
    this.webSites = webSites;
  }

}
