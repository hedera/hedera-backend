/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - Rml.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.humanities;

import java.util.List;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.hedera.HederaConstants;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;

@Schema(description = "RDF Mapping language is a mapping language defined to express customized mapping rules from data structures and serializations to the RDF")
@Entity
public class Rml extends ResourceNormalized implements ResourceFileInterface {
  // Name, description, version, source format, rdf file

  @Schema(description = "The name of the RML.")
  @NotNull
  @Size(min = 1)
  private String name;

  @Schema(description = "The version of the RML.")
  @NotNull
  @Size(min = 1)
  private String version;

  @Schema(description = "The description of the RML.")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @Schema(description = "The format of the RML.")
  @Enumerated(EnumType.STRING)
  @NotNull
  private RmlFormat format;

  @Schema(description = "The list of projects.")
  @ManyToMany(mappedBy = "rmls")
  @JsonIgnore
  private List<Project> projects;

  @Schema(description = "The RDF File (turtle).")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = HederaConstants.DB_RDF_FILE_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private RdfFile rdfFile;

  public String getName() {
    return this.name;
  }

  public String getVersion() {
    return this.version;
  }

  public String getDescription() {
    return this.description;
  }

  public RmlFormat getFormat() {
    return this.format;
  }

  public List<Project> getProjects() {
    return this.projects;
  }

  public RdfFile getRdfFile() {
    return this.rdfFile;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setFormat(RmlFormat format) {
    this.format = format;
  }

  public void setRdfFile(RdfFile rdfFile) {
    this.rdfFile = rdfFile;
  }

  public boolean addProject(Project project) {
    final boolean result = this.projects.add(project);
    if (!project.getRmls().contains(this)) {
      project.addRml(this);
    }
    return result;
  }

  public boolean removeProject(Project project) {
    final boolean result = this.projects.remove(project);
    if (project.getRmls().contains(this)) {
      project.removeRml(this);
    }
    return result;
  }

  @Override
  public void init() {

  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  @Override
  @JsonIgnore
  public ResourceFile setNewResourceFile() {
    this.setRdfFile(new RdfFile());
    return this.getRdfFile();
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getRdfFile();
  }

  @Override
  public void setResourceFile(ResourceFile resourceFile) {
    this.setRdfFile((RdfFile) resourceFile);
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.UPLOAD_RDF).withRel(HederaActionName.UPLOAD_RDF));
      this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.DOWNLOAD_RDF).withRel(HederaActionName.DOWNLOAD_RDF));
    }
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.name, this.description, this.version, this.format, this.rdfFile);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    Rml other = (Rml) obj;
    return Objects.equals(this.name, other.name) && Objects.equals(this.description, other.description) && Objects.equals(this.version,
            other.version) && Objects.equals(this.format, other.format) && Objects.equals(this.rdfFile, other.rdfFile);
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("RML [resId=").append(this.getResId())
            .append(", name=").append(this.name)
            .append(", description=").append(this.description)
            .append(", format=").append(this.format)
            .append(", version=").append(this.version).append("]");
    return builder.toString();
  }
}
