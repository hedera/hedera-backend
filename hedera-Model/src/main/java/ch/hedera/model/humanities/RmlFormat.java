/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - RmlFormat.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.humanities;

import ch.hedera.HederaConstants;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "RML's format.")
public enum RmlFormat {

  //@formatter:off
  CSV(HederaConstants.CSV),
  XML(HederaConstants.XML),
  JSON(HederaConstants.JSON),
  SQL(HederaConstants.SQL);
  //@formatter:on

  @Schema(description = "Name of the format of the RML.")
  private final String name;

  RmlFormat(String name) {
    this.name = name;
  }
  public String getName() { return this.name; }
}
