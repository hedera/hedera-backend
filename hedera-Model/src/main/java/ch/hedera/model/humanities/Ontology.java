/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - Ontology.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.humanities;

import java.net.URI;
import java.net.URL;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.hedera.HederaConstants;
import ch.hedera.model.RdfFormat;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Schema(description = "An ontology is a set of concepts and categories in a subject area that shows their properties and the relations between them.")
@Entity
@Table(uniqueConstraints = @UniqueConstraint(name = "ontology_name_version", columnNames = { "name", "version" }))
public class Ontology extends ResourceNormalized implements ResourceFileInterface {

  @Schema(description = "The name of the ontology.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String name;

  @Schema(description = "The version of the ontology.")
  @NotNull
  @Size(min = 1, max = 50)
  private String version;

  @Schema(description = "The description of the ontology.")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @Schema(description = "The URL of the ontology.")
  private URL url;

  @Schema(description = "If the ontology is defined externaly.")
  private Boolean external;

  @Schema(description = "The format of the ontology.")
  @Enumerated(EnumType.STRING)
  @NotNull
  private RdfFormat format;

  @Schema(description = "The base URI of the ontology.")
  @NotNull
  private URI baseUri;

  @Schema(description = "The Ontology File (turtle).")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = HederaConstants.DB_RDF_FILE_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private RdfFile ontologyFile;

  public String getName() {
    return this.name;
  }

  public String getVersion() {
    return this.version;
  }

  public String getDescription() {
    return this.description;
  }

  public URL getUrl() {
    return this.url;
  }

  public RdfFormat getFormat() {
    return this.format;
  }

  public RdfFile getOntologyFile() {
    return this.ontologyFile;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  public void setFormat(RdfFormat format) {
    this.format = format;
  }

  public void setOntologyFile(RdfFile ontologyFile) {
    this.ontologyFile = ontologyFile;
  }

  public URI getBaseUri() {
    return this.baseUri;
  }

  public Boolean getExternal() {
    return this.external;
  }

  public void setExternal(Boolean external) {
    this.external = external;
  }

  @Schema(description = "The base URI context of the ontology.")
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public String getBaseUriContext() {
    if (this.baseUri == null) {
      return null;
    }
    int startIndex = "http://".length();
    if (this.baseUri.toString().startsWith("https")) {
      startIndex++;
    }
    return this.baseUri.toString().substring(startIndex);
  }

  public void setBaseUri(URI baseUri) {
    this.baseUri = baseUri;
  }

  @Override
  public void init() {
    if (this.external == null) {
      this.external = true;
    }
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result
            + Objects.hash(this.baseUri, this.description, this.external, this.format, this.name, this.ontologyFile, this.url, this.version);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    Ontology other = (Ontology) obj;
    return Objects.equals(this.baseUri, other.baseUri) && Objects.equals(this.description, other.description)
            && Objects.equals(this.external, other.external)
            && this.format == other.format && Objects.equals(this.name, other.name) && Objects.equals(this.ontologyFile, other.ontologyFile)
            && Objects.equals(this.url, other.url) && Objects.equals(this.version, other.version);
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("Ontology [resId=").append(this.getResId())
            .append(", name=").append(this.name)
            .append(", version=").append(this.version)
            .append(", baseURI=").append(this.baseUri)
            .append(", description=").append(this.description)
            .append(", url=").append(this.url)
            .append(", format=").append(this.format)
            .append("]");
    return builder.toString();
  }

  @Override
  @JsonIgnore
  public ResourceFile setNewResourceFile() {
    this.setOntologyFile(new RdfFile());
    return this.getOntologyFile();
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getOntologyFile();
  }

  @Override
  public void setResourceFile(ResourceFile resourceFile) {
    this.setOntologyFile((RdfFile) resourceFile);
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.RDF_CLASS).withRel(ResourceName.RDF_CLASS));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.RDF_PROPERTY).withRel(ResourceName.RDF_PROPERTY));
      this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.IS_PART_OF).slash("name-of-rdf-type-to-check")
              .withRel(HederaActionName.IS_PART_OF));
      this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.UPLOAD_ONTOLOGY).withRel(HederaActionName.UPLOAD_ONTOLOGY));
      this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.DOWNLOAD_ONTOLOGY).withRel(HederaActionName.DOWNLOAD_ONTOLOGY));
    }
  }
}
