/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - SparqlResultRow.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.triplestore;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a row in SPARQL query results.
 */
public class SparqlResultRow implements Serializable {
  private static final long serialVersionUID = -5654444389893686512L;

  Map<String, SparqlResultValue> properties = new HashMap<>();

  public void addRdfNode(String propertyName, SparqlResultValue rdfNode) {
    this.properties.put(propertyName, rdfNode);
  }

  public Map<String, SparqlResultValue> getProperties() {
    return this.properties;
  }

  public void setProperties(Map<String, SparqlResultValue> properties) {
    this.properties = properties;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("DatasetEntry [");
    for (Map.Entry<String, SparqlResultValue> entry : this.properties.entrySet()) {
      SparqlResultValue property = entry.getValue();
      sb.append(entry.getKey());
      String value = property.getValue();
      switch (property.getNodeType()) {
        case URI -> sb.append("<" + value + ">");
        default -> sb.append(value);
      }
    }
    sb.append("]");
    return sb.toString();
  }
}
