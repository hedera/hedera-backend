/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - DatasetSettings.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.triplestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.rest.NoSqlResource;
import ch.unige.solidify.util.JSONTool;

public class DatasetSettings extends NoSqlResource {
  protected Map<String, Object> settings = new HashMap<>();

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof DatasetSettings castOther)) {
      return false;
    }
    return Objects.equals(this.settings, castOther.settings);
  }

  public Map<String, Object> getSettings() {
    return this.settings;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.settings);
  }

  public void setSettings(Map<String, Object> settings) {
    this.settings = settings;
  }

  @JsonIgnore
  public void setSettings(String settings) {
    this.settings = JSONTool.convertJson2Map(settings);
  }

  @Override
  public boolean update(NoSqlResource item) {
    return false;
  }
}
