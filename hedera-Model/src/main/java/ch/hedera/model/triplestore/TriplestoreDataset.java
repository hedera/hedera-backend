/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - TriplestoreDataset.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.triplestore;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.rest.ResourceNormalized;

import ch.hedera.HederaConstants;
import ch.hedera.model.Access;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.ModuleName;

@Schema(description = "A representation of the dataset stored in the triplestore")
@Entity
public class TriplestoreDataset extends ResourceNormalized {

  @Schema(description = "The name of the dataset.")
  @NotNull
  @Column(unique = true)
  @Size(min = 1)
  private String name;

  @Schema(description = "The description of the triplestore dataset.")
  @Size(max = HederaConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @NotNull
  @ManyToOne
  @JoinColumn(name = HederaConstants.DB_PROJECT_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  @Schema(description = "The project to which the triplestore dataset belongs.")
  private Project project;

  @Schema(description = "The access level of the dataset.")
  @Enumerated(EnumType.STRING)
  private Access access;

  @Override
  public void init() {
    // Do nothing
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Project getProject() {
    return this.project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public Access getAccess() {
    return this.access;
  }

  public void setAccess(Access access) {
    this.access = access;
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    TriplestoreDataset that = (TriplestoreDataset) o;
    return Objects.equals(this.name, that.name) && Objects.equals(this.description, that.description) && Objects.equals(
            this.project, that.project) && this.access == that.access;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.name, this.description, this.project, this.access);
  }
}
