/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - SparqlResultValue.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.triplestore;

import java.io.Serializable;

/**
 * Represents an element in a row of SPARQL query results
 */
public class SparqlResultValue implements Serializable {
  private static final long serialVersionUID = -137316595288616484L;

  public enum NodeType {
    URI, LITERAL, BLANK, ANONYMOUS
  }

  private NodeType nodeType;
  private String value;

  public SparqlResultValue() {
  }

  public SparqlResultValue(NodeType nodeType, String value) {
    this.nodeType = nodeType;
    this.value = value;
  }

  public NodeType getNodeType() {
    return this.nodeType;
  }

  public void setNodeType(NodeType nodeType) {
    this.nodeType = nodeType;
  }

  public String getValue() {
    return this.value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
