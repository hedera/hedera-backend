/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ResearchObjectMetadata.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.index;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.model.oai.OAIRecord;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.util.JSONTool;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.ProjectAwareResource;
import ch.hedera.model.xml.hedera.v1.researchObject.ItemType;
import ch.hedera.model.xml.hedera.v1.researchObject.ObjectType;

@JsonDeserialize(using = ResearchObjectMetadataDeserializer.class)
public class ResearchObjectMetadata extends IndexMetadata implements ProjectAwareResource, OAIRecord {

  public ResearchObjectMetadata() {
  }

  public ResearchObjectMetadata(String indexName, String id, String jsonObject) {
    this.setResId(id);
    this.setIndex(indexName);
    this.setMetadata(JSONTool.convertJson2Map(jsonObject));
  }

  public void addLinksForAccess(WebMvcLinkBuilder linkBuilder) {
    if (ItemType.RESEARCH_DATA_FILE == this.getItemType()) {
      this.add(linkBuilder.slash(this.getHederaId()).slash(ActionName.DOWNLOAD).withRel(ActionName.DOWNLOAD));
    }
    super.addLinks(linkBuilder);
  }

  @JsonIgnore
  public ItemType getItemType() {
    final String itemType = (String) this.getMetadata(HederaConstants.ITEM_TYPE_INDEX_FIELD);
    if (itemType == null) {
      return null;
    }
    return ItemType.fromValue(itemType);
  }

  @JsonIgnore
  public ObjectType getObjectType() {
    final String objectType = (String) this.getMetadata(HederaConstants.OBJECT_TYPE_INDEX_FIELD);
    if (objectType == null) {
      return null;
    }
    return ObjectType.fromValue(objectType);
  }

  @Override
  @JsonIgnore
  public OffsetDateTime getCreationDate() {
    final String creation = (String) this.getMetadata(HederaConstants.OBJECT_DATE_INDEX_FIELD);
    return OffsetDateTime.parse(creation).toInstant().atOffset(ZoneOffset.UTC).withNano(0);
  }

  @Override
  @JsonIgnore
  public String getOAIMetadata() {
    return (String) this.getMetadata(HederaConstants.RESEARCH_OBJET_METADATA_INDEX_FIELD, HederaConstants.RESEARCH_OBJET_XML_INDEX_FIELD);
  }

  @JsonIgnore
  public String getAccessibleFrom() {
    return (String) this.getMetadata(HederaConstants.ACCESSIBLE_FROM_FIELD);
  }

  @Override
  @JsonIgnore
  public String getOaiId() {
    return this.getResId();
  }

  @Override
  @JsonIgnore
  public String getProjectId() {
    return (String) this.getMetadata(HederaConstants.PROJECT_ID_INDEX_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP));
  }

  @JsonIgnore
  public String getHederaId() {
    return (String) this.getMetadata(HederaConstants.INDEX_FIELD_HEDERA_ID);
  }

  @JsonIgnore
  public String getFilename() {
    return (String) this.getMetadata(HederaConstants.INDEX_FIELD_FILENAME);
  }

  @JsonIgnore
  public String getRelativeLocation() {
    return (String) this.getMetadata(HederaConstants.INDEX_FIELD_RELATIVE_LOCATION);
  }

  @JsonIgnore
  public String getMimeType() {
    return (String) this.getMetadata(HederaConstants.INDEX_FIELD_MIME_TYPE);
  }

  @JsonIgnore
  public Integer getFileSize() {
    return (Integer) this.getMetadata(HederaConstants.INDEX_FIELD_FILE_SIZE);
  }

  @JsonIgnore
  public String getFullFileName() {
    if (StringTool.isNullOrEmpty(this.getRelativeLocation()) || this.getRelativeLocation().equals("/")) {
      return "/" + this.getFilename();
    }
    return this.getRelativeLocation() + "/" + this.getFilename();
  }

}
