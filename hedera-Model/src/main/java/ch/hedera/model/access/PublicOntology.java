/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - PublicOntology.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.access;

import java.net.URL;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.model.ChangeInfo;
import ch.unige.solidify.rest.NoSqlResource;

import ch.hedera.model.humanities.Ontology;

@Schema(description = "An ontology is a set of concepts and categories in a subject area that shows their properties and the relations between them.")
public class PublicOntology extends NoSqlResource {

  // TODO: Support JSON deserialization
  @JsonIgnore
  private ChangeInfo creation;

  // TODO: Support JSON deserialization
  @JsonIgnore
  private ChangeInfo lastUpdate;

  @Schema(description = "The name of the ontology.")
  private String name;

  @Schema(description = "The version of the ontology.")
  private String version;

  @Schema(description = "The description of the ontology.")
  private String description;

  @Schema(description = "The URL of the ontology.")
  private URL url;

  @Schema(description = "If the ontology is defined externaly.")
  private Boolean external;

  public PublicOntology() {
  }

  public PublicOntology(Ontology onto) {
    this.copyOntology(onto);
  }

  public ChangeInfo getCreation() {
    return this.creation;
  }

  public String getDescription() {
    return this.description;
  }

  public ChangeInfo getLastUpdate() {
    return this.lastUpdate;
  }

  public String getName() {
    return this.name;
  }

  public URL getUrl() {
    return this.url;
  }

  public void setCreation(ChangeInfo creation) {
    this.creation = creation;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setLastUpdate(ChangeInfo lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getVersion() {
    return this.version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  public Boolean getExternal() {
    return this.external;
  }

  public void setExternal(Boolean external) {
    this.external = external;
  }

  @Override
  public boolean update(NoSqlResource item) {
    throw new UnsupportedOperationException();
  }

  private void copyOntology(Ontology onto) {
    this.setResId(onto.getResId());
    this.setCreation(onto.getCreation());
    this.setLastUpdate(onto.getLastUpdate());
    this.setName(onto.getName());
    this.setVersion(onto.getVersion());
    this.setDescription(onto.getDescription());
    this.setUrl(onto.getUrl());
    this.setExternal(onto.getExternal());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.creation, this.description, this.lastUpdate, this.name, this.url, this.version);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    PublicOntology other = (PublicOntology) obj;
    return Objects.equals(this.creation, other.creation) && Objects.equals(this.description, other.description)
            && Objects.equals(this.lastUpdate, other.lastUpdate) && Objects.equals(this.name, other.name) && Objects.equals(this.url, other.url)
            && Objects.equals(this.version, other.version);
  }

}
