/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - PublicProject.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.access;

import java.net.URL;
import java.time.LocalDate;
import java.util.Objects;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

import ch.unige.solidify.model.ChangeInfo;
import ch.unige.solidify.rest.NoSqlResource;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ProjectLogo;
import ch.hedera.rest.HederaActionName;

@Schema(description = "A project represents a research project which contains metadata (source datasets) and files (research data files).")
public class PublicProject extends NoSqlResource {

  // TODO: Support JSON deserialization
  @JsonIgnore
  private ChangeInfo creation;

  // TODO: Support JSON deserialization
  @JsonIgnore
  private ChangeInfo lastUpdate;

  @Schema(description = "The name of the project.")
  private String name;

  @Schema(description = "The short name of the project (folder for file storage, bucket for object storage & triplestore dataset).")
  private String shortName;

  @Schema(description = "The description of the project.")
  private String description;

  @Schema(description = "The URL of the project.")
  private URL url;

  @Schema(description = "The opening date of the project.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_FORMAT)
  private LocalDate openingDate;

  @Schema(description = "The closing date of the project.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_FORMAT)
  private LocalDate closingDate;

  @Schema(description = "The logo of the project.")
  private ProjectLogo logo;

  @Schema(description = "If the content of the project is public or private.")
  private Boolean accessPublic;

  @Schema(description = "The file number of the project (research data files)")
  private Long fileNumber;

  @Schema(description = "The triple number of the project (source datasets)")
  private Long tripleNumber;

  @Schema(description = "The total size of the project ")
  private Long totalSize;

  public PublicProject() {
  }

  public PublicProject(Project project) {
    this.copyProject(project);
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder) {
    super.addLinks(linkBuilder);
    this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.DOWNLOAD_LOGO).withRel(HederaActionName.DOWNLOAD_LOGO));
  }

  public ChangeInfo getCreation() {
    return this.creation;
  }

  public String getDescription() {
    return this.description;
  }

  public ChangeInfo getLastUpdate() {
    return this.lastUpdate;
  }

  public String getName() {
    return this.name;
  }

  public URL getUrl() {
    return this.url;
  }

  public String getShortName() {
    return this.shortName;
  }

  public void setCreation(ChangeInfo creation) {
    this.creation = creation;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setLastUpdate(ChangeInfo lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  public void setShortName(String shortName) {
    this.shortName = shortName;
  }

  public LocalDate getOpeningDate() {
    return this.openingDate;
  }

  public void setOpeningDate(LocalDate openingDate) {
    this.openingDate = openingDate;
  }

  public LocalDate getClosingDate() {
    return this.closingDate;
  }

  public void setClosingDate(LocalDate closingDate) {
    this.closingDate = closingDate;
  }

  public ProjectLogo getLogo() {
    return this.logo;
  }

  public void setLogo(ProjectLogo logo) {
    this.logo = logo;
  }

  public Boolean getAccessPublic() {
    return this.accessPublic;
  }

  public void setAccessPublic(Boolean accessPublic) {
    this.accessPublic = accessPublic;
  }

  public Long getFileNumber() {
    return this.fileNumber;
  }

  public void setFileNumber(Long fileNumber) {
    this.fileNumber = fileNumber;
  }

  public Long getTripleNumber() {
    return this.tripleNumber;
  }

  public void setTripleNumber(Long tripleNumber) {
    this.tripleNumber = tripleNumber;
  }

  public Long getTotalSize() {
    return this.totalSize;
  }

  public void setTotalSize(Long totalSize) {
    this.totalSize = totalSize;
  }

  @Schema(description = "The total size in human-readable format of the project.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = Access.READ_ONLY)
  public String getSmartTotalSize() {
    if (this.totalSize == null) {
      return HederaConstants.NO_SIZE;
    }
    return StringTool.formatSmartSize(this.totalSize);
  }

  @Override
  public boolean update(NoSqlResource item) {
    throw new UnsupportedOperationException();
  }

  private void copyProject(Project project) {
    this.setResId(project.getResId());
    this.setCreation(project.getCreation());
    this.setLastUpdate(project.getLastUpdate());
    this.setName(project.getName());
    this.setDescription(project.getDescription());
    this.setUrl(project.getUrl());
    this.setShortName(project.getShortName());
    this.setOpeningDate(project.getOpeningDate());
    this.setClosingDate(project.getClosingDate());
    this.setLogo(project.getLogo());
    this.setAccessPublic(project.isAccessPublic());
    this.setFileNumber(project.getFileNumber());
    this.setTripleNumber(project.getTripleNumber());
    this.setTotalSize(project.getTotalSize());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result
            + Objects.hash(this.accessPublic, this.closingDate, this.description, this.fileNumber, this.logo, this.name, this.openingDate,
                    this.shortName, this.tripleNumber, this.url);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    PublicProject other = (PublicProject) obj;
    return Objects.equals(this.accessPublic, other.accessPublic) && Objects.equals(this.closingDate, other.closingDate)
            && Objects.equals(this.description, other.description) && Objects.equals(this.fileNumber, other.fileNumber)
            && Objects.equals(this.logo, other.logo)
            && Objects.equals(this.name, other.name) && Objects.equals(this.openingDate, other.openingDate)
            && Objects.equals(this.shortName, other.shortName)
            && Objects.equals(this.tripleNumber, other.tripleNumber) && Objects.equals(this.url, other.url);
  }

}
