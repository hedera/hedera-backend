/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IIIFCollectionEntry.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.access;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.hedera.HederaConstants;
import ch.hedera.model.iiif.IIIFCollection;
import ch.hedera.rest.ModuleName;

@Entity
@Table(name = "iiifCollection", uniqueConstraints = {
        @UniqueConstraint(name = "iiif_collection_project_name", columnNames = { HederaConstants.DB_PROJECT_SHORT_NAME,
                HederaConstants.DB_NAME }) })
@Schema(description = "The IIIF Collection items")
public class IIIFCollectionEntry extends ResourceNormalized implements IIIFEntryInterface<IIIFCollection> {

  @Schema(description = "The project short name of the IIIF collection.")
  @NotNull
  @Size(min = 3, max = 63)
  private String projectShortName;

  @Schema(description = "The name of the IIIF collection.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_LARGE_STRING_LENGTH)
  private String name;

  @Schema(description = "The identifier of the IIIF collection.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String id;

  @Schema(description = "The URL of the IIIF collection.")
  @NotNull
  private String url;

  @Schema(description = "The item number of the IIIF collection.")
  private Long itemNumber;

  @Schema(description = "The content of the IIIF collection.")
  @NotNull
  @Column(columnDefinition = "LONGTEXT")
  private IIIFCollection content;

  @Override
  public void init() {
    if (this.itemNumber == null) {
      this.itemNumber = 0L;
    }
  }

  @Override
  public String managedBy() {
    return ModuleName.ACCESS;
  }

  @Override
  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  @JsonIgnore
  public String getProjectShortName() {
    return this.projectShortName;
  }

  @Override
  public void setProjectShortName(String projectShortName) {
    this.projectShortName = projectShortName;
  }

  @Override
  @JsonIgnore
  public IIIFCollection getContent() {
    return this.content;
  }

  public void setContent(IIIFCollection collection) {
    this.content = collection;
  }

  @Override
  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Long getItemNumber() {
    return this.itemNumber;
  }

  public void setItemNumber(Long itemNumber) {
    this.itemNumber = itemNumber;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    IIIFCollectionEntry other = (IIIFCollectionEntry) obj;
    return Objects.equals(this.id, other.id) && Objects.equals(this.itemNumber, other.itemNumber) && Objects.equals(this.name, other.name)
            && Objects.equals(this.projectShortName, other.projectShortName) && Objects.equals(this.url, other.url);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.id, this.itemNumber, this.name, this.projectShortName, this.url);
    return result;
  }

  @Override
  public String toString() {
    return "IIIFCollectionEntry [resId=" + this.getResId() + ", " +
            "projectShortName=" + this.getProjectShortName() + ", " +
            "name=" + this.getName() + ", " +
            "id=" + this.getId() + ", " +
            "url=" + this.getUrl() + ", " +
            "itemNumber=" + this.getItemNumber() + "]";
  }
}
