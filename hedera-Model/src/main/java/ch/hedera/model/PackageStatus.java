/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - PackageStatus.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = """
        OAIS Package Status:
        - CHECKED => Checked package during archiving process
        - CHECKING => A package verification is in progress during checking process
        - CHECK_PENDING => A package verification is pending during checking process
        - CLEANED => Cleaned package during cleaning process for SIP only
        - CLEANING => A package clean is in progress during cleaning process for SIP only
        - COMPLETED => Completed package
        - DISPOSABLE => The Package is candidate for disposal process for AIP only
        - DISPOSAL_APPROVED => Disposal approval done during disposal process for AIP only
        - DISPOSAL_APPROVED_BY_ORGUNIT => Disposal org. unit approval done during disposal process for AIP only
        - DISPOSED => Disposed package for AIP only
        - DOWNLOADING => A package download is in progress
        - EDITING_METADATA => A package metadata edition is in progress
        - INDEXING => A package indexing is in progress
        - IN_ERROR => Package in error during archiving process
        - IN_PREPARATION => Package in preparation during archiving process
        - IN_PROGRESS => A package archiving process is in progress
        - PRESERVATION_ERROR => Package in error during checking process
        - READY => Package Ready
        - REINDEXING => A package re-indexing is in progress
        - RELOADED => Reloaded package from storage location
        - REPLICATING_PACKAGE => A package replication is in progress
        - REPLICATING_TOMBSTONE => A tombstone package replication is in progress
        - RESUBMITTING => A package re-submission is in progress
        - STORED => Package stored on storage location
        - UPDATING_RETENTION => A package retention update is in progress during disposal process
        """)
public enum PackageStatus {
  CHECKED, CHECKING, CLEANED, CLEANING, COMPLETED, DISPOSABLE, DISPOSAL_APPROVED, DISPOSED, DOWNLOADING, IN_ERROR, EDITING_METADATA, IN_PREPARATION, IN_PROGRESS, INDEXING, PRESERVATION_ERROR, READY, REINDEXING, RELOADED, REPLICATING_TOMBSTONE, REPLICATING_PACKAGE, RESUBMITTING, STORED, UPDATING_RETENTION;

  public static boolean isDisposalProcess(PackageStatus status) {
    return (status.equals(DISPOSABLE)
            || status.equals(DISPOSAL_APPROVED)
            || status.equals(DISPOSED));
  }

  public static boolean isCleaningProcess(PackageStatus status) {
    return (status.equals(CLEANING)
            || status.equals(CLEANED));
  }

  public static boolean isReindexingProcess(PackageStatus status) {
    return (status.equals(REINDEXING));
  }

  public static boolean isMetadataEditionProcess(PackageStatus status) {
    return (status.equals(EDITING_METADATA));
  }

  public static boolean isCompletedProcess(PackageStatus status) {
    return (status == PackageStatus.COMPLETED
            || status == PackageStatus.CLEANED
            || status == PackageStatus.DISPOSED);
  }

  public static boolean isInError(PackageStatus status) {
    return (status == PackageStatus.IN_ERROR
            || status == PackageStatus.PRESERVATION_ERROR);
  }

  public static boolean isInProgress(PackageStatus status) {
    return (!isInError(status) && !isCompletedProcess(status));
  }

}
