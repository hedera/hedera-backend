/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ResearchObjectType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.settings;

import java.util.List;
import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.hedera.HederaConstants;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.rest.ModuleName;

@Schema(description = "A research object delineates a range of academic, administrative, and patrimonial disciplines.\nEach of these disciplines may follow its own classification system, reflecting their unique attributes and contexts (for example: SNF Disciplines, http://www.snf.ch/SiteCollectionDocuments/allg_disziplinenliste.pdf, or re3data Subjects, https://www.re3data.org/browse/by-subject/, for academic disciplines). A research object could be associated with a specific project.")
@Entity
@Table(uniqueConstraints = @UniqueConstraint(name = "ontology_name", columnNames = { "name", HederaConstants.DB_ONTOLOGY_ID }))
public class ResearchObjectType extends ResourceNormalized {

  @Schema(description = "The name of the research object.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_ID_LENGTH)
  @Pattern(regexp = HederaConstants.RDF_NAME_REGEX)
  private String name;

  @Schema(description = "The RDF type from the ontology of the research object.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_ID_LENGTH)
  private String rdfType;

  @Schema(description = "The description of the research object.")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @Schema(description = "SPARQL query for list.")
  @Size(max = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String sparqlListQuery;

  @Schema(description = "SPARQL query for details.")
  @Size(max = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String sparqlDetailQuery;

  @Schema(description = "The list of projects.")
  @ManyToMany(mappedBy = "researchObjectTypes")
  @JsonIgnore
  private List<Project> projects;

  @ManyToOne(targetEntity = Ontology.class)
  @NotNull
  @JoinColumn(name = HederaConstants.DB_ONTOLOGY_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private Ontology ontology;

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getRdfType() {
    return this.rdfType;
  }

  public void setRdfType(String rdfType) {
    this.rdfType = rdfType;
  }

  @JsonProperty(access = Access.READ_ONLY)
  public String getRdfTypeUri() {
    if (this.ontology != null && this.ontology.getBaseUri() != null) {
      return this.ontology.getBaseUri() + this.rdfType;
    }
    return null;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<Project> getProjects() {
    return this.projects;
  }

  public void setProjects(List<Project> projects) {
    this.projects = projects;
  }

  public String getSparqlListQuery() {
    return this.sparqlListQuery;
  }

  public void setSparqlListQuery(String sparqlListQuery) {
    this.sparqlListQuery = sparqlListQuery;
  }

  public String getSparqlDetailQuery() {
    return this.sparqlDetailQuery;
  }

  public void setSparqlDetailQuery(String sparqlDetailQuery) {
    this.sparqlDetailQuery = sparqlDetailQuery;
  }

  public Ontology getOntology() {
    return this.ontology;
  }

  public void setOntology(Ontology ontologies) {
    this.ontology = ontologies;
  }

  public boolean addProject(Project project) {
    boolean result = this.projects.add(project);
    if (result && !project.getResearchObjectTypes().contains(this)) {
      result = project.addResearchObjectType(this);
    }
    return result;
  }

  public boolean removeProject(Project project) {
    boolean result = true;
    if (this.projects.contains(project)) {
      result = this.projects.remove(project);
      if (result && project.getResearchObjectTypes().contains(this)) {
        result = project.removeResearchObjectType(this);
      }
    }
    return result;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    ResearchObjectType that = (ResearchObjectType) o;
    return Objects.equals(this.name, that.name) && Objects.equals(this.rdfType, that.rdfType)
            && Objects.equals(this.description, that.description) && Objects.equals(this.sparqlListQuery, that.sparqlListQuery)
            && Objects.equals(this.sparqlDetailQuery, that.sparqlDetailQuery) && Objects.equals(this.projects, that.projects)
            && Objects.equals(this.ontology, that.ontology);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.name, this.rdfType, this.description, this.sparqlListQuery, this.sparqlDetailQuery,
            this.projects, this.ontology);
  }
}
