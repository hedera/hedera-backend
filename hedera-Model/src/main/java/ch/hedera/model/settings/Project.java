/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - Project.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.settings;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.converter.KeywordsConverter;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.ProjectAwareResource;
import ch.hedera.model.StorageType;
import ch.hedera.model.humanities.Rml;
import ch.hedera.model.security.Role;
import ch.hedera.model.triplestore.TriplestoreDataset;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Schema(description = "A project represents a research project which contains metadata (source datasets) and files (research data files).")
@Entity
@Table(name = "project")
public class Project extends ResourceNormalized implements ResourceFileInterface, ProjectAwareResource {

  @Schema(description = "The closing date of the project.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_FORMAT)
  private LocalDate closingDate;

  @Schema(description = "The default license of the project.")
  @ManyToOne(targetEntity = License.class)
  private License defaultLicense;

  @Schema(description = "The description of the project.")
  @Size(max = HederaConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @JsonIgnore
  @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<TriplestoreDataset> triplestoreDatasets = new ArrayList<>();

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "projectFundingAgencies", joinColumns = { @JoinColumn(name = HederaConstants.DB_PROJECT_ID) }, inverseJoinColumns = {
          @JoinColumn(name = HederaConstants.DB_AGENCY_ID) }, uniqueConstraints = {
                  @UniqueConstraint(name = "project_fundingAgency", columnNames = { HederaConstants.DB_PROJECT_ID,
                          HederaConstants.DB_AGENCY_ID }) })
  private List<FundingAgency> fundingAgencies;

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "projectInstitutions", joinColumns = { @JoinColumn(name = HederaConstants.DB_PROJECT_ID) }, inverseJoinColumns = {
          @JoinColumn(name = HederaConstants.DB_INSTITUTION_ID) }, uniqueConstraints = {
                  @UniqueConstraint(name = "project_institution", columnNames = { HederaConstants.DB_PROJECT_ID,
                          HederaConstants.DB_INSTITUTION_ID }) })
  private List<Institution> institutions;

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "projectRml", joinColumns = { @JoinColumn(name = HederaConstants.DB_PROJECT_ID) }, inverseJoinColumns = {
          @JoinColumn(name = HederaConstants.DB_RML_ID) }, uniqueConstraints = {
                  @UniqueConstraint(name = "project_rml", columnNames = { HederaConstants.DB_PROJECT_ID, HederaConstants.DB_RML_ID }) })
  private List<Rml> rmls;

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "projectResearchObjectTypes", joinColumns = { @JoinColumn(name = HederaConstants.DB_PROJECT_ID) }, inverseJoinColumns = {
          @JoinColumn(name = HederaConstants.DB_RESEARCH_OBJECT_TYPE_ID) }, uniqueConstraints = {
                  @UniqueConstraint(name = "project_researchObjectType", columnNames = { HederaConstants.DB_PROJECT_ID,
                          HederaConstants.DB_RESEARCH_OBJECT_TYPE_ID }) })
  private List<ResearchObjectType> researchObjectTypes;

  @Schema(description = "If the project has data (source dataset or research data file)")
  private Boolean hasData;

  @Schema(description = "The file number of the project (research data files)")
  private Long fileNumber;

  @Schema(description = "The triple number of the project (source datasets)")
  private Long tripleNumber;

  @Schema(description = "The total size of the project ")
  private Long totalSize;

  @Schema(description = "The keywords associated with this project, they are used as default for deposits created within this project.")
  @Convert(converter = KeywordsConverter.class)
  private List<String> keywords;

  @Schema(description = "The name of the project.")
  @NotNull
  @Column(unique = true)
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String name;

  @Schema(description = "The storage type of the project for research data files.")
  @NotNull
  @Enumerated(EnumType.STRING)
  private StorageType storageType;

  @Schema(description = "The short name of the project (folder for file storage, bucket for object storage & triplestore dataset).")
  @NotNull
  @Column(unique = true)
  @Size(min = 3, max = 63) // S3 Bucket limitation
  @Pattern(regexp = HederaConstants.AMAZON_S3_BUCKET_NAME_REGEX)
  private String shortName;

  @Schema(description = "The opening date of the project.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_FORMAT)
  @NotNull
  private LocalDate openingDate;

  @JsonIgnore
  @OneToMany(mappedBy = ProjectPersonRole.PATH_TO_PROJECT, cascade = CascadeType.ALL, orphanRemoval = true)
  private List<ProjectPersonRole> personRoles = new ArrayList<>();

  @Schema(description = "The URL of the project.")
  private URL url;

  @Schema(description = "The logo of the project.")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = HederaConstants.DB_LOGO_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private ProjectLogo logo;

  @Schema(description = "The copyright holder of the project")
  private String copyrightHolder;

  @Schema(description = "If the content of the project is public or private.")
  @NotNull
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean accessPublic;

  @Schema(description = "SPARQL query to generate IIIF manifest.")
  @Size(max = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String iiifManifestSparqlQuery;

  @Schema(description = "The research object type associated with an IIIF manifest")
  @ManyToOne
  @JoinColumn(name = HederaConstants.DB_IIIF_MANIFEST_RESEARCH_OBJECT_TYPE_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private ResearchObjectType iiifManifestResearchObjectType;

  @Schema(description = "The research object type associated with the research data file")
  @ManyToOne
  @JoinColumn(name = HederaConstants.DB_RESEARCH_DATA_FILE_RESEARCH_OBJECT_TYPE_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private ResearchObjectType researchDataFileResearchObjectType;

  public boolean addFundingAgency(FundingAgency fundingAgency) {
    final boolean result = this.fundingAgencies.add(fundingAgency);
    if (!fundingAgency.getProjects().contains(this)) {
      fundingAgency.addProject(this);
    }
    return result;
  }

  public boolean addInstitution(Institution institution) {
    final boolean result = this.institutions.add(institution);
    if (!institution.getProjects().contains(this)) {
      institution.addProject(this);
    }
    return result;
  }

  public boolean addRml(Rml rml) {
    final boolean result = this.rmls.add(rml);
    if (!rml.getProjects().contains(this)) {
      rml.addProject(this);
    }
    return result;
  }

  public boolean addResearchObjectType(ResearchObjectType researchObjectType) {
    final boolean result = this.researchObjectTypes.add(researchObjectType);
    if (!researchObjectType.getProjects().contains(this)) {
      researchObjectType.addProject(this);
    }
    return result;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.INSTITUTION).withRel(ResourceName.INSTITUTION));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PERSON).withRel(ResourceName.PERSON));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.FUNDING_AGENCY).withRel(ResourceName.FUNDING_AGENCY));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.RESEARCH_OBJECT).withRel(ResourceName.RESEARCH_OBJECT));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.RML).withRel(ResourceName.RML));
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.CLOSE).withRel(ActionName.CLOSE));
      this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.UPLOAD_LOGO).withRel(HederaActionName.UPLOAD_LOGO));
      this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.DOWNLOAD_LOGO).withRel(HederaActionName.DOWNLOAD_LOGO));
    }
  }

  public LocalDate getClosingDate() {
    return this.closingDate;
  }

  @JsonIgnore
  public String getDefault() {
    return Role.DEFAULT_ROLE_ID;
  }

  public License getDefaultLicense() {
    return this.defaultLicense;
  }

  public String getDescription() {
    return this.description;
  }

  public List<FundingAgency> getFundingAgencies() {
    return this.fundingAgencies;
  }

  public List<Institution> getInstitutions() {
    return this.institutions;
  }

  public List<ResearchObjectType> getResearchObjectTypes() {
    return this.researchObjectTypes;
  }

  public List<Rml> getRmls() {
    return this.rmls;
  }

  public List<String> getKeywords() {
    return this.keywords;
  }

  public String getName() {
    return this.name;
  }

  public LocalDate getOpeningDate() {
    return this.openingDate;
  }

  public List<ProjectPersonRole> getPersonRoles() {
    return this.personRoles;
  }

  public ProjectLogo getLogo() {
    return this.logo;
  }

  public String getCopyrightHolder() {
    return this.copyrightHolder;
  }

  public URL getUrl() {
    return this.url;
  }

  public String getIiifManifestSparqlQuery() {
    return this.iiifManifestSparqlQuery;
  }

  public String getShortName() {
    return this.shortName;
  }

  public StorageType getStorageType() {
    return this.storageType;
  }

  public Boolean isAccessPublic() {
    return this.accessPublic;
  }

  @JsonProperty("hasData")
  public Boolean hasData() {
    return this.hasData;
  }

  public void setHasData(Boolean hasData) {
    this.hasData = hasData;
  }

  public ResearchObjectType getIiifManifestResearchObjectType() {
    return this.iiifManifestResearchObjectType;
  }

  public ResearchObjectType getResearchDataFileResearchObjectType() {
    return this.researchDataFileResearchObjectType;
  }

  public void setResearchDataFileResearchObjectType(ResearchObjectType researchDataFileResearchObjectType) {
    this.researchDataFileResearchObjectType = researchDataFileResearchObjectType;
  }

  @PrePersist
  @PreUpdate
  @Override
  public void init() {
    if (this.openingDate == null) {
      this.openingDate = LocalDate.now();
    }
    if (this.hasData == null) {
      this.hasData = false;
    }
    if (this.accessPublic == null) {
      this.accessPublic = true;
    }
    if (this.storageType == null) {
      this.storageType = StorageType.FILE;
    }
    if (this.fileNumber == null) {
      this.fileNumber = 0L;
    }
    if (this.tripleNumber == null) {
      this.tripleNumber = 0L;
    }
    if (this.totalSize == null) {
      this.totalSize = 0L;
    }
  }

  public boolean isOpen() {
    final LocalDate now = LocalDate.now();
    return (this.openingDate != null
            && (this.openingDate.isEqual(now) || this.openingDate.isBefore(now))
            && (this.closingDate == null || this.closingDate.isEqual(now) || this.closingDate.isAfter(now)));
  }

  @Override
  @JsonIgnore
  public boolean isDeletable() {
    return !this.hasData && this.tripleNumber == 0 && this.fileNumber == 0 && this.totalSize == 0;
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public boolean removeFundingAgency(FundingAgency fundingAgency) {
    final boolean result = this.fundingAgencies.remove(fundingAgency);
    if (fundingAgency.getProjects().contains(this)) {
      fundingAgency.removeProject(this);
    }
    return result;
  }

  public void setLogo(ProjectLogo logo) {
    // Don't set dirty field, logo is updated through dedicated endpoints
    this.logo = logo;
  }

  public void setCopyrightHolder(String copyrightHolder) {
    this.copyrightHolder = copyrightHolder;
  }

  public void setClosingDate(LocalDate closingDate) {
    this.closingDate = closingDate;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setInstitutions(List<Institution> institutions) {
    this.institutions = institutions;
  }

  public void setDefaultLicense(License defaultLicense) {
    this.defaultLicense = defaultLicense;
  }

  public void setKeywords(List<String> keywords) {
    this.keywords = keywords;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setOpeningDate(LocalDate openingDate) {
    this.openingDate = openingDate;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  public void setAccessPublic(Boolean accessPublic) {
    this.accessPublic = accessPublic;
  }

  public void setIiifManifestSparqlQuery(String iiifManifestSparqlQuery) {
    this.iiifManifestSparqlQuery = iiifManifestSparqlQuery;
  }

  public void setShortName(String shortName) {
    this.shortName = shortName;
  }

  public void setStorageType(StorageType storageType) {
    this.storageType = storageType;
  }

  public void setIiifManifestResearchObjectType(ResearchObjectType iiifManifestResearchObjectType) {
    this.iiifManifestResearchObjectType = iiifManifestResearchObjectType;
  }

  public Long getFileNumber() {
    return this.fileNumber;
  }

  public void setFileNumber(Long fileNumber) {
    this.fileNumber = fileNumber;
  }

  public Long getTripleNumber() {
    return this.tripleNumber;
  }

  public void setTripleNumber(Long tripleNumber) {
    this.tripleNumber = tripleNumber;
  }

  public Long getTotalSize() {
    return this.totalSize;
  }

  public void setTotalSize(Long totalSize) {
    this.totalSize = totalSize;
  }

  @Schema(description = "The total size in human-readable format of the project.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = Access.READ_ONLY)
  public String getSmartTotalSize() {
    if (this.totalSize == null) {
      return HederaConstants.NO_SIZE;
    }
    return StringTool.formatSmartSize(this.totalSize);
  }

  @Override
  public String toString() {
    return super.toString() + " (name=" + this.getName() + ")";
  }

  public boolean removeInstitution(Institution institution) {
    final boolean result = this.institutions.remove(institution);
    if (institution.getProjects().contains(this)) {
      institution.removeProject(this);
    }
    return result;
  }

  public boolean removeRml(Rml rml) {
    final boolean result = this.rmls.remove(rml);
    if (rml.getProjects().contains(this)) {
      rml.removeProject(this);
    }
    return result;
  }

  protected boolean removePerson(Person person) {
    final Person personFinal = person;
    return this.personRoles.removeIf(ouprPredicate -> ouprPredicate.getCompositeKey()
            .getPerson() == personFinal);
  }

  public boolean addPersonRole(Person person, Role role) {
    final ProjectPersonRole projectPersonRole = new ProjectPersonRole();
    projectPersonRole.setProject(this);
    projectPersonRole.setRole(role);
    projectPersonRole.setPerson(person);
    return this.personRoles.add(projectPersonRole);
  }

  public boolean removePersonRole(Person person, Role role) {
    return this.personRoles.removeIf(oupr -> oupr.getProject().equals(this) &&
            oupr.getPerson().equals(person) &&
            oupr.getRole().equals(role));
  }

  public boolean removeResearchObjectType(ResearchObjectType researchObjectType) {
    final boolean result = this.researchObjectTypes.remove(researchObjectType);
    if (researchObjectType.getProjects().contains(this)) {
      researchObjectType.removeProject(this);
    }
    return result;
  }

  @Override
  @JsonIgnore
  public String getProjectId() {
    return this.getResId();
  }

  @Override
  @JsonIgnore
  public ResourceFile setNewResourceFile() {
    this.setLogo(new ProjectLogo());
    return this.getLogo();
  }

  @Override
  public void setResourceFile(ResourceFile logo) {
    this.setLogo((ProjectLogo) logo);
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getLogo();
  }

  @Override
  public List<String> getPatchableSubResources() {
    return List.of("logo");
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result
            + Objects.hash(this.accessPublic, this.closingDate, this.defaultLicense, this.description, this.fileNumber, this.hasData,
                    this.iiifManifestResearchObjectType, this.iiifManifestSparqlQuery, this.keywords, this.logo, this.name, this.openingDate,
                    this.researchDataFileResearchObjectType,
                    this.shortName, this.storageType, this.totalSize, this.tripleNumber, this.triplestoreDatasets, this.url);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    Project other = (Project) obj;
    return Objects.equals(this.accessPublic, other.accessPublic) && Objects.equals(this.closingDate, other.closingDate)
            && Objects.equals(this.defaultLicense, other.defaultLicense) && Objects.equals(this.description, other.description)
            && Objects.equals(this.fileNumber, other.fileNumber) && Objects.equals(this.hasData, other.hasData)
            && Objects.equals(this.iiifManifestResearchObjectType, other.iiifManifestResearchObjectType)
            && Objects.equals(this.iiifManifestSparqlQuery, other.iiifManifestSparqlQuery) && Objects.equals(this.keywords, other.keywords)
            && Objects.equals(this.logo, other.logo) && Objects.equals(this.name, other.name)
            && Objects.equals(this.openingDate, other.openingDate)
            && Objects.equals(this.researchDataFileResearchObjectType, other.researchDataFileResearchObjectType)
            && Objects.equals(this.shortName, other.shortName) && this.storageType == other.storageType
            && Objects.equals(this.totalSize, other.totalSize)
            && Objects.equals(this.tripleNumber, other.tripleNumber) && Objects.equals(this.triplestoreDatasets, other.triplestoreDatasets)
            && Objects.equals(this.url, other.url);
  }

}
