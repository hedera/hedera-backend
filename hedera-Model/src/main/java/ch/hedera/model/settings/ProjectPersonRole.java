/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ProjectPersonRole.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.settings;

import java.util.Objects;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.rest.JoinResource;

import ch.hedera.message.cache.ProjectPersonRoleCacheMessage;
import ch.hedera.model.security.Role;

@Entity
@Table(name = "projectPersonRoles")
public class ProjectPersonRole extends JoinResource<ProjectPersonRoleKey> {

  public static final String PATH_TO_PROJECT = "compositeKey.project";
  public static final String PATH_TO_PERSON = "compositeKey.person";
  public static final String PATH_TO_ROLE = "compositeKey.role";
  public static final String PROJECT_RELATION_PROPERTY_NAME = "personRoles";
  public static final String PERSON_RELATION_PROPERTY_NAME = "projectRoles";
  public static final String ROLE_RELATION_PROPERTY_NAME = "projectPersons";

  @EmbeddedId
  @JsonIgnore
  private ProjectPersonRoleKey compositeKey;

  public ProjectPersonRole() {
    this.compositeKey = new ProjectPersonRoleKey();
  }

  @Override
  @JsonIgnore
  public CacheMessage getCacheMessage() {
    return new ProjectPersonRoleCacheMessage(this.getPerson().getResId(), this.getProject().getResId(),
            this.getRole().getResId());
  }

  @Override
  public ProjectPersonRoleKey getCompositeKey() {
    return this.compositeKey;
  }

  @JsonIgnore
  public Project getProject() {
    return this.compositeKey.getProject();
  }

  @JsonIgnore
  public Person getPerson() {
    return this.compositeKey.getPerson();
  }

  @JsonIgnore
  public Role getRole() {
    return this.compositeKey.getRole();
  }

  @Override
  public void setCompositeKey(ProjectPersonRoleKey compositeKey) {
    this.compositeKey = compositeKey;
  }

  @JsonIgnore
  public void setProject(Project project) {
    this.compositeKey.setProject(project);
  }

  @JsonIgnore
  public void setPerson(Person person) {
    this.compositeKey.setPerson(person);
  }

  @JsonIgnore
  public void setRole(Role role) {
    this.compositeKey.setRole(role);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof ProjectPersonRole))
      return false;
    if (!super.equals(o))
      return false;
    final ProjectPersonRole that = (ProjectPersonRole) o;
    return Objects.equals(this.compositeKey, that.compositeKey);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.compositeKey);
  }
}
