/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - License.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.settings;

import java.net.URL;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotNull;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.rest.Tool;

import ch.hedera.HederaConstants;
import ch.hedera.model.ResourceIdentifierType;
import ch.hedera.rest.ModuleName;

/**
 * Structure based on open licenses. http://licenses.opendefinition.org
 */
@Schema(description = "A license represents software licenses (Academic Free License 3.0, Design Science License, MIT License…​).")
@Entity
public class License extends ResourceNormalized implements ResourceFileInterface {

  @Schema(description = "Conformance Status.")
  public enum ConformanceStatus {
    APPROVED("approved"), EMPTY(""), NOT_REVIEWED("not reviewed"), REJECTED("rejected");

    private String value;

    ConformanceStatus(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return this.value;
    }
  }

  // ======
  // end internals types
  // ======
  @Schema(description = "License status.")
  public enum LicenseStatus {
    ACTIVE("active"), RETIRED("retired"), SUPERSEDED("superseded");

    private String value;

    LicenseStatus(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return this.value;
    }
  }

  @Schema(description = "If the license is applicable for content.")
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean domainContent;

  @Schema(description = "If the license is applicable for data.")
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean domainData;

  @Schema(description = "If the license is applicable for software.")
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean domainSoftware;

  @Schema(description = "The family of the license.")
  private String family;

  @Schema(description = "If the license is generic.")
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean isGeneric;

  @Schema(description = "The maintainer of the license.")
  private String maintainer;

  @Schema(description = "The OD conformance status of the license.")
  @Enumerated(EnumType.STRING)
  private ConformanceStatus odConformance;

  @Schema(description = "The open license identifier of the license. The recommanded format is SPDX short identifier.")
  @NotNull
  @Column(unique = true, nullable = false)
  private String openLicenseId;

  @Schema(description = "The ODS conformance status of the license.")
  @Enumerated(EnumType.STRING)
  private ConformanceStatus osdConformance;

  @Schema(description = "The current status of the license.")
  @Enumerated(EnumType.STRING)
  private LicenseStatus status;

  @Schema(description = "The title of the license.")
  @NotNull
  private String title;

  @Schema(description = "The source URL of the license.")
  private URL url;

  @Schema(description = "The logo of the license.")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = HederaConstants.DB_LOGO_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private LicenseLogo logo;

  @Schema(description = "The description of the license. The format is: <openLicenseId> (<title>).", accessMode = AccessMode.READ_ONLY)
  public String getDescription() {
    return this.getOpenLicenseId() + " (" + this.getTitle() + ")";
  }

  public Boolean getDomainContent() {
    return this.domainContent;
  }

  public Boolean getDomainData() {
    return this.domainData;
  }

  public Boolean getDomainSoftware() {
    return this.domainSoftware;
  }

  public String getFamily() {
    return this.family;
  }

  public Boolean getIsGeneric() {
    return this.isGeneric;
  }

  public String getMaintainer() {
    return this.maintainer;
  }

  public ConformanceStatus getOdConformance() {
    return this.odConformance;
  }

  /***********************************************************/

  public String getOpenLicenseId() {
    return this.openLicenseId;
  }

  public ConformanceStatus getOsdConformance() {
    return this.osdConformance;
  }

  public LicenseStatus getStatus() {
    return this.status;
  }

  public String getTitle() {
    return this.title;
  }

  public URL getUrl() {
    return this.url;
  }

  public LicenseLogo getLogo() {
    return this.logo;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public void setDomainContent(Boolean domainContent) {
    this.domainContent = domainContent;
  }

  public void setDomainData(Boolean domainData) {
    this.domainData = domainData;
  }

  public void setDomainSoftware(Boolean domainSoftware) {
    this.domainSoftware = domainSoftware;
  }

  public void setFamily(String family) {
    this.family = family;
  }

  public void setIsGeneric(Boolean isGeneric) {
    this.isGeneric = isGeneric;
  }

  public void setMaintainer(String maintainer) {
    this.maintainer = maintainer;
  }

  public void setOdConformance(ConformanceStatus odConformance) {
    this.odConformance = odConformance;
  }

  public void setOpenLicenseId(String openLicenseId) {
    this.openLicenseId = openLicenseId;
  }

  public void setOsdConformance(ConformanceStatus osdConformance) {
    this.osdConformance = osdConformance;
  }

  // ==============
  // Internals types
  // ==============

  public void setStatus(LicenseStatus status) {
    this.status = status;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  public void setLogo(LicenseLogo logo) {
    this.logo = logo;
  }

  @Override
  @JsonIgnore
  public ResourceFile setNewResourceFile() {
    this.setLogo(new LicenseLogo());
    return this.getLogo();
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getLogo();
  }

  @Override
  public void setResourceFile(ResourceFile resourceFile) {
    this.setLogo((LicenseLogo) resourceFile);
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(
              Tool.filter(
                      linkBuilder.slash(this.getOpenLicenseId()).toUriComponentsBuilder(),
                      HederaConstants.IDENTIFIER_TYPE_PARAM,
                      ResourceIdentifierType.SPDX_ID.toString())
                      .withRel(ResourceIdentifierType.SPDX_ID.toString().toLowerCase()));
    }
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.domainContent, this.domainData, this.domainSoftware, this.family, this.isGeneric,
            this.maintainer, this.odConformance,
            this.openLicenseId, this.osdConformance, this.status, this.title, this.url);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    License other = (License) obj;
    return Objects.equals(this.domainContent, other.domainContent) && Objects.equals(this.domainData, other.domainData)
            && Objects.equals(this.domainSoftware, other.domainSoftware) && Objects.equals(this.family, other.family)
            && Objects.equals(this.isGeneric, other.isGeneric) && Objects.equals(this.maintainer, other.maintainer)
            && this.odConformance == other.odConformance && Objects.equals(this.openLicenseId, other.openLicenseId)
            && this.osdConformance == other.osdConformance && this.status == other.status && Objects.equals(this.title, other.title)
            && Objects.equals(this.url, other.url);
  }

}
