/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IIIFCollectionSettings.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.settings;

import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.hedera.HederaConstants;
import ch.hedera.rest.ModuleName;

@Entity
@Table(name = "iiifCollectionSettings", uniqueConstraints = {
        @UniqueConstraint(name = "project_name", columnNames = { HederaConstants.DB_PROJECT_ID, HederaConstants.DB_NAME }) })
@Schema(description = "The IIIFCollection configuration")
public class IIIFCollectionSettings extends ResourceNormalized {

  @NotNull
  @Schema(description = "The name of the IIIF collection.")
  private String name;

  @Schema(description = "The description of the IIIF collection.")
  @Size(min = 1, max = SolidifyConstants.DB_LARGE_STRING_LENGTH)
  private String description;

  @Schema(description = "SPARQL requests to generate IIIF collection.")
  @NotNull
  @Size(max = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String iiifCollectionSparqlQuery;

  @ManyToOne
  @NotNull
  @JoinColumn(name = HederaConstants.DB_PROJECT_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private Project project;

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ACCESS;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getIiifCollectionSparqlQuery() {
    return this.iiifCollectionSparqlQuery;
  }

  public void setIiifCollectionSparqlQuery(String iiifCollectionSparqlQuery) {
    this.iiifCollectionSparqlQuery = iiifCollectionSparqlQuery;
  }

  public Project getProject() {
    return this.project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof IIIFCollectionSettings that))
      return false;
    if (!super.equals(o))
      return false;
    return Objects.equals(this.name, that.name) && Objects.equals(this.iiifCollectionSparqlQuery, that.iiifCollectionSparqlQuery)
            && Objects.equals(this.project, that.project);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.name, this.iiifCollectionSparqlQuery, this.project);
  }
}
