/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ProjectPersonRoleKey.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.settings;

import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import ch.hedera.HederaConstants;
import ch.hedera.model.security.Role;

@Embeddable
public class ProjectPersonRoleKey implements Serializable {
  private static final long serialVersionUID = -7546695814255442690L;

  @ManyToOne
  @JoinColumn(name = HederaConstants.DB_PROJECT_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private Project project;

  @ManyToOne
  private Person person;

  @ManyToOne
  private Role role;

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof ProjectPersonRoleKey)) {
      return false;
    }
    final ProjectPersonRoleKey castOther = (ProjectPersonRoleKey) other;
    return Objects.equals(this.getProject(), castOther.getProject()) && Objects.equals(this.getPerson(), castOther.getPerson());
  }

  public Project getProject() {
    return this.project;
  }

  public Person getPerson() {
    return this.person;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getProject(), this.getPerson());
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public Role getRole() {
    return this.role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

}
