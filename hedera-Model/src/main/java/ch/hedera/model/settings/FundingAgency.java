/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - FundingAgency.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.settings;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.ResourceIdentifierType;
import ch.hedera.model.RorInterface;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Schema(description = "A funding agency represents organizations or groups who provide funds for research projects.")
@Entity
public class FundingAgency extends ResourceNormalized implements ResourceFileInterface, RorInterface {

  @Schema(description = "The acronym of the funding agency.")
  @NotNull
  @Column(unique = true)
  @Size(min = 1, max = 100)
  private String acronym;

  @Schema(description = "The description of the funding agency.")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @Schema(description = "The name of the funding agency.")
  @NotNull
  @Column(unique = true)
  @Size(min = 1)
  private String name;

  @ManyToMany(mappedBy = "fundingAgencies")
  @JsonIgnore
  private List<Project> projects;

  @Schema(description = "The URL of the funding agency.")
  private URL url;

  @Schema(description = "The ROR identifier of the funding agency.")
  @Column(unique = true)
  private String rorId;

  @Schema(description = "The logo of the funding agency.")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = HederaConstants.DB_LOGO_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private FundingAgencyLogo logo;

  @Schema(description = "The other identifier list of the funding agency.")
  @Transient
  private Map<String, String> identifiers = new HashMap<>();

  /***************************************************/

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {

    super.addLinks(linkBuilder, mainRes, subResOnly);

    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PROJECT).withRel(ResourceName.PROJECT));
      this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.UPLOAD_LOGO).withRel(HederaActionName.UPLOAD_LOGO));
      this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.DOWNLOAD_LOGO).withRel(HederaActionName.DOWNLOAD_LOGO));
      this.add(Tool.filter(
              linkBuilder.slash(this.getRorId()).toUriComponentsBuilder(),
              HederaConstants.IDENTIFIER_TYPE_PARAM,
              ResourceIdentifierType.ACRONYM.toString())
              .withRel(ResourceIdentifierType.ACRONYM.toString().toLowerCase()));
      this.add(Tool.filter(
              linkBuilder.slash(this.getRorId()).toUriComponentsBuilder(),
              HederaConstants.IDENTIFIER_TYPE_PARAM,
              ResourceIdentifierType.NAME.toString())
              .withRel(ResourceIdentifierType.NAME.toString().toLowerCase()));
      if (!StringTool.isNullOrEmpty(this.getRorId())) {
        this.add(
                Tool.filter(
                        linkBuilder.slash(this.getRorId()).toUriComponentsBuilder(),
                        HederaConstants.IDENTIFIER_TYPE_PARAM,
                        ResourceIdentifierType.ROR_ID.toString())
                        .withRel(ResourceIdentifierType.ROR_ID.toString().toLowerCase()));
      }
    }
  }

  public boolean addProject(Project project) {

    boolean result = this.projects.add(project);

    if (result && !project.getFundingAgencies().contains(this)) {
      result = project.addFundingAgency(this);
    }

    return result;
  }

  public String getAcronym() {
    return this.acronym;
  }

  public String getDescription() {
    return this.description;
  }

  /***************************************************/

  public String getName() {
    return this.name;
  }

  public List<Project> getProjects() {
    return this.projects;
  }

  /***************************************************/

  public URL getUrl() {
    return this.url;
  }

  @Override
  public String getRorId() {
    return this.rorId;
  }

  /***************************************************/

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public boolean removeProject(Project project) {

    boolean result = true;

    if (this.projects.contains(project)) {

      result = this.projects.remove(project);

      if (result && project.getFundingAgencies().contains(this)) {
        result = project.removeFundingAgency(this);
      }
    }

    return result;

  }

  public void setAcronym(String acronym) {
    this.acronym = acronym;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  public FundingAgencyLogo getLogo() {
    return this.logo;
  }

  public void setLogo(FundingAgencyLogo logo) {
    this.logo = logo;
  }

  @Override
  @JsonIgnore
  public ResourceFile setNewResourceFile() {
    this.setLogo(new FundingAgencyLogo());
    return this.getLogo();
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getLogo();
  }

  @Override
  public void setResourceFile(ResourceFile logo) {
    this.setLogo((FundingAgencyLogo) logo);
  }

  @Override
  public Map<String, String> getIdentifiers() {
    return this.identifiers;
  }

  @Override
  public void setRorId(String rorId) {
    this.rorId = rorId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.acronym, this.description, this.name, this.rorId, this.url);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    FundingAgency other = (FundingAgency) obj;
    return Objects.equals(this.acronym, other.acronym) && Objects.equals(this.description, other.description)
            && Objects.equals(this.name, other.name) && Objects.equals(this.rorId, other.rorId) && Objects.equals(this.url, other.url);
  }

}
