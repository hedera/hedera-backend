/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - Person.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.settings;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.auth.model.OrcidInfo;
import ch.unige.solidify.auth.model.PersonInfo;
import ch.unige.solidify.model.PersonWithOrcid;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.SearchableResourceNormalized;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.ResourceIdentifierType;
import ch.hedera.model.security.Role;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;
import ch.hedera.specification.PersonSearchSpecification;

@Schema(description = "A person is associated to a user on the platform. It contains information such as the person’s ORCID, institution and organizational units it belongs to.")
@Entity
public class Person extends SearchableResourceNormalized<Person>
        implements PersonInfo, PersonWithOrcid, ResourceFileInterface {

  @JsonIgnore
  @OneToMany(mappedBy = ProjectPersonRole.PATH_TO_PERSON, cascade = CascadeType.ALL, orphanRemoval = true)
  protected List<ProjectPersonRole> projectRoles = new ArrayList<>();

  @Schema(description = "The first name of the person.")
  @NotNull
  @Size(min = 1)
  private String firstName;

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "personInstitutions", joinColumns = { @JoinColumn(name = HederaConstants.DB_PERSON_ID) }, inverseJoinColumns = {
          @JoinColumn(name = HederaConstants.DB_INSTITUTION_ID) }, uniqueConstraints = {
                  @UniqueConstraint(name = "person_institution", columnNames = { HederaConstants.DB_PERSON_ID,
                          HederaConstants.DB_INSTITUTION_ID }) })
  private List<Institution> institutions = new ArrayList<>();

  @Schema(description = "The last name of the person.")
  @NotNull
  @Size(min = 1)
  private String lastName;

  @Schema(description = "The ORCID of the person.")
  @Pattern(regexp = OrcidInfo.ORCID_PATTERN, message = OrcidInfo.ORCID_MESSAGE)
  @Column(unique = true)
  private String orcid;

  @Schema(description = "If the person ORCID is verified.")
  @NotNull
  private Boolean verifiedOrcid = false;

  @Schema(description = "The avatar of the person.")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = HederaConstants.DB_AVATAR_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private PersonAvatar avatar;

  public boolean addInstitution(Institution institution) {
    final boolean result = this.institutions.add(institution);
    if (!institution.getPeople().contains(this)) {
      institution.addPerson(this);
    }
    return result;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.INSTITUTION).withRel(ResourceName.INSTITUTION));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PROJECT).withRel(ResourceName.PROJECT));
      this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.DOWNLOAD_AVATAR).withRel(HederaActionName.DOWNLOAD_AVATAR));
      this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.UPLOAD_AVATAR).withRel(HederaActionName.UPLOAD_AVATAR));
      if (!StringTool.isNullOrEmpty(this.getOrcid())) {
        this.add(
                Tool.filter(
                        linkBuilder.slash(this.getOrcid()).toUriComponentsBuilder(),
                        HederaConstants.IDENTIFIER_TYPE_PARAM,
                        ResourceIdentifierType.ORCID.toString())
                        .withRel(ResourceIdentifierType.ORCID.toString().toLowerCase()));
      }
    }
  }

  @JsonIgnore
  public String getDefault() {
    return Role.DEFAULT_ROLE_ID;
  }

  /************************************************************/

  public String getFirstName() {
    return this.firstName;
  }

  @Override
  public String getFullName() {
    return this.getLastName() + ", " + this.getFirstName();
  }

  public List<Institution> getInstitutions() {
    return this.institutions;
  }

  public String getLastName() {
    return this.lastName;
  }

  @Override
  public String getOrcid() {
    return this.orcid;
  }

  public List<ProjectPersonRole> getProjectRoles() {
    return this.projectRoles;
  }

  public PersonAvatar getAvatar() {
    return this.avatar;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public boolean removeInstitution(Institution institution) {
    final boolean result = this.institutions.remove(institution);
    if (institution.getPeople().contains(this)) {
      institution.removePerson(this);
    }
    return result;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @Override
  public void setOrcid(String orcid) {
    this.orcid = orcid;
  }

  public void setAvatar(PersonAvatar avatar) {
    this.avatar = avatar;
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("Person [resId=").append(this.getResId())
            .append(", firstName=").append(this.firstName)
            .append(", lastName=").append(this.lastName)
            .append(", orcid=").append(this.orcid).append("]");
    return builder.toString();
  }

  protected boolean removeProject(Project unit) {
    final Project project = unit;
    return this.projectRoles.removeIf(ouprPredicate -> ouprPredicate.getCompositeKey()
            .getProject() == project);
  }

  private boolean removeProjectRole(ProjectPersonRole projectPersonRole) {
    return this.projectRoles.removeIf(ouprPredicate -> ouprPredicate.equals(projectPersonRole));
  }

  @Override
  @JsonIgnore
  public ResourceFile setNewResourceFile() {
    this.setAvatar(new PersonAvatar());
    return this.getAvatar();
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getAvatar();
  }

  @Override
  public void setResourceFile(ResourceFile logo) {
    this.setAvatar((PersonAvatar) logo);
  }

  @Override
  public Specification<Person> getSearchSpecification(SearchCriteria criteria) {
    return new PersonSearchSpecification(criteria);
  }

  @Override
  public Boolean isVerifiedOrcid() {
    return this.verifiedOrcid;
  }

  @Override
  public void setVerifiedOrcid(Boolean verifiedOrcid) {
    this.verifiedOrcid = verifiedOrcid;
  }
}
