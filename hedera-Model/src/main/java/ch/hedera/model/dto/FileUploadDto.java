/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - FileUploadDto.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.dto;

import java.io.InputStream;

import ch.hedera.model.AbstractDatasetFile;

public class FileUploadDto<T extends AbstractDatasetFile> {
  private String originalFileName;
  private InputStream inputStream;
  private T resourceFile;

  public FileUploadDto(T resourceFile) {
    this.setResourceFile(resourceFile);
  }

  public String getOriginalFileName() {
    return this.originalFileName;
  }

  public void setOriginalFileName(String originalFileName) {
    this.originalFileName = originalFileName;
  }

  public InputStream getInputStream() {
    return this.inputStream;
  }

  public void setInputStream(InputStream inputStream) {
    this.inputStream = inputStream;
  }

  public T getResourceFile() {
    return this.resourceFile;
  }

  public void setResourceFile(T resourceFile) {
    this.resourceFile = resourceFile;
  }
}
