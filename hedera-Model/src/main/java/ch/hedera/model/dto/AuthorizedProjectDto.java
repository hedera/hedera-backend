/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - AuthorizedProjectDto.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.dto;

import java.util.Objects;

import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Project;

public class AuthorizedProjectDto extends Project {

  private Role role;

  public Role getRole() {
    return this.role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public static AuthorizedProjectDto fromEntity(Project project) {
    AuthorizedProjectDto projectDto = new AuthorizedProjectDto();
    projectDto.setResId(project.getResId());
    projectDto.setName(project.getName());
    projectDto.setDefaultLicense(project.getDefaultLicense());
    projectDto.setClosingDate(project.getClosingDate());
    projectDto.setDescription(project.getDescription());
    projectDto.setHasData(project.hasData());
    projectDto.setKeywords(project.getKeywords());
    projectDto.setOpeningDate(project.getOpeningDate());
    projectDto.setUrl(project.getUrl());
    projectDto.setLogo(project.getLogo());
    projectDto.setAccessPublic(project.isAccessPublic());
    projectDto.setHasData(project.hasData());
    projectDto.setTripleNumber(project.getTripleNumber());
    projectDto.setFileNumber(project.getFileNumber());
    projectDto.setTotalSize(project.getTotalSize());
    projectDto.setShortName(project.getShortName());
    return projectDto;

  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.role);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    AuthorizedProjectDto other = (AuthorizedProjectDto) obj;
    return Objects.equals(this.role, other.role);
  }

}
