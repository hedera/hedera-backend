/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IIIFCollectionItem.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.iiif;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.hedera.adapter.IIIFMultiLanguageTextDeserializer;

@Schema(description = "A IIIF collection item is the item of a IIIF collection.")
@JsonPropertyOrder({ "id", "type", "label" })
public class IIIFCollectionItem {

  @Schema(description = "The identifier of the IIIF collection item.")
  private String id;

  @Schema(description = "The type of the IIIF collection item.")
  private String type = "Manifest";

  @Schema(description = "The label of the IIIF collection item.")
  @JsonDeserialize(using = IIIFMultiLanguageTextDeserializer.class)
  private IIIFMultiLanguageText label;

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getType() {
    return this.type;
  }

  public IIIFMultiLanguageText getLabel() {
    return this.label;
  }

  public void setLabel(IIIFMultiLanguageText label) {
    this.label = label;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.id, this.label, this.type);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    IIIFCollectionItem other = (IIIFCollectionItem) obj;
    return Objects.equals(this.id, other.id) && Objects.equals(this.label, other.label) && Objects.equals(this.type, other.type);
  }

}
