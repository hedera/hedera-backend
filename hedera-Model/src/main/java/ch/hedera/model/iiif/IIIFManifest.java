/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IIIFManifest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.iiif;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.hedera.adapter.IIIFMultiLanguageTextDeserializer;

@Schema(description = "A IIIF manifest is the metadata of a IIIF resource.")
@JsonPropertyOrder({ "context", "id", "type", "label", "metadata", "requiredStatement", "canvases" })
public class IIIFManifest {

  @Schema(description = "The JSON schema of the IIIF manifest.")
  private List<String> context;

  @Schema(description = "The identifier of the IIIF manifest.")
  private String id;

  @Schema(description = "The type of the IIIF manifest.")
  private String type;

  @Schema(description = "The label of the IIIF manifest.")
  @JsonDeserialize(using = IIIFMultiLanguageTextDeserializer.class)
  private IIIFMultiLanguageText label;

  @Schema(description = "The metadata of the IIIF manifest.")
  private IIIFMetadataItem[] metadata;

  @Schema(description = "The required statement of the IIIF manifest.")
  private IIIFRequiredStatement requiredStatement;

  @Schema(description = "The canvases of the IIIF manifest.")
  private List<IIIFCanvas> canvases;

  @JsonProperty("@context")
  public List<String> getContext() {
    return this.context;
  }

  public void setContext(List<String> context) {
    this.context = context;
  }

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getType() {
    return this.type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public IIIFMultiLanguageText getLabel() {
    return this.label;
  }

  public void setLabel(IIIFMultiLanguageText label) {
    this.label = label;
  }

  public IIIFMetadataItem[] getMetadata() {
    return this.metadata;
  }

  public void setMetadata(IIIFMetadataItem[] metadata) {
    this.metadata = metadata;
  }

  public IIIFRequiredStatement getRequiredStatement() {
    return this.requiredStatement;
  }

  public void setRequiredStatement(IIIFRequiredStatement requiredStatement) {
    this.requiredStatement = requiredStatement;
  }

  @JsonProperty("items")
  public List<IIIFCanvas> getCanvases() {
    return this.canvases;
  }

  public void setCanvases(List<IIIFCanvas> canvases) {
    this.canvases = canvases;
  }

}
