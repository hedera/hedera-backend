/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IIIFPageDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.iiif;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "A IIIF Page DTO.")
public class IIIFPageDTO {

  @Schema(description = "The page identifier of the IIIF page DTO.")
  private String pageId;

  @Schema(description = "The page label of the IIIF page DTO.")
  private String pageLabel;

  @Schema(description = "The width of the IIIF page DTO.")
  private int width;

  @Schema(description = "The height of the IIIF page DTO.")
  private int height;

  public IIIFPageDTO() {

  }

  public IIIFPageDTO(String pageId, String pageLabel, int width, int height) {
    this.pageId = pageId;
    this.pageLabel = pageLabel;
    this.width = width;
    this.height = height;
  }

  public String getPageId() {
    return this.pageId;
  }

  public void setPageId(String pageId) {
    this.pageId = pageId;
  }

  public String getPageLabel() {
    return this.pageLabel;
  }

  public void setPageLabel(String pageLabel) {
    this.pageLabel = pageLabel;
  }

  public int getWidth() {
    return this.width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public int getHeight() {
    return this.height;
  }

  public void setHeight(int height) {
    this.height = height;
  }
}
