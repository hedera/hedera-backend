/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IIIFMultiLanguageText.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.iiif;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "A IIIF label is the description, support multi-languages.")
public class IIIFMultiLanguageText {

  @Schema(description = "The label list by language of the IIIF label.")
  private final Map<String, List<String>> multiLanguageMap = new HashMap<>();

  public void addText(String language, List<String> textValues) {
    this.multiLanguageMap.put(language, textValues);
  }

  @JsonAnyGetter
  public Map<String, List<String>> getMultiLanguageMap() {
    return this.multiLanguageMap;
  }
}
