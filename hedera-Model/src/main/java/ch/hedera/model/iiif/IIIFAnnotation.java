/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IIIFAnnotation.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.iiif;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "A IIIF annotation.")
@JsonPropertyOrder({ "id", "type", "target", "motivation", "body" })
public class IIIFAnnotation {

  @Schema(description = "The identifier of the IIIF annotation.")
  private String id;

  @Schema(description = "The type of the IIIF annotation.")
  private String type;

  @Schema(description = "The target of the IIIF annotation.")
  private String target;

  @Schema(description = "The motivation of the IIIF annotation.")
  private String motivation;

  @Schema(description = "The image of the IIIF annotation.")
  @JsonProperty("body")
  private IIIFImage image;

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getType() {
    return this.type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getTarget() {
    return this.target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public String getMotivation() {
    return this.motivation;
  }

  public void setMotivation(String motivation) {
    this.motivation = motivation;
  }

  public IIIFImage getImage() {
    return this.image;
  }

  public void setImage(IIIFImage image) {
    this.image = image;
  }
}
