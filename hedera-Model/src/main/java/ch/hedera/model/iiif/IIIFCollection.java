/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IIIFCollection.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.iiif;

import java.util.Arrays;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.hedera.adapter.IIIFMultiLanguageTextDeserializer;

@Schema(description = "A IIIF collection is the set of IIIF resources.")
@JsonPropertyOrder({ "context", "id", "type", "label", "items" })
public class IIIFCollection {

  @Schema(description = "The JSON schema of the IIIF collection.")
  private String context = "http://iiif.io/api/presentation/3/context.json";

  @Schema(description = "The identifier of the IIIF collection.")
  private String id;

  @Schema(description = "The type of the IIIF collection.")
  private String type = "Collection";

  @Schema(description = "The label of the IIIF collection.")
  @JsonDeserialize(using = IIIFMultiLanguageTextDeserializer.class)
  private IIIFMultiLanguageText label;

  @Schema(description = "The items of the IIIF collection.")
  private IIIFCollectionItem[] items;

  public String getContext() {
    return this.context;
  }

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getType() {
    return this.type;
  }

  public IIIFMultiLanguageText getLabel() {
    return this.label;
  }

  public void setLabel(IIIFMultiLanguageText label) {
    this.label = label;
  }

  public IIIFCollectionItem[] getItems() {
    return this.items;
  }

  public void setItems(IIIFCollectionItem[] items) {
    this.items = items;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Arrays.hashCode(this.items);
    result = prime * result + Objects.hash(this.context, this.id, this.label, this.type);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    IIIFCollection other = (IIIFCollection) obj;
    return Objects.equals(this.context, other.context) && Objects.equals(this.id, other.id) && Arrays.equals(this.items, other.items)
            && Objects.equals(this.label, other.label) && Objects.equals(this.type, other.type);
  }

  @Override
  public String toString() {
    return "IIIFCollection [id=" + this.getId() + ", " +
           "label=" + this.getLabel() + "]";
  }
}
