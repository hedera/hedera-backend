/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IIIFCanvas.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.iiif;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.hedera.adapter.IIIFMultiLanguageTextDeserializer;

@Schema(description = "A IIIF Canvas.")
@JsonPropertyOrder({ "id", "type", "label", "height", "width", "items" })
public class IIIFCanvas {

  @Schema(description = "The identifier of the IIIF canvas.")
  private String id;

  @Schema(description = "The tyoe of the IIIF canvas.")
  private String type;

  @Schema(description = "The label of the IIIF canvas.")
  @JsonDeserialize(using = IIIFMultiLanguageTextDeserializer.class)
  private IIIFMultiLanguageText label;

  @Schema(description = "The height of the IIIF canvas.")
  private int height;

  @Schema(description = "The width of the IIIF canvas.")
  private int width;

  @Schema(description = "The annotation pages of the IIIF canvas.")
  @JsonProperty("items")
  private List<IIIFAnnotationPage> annotationPages;

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getType() {
    return this.type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public IIIFMultiLanguageText getLabel() {
    return this.label;
  }

  public void setLabel(IIIFMultiLanguageText label) {
    this.label = label;
  }

  public int getHeight() {
    return this.height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public int getWidth() {
    return this.width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public List<IIIFAnnotationPage> getAnnotationPages() {
    return this.annotationPages;
  }

  public void setAnnotationPages(List<IIIFAnnotationPage> annotationPages) {
    this.annotationPages = annotationPages;
  }

}
