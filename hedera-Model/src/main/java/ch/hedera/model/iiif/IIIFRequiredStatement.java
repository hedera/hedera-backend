/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IIIFRequiredStatement.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.iiif;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.hedera.adapter.IIIFMultiLanguageTextDeserializer;

@Schema(description = "A IIIF Required Statement.")
@JsonPropertyOrder({ "label", "value" })
public class IIIFRequiredStatement {

  @Schema(description = "The name of the IIIF required statement.")
  @JsonDeserialize(using = IIIFMultiLanguageTextDeserializer.class)
  private IIIFMultiLanguageText label;

  @Schema(description = "The value of the IIIF required statement.")
  @JsonDeserialize(using = IIIFMultiLanguageTextDeserializer.class)
  private IIIFMultiLanguageText value;

  public IIIFMultiLanguageText getLabel() {
    return this.label;
  }

  public void setLabel(IIIFMultiLanguageText label) {
    this.label = label;
  }

  public IIIFMultiLanguageText getValue() {
    return this.value;
  }

  public void setValue(IIIFMultiLanguageText value) {
    this.value = value;
  }
}
