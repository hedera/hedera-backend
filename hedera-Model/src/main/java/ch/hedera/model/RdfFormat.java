/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - RdfFormat.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.hedera.HederaConstants;

@Schema(description = "RDF format.")
public enum RdfFormat {
  //@formatter:off
  JSON_LD(HederaConstants.JSON_LD),
  RDF_XML(HederaConstants.RDF_XML),
  RDF_JSON(HederaConstants.RDF_JSON),
  N_TRIPLES(HederaConstants.N_TRIPLES),
  TURTLE(HederaConstants.TURTLE),
  N3(HederaConstants.N3),
  N_QUADS(HederaConstants.N_QUADS),
  TRIG(HederaConstants.TRIG),
  TRIX(HederaConstants.TRIX);
  //@formatter:on

  @Schema(description = "Name of the RDF format.")
  private final String name;

  RdfFormat(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

}
