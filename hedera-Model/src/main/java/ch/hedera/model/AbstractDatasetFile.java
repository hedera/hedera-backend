/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - AbstractDatasetFile.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreRemove;
import jakarta.validation.constraints.NotNull;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.SearchableResourceNormalized;
import ch.unige.solidify.util.FileTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.ingest.ResearchDataFile;

@Schema(description = "This class defines properties concerned to the file linked to a datasetFile(could be the SourceDataset or OutputDataset/Dataset.")
@MappedSuperclass
public abstract class AbstractDatasetFile<T extends Resource, V> extends SearchableResourceNormalized<V> {
  @Schema(description = "The file size in bytes.")
  private Long fileSize;

  @Schema(description = "The file name.")
  @NotNull
  private String fileName;

  @Schema(description = "Path in the file system where the file is stored.")
  @Column(length = HederaConstants.DB_LARGE_STRING_LENGTH)
  @JsonIgnore
  private URI sourcePath;

  @Schema(description = "The research data file of source dataset file.")
  @ManyToOne(optional = true)
  @JoinColumn(name = HederaConstants.DB_RESEARCH_DATA_FILE_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private ResearchDataFile researchDataFile;

  public abstract T getDataset();

  public abstract void setDataset(T resource);

  public Long getFileSize() {
    return this.fileSize;
  }

  public String getFileName() {
    return this.fileName;
  }

  public ResearchDataFile getResearchDataFile() {
    return this.researchDataFile;
  }

  public boolean isLinkToResearchDataFile() {
    return this.researchDataFile != null;
  }

  public URI getSourcePath() {
    return this.sourcePath;
  }

  @JsonIgnore
  public Path getEffectiveSourcePath() {
    if (this.isLinkToResearchDataFile()) {
      return Paths.get(this.getResearchDataFile().getSourcePath());
    } else if (this.getSourcePath() != null){
      return Paths.get(this.getSourcePath());
    } else {
      throw new IllegalStateException("SourceDatasetFile is not linked to a ResearchDataFile and the source path is null.");
    }
  }

  public void setFileSize(Long fileSize) {
    this.fileSize = fileSize;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public void setSourcePath(URI sourcePath) {
    this.sourcePath = sourcePath;
  }

  public void setResearchDataFile(ResearchDataFile researchDataFile) {
    this.researchDataFile = researchDataFile;
    if (this.researchDataFile != null) {
      this.setFileName(researchDataFile.getFileName());
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    AbstractDatasetFile other = (AbstractDatasetFile) obj;
    return Objects.equals(this.fileName, other.fileName) && Objects.equals(this.fileSize, other.fileSize)
            && Objects.equals(this.researchDataFile, other.researchDataFile) && Objects.equals(this.sourcePath, other.sourcePath);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.fileName, this.fileSize, this.researchDataFile, this.sourcePath);
    return result;
  }

  @Override
  @PrePersist
  public void init() {
    if (this.fileSize == null) {
      this.fileSize = 0L;
    }
  }

  @PreRemove
  public void removeFile() {
    if (!this.isLinkToResearchDataFile()) {
      try {
        FileTool.deleteFile(Paths.get(this.getSourcePath()));
      } catch (IOException e) {
        throw new SolidifyRuntimeException("Unable to delete file from AbstractDatasetFile", e);
      }
    }
  }
}
