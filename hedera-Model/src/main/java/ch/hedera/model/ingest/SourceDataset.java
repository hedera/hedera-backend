/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - SourceDataset.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.ingest;

import static ch.unige.solidify.SolidifyConstants.DB_DEFAULT_STRING_LENGTH;
import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.SearchableResourceNormalized;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.listener.HistoryListener;
import ch.hedera.listener.ProjectDataListener;
import ch.hedera.model.ProjectAwareResource;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;
import ch.hedera.specification.SourceDatasetSearchSpecification;

@Schema(description = """
        A source dataset represents the input data file uploaded by the user that could be in different formats as: CSV, JSON, XML or RDF.
        Source dataset status:
        - ACTIVE => Modifiable dataset
        - PRE_PUBLISHED => No-modifiable but modifiable again
        - PUBLISHED => Immutable dataset because there is an archive referenced ba the archive DOI
        """)
@EntityListeners({ HistoryListener.class, ProjectDataListener.class })
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "projectId_name", columnNames = { HederaConstants.DB_PROJECT_ID, "name" }) })
public class SourceDataset extends SearchableResourceNormalized<SourceDataset> implements RemoteResourceContainer, ProjectAwareResource,
        ResourceFileInterface {
  public SourceDataset() {
    super();
  }

  public SourceDataset(String projectId) {
    // constructor for specification
    this.setResId(null);
    this.projectId = projectId;
  }

  public enum SourceDatasetStatus {
    ACTIVE, PRE_PUBLISHED, PUBLISHED
  }

  @Schema(description = "The name of the source dataset.")
  @NotNull
  @Column
  @Size(min = 1, max = DB_DEFAULT_STRING_LENGTH)
  private String name;

  @Schema(description = "The description of the source dataset.")
  @Size(max = HederaConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @Schema(description = "The status of the source dataset.")
  @Enumerated(EnumType.STRING)
  private SourceDatasetStatus status;

  @Schema(description = "The detailed message related to the source dataset status.")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String statusMessage;

  @Schema(description = "The Digital Object Identifier (DOI) of the corresponding archive for the source dataset.")
  private String archiveDoi;

  @Schema(description = "The project of the source dataset.")
  @Transient
  private Project project;

  @Schema(description = "The project identifier of the dataset.")
  @NotNull
  @Size(min = 1)
  @Column(name = HederaConstants.DB_PROJECT_ID, length = DB_ID_LENGTH)
  private String projectId;

  @JsonIgnore
  @OneToMany(mappedBy = "sourceDataset", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<SourceDatasetFile> dataFiles = new ArrayList<>();

  @Schema(description = "The date of the last import of one of the SourceDataSetFiles.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @LastModifiedDate
  private OffsetDateTime lastImportDate;

  @Schema(description = "The logo of the SourceDataset.")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = HederaConstants.DB_LOGO_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private SourceDatasetLogo logo;

  public String getName() {
    return this.name;
  }

  public String getDescription() {
    return this.description;
  }

  public SourceDatasetStatus getStatus() {
    return this.status;
  }

  public String getStatusMessage() {
    return this.statusMessage;
  }

  public Project getProject() {
    return this.project;
  }

  @Override
  public String getProjectId() {
    return this.projectId;
  }

  public List<SourceDatasetFile> getDataFiles() {
    return this.dataFiles;
  }

  public OffsetDateTime getLastImportDate() {
    return this.lastImportDate;
  }

  public SourceDatasetLogo getLogo() {
    return this.logo;
  }

  public int getFileNumber() {
    return this.dataFiles.size();
  }

  public String getArchiveDoi() {
    return this.archiveDoi;
  }

  public void setLogo(SourceDatasetLogo logo) {
    this.logo = logo;
  }

  public void setLastImportDate(OffsetDateTime lastImportDate) {
    this.lastImportDate = lastImportDate;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public void setStatus(SourceDatasetStatus status) {
    this.status = status;
  }

  public void setStatusMessage(String statusMessage) {
    this.statusMessage = StringTool.truncateWithEllipsis(statusMessage, SolidifyConstants.DB_LONG_STRING_LENGTH);
  }

  public void setArchiveDoi(String archiveDoi) {
    this.archiveDoi = archiveDoi;
  }

  @Override
  public void init() {
    if (this.status == null) {
      this.status = SourceDatasetStatus.ACTIVE;
    }
  }

  @Override
  public String managedBy() {
    return ModuleName.INGEST;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    SourceDataset other = (SourceDataset) obj;
    return Objects.equals(this.archiveDoi, other.archiveDoi) && Objects.equals(this.dataFiles, other.dataFiles)
            && Objects.equals(this.description, other.description) && Objects.equals(this.lastImportDate, other.lastImportDate)
            && Objects.equals(this.logo, other.logo) && Objects.equals(this.name, other.name) && Objects.equals(this.project, other.project)
            && Objects.equals(this.projectId, other.projectId) && this.status == other.status
            && Objects.equals(this.statusMessage, other.statusMessage);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result
            + Objects.hash(this.archiveDoi, this.dataFiles, this.description, this.lastImportDate, this.logo, this.name, this.project,
                    this.projectId, this.status, this.statusMessage);
    return result;
  }

  @Override
  public <T> boolean addItem(T t) {
    if (t instanceof Project p) {
      this.setProjectId(p.getResId());
    } else if (t instanceof SourceDatasetFile sourceDatasetFile) {
      return this.addSourceDataFile(sourceDatasetFile);
    } else {
      throw new UnsupportedOperationException("Missing implementation 'addItem' method (" + t.getClass().getSimpleName() + ")");
    }
    return true;
  }

  private boolean addSourceDataFile(SourceDatasetFile sourceDatasetFile) {
    return this.dataFiles.add(sourceDatasetFile);
  }

  @Override
  public <T> boolean removeItem(T t) {
    if (t instanceof SourceDatasetFile sourceDatasetFile) {
      return this.removeSourceDataFile(sourceDatasetFile);
    } else if (t instanceof Project) {
      this.setProjectId(null);
    } else {
      throw new UnsupportedOperationException("Missing implementation 'removeItem' method (" + t.getClass().getSimpleName() + ")");
    }
    return true;
  }

  private boolean removeSourceDataFile(SourceDatasetFile sourceDatasetFile) {
    return this.dataFiles.remove(sourceDatasetFile);
  }

  @Override
  @JsonIgnore
  public List<Class<? extends Resource>> getEmbeddedResourceTypes() {
    final List<Class<? extends Resource>> embeddedResources = new ArrayList<>();

    embeddedResources.add(Project.class);

    return embeddedResources;
  }

  @Override
  public <T> List<String> getSubResourceIds(Class<T> type) {

    List<String> subResourceIds = Collections.emptyList();

    if (type == Project.class && this.getProjectId() != null) {
      subResourceIds = CollectionTool.initList(this.getProjectId());
    } else if (type == null) {
      throw new IllegalArgumentException("type should not be null");
    } else if (!this.getEmbeddedResourceTypes().contains(type)) {
      throw new IllegalArgumentException("Class " + type.getCanonicalName() + " is not a subresource of SourceDataset");
    }

    return subResourceIds;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.SOURCE_DATASET_FILE).withRel(ResourceName.SOURCE_DATASET_FILE));
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.UPLOAD).withRel(ActionName.UPLOAD));
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));
      this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.UPLOAD_LOGO).withRel(HederaActionName.UPLOAD_LOGO));
      this.add(linkBuilder.slash(this.getResId()).slash(HederaActionName.DOWNLOAD_LOGO).withRel(HederaActionName.DOWNLOAD_LOGO));

    }
  }

  @Override
  public Specification<SourceDataset> getSearchSpecification(SearchCriteria criteria) {
    return new SourceDatasetSearchSpecification(criteria);
  }

  @Override
  public ResourceFile setNewResourceFile() {
    this.setLogo(new SourceDatasetLogo());
    return this.getLogo();
  }

  @Override
  public ResourceFile getResourceFile() {
    return this.getLogo();
  }

  @Override
  public void setResourceFile(ResourceFile resourceFile) {
    this.setLogo((SourceDatasetLogo) resourceFile);
  }

  @Override
  @JsonIgnore
  public boolean isDeletable() {
    return this.getProject().isOpen() && !this.getStatus().equals(SourceDatasetStatus.PUBLISHED);
  }

  @Override
  @JsonIgnore
  public boolean isModifiable() {
    return this.getProject().isOpen()
            && !this.getStatus().equals(SourceDatasetStatus.PRE_PUBLISHED)
            && !this.getStatus().equals(SourceDatasetStatus.PUBLISHED);
  }
}
