/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - SourceDatasetFile.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.ingest;

import static ch.hedera.HederaConstants.APPLICATION_RDF_XML_MIME_TYPE;
import static ch.hedera.HederaConstants.TEXT_CSV_MIME_TYPE;

import java.net.URI;
import java.util.Objects;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.listener.HistoryListener;
import ch.hedera.model.AbstractDatasetFile;
import ch.hedera.model.ProjectAwareResource;
import ch.hedera.rest.ModuleName;
import ch.hedera.specification.SourceDatasetFileSearchSpecification;

@Schema(description = """
        The file represented by Source Dataset.
        Source Dataset File status:
        - NOT_TRANSFORMED => Initial status
        - TO_TRANSFORM => In queue to be transformed
        - TRANSFORMING => RML conversion in-progress
        - TRANSFORMED => Transformation completed
        - TO_CHECK => In queue ti be checked
        - CHECKING => Verification in-progress
        - CHECKED => Verification done without applying the transformation
        - IN_ERROR => In-Error (see the reason in the history)
        """)
@Entity
@EntityListeners({ HistoryListener.class })
@Table(uniqueConstraints = { @UniqueConstraint(name = "sourceDatasetId_fileName_version", columnNames = { HederaConstants.DB_SOURCE_DATASET_ID,
        "fileName", "version" }) })
public class SourceDatasetFile extends AbstractDatasetFile<SourceDataset, SourceDatasetFile> implements ProjectAwareResource {

  public enum SourceDatasetFileStatus {
    NOT_TRANSFORMED, TO_TRANSFORM, TRANSFORMING, TRANSFORMED, TO_CHECK, CHECKING, CHECKED, IN_ERROR
  }

  @Schema(description = "The status of the source dataset file.")
  @Enumerated(EnumType.STRING)
  private SourceDatasetFileStatus status;

  @Schema(description = "The detailed message related to the source dataset file status.")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String statusMessage;

  @Schema(description = "The type of source dataset file.")
  @Enumerated(EnumType.STRING)
  @NotNull
  private DatasetFileType type;

  @JsonIgnore
  @NotNull
  @ManyToOne
  @JoinColumn(name = HederaConstants.DB_SOURCE_DATASET_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  private SourceDataset sourceDataset;

  @Schema(description = "Version of SourceDatasetFile.")
  @NotNull
  private String version;

  public SourceDatasetFile() {
  }

  public SourceDatasetFile(SourceDataset sourceDataset) {
    super();
    this.sourceDataset = sourceDataset;
  }

  @Override
  public void init() {
    super.init();
    if (this.status == null) {
      this.status = SourceDatasetFileStatus.NOT_TRANSFORMED;
    }
  }

  public SourceDatasetFileStatus getStatus() {
    return this.status;
  }

  public String getStatusMessage() {
    return this.statusMessage;
  }

  public SourceDataset getSourceDataset() {
    return this.sourceDataset;
  }

  public DatasetFileType getType() {
    return this.type;
  }

  public String getVersion() {
    return this.version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public void setStatus(SourceDatasetFileStatus status) {
    this.status = status;
  }

  public void setStatusMessage(String statusMessage) {
    this.statusMessage = StringTool.truncateWithEllipsis(statusMessage, SolidifyConstants.DB_LONG_STRING_LENGTH);
  }

  public void setErrorStatusWithMessage(String statusMessage) {
    this.setStatus(SourceDatasetFileStatus.IN_ERROR);
    this.setStatusMessage(statusMessage);
  }

  public void setType(DatasetFileType type) {
    this.type = type;
  }

  @Override
  public String managedBy() {
    return ModuleName.INGEST;
  }

  @Override
  public SourceDataset getDataset() {
    return this.sourceDataset;
  }

  @Override
  public void setDataset(SourceDataset sourceDataset) {
    this.sourceDataset = sourceDataset;
  }

  @Override
  public String getProjectId() {
    if (this.sourceDataset == null) {
      return null;
    }
    return this.sourceDataset.getProjectId();
  }

  @Override
  public URI getDownloadUri() {
    return this.getEffectiveSourcePath().toUri();
  }

  @Override
  public String getContentType() {
    switch (this.type) {
      case CSV -> {
        return TEXT_CSV_MIME_TYPE;
      }
      case RDF -> {
        return APPLICATION_RDF_XML_MIME_TYPE;
      }
      case XML -> {
        return MediaType.APPLICATION_XML_VALUE;
      }
      default -> {
        return null;
      }
    }
  }

  @Override
  @JsonIgnore
  public boolean isDeletable() {
    return this.getDataset().isDeletable();
  }

  @Override
  @JsonIgnore
  public boolean isModifiable() {
    return this.getDataset().isModifiable();
  }

  @JsonIgnore
  public boolean isInProgress() {
    return this.getStatus().equals(SourceDatasetFileStatus.TO_TRANSFORM)
            || this.getStatus().equals(SourceDatasetFileStatus.TRANSFORMING)
            || this.getStatus().equals(SourceDatasetFileStatus.TO_CHECK)
            || this.getStatus().equals(SourceDatasetFileStatus.CHECKING);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    SourceDatasetFile other = (SourceDatasetFile) obj;
    return Objects.equals(this.sourceDataset, other.sourceDataset) && this.status == other.status
            && Objects.equals(this.statusMessage, other.statusMessage)
            && this.type == other.type && Objects.equals(this.version, other.version);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.sourceDataset, this.status, this.statusMessage, this.type, this.version);
    return result;
  }

  @Override
  public Specification<SourceDatasetFile> getSearchSpecification(SearchCriteria criteria) {
    return new SourceDatasetFileSearchSpecification(criteria);
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("SourceDataFile [resId=").append(this.getResId())
            .append(", status=").append(this.status)
            .append(", version=").append(this.version)
            .append(", type=").append(this.type).append("]");
    return builder.toString();
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));
    this.add(linkBuilder.slash(this.getResId()).slash(ActionName.DOWNLOAD).withRel(ActionName.DOWNLOAD));
  }

}
