/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - HederaIdentifier.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.ingest;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.hedera.HederaConstants;

@Schema(description = "The hedera identifier to generate hedera ID sequence.")
@Table(name = "hederaIdentifier")
@Entity
public class HederaIdentifier {
  @NotNull
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hederaIdSeq")
  @SequenceGenerator(name = "hederaIdSeq", sequenceName = "hederaIdSeq", initialValue = HederaConstants.HEDERA_SEQUENCE_START)
  private Long hederaId;

  public Long getHederaId() {
    return this.hederaId;
  }
}
