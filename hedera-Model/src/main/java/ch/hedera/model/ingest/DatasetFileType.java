/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - DatasetFileType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.ingest;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.hedera.HederaConstants;

@Schema(description = """
        Source Dataset File tyoe:
        - CSV => Comma-Separated Values file
        - XML => Extensible Markup Language file
        - JSON => JavaScript Object Notation file
        - RDF => Resource Description Framework file
        """)

public enum DatasetFileType {
  //@formatter:off
  CSV(HederaConstants.CSV),
  XML(HederaConstants.XML),
  JSON(HederaConstants.JSON),
  RDF(HederaConstants.RDF);
  //@formatter:on

  @Schema(description = "Type of file for dataset.")
  private final String name;

  DatasetFileType(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }
}
