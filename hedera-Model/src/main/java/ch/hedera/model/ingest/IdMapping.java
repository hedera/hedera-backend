/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IdMapping.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.ingest;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "The mapping from a businnes id to an Hedera id.")
@Table(uniqueConstraints = {
        @UniqueConstraint(name = "project_researchObjectType_businessId", columnNames = { "projectShortName", "businessId",
                "researchObjectTypeName" }) })
@Entity
public class IdMapping {
  @NotNull
  @Id
  private Long hederaId;

  @Schema(description = "The business ID of the ID mapping.")
  @NotNull
  private String businessId;

  @Schema(description = "The project short name of the ID mapping.")
  @NotNull
  private String projectShortName;

  @Schema(description = "The research object type name of the ID mapping.")
  @NotNull
  private String researchObjectTypeName;

  public IdMapping() {
    // no-op
  }

  public IdMapping(Long hederaId, String projectShortName, String researchObjectTypeName, String businessId) {
    this.hederaId = hederaId;
    this.businessId = businessId;
    this.projectShortName = projectShortName;
    this.researchObjectTypeName = researchObjectTypeName;
  }

  public Long getHederaId() {
    return this.hederaId;
  }

  public void setHederaId(Long hederaId) {
    this.hederaId = hederaId;
  }

  public String getBusinessId() {
    return this.businessId;
  }

  public void setBusinessId(String businessId) {
    this.businessId = businessId;
  }

  public String getProjectShortName() {
    return this.projectShortName;
  }

  public void setProjectShortName(String projectShortName) {
    this.projectShortName = projectShortName;
  }

  public String getResearchObjectTypeName() {
    return this.researchObjectTypeName;
  }

  public void setResearchObjectTypeName(String researchObjectTypeName) {
    this.researchObjectTypeName = researchObjectTypeName;
  }
}
