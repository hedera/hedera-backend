/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ResearchDataFile.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.ingest;

import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.listener.HistoryListener;
import ch.hedera.listener.ProjectDataListener;
import ch.hedera.model.ProjectAwareResource;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.rest.ModuleName;

@Schema(description = """
        A Research data file is the binary file linked in the project.
        Research data file status:
        - CREATED => The research data file entry is created
        - UPLOADED => The research data file is upload
        - PROCESSING => The research data file processing is in-progress
        - STORED => The research data file is stored in the project storage (file system or S3)
        - REFERENCED = The RDF metadata of the research data file are generated
        - COMPLETED => The research data file is ready to be accessed via search engine or the SPARQL queries
        - IN_ERROR => The research data file is in-error (see the reason in the history)
        - CHANGE_RELATIVE_LOCATION => The research data file 'change relative location' operation is in-progress
        """)
@EntityListeners({ HistoryListener.class, ProjectDataListener.class })
@Entity
public class ResearchDataFile extends ResourceNormalized implements RemoteResourceContainer, ProjectAwareResource {

  public enum ResearchDataFileFileStatus {
    CREATED, UPLOADED, PROCESSING, STORED, REFERENCED, COMPLETED, IN_ERROR, CHANGE_RELATIVE_LOCATION
  }

  @Schema(description = "The type of location where is stored.")
  @NotNull
  @Enumerated(EnumType.STRING)
  private DataAccessibilityType accessibleFrom;

  @Schema(description = "The type of researchObject.")
  @Transient
  private ResearchObjectType researchObjectType;

  @Schema(description = "The researchObjectType identifier of the ResearchDataFile.")
  @NotNull
  @Size(min = 1)
  @Column(name = HederaConstants.DB_RESEARCH_OBJECT_TYPE_ID, length = DB_ID_LENGTH)
  private String researchObjectTypeId;

  @Schema(description = "The project of the researchObject.")
  @Transient
  private Project project;

  @Schema(description = "The project identifier of the ResearchDataFile.")
  @NotNull
  @Size(min = 1)
  @Column(name = HederaConstants.DB_PROJECT_ID, length = DB_ID_LENGTH)
  private String projectId;

  @Schema(description = "The file name.")
  @NotNull
  private String fileName;

  @Schema(description = "The status of the research data file.")
  @Enumerated(EnumType.STRING)
  private ResearchDataFileFileStatus status;

  @Schema(description = "The detailed message related to the research data file status.")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String statusMessage;

  @Schema(description = "The research data file size in bytes.")
  private Long fileSize;

  @Schema(description = "Path in the file system where the research data file is stored.")
  @NotNull
  @Column(length = HederaConstants.DB_LONG_STRING_LENGTH)
  @JsonIgnore
  private URI sourcePath;

  @Size(min = 1, max = HederaConstants.DB_LONG_STRING_LENGTH)
  @Pattern(regexp = "(^\\/$)|(^\\/.*(?<!\\/)$)")
  private String relativeLocation;

  @Schema(description = "The mime-type of the research data file.")
  private String mimeType;

  @Schema(description = "The metadata of the research data file.")
  @Column(length = HederaConstants.DB_LARGE_STRING_LENGTH)
  private Map<String, Object> metadata;

  @Schema(description = "The URI of the research data file.")
  @Column(length = HederaConstants.DB_LONG_STRING_LENGTH)
  private URI uri;

  @Schema(description = "The RDF metadata of the research data file.")
  @Column(length = HederaConstants.DB_LARGE_STRING_LENGTH)
  private String rdfMetadata;

  public DataAccessibilityType getAccessibleFrom() {
    return this.accessibleFrom;
  }

  public String getFileName() {
    return this.fileName;
  }

  public Long getFileSize() {
    return this.fileSize;
  }

  @Schema(description = "The The research data file size in human-readable format.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = Access.READ_ONLY)
  public String getSmartFileSize() {
    if (this.fileSize == null) {
      return HederaConstants.NO_SIZE;
    }
    return StringTool.formatSmartSize(this.fileSize);
  }

  public URI getSourcePath() {
    return this.sourcePath;
  }

  public String getMimeType() {
    return this.mimeType;
  }

  public ResearchObjectType getResearchObjectType() {
    return this.researchObjectType;
  }

  public String getResearchObjectTypeId() {
    return this.researchObjectTypeId;
  }

  public Project getProject() {
    return this.project;
  }

  @Override
  public String getProjectId() {
    return this.projectId;
  }

  /**
   * Get the relative location wich contain a leading slash.
   *
   * @return
   */
  public String getRelativeLocation() {
    return this.relativeLocation;
  }

  /**
   * Get the relative path by combining the relative location and the filename, without a leading slash.
   */
  @JsonIgnore
  public String getRelativePath() {
    if (this.getRelativeLocation().equals("/")) {
      return this.getFileName();
    } else {
      return this.getRelativeLocation().substring(1) + "/" + this.getFileName();
    }
  }

  public Map<String, Object> getMetadata() {
    return this.metadata;
  }

  public String getRdfMetadata() {
    return this.rdfMetadata;
  }

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public String getFullFileName() {
    if (StringTool.isNullOrEmpty(this.getRelativeLocation()) || this.getRelativeLocation().equals("/")) {
      return "/" + this.getFileName();
    }
    return this.getRelativeLocation() + "/" + this.getFileName();
  }

  public void setRelativeLocation(String relativeLocation) {
    this.relativeLocation = relativeLocation;
  }

  public void setAccessibleFrom(DataAccessibilityType accessibleFrom) {
    this.accessibleFrom = accessibleFrom;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public void setFileSize(Long fileSize) {
    this.fileSize = fileSize;
  }

  public void setSourcePath(URI sourcePath) {
    this.sourcePath = sourcePath;
  }

  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  public void setResearchObjectType(ResearchObjectType researchObjectType) {
    this.researchObjectType = researchObjectType;
  }

  public void setResearchObjectTypeId(String researchObjectTypeId) {
    this.researchObjectTypeId = researchObjectTypeId;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  @JsonProperty(access = Access.READ_ONLY)
  public String getHederaId() {
    if (this.getUri() == null) {
      return null;
    }
    String uriStr = this.getUri().getPath();
    return uriStr.substring(uriStr.lastIndexOf("/") + 1);
  }

  public URI getUri() {
    return this.uri;
  }

  public void setUri(URI uri) {
    this.uri = uri;
  }

  public static String checkRelativeLocation(String relativeLocation) {
    if (StringTool.isNullOrEmpty(relativeLocation) || relativeLocation.equals("/")) {
      return "/";
    }
    // Replace multiple /
    relativeLocation = relativeLocation.replaceAll("/+", "/");

    // Check starting / & add it if needed
    if (!relativeLocation.startsWith("/")) {
      relativeLocation = "/" + relativeLocation;
    }
    // Check ending / & remove it if needed
    while (relativeLocation.endsWith("/")) {
      relativeLocation = relativeLocation.substring(0, relativeLocation.length() - 1);
    }
    return relativeLocation;
  }

  public void setStatus(ResearchDataFileFileStatus status) {
    this.status = status;
  }

  public void setMetadata(Map<String, Object> metadata) {
    this.metadata = metadata;
  }

  public void setRdfMetadata(String rdfMetadata) {
    this.rdfMetadata = rdfMetadata;
  }

  public ResearchDataFileFileStatus getStatus() {
    return this.status;
  }

  public String getStatusMessage() {
    return this.statusMessage;
  }

  public void setStatusMessage(String statusMessage) {
    this.statusMessage = StringTool.truncateWithEllipsis(statusMessage, SolidifyConstants.DB_LONG_STRING_LENGTH);
  }

  public void setErrorStatusWithMessage(String statusMessage) {
    this.setStatus(ResearchDataFileFileStatus.IN_ERROR);
    this.setStatusMessage(statusMessage);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    ResearchDataFile that = (ResearchDataFile) o;
    return this.accessibleFrom == that.accessibleFrom && Objects.equals(this.researchObjectType, that.researchObjectType)
            && Objects.equals(this.researchObjectTypeId, that.researchObjectTypeId) && Objects.equals(this.project, that.project)
            && Objects.equals(this.projectId, that.projectId) && Objects.equals(this.fileName, that.fileName)
            && Objects.equals(this.fileSize, that.fileSize) && Objects.equals(this.sourcePath, that.sourcePath)
            && Objects.equals(this.relativeLocation, that.relativeLocation) && Objects.equals(this.mimeType, that.mimeType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.accessibleFrom, this.researchObjectType, this.researchObjectTypeId, this.project, this.projectId,
            this.fileName, this.fileSize,
            this.sourcePath, this.relativeLocation, this.mimeType);
  }

  @Override
  public void init() {
    if (this.fileSize == null) {
      this.fileSize = 0L;
    }
    if (this.relativeLocation == null) {
      this.relativeLocation = "/";
    } else {
      this.setRelativeLocation(ResearchDataFile.checkRelativeLocation(this.getRelativeLocation()));
    }
    if (this.status == null) {
      this.status = ResearchDataFileFileStatus.CREATED;
    }
  }

  @Override
  public String managedBy() {
    return ModuleName.INGEST;
  }

  @Override
  public <T> boolean addItem(T t) {
    if (t instanceof Project p) {
      this.setProjectId(p.getResId());
    } else if (t instanceof ResearchObjectType rot) {
      this.setResearchObjectTypeId(rot.getResId());
    } else {
      throw new UnsupportedOperationException("Missing implementation 'addItem' method (" + t.getClass().getSimpleName() + ")");
    }
    return true;
  }

  @Override
  @JsonIgnore
  public List<Class<? extends Resource>> getEmbeddedResourceTypes() {
    final List<Class<? extends Resource>> embeddedResources = new ArrayList<>();

    embeddedResources.add(ResearchObjectType.class);
    embeddedResources.add(Project.class);

    return embeddedResources;
  }

  @Override
  public <T> List<String> getSubResourceIds(Class<T> type) {
    List<String> subResourceIds = Collections.emptyList();

    if (type == ResearchObjectType.class && this.getResearchObjectTypeId() != null) {
      subResourceIds = CollectionTool.initList(this.getResearchObjectTypeId());
    }
    if (type == Project.class && this.getProjectId() != null) {
      subResourceIds = CollectionTool.initList(this.getProjectId());
    } else if (type == null) {
      throw new IllegalArgumentException("type should not be null");
    } else if (!this.getEmbeddedResourceTypes().contains(type)) {
      throw new IllegalArgumentException("Class " + type.getCanonicalName() + " is not a subresource of ResearchDataFile");
    }

    return subResourceIds;
  }

  @Override
  public <T> boolean removeItem(T t) {
    if (t instanceof Project) {
      this.setProjectId(null);
    } else if (t instanceof ResearchObjectType) {
      this.setResearchObjectTypeId(null);
    } else {
      throw new UnsupportedOperationException("Missing implementation 'removeItem' method (" + t.getClass().getSimpleName() + ")");
    }
    return true;
  }

  @Override
  public URI getDownloadUri() {
    return this.getSourcePath();
  }

  @Override
  public String getContentType() {
    return this.getMimeType();
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));
    this.add(linkBuilder.slash(this.getResId()).slash(ActionName.DOWNLOAD).withRel(ActionName.DOWNLOAD));
  }

  @JsonIgnore
  public boolean isInProgress() {
    return this.status != ResearchDataFileFileStatus.CREATED
            && this.status != ResearchDataFileFileStatus.COMPLETED
            && this.status != ResearchDataFileFileStatus.IN_ERROR;
  }

  @Override
  @JsonIgnore
  public boolean isDeletable() {
    return this.getProject().isOpen() && !this.isInProgress();
  }

  @Override
  @JsonIgnore
  public boolean isModifiable() {
    return this.getProject().isOpen() && !this.isInProgress();
  }

  @JsonIgnore
  public boolean isImage() {
    return this.mimeType != null && this.mimeType.startsWith("image");
  }

  @JsonIgnore
  public boolean isXmlDocument() {
    return this.mimeType != null && this.mimeType.startsWith(MediaType.APPLICATION_XML_VALUE);
  }

  @JsonIgnore
  public boolean isPdfDocument() {
    return this.mimeType != null && this.mimeType.startsWith(MediaType.APPLICATION_PDF_VALUE);
  }

  @JsonIgnore
  public boolean isDocument() {
    return this.mimeType != null
            && (this.mimeType.equals(MediaType.APPLICATION_PDF_VALUE)
                    || this.mimeType.equals(HederaConstants.APPLICATION_RTF_MIME_TYPE)
                    || this.mimeType.equals(HederaConstants.APPLICATION_MS_WORD_MIME_TYPE)
                    || this.mimeType.startsWith(HederaConstants.APPLICATION_OPEN_DOC_MIME_TYPE));
  }

}
