/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - RdfDatasetFile.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.ingest;

import java.io.IOException;
import java.net.URI;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PreRemove;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.listener.HistoryListener;
import ch.hedera.listener.ProjectDataListener;
import ch.hedera.model.ProjectAwareResource;
import ch.hedera.model.RdfFormat;
import ch.hedera.rest.ModuleName;
import ch.hedera.util.RdfTool;

@Schema(description = """
        The file representing the output of a RML mapping.
        RDF Dataset File status:
        - NOT_IMPORTED => Initial status
        - TO_IMPORT => In queue to be imported
        - IMPORTING => RDF metadata storing in-progress (triple store)
        - INDEXING => Index metadata storing in-progress (index)
        - TO_REPLACE => In queue to be replaced
        - REPLACING => Replacement in-progress (purge metadat in triple store & index)
        - IMPORTED => RDF metadata imported
        - TO_REIMPORT => In queue to be reimport
        - IN_ERROR => In-Error (see the reason in the history)
        - TO_REMOVE => In queue to be removed
        - REMOVING => Deletion in-progress
        """)
@EntityListeners({ HistoryListener.class, ProjectDataListener.class })
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "sourceDatasetFile_fileName", columnNames = { HederaConstants.DB_SOURCE_DATASET_FILE_ID,
        "fileName" }) })
public class RdfDatasetFile extends ResourceNormalized implements ProjectAwareResource {

  public enum RdfDatasetFileStatus {
    NOT_IMPORTED, TO_IMPORT, IMPORTING, INDEXING, TO_REPLACE, REPLACING, IMPORTED, TO_REIMPORT, IN_ERROR, TO_REMOVE, REMOVING
  }

  @Schema(description = "The status of the RDF dataset file.")
  @Enumerated(EnumType.STRING)
  private RdfDatasetFileStatus statusImport;

  @Schema(description = "The detailed message related to the RDF dataset file status.")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String statusMessage;

  @NotNull
  @ManyToOne
  @JoinColumn(name = HederaConstants.DB_SOURCE_DATASET_FILE_ID, referencedColumnName = HederaConstants.DB_RES_ID)
  @Schema(description = "The source dataset file used to generate this RDF dataset file")
  private SourceDatasetFile sourceDatasetFile;

  @NotNull
  @Schema(description = "The RML file id used to generate this RDF dataset file")
  private String rmlId;

  @Schema(description = "The Triplestore dataset on which the RDF dataset file is deployer")
  private String triplestoreDatasetId;

  @Schema(description = "Path in the file system where the file is stored.")
  @NotNull
  @Column(length = HederaConstants.DB_LARGE_STRING_LENGTH)
  @JsonIgnore
  private URI filePath;

  @Schema(description = "The file name.")
  @NotNull
  private String fileName;

  @Schema(description = "The file size in bytes.")
  private Long fileSize;

  @Schema(description = "The format of the RDF dataset file")
  @Enumerated(EnumType.STRING)
  @NotNull
  private RdfFormat rdfFormat;

  public SourceDatasetFile getSourceDatasetFile() {
    return this.sourceDatasetFile;
  }

  public String getRmlId() {
    return this.rmlId;
  }

  public String getTriplestoreDatasetId() {
    return this.triplestoreDatasetId;
  }

  public URI getFilePath() {
    return this.filePath;
  }

  public String getFileName() {
    return this.fileName;
  }

  public Long getFileSize() {
    return this.fileSize;
  }

  public RdfFormat getRdfFormat() {
    return this.rdfFormat;
  }

  public RdfDatasetFileStatus getStatusImport() {
    return this.statusImport;
  }

  public String getStatusMessage() {
    return this.statusMessage;
  }

  public void setStatusMessage(String statusMessage) {
    this.statusMessage = StringTool.truncateWithEllipsis(statusMessage, SolidifyConstants.DB_LONG_STRING_LENGTH);
  }

  public void setErrorStatusWithMessage(String statusMessage) {
    this.setStatusImport(RdfDatasetFileStatus.IN_ERROR);
    this.setStatusMessage(statusMessage);
  }

  public void setStatusImport(RdfDatasetFileStatus status) {
    this.statusImport = status;
  }

  public void setSourceDatasetFile(SourceDatasetFile sourceDatasetFile) {
    this.sourceDatasetFile = sourceDatasetFile;
  }

  public void setRmlId(String rmlId) {
    this.rmlId = rmlId;
  }

  public void setTriplestoreDatasetId(String triplestoreDatasetId) {
    this.triplestoreDatasetId = triplestoreDatasetId;
  }

  public void setFilePath(URI filePath) {
    this.filePath = filePath;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public void setFileSize(Long fileSize) {
    this.fileSize = fileSize;
  }

  public void setRdfFormat(RdfFormat rdfFormat) {
    this.rdfFormat = rdfFormat;
  }

  @Override
  public URI getDownloadUri() {
    return this.filePath;
  }

  @Override
  public String getContentType() {
    return RdfTool.getContentType(this.rdfFormat);
  }

  public String getExtension() {
    return RdfTool.getExtension(this.rdfFormat);
  }

  @Override
  public void init() {
    if (this.statusImport == null) {
      this.statusImport = RdfDatasetFileStatus.NOT_IMPORTED;
    }
  }

  @Override
  @JsonIgnore
  public boolean isDeletable() {
    return this.getSourceDatasetFile().getDataset().isDeletable()
            && (this.getStatusImport() == RdfDatasetFileStatus.IN_ERROR
                    || this.getStatusImport() == RdfDatasetFileStatus.NOT_IMPORTED);
  }

  @Override
  @JsonIgnore
  public boolean isModifiable() {
    return this.getSourceDatasetFile().getDataset().isModifiable();
  }

  @Override
  public String managedBy() {
    return ModuleName.INGEST;
  }

  @Override
  public String toString() {
    return "RdfDataFile [resId=" + this.getResId()
            + ", sourceDatasetFile=" + this.sourceDatasetFile.getResId()
            + ", rmlId=" + this.rmlId
            + ", status=" + this.statusImport
            + ", sourcePath" + this.filePath
            + ", fileName=" + this.fileName
            + ", fileSize=" + this.fileSize + "]";
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    RdfDatasetFile that = (RdfDatasetFile) o;
    return this.statusImport == that.statusImport && Objects.equals(this.sourceDatasetFile, that.sourceDatasetFile) && Objects.equals(
            this.rmlId, that.rmlId) && Objects.equals(this.triplestoreDatasetId, that.triplestoreDatasetId) && Objects.equals(this.filePath,
                    that.filePath)
            && Objects.equals(this.fileName, that.fileName) && Objects.equals(this.fileSize, that.fileSize)
            && Objects.equals(this.rdfFormat, that.rdfFormat);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.statusImport, this.sourceDatasetFile, this.rmlId, this.triplestoreDatasetId, this.filePath,
            this.fileName, this.fileSize, this.rdfFormat);
  }

  @Override
  public String getProjectId() {
    return this.sourceDatasetFile.getSourceDataset().getProjectId();
  }

  @PreRemove
  public void removeFile() {
    try {
      FileTool.deleteFile(this.getFilePath());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Unable to delete rdfDatasetFile", e);
    }
  }

  @JsonIgnore
  public boolean isInProgress() {
    return this.getStatusImport().equals(RdfDatasetFileStatus.TO_IMPORT)
            || this.getStatusImport().equals(RdfDatasetFileStatus.TO_REIMPORT)
            || this.getStatusImport().equals(RdfDatasetFileStatus.IMPORTING)
            || this.getStatusImport().equals(RdfDatasetFileStatus.INDEXING)
            || this.getStatusImport().equals(RdfDatasetFileStatus.TO_REPLACE)
            || this.getStatusImport().equals(RdfDatasetFileStatus.REPLACING)
            || this.getStatusImport().equals(RdfDatasetFileStatus.TO_REMOVE)
            || this.getStatusImport().equals(RdfDatasetFileStatus.REMOVING);
  }
}
