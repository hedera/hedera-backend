/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ScheduledTask.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.schedule;

import java.time.OffsetDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.scheduling.support.CronExpression;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.scheduler.ScheduledTaskInterface;
import ch.unige.solidify.util.StringTool;

import ch.hedera.rest.ModuleName;

@Schema(description = "The scheduled task.")
@Entity
public class ScheduledTask extends ResourceNormalized implements ScheduledTaskInterface {

  @Schema(description = "The name of the scheduled task.")
  @NotNull
  @Size(min = 1)
  @Column(unique = true)
  private String name;

  @Schema(description = "The type of the scheduled task.")
  @NotNull
  @Enumerated(EnumType.STRING)
  private TaskType taskType;

  /**
   * min length is with "* * * * * *" which has 11 chars
   */
  @Schema(description = "The CRON expression (Seconds Minutes Hours Day-of-month Month Year, 11 characters) of the scheduled task.")
  @NotNull
  @Size(min = 11)
  private String cronExpression;

  @Schema(description = "The last execution start of the scheduled task.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  private OffsetDateTime lastExecutionStart;

  @Schema(description = "The last execution end of the scheduled task.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  private OffsetDateTime lastExecutionEnd;

  @Schema(description = "If the scheduled task is enable.")
  private Boolean enabled;

  /****************************************************************************************************/

  @Override
  public void init() {
    if (this.enabled == null) {
      this.enabled = true;
    }
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  /****************************************************************************************************/

  @Override
  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public TaskType getTaskType() {
    return this.taskType;
  }

  public void setTaskType(TaskType taskType) {
    this.taskType = taskType;
  }

  @Override
  public String getCronExpression() {
    return this.cronExpression;
  }

  public void setCronExpression(String cronExpression) {
    this.cronExpression = cronExpression;
  }

  @Override
  public OffsetDateTime getLastExecutionStart() {
    return this.lastExecutionStart;
  }

  @Override
  public void setLastExecutionStart(OffsetDateTime lastExecutionStart) {
    this.lastExecutionStart = lastExecutionStart;
  }

  @Override
  public OffsetDateTime getLastExecutionEnd() {
    return this.lastExecutionEnd;
  }

  @Override
  public void setLastExecutionEnd(OffsetDateTime lastExecutionEnd) {
    this.lastExecutionEnd = lastExecutionEnd;
  }

  @Override
  public Boolean isEnabled() {
    return this.enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  /****************************************************************************************************/

  public OffsetDateTime getNextExecution() {
    if (StringTool.isNullOrEmpty(this.cronExpression) || !this.enabled) {
      return null;
    }

    CronExpression cronTrigger = CronExpression.parse(this.cronExpression);
    return cronTrigger.next(OffsetDateTime.now());
  }
}
