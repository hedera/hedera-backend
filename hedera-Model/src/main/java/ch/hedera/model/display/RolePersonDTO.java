/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - RolePersonDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.display;

import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.ProjectPersonRole;

import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.rest.ResourceBase;
import ch.unige.solidify.util.ReflectionTool;

public class RolePersonDTO extends Person implements JoinResourceContainer<ProjectPersonRole> {
  ProjectPersonRole joinResource;

  public RolePersonDTO(ProjectPersonRole projectPersonRole) {
    ReflectionTool.copyFields(this, projectPersonRole.getPerson(), ResourceBase.class);
    this.add(projectPersonRole.getPerson().getLinks());
    this.joinResource = projectPersonRole;
  }

  @Override
  public ProjectPersonRole getJoinResource() {
    return this.joinResource;
  }
}
