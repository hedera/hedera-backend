/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - PersonRoleListDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.display;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.rest.ResourceBase;
import ch.unige.solidify.util.ReflectionTool;

import ch.hedera.model.settings.Person;

public class PersonRoleListDTO extends Person implements Relation3TiersChildDTO {

  @JsonProperty("roles")
  List<ProjectPersonRoleDTO> grandChildren;

  public PersonRoleListDTO() {
  }

  public PersonRoleListDTO(Person person, List<ProjectPersonRoleDTO> grandChildren) {
    ReflectionTool.copyFields(this, person, ResourceBase.class);
    this.add(person.getLinks());
    this.grandChildren = grandChildren;
  }

  @Override
  public List<ProjectPersonRoleDTO> getGrandChildren() {
    return this.grandChildren;
  }
}
