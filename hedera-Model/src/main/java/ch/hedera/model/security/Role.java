/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - Role.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.model.security;

import static ch.hedera.HederaRestFields.LEVEL_FIELD;
import static ch.hedera.HederaRestFields.RES_ID_FIELD;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.rest.ResourceNormalized;

import ch.hedera.model.settings.ProjectPersonRole;
import ch.hedera.rest.ModuleName;

@Schema(description = "A role defines a user’s access level on the platform, giving different permissions for performing actions on it.")
@Entity
public class Role extends ResourceNormalized {

  public static final Role MANAGER = new Role("Manager", 10);

  public static final Role CREATOR = new Role("Creator", 20);
  public static final Role VISITOR = new Role("Visitor", 30);

  public static final String MANAGER_ID = "MANAGER";
  public static final String CREATOR_ID = "CREATOR";
  public static final String VISITOR_ID = "VISITOR";

  private static final List<Role> roleList = Arrays.asList(MANAGER, CREATOR, VISITOR);

  public static final String DEFAULT_ROLE_ID = Role.VISITOR_ID;

  @Schema(description = "The name of the role.")
  @NotNull
  @Column(unique = true)
  private String name;

  @Schema(description = "The level of the role.")
  @NotNull
  @Column(unique = true)
  private int level;

  @JsonIgnore
  @OneToMany(mappedBy = ProjectPersonRole.PATH_TO_ROLE, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  private List<ProjectPersonRole> projectPersons = new ArrayList<>();

  public Role() {
    // no-arg constructor
  }

  public Role(String name, int level) {
    this.setResId(name.toUpperCase());
    this.name = name;
    this.level = level;
  }

  public String getName() {
    return this.name;
  }

  public int getLevel() {
    return this.level;
  }

  public List<ProjectPersonRole> getProjectPersons() {
    return this.projectPersons;
  }

  public static List<Role> getRoleList() {
    return roleList;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public List<String> getNonUpdatableFields() {
    return Arrays.asList(RES_ID_FIELD, LEVEL_FIELD);
  }

  @Override
  public String toString() {
    return "Role " + this.name;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Role))
      return false;
    final Role role = (Role) o;
    return this.getResId().equals(role.getResId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.name, this.level, this.projectPersons);
  }
}
