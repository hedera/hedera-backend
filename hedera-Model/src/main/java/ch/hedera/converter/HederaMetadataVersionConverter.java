/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - HederaMetadataVersionConverter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaMetadataVersion;

@Converter(autoApply = true)
public class HederaMetadataVersionConverter implements AttributeConverter<HederaMetadataVersion, String> {

  @Override
  public String convertToDatabaseColumn(HederaMetadataVersion version) {
    return (version == null) ? HederaMetadataVersion.getDefaultVersion().getVersion() : version.getVersion();
  }

  @Override
  public HederaMetadataVersion convertToEntityAttribute(String version) {
    if (StringTool.isNullOrEmpty(version)) {
      return HederaMetadataVersion.V1_0;
    }
    return HederaMetadataVersion.fromVersion(version);
  }
}
