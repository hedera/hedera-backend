/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IIIFCollectionConverter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.JSONTool;
import ch.unige.solidify.util.StringTool;

import ch.hedera.model.iiif.IIIFCollection;

@Converter(autoApply = true)
public class IIIFCollectionConverter implements AttributeConverter<IIIFCollection, String> {

  @Override
  public String convertToDatabaseColumn(IIIFCollection manifest) {
    if (manifest == null) {
      return null;
    }
    return JSONTool.convert2JsonString(manifest);
  }

  @Override
  public IIIFCollection convertToEntityAttribute(String manifest) {
    if (StringTool.isNullOrEmpty(manifest)) {
      return null;
    }
    try {
      return new ObjectMapper().readValue(manifest, IIIFCollection.class);
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("Cannot read IIIF Collection from DB", e);
    }
  }
}
