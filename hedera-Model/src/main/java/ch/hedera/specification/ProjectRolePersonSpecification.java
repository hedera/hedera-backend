/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ProjectRolePersonSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.specification;

import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.Join3TiersSpecification;

import ch.hedera.model.security.Role;
import ch.hedera.model.settings.ProjectPersonRole;

public class ProjectRolePersonSpecification extends Join3TiersSpecification<ProjectPersonRole> {
  private static final long serialVersionUID = 1L;

  public ProjectRolePersonSpecification(ProjectPersonRole joinCriteria) {
    super(joinCriteria, ProjectPersonRole.PATH_TO_PROJECT, ProjectPersonRole.PATH_TO_ROLE,
            ProjectPersonRole.PATH_TO_PERSON);
  }

  @Override
  protected String getParentId() {
    return this.joinCriteria.getProject().getResId();
  }

  @Override
  protected String getChildId() {
    return this.joinCriteria.getRole().getResId();
  }

  @Override
  protected String getGrandChildId() {
    return this.joinCriteria.getPerson().getResId();
  }

  @Override
  protected void completeJoinPredicatesList(Root<ProjectPersonRole> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
  }

  /**
   * Filter on structure
   *
   * @param root
   * @param query
   * @param builder
   * @param predicatesList
   */
  @Override
  protected void completeChildPredicatesList(Root<ProjectPersonRole> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {

    Role role = this.joinCriteria.getRole();
    Path<?> rolePath = this.getChildPath(root);

    if (role.getName() != null) {
      predicatesList.add(builder.like(rolePath.get("name"), "%" + role.getName() + "%"));
    }
  }

}
