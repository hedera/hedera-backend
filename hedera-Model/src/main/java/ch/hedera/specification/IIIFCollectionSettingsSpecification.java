/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IIIFCollectionSettingsSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.specification;

import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.hedera.model.settings.IIIFCollectionSettings;

public class IIIFCollectionSettingsSpecification extends SolidifySpecification<IIIFCollectionSettings> {
  private static final long serialVersionUID = -3771164998238546934L;

  public IIIFCollectionSettingsSpecification(IIIFCollectionSettings criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<IIIFCollectionSettings> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getName() != null) {
      predicatesList.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }
    if (this.criteria.getDescription() != null) {
      predicatesList.add(builder.like(root.get("description"), "%" + this.criteria.getDescription() + "%"));
    }
    if (this.criteria.getIiifCollectionSparqlQuery() != null) {
      predicatesList.add(builder.like(root.get("iiifCollectionSparqlQuery"), "%" + this.criteria.getIiifCollectionSparqlQuery() + "%"));
    }
    if (this.criteria.getProject() != null && this.criteria.getProject().getResId() != null) {
      predicatesList.add(builder.equal(root.get("project").get("resId"), this.criteria.getProject().getResId()));
    }
  }
}
