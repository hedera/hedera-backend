/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ResearchObjectTypeSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.hedera.model.settings.ResearchObjectType;

public class ResearchObjectTypeSpecification extends SolidifySpecification<ResearchObjectType> {

  @Serial
  private static final long serialVersionUID = 4469694081114429721L;

  public ResearchObjectTypeSpecification(ResearchObjectType criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<ResearchObjectType> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getName() != null) {
      predicatesList.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }
    if (this.criteria.getDescription() != null) {
      predicatesList.add(builder.like(root.get("description"), "%" + this.criteria.getDescription() + "%"));
    }
    if (this.criteria.getRdfType() != null) {
      predicatesList.add(builder.like(root.get("rdfType"), "%" + this.criteria.getRdfType() + "%"));
    }
    if (this.criteria.getSparqlListQuery() != null) {
      predicatesList.add(builder.like(root.get("sparqlListQuery"), "%" + this.criteria.getSparqlListQuery() + "%"));
    }
    if (this.criteria.getSparqlDetailQuery() != null) {
      predicatesList.add(builder.like(root.get("sparqlDetailQuery"), "%" + this.criteria.getSparqlDetailQuery() + "%"));
    }

    if (this.criteria.getOntology() != null) {
      predicatesList.add(builder.like(root.get("ontology").get("resId"), this.criteria.getOntology().getResId()));
    }
  }
}
