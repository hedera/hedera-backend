/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ResearchDataFileSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.specification;

import static ch.hedera.HederaConstants.ACCESSIBLE_FROM_FIELD;
import static ch.hedera.HederaConstants.FILE_NAME;
import static ch.hedera.HederaConstants.PROJECT_ID_FIELD;
import static ch.hedera.HederaConstants.RELATIVE_LOCATION_PARAM;
import static ch.hedera.HederaConstants.RESEARCH_OBJECT_TYPE_ID_FIELD;
import static ch.hedera.HederaConstants.STATUS;
import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.hedera.model.ingest.ResearchDataFile;

import ch.unige.solidify.specification.SolidifySpecification;

public class ResearchDataFileSpecification extends SolidifySpecification<ResearchDataFile> {
  @Serial
  private static final long serialVersionUID = 6569804575101100721L;

  public ResearchDataFileSpecification(ResearchDataFile criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<ResearchDataFile> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getFileName() != null) {
      predicatesList.add(builder.like(root.get(FILE_NAME), "%" + this.criteria.getFileName() + "%"));
    }
    if (this.criteria.getMimeType() != null) {
      predicatesList.add(builder.equal(root.get(MIME_TYPE_PARAM), this.criteria.getMimeType()));
    }
    if (this.criteria.getResearchObjectTypeId() != null) {
      predicatesList.add(builder.equal(root.get(RESEARCH_OBJECT_TYPE_ID_FIELD), this.criteria.getResearchObjectTypeId()));
    }
    if (this.criteria.getProjectId() != null) {
      predicatesList.add(builder.equal(root.get(PROJECT_ID_FIELD), this.criteria.getProjectId()));
    }
    if (this.criteria.getAccessibleFrom() != null) {
      predicatesList.add(builder.equal(root.get(ACCESSIBLE_FROM_FIELD), this.criteria.getAccessibleFrom()));
    }
    if (this.criteria.getStatus() != null) {
      predicatesList.add(builder.equal(root.get(STATUS), this.criteria.getStatus()));
    }
    if (this.criteria.getRelativeLocation() != null) {
      predicatesList.add(builder.equal(root.get(RELATIVE_LOCATION_PARAM), this.criteria.getRelativeLocation()));
    }
  }
}
