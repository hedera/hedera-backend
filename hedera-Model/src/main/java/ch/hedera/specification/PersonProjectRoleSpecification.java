/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - PersonProjectRoleSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.Join3TiersSpecification;

import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ProjectPersonRole;

public class PersonProjectRoleSpecification extends Join3TiersSpecification<ProjectPersonRole> {

  @Serial
  private static final long serialVersionUID = 1L;

  public PersonProjectRoleSpecification(ProjectPersonRole joinCriteria) {
    super(joinCriteria, ProjectPersonRole.PATH_TO_PERSON, ProjectPersonRole.PATH_TO_PROJECT,
            ProjectPersonRole.PATH_TO_ROLE);
  }

  @Override
  protected String getParentId() {
    return this.joinCriteria.getPerson().getResId();
  }

  @Override
  protected String getChildId() {
    return this.joinCriteria.getProject().getResId();
  }

  @Override
  protected String getGrandChildId() {
    return this.joinCriteria.getRole().getResId();
  }

  @Override
  protected void completeJoinPredicatesList(Root<ProjectPersonRole> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    // do nothing, validation rights have no property in join table
  }

  /**
   * Filter on structure
   */
  @Override
  protected void completeChildPredicatesList(Root<ProjectPersonRole> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {

    Project project = this.joinCriteria.getProject();
    Path<?> projectPath = this.getChildPath(root);

    if (project.getName() != null) {
      predicatesList.add(builder.like(projectPath.get("name"), "%" + project.getName() + "%"));
    }
    if (project.getDescription() != null) {
      predicatesList.add(builder.like(projectPath.get("description"), "%" + project.getDescription() + "%"));
    }
  }

}
