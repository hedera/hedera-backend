/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - RdfDatasetFileSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.specification;

import static ch.hedera.HederaConstants.FILE_NAME;
import static ch.hedera.HederaConstants.PROJECT_ID_FIELD;
import static ch.hedera.HederaConstants.RDF_FORMAT;
import static ch.hedera.HederaConstants.RML_ID;
import static ch.hedera.HederaConstants.SOURCE_DATASET;
import static ch.hedera.HederaConstants.SOURCE_DATASET_FILE;
import static ch.hedera.HederaConstants.STATUS_IMPORT;
import static ch.unige.solidify.SolidifyConstants.RES_ID_FIELD;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.hedera.model.ingest.RdfDatasetFile;

public class RdfDatasetFileSpecification extends SolidifySpecification<RdfDatasetFile> {

  private String projectId;
  private String sourceDatasetId;
  private String sourceDatasetFileId;

  @Serial
  private static final long serialVersionUID = -1868519673185881421L;

  public RdfDatasetFileSpecification(RdfDatasetFile criteria) {
    super(criteria);
  }

  @Override
  public Predicate toPredicate(Root<RdfDatasetFile> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    final List<Predicate> listPredicate = new ArrayList<>();
    if (this.projectId != null) {
      listPredicate.add(builder.equal(root.get(SOURCE_DATASET_FILE).get(SOURCE_DATASET).get(PROJECT_ID_FIELD), this.projectId));
    }
    if (this.sourceDatasetId != null) {
      listPredicate.add(builder.equal(root.get(SOURCE_DATASET_FILE).get(SOURCE_DATASET).get(RES_ID_FIELD), this.sourceDatasetId));
    }
    if (this.sourceDatasetFileId != null) {
      listPredicate.add(builder.equal(root.get(SOURCE_DATASET_FILE).get(RES_ID_FIELD), this.sourceDatasetFileId));
    }
    if (this.criteria.getRmlId() != null) {
      listPredicate.add(builder.equal(root.get(RML_ID), this.criteria.getRmlId()));
    }
    if (this.criteria.getFileName() != null) {
      listPredicate.add(builder.like(root.get(FILE_NAME), "%" + this.criteria.getFileName() + "%"));
    }
    if (this.criteria.getStatusImport() != null) {
      listPredicate.add(builder.equal(root.get(STATUS_IMPORT), this.criteria.getStatusImport()));
    }
    if (this.criteria.getRdfFormat() != null) {
      listPredicate.add(builder.equal(root.get(RDF_FORMAT), this.criteria.getRdfFormat()));
    }
    if (this.criteria.getTriplestoreDatasetId() != null) {
      listPredicate.add(builder.equal(root.get("triplestoreDatasetId"), this.criteria.getTriplestoreDatasetId()));
    }
    return builder.and(listPredicate.toArray(new Predicate[listPredicate.size()]));
  }

  @Override
  protected void completePredicatesList(Root<RdfDatasetFile> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    // no-op
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public void setSourceDatasetId(String sourceDatasetId) {
    this.sourceDatasetId = sourceDatasetId;
  }

  public void setSourceDatasetFileId(String sourceDatasetFileId) {
    this.sourceDatasetFileId = sourceDatasetFileId;
  }

}
