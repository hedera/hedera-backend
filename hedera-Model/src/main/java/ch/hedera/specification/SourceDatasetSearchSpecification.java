/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - SourceDatasetSearchSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.specification;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SearchSpecification;
import ch.unige.solidify.util.SearchCriteria;

import ch.hedera.model.ingest.SourceDataset;

public class SourceDatasetSearchSpecification extends SearchSpecification<SourceDataset> {
  @Serial
  private static final long serialVersionUID = 2580232426920635672L;

  public SourceDatasetSearchSpecification(SearchCriteria criteria) {
    super(criteria);
  }

  @Override
  public Predicate toPredicate(Root<SourceDataset> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    final List<Predicate> listPredicate = new ArrayList<>();
    if (this.getCriteria().getKey().equals("name")) {
      listPredicate.add(builder.like(root.get("name"), "%" + this.getCriteria().getValue() + "%"));
    }
    if (this.getCriteria().getKey().equals("creation.who")) {
      listPredicate.add(builder.equal(root.get("creation").get("who"), this.getCriteria().getValue()));
    }
    return builder.and(listPredicate.toArray(new Predicate[listPredicate.size()]));
  }
}
