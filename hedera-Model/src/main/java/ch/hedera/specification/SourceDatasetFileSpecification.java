/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - SourceDatasetFileSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.specification;

import static ch.hedera.HederaConstants.FILE_NAME;
import static ch.hedera.HederaConstants.PROJECT_ID_FIELD;
import static ch.hedera.HederaConstants.SOURCE_DATASET;
import static ch.hedera.HederaConstants.SOURCE_PATH;
import static ch.hedera.HederaConstants.STATUS;
import static ch.hedera.HederaConstants.TYPE;
import static ch.hedera.HederaConstants.VERSION;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.hedera.HederaConstants;
import ch.hedera.model.ingest.SourceDatasetFile;

public class SourceDatasetFileSpecification extends SolidifySpecification<SourceDatasetFile> {
  @Serial
  private static final long serialVersionUID = -1353365423369560781L;

  public SourceDatasetFileSpecification(SourceDatasetFile criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<SourceDatasetFile> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getFileName() != null) {
      predicatesList.add(builder.like(root.get(FILE_NAME), "%" + this.criteria.getFileName() + "%"));
    }
    if (this.criteria.getSourcePath() != null) {
      predicatesList.add(builder.like(root.get(SOURCE_PATH).as(String.class), "%" + this.criteria.getSourcePath() + "%"));
    }
    if (this.criteria.getStatus() != null) {
      predicatesList.add(builder.equal(root.get(STATUS), this.criteria.getStatus()));
    }
    if (this.criteria.getType() != null) {
      predicatesList.add(builder.equal(root.get(TYPE), this.criteria.getType()));
    }
    if (this.criteria.getVersion() != null) {
      predicatesList.add(builder.like(root.get(VERSION), "%" + this.criteria.getVersion() + "%"));
    }
    if (this.criteria.getSourceDataset() != null) {
      if (this.criteria.getSourceDataset().getProjectId() != null) {
        predicatesList.add(builder.equal(root.get(SOURCE_DATASET).get(PROJECT_ID_FIELD), this.criteria.getSourceDataset().getProjectId()));
      }
      if (this.criteria.getSourceDataset().getResId() != null) {
        predicatesList.add(builder.equal(root.get(SOURCE_DATASET).get(HederaConstants.DB_RES_ID), this.criteria.getSourceDataset().getResId()));
      }
    }
    if (this.criteria.isLinkToResearchDataFile() && this.criteria.getResearchDataFile().getResId() != null) {
      predicatesList.add(
              builder.like(root.get("researchDataFile.resId").as(String.class), "%" + this.criteria.getResearchDataFile().getResId() + "%"));
    }
  }
}
