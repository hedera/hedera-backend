/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - PersonSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Person;

public class PersonSpecification extends SolidifySpecification<Person> {
  @Serial
  private static final long serialVersionUID = -7232720336130296433L;

  private String projectResId;
  private Role role;

  public PersonSpecification(Person criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<Person> root, CriteriaQuery<?> query, CriteriaBuilder builder, List<Predicate> predicatesList) {
    if (this.criteria.getFirstName() != null) {
      predicatesList.add(builder.like(root.get("firstName"), "%" + this.criteria.getFirstName() + "%"));
    }
    if (this.criteria.getLastName() != null) {
      predicatesList.add(builder.like(root.get("lastName"), "%" + this.criteria.getLastName() + "%"));
    }
    if (this.criteria.getOrcid() != null) {
      predicatesList.add(builder.like(root.get("orcid"), "%" + this.criteria.getOrcid() + "%"));
    }
    if (this.projectResId != null && this.role.getLevel() > 0) {
      query.distinct(true);
      predicatesList.add(builder.and(
              builder.like(root.join("projectRoles").get("compositeKey").get("project").get("resId"), this.projectResId),
              builder.and(builder.lessThanOrEqualTo(root.join("projectRoles").get("compositeKey").get("role").get("level"),
                      this.role.getLevel()))));
    }
  }

  public String getProjectResId() {
    return this.projectResId;
  }

  public void setProjectResId(String projectResId) {
    this.projectResId = projectResId;
  }

  public Role getRole() {
    return this.role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

}
