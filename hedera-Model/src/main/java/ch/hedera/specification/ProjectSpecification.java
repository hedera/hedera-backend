/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ProjectSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.specification;

import java.io.Serial;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ProjectPersonRole;

public class ProjectSpecification extends SolidifySpecification<Project> {
  @Serial
  private static final long serialVersionUID = -7633952907605899870L;

  private static final String PUBLIC_ACCESS = "accessPublic";
  private static final String OPENING_DATE = "openingDate";
  private static final String CLOSING_DATE = "closingDate";

  public ProjectSpecification(Project criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<Project> root, CriteriaQuery<?> query, CriteriaBuilder builder, List<Predicate> predicatesList) {
    if (this.criteria.getName() != null) {
      predicatesList.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }
    if (this.criteria.getDescription() != null) {
      predicatesList.add(builder.like(root.get("description"), "%" + this.criteria.getDescription() + "%"));
    }
    if (this.criteria.getOpeningDate() != null) {
      predicatesList.add(builder.lessThanOrEqualTo(root.get(OPENING_DATE), this.criteria.getOpeningDate()));
    }
    if (this.criteria.getClosingDate() != null) {
      predicatesList.add(builder.or(builder.isNull(root.get(CLOSING_DATE)),
              builder.greaterThanOrEqualTo(root.get(CLOSING_DATE), this.criteria.getClosingDate())));
    }
    if (this.criteria.isAccessPublic() != null) {
      predicatesList.add(builder.equal(root.get(PUBLIC_ACCESS), this.criteria.isAccessPublic()));
    }
    if (this.criteria.hasData() != null) {
      predicatesList.add(builder.equal(root.get("hasData"), this.criteria.hasData()));
    }
    if (this.criteria.getShortName() != null) {
      predicatesList.add(builder.equal(root.get("shortName"), this.criteria.getShortName()));
    }
    if (this.criteria.getFileNumber() != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("fileNumber"), this.criteria.getFileNumber()));
    }
    if (this.criteria.getTripleNumber() != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("tripleNumber"), this.criteria.getTripleNumber()));
    }
    if (this.criteria.getTotalSize() != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("totalSize"), this.criteria.getTotalSize()));
    }
    if(this.criteria.getCopyrightHolder() != null) {
      predicatesList.add(builder.like(root.get("copyrightHolder"), "%" + this.criteria.getCopyrightHolder() + "%"));
    }
  }

  /***
   * Specification that will query all the Open public projects and also those where personId has a role.
   * The query is the following adding later the filtering if needed:
   * SELECT DISTINCT po
   * FROM Project po
   * LEFT JOIN p.projectRoles oupr
   * WHERE (oupr.compositeKey.person.resId = :personId) OR (po.access_public = "true")
   * AND oupr.compositeKey.project.openingDate <= CURRENT_DATE
   * AND (oupr.compositeKey.project.closingDate IS NULL OR oupr.compositeKey.project.closingDate > CURRENT_DATE)
   ***/
  public static Specification<Project> getOpenProjectsByPersonIdAndProjectNameAddingAllPublic(String personId, Project project) {
    return (root, criteria, builder) -> {
      final List<Predicate> predicatesList = new ArrayList<>();

      criteria.distinct(true);

      ProjectSpecification.addProjectPersonRolePredicates(personId, predicatesList, builder, root, true);

      ProjectSpecification.addDatesPredicates(predicatesList, builder, root);

      ProjectSpecification.addProjectPredicates(project, predicatesList, builder, root);

      return builder.and(predicatesList.toArray(new Predicate[predicatesList.size()]));
    };
  }

  /***
   * Specification that will query all the Open public projects and also those where personId has a role.
   * The query is the following adding later the filtering if needed:
   * SELECT DISTINCT oupr.compositeKey.project FROM Project po JOIN po.personRoles oupr
   * WHERE oupr.compositeKey.person.resId = :personId
   * AND (:projectName IS NULL OR (:projectName IS NOT NULL AND oupr.compositeKey.project.name LIKE %:projectName%))
   * AND oupr.compositeKey.project.openingDate <= CURRENT_DATE
   * AND (oupr.compositeKey.project.closingDate IS NULL OR oupr.compositeKey.project.closingDate > CURRENT_DATE)
   ***/
  public static Specification<Project> getOpenProjectsByPersonIdAndProjectName(String personId, Project project) {
    return (root, criteria, builder) -> {
      final List<Predicate> predicatesList = new ArrayList<>();
      criteria.distinct(true);

      ProjectSpecification.addProjectPersonRolePredicates(personId, predicatesList, builder, root, false);

      ProjectSpecification.addDatesPredicates(predicatesList, builder, root);

      ProjectSpecification.addProjectPredicates(project, predicatesList, builder, root);

      return builder.and(predicatesList.toArray(new Predicate[predicatesList.size()]));
    };
  }

  /***
   * Specification that will query all the Open public projects and also those where personId has a role.
   * The query is the following adding later the filtering if needed:
   * SELECT DISTINCT po FROM Project po JOIN po.personRoles oupr
   * WHERE oupr.compositeKey.person.resId = :personId
   ***/
  public static Specification<Project> getProjectsByPersonIdAndProjectName(String personId, Project project) {
    return (root, criteria, builder) -> {
      final List<Predicate> predicatesList = new ArrayList<>();

      criteria.distinct(true);

      ProjectSpecification.addProjectPersonRolePredicates(personId, predicatesList, builder, root, false);

      ProjectSpecification.addProjectPredicates(project, predicatesList, builder, root);

      return builder.and(predicatesList.toArray(new Predicate[predicatesList.size()]));
    };
  }

  /***
   * Specification that will query all the public projects and also those where personId has a role.
   * The query is the following adding later the filtering if needed:
   * SELECT DISTINCT po
   * FROM Project po
   * LEFT JOIN oupr.compositeKey.project FROM Person p JOIN p.projectRoles oupr
   * WHERE (oupr.compositeKey.person.resId = :personId) OR (po.access_public = "true")
   ***/
  public static Specification<Project> getProjectsByPersonIdAndProjectNameAddingAllPublic(String personId, Project project) {
    return (root, criteria, builder) -> {
      final List<Predicate> predicatesList = new ArrayList<>();

      criteria.distinct(true);

      ProjectSpecification.addProjectPersonRolePredicates(personId, predicatesList, builder, root, true);

      ProjectSpecification.addProjectPredicates(project, predicatesList, builder, root);

      return builder.and(predicatesList.toArray(new Predicate[predicatesList.size()]));
    };
  }

  public static void addProjectPredicates(Project project, List<Predicate> predicatesList, CriteriaBuilder builder, Root<Project> root) {
    // Add possible filters to both root and personRolesJoin since they are two queries separated.
    if (project.getName() != null) {
      predicatesList.add(builder.like(root.get("name"), "%" + project.getName() + "%"));
    }
    if (project.isAccessPublic() != null) {
      predicatesList.add(builder.equal(root.get(PUBLIC_ACCESS), project.isAccessPublic()));
    }
    if (project.hasData() != null) {
      predicatesList.add(builder.equal(root.get("hasData"), project.hasData()));
    }
  }

  public static void addProjectPersonRolePredicates(String personId, List<Predicate> predicatesList, CriteriaBuilder builder, Root<Project> root,
          boolean withAllPublic) {
    Join<Project, ProjectPersonRole> personRolesJoin = root.join("personRoles", JoinType.LEFT);
    if (withAllPublic) {
      predicatesList.add(builder.or(builder.equal(personRolesJoin.get("compositeKey").get("person").get("resId"), personId),
              builder.equal(root.get(PUBLIC_ACCESS), "true")));
    } else {
      predicatesList.add(builder.equal(personRolesJoin.get("compositeKey").get("person").get("resId"), personId));
    }
  }

  public static void addDatesPredicates(List<Predicate> predicatesList, CriteriaBuilder builder, Root<Project> root) {
    LocalDate now = LocalDate.now();
    predicatesList.add(builder.or(builder.isNull(root.get(CLOSING_DATE)), builder.greaterThan(root.get(CLOSING_DATE), now)));
    predicatesList.add(builder.lessThanOrEqualTo(root.get(OPENING_DATE), now));
  }

}
