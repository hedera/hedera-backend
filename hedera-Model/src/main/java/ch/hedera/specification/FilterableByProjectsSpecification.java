/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - FilterableByProjectsSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.specification;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.hedera.HederaConstants;
import ch.hedera.model.Access;
import ch.hedera.model.settings.Project;

public interface FilterableByProjectsSpecification<T> {

  /**
   * value used to get a Predicate discarding all search results
   */
  String NO_PROJECT_VALUE = "-1";

  /**
   * If projects == null ----> no filter must be applied ----> return null If projects != null ----->
   * use the values to build a Predicate or a fake value of "-1" to obtain a query returning nothing
   *
   * @param root
   * @param projects
   * @return
   */
  default Predicate getInProjectsPredicate(Root<T> root, List<Project> projects) {

    if (projects != null) {

      List<String> projectIds = new ArrayList<>();

      if (!projects.isEmpty()) {
        projectIds = this.getProjectIds(projects);
      } else {
        projectIds.add(NO_PROJECT_VALUE);
      }

      return this.getProjectPath(root).in(projectIds);

    } else {
      return null;
    }
  }

  /**
   * Return a Predicate to search for items without embargo and public final access
   *
   * @param root
   * @param builder
   * @return
   */
  default Predicate getNoEmbargoAndPublicAccessPredicate(Root<T> root, CriteriaBuilder builder) {
    return builder.and(
            builder.isNull(this.getEmbargoAccessPath(root)),
            builder.equal(this.getFinalAccessPath(root), Access.PUBLIC));
  }

  /**
   * Get a list of ids as String with each project resId
   *
   * @param projects
   * @return
   */
  default List<String> getProjectIds(List<Project> projects) {

    final List<String> projectIds = new ArrayList<>();

    if (projects != null) {
      for (final Project unit : projects) {
        projectIds.add(unit.getResId());
      }
    }

    return projectIds;
  }

  /**
   * Must return the Path to the projectId property
   *
   * @param root
   * @return
   */
  default Path<Object> getProjectPath(Root<T> root) {
    return root.get("info").get(HederaConstants.PROJECT_ID_FIELD);
  }

  default Path<Object> getFinalAccessPath(Root<T> root) {
    return root.get("info").get("access");
  }

  default Path<Object> getEmbargoAccessPath(Root<T> root) {
    return root.get("info").get("embargo").get("access");
  }

  /**
   * Set a list of project that are used to build a filtering 'in' Predicate
   *
   * @param projects
   */
  void setProjects(List<Project> projects);

}
