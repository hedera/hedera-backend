/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - HederaConstants.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera;

import java.util.List;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;

import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

public class HederaConstants {

  private HederaConstants() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  // Modules
  public static final String HEDERA = "hedera";
  public static final String HEDERA_SOLUTION = "hedera Solution";
  public static final String HEDERA_WEB_SITE = "https://www.unige.ch";
  public static final int HEDERA_SEQUENCE_START = 30000000;

  public static final String NO_SIZE = "unknown";

  // URLs
  public static final String URL_PROJECT_FIELD = "projectShortName";
  public static final String URL_PROJECT = "{" + URL_PROJECT_FIELD + "}";
  // @formatter:off
  private static final String[] PUBLIC_URLS = {
          "/docs/**",
          "/hedera.png",
          "/hedera.svg",
          "/css/*.css",
          "/.well-known/jwks.json",
          "/swagger-ui.html",
          "/swagger-config",
          "/swagger-ui/**",
          "/api-docs",
          "/api-docs.yaml",
          "/api-docs/**",
          "/" + ActionName.DASHBOARD,
          "/" + ResourceName.SCHEMA,
          "/" + ResourceName.PROFILE,
          "/" + ResourceName.XSL,
          "/" + HederaActionName.LIST_VERSION,
          "/" + ModuleName.ADMIN + "/" + SolidifyConstants.ORCID + "/" + ActionName.LANDING,
          "/" + ModuleName.ADMIN + "/" + ResourceName.MODULE,
          "/" + ModuleName.ADMIN + "/" + ResourceName.SYSTEM_PROPERTY,
          "/" + ModuleName.ADMIN + "/" + ResourceName.GLOBAL_BANNER + "/" + HederaActionName.GET_ACTIVE,
          "/" + ModuleName.ADMIN + "/" + ResourceName.LICENSE + "/*/" + HederaActionName.DOWNLOAD_LOGO,
          "/" + ModuleName.ADMIN + "/" + ResourceName.LICENSE + "/*",
          "/" + ModuleName.ADMIN + "/" + ResourceName.INSTITUTION + "/*/" + HederaActionName.DOWNLOAD_LOGO,
          "/" + ModuleName.ADMIN + "/" + ResourceName.INSTITUTION + "/*",
          "/" + ModuleName.ADMIN + "/" + ResourceName.FUNDING_AGENCY + "/*/" + HederaActionName.DOWNLOAD_LOGO,
          "/" + ModuleName.ADMIN + "/" + ResourceName.FUNDING_AGENCY + "/*",
          "/" + ModuleName.ADMIN + "/" + ResourceName.PROJECT + "/*/" + ResourceName.RESEARCH_OBJECT_TYPE,
          "/" + ModuleName.ADMIN + "/" + ResourceName.RESEARCH_OBJECT_TYPE + "/*",
          "/" + ModuleName.ACCESS + "/" + ActionName.SITEMAP,
          "/" + ModuleName.ACCESS + "/" + ResourceName.SYSTEM_PROPERTY,
          "/" + ModuleName.ACCESS + "/" + ResourceName.PROJECT + "/**",
          "/" + ModuleName.ACCESS + "/" + ResourceName.RESEARCH_OBJECT + "/**",
          "/" + OAIConstants.OAI_MODULE + "/" + OAIConstants.OAI_PROVIDER,
          "/" + OAIConstants.OAI_MODULE + "/" + OAIConstants.OAI_PROVIDER + "/" + OAIConstants.OAI_RESOURCE,
          "/" + OAIConstants.OAI_MODULE + "/" + OAIConstants.OAI_PROVIDER + "/" + OAIConstants.OAI_RESOURCE + "/" + ResourceName.XSL,
          "/" + OAIConstants.OAI_RESOURCE,
          "/" + OAIConstants.OAI_RESOURCE + "/" + ResourceName.XSL,
          "/" + ModuleName.IIIF + "/**",
          "/" + ModuleName.IIIF + "/" + ResourceName.MANIFEST + "/**",
          "/" + ModuleName.SPARQL + "/**",
          "/" + ModuleName.LINKED_DATA + "/**",
          "/" + ModuleName.ONTOLOGY + "/**",
          "/" + ActionName.DOWNLOAD,
          "/" + ActionName.SITEMAP_XML,
          "/" + ActionName.SITEMAP + "-*" + ActionName.XML_EXTENSION,
  };

  /**
   * Urls used to deactivate CSRF control when using POST verb.
   */
  private static final String[] DISABLED_CSRF_URLS = {
          "/" + ModuleName.ACCESS + "/" + ResourceName.RESEARCH_OBJECT + "/**",
          "/" + ModuleName.ACCESS + "/" + ResourceName.PROJECT + "/**",
          "/" + OAIConstants.OAI_RESOURCE,
          "/" + ModuleName.IIIF + "/**",
          "/" + ModuleName.SPARQL + "/**"
  };
  // @formatter:on

  // Default Values
  public static final int DAYS_BY_YEAR = 365;
  public static final int DAYS_BY_MONTH = 30;
  public static final int YEARS_BY_CENTURY = 100;
  public static final int ORDER_INCREMENT = 10;
  public static final String DEFAULT_COPYRIGHT_HOLDER = "Université de Genève";

  // Database column size
  public static final int DB_SHORT_STRING_LENGTH = SolidifyConstants.DB_SHORT_STRING_LENGTH; // 30
  public static final int DB_MEDIUM_STRING_LENGTH = SolidifyConstants.DB_MEDIUM_STRING_LENGTH; // 100
  public static final int DB_DEFAULT_STRING_LENGTH = SolidifyConstants.DB_DEFAULT_STRING_LENGTH; // 255
  public static final int DB_LONG_STRING_LENGTH = SolidifyConstants.DB_LONG_STRING_LENGTH; // 1024
  public static final int DB_LARGE_STRING_LENGTH = SolidifyConstants.DB_LARGE_STRING_LENGTH; // 4096

  // Database column name
  public static final String DB_RES_ID = SolidifyConstants.DB_RES_ID; // "resId"
  public static final String DB_PROJECT_ID = "projectId";
  public static final String DB_INSTITUTION_ID = "institutionId";
  public static final String DB_ONTOLOGY_ID = "ontologyId";
  public static final String DB_RML_ID = "rmlId";
  public static final String DB_PERSON_ID = "personId";
  public static final String DB_AGENCY_ID = "agencyId";
  public static final String DB_RESEARCH_DATA_FILE_RESEARCH_OBJECT_TYPE_ID = "researchDataFileResearchObjectTypeId";
  public static final String DB_RESEARCH_OBJECT_TYPE_ID = "researchObjectTypeId";
  public static final String DB_IIIF_MANIFEST_RESEARCH_OBJECT_TYPE_ID = "iiifManifestResearchObjectTypeId";
  public static final String DB_NAME = "name";
  public static final String DB_PROJECT_SHORT_NAME = "projectShortName";

  public static final String DB_LOGO_ID = "logoId";
  public static final String DB_AVATAR_ID = "avatarId";
  public static final String DB_RDF_FILE_ID = "rdfFileId";
  public static final String DB_RESEARCH_DATA_FILE_ID = "researchDataFileId";

  public static final String DB_SOURCE_DATASET_ID = "sourceDatasetId";
  public static final String DB_SOURCE_DATASET_FILE_ID = "sourceDatasetFileId";

  public static final String PROJECT_ID_FIELD = "projectId";
  public static final String RESEARCH_OBJECT_TYPE_ID_FIELD = "researchObjectTypeId";

  public static final String ACCESSIBLE_FROM_FIELD = "accessibleFrom";

  // Index
  public static final String PRIVATE = "private";
  // Facets
  public static final String INDEXING_KEYWORD = ".keyword";
  public static final String ITEM_TYPE_FACET = "item-types";
  public static final String PROJECT_FACET = "projects";
  public static final String RESEARCH_OBJET_TYPE_FACET = "research-object-types";
  public static final String RESEARCH_OBJET_RDF_TYPE_FACET = "research-object-rdf-types";
  public static final String OBJECT_TYPE_FACET = "object-types";
  public static final String CONTENT_TYPE_FACET = "content-types";
  public static final String ONTOLOGY_FACET = "ontologies";
  public static final String ONTOLOGY_VERSION_FACET = "ontology-versions";
  // Fields
  public static final String ITEM_TYPE_INDEX_FIELD = "itemType";
  public static final String OBJECT_TYPE_INDEX_FIELD = "objectType";
  public static final String OBJECT_DATE_INDEX_FIELD = "objectDate";
  public static final String PROJECT_ID_INDEX_FIELD = "researchProject.id";
  public static final String HEDERA_ID_INDEX_FIELD = "researchProject.hederaId";
  public static final String CONTENT_TYPE_INDEX_FIELD = "mimeType";
  public static final String PROJECT_INDEX_FIELD = "researchProject.name";
  public static final String PROJECT_SHORT_NAME_INDEX_FIELD = "researchProject.shortName";
  public static final String RESEARCH_OBJET_TYPE_NAME_INDEX_FIELD = "researchObjectType.name";
  public static final String RESEARCH_OBJET_RDF_TYPE_INDEX_FIELD = "researchObjectType.rdfType";
  public static final String ONTOLOGY_NAME_INDEX_FIELD = "researchObjectType.ontology.name";
  public static final String ONTOLOGY_VERSION_INDEX_FIELD = "researchObjectType.ontology.version";
  public static final String RESEARCH_OBJET_METADATA_INDEX_FIELD = "metadataList";
  public static final String RESEARCH_OBJET_XML_INDEX_FIELD = "xml";
  public static final String INDEX_FIELD_FILENAME = "fileName";
  public static final String INDEX_FIELD_RELATIVE_LOCATION = "relativeLocation";
  public static final String INDEX_FIELD_MIME_TYPE = "mimeType";
  public static final String INDEX_FIELD_HEDERA_ID = "hederaId";
  public static final String INDEX_FIELD_FILE_SIZE = "fileSize";

  public static final String INDEX_FIELD_URI_SORT_ALIAS = "uriSort";
  public static final String INDEX_FIELD_URI_SORT = "uri.keyword";
  public static final String INDEX_FIELD_OBJECT_DATE_ALIAS = "objectDate";
  public static final String INDEX_FIELD_OBJECT_DATE = "metadata.objectDate";

  // Thumbnails
  public static final String THUMBNAIL_EXTENSION = ".thumbnail";

  // Search
  public static final String SEARCH_PROJECT = "project";
  public static final String SOURCE_DATASET_RES_ID = "sourceDataset.resId";
  public static final String SOURCE_DATASET_FILE = "sourceDatasetFile";
  public static final String SOURCE_DATASET = "sourceDataset";
  public static final String RML_ID = "rmlId";
  public static final String FILE_NAME = "fileName";
  public static final String SOURCE_PATH = "sourcePath";
  public static final String STATUS_IMPORT = "statusImport";
  public static final String STATUS = "status";
  public static final String RDF_FORMAT = "rdfFormat";
  public static final String DATACITE_TYPE_BOOK = "Book";
  public static final String DATACITE_TYPE_BOOK_CHAPTER = "BookChapter";
  public static final String DATACITE_TYPE_CONFERENCE_PAPER = "ConferencePaper";
  public static final String DATACITE_TYPE_DATASET = "Dataset";
  public static final String DATACITE_TYPE_JOURNAL_ARTICLE = "JournalArticle";
  public static final String DATACITE_TYPE_PEER_REVIEW = "PeerReview";
  public static final String DATACITE_TYPE_REPORT = "Report";

  public static final String ARCHIVE_ID_FIELD = "archiveId";
  public static final String URL_TRIPLESTORE_DATASET_ID = "/{triplestoreDatasetId}";
  public static final String FAMILY_NAME = "familyName";
  public static final String GIVEN_NAME = "givenName";

  // OAI
  public static final String OAI_PROJECT = "projects";
  public static final String OAI_RESEARCH_OBJECT = "research-objects";
  public static final String OAI_RESEARCH_DATA_FILE = "research-data-files";
  public static final String OAI_DIGITAL_OBJECT = "digital-objects";
  public static final String OAI_HEDERA = "hedera";
  public static final String OAI_HEDERA_NAME = "hedera Metadata";

  // Ontology
  public static final String ONTOLOGY_RDF_CLASS = "RDF-Class";
  public static final String ONTOLOGY_RDF_PROPERTY = "RDF-Property";
  public static final String ONTOLOGY_RDF = "RDF";
  public static final String ONTOLOGY_RDF_BASE_URI = "http://www.w3.org/2000/01/rdf-schema#";
  public static final String ONTOLOGY_CIDOC_CRM = "CIDOC CRM";
  public static final String ONTOLOGY_CIDOC_CRM_BASE_URI = "http://www.cidoc-crm.org/cidoc-crm/";
  public static final String ONTOLOGY_CRM_DIG = "CRMdig";
  public static final String ONTOLOGY_CRM_DIG_BASE_URI = "http://www.ics.forth.gr/isl/CRMdig/";
  public static final String ONTOLOGY_RIC_O = "RiC-O";
  public static final String ONTOLOGY_RIC_O_BASE_URI = "https://www.ica.org/standards/RiC/ontology#";
  public static final String ONTOLOGY_EXIF = "Exif";
  public static final String ONTOLOGY_EXIF_BASE_URI = "http://www.w3.org/2003/12/exif/ns#";
  public static final String ONTOLOGY_EBU_CORE = "EBU Core";
  public static final String ONTOLOGY_EBU_CORE_BASE_URI = "http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#";

  // Research Object Type IDs
  protected static final String RESEARCH_OBJECT_TYPE_SUFFIX = "-default";
  public static final String RESEARCH_OBJECT_TYPE_E24 = "cidoc-crm-e24" + RESEARCH_OBJECT_TYPE_SUFFIX;
  public static final String RESEARCH_OBJECT_TYPE_E22 = "cidoc-crm-e22" + RESEARCH_OBJECT_TYPE_SUFFIX;
  public static final String RESEARCH_OBJECT_TYPE_E41 = "cidoc-crm-e41" + RESEARCH_OBJECT_TYPE_SUFFIX;
  public static final String RESEARCH_OBJECT_TYPE_E53 = "cidoc-crm-e53" + RESEARCH_OBJECT_TYPE_SUFFIX;
  public static final String RESEARCH_OBJECT_TYPE_E39 = "cidoc-crm-e39" + RESEARCH_OBJECT_TYPE_SUFFIX;
  public static final String RESEARCH_OBJECT_TYPE_E21 = "cidoc-crm-e21" + RESEARCH_OBJECT_TYPE_SUFFIX;
  public static final String RESEARCH_OBJECT_TYPE_D1 = "cidoc-crm-dig-d1" + RESEARCH_OBJECT_TYPE_SUFFIX;
  public static final String RESEARCH_OBJECT_TYPE_RDF_TYPE_D1 = "D1_Digital_Object";

  // DataCite
  public static final String HEDERA_NAMESPACE_1 = "http://www.unige.ch/hedera/v1";
  public static final String HEDERA_SCHEMA_1 = "http://www.unige.ch/hedera/v1/hedera-research-object.xsd";
  public static final String HEDERA_REPOSITORY_SCHEMA_1 = "hedera-repository-1.0.xsd";
  public static final String HEDERA_RESEARCH_OBJECT_SCHEMA_1 = "hedera-research-object-1.0.xsd";

  // File Metadata
  public static final String FILE_METADATA_WIDTH = "width";
  public static final String FILE_METADATA_HEIGHT = "height";
  public static final String FILE_METADATA_PAGES = "pages";
  public static final String FILE_WEB_URL = "web";
  public static final String FILE_METADATA_PDF_VERSION = "pdf";

  // Misc.
  public static final String UNKNOWN = "unknown";
  public static final String DATASET_FILE_TYPE = "datasetFileType";
  public static final String VERSION = "version";

  public static final String ORCID = "ORCID";
  public static final String ROR = "ROR";
  private static final String[] ROR_PREFIXES = { "https://ror.org/", "http://ror.org/", "https://www.ror.org/", "http://www.ror.org/" };

  public static final String WEB = "WEB";
  public static final String GRID = "GRID";
  public static final String ISNI = "ISNI";
  public static final String CROSSREF_FUNDER = "CrossrefFunder";
  public static final String WIKIDATA = "Wikidata";
  public static final String SPDX = "SPDX";
  public static final String NO_REPLY_PREFIX = "noreply-";

  // IIIF
  public static final String IIIF_IMAGE_INFO = "info.json";

  // IIIF Manifest
  public static final String MANIFEST_DEFAULT_LANGUAGE = "en";
  public static final String MANIFEST_RESOURCE_SUBJECT_VAR = "resource_subject";
  public static final String MANIFEST_IMG_SUBJECT_VAR = "img_subject";
  public static final String MANIFEST_IMG_IIIF_URL_VAR = "img_iiif_url";
  public static final String MANIFEST_IMG_HEIGHT_VAR = "img_height";
  public static final String MANIFEST_IMG_WIDTH_VAR = "img_width";
  public static final String MANIFEST_LABEL_VAR = "label";
  public static final String MANIFEST_METADATA_LABEL_PREFIX = "manifest_md_";

  // Message processor
  public static final String MESSAGE_PROCESSOR_NAME = "hedera Message Processor";

  // Emails
  public static final String NOTIFICATION_EMAIL_HEDERA_PREFIX = "[hedera]";
  public static final String NOTIFICATION_EMAIL_VALIDATION_PREFIX = "[s validation]";
  public static final String NOTIFICATION_IDS = "notificationIds";

  // Checksums
  public static final String FILE = "file";
  public static final String TYPE = "type";

  // Web service Parameter
  public static final String QUERY_PARAM = "query";
  public static final String IDENTIFIER_TYPE_PARAM = "identifierType";
  public static final String ACCESSIBLE_FROM_PARAM = "accessibleFrom";
  public static final String RELATIVE_LOCATION_PARAM = "relativeLocation";

  // Licenses
  public static final String OPEN_LICENSE_ID_FIELD = "openLicenseId";

  // Formats
  public static final String OWL = "OWL";
  public static final String RDFS = "RDFS";
  public static final String CSV = "CSV";
  public static final String RDF_XML = "RDF/XML";
  public static final String RDF_JSON = "RDF/JSON";
  public static final String JSON_LD = "JSON-LD";
  public static final String TURTLE = "TURTLE";
  public static final String N_TRIPLES = "N-TRIPLES";
  public static final String N_QUADS = "NQuads";
  public static final String N3 = "N3";
  public static final String TRIG = "TriG";
  public static final String TRIX = "TriX";
  public static final String SQL = "SQL";
  public static final String XML = "XML";
  public static final String JSON = "JSON";
  public static final String TTL = "TTL";
  public static final String RDF = "RDF";
  public static final String RESEARCH_DATA_FILE = "RESEARCH-DATA-FILE";

  public static final String IIIF = "IIIF";
  public static final String IIIF_MANIFEST = "IIIF_MANIFEST";
  public static final String IIIF_PROJECT_DEFAULT_COLLECTION = "project-collection";
  public static final String TEI = "TEI";

  // Mime Types
  public static final String TEXT_CSV_MIME_TYPE = "text/csv";
  public static final String TEXT_TURTLE_MIME_TYPE = "text/turtle";
  public static final String TEXT_N3_MIME_TYPE = "text/n3";
  public static final String APPLICATION_RDF_XML_MIME_TYPE = "application/rdf+xml";
  public static final String APPLICATION_RTF_MIME_TYPE = "application/rtf";
  public static final String APPLICATION_MS_WORD_MIME_TYPE = "application/msword";
  public static final String APPLICATION_OPEN_DOC_MIME_TYPE = "application/vnd.oasis.opendocument";
  public static final String APPLICATION_SPARQL_RESULT_XML_MIME_TYPE = "application/sparql-results+xml";
  public static final String APPLICATION_SPARQL_RESULT_JSON_MIME_TYPE = "application/sparql-results+json";
  public static final String APPLICATION_TRIG_MIME_TYPE = "application/trig";
  public static final String APPLICATION_JSON_LD_MIME_TYPE = "application/ld+json";
  public static final String APPLICATION_N_TRIPLES_MIME_TYPE = "application/n-triples";
  public static final String APPLICATION_N_QUADS_MIME_TYPE = "application/n-quads";
  public static final String APPLICATION_TRIX_MIME_TYPE = "application/trix";

  public static final String PERCENT_ENCODING_SPACE_CHAR = "%20";

  // Regex
  public static final String RDF_NAME_REGEX = "^[a-zA-Z0-9\\-_]+$";

  /**
   * If object storage, check that the name respect AWS bucket naming requirements
   * see : https://docs.aws.amazon.com/AmazonS3/latest/userguide/bucketnamingrules.html
   */
  public static final String AMAZON_S3_BUCKET_NAME_REGEX = "(?!(^xn--|^sthree-|.+-s3alias|.+--ol-s3$))^[a-z0-9][a-z0-9-]{1,61}[a-z0-9]$";

  public static String[] getPublicUrls() {
    return PUBLIC_URLS.clone();
  }

  public static String[] getDisabledCsrfUrls() {
    return DISABLED_CSRF_URLS.clone();
  }

  public static String[] getRorPrefixList() {
    return ROR_PREFIXES.clone();
  }

  public static List<String> getIIIFManifestMandatoryVariables() {
    return List.of(MANIFEST_RESOURCE_SUBJECT_VAR, MANIFEST_IMG_SUBJECT_VAR, MANIFEST_IMG_IIIF_URL_VAR, MANIFEST_IMG_HEIGHT_VAR,
            MANIFEST_IMG_WIDTH_VAR, MANIFEST_LABEL_VAR);
  }

}
