/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - HederaValidationTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.tool;

import ch.unige.solidify.util.StringTool;

public class HederaValidationTool {

  // Regular expression to allow alpha-numeric, underscore, hyphen, dot, slash and parenthesis
  private static final String VALID_FOLDER_PATH_NAME = "[\\w\\/\\-\\.\\(\\)]+";

  // Regular expression to allow alpha-numeric, underscore, hyphen, dot and parenthesis
  private static final String VALID_FILE_NAME = "[\\w\\-\\.\\(\\)]+";

  public static boolean isValidFileName(String name) {
    if (StringTool.isNullOrEmpty(name)) {
      return false;
    }
    return name.matches(VALID_FILE_NAME);
  }

  public static boolean isValidRelativeLocation(String relativeLocation) {
    if (StringTool.isNullOrEmpty(relativeLocation)) {
      return false;
    }
    return relativeLocation.matches(VALID_FOLDER_PATH_NAME);
  }

}
