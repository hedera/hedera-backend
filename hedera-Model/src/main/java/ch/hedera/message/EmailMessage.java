/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - EmailMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.message;

import java.io.Serializable;
import java.util.Map;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;

import ch.hedera.HederaConstants;

public class EmailMessage implements Serializable {
  private static final long serialVersionUID = 6428936902341150988L;

  //@formatter:off
  public enum EmailTemplate {
    NEW_SUBSCRIPTION(HederaConstants.NOTIFICATION_EMAIL_HEDERA_PREFIX + " Inscription à des notifications / Subscription to notifications"),
    REMOVE_SUBSCRIPTION(HederaConstants.NOTIFICATION_EMAIL_HEDERA_PREFIX + " Désinscription à des notifications / Unsubscription to notifications");
  
    private final String subject;

    EmailTemplate(String subject) {
      this.subject = subject;
    }

    public String getSubject() {
      return this.subject;
    }
  }
  //@formatter:on

  @Enumerated(EnumType.STRING)
  @NotNull
  private EmailTemplate template;

  private String to;

  private Map<String, Object> parameters;

  public EmailMessage(String toRecipient, EmailTemplate template, Map<String, Object> parameters) {
    this.template = template;
    this.to = toRecipient;
    this.parameters = parameters;
  }

  public EmailTemplate getTemplate() {
    return this.template;
  }

  public void setTemplate(EmailTemplate template) {
    this.template = template;
  }

  public String getTo() {
    return this.to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public Map<String, Object> getParameters() {
    return this.parameters;
  }

  public void setParameters(Map<String, Object> parameters) {
    this.parameters = parameters;
  }

  @Override
  public String toString() {
    return super.toString() + " [template=" + this.template + "]" + " [to=" + this.to + "]";
  }
}
