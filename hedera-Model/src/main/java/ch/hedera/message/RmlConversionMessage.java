/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - RmlConversionMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.message;

import ch.hedera.model.RdfFormat;

public class RmlConversionMessage extends LongProcessingMessage {

  private static final long serialVersionUID = -7096041436011000525L;
  private String rmlFileId;
  private RdfFormat rdfFormat;

  public RmlConversionMessage(String sourceDatasetFileId, boolean isLongProcessing) {
    super(sourceDatasetFileId, isLongProcessing);
  }

  public RmlConversionMessage(String sourceDatasetFileId, String rmlFileId, RdfFormat rdfFormat) {
    super(sourceDatasetFileId);
    this.rmlFileId = rmlFileId;
    this.rdfFormat = rdfFormat;
  }

  public RmlConversionMessage(String sourceDatasetFileId, String rmlFileId, RdfFormat rdfFormat, boolean isLongProcessing) {
    super(sourceDatasetFileId, isLongProcessing);
    this.rmlFileId = rmlFileId;
    this.rdfFormat = rdfFormat;
  }

  public String getSourceDatasetFileId() {
    return this.resId;
  }

  public String getRmlFileId() {
    return this.rmlFileId;
  }

  public RdfFormat getRdfFormat() {
    return this.rdfFormat;
  }

  @Override
  public String toString() {
    return super.toString() + " rmlFileId=" + this.rmlFileId + ", rdfFormat=" + this.rdfFormat;
  }
}
