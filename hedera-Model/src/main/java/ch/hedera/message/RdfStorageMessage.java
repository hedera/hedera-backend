/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - RdfStorageMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.message;

import ch.hedera.model.RdfOperation;

public class RdfStorageMessage extends LongProcessingMessage {

  private static final long serialVersionUID = 5714141146009028874L;

  private String triplestoreDatasetId;
  private RdfOperation operation;

  public RdfStorageMessage(String resId, String triplestoreDatasetId, RdfOperation operation, boolean largeFile) {
    super(resId, largeFile);
    this.triplestoreDatasetId = triplestoreDatasetId;
    this.operation = operation;
  }

  public String getTriplestoreDatasetId() {
    return this.triplestoreDatasetId;
  }

  public RdfOperation getOperation() {
    return this.operation;
  }

  @Override
  public String toString() {
    return super.toString() + " triplestoreDatasetId=" + this.triplestoreDatasetId + ", operation=" + this.operation;
  }
}
