/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ProjectPersonRoleCacheMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.message.cache;

import ch.unige.solidify.message.CacheMessage;

public class ProjectPersonRoleCacheMessage extends CacheMessage {

  private static final long serialVersionUID = 1L;

  protected String projectId;
  protected String personId;
  protected String roleId;

  /******************/

  public ProjectPersonRoleCacheMessage(String personId, String projectId, String roleId) {
    this.personId = personId;
    this.projectId = projectId;
    this.roleId = roleId;
  }

  public String getProjectId() {
    return this.projectId;
  }

  /******************/

  public String getPersonId() {
    return this.personId;
  }

  public String getRoleId() {
    return this.roleId;
  }

  @Override
  public String toString() {
    return "CacheMessage [resourceClass=ProjectPersonRoleCacheMessage, personId=" + this.personId + ", projectId="
            + this.projectId + ", role=" + this.roleId + "]";
  }
}
