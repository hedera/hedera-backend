/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - NotificationMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.message;

import java.io.Serializable;

public class NotificationMessage implements Serializable {

    private static final long serialVersionUID = 6251638532680367587L;

    private final String resId;

    public NotificationMessage(String resId) {
        this.resId = resId;
    }

    public String getResId() {
        return this.resId;
    }

    @Override
    public String toString() {
        return "NotificationMessage [resId=" + this.resId + "]";
    }

}
