/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - LongProcessingMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.message;

public abstract class LongProcessingMessage extends HederaMessage {

  private static final long serialVersionUID = -7971167360046929334L;

  private boolean isLongProcessing = false;

  protected LongProcessingMessage(String resId) {
    super(resId);
  }

  protected LongProcessingMessage(String resId, boolean isLongProcessing) {
    super(resId);
    this.isLongProcessing = isLongProcessing;
  }

  public boolean isLongProcessing() {
    return this.isLongProcessing;
  }

  public void setLongProcessing(boolean isLongProcessing) {
    this.isLongProcessing = isLongProcessing;
  }

  @Override
  public String toString() {
    return super.toString() + " [isLongProcessing=" + this.isLongProcessing + "]";
  }
}
