/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ResourceName.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.rest;

import ch.unige.solidify.SolidifyConstants;

public class ResourceName {

  public static final String AUTHORIZED_PROJECT = "authorized-projects";
  // Resources
  public static final String FUNDING_AGENCY = "funding-agencies";
  public static final String ARCHIVE_THUMBNAIL = "thumbnails";
  // Miscellaneous
  public static final String HISTORY = "history";
  public static final String INSTITUTION = "institutions";
  public static final String ITEM = "items";
  public static final String LANGUAGE = "languages";
  public static final String LICENSE = "licenses";
  public static final String LOGIN = "shiblogin";
  public static final String MODULE = "modules";
  public static final String MONITOR = "monitor";
  public static final String CURRENT_VERSION = "current-version";
  public static final String ORCID = "orcid";
  public static final String PROJECT = "projects";
  public static final String PERSON = "people";
  public static final String PROFILE = "profile";
  public static final String QUERY = "queries";
  public static final String RDF_CLASS = "rdf-classes";
  public static final String RDF_PROPERTY = "rdf-properties";

  public static final String SOURCE_DATA_FILE = "sourceDataFile";
  public static final String SOURCE_DATASET_FILE = "source-dataset-files";

  public static final String SCHEDULED_TASKS = "scheduled-tasks";
  public static final String GLOBAL_BANNER = "global-banners";
  public static final String SOURCE_DATASET = "source-datasets";
  public static final String TRIPLESTORE = "triplestore";
  public static final String TRIPLESTORE_DATASET = "triplestore-datasets";
  public static final String SETTINGS = "settings";
  public static final String ONTOLOGY = "ontologies";
  public static final String RESEARCH_OBJECT = "research-objects";
  public static final String RESEARCH_DATA_FILE = "research-data-files";
  public static final String RESEARCH_OBJECT_TYPE = "research-object-types";
  public static final String RDF_DATASET_FILE = "rdf-dataset-files";
  public static final String PUBLIC_INDEX = "public-index";
  public static final String PRIVATE_INDEX = "private-index";

  public static final String ROLE = "roles";
  public static final String APPLICATION_ROLE = "application-roles";
  public static final String SCHEMA = "schema";
  public static final String MANIFEST = "manifests";
  public static final String COLLECTION = "collections";

  public static final String USER = "users";
  public static final String SYSTEM_PROPERTY = "system-properties";
  public static final String XSL = "xsl";
  public static final String RML = "rmls";
  public static final String DETAIL = "details";

  public static final String IIIF_COLLECTION_CONFIG = "iiif-collection-configs";

  public static final String ID_MAPPING = "id-mappings";

  private ResourceName() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}
