/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - UrlPath.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.rest;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;

import ch.hedera.HederaConstants;

public final class UrlPath {
  //@formatter:off
  // ==========
  // IIIF PROXY
  // ==========
  public static final String IIIF_PROXY = "/" + ModuleName.IIIF;
  public static final String IIIF_MANIFEST = IIIF_PROXY + "/" + ResourceName.MANIFEST;
  public static final String IIIF_COLLECTION = IIIF_PROXY + "/" + ResourceName.COLLECTION;

  // ===========
  // SPAQL PROXY
  // ===========
  public static final String SPARQL_PROXY = "/" + ModuleName.SPARQL;
  
  // ========
  // LD PROXY
  // ========
  public static final String LINKED_DATA_PROXY = "/" + ModuleName.LINKED_DATA;

  // ==============
  // ONTOLOGY PROXY
  // ==============
  public static final String ONTOLOGY_PROXY = "/" + ModuleName.ONTOLOGY;

  // ==========
  // Access
  // ==========
  public static final String ACCESS = "/" + ModuleName.ACCESS;
  public static final String ACCESS_PROJECT= ACCESS + "/" + ResourceName.PROJECT;
  public static final String ACCESS_PROJECT_RESEARCH_DATA_FILE = ACCESS_PROJECT + "/" + HederaConstants.URL_PROJECT + "/" + ResourceName.RESEARCH_DATA_FILE ;
  public static final String ACCESS_PROJECT_RESEARCH_OBJECT = ACCESS_PROJECT + "/" + HederaConstants.URL_PROJECT + "/" + ResourceName.RESEARCH_OBJECT ;
  public static final String ACCESS_PROJECT_RESEARCH_OBJECT_TYPE = ACCESS_PROJECT + "/" + HederaConstants.URL_PROJECT;
  public static final String ACCESS_RESEARCH_OBJECT = ACCESS + "/" + ResourceName.RESEARCH_OBJECT;
 
  // ==========
  // Admin
  // ==========
  public static final String ADMIN = "/" + ModuleName.ADMIN;
  public static final String ADMIN_AUTHORIZED_PROJECT = ADMIN + "/" + ResourceName.AUTHORIZED_PROJECT;
  public static final String ADMIN_FUNDING_AGENCY = ADMIN + "/" + ResourceName.FUNDING_AGENCY;
  public static final String ADMIN_INSTITUTION = ADMIN + "/" + ResourceName.INSTITUTION;
  public static final String ADMIN_LANGUAGE = ADMIN + "/" + ResourceName.LANGUAGE;
  public static final String ADMIN_LICENSE = ADMIN + "/" + ResourceName.LICENSE;
  public static final String ADMIN_LICENSE_IMPORT_FILE = "/" + ActionName.IMPORT + "/" + ActionName.FILE;
  public static final String ADMIN_LICENSE_IMPORT_LIST = "/" + ActionName.IMPORT + "/" + ActionName.LIST;
  public static final String ADMIN_OPEN_LICENSE_IMPORT_FILE = "/" + ActionName.IMPORT + "/open-license/" + ActionName.FILE;
  public static final String ADMIN_PERSON = ADMIN + "/" + ResourceName.PERSON;
  public static final String ADMIN_RESEARCH_OBJECT = ADMIN + "/" + ResourceName.RESEARCH_OBJECT;
  public static final String ADMIN_RESEARCH_OBJECT_TYPE = ADMIN + "/" + ResourceName.RESEARCH_OBJECT_TYPE;

  public static final String ADMIN_ROLE = ADMIN + "/" + ResourceName.ROLE;
  public static final String ADMIN_PROJECT = ADMIN + "/" + ResourceName.PROJECT;
  public static final String ADMIN_USER = ADMIN + "/" + ResourceName.USER;
  public static final String ADMIN_SYSTEM_PROPERTY = ADMIN + "/" + ResourceName.SYSTEM_PROPERTY;
  public static final String ADMIN_ROLE_APPLICATION = ADMIN + "/" + ResourceName.APPLICATION_ROLE;
  public static final String ADMIN_SCHEDULED_TASKS = ADMIN + "/" + ResourceName.SCHEDULED_TASKS;
  public static final String ADMIN_GLOBAL_BANNER = ADMIN + "/" + ResourceName.GLOBAL_BANNER;
  public static final String ADMIN_MODULE = ADMIN + "/" + ResourceName.MODULE;
  public static final String ADMIN_MONITOR = ADMIN + "/" + ResourceName.MONITOR;
  public static final String ADMIN_ONTOLOGY = ADMIN + "/" + ResourceName.ONTOLOGY;
  public static final String ADMIN_TRIPLESTORE_DATASET = ADMIN + "/" + ResourceName.TRIPLESTORE_DATASET;
  public static final String ADMIN_RML = ADMIN + "/" + ResourceName.RML;
  public static final String ADMIN_IIIF_COLLECTION_CONFIG = ADMIN + "/" + ResourceName.IIIF_COLLECTION_CONFIG;

  // =====================
  // Authorization Server
  // =====================
  public static final String AUTH = "/" + ModuleName.AUTH;
  public static final String AUTH_AUTHORIZE = AUTH + "/" + HederaActionName.AUTHORIZE;
  public static final String AUTH_CHECK_TOKEN = AUTH + "/" + HederaActionName.CHECK_TOKEN;
  public static final String AUTH_SHIBLOGIN = "/" + ResourceName.LOGIN;
  public static final String AUTH_TOKEN = AUTH + "/" + HederaActionName.TOKEN;
  public static final String AUTH_USER_ID = "{" + HederaActionName.USER_ID + "}";

  // ==========
  // Ingest
  // ==========
  public static final String INGEST = "/" + ModuleName.INGEST;
  public static final String INGEST_SOURCE_DATASET = INGEST + "/" + ResourceName.SOURCE_DATASET;
  public static final String INGEST_SOURCE_DATASET_FILE = INGEST + "/" + ResourceName.SOURCE_DATASET_FILE;
  public static final String INGEST_RESEARCH_DATA_FILE = INGEST + "/" + ResourceName.RESEARCH_DATA_FILE;
  public static final String INGEST_RDF_DATASET_FILE = INGEST + "/" + ResourceName.RDF_DATASET_FILE;
  public static final String INGEST_PUBLIC_INDEX = INGEST + "/" +  ResourceName.PUBLIC_INDEX;
  public static final String INGEST_PRIVATE_INDEX = INGEST + "/" +  ResourceName.PRIVATE_INDEX;
  
  // ==========
  // Triplestore
  // ==========
  public static final String TRIPLESTORE = "/" + ResourceName.TRIPLESTORE;
  public static final String TRIPLESTORE_SETTINGS = TRIPLESTORE + "/" + ResourceName.SETTINGS;
  public static final String ADMIN_TRIPLESTORE_SETTINGS = ADMIN + TRIPLESTORE_SETTINGS;


  public static final String TRIPLESTORE_QUERIES = TRIPLESTORE + "/" + ResourceName.QUERY;
  public static final String ADMIN_TRIPLESTORE_QUERIES = ADMIN + TRIPLESTORE_QUERIES;


  // ================
  // Resource Server
  // ================
  public static final String RES_SRV = "/" + ModuleName.RES_SRV;

  // @formatter on


  private UrlPath() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}
