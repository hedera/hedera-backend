/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ModuleName.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.rest;

import ch.unige.solidify.SolidifyConstants;

public class ModuleName {

  // Modules
  public static final String ACCESS = "access";
  public static final String ADMIN = "admin";
  public static final String AUTH = "oauth";
  public static final String INGEST = "ingest";
  public static final String RES_SRV = "resource-srv";
  public static final String SPARQL = "sparql";
  public static final String IIIF = "iiif";
  public static final String LINKED_DATA = "ld";
  public static final String ONTOLOGY = "ontologies";

  private ModuleName() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}
