/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - HederaMetadataVersion.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera;

import com.fasterxml.jackson.annotation.JsonValue;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;

public enum HederaMetadataVersion {
  V1_0("1.0");

  private static final String METS_PROFILE_PREFIX = "hedera_profile-";
  private static final String METS_SCHEMA_PREFIX = "hedera_mets-";
  private static final String PREMIS_3 = "premis-3.0.xsd";
  private static final String REP_INFO_SCHEMA_PREFIX = "hedera_datacite-";
  private static final String INDEXING_XSL = "hedera4indexing.xsl";

  public static HederaMetadataVersion fromVersion(String version) {
    switch (version) {
      case "1.0":
        return V1_0;
      default:
        throw new SolidifyRuntimeException("Wrong metadata version: " + version);
    }
  }

  public static HederaMetadataVersion getDefaultVersion() {
    return HederaMetadataVersion.V1_0;
  }

  private String version;

  private HederaMetadataVersion(String v) {
    this.version = v;
  }

  public String getAdministrativeInfoSchema() {
    return PREMIS_3;
  }

  public String getMetsProfile() {
    return METS_PROFILE_PREFIX + this.getVersion() + SolidifyConstants.XML_EXT;
  }

  public String getMetsSchema() {
    return METS_SCHEMA_PREFIX + this.getVersion() + SolidifyConstants.XSD_EXT;
  }

  public String getRepresentationInfoSchema() {
    switch (this.version) {
      case "1.0":
        return REP_INFO_SCHEMA_PREFIX + this.getVersion() + SolidifyConstants.XSD_EXT;
      default:
        throw new SolidifyRuntimeException("Wrong metadata version: " + this.version);
    }
  }

  public String getIndexingTransformation() {
    return INDEXING_XSL;
  }

  @JsonValue
  public String getVersion() {
    return this.version;
  }

  @Override
  public String toString() {
    return this.getVersion();
  }
}
