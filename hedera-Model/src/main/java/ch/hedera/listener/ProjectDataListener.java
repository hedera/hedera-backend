/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - ProjectDataListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.listener;

import jakarta.persistence.PostPersist;
import jakarta.persistence.PostRemove;
import jakarta.persistence.PostUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.config.SolidifyEventPublisher;

import ch.hedera.message.ProjectMessage;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.RdfDatasetFile.RdfDatasetFileStatus;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.SourceDataset;

public class ProjectDataListener {
  private static final Logger log = LoggerFactory.getLogger(ProjectDataListener.class);

  public void logData(String projectId) {
    log.trace("Sending message {}", projectId);
    SolidifyEventPublisher.getPublisher().publishEvent(new ProjectMessage(projectId));
  }

  @PostPersist
  private void logCreation(Object object) {
    if (object instanceof SourceDataset sourceDataset) {
      this.logData(sourceDataset.getProjectId());
    } else if (object instanceof ResearchDataFile researchDataFile) {
      this.logData(researchDataFile.getProjectId());
    }
  }

  @PostUpdate
  private void logUpdate(Object object) {
    if (object instanceof RdfDatasetFile rdfDatasetFile) {
      if (rdfDatasetFile.getStatusImport() == RdfDatasetFileStatus.IMPORTED
              || rdfDatasetFile.getStatusImport() == RdfDatasetFileStatus.NOT_IMPORTED) {
        this.logData(rdfDatasetFile.getProjectId());
      }
    }
  }

  @PostRemove
  private void logDeletion(Object object) {
    if (object instanceof SourceDataset sourceDataset) {
      this.logData(sourceDataset.getProjectId());
    } else if (object instanceof ResearchDataFile researchDataFile) {
      this.logData(researchDataFile.getProjectId());
    }
  }
}
