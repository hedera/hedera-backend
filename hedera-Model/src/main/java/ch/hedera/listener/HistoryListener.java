/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - HistoryListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.listener;

import java.time.Duration;
import java.time.OffsetDateTime;

import jakarta.persistence.PostPersist;
import jakarta.persistence.PostRemove;
import jakarta.persistence.PostUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.rest.Resource;

import ch.hedera.model.StatusHistory;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;

public class HistoryListener {
  private static final Logger log = LoggerFactory.getLogger(HistoryListener.class);

  @PostPersist
  private void logCreation(Object object) {
    final Resource res = (Resource) object;

    /*
     * Send a message to store object creation in history
     */
    final StatusHistory stsHistory = new StatusHistory(res.getClass().getSimpleName(), res.getResId(),
            this.generateCreationTimeForCreationStatus(res),
            res.getCreatedBy());
    log.trace("Sending message {}", stsHistory);
    SolidifyEventPublisher.getPublisher().publishEvent(stsHistory);

    /*
     * Send a message to store first object's status in history
     */
    this.logStatusHistory(object);
  }

  @PostUpdate
  @PostRemove
  private void logStatusHistory(Object object) {
    StatusHistory stsHistory = null;
    if (object instanceof ResearchDataFile researchDataFile) {
      stsHistory = new StatusHistory(researchDataFile.getClass().getSimpleName(), researchDataFile.getResId(), researchDataFile.getUpdateTime(),
              researchDataFile.getStatus().toString(), researchDataFile.getStatusMessage(), researchDataFile.getUpdatedBy());
    } else if (object instanceof SourceDataset sourceDataset) {
      stsHistory = new StatusHistory(sourceDataset.getClass().getSimpleName(), sourceDataset.getResId(),
              sourceDataset.getUpdateTime(), sourceDataset.getStatus().toString(), sourceDataset.getStatusMessage(),
              sourceDataset.getUpdatedBy());
    } else if (object instanceof SourceDatasetFile sourceDatasetFile) {
      stsHistory = new StatusHistory(sourceDatasetFile.getClass().getSimpleName(), sourceDatasetFile.getResId(),
              sourceDatasetFile.getUpdateTime(), sourceDatasetFile.getStatus().toString(), sourceDatasetFile.getStatusMessage(),
              sourceDatasetFile.getUpdatedBy());
    } else if (object instanceof RdfDatasetFile rdfDatasetFile) {
      stsHistory = new StatusHistory(rdfDatasetFile.getClass().getSimpleName(), rdfDatasetFile.getResId(),
              rdfDatasetFile.getUpdateTime(), rdfDatasetFile.getStatusImport().toString(), rdfDatasetFile.getStatusMessage(),
              rdfDatasetFile.getUpdatedBy());
    }
    if (stsHistory != null) {
      log.trace("Sending event {}", stsHistory);
      SolidifyEventPublisher.getPublisher().publishEvent(stsHistory);
    }
  }

  private OffsetDateTime generateCreationTimeForCreationStatus(Resource res) {
    // Subtract 1 ms to correct the ascending descending sort.
    // The event created is not exactly the same time as the first StatusHistory
    return res.getCreationTime().minus(Duration.ofMillis(1));
  }
}
