/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - SparqlUpdateTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.util;

import java.io.InputStream;

import org.apache.jena.arq.querybuilder.UpdateBuilder;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.update.UpdateRequest;

import ch.unige.solidify.SolidifyConstants;

import ch.hedera.model.RdfFormat;

public class SparqlUpdateTool extends SparqlBuilderTool {

  private SparqlUpdateTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  public static UpdateRequest updateToDelete() {
    final UpdateBuilder updateBuilder = new UpdateBuilder()
            .addDelete(SUBJECT_VARIABLE, PROPERTY_VARIABLE, VALUE_VARIABLE)
            .addWhere(SUBJECT_VARIABLE, PROPERTY_VARIABLE, VALUE_VARIABLE);
    return SparqlBuilderTool.createUpdate(updateBuilder);
  }

  public static UpdateRequest updateToDeleteRdfMetadata(String objectUri, InputStream rdfMetadata) {
    // Parse the RDFMetadata using Jena to extract each of the properties to then build the query
    Model model = RdfTool.loadModel(rdfMetadata, RdfFormat.TURTLE);
    final UpdateBuilder updateBuilder = new UpdateBuilder()
            .addDelete(model);
    return SparqlBuilderTool.createUpdate(updateBuilder);
  }
}
