/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - RdfTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.springframework.http.MediaType;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

import ch.hedera.HederaConstants;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.humanities.Ontology;

public class RdfTool {

  private static final String UNSUPPORTED_FORMAT = "Unsupported RDF format: ";

  public static final String RDF_CLASS_URI = "http://www.w3.org/2000/01/rdf-schema#Class";
  public static final String RDF_PROPERTY_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#Property";
  public static final String OWL_CLASS_URI = "http://www.w3.org/2002/07/owl#Class";
  public static final String OWL_PROPERTY_URI = "http://www.w3.org/2002/07/owl#ObjectProperty";
  public static final String L54_SAME_AS_URI = "http://www.ics.forth.gr/isl/CRMdig/L54_is_same-as";

  private RdfTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  public static Model getRdfModel(Path rdfPath) {
    return RDFDataMgr.loadModel(rdfPath.toString());
  }

  public static Model getRdfModel(Path rdfPath, RdfFormat rdfFormat) throws IOException {
    return RdfTool.loadModel(FileTool.getInputStream(rdfPath), rdfFormat);
  }

  public static void wellformed(Path dataFile, RdfFormat rdfFormat) throws IOException {
    wellformed(FileTool.getInputStream(dataFile), rdfFormat);
  }

  public static void wellformed(String dataString, RdfFormat rdfFormat) {
    wellformed(StringTool.toInputStream(dataString), rdfFormat);
  }

  public static void wellformed(InputStream inputStream, RdfFormat rdfFormat) {
    try {
      loadModel(inputStream, rdfFormat);
    } catch (Exception e) {
      // If an exception is raised, the syntax is invalid
      throw new SolidifyCheckingException(
              "Error during the well-formedness verification for " + rdfFormat.getName() + " syntax of the file: " + e.getMessage());
    }
  }

  public static boolean isPartOfOntology(String rdfType, Ontology ontology) {
    final Model model = RdfTool.loadModel(new ByteArrayInputStream(ontology.getResourceFile().getFileContent()), ontology.getFormat());
    final String rdfTypeUri = ontology.getBaseUri() + rdfType;
    return RdfTool.isRdfClass(model, rdfTypeUri)
            || RdfTool.isRdfProperty(model, rdfTypeUri);
  }

  public static boolean isRdfClass(Model model, String rdfTypeUri) {
    final Query query = SparqlQueryTool.queryToGetNumberOfRdfTypes(rdfTypeUri, SparqlBuilderTool.ONTOLOGY_RDF_CLASS);
    return checkIfQueryHasOneResult(model, query);
  }

  public static boolean isRdfProperty(Model model, String rdfTypeUri) {
    final Query query = SparqlQueryTool.queryToGetNumberOfRdfTypes(rdfTypeUri, SparqlBuilderTool.ONTOLOGY_RDF_PROPERTY);
    return checkIfQueryHasOneResult(model, query);
  }

  private static boolean checkIfQueryHasOneResult(Model model, Query query) {
    try (QueryExecution localExecution = QueryExecutionFactory.create(query, model)) {
      ResultSet resultSet = localExecution.execSelect();
      while (resultSet.hasNext()) {
        QuerySolution solution = resultSet.nextSolution();
        int count = solution.getLiteral(SparqlBuilderTool.QUERY_VARIABLE).getInt();
        return count == 1;
      }
    }
    return false;
  }

  public static Model loadModel(InputStream inputStream, RdfFormat rdfFormat) {
    // Create an RDF model
    Model model = ModelFactory.createDefaultModel();
    // Read the OWL ontology file and add its content to the model
    switch (rdfFormat) {
      case RDF_XML -> model.read(inputStream, null);
      case JSON_LD, N_TRIPLES, TURTLE -> model.read(inputStream, null, rdfFormat.getName());
      default -> throw new SolidifyCheckingException("Unsupported format: " + rdfFormat);
    }
    return model;
  }

  public static void validate(Path dataFile, RdfFormat rdfFormat) throws IOException {
    validate(FileTool.getInputStream(dataFile), rdfFormat);
  }

  public static void validate(String dataString, RdfFormat rdfFormat) {
    validate(StringTool.toInputStream(dataString), rdfFormat);
  }

  public static void validate(InputStream inputStream, RdfFormat rdfFormat) {
    try {
      // TODO : loading with strict check
      Model model = loadModel(inputStream, rdfFormat);
      StmtIterator iter = model.listStatements();
      while (iter.hasNext()) {
        iter.nextStatement();
      }
    } catch (Exception e) {
      // If an exception is raised, the syntax is invalid
      throw new SolidifyCheckingException("Error during the validition for " + rdfFormat.getName() + " syntax of the file: " + e.getMessage());
    }
  }

  public static String getContentType(RdfFormat rdfFormat) {
    return switch (rdfFormat) {
      case JSON_LD -> HederaConstants.APPLICATION_JSON_LD_MIME_TYPE;
      case N3 -> HederaConstants.TEXT_N3_MIME_TYPE;
      case N_QUADS -> HederaConstants.APPLICATION_N_QUADS_MIME_TYPE;
      case N_TRIPLES -> HederaConstants.APPLICATION_N_TRIPLES_MIME_TYPE;
      case RDF_JSON -> MediaType.APPLICATION_JSON_VALUE;
      case RDF_XML -> HederaConstants.APPLICATION_RDF_XML_MIME_TYPE;
      case TRIG -> HederaConstants.APPLICATION_TRIG_MIME_TYPE;
      case TRIX -> HederaConstants.APPLICATION_TRIX_MIME_TYPE;
      case TURTLE -> HederaConstants.TEXT_TURTLE_MIME_TYPE;
      default -> throw new SolidifyValidationException(new ValidationError(UNSUPPORTED_FORMAT + rdfFormat.toString()));
    };
  }

  public static String getExtension(RdfFormat rdfFormat) {
    return switch (rdfFormat) {
      case JSON_LD -> "jsonld";
      case N3 -> "n3";
      case N_QUADS -> "nq";
      case N_TRIPLES -> "nt";
      case RDF_JSON -> "rj";
      case RDF_XML -> "rdf";
      case TRIG -> "trig";
      case TRIX -> "trix";
      case TURTLE -> "ttl";
      default -> throw new SolidifyValidationException(new ValidationError(UNSUPPORTED_FORMAT + rdfFormat.toString()));
    };
  }

  public static String getJenaLanguage(RdfFormat rdfFormat) {
    return switch (rdfFormat) {
      case JSON_LD -> "JSON-LD";
      case N3 -> "N3";
      case N_QUADS -> "N-Quads";
      case N_TRIPLES -> "N-Triples";
      case RDF_JSON -> "RDF/JSON";
      case RDF_XML -> "RDF/XML";
      case TRIG -> "TriG";
      case TRIX -> "TriX";
      case TURTLE -> "Turtle";
      default -> throw new SolidifyValidationException(new ValidationError(UNSUPPORTED_FORMAT + rdfFormat.toString()));
    };
  }

  public static org.apache.jena.riot.RDFFormat getJenaRdfFormat(RdfFormat rdfFormat) {
    return switch (rdfFormat) {
      case JSON_LD -> RDFFormat.JSONLD;
      case N_QUADS -> RDFFormat.NQUADS;
      case N_TRIPLES -> RDFFormat.NTRIPLES;
      case RDF_JSON -> RDFFormat.RDFJSON;
      case RDF_XML -> RDFFormat.RDFXML;
      case TRIG -> RDFFormat.TRIG;
      case TRIX -> RDFFormat.TRIX;
      case TURTLE -> RDFFormat.TURTLE;
      default -> throw new SolidifyValidationException(new ValidationError(UNSUPPORTED_FORMAT + rdfFormat.toString()));
    };
  }

}
