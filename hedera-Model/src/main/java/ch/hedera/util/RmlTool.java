/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - RmlTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;

public class RmlTool {

  public static final String FILE_PLACE_HOLDER = "%(file)";
  public static final String PROJECT_PLACE_HOLDER = "%(projectUri)";
  public static final String RESEARCH_DATA_FILE_ACCESS_URL_PLACE_HOLDER = "%(researchDataFileAccessUrl)";

  private RmlTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  public static Map<String, String> getPlaceHolderValues(URI ldProjectBaseUri) {
    return Map.of(PROJECT_PLACE_HOLDER, ldProjectBaseUri.toString());
  }

  public static Map<String, String> getPlaceHolderValues(URI ldProjectBaseUri, Path sourcePath, String researchDataFileAccessUrl) {
    return Map.of(
            PROJECT_PLACE_HOLDER, ldProjectBaseUri.toString(),
            FILE_PLACE_HOLDER, sourcePath.toString(),
            RESEARCH_DATA_FILE_ACCESS_URL_PLACE_HOLDER, researchDataFileAccessUrl);
  }

  public static String replacePlaceHoldersInFile(Path rmlFilePath, Map<String, String> parameters) {
    try {
      return replacePlaceHoldersInFile(Files.newInputStream(rmlFilePath), parameters);
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Unable to transform RML file");
    }
  }

  public static String replacePlaceHoldersInFile(InputStream inputStream, Map<String, String> parameters) {
    try (Scanner scanner = new Scanner(inputStream);
            StringWriter writer = new StringWriter()) {
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        for (Entry<String, String> param : parameters.entrySet()) {
          line = line.replace(param.getKey(), param.getValue());
        }
        writer.append(line).append("\n");
      }
      return writer.toString();
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Unable to transform RML input stream");
    }
  }

}
