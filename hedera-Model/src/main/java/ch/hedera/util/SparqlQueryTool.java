/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - SparqlQueryTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.util;

import java.io.InputStream;
import java.util.List;

import org.apache.jena.arq.querybuilder.SelectBuilder;
import org.apache.jena.arq.querybuilder.WhereBuilder;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;

import ch.unige.solidify.SolidifyConstants;

import ch.hedera.model.RdfFormat;
import ch.hedera.model.settings.ResearchObjectType;

public class SparqlQueryTool extends SparqlBuilderTool {

  private SparqlQueryTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  public static Query queryToCountTotal(String countVariable) {
    return SparqlBuilderTool.createQuery("SELECT (count(*) as " + countVariable + ") WHERE { ?s ?p ?o }");
  }

  public static Query queryToCount(Query query, String countVariable) {
    return SparqlBuilderTool.createQuery("SELECT (count(*) as " + countVariable + ") WHERE { " + query.toString() + "}");
  }

  public static Query queryToGetNumberOfRdfTypes(String rdfTypeUri, List<String> rdfTypeList) {
    final SelectBuilder queryBuilder = new SelectBuilder()
            .addVar("count(*)", QUERY_VARIABLE)
            .addWhere(SparqlQueryTool.addCriteriaUnion(SparqlBuilderTool.getNode(rdfTypeUri), rdfTypeList));
    return SparqlBuilderTool.createQuery(queryBuilder);
  }

  public static Query queryToListSubjectProperties(String subjectName, String propertyVariable, String valueVariable) {
    final String subject = SparqlBuilderTool.getNode(subjectName);
    final SelectBuilder queryBuilder = new SelectBuilder()
            .setDistinct(true)
            .addVar(propertyVariable).addVar(valueVariable)
            .addWhere(subject, propertyVariable, valueVariable);
    return SparqlBuilderTool.createQuery(queryBuilder);
  }

  public static Query queryToListResearchObjectTypes(String projectShortName, String researchObjectTypeVariable) {
    SelectBuilder queryBuilder = new SelectBuilder()
            .setDistinct(true)
            .addVar(researchObjectTypeVariable)
            .addWhere(SUBJECT_VARIABLE, PROPERTY_VARIABLE, OBJECT_VARIABLE)
            .addBind("strbefore(strafter(str(" + SUBJECT_VARIABLE + "),'" + projectShortName + "/'), '/')", researchObjectTypeVariable);
    return SparqlBuilderTool.createQuery(queryBuilder);
  }

  public static Query queryToListResearchObjectsOfTypes(String projectShortName, String researchObjectName, String researchObjectVariable) {
    SelectBuilder queryBuilder = new SelectBuilder()
            .setDistinct(true)
            .addVar(researchObjectVariable)
            .addWhere(researchObjectVariable, PROPERTY_VARIABLE, OBJECT_VARIABLE)
            .addFilter("regex(str(" + researchObjectVariable + "),'" + projectShortName + "/" + researchObjectName + "')");
    return SparqlBuilderTool.createQuery(queryBuilder);
  }

  public static Query queryToListRdfTypes(String typeVariable) {
    final SelectBuilder queryBuilder = new SelectBuilder()
            .setDistinct(true)
            .addVar(typeVariable)
            .addWhere(SUBJECT_VARIABLE, "a", typeVariable);
    return SparqlBuilderTool.createQuery(queryBuilder);
  }

  public static Query queryToListSubjectsOfRdfType(String subjectVariable, String rdfType) {
    final SelectBuilder queryBuilder = new SelectBuilder()
            .setDistinct(true)
            .addVar(subjectVariable)
            .addWhere(SparqlQueryTool.selectForRdfType(subjectVariable, rdfType));
    return SparqlBuilderTool.createQuery(queryBuilder);
  }

  public static Query queryToListSubjects(String subjectVariable) {
    final SelectBuilder queryBuilder = new SelectBuilder()
            .setDistinct(true)
            .addVar(subjectVariable)
            .addWhere(subjectVariable, PROPERTY_VARIABLE, VALUE_VARIABLE);
    return SparqlBuilderTool.createQuery(queryBuilder);
  }

  public static Query queryToListRdfClasses(String baseUriFilter, String classVariable) {
    return QueryFactory.create(SparqlQueryTool.queryToGetInstancesOfRdfTypes(classVariable, baseUriFilter, ONTOLOGY_RDF_CLASS));
  }

  public static Query queryToListRdfProperties(String baseUriFilter, String propVariable) {
    return SparqlQueryTool.queryToGetInstancesOfRdfTypes(propVariable, baseUriFilter, ONTOLOGY_RDF_PROPERTY);
  }

  private static Query queryToGetInstancesOfRdfTypes(String variable, String baseUriFilter, List<String> rdfTypeList) {
    final SelectBuilder queryBuilder = new SelectBuilder()
            .setDistinct(true)
            .addVar(variable)
            .addFilter("isIRI(" + variable + ")")
            .addFilter("strstarts(str(" + variable + "),'" + baseUriFilter + "')")
            .addWhere(SparqlQueryTool.addCriteriaUnion(variable, rdfTypeList));
    return SparqlBuilderTool.createQuery(queryBuilder);
  }

  public static Query queryToListSubjectsOfResearchObjectType(String variableName, ResearchObjectType researchObjectType) {
    final SelectBuilder queryBuilder = new SelectBuilder()
            .setDistinct(true)
            .addVar(variableName)
            .addWhere(SparqlQueryTool.selectForRdfType(variableName,
                    researchObjectType.getOntology().getBaseUri() + researchObjectType.getRdfType()))
            .addFilter("regex(str(" + variableName + "),'" + researchObjectType.getName() + "')");
    return SparqlBuilderTool.createQuery(queryBuilder);
  }

  public static Query queryToListResearchObjectType(String ontoBase, String ontoProperty) {
    final SelectBuilder queryBuilder = new SelectBuilder()
            .setDistinct(true)
            .addVar(SUBJECT_VARIABLE)
            .addWhere(SparqlQueryTool.selectForRdfType(SUBJECT_VARIABLE, ontoBase + ontoProperty));
    return SparqlBuilderTool.createQuery(queryBuilder);
  }

  public static Query queryAsObject(String objectUri) {
    final SelectBuilder queryBuilder = new SelectBuilder()
            .setDistinct(true)
            .addVar(SUBJECT_VARIABLE).addVar(PROPERTY_VARIABLE)
            .addWhere(SUBJECT_VARIABLE, PROPERTY_VARIABLE, SparqlBuilderTool.getNode(objectUri));
    return SparqlBuilderTool.createQuery(queryBuilder);
  }

  public static Query queryToFilterByRdfMetadata(String objectUri, InputStream rdfMetadata) {
    final SelectBuilder queryBuilder = new SelectBuilder()
            .setDistinct(true)
            .addVar(PROPERTY_VARIABLE).addVar(OBJECT_VARIABLE)
            .addWhere(SparqlBuilderTool.getNode(objectUri), PROPERTY_VARIABLE, OBJECT_VARIABLE);
    // Parse the RDFMetadata using Jena to extract each of the properties to then build the query
    Model model = RdfTool.loadModel(rdfMetadata, RdfFormat.TURTLE);
    // Iterate over all statements in the model
    StmtIterator iter = model.listStatements();
    while (iter.hasNext()) {
      Statement stmt = iter.nextStatement();
      Property predicate = stmt.getPredicate();

      // For each of the properties, it is needed to add a filter clause in the query
      queryBuilder.addFilter("not exists { ?s " + SparqlBuilderTool.getNode(predicate.toString()) + " ?o }");
    }
    return SparqlBuilderTool.createQuery(queryBuilder);
  }

  public static Query queryToListAllTriples(String subjectVariable, String propertyVariable, String objectVariable) {
    SelectBuilder queryBuilder = new SelectBuilder()
            .setDistinct(true)
            .addVar(ALL_VARIABLES)
            .addWhere(subjectVariable, propertyVariable, objectVariable);
    return SparqlBuilderTool.createQuery(queryBuilder);
  }

  private static WhereBuilder addCriteriaUnion(String variable, List<String> rdfTypeList) {
    final WhereBuilder whereBuilder = new WhereBuilder();
    boolean first = true;
    for (String typeUri : rdfTypeList) {
      if (first) {
        whereBuilder.addWhere(SparqlQueryTool.selectForRdfType(variable, typeUri));
        first = false;
      } else {
        whereBuilder.addUnion(new WhereBuilder().addWhere(SparqlQueryTool.selectForRdfType(variable, typeUri)));
      }
    }
    return whereBuilder;
  }

  private static WhereBuilder selectForRdfType(String subjectVariable, String rdfType) {
    return new WhereBuilder().addWhere(subjectVariable, RDF.type, SparqlBuilderTool.getNode(rdfType));
  }
}
