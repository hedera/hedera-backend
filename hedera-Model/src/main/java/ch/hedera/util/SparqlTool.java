/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - SparqlTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.util;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QueryParseException;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.syntax.Element;
import org.apache.jena.sparql.syntax.ElementGroup;
import org.apache.jena.sparql.syntax.ElementService;
import org.apache.jena.update.UpdateFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.validation.ValidationError;

public class SparqlTool {
  private static final Logger logger = LoggerFactory.getLogger(SparqlTool.class);

  private SparqlTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  public static void validateSparqlSyntax(String sparqlQuery) {
    try {
      QueryFactory.create(sparqlQuery);
    } catch (QueryParseException e) {
      logger.debug("SPARQL query is not valid : {}", sparqlQuery);
      throw new SolidifyCheckingException("SPARQL query is not valid: " + e.getMessage());
    }
  }

  public static boolean queryContainsVariable(Query sparqlQuery, String variable) {
    final String variableName = SparqlBuilderTool.getVariableName(variable);
    try {
      List<Var> vars = sparqlQuery.getProjectVars();
      Optional<Var> subjectVarOpt = vars.stream().filter(v -> v.getVarName().equals(variableName)).findFirst();
      return subjectVarOpt.isPresent();
    } catch (QueryParseException e) {
      logger.debug("SPARQL query is not valid : {}", sparqlQuery);
      throw new SolidifyCheckingException("SPARQL query is not valid: " + e.getMessage());
    }
  }

  /**
   * This return true if the sparql method is not an update or delete
   * it will throw an exception if the query is delete or update and return true
   *
   * @param sparqlQuery
   * @return true if query is a UPDATE or DELETE
   */
  public static boolean isDeleteOrUpdateQuery(String sparqlQuery) {
    try {
      QueryFactory.create(sparqlQuery);
      return false;
    } catch (QueryParseException e1) {
      try {
        UpdateFactory.create(sparqlQuery);
        logger.debug("SPARQL query is of type DELETE or UPDATE : {}", sparqlQuery);
        return true;
      } catch (QueryParseException e2) {
        throw new SolidifyValidationException(new ValidationError("Syntax error in SPARQL query"), e2.getMessage());
      }
    }
  }

  public static boolean isFederatedQuery(String sparqlQuery) {
    // Check for the presence of SERVICE clauses
    logger.debug("SPARQL query is a federate query : {}", sparqlQuery);
    return sparqlQuery.contains("SERVICE");
  }

  public static Set<String> extractEndpoints(String sparqlQuery) {
    Query query = QueryFactory.create(sparqlQuery);
    Set<String> endpoints = new HashSet<>();
    if (query.getQueryPattern() instanceof ElementGroup queryPattern) {
      logger.debug("Extracting endpoints from sparql query: {}", sparqlQuery);
      for (Element element : queryPattern.getElements()) {
        if (((ElementService) element).getServiceNode() != null) {
          logger.debug("Endpoint found : {}", element);
          endpoints.add(((ElementService) element).getServiceNode().getURI());
        }
      }
    }
    return endpoints;
  }
}
