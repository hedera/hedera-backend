/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - SparqlBuilderTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.util;

import java.util.List;

import org.apache.jena.arq.querybuilder.ConstructBuilder;
import org.apache.jena.arq.querybuilder.SelectBuilder;
import org.apache.jena.arq.querybuilder.UpdateBuilder;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.update.UpdateRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;

public abstract class SparqlBuilderTool {

  private static final Logger log = LoggerFactory.getLogger(SparqlBuilderTool.class);

  public static final List<String> ONTOLOGY_RDF_CLASS = List.of(
          RdfTool.RDF_CLASS_URI,
          RdfTool.OWL_CLASS_URI);
  public static final List<String> ONTOLOGY_RDF_PROPERTY = List.of(
          RdfTool.RDF_PROPERTY_URI,
          RdfTool.OWL_PROPERTY_URI);

  public static final String ALL_VARIABLES = "*";
  public static final String SUBJECT_VARIABLE = "?s";
  public static final String PROPERTY_VARIABLE = "?p";
  public static final String OBJECT_VARIABLE = "?o";
  public static final String VALUE_VARIABLE = "?v";
  public static final String QUERY_VARIABLE = "?x";

  public static final String SUBJECT_FULL_NAME_VARIABLE = "?subject";
  public static final String PREDICATE_FULL_NAME_VARIABLE = "?predicate";
  public static final String OBJECT_FULL_NAME_VARIABLE = "?object";

  enum Sparqltype {
    QUERY, CONSTRUCT, UDPATE
  }

  protected SparqlBuilderTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  public static String getVariableName(String variable) {
    if (!variable.startsWith("?")) {
      throw new SolidifyCheckingException(variable + " is not a SPARQL variable.");
    }
    return variable.substring(1);
  }

  private static void log(Sparqltype sparqlType, String queryStr) {
    SparqlBuilderTool.log.debug("SPARQL {}:\n{}\n", sparqlType, queryStr);
  }

  protected static String getNode(String nodeStr) {
    return "<" + nodeStr + ">";
  }

  protected static Query createQuery(SelectBuilder queryB) {
    SparqlBuilderTool.log(Sparqltype.QUERY, queryB.toString());
    return queryB.build();
  }

  public static Query createQuery(String queryStr) {
    SparqlBuilderTool.log(Sparqltype.QUERY, queryStr);
    return QueryFactory.create(queryStr);
  }

  protected static Query createConstruct(ConstructBuilder constructB) {
    SparqlBuilderTool.log(Sparqltype.CONSTRUCT, constructB.toString());
    return constructB.build();
  }

  protected static UpdateRequest createUpdate(UpdateBuilder updateB) {
    SparqlBuilderTool.log(Sparqltype.UDPATE, updateB.toString());
    return updateB.buildRequest();
  }

}
