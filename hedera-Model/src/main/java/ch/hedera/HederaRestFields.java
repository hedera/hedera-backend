/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - HederaRestFields.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera;

public class HederaRestFields {

  public static final String RES_ID_FIELD = "resId";
  public static final String AIP_ID_FIELD = "aipId";
  public static final String ARCHIVE_ID_FIELD = "archiveId";
  public static final String USER_FIELD = "user";
  public static final String APPLICATION_ROLE_FIELD = "applicationRole";
  public static final String RATING_TYPE_FIELD = "ratingType";
  public static final String RETENTION_FIELD = "retention";
  public static final String DISPOSITION_APPROVAL_FIELD = "dispositionApproval";
  public static final String PROJECT_FIELD = "project";
  public static final String LEVEL_FIELD = "level";
  public static final String VIEW_NUMBER_FIELD = "viewNumber";
  public static final String DOWNLOAD_NUMBER_FIELD = "downloadNumber";
  public static final String METADATA_VERSION_FIELD = "metadataVersion";
  public static final String PROJECT_ID_FIELD = "projectId";
  public static final String RELATIVE_LOCATION_FIELD = "relativeLocation";
  public static final String STATUS_FIELD = "status";
  public static final String STATUS_MESSAGE_FIELD = "statusMessage";
  public static final String PUBLIC_ORDER_FIELD = "publicOrder";
  public static final String AIP_PACKAGES_FIELD = "aipPackages";
  public static final String DIP_PACKAGES_FIELD = "dipPackages";
  public static final String QUERY_TYPE_FIELD = "queryType";
}
