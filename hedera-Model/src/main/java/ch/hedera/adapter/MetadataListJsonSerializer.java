/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - MetadataListJsonSerializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.adapter;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import ch.hedera.model.xml.hedera.v1.researchObject.Metadata;
import ch.hedera.model.xml.hedera.v1.researchObject.MetadataList;

public class MetadataListJsonSerializer extends JsonSerializer<MetadataList> {

  @Override
  public void serialize(MetadataList metadataList, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
    jsonGenerator.writeStartObject();
    for (Metadata metadata : metadataList.getMetadata()) {
      jsonGenerator.writeStringField(metadata.getKey(), metadata.getValue());
    }
    jsonGenerator.writeEndObject();
  }

  @Override
  public Class<MetadataList> handledType() {
    return MetadataList.class;
  }

}
