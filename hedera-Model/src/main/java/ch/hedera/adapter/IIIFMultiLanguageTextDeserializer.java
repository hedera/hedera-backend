/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Model - IIIFMultiLanguageTextDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.adapter;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import ch.hedera.model.iiif.IIIFMultiLanguageText;

public class IIIFMultiLanguageTextDeserializer extends JsonDeserializer<IIIFMultiLanguageText> {

  @Override
  public IIIFMultiLanguageText deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {

    Map<String, List<String>> map = jsonParser.readValueAs(new TypeReference<>() {
    });

    IIIFMultiLanguageText text = new IIIFMultiLanguageText();
    for (Map.Entry<String, List<String>> entry : map.entrySet()) {
      List<String> t = entry.getValue();
      text.addText(entry.getKey(), t);
    }
    return text;
  }
}
