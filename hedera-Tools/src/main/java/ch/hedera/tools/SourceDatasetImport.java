/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Tools - SourceDatasetImport.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.tools;

import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.hedera.model.settings.Project;
import ch.hedera.tools.common.HederaTool;
import ch.hedera.tools.common.SourceDatasetImportService;

@Service
@Profile("source-dataset-import")
public class SourceDatasetImport extends HederaTool implements CommandLineRunner {

  @Value("${hedera.project}")
  protected String projectId;

  private SourceDatasetImportService sourceDatasetImportService;

  public SourceDatasetImport(SourceDatasetImportService sourceDatasetImportService) {
    this.sourceDatasetImportService = sourceDatasetImportService;
  }

  @Override
  public void run(String... arg0) throws Exception {
    if (this.importFolder.isEmpty()) {
      return;
    }
    Project project = this.sourceDatasetImportService.getProject(this.projectId);
    this.sourceDatasetImportService.importFolder(project, Paths.get(this.importFolder));
  }

}
