/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Tools - SourceCodeService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.tools;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ZipTool;

import ch.hedera.tools.common.HederaTool;
import ch.hedera.tools.config.GitConfig;

@Service
@Profile("git")
public class SourceCodeService extends HederaTool implements CommandLineRunner {

  private static final Logger log = LoggerFactory.getLogger(SourceCodeService.class);

  private GitConfig gitConfig;

  public SourceCodeService(GitConfig gitConfig) {
    this.gitConfig = gitConfig;
  }

  @Override
  public void run(String... args) throws Exception {
    final File workingDir = this.cloneGitRepository();

    // remove .git folder after cloning
    FileTool.deleteFolder(Paths.get(workingDir.getPath() + File.separatorChar + ".git"));
    final Path zipContainingCode = this.createZipAndUpload(workingDir);

    // clean space
    FileTool.deleteFolder(workingDir.toPath());
    FileTool.deleteFile(zipContainingCode);

  }

  private File cloneGitRepository() {
    final File workingDir;
    final Git git;
    final TransportConfigCallback transportConfigCallback;

    transportConfigCallback = new SshTransportConfigCallback.SshWithPubKeyAndPassphrase(this.gitConfig.getSshPassphrase(),
            this.gitConfig.getPrivateKeyFile());
    log.info("Git configurations are : {}, {}", this.gitConfig.getRepositoryUrl(), this.gitConfig.getBranch());

    try {
      workingDir = Files.createTempDirectory("code-repository").toFile();
      final CloneCommand cloneCommand;

      cloneCommand = Git.cloneRepository().setDirectory(workingDir).setTransportConfigCallback(transportConfigCallback)
              .setURI(this.gitConfig.getRepositoryUrl());

      if (!StringTool.isNullOrEmpty(this.gitConfig.getBranch())) {
        cloneCommand.setBranchesToClone(Collections.singleton("refs/heads/" + this.gitConfig.getBranch()))
                .setBranch("refs/heads/" + this.gitConfig.getBranch()).call();
      } else {
        // CLone all branches in order to get all tags/commits
        git = Git.cloneRepository().setURI(this.gitConfig.getRepositoryUrl()).setDirectory(workingDir)
                .setTransportConfigCallback(transportConfigCallback)
                .setCloneAllBranches(true)
                .call();
        if (!StringTool.isNullOrEmpty(this.gitConfig.getCommit())) {
          // checkout to commit
          git.checkout().setCreateBranch(true).setName("new-branch").setStartPoint(this.gitConfig.getCommit()).call();
        } else if (!StringTool.isNullOrEmpty(this.gitConfig.getTag())) {
          // checkout to tag
          git.checkout().setCreateBranch(true).setName("my-branch").setStartPoint("refs/tags/" + this.gitConfig.getTag()).call();
        }

      }
    } catch (IOException | GitAPIException e) {
      log.error("Error when trying to clone repository : {}", this.gitConfig.getRepositoryUrl());
      throw new SolidifyRuntimeException("Error when trying to clone repository.", e);
    }

    return workingDir;
  }

  private Path createZipAndUpload(File workingDir) throws IOException {
    final String projectName = this.gitConfig.getRepositoryUrl().substring(this.gitConfig.getRepositoryUrl().lastIndexOf('/') + 1)
            .replaceFirst("[.][^.]+$", "");

    final Path zipContainingCode = Files.createTempFile(projectName, ".zip");

    final ZipTool zip = new ZipTool(zipContainingCode.toUri());
    final boolean isZipFileGenerated = zip.zipFiles(workingDir.toPath());

    if (!isZipFileGenerated) {
      throw new SolidifyRuntimeException("Error when generating the Zip containing source code files");
    }

    return zipContainingCode;
  }

}
