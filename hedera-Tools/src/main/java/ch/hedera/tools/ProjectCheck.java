/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Tools - ProjectCheck.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.tools;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.FileTool;

import ch.hedera.tools.common.HederaTool;
import ch.hedera.tools.common.ProjectCheckService;

@Service
@Profile("project-check")
public class ProjectCheck extends HederaTool implements CommandLineRunner {

  private static final Logger log = LoggerFactory.getLogger(ProjectCheck.class);

  private ProjectCheckService projectCheckService;

  public ProjectCheck(ProjectCheckService projectCheckService) {
    this.projectCheckService = projectCheckService;
  }

  @Override
  public void run(String... arg0) throws Exception {
    final String prefix = "[Project Check]";
    if (!this.importFolder.isEmpty()) {
      log.info("{} Scanning folder : {}", prefix, this.importFolder);
      int count = 0;
      // Scan project short name list
      for (final Path projectShortName : FileTool.scanFolder(Paths.get(this.importFolder), Files::isDirectory)) {
        if (Files.isHidden(projectShortName)) {
          continue;
        }
        log.info("{} Project check for '{}' starting", prefix, projectShortName.getFileName());
        this.projectCheckService.checkProject(prefix, projectShortName);
        log.info("{} Project check for '{}' completed", prefix, projectShortName.getFileName());
        count++;
      }
      log.info("{} Project checking end '{}' : {} checked project(s)", prefix, this.importFolder, count);
    }
  }

}
