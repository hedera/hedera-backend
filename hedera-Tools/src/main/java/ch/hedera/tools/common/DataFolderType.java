/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Tools - DataFolderType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.tools.common;

import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.validation.ValidationError;

public enum DataFolderType {
  RML(HederaTool.HEDERA_RML_FOLDER), SOURCE_DATASET(HederaTool.HEDERA_SOURCE_DATASET_FOLDER), RESEARCH_DATA_FILE(
          HederaTool.HEDERA_RESEARCH_DATA_FILE_FOLDER);

  private final String name;

  DataFolderType(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public static DataFolderType fromFolderName(String folderName) {
    return switch (folderName) {
      case HederaTool.HEDERA_RML_FOLDER -> RML;
      case HederaTool.HEDERA_SOURCE_DATASET_FOLDER -> SOURCE_DATASET;
      case HederaTool.HEDERA_RESEARCH_DATA_FILE_FOLDER -> RESEARCH_DATA_FILE;
      default -> throw new SolidifyValidationException(new ValidationError("Unknown folder"));
    };
  }
}
