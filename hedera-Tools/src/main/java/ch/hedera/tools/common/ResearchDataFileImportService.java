/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Tools - ResearchDataFileImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.tools.common;

import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.PathResource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StandaloneRestClientTool;

import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.ProjectClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;

@Service
public class ResearchDataFileImportService extends HederaToolService {
  private static final Logger log = LoggerFactory.getLogger(ResearchDataFileImportService.class);

  private ResearchDataFileClientService researchDataFileClientService;

  public ResearchDataFileImportService(StandaloneRestClientTool restClientTool, ProjectClientService projectClientService,
          ResearchDataFileClientService researchDataFileClientService) {
    super(restClientTool, projectClientService);
    this.researchDataFileClientService = researchDataFileClientService;
  }

  public void importFolder(Project project, Path folder) {
    final String prefix = "[Research Data File]";
    log.info("{} Scanning folder : {}", prefix, folder);
    int count = 0;
    // Scan folder
    for (final Path accessibleFromType : FileTool.scanFolder(folder, Files::isDirectory)) {
      for (final Path file : this.scanAll(accessibleFromType)) {
        try {
          if (Files.isHidden(file)) {
            continue;
          }
          final PathResource resource = new PathResource(file);
          final DataAccessibilityType accessibleFrom = DataAccessibilityType.valueOf(accessibleFromType.getFileName().toString());
          final String relativeLocation = "/" + accessibleFromType.relativize(file.getParent());
          this.researchDataFileClientService.uploadFile(resource, project.getResId(), accessibleFrom.toString(), relativeLocation);
          log.info("{} Research Data File '{}' import completed", prefix, file.getFileName());
          count++;
        } catch (Exception e) {
          log.error("{} Error in importing Research Data File '{}': {}", prefix, file.getFileName(), e.getMessage());
        }
      }
    }
    log.info("{} Research Data File import end {} : {} processed research data file(s)", prefix, folder, count);
  }

}
