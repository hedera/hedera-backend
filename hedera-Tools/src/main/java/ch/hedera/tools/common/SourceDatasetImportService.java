/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Tools - SourceDatasetImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.tools.common;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.PathResource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StandaloneRestClientTool;
import ch.unige.solidify.util.StringTool;

import ch.hedera.model.ingest.DatasetFileType;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.ProjectClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;

@Service
public class SourceDatasetImportService extends HederaToolService {
  private static final Logger log = LoggerFactory.getLogger(SourceDatasetImportService.class);

  private SourceDatasetClientService sourceDatasetClientService;

  public SourceDatasetImportService(StandaloneRestClientTool restClientTool, ProjectClientService projectClientService,
          SourceDatasetClientService sourceDatasetClientService) {
    super(restClientTool, projectClientService);
    this.sourceDatasetClientService = sourceDatasetClientService;
  }

  public void importFolder(Project project, Path folder) {
    final String prefix = "[Source Dataset]";

    log.info("{} Scanning folder : {}", prefix, this.importFolder);
    int datasetCount = 0;
    int fileTotalCount = 0;
    // Scan folder
    for (final Path dataset : FileTool.scanFolder(folder, Files::isDirectory)) {
      final String datasetName = dataset.getFileName().toString();
      int fileCount = 0;
      log.info("{} Creating dataset '{}'", prefix, datasetName);
      final SourceDataset sourceDataset = this.getOrCreateSourceDataset(project, datasetName);
      for (final Path fileType : FileTool.scanFolder(dataset, Files::isDirectory)) {
        for (final Path file : this.scanAll(fileType)) {
          try {
            if (Files.isHidden(file)) {
              continue;
            }
            final DatasetFileType datasetType = DatasetFileType.valueOf(fileType.getFileName().toString());
            final PathResource resource = new PathResource(file);
            final String version = OffsetDateTime.now().format(DateTimeFormatter.ofPattern(StringTool.DATE_TIME_FORMAT_FOR_FILE));
            this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(), resource, datasetType.toString(), version);
            log.info("{} Source dataset file '{}': upload completed", prefix, file.getFileName());
            fileCount++;
          } catch (Exception e) {
            log.error("{} Error in uploading source dataset file '{}': {}", prefix, file.getFileName(), e.getMessage());
          }
        }
      }
      datasetCount++;
      fileTotalCount += fileCount;
      log.info("{} Source dataset '{}' completed: id={}, {} file(s)", prefix, datasetName, sourceDataset.getResId(), fileCount);
    }
    log.info("{} Source Dataset import end {} : {} processed dataset(s) with {} file(s)", prefix, folder, datasetCount, fileTotalCount);
  }

  private SourceDataset getOrCreateSourceDataset(Project project, String datasetName) {
    SourceDataset dataset = null;
    List<SourceDataset> datasetList = this.sourceDatasetClientService
            .searchByProperties((Map.of("name", datasetName, "projectId", project.getResId())));
    if (datasetList.isEmpty()) {
      dataset = new SourceDataset();
      dataset.setName(datasetName);
      dataset.setProjectId(project.getResId());
      dataset = this.sourceDatasetClientService.create(dataset);
    } else {
      dataset = datasetList.get(0);
    }
    // Check logo
    return dataset;
  }

}
