/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Tools - RmlImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.tools.common;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.PathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StandaloneRestClientTool;
import ch.unige.solidify.util.StringTool;

import ch.hedera.model.humanities.Rml;
import ch.hedera.model.humanities.RmlFormat;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.ProjectClientService;
import ch.hedera.service.admin.RmlClientService;

@Service
public class RmlImportService extends HederaToolService {
  private static final Logger log = LoggerFactory.getLogger(RmlImportService.class);

  private RmlClientService rmlClientService;

  public RmlImportService(StandaloneRestClientTool restClientTool, ProjectClientService projectClientService,
          RmlClientService rmlClientService) {
    super(restClientTool, projectClientService);
    this.rmlClientService = rmlClientService;
  }

  public void importFolder(Project project, Path folder) {
    final String prefix = "[RML]";

    log.info("{} Scanning folder : {}", prefix, this.importFolder);
    int rmlCount = 0;
    // Scan folder
    for (final Path rmlType : FileTool.scanFolder(folder, Files::isDirectory)) {
      rmlType.getFileName().toString();
      for (final Path rmlFile : this.scanAll(rmlType)) {
        try {
          if (Files.isHidden(rmlFile)) {
            continue;
          }
          log.info("{} Creating RML '{}'", prefix, rmlFile.getFileName());
          final RmlFormat rmlFormat = RmlFormat.valueOf(rmlType.getFileName().toString());
          final PathResource resource = new PathResource(rmlFile);
          Rml rml = new Rml();
          rml.setName(this.getRmlName(rmlFile.getFileName().toString()));
          rml.setDescription(rmlFile.getFileName().toString());
          rml.setVersion(this.getRmlVersion(rmlFile.getFileName().toString()));
          rml.setFormat(rmlFormat);
          try {
            rml = this.rmlClientService.createAndUploadRdfFile(rml, resource);
            log.info("{} RML file '{}': upload completed", prefix, rmlFile.getFileName());
            this.projectClientService.addRml(project.getResId(), rml.getResId());
            log.info("{} RML file '{}' associated to projet '{}': upload completed", prefix, rmlFile.getFileName(), project.getResId());
            rmlCount++;
          } catch (HttpClientErrorException.BadRequest e) {
            log.warn("{} RML file '{}': error in uploading", prefix, rmlFile.getFileName());
          }
        } catch (Exception e) {
          log.error("{} Error in uploading RML file '{}': {}", prefix, rmlFile.getFileName(), e.getMessage());
        }
      }
    }
    log.info("{} RML import end {} : {} processed RML file(s)", prefix, folder, rmlCount);
  }

  private String getRmlVersion(String filename) {
    if (filename.contains("-") && filename.contains(".")) {
      return filename.substring(filename.lastIndexOf("-") + 1, filename.lastIndexOf("."));
    }
    return OffsetDateTime.now().format(DateTimeFormatter.ofPattern(StringTool.DATE_TIME_FORMAT_FOR_FILE));
  }

  private String getRmlName(String filename) {
    if (filename.contains("-")) {
      return filename.substring(0, filename.lastIndexOf("-"));
    }
    if (filename.contains(".")) {
      return filename.substring(0, filename.lastIndexOf("."));
    }
    return filename;
  }

}
