/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Tools - HederaTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.tools.common;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;

import ch.unige.solidify.util.FileTool;

public abstract class HederaTool {

  public static final String HEDERA_TOOL_CREATION = "created by hedera Tools";
  public static final String HEDERA_TOOL_DATASET = "dataset";
  public static final String HEDERA_TOOL_RESEARCH_DATA_FILE = "researchDataFile";

  public static final String HEDERA_RML_FOLDER = "rml";
  public static final String HEDERA_RESEARCH_OBJECT_TYPE_FOLDER = "object-types";
  public static final String HEDERA_SOURCE_DATASET_FOLDER = "source-datasets";
  public static final String HEDERA_RESEARCH_DATA_FILE_FOLDER = "research-data-files";

  @Value("${hedera.import}")
  protected String importFolder;

  protected List<Path> scanAll(Path root) {
    final List<Path> list = new ArrayList<>();
    for (final Path d : FileTool.scanFolder(root, Files::isDirectory)) {
      list.addAll(this.scanAll(d));
    }
    list.addAll(FileTool.scanFolder(root, Files::isRegularFile));
    return list;
  }

  protected List<Path> scanAllFolders(Path root) {
    final List<Path> list = new ArrayList<>();
    list.add(root);
    for (final Path d : FileTool.scanFolder(root, Files::isDirectory)) {
      list.addAll(this.scanAllFolders(d));
    }
    return list;
  }

}
