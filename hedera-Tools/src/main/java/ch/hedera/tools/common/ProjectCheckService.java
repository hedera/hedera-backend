/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Tools - ProjectCheckService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.tools.common;

import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StandaloneRestClientTool;

import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.DatasetFileType;
import ch.hedera.service.admin.ProjectClientService;

@Service
public class ProjectCheckService extends HederaToolService {
  private static final Logger log = LoggerFactory.getLogger(ProjectCheckService.class);

  public static final String FOLDER_CONTENT = "{} '{}' folder: {} file(s)";
  public static final String UNKNOWN_FOLDER = "{} Unknown folder '{}'";
  public static final String UNKNOWN_FOLDER2 = "{} Unknown folder '{}/{}'";
  public static final String UNKNOWN_FOLDER3 = "{} Unknown folder '{}/{}/{}'";

  public ProjectCheckService(StandaloneRestClientTool restClientTool, ProjectClientService projectClientService) {
    super(restClientTool, projectClientService);
  }

  public void checkProject(String prefix, Path projectFolder) {
    final String projectShortName = projectFolder.getFileName().toString();
    prefix += " --";
    // Check project
    try {
      this.projectClientService.findByShortName(projectShortName);
      log.info("{} Project '{}' already exists", prefix, projectShortName);
    } catch (HttpClientErrorException.NotFound e) {
      log.warn("{} Project '{}' has to be create", prefix, projectShortName);

    } catch (Exception e) {
      log.error("{} Error when checking project '{}': {}", prefix, projectShortName, e.getMessage());
      return;
    }
    // Check project thumbnail
    if (this.checkIfThumbnailFileIsPresent(projectFolder, projectShortName)) {
      log.warn("{} Project '{}' has a thumbnail", prefix, projectShortName);
    } else {
      log.info("{} Project '{}' has no thumbnail", prefix, projectShortName);
    }
    // Check associated data
    log.info("{} Project '{}' checking associated data", prefix, projectShortName);
    for (final Path folder : FileTool.scanFolder(projectFolder, Files::isDirectory)) {
      this.checkFolder(prefix, folder);
    }
  }

  private void checkFolder(String prefix, Path folder) {
    final String folderName = folder.getFileName().toString();
    prefix += "--";
    try {
      DataFolderType dataFolder = DataFolderType.fromFolderName(folderName);
      switch (dataFolder) {
        case RESEARCH_DATA_FILE -> this.checkResearchDataFileSubFolder(prefix, dataFolder, folder);
        case SOURCE_DATASET -> this.checkSourceDatasetSubFolder(prefix, dataFolder, folder);
        default -> log.error(UNKNOWN_FOLDER, prefix, folderName);
      }
    } catch (Exception e) {
      log.error(UNKNOWN_FOLDER, prefix, folderName);
    }
  }

  private void checkResearchDataFileSubFolder(String prefix, DataFolderType dataFolder, Path folder) {
    log.info("{} Research Data Files", prefix);
    for (final Path subFolder : FileTool.scanFolder(folder, Files::isDirectory)) {
      final String subFolderName = subFolder.getFileName().toString();
      try {
        DataAccessibilityType accessibilityType = DataAccessibilityType.valueOf(subFolderName);
        this.logSubFolderContent(prefix, dataFolder, accessibilityType.toString(), folder);
      } catch (Exception e) {
        log.error(UNKNOWN_FOLDER2, prefix, dataFolder.getName(), subFolderName);
      }
    }
  }

  private void checkSourceDatasetSubFolder(String prefix, DataFolderType dataFolder, Path folder) {
    for (final Path dataset : FileTool.scanFolder(folder, Files::isDirectory)) {
      final String datasetName = dataset.getFileName().toString();
      log.info("{} Dataset '{}'", prefix, datasetName);
      for (final Path datasetFileTypeFolder : FileTool.scanFolder(dataset, Files::isDirectory)) {
        final String datasetFileTypeName = datasetFileTypeFolder.getFileName().toString();
        try {
          DatasetFileType datasetFileType = DatasetFileType.valueOf(datasetFileTypeName);
          this.logSubFolderContent(prefix, dataFolder, datasetFileType.toString(), dataset);
        } catch (Exception e) {
          log.error(UNKNOWN_FOLDER3, prefix, dataFolder.getName(), datasetName, datasetFileTypeName, datasetFileTypeName);
        }
      }
    }
  }

  private void logFolderContent(String prefix, DataFolderType folderName, Path folder) {
    log.info(FOLDER_CONTENT, prefix, folderName.getName(), this.checkSubFolder(folder));
  }

  private void logSubFolderContent(String prefix, DataFolderType folderName, String subFolderName, Path folder) {
    prefix += "--";
    log.info(FOLDER_CONTENT, prefix, subFolderName, this.checkSubFolder(folder.resolve(subFolderName)));
  }

  private int checkSubFolder(Path folder) {
    if (FileTool.checkFile(folder)) {
      return this.scanAll(folder).size();
    }
    return 0;
  }

}
