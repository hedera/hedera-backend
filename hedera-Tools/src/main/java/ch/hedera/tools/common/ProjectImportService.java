/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Tools - ProjectImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.tools.common;

import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.PathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StandaloneRestClientTool;

import ch.hedera.model.StorageType;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.ProjectClientService;

@Service
public class ProjectImportService extends HederaToolService {
  private static final Logger log = LoggerFactory.getLogger(ProjectImportService.class);

  public static final String FOLDER_CONTENT = "{} {} folder: {} file(s)";

  private RmlImportService rmlImportService;

  private ResearchDataFileImportService researchDataFileImportService;

  private SourceDatasetImportService sourceDatasetImportService;

  public ProjectImportService(StandaloneRestClientTool restClientTool,
          ProjectClientService projectClientService,
          RmlImportService rmlImportService,
          ResearchDataFileImportService researchDataFileImportService,
          SourceDatasetImportService sourceDatasetImportService) {
    super(restClientTool, projectClientService);
    this.rmlImportService = rmlImportService;
    this.researchDataFileImportService = researchDataFileImportService;
    this.sourceDatasetImportService = sourceDatasetImportService;
  }

  public void importProject(String prefix, Path projectFolder) {
    final String projectShortName = projectFolder.getFileName().toString();
    prefix += " --";
    // Creating project
    Project project = this.getOrCreateProject(prefix, projectShortName);

    // Adding project thumbnail
    if (this.checkIfThumbnailFileIsPresent(projectFolder, projectShortName)) {
      log.info("{} Adding thumbnail to project '{}'", prefix, projectShortName);
      this.addProjectThumbnail(project, projectFolder);
    }
    // Check associated data
    log.info("{} Project '{}' checking associated data", prefix, projectShortName);
    this.importRml(prefix, project, projectFolder);
    this.importResearchDataFiles(prefix, project, projectFolder);
    this.importSourceDatasets(prefix, project, projectFolder);
  }

  private void importRml(String prefix, Project project, Path projectFolder) {
    final Path rmlFolder = projectFolder.resolve(DataFolderType.RML.getName());
    if (!FileTool.checkFile(rmlFolder)) {
      return;
    }
    log.info("{} Project '{}' import RML files starting", prefix, project.getShortName());
    this.rmlImportService.importFolder(project, rmlFolder);
    log.info("{} Project '{}' import RML files completed", prefix, project.getShortName());
  }

  private void importResearchDataFiles(String prefix, Project project, Path projectFolder) {
    final Path researchDataFileFolder = projectFolder.resolve(DataFolderType.RESEARCH_DATA_FILE.getName());
    if (!FileTool.checkFile(researchDataFileFolder)) {
      return;
    }
    log.info("{} Project '{}' import research data files starting", prefix, project.getShortName());
    this.researchDataFileImportService.importFolder(project, researchDataFileFolder);
    log.info("{} Project '{}' import research data files completed", prefix, project.getShortName());
  }

  private void importSourceDatasets(String prefix, Project project, Path projectFolder) {
    final Path sourceDatasetFolder = projectFolder.resolve(DataFolderType.SOURCE_DATASET.getName());
    if (!FileTool.checkFile(sourceDatasetFolder)) {
      return;
    }
    log.info("{} Project '{}' import source datasets starting", prefix, project.getShortName());
    this.sourceDatasetImportService.importFolder(project, sourceDatasetFolder);
    log.info("{} Project '{}' import source datasets completed", prefix, project.getShortName());
  }

  private Project getOrCreateProject(String prefix, String projectShortName) {
    try {
      return this.projectClientService.findByShortName(projectShortName);
    } catch (HttpClientErrorException.NotFound e) {
      log.info("{} Creating project '{}'", prefix, projectShortName);
      Project project = new Project();
      project.setName(projectShortName);
      project.setShortName(projectShortName);
      project.setStorageType(StorageType.FILE);
      return this.projectClientService.create(project);
    } catch (Exception e) {
      throw new SolidifyProcessingException("Error when checking project: " + e.getMessage());
    }
  }

  private void addProjectThumbnail(Project project, Path projectFolder) {
    PathResource thumbnail = new PathResource(this.getThumbnailFile(projectFolder, project.getShortName()));
    this.projectClientService.uploadLogo(project.getResId(), thumbnail);
  }

}
