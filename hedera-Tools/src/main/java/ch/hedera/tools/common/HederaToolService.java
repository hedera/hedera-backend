/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Tools - HederaToolService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.tools.common;

import java.net.URI;
import java.nio.file.Path;

import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StandaloneRestClientTool;

import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.ProjectClientService;

public abstract class HederaToolService extends HederaTool {

  protected StandaloneRestClientTool restClientTool;
  protected ProjectClientService projectClientService;

  protected HederaToolService(StandaloneRestClientTool restClientTool, ProjectClientService projectClientService) {
    this.restClientTool = restClientTool;
    this.projectClientService = projectClientService;
  }

  public Project getProject(String projectIdentifier) {
    try {
      return this.projectClientService.findByShortName(projectIdentifier);

    } catch (HttpClientErrorException.NotFound e1) {
      try {
        return this.projectClientService.findByName(projectIdentifier);
      } catch (HttpClientErrorException.NotFound e2) {
        return this.projectClientService.findOne(projectIdentifier);
      }
    }
  }

  protected boolean checkIfThumbnailFileIsPresent(Path folder, String thunbnailName) {
    return FileTool.checkFile(folder.resolve(thunbnailName + ".png"))
            || FileTool.checkFile(folder.resolve(thunbnailName + ".jpg"))
            || FileTool.checkFile(folder.resolve(thunbnailName + ".jpeg"))
            || FileTool.checkFile(folder.resolve(thunbnailName + ".svg"));
  }

  protected Path getThumbnailFile(Path folder, String thunbnailName) {
    if (FileTool.checkFile(folder.resolve(thunbnailName + ".png"))) {
      return folder.resolve(thunbnailName + ".png");
    } else if (FileTool.checkFile(folder.resolve(thunbnailName + ".jpg"))) {
      return folder.resolve(thunbnailName + ".jpg");
    } else if (FileTool.checkFile(folder.resolve(thunbnailName + ".jpeg"))) {
      return folder.resolve(thunbnailName + ".jpeg");
    } else if (FileTool.checkFile(folder.resolve(thunbnailName + ".svg"))) {
      return folder.resolve(thunbnailName + ".svg");
    }
    return null;
  }

  protected void downloadContentWithToken(String url, Path path) {
    this.restClientTool.downloadContent(URI.create(url), path, TokenUsage.WITH_TOKEN);
  }
}
