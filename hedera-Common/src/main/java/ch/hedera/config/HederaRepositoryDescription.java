/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Common - HederaRepositoryDescription.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.config;

import javax.xml.namespace.QName;

import jakarta.xml.bind.JAXBElement;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import ch.unige.solidify.model.oai.OAIRepositoryInfo;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.xml.hedera.v1.repository.RepositoryInfo;

@Component
@ConfigurationProperties(prefix = "hedera.repository")
public class HederaRepositoryDescription implements OAIRepositoryInfo {

  private String name = "hedera";
  private String longName = "hedera Solution";
  private String description = "Repository for Active Research Datasets";
  private String institution = "Université de Genève";
  private String location = "Genève, CH";
  private String email = "eresearch-opensource@unige.ch";
  private String productionDate = "2022-08-01T09:00:00Z";
  private String archiveHomePage = "";
  private String prefix = "hedera";
  private String domain = "hedera.ch";

  private final GitInfoProperties gitInfoProperties;

  public HederaRepositoryDescription(GitInfoProperties gitInfoProperties) {
    this.gitInfoProperties = gitInfoProperties;
  }

  @Override
  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getLongName() {
    return this.longName;
  }

  public void setLongName(String longName) {
    this.longName = longName;
  }

  @Override
  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getInstitution() {
    return this.institution;
  }

  public void setInstitution(String institution) {
    this.institution = institution;
  }

  public String getLocation() {
    return this.location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  @Override
  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public String getProductionDate() {
    return this.productionDate;
  }

  public void setProductionDate(String productionDate) {
    this.productionDate = productionDate;
  }

  @Override
  public String getPrefix() {
    return this.prefix;
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  @Override
  public String getDomain() {
    return this.domain;
  }

  public void setDomain(String domain) {
    this.domain = domain;
  }

  @Override
  public String getArchiveHomePage() {
    return StringTool.normalizeBaseUrl(this.archiveHomePage);
  }

  public void setArchiveHomePage(String archiveHomePage) {
    this.archiveHomePage = archiveHomePage;
  }

  public String getVersion() {
    return this.gitInfoProperties.getBuild().getVersion();
  }

  @Override
  public JAXBElement<?> getRepositoryDefinition() {
    final RepositoryInfo repoInfo = new RepositoryInfo();
    repoInfo.setName(this.getName());
    repoInfo.setDescription(this.getDescription());
    repoInfo.setInstitution(this.getInstitution());
    repoInfo.setPoweredBy(HederaConstants.HEDERA_SOLUTION + " v" + this.gitInfoProperties.getBuild().getVersion());
    return new JAXBElement<>(new QName(HederaConstants.HEDERA_NAMESPACE_1, "repositoryInfo"), RepositoryInfo.class, repoInfo);
  }

  @Override
  public String getRepositoryNamespace() {
    return HederaConstants.HEDERA_NAMESPACE_1;
  }

  @Override
  public String getRepositorySchema() {
    return HederaConstants.HEDERA_REPOSITORY_SCHEMA_1;
  }

}
