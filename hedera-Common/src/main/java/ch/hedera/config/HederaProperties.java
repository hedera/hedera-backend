/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Common - HederaProperties.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.config;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.unit.DataSize;

import jakarta.annotation.PostConstruct;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.index.IndexDefinition;
import ch.unige.solidify.model.index.IndexDefinitionList;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.rest.ModuleName;

@Component
@ConfigurationProperties(prefix = "hedera")
public class HederaProperties {

  private static final Logger log = LoggerFactory.getLogger(HederaProperties.class);
  private static final String HEDERA = "hedera";
  private static final String TMP = "tmp";

  private SolidifyProperties appConfig;

  public HederaProperties(SolidifyProperties appConfig) {
    this.appConfig = appConfig;
  }

  // Working folders for the applications
  private enum WorkingDirectory {

    RML("ingest/rml"), SOURCE_DATASET("ingest/sourceDataset"), RESEARCH_DATA_FILE("ingest/researchDataFile"), RDF_DATASET(
            "ingest/rdfDataset"), ACCESS("access"), ONTOLOGY("access/ontology"), LOG("log");

    private final String name;

    WorkingDirectory(String name) {
      this.name = name;
    }

    private String getName() {
      return this.name;
    }
  }

  public SolidifyProperties getAppConfig() {
    return this.appConfig;
  }

  @PostConstruct
  private void checkConfiguration() {
    try {
      // Use a temporary directory if hedera.home is not defined
      if (this.home.isEmpty()) {
        this.home = Files.createTempDirectory(HEDERA).toString();
        log.warn("hedera.home not defined, using the temporary directory {}", this.home);
      }

      // Create all needed directories
      for (WorkingDirectory d : WorkingDirectory.values()) {
        Path p = Paths.get(this.home, d.getName());
        // Create directory
        Files.createDirectories(p);
        // Create temporary directory
        Files.createDirectories(p.resolve(TMP));
      }

      // Check project file storage
      boolean fileStorage = true;
      try {
        this.getStorage().getFileStorage().getUrl();
      } catch (SolidifyRuntimeException e) {
        fileStorage = false;
      }
      // Check project file storage
      boolean objectStorage = true;
      try {
        this.getStorage().getObjectStorage().getUrl();
      } catch (SolidifyRuntimeException e) {
        objectStorage = false;
      }
      if (!fileStorage && !objectStorage) {
        throw new SolidifyRuntimeException("Wrong configuration of project storage");
      }
    } catch (SolidifyRuntimeException | IOException e) {
      throw new SolidifyRuntimeException(e.getMessage());
    }
  }

  // ****************
  // ** Properties **
  // ****************
  private String home = "";
  private Data data = new Data();
  private Wait wait = new Wait();
  private Queue queue = new Queue();
  private Topic topic = new Topic();
  private Http http = new Http();
  private Module module = new Module();
  private Parameters parameters = new Parameters();
  private Storage storage = new Storage();
  private Indexing indexing = new Indexing();
  private Documentation documentation = new Documentation();
  private List<ScheduledTaskConfig> scheduledTaskConfigs = new ArrayList<>();
  private WebBaseUrls webBaseUrls = new WebBaseUrls();
  private Triplestore triplestore = new Triplestore();
  private IIIF iiif = new IIIF();

  // *************************
  // ** Embedded Properties **
  // *************************

  // Global Parameters
  public static class Parameters {
    private String[] forbiddenCharacters = { "~", "!", "@", "#", "$", "%", "^", "&", "*", "`", ";", "?", "\\" };
    private String[] checksumList = { "MD5", "SHA1", "SHA256" };
    private String defaultChecksum = "SHA256";
    private String defaultLicense = "CC-BY-4.0";
    private String defaultCopyrightHolder = "Université de Genève";
    private int authorizedUnitsCacheTime = 60;
    private int archiveUserFingerprintDurationMinutes = 5;

    private DataSize fileSizeLimit = DataSize.parse("4GB");
    private long defaultAsyncExecutionTimeout = 0;

    private String portalHomePage = "http://localhost:4500";

    private String projectNameForEmailTemplates = HEDERA;

    private boolean emailOnNotificationTypesSubscriptionChangeEnabled = true;

    private int maxFileNumber = 50000;

    private List<String> uriListToMigrate = List.of("https://lod.unige.ch/rest/");

    public String[] getForbiddenCharacters() {
      return this.forbiddenCharacters;
    }

    public String[] getChecksumList() {
      return this.checksumList;
    }

    public String getDefaultChecksum() {
      return this.defaultChecksum;
    }

    public String getDefaultLicense() {
      return this.defaultLicense;
    }

    public String getDefaultCopyrightHolder() {
      return this.defaultCopyrightHolder;
    }

    public int getAuthorizedUnitsCacheTime() {
      return this.authorizedUnitsCacheTime;
    }

    public void setForbiddenCharacters(String[] forbiddenCharacters) {
      this.forbiddenCharacters = forbiddenCharacters;
    }

    public void setChecksumList(String[] checksumList) {
      this.checksumList = checksumList;
    }

    public void setDefaultChecksum(String defaultChecksum) {
      this.defaultChecksum = defaultChecksum;
    }

    public void setDefaultLicense(String defaultLicense) {
      this.defaultLicense = defaultLicense;
    }

    public void setDefaultCopyrightHolder(String defaultCopyrightHolder) {
      this.defaultCopyrightHolder = defaultCopyrightHolder;
    }

    public void setAuthorizedUnitsCacheTime(int authorizedUnitsCacheTime) {
      this.authorizedUnitsCacheTime = authorizedUnitsCacheTime;
    }

    public DataSize getFileSizeLimit() {
      return this.fileSizeLimit;
    }

    public void setFileSizeLimit(DataSize fileSizeLimit) {
      this.fileSizeLimit = fileSizeLimit;
    }

    public long getDefaultAsyncExecutionTimeout() {
      return this.defaultAsyncExecutionTimeout;
    }

    public void setDefaultAsyncExecutionTimeout(long defaultAsyncExecutionTimeout) {
      this.defaultAsyncExecutionTimeout = defaultAsyncExecutionTimeout;
    }

    public boolean isEmailOnNotificationTypesSubscriptionChangeEnabled() {
      return this.emailOnNotificationTypesSubscriptionChangeEnabled;
    }

    public void setEmailOnNotificationTypesSubscriptionChangeEnabled(boolean emailOnNotificationTypesSubscriptionChangeEnabled) {
      this.emailOnNotificationTypesSubscriptionChangeEnabled = emailOnNotificationTypesSubscriptionChangeEnabled;
    }

    public String getProjectNameForEmailTemplates() {
      return this.projectNameForEmailTemplates;
    }

    public void setProjectNameForEmailTemplates(String projectNameForEmailTemplates) {
      this.projectNameForEmailTemplates = projectNameForEmailTemplates;
    }

    public int getMaxFileNumber() {
      return this.maxFileNumber;
    }

    public void setMaxFileNumber(int maxFileNumber) {
      this.maxFileNumber = maxFileNumber;
    }

    public int getArchiveUserFingerprintDurationMinutes() {
      return this.archiveUserFingerprintDurationMinutes;
    }

    public void setArchiveUserFingerprintDurationMinutes(int archiveUserFingerprintDurationMinutes) {
      this.archiveUserFingerprintDurationMinutes = archiveUserFingerprintDurationMinutes;
    }

    public List<String> getUriListToMigrate() {
      return this.uriListToMigrate;
    }

    public void setUriListToMigrate(List<String> uriListToMigrate) {
      this.uriListToMigrate = uriListToMigrate;
    }

    public String getPortalHomePage() {
      return this.portalHomePage;
    }

    public void setPortalHomePage(String portalHomePage) {
      this.portalHomePage = portalHomePage;
    }

  }

  public static class FileList {
    private String[] files = {};
    private String[] puids = {};

    public FileList(String[] puids, String[] files) {
      if (puids != null && puids.length > 0) {
        this.puids = puids;
      }
      if (files != null && files.length > 0) {
        this.files = files;
      }
    }

    public String[] getFiles() {
      return this.files;
    }

    public void setFiles(String[] files) {
      this.files = files;
    }

    public String[] getPuids() {
      return this.puids;
    }

    public void setPuids(String[] puids) {
      this.puids = puids;
    }
  }

  public static class Data {
    private boolean init = false;
    private boolean initIndexes = true;

    public boolean isInit() {
      return this.init;
    }

    public boolean isInitIndexes() {
      return this.initIndexes;
    }

    public void setInit(boolean init) {
      this.init = init;
    }

    public void setInitIndexes(boolean init) {
      this.initIndexes = init;
    }

  }

  // Wait Parameter
  public static class Wait {
    private int maxTries = 10;
    private int milliseconds = 500;

    public int getMaxTries() {
      return this.maxTries;
    }

    public int getMilliseconds() {
      return this.milliseconds;
    }

    public void setMaxTries(int maxTries) {
      this.maxTries = maxTries;
    }

    public void setMilliseconds(int milliseconds) {
      this.milliseconds = milliseconds;
    }
  }

  // Queues
  public static class Queue {
    private long sourceDatasetBigFileLimit = 100L * 1024 * 1024; // 100MB
    private int nbParallelThreads = 2;

    private String project = "project";
    private String rmlConversion = "rml-conversion";
    private String researchDataFile = "research-data-file";
    private String rdfStorage = "rdf-storage";

    private String idMapping = "idMapping";

    private String iiif = "iiif";

    public String getIiif() {
      return this.iiif;
    }

    public void setIiif(String iiif) {
      this.iiif = iiif;
    }

    public String getIdMapping() {
      return this.idMapping;
    }

    public void setIdMapping(String idMapping) {
      this.idMapping = idMapping;
    }

    public String getProject() {
      return this.project;
    }

    public void setProject(String project) {
      this.project = project;
    }

    public String getRmlConversion() {
      return this.rmlConversion;
    }

    public void setRmlConversion(String rmlConversion) {
      this.rmlConversion = rmlConversion;
    }

    public String getResearchDataFile() {
      return this.researchDataFile;
    }

    public void setResearchDataFile(String researchDataFile) {
      this.researchDataFile = researchDataFile;
    }

    public String getRdfStorage() {
      return this.rdfStorage;
    }

    public void setRdfStorage(String rdfStorage) {
      this.rdfStorage = rdfStorage;
    }

    public long getSourceDatasetBigFileLimit() {
      return this.sourceDatasetBigFileLimit;
    }

    public void setSourceDatasetBigFileLimit(long sourceDatasetBigFileLimit) {
      this.sourceDatasetBigFileLimit = sourceDatasetBigFileLimit;
    }

    public int getNbParallelThreads() {
      return this.nbParallelThreads;
    }

    public void setNbParallelThreads(int nbParallelThreads) {
      this.nbParallelThreads = nbParallelThreads;
    }
  }

  // Topic queues
  public static class Topic {
    private String cache = "cache";
    private String emails = "emails";

    public String getCache() {
      return this.cache;
    }

    public String getEmails() {
      return this.emails;
    }

    public void setCache(String cache) {
      this.cache = cache;
    }

    public void setEmails(String emails) {
      this.emails = emails;
    }

  }

  // Web URLs
  public static class WebBaseUrls {
    private String orcid = "https://orcid.org/";
    private String ror = "https://ror.org/";
    private String rorApi = "https://api.ror.org/organizations/";
    private String grid = "https://grid.ac/institutes/";
    private String isni = "http://isni.org/isni/";
    private String crossrefFunder = "https://api.crossref.org/funders/";
    private String wikidata = "https://www.wikidata.org/wiki/";

    public String getOrcid() {
      return StringTool.normalizeBaseUrl(this.orcid);
    }

    public void setOrcid(String orcid) {
      this.orcid = orcid;
    }

    public String getRor() {
      return StringTool.normalizeBaseUrl(this.ror);
    }

    public void setRor(String ror) {
      this.ror = ror;
    }

    public String getRorApi() {
      return StringTool.normalizeBaseUrl(this.rorApi);
    }

    public void setRorApi(String rorApi) {
      this.rorApi = rorApi;
    }

    public String getGrid() {
      return StringTool.normalizeBaseUrl(this.grid);
    }

    public void setGrid(String grid) {
      this.grid = grid;
    }

    public String getIsni() {
      return StringTool.normalizeBaseUrl(this.isni);
    }

    public void setIsni(String isni) {
      this.isni = isni;
    }

    public String getCrossrefFunder() {
      return StringTool.normalizeBaseUrl(this.crossrefFunder);
    }

    public void setCrossrefFunder(String crossrefFunder) {
      this.crossrefFunder = crossrefFunder;
    }

    public String getWikidata() {
      return StringTool.normalizeBaseUrl(this.wikidata);
    }

    public void setWikidata(String wikidata) {
      this.wikidata = wikidata;
    }

  }

  // HTTP parameters
  public static class Http {
    private String expectedHeadersCharset = "ISO-8859-1";
    private String shibbolethHeadersCharset = "UTF-8";

    public String getExpectedHeadersCharset() {
      return this.expectedHeadersCharset;
    }

    public String getShibbolethHeadersCharset() {
      return this.shibbolethHeadersCharset;
    }

    public void setExpectedHeadersCharset(String expectedHeadersCharset) {
      this.expectedHeadersCharset = expectedHeadersCharset;
    }

    public void setShibbolethHeadersCharset(String shibbolethHeadersCharset) {
      this.shibbolethHeadersCharset = shibbolethHeadersCharset;
    }
  }

  // Modules
  public static class Module {
    private ModuleDetail admin = new ModuleDetail();
    private ModuleDetail ingest = new ModuleDetail();
    private ModuleDetail access = new ModuleDetail();
    private ModuleDetail iiif = new ModuleDetail();
    private ModuleDetail sparql = new ModuleDetail();
    private ModuleDetail linkedData = new ModuleDetail();
    private ModuleDetail ontologies = new ModuleDetail();

    public ModuleDetail getAdmin() {
      return this.admin;
    }

    public ModuleDetail getIngest() {
      return this.ingest;
    }

    public ModuleDetail getAccess() {
      return this.access;
    }

    public ModuleDetail getIIIF() {
      return this.iiif;
    }

    public ModuleDetail getSparql() {
      return this.sparql;
    }

    public ModuleDetail getLinkedData() {
      return this.linkedData;
    }

    public ModuleDetail getOntologies() {
      return this.ontologies;
    }

    public void setAdmin(ModuleDetail admin) {
      this.admin = admin;
    }

    public void setIngest(ModuleDetail ingest) {
      this.ingest = ingest;
    }

    public void setAccess(ModuleDetail access) {
      this.access = access;
      this.setLinkedModule(this.iiif, ModuleName.IIIF, this.access);
      this.setLinkedModule(this.sparql, ModuleName.SPARQL, this.access);
      this.setLinkedModule(this.linkedData, ModuleName.LINKED_DATA, this.access);
      this.setLinkedModule(this.ontologies, ModuleName.ONTOLOGY, this.access);
    }

    private void setLinkedModule(ModuleDetail module, String moduleContext, ModuleDetail mainModule) {
      module.setEnable(mainModule.isEnable());
      module.setUrl(mainModule.getUrl().substring(0, mainModule.getUrl().lastIndexOf("/") + 1) + moduleContext);
      module.setPublicUrl(mainModule.getPublicUrl().substring(0, mainModule.getPublicUrl().lastIndexOf("/") + 1) + moduleContext);
    }

  }

  public static class ModuleDetail {
    private String url = "";
    private String publicUrl = "";
    private boolean enable = false;

    public String getUrl() {
      return StringTool.normalizeUrl(this.url);
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getPublicUrl() {
      if (this.publicUrl.isEmpty()) {
        return this.getUrl();
      }
      return StringTool.normalizeUrl(this.publicUrl);
    }

    public void setPublicUrl(String publicUrl) {
      this.publicUrl = publicUrl;
    }

    public boolean isEnable() {
      return this.enable;
    }

    public void setEnable(boolean enable) {
      this.enable = enable;
    }
  }

  // Storage service in archival storage
  public static class Storage {
    private FileStorage fileStorage = new FileStorage();
    private ObjectStorage objectStorage = new ObjectStorage();

    public FileStorage getFileStorage() {
      return this.fileStorage;
    }

    public void setFileStorage(FileStorage fileStorage) {
      this.fileStorage = fileStorage;
    }

    public ObjectStorage getObjectStorage() {
      return this.objectStorage;
    }

    public void setObjectStorage(ObjectStorage objectStorage) {
      this.objectStorage = objectStorage;
    }

  }

  public static class FileStorage {
    private String url;

    public void setUrl(String url) {
      this.url = url;
    }

    public String getUrl() {
      return StringTool.normalizeUrl(this.url);
    }
  }

  public static class ObjectStorage {
    private String url;
    private String accessKey = "hedera-key";
    private String secretKey = "hedera-secret-changeit";
    private String s3Signer = "S3SignerType";
    private boolean isPathStyleAccess = false;

    public String getUrl() {
      return StringTool.normalizeUrl(this.url);
    }

    public String getAccessKey() {
      return this.accessKey;
    }

    public String getSecretKey() {
      return this.secretKey;
    }

    public String getS3Signer() {
      return this.s3Signer;
    }

    public boolean getIsPathStyleAccess() {
      return this.isPathStyleAccess;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public void setAccessKey(String accessKey) {
      this.accessKey = accessKey;
    }

    public void setSecretKey(String secretKey) {
      this.secretKey = secretKey;
    }

    public void setS3Signer(String s3Signer) {
      this.s3Signer = s3Signer;
    }

    public void setIsPathStyleAccess(boolean isPathStyleAccess) {
      this.isPathStyleAccess = isPathStyleAccess;
    }
  }

  public static class Triplestore {
    private String url;
    private String login;
    private String password;
    private String datasetPrefix;
    private int maxDetailProperties = 1000;

    private int fusekiQueryTimeout = 10000;

    public String getUrl() {
      return StringTool.normalizeUrl(this.url);
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getLogin() {
      return this.login;
    }

    public void setLogin(String login) {
      this.login = login;
    }

    public String getPassword() {
      return this.password;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    public int getMaxDetailProperties() {
      return this.maxDetailProperties;
    }

    public void setMaxDetailProperties(int maxDetailProperties) {
      this.maxDetailProperties = maxDetailProperties;
    }

    public int getFusekiQueryTimeout() {
      return this.fusekiQueryTimeout;
    }

    public void setFusekiQueryTimeout(int fusekiQueryTimeout) {
      this.fusekiQueryTimeout = fusekiQueryTimeout;
    }

    public String getDatasetPrefix() {
      return this.datasetPrefix;
    }

    public void setDatasetPrefix(String datasetPrefix) {
      this.datasetPrefix = datasetPrefix;
    }
  }

  // Indexing service in data mgmt
  public static class Indexing {
    private static final String INDEX_MAPPING = "index.json";
    private static final String INDEX_SETTINGS = "settings.json";

    private String indexName = HEDERA;
    private String[] exceptions = {};

    public String getIndexName() {
      return this.indexName;
    }

    public void setIndexName(String indexName) {
      this.indexName = indexName;
    }

    public String getPrivateIndexName() {
      return this.getIndexName() + HederaConstants.PRIVATE;
    }

    public IndexDefinitionList getIndexDefinitionList() {
      IndexDefinitionList list = new IndexDefinitionList();
      list.getIndexList().add(new IndexDefinition(this.getIndexName(), INDEX_MAPPING, INDEX_SETTINGS));
      list.getIndexList().add(new IndexDefinition(this.getPrivateIndexName(), INDEX_MAPPING, INDEX_SETTINGS));
      return list;
    }

    public String[] getExceptions() {
      return this.exceptions;
    }

    public void setExceptions(String[] exceptions) {
      this.exceptions = exceptions;
    }

  }

  public static class Documentation {

    public static class User {
      private String name;
      private String role;

      public String getName() {
        return this.name;
      }

      public void setName(final String name) {
        this.name = name;
      }

      public String getRole() {
        return this.role;
      }

      public void setRole(final String role) {
        this.role = role;
      }
    }

    private boolean generateData = false;
    private List<Documentation.User> users = new ArrayList<>();

    public List<Documentation.User> getUsers() {
      return this.users;
    }

    public void setUsers(final List<Documentation.User> users) {
      this.users = users;
    }

    public boolean shouldGenerateData() {
      return this.generateData;
    }

    public void setGenerateData(boolean generateData) {
      this.generateData = generateData;
    }

  }

  public static class IIIF {

    // IIIF specification: https://iiif.io/api/image/3.0/#21-image-request-uri-syntax
    // Template: /{region}/{size}/{rotation}/{quality}.{format}
    // Default v2: /full/full/0/default.jpg
    // Default v3: /full/max/0/default.jpg

    private String defaultVersion = "3";
    private String defaultParameters = "/full/max/0/default.jpg";
    private int maxSize = 65500;
    private String imageUrl = "https://iiif.unige.ch/iiif";

    public String getImageUrl() {
      return StringTool.normalizeUrl(this.imageUrl);
    }

    public void setImageUrl(String imageUrl) {
      this.imageUrl = imageUrl;
    }

    public String getDefaultVersion() {
      return this.defaultVersion;
    }

    public void setDefaultVersion(String defaultVersion) {
      this.defaultVersion = defaultVersion;
    }

    public String getDefaultParameters() {
      return this.defaultParameters;
    }

    public void setDefaultParameters(String defaultParameters) {
      this.defaultParameters = defaultParameters;
    }

    public int getMaxSize() {
      return this.maxSize;
    }

    public void setMaxSize(int maxSize) {
      this.maxSize = maxSize;
    }

  }

  public Data getData() {
    return this.data;
  }

  public Wait getWait() {
    return this.wait;
  }

  public Queue getQueue() {
    return this.queue;
  }

  public Topic getTopic() {
    return this.topic;
  }

  public Http getHttp() {
    return this.http;
  }

  public Module getModule() {
    return this.module;
  }

  public static class ScheduledTaskConfig {
    private String id;
    private String type;
    private String name;
    private String cronExpression;
    private boolean enabled = true;

    public String getId() {
      return this.id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getType() {
      return this.type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public String getName() {
      return this.name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getCronExpression() {
      return this.cronExpression;
    }

    public void setCronExpression(String cronExpression) {
      this.cronExpression = cronExpression;
    }

    public boolean isEnabled() {
      return this.enabled;
    }

    public void setEnabled(boolean enabled) {
      this.enabled = enabled;
    }
  }

  public Parameters getParameters() {
    return this.parameters;
  }

  public WebBaseUrls getWebBaseUrls() {
    return this.webBaseUrls;
  }

  public Storage getStorage() {
    return this.storage;
  }

  public Indexing getIndexing() {
    return this.indexing;
  }

  public Triplestore getTriplestore() {
    return this.triplestore;
  }

  public void setHome(String home) {
    this.home = home;
  }

  public void setData(Data data) {
    this.data = data;
  }

  public void setWait(Wait wait) {
    this.wait = wait;
  }

  public void setQueue(Queue queue) {
    this.queue = queue;
  }

  public void setTopic(Topic topic) {
    this.topic = topic;
  }

  public void setHttp(Http http) {
    this.http = http;
  }

  public void setModule(Module module) {
    this.module = module;
  }

  public void setParameters(Parameters parameters) {
    this.parameters = parameters;
  }

  public void setWebBaseUrls(WebBaseUrls webBaseUrls) {
    this.webBaseUrls = webBaseUrls;
  }

  public void setStorage(Storage storage) {
    this.storage = storage;
  }

  public void setIndexing(Indexing indexing) {
    this.indexing = indexing;
  }

  public void setTriplestore(Triplestore triplestore) {
    this.triplestore = triplestore;
  }

  public void setDocumentation(Documentation documentation) {
    this.documentation = documentation;
  }

  public void setIIIF(IIIF iiif) {
    this.iiif = iiif;
  }

  public String getHome() {
    return this.home;
  }

  public String getSourceDatasetLocation() {
    return this.getHome() + File.separator + WorkingDirectory.SOURCE_DATASET.getName();
  }

  public String getRdfDatasetLocation() {
    return this.getHome() + File.separator + WorkingDirectory.RDF_DATASET.getName();
  }

  public String getResearchDataFileLocation() {
    return this.getHome() + File.separator + WorkingDirectory.RESEARCH_DATA_FILE.getName();
  }

  public String getRmlLocation() {
    return this.getHome() + File.separator + WorkingDirectory.RML.getName();
  }

  public String getOntologyLocation() {
    return this.getHome() + File.separator + WorkingDirectory.ONTOLOGY.getName();
  }

  public String getAccessLocation() {
    return this.getHome() + File.separator + WorkingDirectory.ACCESS.getName();
  }

  public String getTempLocation(String moduleLocation) {
    return moduleLocation + File.separator + TMP;
  }

  public Documentation getDocumentation() {
    return this.documentation;
  }

  public List<ScheduledTaskConfig> getScheduledTasks() {
    return this.scheduledTaskConfigs;
  }

  public IIIF getIIIF() {
    return this.iiif;
  }
}
