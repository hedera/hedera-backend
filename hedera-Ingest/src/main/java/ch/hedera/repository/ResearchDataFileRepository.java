/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - ResearchDataFileRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.hedera.controller.IngestController;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.ResearchDataFile.ResearchDataFileFileStatus;

@Repository
@ConditionalOnBean(IngestController.class)
public interface ResearchDataFileRepository extends SolidifyRepository<ResearchDataFile> {

  List<ResearchDataFile> findByProjectIdAndRelativeLocationAndFileNameIgnoreCase(String projectId, String relativeLocation, String fileName);

  List<ResearchDataFile> findByProjectId(String projectId);

  List<ResearchDataFile> findByProjectIdAndStatus(String projectId, ResearchDataFileFileStatus status);

  @Query("""
          SELECT DISTINCT rdf.relativeLocation FROM ResearchDataFile rdf
          WHERE rdf.projectId = :projectId ORDER BY rdf.relativeLocation
           """)
  List<String> findDistinctRelativeLocationByProject(String projectId);

  @Query("""
          SELECT rdf.resId FROM ResearchDataFile rdf
          WHERE rdf.projectId = :projectId
          AND (rdf.relativeLocation = :relativeLocation OR rdf.relativeLocation LIKE :relativeLocationSubFolder)
          """)
  List<String> findResearchDataFileIdByRelativeLocationFolderAndProject(String projectId, String relativeLocation,
          String relativeLocationSubFolder);

  @Query("""
          SELECT SUM(rdf.fileSize) FROM ResearchDataFile rdf
          WHERE rdf.projectId = :projectId
          """)
  Long getTotalSizeByProject(String projectId);

}
