/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - IngestController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.controller.ModuleController;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.TrustedUserPermissions;

import ch.hedera.message.IdMappingMessage;
import ch.hedera.message.ProjectMessage;
import ch.hedera.message.RdfStorageMessage;
import ch.hedera.model.RdfOperation;
import ch.hedera.model.settings.Project;
import ch.hedera.model.triplestore.TriplestoreDataset;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.rest.abstractservice.IIIFRefreshRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedTriplestoreDatasetRemoteResourceService;

@RestController
@DependsOn("solidifyEventPublisher")
@ConditionalOnProperty(prefix = "hedera.module.ingest", name = "enable")
@RequestMapping(UrlPath.INGEST)
public class IngestController extends ModuleController {

  private final TrustedProjectRemoteResourceService projectRemoteResourceService;

  private final TrustedTriplestoreDatasetRemoteResourceService triplestoreDatasetRemoteResourceService;

  private final IIIFRefreshRemoteResourceService iiifRefreshRemoteResourceService;

  IngestController(
          TrustedProjectRemoteResourceService projectRemoteResourceService,
          TrustedTriplestoreDatasetRemoteResourceService triplestoreDatasetRemoteResourceService,
          IIIFRefreshRemoteResourceService iiifRefreshRemoteResourceService) {
    super(ModuleName.INGEST);
    this.projectRemoteResourceService = projectRemoteResourceService;
    this.triplestoreDatasetRemoteResourceService = triplestoreDatasetRemoteResourceService;
    this.iiifRefreshRemoteResourceService = iiifRefreshRemoteResourceService;
  }

  @TrustedUserPermissions
  @PostMapping("/" + HederaActionName.PURGE_ID_MAPPING + "/{projectShortName}")
  public ResponseEntity<Void> purge(@PathVariable String projectShortName) {
    SolidifyEventPublisher.getPublisher().publishEvent(new IdMappingMessage(projectShortName));
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProject(#projectId, 'UPDATE')")
  @PostMapping("/" + HederaActionName.IIIF_REFRESH + "/{projectId}")
  public ResponseEntity<Void> refreshIIIF(@PathVariable String projectId) {
    final Project project = this.projectRemoteResourceService.findOne(projectId);
    this.iiifRefreshRemoteResourceService.refresh(project.getShortName());
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @RootPermissions
  @PostMapping("/" + HederaActionName.REFRESH)
  public ResponseEntity<Void> refreshAll() {
    for (Project project : this.projectRemoteResourceService.findAll(PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE)).getData()) {
      SolidifyEventPublisher.getPublisher().publishEvent(new ProjectMessage(project.getProjectId()));
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @RootPermissions
  @PostMapping("/" + HederaActionName.REFRESH + "/{projectId}")
  public ResponseEntity<Void> refresh(@PathVariable String projectId) {
    SolidifyEventPublisher.getPublisher().publishEvent(new ProjectMessage(projectId));
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProject(#projectId, 'UPDATE')")
  @PostMapping("/" + HederaActionName.PURGE_RDF + "/{projectId}")
  public ResponseEntity<Void> purgeRDF(@PathVariable String projectId) {
    final Project project = this.projectRemoteResourceService.findOne(projectId);
    for (TriplestoreDataset triplestoreDataset : this.triplestoreDatasetRemoteResourceService.findByProject(project)) {
      SolidifyEventPublisher.getPublisher()
              .publishEvent(new RdfStorageMessage(project.getProjectId(), triplestoreDataset.getResId(), RdfOperation.PURGE, true));
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

}
