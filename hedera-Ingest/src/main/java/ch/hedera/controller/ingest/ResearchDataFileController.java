/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - ResearchDataFileController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.ingest;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyBulkActionException;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyUndeletableException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ZipTool;
import ch.unige.solidify.validation.ValidationError;

import ch.hedera.HederaConstants;
import ch.hedera.business.ResearchDataFileService;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.HederaResourceController;
import ch.hedera.controller.IngestController;
import ch.hedera.controller.UploadZipController;
import ch.hedera.message.ResearchDataFileReloadMessage;
import ch.hedera.model.StatusHistory;
import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.ResearchDataFile.ResearchDataFileFileStatus;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.HistoryService;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;
import ch.hedera.service.storage.ProjectStorage;

@RestController
@ConditionalOnBean(IngestController.class)
@RequestMapping(UrlPath.INGEST_RESEARCH_DATA_FILE)
public class ResearchDataFileController extends HederaResourceController<ResearchDataFile> implements UploadZipController {

  private final TrustedProjectRemoteResourceService projectRemoteResourceService;
  private final HederaProperties hederaProperties;

  public ResearchDataFileController(
          HederaProperties config,
          HistoryService historyService,
          TrustedProjectRemoteResourceService projectRemoteResourceService,
          HederaProperties hederaProperties) {
    super(config, historyService);
    this.projectRemoteResourceService = projectRemoteResourceService;
    this.hederaProperties = hederaProperties;
  }

  @Override
  @PreAuthorize("@researchDataFilePermissionService.isAllowedToCreate(#researchDataFile)")
  public HttpEntity<ResearchDataFile> create(@RequestBody ResearchDataFile researchDataFile) {
    return super.create(researchDataFile);
  }

  @Override
  @PreAuthorize("@researchDataFilePermissionService.isAllowed(#id, 'GET')")
  public HttpEntity<ResearchDataFile> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  @PreAuthorize("@researchDataFilePermissionService.isAllowedToListWithProjectId(#search, 'LIST')")
  public HttpEntity<RestCollection<ResearchDataFile>> list(@ModelAttribute ResearchDataFile search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  @PreAuthorize("@researchDataFilePermissionService.isAllowed(#id, 'UPDATE')")
  public HttpEntity<ResearchDataFile> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  @PreAuthorize("@researchDataFilePermissionService.isAllowed(#id, 'DELETE')")
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  @PreAuthorize("@researchDataFilePermissionService.isAllowedToDeleteList(#ids, 'DELETE')")
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @GetMapping("/" + HederaActionName.LIST_FOLDERS)
  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProject(#projectId, 'LIST_FILES')")
  public HttpEntity<List<String>> listFolders(@RequestParam(HederaConstants.PROJECT_ID_FIELD) String projectId) {
    final List<String> relativeLocations = ((ResearchDataFileService) this.itemService).getRelativeLocationList(projectId);
    return new ResponseEntity<>(relativeLocations, HttpStatus.OK);
  }

  @PreAuthorize("@researchDataFilePermissionService.isAllowed(#Id, 'HISTORY')")
  @Override
  public HttpEntity<RestCollection<StatusHistory>> history(@PathVariable String id, Pageable pageable) {
    return super.history(id, pageable);
  }

  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProject(#projectId, 'UPLOAD_RESEARCH_DATA_FILE')")
  @PostMapping("/" + ActionName.UPLOAD)
  public HttpEntity<ResearchDataFile> uploadFile(@RequestParam(HederaConstants.PROJECT_ID_FIELD) String projectId,
          @RequestParam(HederaConstants.FILE) MultipartFile file,
          @RequestParam(HederaConstants.ACCESSIBLE_FROM_PARAM) String accessibleFrom,
          @RequestParam(value = HederaConstants.RELATIVE_LOCATION_PARAM, required = false) String relLocation,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) throws IOException {
    Project item = this.getProject(projectId);

    // Check first if the property researchDataFileResearchObjectType in project has been defined
    if (item.getResearchDataFileResearchObjectType() == null) {
      final BindingResult errors = new BeanPropertyBindingResult(item, item.getClass().getName());
      errors.addError(new FieldError(projectId.getClass().getSimpleName(), "researchDataFileResearchObjectType",
              this.messageService.get("validation.researchDataFile.upload.noResearchDataFileResearchObjectType")));
      throw new SolidifyValidationException(new ValidationError(errors));
    }
    // Check temporary folder
    String finalDir = this.hederaProperties.getResearchDataFileLocation() + "/" + projectId;
    if (!FileTool.ensureFolderExists(Paths.get(finalDir))) {
      throw new IOException(this.messageService.get("message.upload.folder.notcreated", new Object[] { finalDir }));
    }

    // Check relative location
    String relativeLocation = relLocation;

    if (!StringTool.isNullOrEmpty(relativeLocation)) {
      // Check format
      relativeLocation = ResearchDataFile.checkRelativeLocation(relativeLocation);
      // Check temporary folder
      finalDir += relativeLocation;
      if (!FileTool.ensureFolderExists(Paths.get(finalDir))) {
        throw new IOException(this.messageService.get("message.upload.folder.notcreated", new Object[] { finalDir }));
      }
    } else {
      relativeLocation = "/";
    }

    // Check if duplicates data files
    if (((ResearchDataFileService) this.itemService).findByProjectIdAndRelativeLocationAndFileName(item.getResId(), relativeLocation,
            file.getOriginalFilename()).isPresent()) {
      throw new IOException(this.messageService.get("message.upload.file.duplicate", new Object[] { file.getOriginalFilename() }));
    }

    final String originalFilename = file.getOriginalFilename();
    final Path destination = Paths.get(finalDir + "/" + originalFilename).normalize();
    if (originalFilename != null) {
      Files.copy(file.getInputStream(), destination);
    } else {
      throw new SolidifyValidationException(new ValidationError("Missing original file name"));
    }

    ResearchDataFile researchDataFile = this.getResearchDataFile(item, destination, accessibleFrom, relativeLocation, mimeType);
    try {
      this.itemService.save(researchDataFile);
    } catch (Exception exception) {
      Files.deleteIfExists(destination);
      throw exception;
    }
    ((ResearchDataFileService) this.itemService).putInProcessingQueue(researchDataFile);
    return new ResponseEntity<>(researchDataFile, HttpStatus.OK);
  }

  private Project getProject(String projectId) {
    Project item = this.projectRemoteResourceService.findOne(projectId);
    if (!item.isOpen()) {
      throw new IllegalStateException(this.messageService.get("validation.resource.unmodifiable",
              new Object[] { item.getClass().getSimpleName() + ": " + item.getResId() }));
    }
    return item;
  }

  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProject(#projectId, 'UPLOAD_RESEARCH_DATA_FILE')")
  @PostMapping("/" + HederaActionName.UPLOAD_ZIP)
  public HttpEntity<List<ResearchDataFile>> uploadArchive(@RequestParam(HederaConstants.PROJECT_ID_FIELD) String projectId,
          @RequestParam(HederaConstants.FILE) MultipartFile archiveFile,
          @RequestParam(HederaConstants.ACCESSIBLE_FROM_PARAM) String accessibleFrom,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) throws IOException {
    Project project = this.projectRemoteResourceService.findOne(projectId);

    this.checkIfResearchDataFileResearchObjectTypeExist(project);

    final String tmpFolder = this.hederaProperties.getTempLocation(this.hederaProperties.getResearchDataFileLocation()) + File.separator
            + projectId;
    final String extractFolder = this.hederaProperties.getResearchDataFileLocation() + File.separator + projectId;
    final List<Path> extractedFilesPaths = this.uploadZip(tmpFolder, archiveFile, extractFolder, this.messageService);

    // Save research data files
    final List<ResearchDataFile> filesList = this.getResearchDataFileList(project, extractFolder, extractedFilesPaths, accessibleFrom, mimeType);
    filesList.forEach(rdf -> {
      this.itemService.save(rdf);
      ((ResearchDataFileService) this.itemService).putInProcessingQueue(rdf);
    });
    return new ResponseEntity<>(filesList, HttpStatus.OK);
  }

  @PreAuthorize("@researchDataFilePermissionService.isAllowed(#id, 'DOWNLOAD_FILE')")
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD)
  public ResponseEntity<StreamingResponseBody> download(@PathVariable String id) throws IOException {
    final ResearchDataFile item = this.itemService.findOne(id);
    if (item == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    ProjectStorage projectStorage = ProjectStorage.getProjectStorage(this.config.getStorage(), item.getProject());
    InputStream is = projectStorage.getResearchDataFile(item);
    return this.buildDownloadResponseEntity(is, item.getFileName(), item.getMimeType(), item.getFileSize());
  }

  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProject(#projectId, 'DOWNLOAD')")
  @GetMapping("/" + ActionName.DOWNLOAD)
  public HttpEntity<FileSystemResource> downloadFolder(@RequestParam(HederaConstants.PROJECT_ID_FIELD) String projectId,
          @RequestParam(value = HederaConstants.RELATIVE_LOCATION_PARAM) String relativeLocation) {
    // Check if project exist
    Project project = this.projectRemoteResourceService.findOne(projectId);

    this.checkIfResearchDataFileResearchObjectTypeExist(project);

    String folderMatch = relativeLocation.equals("/") ? "(\\/.*)*" : relativeLocation + "(\\/.+)*";
    final List<ResearchDataFile> researchDataFilesByProject = ((ResearchDataFileService) this.itemService).findByProjectId(projectId);

    final List<Path> filesToZip = researchDataFilesByProject.stream().filter(rdf -> rdf.getRelativeLocation().matches(folderMatch))
            .map(ResearchDataFile::getSourcePath).map(Paths::get).toList();
    if (filesToZip.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    final Path source = Paths.get(this.hederaProperties.getResearchDataFileLocation() + "/" + projectId);
    final ZipTool zipPackage = new ZipTool(this.hederaProperties.getTempLocation(this.hederaProperties.getResearchDataFileLocation())
            + File.separatorChar + projectId + SolidifyConstants.ZIP_EXT);

    final List<Path> destinationPaths = researchDataFilesByProject.stream().filter(rdf -> rdf.getRelativeLocation().matches(folderMatch))
            .map(rdf -> {
              if (rdf.isInProgress()) {
                return source.relativize(Paths.get(rdf.getSourcePath()));
              } else {
                return Paths.get(rdf.getRelativeLocation(), rdf.getFileName());
              }
            }).toList();
    zipPackage.addFilesToZip(source, filesToZip, destinationPaths, false);

    final String archivePath = zipPackage.getArchive();
    return this.buildDownloadResponseEntity(archivePath, projectId + SolidifyConstants.ZIP_EXT, SolidifyConstants.ZIP_MIME_TYPE,
            FileTool.getSize(Paths.get(archivePath)));
  }

  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProject(#projectId, 'DELETE_FOLDER')")
  @PostMapping("/" + HederaActionName.DELETE_FOLDER)
  public ResponseEntity<Void> deleteFolder(@RequestParam(HederaConstants.PROJECT_ID_FIELD) String projectId,
          @RequestParam(value = HederaConstants.RELATIVE_LOCATION_PARAM) String relativeLocation) {
    if (relativeLocation == null || relativeLocation.isEmpty() || !relativeLocation.startsWith("/")) {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST, "Wrong relative location");
    }
    List<String> listResearchDataFileIds = ((ResearchDataFileService) this.itemService).findByProjectIdAndRelativeLocation(projectId,
            relativeLocation);
    if (listResearchDataFileIds.isEmpty()) {
      throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND, "No data files found");
    }
    super.deleteList(listResearchDataFileIds.toArray(new String[0]));
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PreAuthorize("@researchDataFilePermissionService.isAllowedByProject(#projectId, 'RELOAD')")
  @PostMapping("/" + HederaActionName.RELOAD)
  public ResponseEntity<Result> reload(@RequestParam(HederaConstants.PROJECT_ID_FIELD) String projectId) {
    // Check if project exist
    final Result result = new Result(projectId);
    try {
      final Project project = this.projectRemoteResourceService.findOne(projectId);
      if (!project.isOpen()) {
        result.setMesssage(
                this.messageService.get("message.project.closed", new Object[] { project.getName() }));
      } else {
        result.setStatus(ActionStatus.NOT_EXECUTED);
        SolidifyEventPublisher.getPublisher().publishEvent(new ResearchDataFileReloadMessage(projectId));
        result.setStatus(ActionStatus.EXECUTED);
        result.setMesssage(this.messageService.get("message.researchDataFile.reload.started", new Object[] { projectId }));
      }
    } catch (SolidifyUndeletableException e) {
      result.setMesssage(e.getMessage());
    }
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @AdminPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.PUT_IN_ERROR)
  public HttpEntity<Result> putInError(@PathVariable("id") String researchDataFileId) {
    final ResearchDataFile researchDataFile = this.itemService.findOne(researchDataFileId);
    final Result result = this.putInError(researchDataFile);
    result.add(linkTo(this.getClass()).slash(researchDataFileId).slash(HederaActionName.PUT_IN_ERROR).withSelfRel());
    result.add(linkTo(this.getClass()).slash(researchDataFileId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @AdminPermissions
  @PostMapping({ "/" + HederaActionName.PUT_IN_ERROR })
  public HttpEntity<Void> putInErrorList(@RequestBody String[] ids) {
    Map<String, String> errorList = new HashMap<>();
    for (final String id : ids) {
      final ResearchDataFile researchDataFile = this.itemService.findOne(id);
      final Result result = this.putInError(researchDataFile);
      if (result.getStatus() != ActionStatus.EXECUTED) {
        errorList.put(id, result.getMessage());
      }
    }
    // Check if there is error
    if (!errorList.isEmpty()) {
      throw new SolidifyBulkActionException(this.messageService.get("message.researchDataFile.bulkPutInError"), errorList);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  private Result putInError(ResearchDataFile researchDataFile) {
    final Result result = new Result(researchDataFile.getResId());

    if (ResearchDataFile.ResearchDataFileFileStatus.COMPLETED.equals(researchDataFile.getStatus())) {
      result.setMesssage(this.messageService.get("message.researchDataFile.alreadyCompleted", new Object[] { researchDataFile.getResId() }));
    } else {
      researchDataFile.setStatus(ResearchDataFile.ResearchDataFileFileStatus.IN_ERROR);
      this.itemService.save(researchDataFile);
      result.setStatus(ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("message.researchDataFile.hasBeenPutInError", new Object[] { researchDataFile.getResId() }));
    }
    return result;
  }

  private ResearchDataFile getResearchDataFile(Project project, Path path, String accessibleFrom, String relativeLocation, String mimeType) {
    ResearchDataFile researchDataFile = new ResearchDataFile();
    researchDataFile.setProjectId(project.getResId());
    researchDataFile.setFileName(path.getFileName().toString());
    researchDataFile.setStatus(ResearchDataFileFileStatus.UPLOADED);
    researchDataFile.setAccessibleFrom(DataAccessibilityType.valueOf(accessibleFrom));
    researchDataFile.setResearchObjectTypeId(project.getResearchDataFileResearchObjectType().getResId());
    researchDataFile.setFileSize(FileTool.getSize(path));
    researchDataFile.setSourcePath(path.toUri());
    researchDataFile.setMimeType(mimeType);
    researchDataFile.setRelativeLocation(relativeLocation);
    return researchDataFile;
  }

  private List<ResearchDataFile> getResearchDataFileList(Project project, String extractFolder, List<Path> extractedFiles, String accessibleFrom,
          String mimeType) {
    final List<ResearchDataFile> fileList = new ArrayList<>();
    for (final Path extractedFilePath : extractedFiles) {
      // Skip Folder
      if (FileTool.isFolder(extractedFilePath)) {
        continue;
      }
      // Get relative location
      String relativeLocation = null;
      final Path relativePath = Paths.get(extractFolder).toAbsolutePath().relativize(extractedFilePath).getParent();
      if (relativePath != null && !StringTool.isNullOrEmpty(relativePath.toString())) {
        relativeLocation = this.normalizePath(relativePath.toString());
      } else {
        relativeLocation = "/";
      }
      // Create research data file
      final ResearchDataFile researchDataFile = this.getResearchDataFile(project, extractedFilePath, accessibleFrom, relativeLocation, mimeType);
      fileList.add(researchDataFile);
    }
    return fileList;
  }

  private String normalizePath(String path) {
    return path.replace("\\", "/");
  }

  public void checkIfResearchDataFileResearchObjectTypeExist(Project project) {

    // Check first if the property researchDataFileResearchObjectType in project has been defined
    if (project.getResearchDataFileResearchObjectType() == null) {
      final BindingResult errors = new BeanPropertyBindingResult(project, project.getClass().getName());
      errors.addError(new FieldError(project.getClass().getSimpleName(), "researchDataFileResearchObjectType",
              this.messageService.get("validation.researchDataFile.upload.noResearchDataFileResearchObjectType")));
      throw new SolidifyValidationException(new ValidationError(errors));
    }
  }
}
