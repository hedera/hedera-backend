/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - SourceDatasetFileController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.ingest;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ResourceReadOnlyController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.NoOnePermissions;

import ch.hedera.HederaConstants;
import ch.hedera.controller.IngestController;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.rest.UrlPath;

@RestController
@ConditionalOnBean(IngestController.class)
@RequestMapping(UrlPath.INGEST_SOURCE_DATASET_FILE)
public class SourceDatasetFileController extends ResourceReadOnlyController<SourceDatasetFile> {

  @Override
  @PreAuthorize("@sourceDatasetFilePermissionService.isAllowed(#id, 'GET')")
  public HttpEntity<SourceDatasetFile> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  @AdminPermissions
  public HttpEntity<RestCollection<SourceDatasetFile>> list(@ModelAttribute SourceDatasetFile search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @GetMapping(params = "projectId")
  @PreAuthorize("@sourceDatasetFilePermissionService.isAllowedToListWithProjectId(#search, 'LIST', #projectId)")
  public HttpEntity<RestCollection<SourceDatasetFile>> list(@ModelAttribute SourceDatasetFile search, Pageable pageable,
          @RequestParam(HederaConstants.PROJECT_ID_FIELD) String projectId) {
    // projectId is already present in sourceDataset.projectId
    return super.list(search, pageable);
  }

  @Override
  @NoOnePermissions
  public HttpEntity<RestCollection<SourceDatasetFile>> advancedSearch(@ModelAttribute SourceDatasetFile resource,
          @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(resource, search, matchtype, pageable);
  }

  @Override
  @NoOnePermissions
  public HttpEntity<RestCollection<SourceDatasetFile>> advancedSearch(@RequestBody SourceDatasetFile search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(search, matchtype, pageable);
  }
}
