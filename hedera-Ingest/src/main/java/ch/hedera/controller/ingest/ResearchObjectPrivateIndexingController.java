/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - ResearchObjectPrivateIndexingController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.ingest;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.service.IndexFieldAliasInfoProvider;

import ch.hedera.config.HederaProperties;
import ch.hedera.controller.HederaIndexDataWriteController;
import ch.hedera.controller.IngestController;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.rest.UrlPath;

@RestController
@ConditionalOnBean(IngestController.class)
@RequestMapping(UrlPath.INGEST_PRIVATE_INDEX)
public class ResearchObjectPrivateIndexingController extends HederaIndexDataWriteController<ResearchObjectMetadata> {

  public ResearchObjectPrivateIndexingController(
          HederaProperties config,
          IndexingService<ResearchObjectMetadata> indexingService,
          IndexFieldAliasInfoProvider indexFieldAliasInfoProvider) {
    super(indexingService, indexFieldAliasInfoProvider, config.getIndexing().getPrivateIndexName());
  }

  @Override
  @TrustedUserPermissions
  public HttpEntity<ResearchObjectMetadata> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  @TrustedUserPermissions
  public HttpEntity<RestCollection<ResearchObjectMetadata>> list(@ModelAttribute ResearchObjectMetadata search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  @TrustedUserPermissions
  public HttpEntity<ResearchObjectMetadata> create(@RequestBody ResearchObjectMetadata t) {
    return super.create(t);
  }

  @Override
  @TrustedUserPermissions
  public HttpEntity<ResearchObjectMetadata> update(@PathVariable String id, @RequestBody ResearchObjectMetadata t2) {
    return super.update(id, t2);
  }

  @Override
  @TrustedUserPermissions
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  @TrustedUserPermissions
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @Override
  @TrustedUserPermissions
  public HttpEntity<Long> deleteByQuery(@RequestBody(required = true) List<SearchCondition> searchConditions) {
    return super.deleteByQuery(searchConditions);
  }

  @Override
  @TrustedUserPermissions
  public HttpEntity<RestCollection<ResearchObjectMetadata>> search(
          @RequestParam(required = false) String query,
          @RequestParam(required = false) String conditions,
          Pageable pageable) {
    return super.search(query, conditions, pageable);
  }

  @Override
  @TrustedUserPermissions
  public HttpEntity<RestCollection<ResearchObjectMetadata>> searchPost(@RequestBody MultiValueMap<String, String> params) {
    return super.searchPost(params);
  }

  @Override
  @TrustedUserPermissions
  public HttpEntity<RestCollection<ResearchObjectMetadata>> searchPost(
          @RequestParam(required = false) String query,
          @RequestParam(required = false) String conditions,
          @RequestBody(required = false) List<SearchCondition> searchConditions,
          Pageable pageable) {
    return super.searchPost(query, conditions, searchConditions, pageable);
  }
}
