/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - RdfDatasetFileController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.ingest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyBulkActionException;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyUndeletableException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.NoOnePermissions;
import ch.unige.solidify.util.FileTool;

import ch.hedera.HederaConstants;
import ch.hedera.HederaRestFields;
import ch.hedera.business.RdfDatasetFileService;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.HederaResourceController;
import ch.hedera.controller.IngestController;
import ch.hedera.model.RdfOperation;
import ch.hedera.model.StatusHistory;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.RdfDatasetFile.RdfDatasetFileStatus;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.HistoryService;
import ch.hedera.specification.RdfDatasetFileSpecification;

@RestController
@ConditionalOnBean(IngestController.class)
@RequestMapping(UrlPath.INGEST_RDF_DATASET_FILE)
public class RdfDatasetFileController extends HederaResourceController<RdfDatasetFile> {

  public RdfDatasetFileController(HederaProperties config, HistoryService historyService) {
    super(config, historyService);
  }

  @Override
  @NoOnePermissions
  public HttpEntity<RdfDatasetFile> create(@RequestBody RdfDatasetFile rdfDatasetFile) {
    throw new UnsupportedOperationException();
  }

  @Override
  @PreAuthorize("@rdfDatasetFilePermissionService.isAllowed(#rdfDatasetFileId, 'GET_FILE')")
  public HttpEntity<RdfDatasetFile> get(@PathVariable("id") String rdfDatasetFileId) {
    return super.get(rdfDatasetFileId);
  }

  @Override
  @AdminPermissions
  public HttpEntity<RestCollection<RdfDatasetFile>> list(@ModelAttribute RdfDatasetFile search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @PreAuthorize("@rdfDatasetFilePermissionService.isAllowed(#rdfDatasetFileId, 'HISTORY')")
  @Override
  public HttpEntity<RestCollection<StatusHistory>> history(@PathVariable("id") String rdfDatasetFileId, Pageable pageable) {
    return super.history(rdfDatasetFileId, pageable);
  }

  @GetMapping(params = HederaRestFields.PROJECT_ID_FIELD)
  @SuppressWarnings("squid:S4684")
  @PreAuthorize("@rdfDatasetFilePermissionService.isAllowedToListFiles(#projectId, 'LIST_FILES')")
  public HttpEntity<RestCollection<RdfDatasetFile>> list(@ModelAttribute RdfDatasetFile search,
          @RequestParam(value = HederaRestFields.PROJECT_ID_FIELD) String projectId,
          @RequestParam(value = "datasetSourceId", required = false) String datasetSourceId,
          @RequestParam(value = "datasetSourceFileId", required = false) String datasetSourceFileId,
          Pageable pageable) {
    final RdfDatasetFileSpecification spec = (RdfDatasetFileSpecification) this.itemService.getSpecification(search);
    spec.setProjectId(projectId);
    spec.setSourceDatasetId(datasetSourceId);
    spec.setSourceDatasetFileId(datasetSourceFileId);
    final Page<RdfDatasetFile> listItem = this.itemService.findAll(spec, pageable);
    this.setResourceLinks(listItem);
    final RestCollection<RdfDatasetFile> collection = this.setCollectionLinks(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @Override
  @PreAuthorize("@rdfDatasetFilePermissionService.isAllowed(#rdfDatasetFileId, 'UPDATE_FILE')")
  public HttpEntity<RdfDatasetFile> update(@PathVariable("id") String rdfDatasetFileId, @RequestBody Map<String, Object> updateMap) {
    return super.update(rdfDatasetFileId, updateMap);
  }

  @Override
  @PreAuthorize("@rdfDatasetFilePermissionService.isAllowed(#rdfDatasetFileId, 'DELETE_FILE')")
  public ResponseEntity<Void> delete(@PathVariable("id") String rdfDatasetFileId) {
    return super.delete(rdfDatasetFileId);
  }

  @Override
  @PreAuthorize("@rdfDatasetFilePermissionService.isAllowedToDeleteList(#ids, 'DELETE_FILE')")
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD)
  @PreAuthorize("@rdfDatasetFilePermissionService.isAllowed(#rdfDatasetFileId, 'DOWNLOAD_FILE')")
  public HttpEntity<FileSystemResource> download(@PathVariable("id") String rdfDatasetFileId) {
    final RdfDatasetFile rdfDatasetFile = this.itemService.findOne(rdfDatasetFileId);
    final URI fileUri = rdfDatasetFile.getDownloadUri();
    final String contentTypeFile = (rdfDatasetFile.getContentType() == null) ? FileTool.getContentType(fileUri).toString()
            : rdfDatasetFile.getContentType();
    return this.buildDownloadResponseEntity(fileUri.getPath(), fileUri.getPath().substring(fileUri.getPath().lastIndexOf('/') + 1),
            contentTypeFile, FileTool.getSize(fileUri));
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.ADD_DATASET + HederaConstants.URL_TRIPLESTORE_DATASET_ID)
  @PreAuthorize("@rdfDatasetFilePermissionService.isAllowed(#rdfDatasetFileID, 'ADD_DATASET')")
  public ResponseEntity<Result> addDataset(@PathVariable("id") String rdfDatasetFileId, @PathVariable String triplestoreDatasetId) {
    final Result result = this.launchImport(rdfDatasetFileId, triplestoreDatasetId, RdfOperation.ADD);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping("/" + HederaActionName.ADD_DATASET + HederaConstants.URL_TRIPLESTORE_DATASET_ID)
  @PreAuthorize("@rdfDatasetFilePermissionService.isAllowed(#rdfDatasetFileID, 'ADD_DATASET')")
  public ResponseEntity<Void> addDatasetList(@PathVariable String triplestoreDatasetId, @RequestBody String[] ids) {
    return this.bulkImport(triplestoreDatasetId, ids, RdfOperation.ADD);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.REPLACE_DATASET + HederaConstants.URL_TRIPLESTORE_DATASET_ID)
  @PreAuthorize("@rdfDatasetFilePermissionService.isAllowed(#rdfDatasetFileId, 'REPLACE_DATASET')")
  public ResponseEntity<Result> replaceDataset(@PathVariable("id") String rdfDatasetFileId, @PathVariable String triplestoreDatasetId) {
    final Result result = this.launchImport(rdfDatasetFileId, triplestoreDatasetId, RdfOperation.REPLACE);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping("/" + HederaActionName.REPLACE_DATASET + HederaConstants.URL_TRIPLESTORE_DATASET_ID)
  @PreAuthorize("@rdfDatasetFilePermissionService.isAllowed(#rdfDatasetFileID, 'REPLACE_DATASET')")
  public ResponseEntity<Void> replaceDatasetList(@PathVariable String triplestoreDatasetId, @RequestBody String[] ids) {
    return this.bulkImport(triplestoreDatasetId, ids, RdfOperation.REPLACE);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.DELETE_DATASET + HederaConstants.URL_TRIPLESTORE_DATASET_ID)
  @PreAuthorize("@rdfDatasetFilePermissionService.isAllowed(#rdfDatasetFileId, 'DELETE_DATASET')")
  public ResponseEntity<Result> deleteDataset(@PathVariable("id") String rdfDatasetFileId, @PathVariable String triplestoreDatasetId) {
    final Result result = this.launchImport(rdfDatasetFileId, triplestoreDatasetId, RdfOperation.REMOVE);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping("/" + HederaActionName.DELETE_DATASET + HederaConstants.URL_TRIPLESTORE_DATASET_ID)
  @PreAuthorize("@rdfDatasetFilePermissionService.isAllowed(#rdfDatasetFileID, 'DELETE_DATASET')")
  public ResponseEntity<Void> deleteDatasetList(@PathVariable String triplestoreDatasetId, @RequestBody String[] ids) {
    return this.bulkImport(triplestoreDatasetId, ids, RdfOperation.REMOVE);
  }

  @AdminPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.PUT_IN_ERROR)
  public HttpEntity<Result> putInError(@PathVariable("id") String rdfDatasetFileId) {
    final RdfDatasetFile rdfDatasetFile = this.itemService.findOne(rdfDatasetFileId);
    final Result result = this.putInError(rdfDatasetFile);
    result.add(linkTo(this.getClass()).slash(rdfDatasetFileId).slash(HederaActionName.PUT_IN_ERROR).withSelfRel());
    result.add(linkTo(this.getClass()).slash(rdfDatasetFileId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @AdminPermissions
  @PostMapping({ "/" + HederaActionName.PUT_IN_ERROR })
  public HttpEntity<Void> putInErrorList(@RequestBody String[] ids) {
    Map<String, String> errorList = new HashMap<>();
    for (final String id : ids) {
      final RdfDatasetFile rdfDatasetFile = this.itemService.findOne(id);
      final Result result = this.putInError(rdfDatasetFile);
      if (result.getStatus() != ActionStatus.EXECUTED) {
        errorList.put(id, result.getMessage());
      }
    }
    // Check if there is error
    if (!errorList.isEmpty()) {
      throw new SolidifyBulkActionException(this.messageService.get("message.rdfDatasetFile.bulkPutInError"), errorList);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  private Result putInError(RdfDatasetFile rdfDatasetFile) {
    final Result result = new Result(rdfDatasetFile.getResId());

    if (RdfDatasetFile.RdfDatasetFileStatus.IMPORTED.equals(rdfDatasetFile.getStatusImport())) {
      result.setMesssage(this.messageService.get("message.rdfDatasetFile.alreadyImported", new Object[] { rdfDatasetFile.getResId() }));
    } else {
      rdfDatasetFile.setStatusImport(RdfDatasetFile.RdfDatasetFileStatus.IN_ERROR);
      this.itemService.save(rdfDatasetFile);
      result.setStatus(ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("message.rdfDatasetFile.hasBeenPutInError", new Object[] { rdfDatasetFile.getResId() }));
    }
    return result;
  }

  private Result launchImport(String rdfDatasetFileId, String triplestoreDatasetId, RdfOperation rdfOperation) {
    final RdfDatasetFile rdfDatasetFile = this.itemService.findOne(rdfDatasetFileId);
    final Result result = new Result(rdfDatasetFileId);
    if (!rdfDatasetFile.getSourceDatasetFile().getDataset().getProject().isOpen()) {
      result.setMesssage(
              this.messageService.get("message.project.closed",
                      new Object[] { rdfDatasetFile.getSourceDatasetFile().getDataset().getProject().getName() }));
    } else if (rdfDatasetFile.isInProgress()) {
      result.setMesssage(this.messageService.get("message.rdfDatasetFile.import.inprogress", new Object[] { rdfDatasetFileId }));
    } else if (rdfOperation.equals(RdfOperation.ADD) && rdfDatasetFile.getStatusImport().equals(RdfDatasetFileStatus.IMPORTED)) {
      result.setMesssage(this.messageService.get("message.rdfDatasetFile.import.alreadydone", new Object[] { rdfDatasetFileId }));
    } else if ((rdfOperation.equals(RdfOperation.REPLACE) || rdfOperation.equals(RdfOperation.REMOVE)) && rdfDatasetFile.getStatusImport()
            .equals(RdfDatasetFileStatus.NOT_IMPORTED)) {
      result.setMesssage(this.messageService.get("message.rdfDatasetFile.import.notimported", new Object[] { rdfDatasetFileId }));
    } else {
      result.setStatus(ActionStatus.NOT_EXECUTED);
      try {
        switch (rdfOperation) {
          case ADD -> rdfDatasetFile.setStatusImport(RdfDatasetFileStatus.TO_IMPORT);
          case REPLACE -> rdfDatasetFile.setStatusImport(RdfDatasetFileStatus.TO_REPLACE);
          case REMOVE -> rdfDatasetFile.setStatusImport(RdfDatasetFileStatus.TO_REMOVE);
          default -> throw new SolidifyCheckingException("Wrong operation: " + rdfOperation.name());
        }
        rdfDatasetFile.setStatusMessage("");
        this.itemService.save(rdfDatasetFile);
        ((RdfDatasetFileService) this.itemService).putInProcessingQueue(rdfDatasetFile, triplestoreDatasetId, rdfOperation);
        result.setStatus(ActionStatus.EXECUTED);
        result.setMesssage(this.messageService.get("message.rdfDatasetFile.import.started", new Object[] { rdfDatasetFileId }));
      } catch (SolidifyUndeletableException e) {
        result.setMesssage(e.getMessage());
      }
    }
    return result;
  }

  private ResponseEntity<Void> bulkImport(String triplestoreDatasetId, String[] rdfFileIds, RdfOperation action) {
    Map<String, String> errorList = new HashMap<>();
    for (final String rdfFileId : rdfFileIds) {
      final Result result = this.launchImport(rdfFileId, triplestoreDatasetId, action);
      if (result.getStatus() != ActionStatus.EXECUTED) {
        errorList.put(rdfFileId, result.getMessage());
      }
    }
    // Check if there is error
    if (!errorList.isEmpty()) {
      throw new SolidifyBulkActionException(this.messageService.get("message.rdfDatasetFile.bulkAction", new Object[] { action }), errorList);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
