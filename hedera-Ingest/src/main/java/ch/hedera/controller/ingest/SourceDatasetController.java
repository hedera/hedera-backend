/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - SourceDatasetController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.ingest;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.validation.ValidationError;

import ch.hedera.HederaConstants;
import ch.hedera.business.SourceDatasetFileService;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.HederaResourceWithFileController;
import ch.hedera.controller.IngestController;
import ch.hedera.controller.UploadZipController;
import ch.hedera.model.dto.FileUploadDto;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.HistoryService;

@RestController
@ConditionalOnBean(IngestController.class)
@RequestMapping(UrlPath.INGEST_SOURCE_DATASET)
public class SourceDatasetController extends HederaResourceWithFileController<SourceDataset> implements UploadZipController {

  private final SourceDatasetFileService sourceDatasetFileService;

  public SourceDatasetController(HederaProperties hederaProperties, HistoryService historyService,
          SourceDatasetFileService sourceDatasetFileService) {
    super(hederaProperties, historyService);
    this.sourceDatasetFileService = sourceDatasetFileService;
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowedToCreate(#sourceDataset)")
  public HttpEntity<SourceDataset> create(@RequestBody SourceDataset sourceDataset) {
    return super.create(sourceDataset);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#id, 'GET')")
  public HttpEntity<SourceDataset> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowedToListWithProjectId(#search, 'LIST')")
  public HttpEntity<RestCollection<SourceDataset>> list(@ModelAttribute SourceDataset search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#id, 'UPDATE')")
  public HttpEntity<SourceDataset> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#id, 'DELETE')")
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowedToDeleteList(#ids, 'DELETE')")
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @Override
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.UPLOAD_LOGO)
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#id, 'UPDATE')")
  public HttpEntity<SourceDataset> uploadFile(
          @PathVariable String id,
          @RequestParam(HederaConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(id, file, mimeType);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#id, 'GET')")
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.DOWNLOAD_LOGO)
  public HttpEntity<StreamingResponseBody> downloadFile(@PathVariable String id) {
    return super.downloadFile(id);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#id, 'DELETE')")
  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.DELETE_LOGO)
  public HttpEntity<SourceDataset> deleteFile(@PathVariable String id) {
    return super.deleteFile(id);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#resource, 'LIST')")
  public HttpEntity<RestCollection<SourceDataset>> advancedSearch(@ModelAttribute SourceDataset resource,
          @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(resource, search, matchtype, pageable);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#search, 'LIST')")
  public HttpEntity<RestCollection<SourceDataset>> advancedSearch(@RequestBody SourceDataset search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(search, matchtype, pageable);
  }

  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#sourceDatasetId, 'UPLOAD_FILE')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.UPLOAD)
  public HttpEntity<SourceDatasetFile> uploadFile(
          @PathVariable("id") String sourceDatasetId,
          @RequestParam(HederaConstants.FILE) MultipartFile file,
          @RequestParam(HederaConstants.VERSION) String version,
          @RequestParam(HederaConstants.DATASET_FILE_TYPE) String datasetFileType) throws IOException {

    final SourceDataset sourceDataset = this.getSourceDataset(sourceDatasetId);
    FileUploadDto<SourceDatasetFile> fileUploadDto = this.getFileUploadDto(file);

    // Check temporary folder
    String finalDir = this.config.getSourceDatasetLocation() + File.separatorChar + sourceDatasetId;
    Path finalPath = Paths.get(finalDir);
    if (!FileTool.ensureFolderExists(finalPath)) {
      throw new IOException(this.messageService.get("message.upload.folder.notcreated", new Object[] { finalDir }));
    }

    String filename = null;
    try {

      filename = this.sourceDatasetFileService.generateFilename(file.getOriginalFilename(), version);
      Files.copy(file.getInputStream(), finalPath.resolve(filename));
    } catch (NullPointerException e) {
      throw new SolidifyValidationException(new ValidationError("Missing original file name"));
    }

    SourceDatasetFile sourceDatasetFile = this.sourceDatasetFileService.getSourceDatasetFile(sourceDataset, datasetFileType,
            file.getOriginalFilename(), version, finalPath.resolve(filename), fileUploadDto);
    sourceDatasetFile = this.sourceDatasetFileService.save(sourceDatasetFile);

    return new ResponseEntity<>(sourceDatasetFile, HttpStatus.OK);
  }

  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#sourceDatasetId, 'UPDATE_FILE')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.UPLOAD_ZIP)
  public HttpEntity<List<SourceDatasetFile>> uploadArchive(
          @PathVariable("id") String sourceDatasetId,
          @RequestParam(HederaConstants.FILE) MultipartFile archiveFile,
          @RequestParam(HederaConstants.VERSION) String version,
          @RequestParam(HederaConstants.DATASET_FILE_TYPE) String datasetFileType) throws IOException {
    final SourceDataset sourceDataset = this.getSourceDataset(sourceDatasetId);
    final String tmpFolder = this.config.getTempLocation(this.config.getSourceDatasetLocation()) + File.separatorChar
            + sourceDatasetId;
    final String extractFolder = tmpFolder + File.separatorChar + archiveFile.getOriginalFilename() + "_" + Instant.now().toEpochMilli();
    final List<Path> extractedFilesPaths = this.uploadZip(tmpFolder, archiveFile, extractFolder, this.messageService);

    // Save source dataset files
    final String destinationFolder = this.config.getSourceDatasetLocation() + File.separatorChar + sourceDatasetId;
    final List<SourceDatasetFile> filesList = this.getSourceDatasetFileList(sourceDataset, destinationFolder, version, datasetFileType,
            extractFolder, extractedFilesPaths);
    filesList.forEach(this.sourceDatasetFileService::save);
    return new ResponseEntity<>(filesList, HttpStatus.OK);
  }

  private SourceDataset getSourceDataset(String parentid) {
    final SourceDataset sourceDataset = this.itemService.findOne(parentid);
    if (!sourceDataset.isModifiable()) {
      throw new IllegalStateException(this.messageService.get("validation.resource.unmodifiable",
              new Object[] { sourceDataset.getClass().getSimpleName() + ": " + sourceDataset.getResId() }));
    }
    return sourceDataset;
  }

  private List<SourceDatasetFile> getSourceDatasetFileList(SourceDataset sourceDataset, String destinationFolder, String version,
          String datasetFileType, String extractFolder, List<Path> extractedFiles) {
    final List<SourceDatasetFile> fileList = new ArrayList<>();
    final Path extractFolderPath = Paths.get(extractFolder);
    final Path destinationPath = Paths.get(destinationFolder);
    for (final Path extractedFilePath : extractedFiles) {
      // Skip Folder
      if (FileTool.isFolder(extractedFilePath)) {
        continue;
      }
      try {
        final Path relativePath = extractFolderPath.toAbsolutePath().relativize(extractedFilePath);
        final String filename = this.sourceDatasetFileService.generateFilename(relativePath.toString(), version);
        final Path storedFile = destinationPath.resolve(filename);
        FileTool.moveFile(extractedFilePath, storedFile);
        final SourceDatasetFile sourceDatasetFile = this.sourceDatasetFileService.getSourceDatasetFile(sourceDataset, datasetFileType,
                relativePath.toString(), version, storedFile);
        fileList.add(sourceDatasetFile);
      } catch (NullPointerException e) {
        throw new SolidifyValidationException(new ValidationError("Missing original file name"));
      } catch (IOException e) {
        throw new SolidifyProcessingException("Cannot add source dataset file", e);
      }

    }
    return fileList;
  }

  private FileUploadDto<SourceDatasetFile> getFileUploadDto(MultipartFile file) throws IOException {
    FileUploadDto<SourceDatasetFile> fileUploadDto = new FileUploadDto<>(new SourceDatasetFile());
    fileUploadDto.setOriginalFileName(Normalizer.normalize(Objects.requireNonNull(file.getOriginalFilename()), Normalizer.Form.NFC));
    fileUploadDto.setInputStream(file.getInputStream());
    return fileUploadDto;
  }
}
