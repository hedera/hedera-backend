/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - SourceDatasetSourceDatasetFileController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.ingest;

import static ch.hedera.HederaConstants.SOURCE_DATASET_RES_ID;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyBulkActionException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringParserTool;

import ch.hedera.config.HederaProperties;
import ch.hedera.controller.HederaCompositionController;
import ch.hedera.controller.IngestController;
import ch.hedera.dto.MappingParameters;
import ch.hedera.message.RmlConversionMessage;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.StatusHistory;
import ch.hedera.model.ingest.DatasetFileType;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.ingest.SourceDatasetFile.SourceDatasetFileStatus;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ResourceName;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.HistoryService;

@RestController
@ConditionalOnBean(IngestController.class)
@RequestMapping(UrlPath.INGEST_SOURCE_DATASET + SolidifyConstants.URL_PARENT_ID + ResourceName.SOURCE_DATASET_FILE)
public class SourceDatasetSourceDatasetFileController extends HederaCompositionController<SourceDataset, SourceDatasetFile> {

  private final HederaProperties hederaProperties;

  public SourceDatasetSourceDatasetFileController(HederaProperties hederaProperties, HistoryService historyService) {
    super(historyService);
    this.hederaProperties = hederaProperties;
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#parentid, 'CREATE_FILE')")
  public HttpEntity<SourceDatasetFile> create(@PathVariable final String parentid, final @Valid @RequestBody SourceDatasetFile childItem) {
    return super.create(parentid, childItem);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#parentid, 'GET_FILE')")
  public HttpEntity<SourceDatasetFile> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#parentid, 'LIST_FILES')")
  public HttpEntity<RestCollection<SourceDatasetFile>> list(@PathVariable String parentid, @ModelAttribute SourceDatasetFile filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#parentid, 'UPDATE_FILE')")
  public HttpEntity<SourceDatasetFile> update(@PathVariable final String parentid, @PathVariable final String id,
          @RequestBody final Map<String, Object> newChildItem) {
    return super.update(parentid, id, newChildItem);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#parentid, 'DELETE_FILE')")
  public ResponseEntity<Void> delete(@PathVariable final String parentid, @PathVariable final String id) {
    return super.delete(parentid, id);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#parentid, 'DELETE_FILE')")
  public ResponseEntity<Void> deleteList(@PathVariable final String parentid, @RequestBody String[] ids) {
    return super.deleteList(parentid, ids);
  }

  @Override
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#parentid, 'DOWNLOAD_FILE')")
  public HttpEntity<FileSystemResource> download(@PathVariable String parentid, @PathVariable String id) {
    return super.download(parentid, id);
  }

  @GetMapping("/" + ActionName.SEARCH)
  @SuppressWarnings("squid:S4684")
  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#parentid, 'LIST_FILES')")
  public HttpEntity<RestCollection<SourceDatasetFile>> search(@PathVariable String parentid, @ModelAttribute SourceDatasetFile sourceDatasetFile,
          @RequestParam("search") String search, @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    if (!this.resourceService.existsById(parentid)) {
      throw new NoSuchElementException("No SourceDataset with id " + parentid);
    }
    final List<SearchCriteria> criterias = StringParserTool.parseSearchString(search);
    final SearchCriteria parent = new SearchCriteria(SOURCE_DATASET_RES_ID, "~", parentid);
    final Page<SourceDatasetFile> itemlist = this.subResourceService.findBySubResourceSearchCriteria(sourceDatasetFile, matchtype, criterias,
            parent,
            pageable);
    for (final SourceDatasetFile sdf : itemlist) {
      this.addLinks(parentid, sdf);
    }
    final RestCollection<SourceDatasetFile> collection = this.processPageAndLinks(itemlist, parentid, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @Override
  protected SourceDataset getParentResourceProperty(SourceDatasetFile subResource) {
    return subResource.getSourceDataset();
  }

  @Override
  protected void setParentResourceProperty(SourceDatasetFile subResource, SourceDataset parentResource) {
    subResource.setDataset(parentResource);
  }

  @PostMapping({ SolidifyConstants.URL_ID + "/" + HederaActionName.CHECK })
  public HttpEntity<Result> check(@PathVariable String parentid, @PathVariable String id) {
    final Result result = this.launchTransform(parentid, id, null);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping({ SolidifyConstants.URL_ID + "/" + HederaActionName.APPLY_RML })
  public HttpEntity<Result> applyRml(@PathVariable String parentid, @PathVariable String id,
          @Valid @RequestBody MappingParameters rmlMappingParameters) {
    final Result result = this.launchTransform(parentid, id, rmlMappingParameters);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping({ "/" + HederaActionName.APPLY_RML })
  public HttpEntity<Void> applyRmlList(@PathVariable String parentid, @Valid @RequestBody MappingParameters rmlMappingParameters) {
    Map<String, String> errorList = new HashMap<>();
    for (final String id : rmlMappingParameters.getSourceDatasetFileIds()) {
      final Result result = this.launchTransform(parentid, id, rmlMappingParameters);
      if (result.getStatus() != ActionStatus.EXECUTED) {
        errorList.put(id, result.getMessage());
      }
    }
    // Check if there is error
    if (!errorList.isEmpty()) {
      throw new SolidifyBulkActionException(this.messageService.get("message.sourceDatasetFile.bulkRmlApplication"), errorList);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PostMapping({ SolidifyConstants.URL_ID + "/" + HederaActionName.TRANSFORM_RDF })
  public HttpEntity<Result> transformRdf(@PathVariable String parentid, @PathVariable String id) {
    final MappingParameters rmlParametersForRdf = new MappingParameters();
    rmlParametersForRdf.setRdfFormat(RdfFormat.N_TRIPLES);
    final Result result = this.launchTransform(parentid, id, rmlParametersForRdf);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping({ "/" + HederaActionName.TRANSFORM_RDF })
  public HttpEntity<Void> transformRdfList(@PathVariable String parentid, @RequestBody String[] ids) {
    Map<String, String> errorList = new HashMap<>();
    for (final String id : ids) {
      final MappingParameters rmlParametersForRdf = new MappingParameters();
      rmlParametersForRdf.setRdfFormat(RdfFormat.N_TRIPLES);
      final Result result = this.launchTransform(parentid, id, rmlParametersForRdf);
      if (result.getStatus() != ActionStatus.EXECUTED) {
        errorList.put(id, result.getMessage());
      }
    }
    // Check if there is error
    if (!errorList.isEmpty()) {
      throw new SolidifyBulkActionException(this.messageService.get("message.sourceDatasetFile.bulkRdfTransform"), errorList);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  private Result launchTransform(String parentid, String id, MappingParameters rmlMappingParameters) {
    SourceDatasetFile sourceDatasetFile = this.checkSourceDataFile(parentid, id);
    final Result result = new Result(id);
    if (!sourceDatasetFile.getDataset().getProject().isOpen()) {
      result.setMesssage(
              this.messageService.get("message.project.closed", new Object[] { sourceDatasetFile.getDataset().getProject().getName() }));
    } else if (sourceDatasetFile.getType().equals(DatasetFileType.RDF)
            && rmlMappingParameters != null
            && (rmlMappingParameters.getRmlFileId() != null
            || !rmlMappingParameters.getRdfFormat().equals(RdfFormat.N_TRIPLES))) {
      result.setMesssage(
              this.messageService.get("message.sourceDatasetFile.rdf.wrongParameters",
                      new Object[] { rmlMappingParameters.getRmlFileId(), rmlMappingParameters.getRdfFormat() }));
    } else {
      result.setStatus(ActionStatus.NOT_EXECUTED);
      boolean isLongProcessing = sourceDatasetFile.getFileSize() > this.hederaProperties.getQueue().getSourceDatasetBigFileLimit();
      sourceDatasetFile.setStatusMessage("");
      if (rmlMappingParameters == null) {
        sourceDatasetFile.setStatus(SourceDatasetFileStatus.TO_CHECK);
        this.subResourceService.save(sourceDatasetFile);
        result.setMesssage(this.messageService.get("message.sourceDatasetFile.check.started"));
        SolidifyEventPublisher.getPublisher()
                .publishEvent(new RmlConversionMessage(id, isLongProcessing));
      } else {
        sourceDatasetFile.setStatus(SourceDatasetFileStatus.TO_TRANSFORM);
        this.subResourceService.save(sourceDatasetFile);
        result.setMesssage(this.messageService.get("message.sourceDatasetFile.transform.started"));
        SolidifyEventPublisher.getPublisher()
                .publishEvent(
                        new RmlConversionMessage(id, rmlMappingParameters.getRmlFileId(), rmlMappingParameters.getRdfFormat(),
                                isLongProcessing));
      }
      result.setStatus(ActionStatus.EXECUTED);
    }
    return result;
  }

  @AdminPermissions
  @PostMapping({ SolidifyConstants.URL_ID + "/" + HederaActionName.PUT_IN_ERROR })
  public HttpEntity<Result> putInError(@PathVariable String parentid, @PathVariable("id") String sourceDatasetFileId) {
    final SourceDatasetFile sourceDatasetFile = this.checkSourceDataFile(parentid, sourceDatasetFileId);
    final Result result = this.putInError(sourceDatasetFile);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @AdminPermissions
  @PostMapping({ "/" + HederaActionName.PUT_IN_ERROR })
  public HttpEntity<Void> putInErrorList(@PathVariable String parentid, @RequestBody String[] ids) {
    Map<String, String> errorList = new HashMap<>();
    for (final String id : ids) {
      final SourceDatasetFile sourceDatasetFile = this.checkSourceDataFile(parentid, id);
      final Result result = this.putInError(sourceDatasetFile);
      if (result.getStatus() != ActionStatus.EXECUTED) {
        errorList.put(id, result.getMessage());
      }
    }
    // Check if there is error
    if (!errorList.isEmpty()) {
      throw new SolidifyBulkActionException(this.messageService.get("message.sourceDatasetFile.bulkPutInError"), errorList);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  private Result putInError(SourceDatasetFile sourceDatasetFile) {
    final Result result = new Result(sourceDatasetFile.getResId());

    if (SourceDatasetFileStatus.TRANSFORMED.equals(sourceDatasetFile.getStatus())) {
      result.setMesssage(this.messageService.get("message.sourceDatasetFile.alreadyTransformed", new Object[] { sourceDatasetFile.getResId() }));
    } else {
      sourceDatasetFile.setStatus(SourceDatasetFileStatus.IN_ERROR);
      this.subResourceService.save(sourceDatasetFile);
      result.setStatus(ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("message.sourceDatasetFile.hasBeenPutInError", new Object[] { sourceDatasetFile.getResId() }));
    }
    return result;
  }

  private SourceDatasetFile checkSourceDataFile(String parentid, String id) {
    if (!this.resourceService.existsById(parentid)) {
      throw new NoSuchElementException("No SourceDataset with id " + parentid);
    }
    if (!this.subResourceService.existsById(id)) {
      throw new NoSuchElementException("No SourceDatasetFile with id " + id);
    }
    return this.subResourceService.findOne(id);
  }

  @PreAuthorize("@sourceDatasetPermissionService.isAllowed(#parentid, 'HISTORY')")
  @Override
  public HttpEntity<RestCollection<StatusHistory>> history(@PathVariable String parentid, @PathVariable String id, Pageable pageable) {
    return super.history(parentid, id, pageable);
  }
}
