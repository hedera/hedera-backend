/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - ResearchDataFileProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.researchdatafile;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import jakarta.xml.bind.JAXBException;

import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.business.ResearchDataFileService;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.IngestController;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.ResearchDataFile.ResearchDataFileFileStatus;
import ch.hedera.rest.ResourceName;
import ch.hedera.service.HederaIngestService;
import ch.hedera.service.IdMappingService;
import ch.hedera.service.MetadataService;
import ch.hedera.service.rest.trusted.TrustedResearchObjectMetadataRemoteResourceService;
import ch.hedera.service.rml.RmlMappingService;
import ch.hedera.service.storage.ProjectStorage;
import ch.hedera.service.triplestore.TriplestoreResourceService;

@Service
@ConditionalOnBean(IngestController.class)
public class ResearchDataFileProcessingService extends HederaIngestService {

  private static final Logger log = LoggerFactory.getLogger(ResearchDataFileProcessingService.class);

  private static final String RESEARCH_DATA_FILE_RDF_FILE = "research-data-file.ttl";

  private final String researchDataFileLocation;

  private final String iiifImageBaseURL;
  private final String iiifDefaultVersion;
  private final String iiifDefaultParameters;
  private final int iiifMaxSize;

  private final String accessUrl;

  private final ResearchDataFileService researchDataFileService;
  private final RmlMappingService rmlMappingService;
  private final TriplestoreResourceService triplestoreResourceService;
  private final TrustedResearchObjectMetadataRemoteResourceService indexingService;
  private final MetadataService metadataService;
  private final IdMappingService idMappingService;

  public ResearchDataFileProcessingService(
          MessageService messageService,
          HederaProperties hederaProperties,
          ResearchDataFileService researchDataFileService,
          RmlMappingService rmlMappingService,
          TriplestoreResourceService triplestoreResourceService,
          TrustedResearchObjectMetadataRemoteResourceService indexingService,
          MetadataService metadataService,
          IdMappingService idMappingService) {
    super(messageService, hederaProperties);
    this.researchDataFileLocation = hederaProperties.getResearchDataFileLocation();
    this.accessUrl = hederaProperties.getModule().getAccess().getPublicUrl();
    this.iiifImageBaseURL = hederaProperties.getModule().getIIIF().getPublicUrl();
    this.iiifDefaultVersion = hederaProperties.getIIIF().getDefaultVersion();
    this.iiifDefaultParameters = hederaProperties.getIIIF().getDefaultParameters();
    this.iiifMaxSize = hederaProperties.getIIIF().getMaxSize();
    this.researchDataFileService = researchDataFileService;
    this.rmlMappingService = rmlMappingService;
    this.triplestoreResourceService = triplestoreResourceService;
    this.indexingService = indexingService;
    this.metadataService = metadataService;
    this.idMappingService = idMappingService;
  }

  @Transactional
  public ResearchDataFile processResearchDataFile(String researchDataFileId, ProjectStorage storageService) {
    // Load research data file
    final ResearchDataFile researchDataFile = this.researchDataFileService.findOne(researchDataFileId);
    try {
      if (!researchDataFile.getProject().isOpen()) {
        throw new SolidifyCheckingException(this.messageService.get("ingest.project.closed"));
      }
      switch (researchDataFile.getStatus()) {
        case UPLOADED -> this.startProcessResearchDataFile(researchDataFile);
        case PROCESSING -> this.processResearchDataFile(researchDataFile, storageService);
        case STORED -> this.processStoredResearchDataFile(researchDataFile);
        case REFERENCED -> this.processReferencedResearchDataFile(researchDataFile);
        case CHANGE_RELATIVE_LOCATION -> this.moveRelativeLocation(researchDataFile, storageService);
        default -> throw new SolidifyRuntimeException(
                this.messageService.get("ingest.error.status", new Object[] { researchDataFile.getStatus() }));
      }

    } catch (final SolidifyCheckingException e) {
      log.error("Checking error while processing ResearchDataFile " + researchDataFile.getResId(), e);
      researchDataFile
              .setErrorStatusWithMessage(this.messageService.get("ingest.error.check", new Object[] { e.getMessage() }));
    } catch (final SolidifyProcessingException e) {
      log.error("Processing error while processing ResearchDataFile " + researchDataFile.getResId(), e);
      researchDataFile
              .setErrorStatusWithMessage(this.messageService.get("ingest.error.process", new Object[] { e.getMessage() }));
    } catch (final Exception e) {
      log.error("Error while processing ResearchDataFile " + researchDataFile.getResId(), e);
      researchDataFile.setErrorStatusWithMessage(this.messageService.get("ingest.error", new Object[] { e.getMessage() }));
    }
    return this.researchDataFileService.save(researchDataFile);
  }

  private void startProcessResearchDataFile(ResearchDataFile researchDataFile) {
    // start process
    researchDataFile.setStatus(ResearchDataFileFileStatus.PROCESSING);
  }

  private void moveRelativeLocation(ResearchDataFile researchDataFile, ProjectStorage storageService) throws URISyntaxException, IOException {
    // Remove RDF triples
    this.researchDataFileService.deleteResearchDatasetFromFuseki(researchDataFile);
    // Copy file applying new relative Location
    storageService.storeResearchDataFile(researchDataFile);
    // Delete file from previous location
    storageService.deleteResearchDataFile(researchDataFile);
    // Update new location
    researchDataFile.setSourcePath(storageService.getStorageLocation(researchDataFile));
    researchDataFile.getMetadata().put(HederaConstants.FILE_WEB_URL, this.getResearchDataFileAccessUrl(researchDataFile));
    // Update ID mapping
    this.idMappingService.updateBusinessId(researchDataFile.getHederaId(), researchDataFile.getRelativeLocation(),
            researchDataFile.getFileName());
    // Update status
    researchDataFile.setStatus(ResearchDataFileFileStatus.STORED);
  }

  private void processResearchDataFile(ResearchDataFile researchDataFile, ProjectStorage storageService)
          throws URISyntaxException, IOException {
    // Detect content type
    this.extractContentType(researchDataFile);
    // Extract file information (image size, doc page number...)
    this.extractFileMetadata(researchDataFile);
    // Copy file from temp location to project location
    storageService.storeResearchDataFile(researchDataFile);
    // Delete temporary file
    FileTool.deleteFile(researchDataFile.getSourcePath());
    // Update new location
    researchDataFile.setSourcePath(storageService.getStorageLocation(researchDataFile));
    // Update status
    researchDataFile.setStatus(ResearchDataFileFileStatus.STORED);
  }

  private void extractContentType(ResearchDataFile researchDataFile) {
    if (researchDataFile.getMimeType() != null) {
      return;
    }
    Tika tika = new Tika();
    String tikaMimeType;
    try {
      tikaMimeType = tika.detect(FileTool.getInputStream(Paths.get(researchDataFile.getSourcePath())));
      researchDataFile.setMimeType(tikaMimeType);
    } catch (IOException e) {
      tikaMimeType = tika.detect(researchDataFile.getFileName());
    }
    if (tikaMimeType != null) {
      researchDataFile.setMimeType(tikaMimeType);
    }
  }

  private void extractFileMetadata(ResearchDataFile researchDataFile) {
    final Map<String, Object> fileMetadata = new HashMap<>();
    if (researchDataFile.isImage()) {
      // Image metadata
      this.extractImageMetadata(fileMetadata, researchDataFile.getSourcePath());
    } else if (researchDataFile.isPdfDocument()) {
      // PDF Metadata
      this.extractPdfMetadata(fileMetadata, researchDataFile.getSourcePath());
    }
    researchDataFile.setMetadata(fileMetadata);
    // Web Access
    researchDataFile.getMetadata().put(HederaConstants.FILE_WEB_URL, this.getResearchDataFileAccessUrl(researchDataFile));
  }

  private String getResearchDataFileAccessUrl(ResearchDataFile researchDataFile) {
    final String projectShortName = researchDataFile.getProject().getShortName();
    String researchDataFileUrlPart;
    if ("/".equals(researchDataFile.getRelativeLocation())) {
      researchDataFileUrlPart = "/" + researchDataFile.getFileName();
    } else {
      researchDataFileUrlPart = researchDataFile.getRelativeLocation() + "/" + researchDataFile.getFileName();
    }
    researchDataFileUrlPart = researchDataFileUrlPart.replace(" ", "%20");
    if (researchDataFile.getAccessibleFrom().equals(DataAccessibilityType.IIIF)) {
      return this.iiifImageBaseURL
              + "/" + this.iiifDefaultVersion
              + "/" + projectShortName
              + researchDataFileUrlPart
              + this.generateIIIFParameter(researchDataFile);
    } else {
      return this.accessUrl
              + "/" + ResourceName.PROJECT
              + "/" + projectShortName
              + "/" + ResourceName.RESEARCH_DATA_FILE
              + researchDataFileUrlPart
              + "/" + ActionName.DOWNLOAD;
    }
  }

  private void extractPdfMetadata(Map<String, Object> fileMetadata, URI sourcePath) {
    try (InputStream inputStream = FileTool.getInputStream(Paths.get(sourcePath));
            PdfDocument pdf = new PdfDocument(new PdfReader(inputStream))) {
      fileMetadata.put(HederaConstants.FILE_METADATA_PAGES, pdf.getNumberOfPages());
      fileMetadata.put(HederaConstants.FILE_METADATA_PDF_VERSION, pdf.getPdfVersion().toString());
    } catch (IOException e) {
      throw new SolidifyProcessingException("Cannot extract document information", e);
    }
  }

  private void extractImageMetadata(Map<String, Object> fileMetadata, URI sourcePath) {
    try (InputStream inputStream = FileTool.getInputStream(Paths.get(sourcePath))) {
      BufferedImage image = ImageIO.read(inputStream);
      fileMetadata.put(HederaConstants.FILE_METADATA_WIDTH, image.getWidth());
      fileMetadata.put(HederaConstants.FILE_METADATA_HEIGHT, image.getHeight());
    } catch (IOException e) {
      throw new SolidifyProcessingException("Cannot extract image information", e);
    }
  }

  private void processStoredResearchDataFile(ResearchDataFile researchDataFile) throws IOException, JAXBException {
    // Apply RML file to convert file information in RDF
    researchDataFile = this.rmlMappingService.applyRml(
            researchDataFile.getProject(),
            researchDataFile,
            RdfFormat.TURTLE);
    // Update status
    researchDataFile.setStatus(ResearchDataFileFileStatus.REFERENCED);
  }

  private void processReferencedResearchDataFile(ResearchDataFile researchDataFile) throws IOException, JAXBException {
    // RDF metadata in file
    try (InputStream inputStream = StringTool.toInputStream(researchDataFile.getRdfMetadata())) {
      final Path rdfFile = Paths.get(this.researchDataFileLocation, researchDataFile.getResId(), RESEARCH_DATA_FILE_RDF_FILE);
      if (FileTool.copyFile(inputStream, rdfFile)) {
        // Add RDF information in database
        Resource rdfMetadata = new FileSystemResource(rdfFile);
        this.triplestoreResourceService.addDatasetContent(researchDataFile.getProject().getShortName(), rdfMetadata);
        FileTool.deleteFile(rdfFile);
      } else {
        throw new SolidifyProcessingException("Cannot store RDF information");
      }
    }
    // Index RDF information in index database
    this.indexRdfInfo(ResourceName.PRIVATE_INDEX, this.getPrivateIndex(), researchDataFile);

    if (researchDataFile.getProject().isAccessPublic().booleanValue()) {
      this.indexRdfInfo(ResourceName.PUBLIC_INDEX, this.getPublicIndex(), researchDataFile);
    }

    // Update status
    researchDataFile.setStatus(ResearchDataFileFileStatus.COMPLETED);
  }

  private void indexRdfInfo(String indexType, String indexName, ResearchDataFile researchDataFile)
          throws JAXBException, JsonProcessingException {
    if (this.indexingService.checkMetadata(indexType, researchDataFile.getResId())
            && !this.indexingService.deleteIndexMetadata(indexType, researchDataFile.getResId())) {
      throw new SolidifyProcessingException(
              "Cannot delete index information for " + researchDataFile.getResId() + " in " + indexType);
    }
    if (!this.indexingService.createIndexMetadata(indexType,
            new ResearchObjectMetadata(
                    indexName,
                    researchDataFile.getResId(),
                    this.metadataService.getMetadataInJson(researchDataFile)))) {
      throw new SolidifyProcessingException("Cannot index information for " + researchDataFile.getResId() + " in " + indexName);
    }
  }

  @Transactional
  public void reloadResearchDataFiles(String projectId) {
    for (ResearchDataFile researchDataFile : this.researchDataFileService.findByProjectIdAndStatus(projectId,
            ResearchDataFileFileStatus.COMPLETED)) {
      log.info("Reloading research data file: {}", researchDataFile.getResId());
      researchDataFile.setStatus(ResearchDataFileFileStatus.REFERENCED);
      this.researchDataFileService.save(researchDataFile);
      this.researchDataFileService.putInProcessingQueue(researchDataFile);
    }
  }

  private String generateIIIFParameter(ResearchDataFile researchDataFile) {
    // IIIF specification: https://iiif.io/api/image/3.0/#21-image-request-uri-syntax
    // Template: /{region}/{size}/{rotation}/{quality}.{format}

    // Check if there are image information
    if (researchDataFile.getMetadata() == null
            || !researchDataFile.getMetadata().containsKey(HederaConstants.FILE_METADATA_WIDTH)
            || !researchDataFile.getMetadata().containsKey(HederaConstants.FILE_METADATA_HEIGHT)) {
      return this.iiifDefaultParameters;
    }

    // Get Image size
    final int width = (int) researchDataFile.getMetadata().get(HederaConstants.FILE_METADATA_WIDTH);
    final int height = (int) researchDataFile.getMetadata().get(HederaConstants.FILE_METADATA_HEIGHT);
    // Check if < max size
    if (width <= this.iiifMaxSize && height <= this.iiifMaxSize) {
      // Return default
      return this.iiifDefaultParameters;
    }
    // Generate default parameters
    StringBuilder iiifParameters = new StringBuilder("/full/");
    if (width > height) {
      iiifParameters
              .append(this.iiifMaxSize)
              .append(",");
    } else {
      iiifParameters
              .append(",")
              .append(this.iiifMaxSize);
    }
    iiifParameters.append("/0/default.jpg");
    return iiifParameters.toString();
  }
}
