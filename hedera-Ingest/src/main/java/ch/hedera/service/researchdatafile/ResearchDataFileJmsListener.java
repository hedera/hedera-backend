/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - ResearchDataFileJmsListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.researchdatafile;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.hedera.business.ResearchDataFileService;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.IngestController;
import ch.hedera.message.ResearchDataFileMessage;
import ch.hedera.message.ResearchDataFileReloadMessage;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.service.MessageProcessorBySize;
import ch.hedera.service.storage.ProjectStorage;

@Service
@ConditionalOnBean(IngestController.class)
public class ResearchDataFileJmsListener extends MessageProcessorBySize<ResearchDataFileMessage> {

  private static final Logger log = LoggerFactory.getLogger(ResearchDataFileJmsListener.class);

  private final ResearchDataFileService researchDataFileService;
  private final ResearchDataFileProcessingService researchDataFileProcessingService;

  protected ResearchDataFileJmsListener(
          HederaProperties configService,
          ResearchDataFileService researchDataFileService,
          ResearchDataFileProcessingService researchDataFileProcessingService) {
    super(configService);
    this.researchDataFileService = researchDataFileService;
    this.researchDataFileProcessingService = researchDataFileProcessingService;
  }

  @JmsListener(destination = "${hedera.queue.researchDataFile}")
  @Override
  public void receiveMessage(ResearchDataFileMessage researchDataFileMessage) {
    this.sendForParallelProcessing(researchDataFileMessage);
  }

  @Override
  public void processMessage(ResearchDataFileMessage researchDataFileMessage) {
    log.info("Reading message {}", researchDataFileMessage);
    if (researchDataFileMessage instanceof ResearchDataFileReloadMessage reloadMessage) {
      // Re-load all research data files
      log.info("Research data file reload started");
      this.researchDataFileProcessingService.reloadResearchDataFiles(reloadMessage.getResId());
      log.info("Research data file reload completed");
    } else {
      // Process the specific research data file
      try {
        ResearchDataFile researchDataFile = this.researchDataFileService.findOne(researchDataFileMessage.getResId());
        final ProjectStorage storageService = this.getProjectStorage(researchDataFile);
        log.info("Research data file '{}' ({}) started", researchDataFile.getResId(), researchDataFile.getFileName());
        while (storageService != null && researchDataFile.isInProgress()) {
          // Process research data file
          researchDataFile = this.researchDataFileProcessingService.processResearchDataFile(researchDataFileMessage.getResId(), storageService);
        }
        log.info("Research data file '{}' ({}) processed", researchDataFile.getResId(), researchDataFile.getFileName());
      } catch (NoSuchElementException e) {
        log.error("Cannot find research data file ({})", researchDataFileMessage.getResId());
      }
    }
  }

  private ProjectStorage getProjectStorage(ResearchDataFile researchDataFile) {
    try {
      return ProjectStorage.getProjectStorage(this.configService.getStorage(), researchDataFile.getProject());
    } catch (SolidifyRuntimeException e) {
      log.error("Unable to get ProjectStorage for project '{}' ", researchDataFile.getProject(), e);
      researchDataFile.setErrorStatusWithMessage(e.getMessage());
      this.researchDataFileService.save(researchDataFile);
    }
    return null;
  }

}
