/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - IdMappingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.rdf.model.impl.ResourceImpl;
import org.apache.jena.riot.RDFDataMgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.IngestController;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.ingest.IdMapping;
import ch.hedera.repository.IdMappingRepository;
import ch.hedera.util.RdfTool;

@Service
@ConditionalOnBean(IngestController.class)
public class IdMappingService {

  private static final Logger log = LoggerFactory.getLogger(IdMappingService.class);

  private final String ldBaseUri;
  private final List<String> uriListToMigrate;
  private final IdMappingRepository idMappingRepository;
  private final HederaIdentifierService hederaIdentifierService;
  private final Pattern uriPattern = Pattern.compile("^([^/]+)/([^/]+)/(.*$)");
  private final Pattern idPattern = Pattern.compile("\\d+");

  public IdMappingService(
          HederaIdentifierService hederaIdentifierService,
          IdMappingRepository idMappingRepository,
          HederaProperties config) {
    this.ldBaseUri = config.getModule().getLinkedData().getPublicUrl() + "/";
    this.uriListToMigrate = config.getParameters().getUriListToMigrate();
    this.hederaIdentifierService = hederaIdentifierService;
    this.idMappingRepository = idMappingRepository;
  }

  public void assignId(URI projectUri, Path rdfFile, RdfFormat rdfFormat) {
    final Model businessIdModel = ModelFactory.createDefaultModel();
    final Model hederaIdModel = ModelFactory.createDefaultModel();
    RDFDataMgr.read(businessIdModel, rdfFile.toString());
    long i = 0;
    log.info("Starting processing for {}", rdfFile);
    StmtIterator iter = businessIdModel.listStatements();
    while (iter.hasNext()) {
      i++;
      final Statement stmt = iter.nextStatement();
      final Property originalPredicate = stmt.getPredicate();
      Resource subject = stmt.getSubject();
      RDFNode object = stmt.getObject();
      if (i % 1000 == 0) {
        log.info("Processing {} triplets: {}", i, stmt);
      }
      // Process subject
      if (this.isProjectToMigrateResource(subject)) {
        subject = this.migrateResourceWithHederaId(subject, projectUri, hederaIdModel);
      } else if (this.isHederaResource(projectUri, subject)) {
        subject = this.createResourceWithHederaId(subject, hederaIdModel);
      }
      // Process object
      if (this.isProjectToMigrateResource(object)) {
        object = this.migrateResourceWithHederaId(object.asResource(), projectUri, hederaIdModel);
      } else if (this.isHederaResource(projectUri, object)) {
        object = this.createResourceWithHederaId(object.asResource(), hederaIdModel);
      }
      hederaIdModel.add(subject.asResource(), originalPredicate, object);
    }
    log.info("Completing ID mapping for {}", rdfFile);
    try (FileOutputStream fileOutputStream = new FileOutputStream(rdfFile.toString())) {
      hederaIdModel.write(fileOutputStream, RdfTool.getJenaLanguage(rdfFormat));
    } catch (IOException e) {
      throw new SolidifyProcessingException("Unable to write RDF file" + rdfFile, e);
    }
    log.info("Processing completed for {}", rdfFile);
  }

  public void updateBusinessId(String hederaId, String relativeLocation, String fileName) {
    final Optional<IdMapping> idMappingOptional = this.idMappingRepository.findById(hederaId);
    if (idMappingOptional.isEmpty()) {
      throw new SolidifyCheckingException("Missing mapping for " + hederaId);
    }
    final IdMapping idMapping = idMappingOptional.get();
    idMapping.setBusinessId(relativeLocation + "/" + fileName);
    this.idMappingRepository.save(idMapping);
  }

  private Resource createResourceWithHederaId(Resource businessIdResource, Model hederaIdModel) {
    final String businessUri = StringTool.removePrefix(businessIdResource.getURI(), this.ldBaseUri);
    final Matcher matcher = this.applyUriRegex(businessUri);
    final String projectShortName = matcher.group(1);
    final String researchObjectTypeName = matcher.group(2);
    final String businessId = matcher.group(3);
    final String normalizedBusinessId = this.normalizeBusinessId(businessId);

    Optional<IdMapping> existingMapping = this.idMappingRepository.findByBusinessIdAndProjectShortNameAndResearchObjectTypeName(
            normalizedBusinessId,
            projectShortName, researchObjectTypeName);
    if (existingMapping.isPresent()) {
      final String hederaId = existingMapping.get().getHederaId().toString();
      return new ResourceImpl(businessIdResource.getURI().replace(businessId, hederaId));
    }
    final IdMapping idMapping = this.getNewIdMapping(projectShortName, researchObjectTypeName, normalizedBusinessId);
    return hederaIdModel.createResource(businessIdResource.getURI().replace(businessId, idMapping.getHederaId().toString()));
  }

  private Resource migrateResourceWithHederaId(Resource businessIdResource, URI projectUri, Model hederaIdModel) {
    final String projectShortName = projectUri.toString().substring(projectUri.toString().lastIndexOf("/") + 1);
    final String projectToMigrateUri = this.getProjectToMigrate(businessIdResource.getURI());
    final String businessUri = StringTool.removePrefix(businessIdResource.getURI(), projectToMigrateUri);
    final Matcher matcher = this.applyUriRegex(businessUri);
    final String projectToMigrateName = matcher.group(1);
    final String researchObjectTypeName = matcher.group(2);
    final String businessId = matcher.group(3);
    final String normalizedBusinessId = this.normalizeBusinessId(businessId);
    final Long migratedId = this.extractBusinessId(businessId);

    // Check project to migrate
    if (!businessUri.contains(projectToMigrateName)) {
      throw new SolidifyProcessingException("Cannot project name to migrate inconsitant :" + businessUri + " & " + projectToMigrateName);
    }

    Optional<IdMapping> existingMapping = this.idMappingRepository.findByBusinessIdAndProjectShortNameAndResearchObjectTypeName(
            normalizedBusinessId,
            projectShortName, researchObjectTypeName);
    if (existingMapping.isPresent()) {
      final String hederaId = existingMapping.get().getHederaId().toString();
      final Resource newResource = new ResourceImpl(projectUri.toString() + "/" + researchObjectTypeName + "/" + hederaId);
      this.addSameAsTriple(hederaIdModel, newResource, businessIdResource);
      return newResource;
    }
    // New ID mapping
    final IdMapping idMapping = this.getNewIdMappingForLegacy(projectShortName, researchObjectTypeName, normalizedBusinessId, migratedId);
    // Create RDF Resource
    final String newUri = projectUri.toString() + "/" + researchObjectTypeName + "/" + idMapping.getHederaId();
    final Resource newResource = hederaIdModel.createResource(newUri);
    this.addSameAsTriple(hederaIdModel, newResource, businessIdResource);
    return newResource;
  }

  private void addSameAsTriple(Model model, Resource newResource, Resource legacyResource) {
    // Add triple 'L54_is_same-as'
    Property sameAsProperty = model.createProperty(RdfTool.L54_SAME_AS_URI);
    model.add(newResource, sameAsProperty, legacyResource);
  }

  private IdMapping getNewIdMappingForLegacy(String projectShortName, String researchObjectTypeName, String businessId,
          Long legacyId) {
    if (legacyId >= HederaConstants.HEDERA_SEQUENCE_START) {
      throw new SolidifyProcessingException("Wrong ID to migrate: " + legacyId);
    }
    return this.idMappingRepository.save(new IdMapping(legacyId, projectShortName, researchObjectTypeName, businessId));
  }

  private IdMapping getNewIdMapping(String projectShortName, String researchObjectTypeName, String businessId) {
    final Long newId = this.hederaIdentifierService.generateNewHederaId();
    return this.idMappingRepository.save(new IdMapping(newId, projectShortName, researchObjectTypeName, businessId));
  }

  private Matcher applyUriRegex(String businessUri) {
    final Matcher matcher = this.uriPattern.matcher(businessUri);
    if (!matcher.matches()) {
      throw new SolidifyProcessingException("The format of the resource " + businessUri + " is not correct");
    }
    return matcher;
  }

  private String normalizeBusinessId(String businessId) {
    if (businessId.startsWith("/")) {
      return businessId;
    }
    return "/" + businessId;
  }

  private boolean isHederaResource(URI projectUri, RDFNode resource) {
    return resource.isURIResource()
            && resource.asResource().getURI().startsWith(this.ldBaseUri)
            && resource.asResource().getURI().startsWith(projectUri.toString());
  }

  private boolean isProjectToMigrateResource(RDFNode resource) {
    if (resource.isURIResource()) {
      return this.uriListToMigrate.stream().filter(uri -> resource.asResource().getURI().startsWith(uri)).count() > 0;
    }
    return false;
  }

  private String getProjectToMigrate(String projectUri) {
    for (String projetToMigrate : this.uriListToMigrate) {
      if (projectUri.startsWith(projetToMigrate)) {
        return projetToMigrate;
      }
    }
    throw new SolidifyProcessingException("Cannot identify project for " + projectUri);
  }

  private Long extractBusinessId(String businessId) {
    // Manage if there is relative location
    if (businessId.contains("/")) {
      businessId = businessId.substring(businessId.lastIndexOf("/") + 1);
    }
    final Matcher matcher = this.idPattern.matcher(businessId);
    if (matcher.find()) {
      return Long.valueOf(matcher.group());
    }
    throw new SolidifyProcessingException("The format of the resource identifier " + businessId + " is not correct");
  }

  public List<IdMapping> findByProjectShortName(String projectShortName) {
    return this.idMappingRepository.findByProjectShortName(projectShortName);
  }

  public void delete(Long hederaId) {
    this.idMappingRepository.deleteById(hederaId.toString());
  }

}
