/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - HederaIdentifierService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.hedera.controller.IngestController;
import ch.hedera.model.ingest.HederaIdentifier;
import ch.hedera.repository.HederaIdentifierRepository;

@Service
@ConditionalOnBean(IngestController.class)
public class HederaIdentifierService {

  private final HederaIdentifierRepository hederaIdentifierRepository;

  public HederaIdentifierService(HederaIdentifierRepository hederaIdentifierRepository) {
    this.hederaIdentifierRepository = hederaIdentifierRepository;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public synchronized Long generateNewHederaId() {
    final HederaIdentifier newId = this.hederaIdentifierRepository.save(new HederaIdentifier());
    return newId.getHederaId();
  }

}
