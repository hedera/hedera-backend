/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - ProjectListenerService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import java.util.Map;

import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.service.MessageService;

import ch.hedera.business.ResearchDataFileService;
import ch.hedera.business.SourceDatasetService;
import ch.hedera.controller.IngestController;
import ch.hedera.message.ProjectMessage;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.settings.Project;
import ch.hedera.service.rest.fallback.FallbackProjectRemoteResourceService;
import ch.hedera.service.triplestore.TriplestoreResourceService;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlQueryTool;

@Service
@ConditionalOnBean(IngestController.class)
public class ProjectListenerService extends HederaService {
  private static final Logger log = LoggerFactory.getLogger(ProjectListenerService.class);

  private final FallbackProjectRemoteResourceService projectRemoteResourceService;

  private final TriplestoreResourceService triplestoreResourceService;

  private final SourceDatasetService sourceDatasetService;

  private final ResearchDataFileService researchDataFileService;

  public ProjectListenerService(MessageService messageService,
          FallbackProjectRemoteResourceService projectRemoteResourceService,
          TriplestoreResourceService triplestoreResourceService,
          SourceDatasetService sourceDatasetService,
          ResearchDataFileService researchDataFileService) {
    super(messageService);
    this.projectRemoteResourceService = projectRemoteResourceService;
    this.triplestoreResourceService = triplestoreResourceService;
    this.sourceDatasetService = sourceDatasetService;
    this.researchDataFileService = researchDataFileService;
  }

  @JmsListener(destination = "${hedera.queue.project}")
  public void processProjectData(ProjectMessage projectMessage) {
    log.trace("Reading message {}", projectMessage);

    try {
      final Project project = this.projectRemoteResourceService.getProject(projectMessage.getResId());
      final long fileNumber = this.countResearchDataFiles(project);
      final long totalSize = this.getResearchDataFileTotalSize(project);
      final long tripleNumber = this.countTriples(project);
      final boolean hasData = (this.hasSourceDataset(project) ? true : fileNumber > 0);

      if (project.hasData().booleanValue() != hasData
              || project.getFileNumber() != fileNumber
              || project.getTripleNumber() != tripleNumber
              || project.getTotalSize() != totalSize) {
        // Update project
        this.projectRemoteResourceService.updateProject(projectMessage.getResId(),
                Map.of("hasData", hasData,
                        "fileNumber", fileNumber,
                        "tripleNumber", tripleNumber,
                        "totalSize", totalSize));
      }
    } catch (HttpClientErrorException.NotFound e) {
      log.error("Project '{}' not found", projectMessage.getResId());
    } catch (Exception e) {
      log.error("Error with project '{}': {}", projectMessage.getResId(), e.getMessage());
    }
  }

  private boolean hasSourceDataset(Project project) {
    SourceDataset sourceDataset = new SourceDataset();
    sourceDataset.setProjectId(project.getProjectId());
    return !this.sourceDatasetService.findAll(this.sourceDatasetService.getSpecification(sourceDataset), PageRequest.of(0, 1)).isEmpty();
  }

  private long countResearchDataFiles(Project project) {
    ResearchDataFile researchDataFile = new ResearchDataFile();
    researchDataFile.setProjectId(project.getProjectId());
    return this.researchDataFileService.findAll(this.researchDataFileService.getSpecification(researchDataFile), PageRequest.of(0, 1))
            .getTotalElements();
  }

  private long getResearchDataFileTotalSize(Project project) {
    return this.researchDataFileService.getTotalSizeByProject(project.getProjectId());
  }

  private long countTriples(Project project) {
    ResultSet resultSet = this.triplestoreResourceService.executeQuery(project.getShortName(),
            SparqlQueryTool.queryToCountTotal(SparqlBuilderTool.QUERY_VARIABLE));
    if (resultSet.hasNext()) {
      QuerySolution solution = resultSet.nextSolution();
      return solution.get(SparqlBuilderTool.QUERY_VARIABLE).asLiteral().getLong();
    }
    return 0L;
  }

}
