/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - RmlMappingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rml;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import jakarta.xml.bind.JAXBException;
import software.amazon.awssdk.utils.StringInputStream;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.settings.Project;
import ch.hedera.service.HederaService;
import ch.hedera.service.IdMappingService;
import ch.hedera.service.MetadataService;
import ch.hedera.service.rest.trusted.TrustedRmlRemoteResourceService;
import ch.hedera.util.RdfTool;
import ch.hedera.util.RmlTool;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlQueryTool;

public abstract class RmlMappingService extends HederaService {

  private static final String NO_RML = "no-rml";

  private static final String RML_LOCATION = "rml";
  private static final String RESEARCH_DATA_FILE_RML_FILE = "research-data-file-by-project-xml.ttl";
  private static final String RESEARCH_DATA_FILE_FILE = "research-data-file.xml";

  private final String ldBaseURI;
  private final String rdfDatasetLocation;
  private final String researchDataFileLocation;
  private final String rmlLocation;

  private final DateTimeFormatter fileFormatForFile = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");

  private final TrustedRmlRemoteResourceService trustedRmlRemoteResourceService;

  private final MetadataService metadataService;

  private final IdMappingService idMappingService;

  protected RmlMappingService(
          MessageService messageService,
          HederaProperties config,
          TrustedRmlRemoteResourceService trustedRmlRemoteResourceService,
          MetadataService metadataService,
          IdMappingService idMappingService) {
    super(messageService);
    this.ldBaseURI = config.getModule().getLinkedData().getPublicUrl() + "/";
    this.rdfDatasetLocation = config.getRdfDatasetLocation();
    this.researchDataFileLocation = config.getResearchDataFileLocation();
    this.rmlLocation = config.getRmlLocation();
    this.trustedRmlRemoteResourceService = trustedRmlRemoteResourceService;
    this.metadataService = metadataService;
    this.idMappingService = idMappingService;
  }

  public abstract Path applyRml(Path sourcePath, Path rmlFilePath, Map<String, String> parametersMap, RdfFormat rdfFormat);

  public ResearchDataFile applyRml(Project project, ResearchDataFile researchDataFile, RdfFormat rdfFormat) throws IOException, JAXBException {
    final URI projetUri = this.getLdProjectBaseUri(project);
    // Get RML file
    final Path rmlFilePath = this.getRmlForResearchDataFile(researchDataFile);
    // Get XML from object
    final Path sourcePath = this.getForResearchDataFile(researchDataFile);
    // Appl RML
    final Map<String, String> parametersMap = RmlTool.getPlaceHolderValues(projetUri, sourcePath,
            this.getResearchDataFileAccessUrl(researchDataFile));
    final Path resultPath = this.applyRml(sourcePath, rmlFilePath, parametersMap, rdfFormat);
    // Assign hedera Ids
    this.idMappingService.assignId(projetUri, resultPath, rdfFormat);
    // Save RDF information
    researchDataFile.setUri(URI.create(this.getObjectUri(resultPath)));
    researchDataFile.setRdfMetadata(FileTool.toString(resultPath));
    // Delete files
    FileTool.deleteFile(rmlFilePath);
    FileTool.deleteFile(sourcePath);
    FileTool.deleteFile(resultPath);
    return researchDataFile;
  }

  private String getResearchDataFileAccessUrl(ResearchDataFile researchDataFile) {
    return (String) researchDataFile.getMetadata().get(HederaConstants.FILE_WEB_URL);
  }

  public RdfDatasetFile transformRdf(Project project, SourceDatasetFile sourceDatasetFile, RdfFormat rdfFormat) throws IOException {
    // Check format
    if (RdfFormat.N_TRIPLES != rdfFormat) {
      throw new SolidifyCheckingException("Wrong RDF format: " + rdfFormat);
    }
    final URI projetUri = this.getLdProjectBaseUri(project);
    final Path effectiveSourcePath = sourceDatasetFile.getEffectiveSourcePath();
    final Path resultFile = effectiveSourcePath.getParent().resolve(this.getResultFilename(sourceDatasetFile.getFileName(), rdfFormat));
    // Copy RDF file
    FileTool.copyFile(effectiveSourcePath, resultFile);
    // Generate RdfDatasetFile
    final RdfDatasetFile rdfDatasetFile = this.createRdfDatasetFile(sourceDatasetFile, NO_RML, rdfFormat, resultFile);
    // Assign IDs
    this.idMappingService.assignId(projetUri, Paths.get(rdfDatasetFile.getFilePath()), rdfDatasetFile.getRdfFormat());
    return rdfDatasetFile;
  }

  public RdfDatasetFile applyRml(Project project, SourceDatasetFile sourceDatasetFile, String rmlFileId, RdfFormat rdfFormat)
          throws IOException {
    final URI projetUri = this.getLdProjectBaseUri(project);
    final Path rmlFilePath = this.getRmlFile(rmlFileId);
    final Map<String, String> parametersMap = RmlTool.getPlaceHolderValues(projetUri, sourceDatasetFile.getEffectiveSourcePath(), "");
    final Path resultFile = this.applyRml(sourceDatasetFile.getEffectiveSourcePath(), rmlFilePath, parametersMap, rdfFormat);
    final RdfDatasetFile rdfDatasetFile = this.createRdfDatasetFile(sourceDatasetFile, rmlFileId, rdfFormat, resultFile);
    // Assign IDs
    this.idMappingService.assignId(projetUri, Paths.get(rdfDatasetFile.getFilePath()), rdfDatasetFile.getRdfFormat());
    return rdfDatasetFile;
  }

  protected RdfDatasetFile createRdfDatasetFile(SourceDatasetFile sourceDatasetFile, String rmlFileId, RdfFormat rdfFormat, Path resultPath)
          throws IOException {
    final RdfDatasetFile rdfDatasetFile = new RdfDatasetFile();
    rdfDatasetFile.setRmlId(rmlFileId);
    rdfDatasetFile.setSourceDatasetFile(sourceDatasetFile);
    rdfDatasetFile.setRdfFormat(rdfFormat);
    rdfDatasetFile.setFileName(resultPath.getFileName().toString());
    Path filePath = Path.of(this.rdfDatasetLocation, rdfDatasetFile.getFileName());
    FileTool.moveFile(resultPath, filePath);
    rdfDatasetFile.setFilePath(filePath.toUri());
    rdfDatasetFile.setFileSize(FileTool.getSize(filePath));
    return rdfDatasetFile;
  }

  protected Path getRmlFile(String rmlId) {
    return this.trustedRmlRemoteResourceService.downloadRmlFile(rmlId);
  }

  protected String getResultFilename(String sourceFilename, RdfFormat rdfFormat) {
    return sourceFilename.replace(".", "_") + "_" + OffsetDateTime.now().format(this.fileFormatForFile) + "."
            + RdfTool.getExtension(rdfFormat);
  }

  protected Path getResultPath(String sourceFilename, RdfFormat rdfFormat) {
    return Path.of(this.rmlLocation, this.getResultFilename(sourceFilename, rdfFormat));
  }

  protected Path getRmlForResearchDataFile(ResearchDataFile researchDataFile) {
    final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
    try {
      final org.springframework.core.io.Resource[] rml = resolver
              .getResources("classpath*:/" + RML_LOCATION + "/" + RESEARCH_DATA_FILE_RML_FILE);
      if (rml.length > 0) {
        final Path rmlFile = Path.of(this.researchDataFileLocation, researchDataFile.getResId(), RESEARCH_DATA_FILE_RML_FILE);
        if (FileTool.copyFile(rml[0].getInputStream(), rmlFile)) {
          return rmlFile;
        }
      }
    } catch (final IOException e) {
      throw new SolidifyRuntimeException("RML", e);
    }
    throw new SolidifyCheckingException("Cannot find: " + RML_LOCATION + "/" + RESEARCH_DATA_FILE_RML_FILE);
  }

  private Path getForResearchDataFile(ResearchDataFile researchDataFile) throws IOException, JAXBException {
    final Path objectFile = Path.of(this.researchDataFileLocation, researchDataFile.getResId(), RESEARCH_DATA_FILE_FILE);
    FileTool.copyFile(new StringInputStream(this.metadataService.getMetadataInXml(researchDataFile)), objectFile);
    return objectFile;
  }

  protected URI getLdProjectBaseUri(Project project) {
    return URI.create(this.ldBaseURI + project.getShortName());
  }

  private String getObjectUri(Path rdfPath) {
    final Model model = RdfTool.getRdfModel(rdfPath);
    final Query listQuery = SparqlQueryTool.queryToListSubjects(SparqlBuilderTool.SUBJECT_VARIABLE);

    try (QueryExecution localExecution = QueryExecutionFactory.create(listQuery, model)) {
      ResultSet resultSet = localExecution.execSelect();
      // TODO Check that there is one subject
      // For each subject
      while (resultSet.hasNext()) {
        QuerySolution solution = resultSet.nextSolution();
        return solution.getResource(SparqlBuilderTool.SUBJECT_VARIABLE).toString();
      }
    }
    return null;
  }

}
