/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - EmptyRmlMappingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rml;

import java.nio.file.Path;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.MessageService;

import ch.hedera.config.HederaProperties;
import ch.hedera.controller.IngestController;
import ch.hedera.model.RdfFormat;
import ch.hedera.service.IdMappingService;
import ch.hedera.service.MetadataService;
import ch.hedera.service.rest.trusted.TrustedRmlRemoteResourceService;

@Profile("rml-empty")
@Service
@ConditionalOnBean(IngestController.class)
public class EmptyRmlMappingService extends RmlMappingService {

  public EmptyRmlMappingService(MessageService messageService, HederaProperties hederaProperties,
          TrustedRmlRemoteResourceService trustedRmlRemoteResourceService, MetadataService metadataService, IdMappingService idMappingService) {
    super(messageService, hederaProperties, trustedRmlRemoteResourceService, metadataService, idMappingService);
  }

  @Override
  public Path applyRml(Path sourcePath, Path rmlFilePath, Map<String, String> parametersMap, RdfFormat rdfFormat) {
    throw new UnsupportedOperationException();
  }

}
