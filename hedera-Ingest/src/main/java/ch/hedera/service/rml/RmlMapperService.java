/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - RmlMapperService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rml;

import java.nio.file.Path;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;

import ch.hedera.config.HederaProperties;
import ch.hedera.controller.IngestController;
import ch.hedera.model.RdfFormat;
import ch.hedera.service.IdMappingService;
import ch.hedera.service.MetadataService;
import ch.hedera.service.rest.trusted.TrustedRmlRemoteResourceService;

@Profile("rml-rmlmapper")
@Service
@ConditionalOnBean(IngestController.class)
public class RmlMapperService extends RmlMappingService {

  // private static final String DEFAULT_STORE = "rmlmapper://default.store";

  public RmlMapperService(MessageService messageService,
          HederaProperties hederaProperties,
          TrustedRmlRemoteResourceService trustedRmlRemoteResourceService,
          MetadataService metadataService, IdMappingService idMappingService) {
    super(messageService, hederaProperties, trustedRmlRemoteResourceService, metadataService, idMappingService);
  }

  @Override
  public Path applyRml(Path sourcePath, Path rmlFilePath, Map<String, String> parmetersMap, RdfFormat rdfFormat) {
    // this.checkRdfFormat(rdfFormat);
    // this.replaceParametersInRml(rmlFilePath, this.getRmlParameters(projectUri, sourcePath));
    // final QuadStore rmlStore = new RDF4JStore();
    // final QuadStore resultingStore = new RDF4JStore();
    // final RecordsFactory factory = new RecordsFactory("");
    // try (InputStream rmlInputStream = new ByteArrayInputStream(replacedRmlString.getBytes())) {
    // rmlStore.read(rmlInputStream, null, RDFFormat.TURTLE);
    // final Executor executor = new Executor(rmlStore, factory, resultingStore, null, null);
    // Map<Term, QuadStore> result = executor.execute(null);
    // final NamedNode key = new NamedNode(DEFAULT_STORE);
    // final QuadStore quadStore = result.get(key);
    //
    // // Save the result as a RdfDatasetFile
    // final Path resultPath = this.getResultPath(sourcePath.getFileName().toString(), rdfFormat);
    // try (final FileWriter fileWriter = new FileWriter(resultPath.toFile())) {
    // quadStore.write(fileWriter, rdfFormat);
    // }
    // return resultPath;
    // } catch (Exception e) {
    // throw new SolidifyProcessingException("[RML Mapper] Cannot apply RML file: " + e.getMessage(), e);
    // }
    throw new SolidifyRuntimeException("RML Mapper not supported");
  }
}
