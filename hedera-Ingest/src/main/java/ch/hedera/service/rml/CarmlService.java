/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - CarmlService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rml;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import io.carml.engine.rdf.RdfRmlMapper;
import io.carml.logicalsourceresolver.CsvResolver;
import io.carml.logicalsourceresolver.JsonPathResolver;
import io.carml.logicalsourceresolver.XPathResolver;
import io.carml.model.TriplesMap;
import io.carml.util.RmlMappingLoader;
import io.carml.vocab.Rdf;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.validation.ValidationError;

import ch.hedera.config.HederaProperties;
import ch.hedera.controller.IngestController;
import ch.hedera.model.RdfFormat;
import ch.hedera.service.IdMappingService;
import ch.hedera.service.MetadataService;
import ch.hedera.service.rest.trusted.TrustedRmlRemoteResourceService;
import ch.hedera.util.RmlTool;

@Profile("rml-carml")
@Service
@ConditionalOnBean(IngestController.class)
public class CarmlService extends RmlMappingService {
  private static final Logger logger = LoggerFactory.getLogger(CarmlService.class);

  public CarmlService(MessageService messageService, HederaProperties hederaProperties,
          TrustedRmlRemoteResourceService trustedRmlRemoteResourceService, MetadataService metadataService, IdMappingService idMappingService) {
    super(messageService, hederaProperties, trustedRmlRemoteResourceService, metadataService, idMappingService);
  }

  @Override
  public Path applyRml(Path sourcePath, Path rmlFilePath, Map<String, String> parametersMap, RdfFormat rdfFormat) {
    final String replacedRmlString = RmlTool.replacePlaceHoldersInFile(rmlFilePath, parametersMap);
    final InputStream replacedRmlInputStream = new ByteArrayInputStream(replacedRmlString.getBytes());
    final Set<TriplesMap> rmlMapping = RmlMappingLoader.build().load(RDFFormat.TURTLE, replacedRmlInputStream);

    final RdfRmlMapper mapper = RdfRmlMapper.builder()
            .triplesMaps(rmlMapping)
            .setLogicalSourceResolver(Rdf.Ql.XPath, XPathResolver::getInstance)
            .setLogicalSourceResolver(Rdf.Ql.Csv, CsvResolver::getInstance)
            .setLogicalSourceResolver(Rdf.Ql.JsonPath, JsonPathResolver::getInstance)
            .fileResolver(sourcePath)
            .build();
    final Model model = mapper.mapToModel();

    // Save the result
    final Path resultPath = this.getResultPath(sourcePath.getFileName().toString(), rdfFormat);
    try (final FileOutputStream output = new FileOutputStream(resultPath.toFile())) {
      final RDFWriter writer = Rio.createWriter(this.getRioRdfFormat(rdfFormat), output);
      Rio.write(model, writer);
    } catch (IOException e) {
      throw new SolidifyProcessingException("[CARML] Cannot apply RML file: " + e.getMessage(), e);
    }
    this.replacePercentEncodedSlash(resultPath);
    return resultPath;
  }

  public void replacePercentEncodedSlash(Path filePath) {
    try {
      List<String> fileContent = Files.readAllLines(filePath).stream()
              .map(line -> line.replace("%2F", "/").replace("%23", "#"))
              .toList();
      Files.write(filePath, fileContent, StandardCharsets.UTF_8);
    } catch (IOException e) {
      logger.error("An error occurred while replacing percent encoded slash in the file " + filePath, e);
    }
  }

  private org.eclipse.rdf4j.rio.RDFFormat getRioRdfFormat(RdfFormat rdfFormat) {
    return switch (rdfFormat) {
      case JSON_LD -> RDFFormat.JSONLD;
      case N3 -> RDFFormat.N3;
      case N_QUADS -> RDFFormat.NQUADS;
      case N_TRIPLES -> RDFFormat.NTRIPLES;
      case RDF_JSON -> RDFFormat.RDFJSON;
      case RDF_XML -> RDFFormat.RDFXML;
      case TRIG -> RDFFormat.TRIG;
      case TRIX -> RDFFormat.TRIX;
      case TURTLE -> RDFFormat.TURTLE;
      default -> throw new SolidifyValidationException(new ValidationError("Unsupported RDF format: " + rdfFormat.toString()));
    };
  }

}
