/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - SourceDatasetFilePermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.security;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.hedera.business.SourceDatasetFileService;
import ch.hedera.controller.IngestController;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.service.rest.propagate.PropagateProjectRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedPersonRemoteResourceService;

@Service
@ConditionalOnBean(IngestController.class)
public class SourceDatasetFilePermissionService extends ProjectDataPermissionService<SourceDatasetFile> {

  private final SourceDatasetFileService sourceDatasetFileService;

  public SourceDatasetFilePermissionService(TrustedPersonRemoteResourceService personRemoteResourceService,
          SourceDatasetFileService sourceDatasetFileService, PropagateProjectRemoteResourceService projectService) {
    super(personRemoteResourceService, projectService);
    this.sourceDatasetFileService = sourceDatasetFileService;
  }

  public boolean isAllowedToListWithProjectId(SourceDatasetFile sourceDatasetFile, String actionFromController, String projectId) {
    final SourceDataset sourceDataset = new SourceDataset(projectId);
    sourceDatasetFile.setDataset(sourceDataset);
    return super.isAllowedToListWithProjectId(sourceDatasetFile, actionFromController);
  }

  @Override
  SourceDatasetFile getExistingResource(String resId) {
    return this.sourceDatasetFileService.findOne(resId);
  }
}
