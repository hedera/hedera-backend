/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - RdfDatasetFilePermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.security;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;

import ch.hedera.business.RdfDatasetFileService;
import ch.hedera.controller.HederaControllerAction;
import ch.hedera.controller.IngestController;
import ch.hedera.model.ProjectAwareResource;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.security.Role;
import ch.hedera.service.rest.propagate.PropagateProjectRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedPersonRemoteResourceService;

@Service
@ConditionalOnBean(IngestController.class)
public class RdfDatasetFilePermissionService extends ProjectDataPermissionService<RdfDatasetFile> {

  private static final Logger logger = LoggerFactory.getLogger(RdfDatasetFilePermissionService.class);

  private final RdfDatasetFileService rdfDatasetFileService;

  protected RdfDatasetFilePermissionService(TrustedPersonRemoteResourceService trustedPersonRemoteResourceService,
          RdfDatasetFileService rdfDatasetFileService, PropagateProjectRemoteResourceService projectService) {
    super(trustedPersonRemoteResourceService, projectService);
    this.rdfDatasetFileService = rdfDatasetFileService;
  }

  @Override
  protected String getProjectIdToCheckPermission(ProjectAwareResource existingResource) {
    if (existingResource != null) {
      RdfDatasetFile existingRdfFile = this.rdfDatasetFileService.findOne(existingResource.getResId());
      return existingRdfFile.getProjectId();
    } else {
      throw new SolidifyRuntimeException("error while checking user permissions on project: no given resource to check");
    }
  }

  @Override
  RdfDatasetFile getExistingResource(String resId) {
    return this.rdfDatasetFileService.findOne(resId);
  }

  public boolean isAllowedToListFiles(String projectId, String actionString) {

    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }

    String personId = this.getPersonId();
    HederaControllerAction action = this.getControllerAction(actionString);

    if (StringTool.isNullOrEmpty(projectId)) {
      logger.error("Unable to check permissions on {}: projectId is not set", new SolidifyRuntimeException("Unable to check permissions"));
      return false;
    }
    if (StringTool.isNullOrEmpty(personId)) {
      logger.warn("Unable to check permissions on {} ({}): personId is not set");
      return false;
    }

    final Optional<Role> roleOpt = this.trustedPersonRemoteResourceService.findProjectRole(personId, projectId);

    if (roleOpt.isEmpty()) {
      logger.warn("The person '{}' does not have any role on project '{}'", personId, projectId);
      return false;
    }

    if (action == HederaControllerAction.LIST || action == HederaControllerAction.LIST_FILES) {
      logger.trace("The person '{}' with role '{}' is allowed to perform the action '{}' for project '{}'", personId,
              roleOpt.get().getName(), action, projectId);
      return true;
    }
    return false;
  }
}
