/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - ProjectIdMappingJmsListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.MessageService;

import ch.hedera.controller.IngestController;
import ch.hedera.message.IdMappingMessage;
import ch.hedera.model.ingest.IdMapping;

@Service
@ConditionalOnBean(IngestController.class)
public class ProjectIdMappingJmsListener extends HederaService {

  private static final Logger log = LoggerFactory.getLogger(ProjectIdMappingJmsListener.class);

  private final IdMappingService idMappingService;

  public ProjectIdMappingJmsListener(MessageService messageService, IdMappingService idMappingService) {
    super(messageService);
    this.idMappingService = idMappingService;
  }

  @JmsListener(destination = "${hedera.queue.idMapping}")
  public void processProjectData(IdMappingMessage idMappingMessage) {
    log.info("Reading message {}", idMappingMessage);

    List<IdMapping> idMappingList = this.idMappingService.findByProjectShortName(idMappingMessage.getResId());
    for (IdMapping id : idMappingList) {
      this.idMappingService.delete(id.getHederaId());
    }
  }
}
