/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - RmlProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rdfdataset;

import java.io.IOException;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.CSVTool;
import ch.unige.solidify.util.JSONTool;
import ch.unige.solidify.util.XMLTool;

import ch.hedera.business.RdfDatasetFileService;
import ch.hedera.business.SourceDatasetFileService;
import ch.hedera.controller.IngestController;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.StatusHistory;
import ch.hedera.model.ingest.DatasetFileType;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.ingest.SourceDatasetFile.SourceDatasetFileStatus;
import ch.hedera.model.settings.Project;
import ch.hedera.service.HederaService;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedRmlRemoteResourceService;
import ch.hedera.service.rml.RmlMappingService;
import ch.hedera.util.RdfTool;

@Service
@ConditionalOnBean(IngestController.class)
public class RmlProcessingService extends HederaService {
  private static final Logger logger = LoggerFactory.getLogger(RmlProcessingService.class);

  private final TrustedProjectRemoteResourceService projectRemoteService;

  private final TrustedRmlRemoteResourceService rmlRemoteService;

  private final RmlMappingService rmlMappingService;

  private final SourceDatasetFileService sourceDatasetFileService;

  private final RdfDatasetFileService rdfDatasetFileService;

  public RmlProcessingService(
          MessageService messageService,
          TrustedProjectRemoteResourceService projectRemoteService,
          TrustedRmlRemoteResourceService rmlRemoteService,
          RmlMappingService rmlMappingService,
          SourceDatasetFileService sourceDatasetFileService,
          RdfDatasetFileService rdfDatasetFileService) {
    super(messageService);
    this.projectRemoteService = projectRemoteService;
    this.rmlRemoteService = rmlRemoteService;
    this.rmlMappingService = rmlMappingService;
    this.sourceDatasetFileService = sourceDatasetFileService;
    this.rdfDatasetFileService = rdfDatasetFileService;
  }

  @Transactional
  public SourceDatasetFile processSourceDatasetFile(String sourceDatasetFileId, String rmlFileId, RdfFormat rdfFormat) {
    // Get Source dataset file
    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetFileService.findOne(sourceDatasetFileId);
    final boolean checkOnly = (rmlFileId == null && rdfFormat == null);
    try {
      final Project project = this.projectRemoteService.findOne(sourceDatasetFile.getDataset().getProjectId());
      if (!project.isOpen()) {
        throw new SolidifyCheckingException(this.messageService.get("ingest.project.closed"));
      }
      switch (sourceDatasetFile.getStatus()) {
        case TO_CHECK, TO_TRANSFORM -> this.processToCheckSourceDataFile(sourceDatasetFile);
        case CHECKING -> this.processCheckingSourceDataFile(sourceDatasetFile, checkOnly);
        case TRANSFORMING -> this.processTransformingDataFile(sourceDatasetFile, project, rmlFileId, rdfFormat);
        default -> throw new SolidifyRuntimeException(
                this.messageService.get("ingest.error.status", new Object[] { sourceDatasetFile.getStatus() }));
      }

    } catch (final SolidifyCheckingException e) {
      logger.error("SolidifyCheckingException when processing source dataset file", e);
      sourceDatasetFile
              .setErrorStatusWithMessage(this.messageService.get("ingest.error.check", new Object[] { e.getMessage() }));
    } catch (final SolidifyProcessingException e) {
      logger.error("SolidifyProcessingException when processing source dataset file", e);
      sourceDatasetFile
              .setErrorStatusWithMessage(this.messageService.get("ingest.error.process", new Object[] { e.getMessage() }));
    } catch (final Exception e) {
      logger.error("Exception when processing source dataset file", e);
      sourceDatasetFile.setErrorStatusWithMessage(this.messageService.get("ingest.error", new Object[] { e.getMessage() }));
    }

    return this.sourceDatasetFileService.save(sourceDatasetFile);
  }

  private void processToCheckSourceDataFile(SourceDatasetFile sourceDatasetFile) {
    // Start process
    sourceDatasetFile.setStatus(SourceDatasetFileStatus.CHECKING);
  }

  private void processCheckingSourceDataFile(SourceDatasetFile sourceDatasetFile, boolean checkOnly) throws IOException {
    // Check file
    Path effectiveSourcePath = sourceDatasetFile.getEffectiveSourcePath();
    switch (sourceDatasetFile.getType()) {
      case CSV -> CSVTool.wellformed(effectiveSourcePath);
      case JSON -> JSONTool.wellformed(effectiveSourcePath);
      case RDF -> RdfTool.wellformed(effectiveSourcePath, RdfFormat.N_TRIPLES);
      case XML -> XMLTool.wellformed(effectiveSourcePath);
      default -> throw new SolidifyCheckingException("wrong format: " + sourceDatasetFile.getType().name());
    }
    // Update status
    if (checkOnly) {
      sourceDatasetFile.setStatus(SourceDatasetFileStatus.CHECKED);
    } else {
      sourceDatasetFile.setStatus(SourceDatasetFileStatus.TRANSFORMING);
    }
  }

  private void processTransformingDataFile(SourceDatasetFile sourceDatasetFile, Project project, String rmlFileId, RdfFormat rdfFormat)
          throws IOException {
    RdfDatasetFile rdfDatasetFile;
    if (sourceDatasetFile.getType().equals(DatasetFileType.RDF)) {
      rdfDatasetFile = this.rmlMappingService.transformRdf(project, sourceDatasetFile, rdfFormat);
    } else {
      // Check RML with project
      this.checkRml(project, rmlFileId);
      // Add history event
      this.addHistoryEvent(sourceDatasetFile, rmlFileId);
      // Apply RML
      rdfDatasetFile = this.rmlMappingService.applyRml(
              project,
              sourceDatasetFile,
              rmlFileId,
              rdfFormat);

    }
    // Save result
    this.rdfDatasetFileService.save(rdfDatasetFile);
    // Update source dataset file
    sourceDatasetFile.setStatus(SourceDatasetFileStatus.TRANSFORMED);
  }

  private void checkRml(Project project, String rmlFileId) {
    this.rmlRemoteService.findOne(rmlFileId);
    this.projectRemoteService.getRmlFile(project.getProjectId(), rmlFileId);
  }

  private void addHistoryEvent(SourceDatasetFile sourceDatasetFile, String rmlFileId) {
    final StatusHistory stsHistory = new StatusHistory(sourceDatasetFile.getClass().getSimpleName(), sourceDatasetFile.getResId(),
            "RML Conversion");
    stsHistory.setDescription(this.messageService.get("ingest.rml.history", new Object[] { rmlFileId }));
    SolidifyEventPublisher.getPublisher().publishEvent(stsHistory);
  }

}
