/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - RdfStorageJmsListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rdfdataset;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.hedera.business.RdfDatasetFileService;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.IngestController;
import ch.hedera.message.RdfStorageMessage;
import ch.hedera.model.RdfOperation;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.settings.Project;
import ch.hedera.model.triplestore.TriplestoreDataset;
import ch.hedera.service.MessageProcessorBySize;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedTriplestoreDatasetRemoteResourceService;

@Service
@ConditionalOnBean(IngestController.class)
public class RdfStorageJmsListener extends MessageProcessorBySize<RdfStorageMessage> {

  private static final Logger log = LoggerFactory.getLogger(RdfStorageJmsListener.class);

  private final TrustedProjectRemoteResourceService projectRemoteResourceService;

  private final TrustedTriplestoreDatasetRemoteResourceService triplestoreDatasetRemoteService;

  private final RdfDatasetFileService rdfDatasetFileService;

  private final RdfStorageProcessingService rdfStorageProcessingService;

  protected RdfStorageJmsListener(
          HederaProperties configService,
          RdfDatasetFileService rdfDatasetFileService,
          TrustedProjectRemoteResourceService projectRemoteResourceService,
          TrustedTriplestoreDatasetRemoteResourceService triplestoreDatasetRemoteService,
          RdfStorageProcessingService rdfStorageProcessingService) {
    super(configService);
    this.rdfDatasetFileService = rdfDatasetFileService;
    this.projectRemoteResourceService = projectRemoteResourceService;
    this.triplestoreDatasetRemoteService = triplestoreDatasetRemoteService;
    this.rdfStorageProcessingService = rdfStorageProcessingService;
  }

  @JmsListener(destination = "${hedera.queue.rdfStorage}")
  @Override
  public void receiveMessage(RdfStorageMessage rdfStorageMessage) {
    this.sendForParallelProcessing(rdfStorageMessage);
  }

  @Override
  public void processMessage(RdfStorageMessage rdfStorageMessage) {
    log.info("Reading message {}", rdfStorageMessage);
    if (rdfStorageMessage.getOperation() == RdfOperation.PURGE) {
      // Purge triplestore by project
      this.processProjectTriplestoreDataset(rdfStorageMessage);
    } else {
      // Process RDF dataset file
      this.processRdfDatasetFile(rdfStorageMessage);
    }
  }

  private void processProjectTriplestoreDataset(RdfStorageMessage rdfStorageMessage) {
    log.info("Project '{}' (triplestore: {}) started", rdfStorageMessage.getResId(), rdfStorageMessage.getTriplestoreDatasetId());
    try {
      Project project = this.projectRemoteResourceService.findOne(rdfStorageMessage.getResId());
      final TriplestoreDataset triplestoreDataset = this.triplestoreDatasetRemoteService.findOne(rdfStorageMessage.getTriplestoreDatasetId());
      this.rdfStorageProcessingService.purgeProjectTriplestore(project, triplestoreDataset);
      log.info("Project '{}' (triplestore: {}) processed", rdfStorageMessage.getResId(), rdfStorageMessage.getTriplestoreDatasetId());
    } catch (NoSuchElementException e) {
      log.error("Cannot find Project ({})", rdfStorageMessage.getResId());
    }
  }

  private void processRdfDatasetFile(RdfStorageMessage rdfStorageMessage) {
    log.info("RDF dataset file '{}' (triplestore: {}) started", rdfStorageMessage.getResId(), rdfStorageMessage.getTriplestoreDatasetId());
    try {
      RdfDatasetFile rdfDatasetFile = this.rdfDatasetFileService.findOne(rdfStorageMessage.getResId());
      final TriplestoreDataset triplestoreDataset = this.triplestoreDatasetRemoteService.findOne(rdfStorageMessage.getTriplestoreDatasetId());
      while (rdfDatasetFile.isInProgress()) {
        // Process RDF dataset file
        rdfDatasetFile = this.rdfStorageProcessingService.processRdfDatasetFile(rdfStorageMessage.getResId(), triplestoreDataset);
      }
      log.info("RDF dataset file '{}' (triplestore: {}) processed", rdfStorageMessage.getResId(), rdfStorageMessage.getTriplestoreDatasetId());
    } catch (NoSuchElementException e) {
      log.error("Cannot find RDF dataset file ({})", rdfStorageMessage.getResId());
    }
  }

}
