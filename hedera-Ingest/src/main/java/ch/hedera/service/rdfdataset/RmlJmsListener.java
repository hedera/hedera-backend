/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - RmlJmsListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rdfdataset;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.hedera.business.SourceDatasetFileService;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.IngestController;
import ch.hedera.message.RmlConversionMessage;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.service.MessageProcessorBySize;

@Service
@ConditionalOnBean(IngestController.class)
public class RmlJmsListener extends MessageProcessorBySize<RmlConversionMessage> {

  private static final Logger log = LoggerFactory.getLogger(RmlJmsListener.class);

  private final SourceDatasetFileService sourceDatasetFileService;
  private final RmlProcessingService rmlProcessingService;

  protected RmlJmsListener(HederaProperties configService, SourceDatasetFileService sourceDatasetFileService,
          RmlProcessingService rmlProcessingService) {
    super(configService);
    this.sourceDatasetFileService = sourceDatasetFileService;
    this.rmlProcessingService = rmlProcessingService;
  }

  @JmsListener(destination = "${hedera.queue.rmlConversion}")
  @Override
  public void receiveMessage(RmlConversionMessage rmlConversionMessage) {
    this.sendForParallelProcessing(rmlConversionMessage);
  }

  @Override
  public void processMessage(RmlConversionMessage rmlConversionMessage) {
    log.info("Reading message {}", rmlConversionMessage);
    try {
      log.info("Source dataset file '{}': RML application '{}' in '{}' format started", rmlConversionMessage.getSourceDatasetFileId(),
              rmlConversionMessage.getRmlFileId(), rmlConversionMessage.getRdfFormat());
      SourceDatasetFile sourceDatasetFile = this.sourceDatasetFileService.findOne(rmlConversionMessage.getResId());
      while (sourceDatasetFile.isInProgress()) {
        // Process Source dataset file
        sourceDatasetFile = this.rmlProcessingService.processSourceDatasetFile(rmlConversionMessage.getSourceDatasetFileId(),
                rmlConversionMessage.getRmlFileId(), rmlConversionMessage.getRdfFormat());
      }
      log.info("Source dataset file '{}': RML application '{}' in '{}' format completed", rmlConversionMessage.getSourceDatasetFileId(),
              rmlConversionMessage.getRmlFileId(), rmlConversionMessage.getRdfFormat());
    } catch (NoSuchElementException e) {
      log.error("Cannot find source dataset file ({})", rmlConversionMessage.getResId());
    }
  }

}
