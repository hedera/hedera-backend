/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - RdfStorageProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rdfdataset;

import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.xml.bind.JAXBException;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.MessageService;

import ch.hedera.business.RdfDatasetFileService;
import ch.hedera.business.SourceDatasetFileService;
import ch.hedera.business.SourceDatasetService;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.IngestController;
import ch.hedera.message.ResearchDataFileReloadMessage;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.RdfDatasetFile.RdfDatasetFileStatus;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.model.triplestore.TriplestoreDataset;
import ch.hedera.model.xml.hedera.v1.researchObject.ObjectType;
import ch.hedera.rest.ResourceName;
import ch.hedera.service.HederaIngestService;
import ch.hedera.service.MetadataService;
import ch.hedera.service.rest.fallback.FallbackProjectRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedResearchObjectMetadataRemoteResourceService;
import ch.hedera.service.triplestore.TriplestoreResourceService;
import ch.hedera.specification.RdfDatasetFileSpecification;
import ch.hedera.specification.SourceDatasetFileSpecification;
import ch.hedera.specification.SourceDatasetSpecification;
import ch.hedera.util.RdfTool;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlQueryTool;

@Service
@ConditionalOnBean(IngestController.class)
public class RdfStorageProcessingService extends HederaIngestService {

  private static final Logger log = LoggerFactory.getLogger(RdfStorageProcessingService.class);

  private final SourceDatasetService sourceDatasetService;

  private final SourceDatasetFileService sourceDatasetFileService;

  private final RdfDatasetFileService rdfDatasetFileService;

  private final FallbackProjectRemoteResourceService projectService;

  private final TriplestoreResourceService triplestoreResourceService;

  private final TrustedResearchObjectMetadataRemoteResourceService indexingService;

  private final MetadataService metadataService;

  public RdfStorageProcessingService(
          MessageService messageService,
          HederaProperties config,
          SourceDatasetService sourceDatasetService,
          SourceDatasetFileService sourceDatasetFileService,
          RdfDatasetFileService rdfDatasetFileService,
          FallbackProjectRemoteResourceService projectService,
          TriplestoreResourceService triplestoreResourceService,
          TrustedResearchObjectMetadataRemoteResourceService indexingService,
          MetadataService metadataService) {
    super(messageService, config);
    this.sourceDatasetService = sourceDatasetService;
    this.sourceDatasetFileService = sourceDatasetFileService;
    this.rdfDatasetFileService = rdfDatasetFileService;
    this.projectService = projectService;
    this.triplestoreResourceService = triplestoreResourceService;
    this.indexingService = indexingService;
    this.metadataService = metadataService;
  }

  @Transactional
  public RdfDatasetFile processRdfDatasetFile(String rdfDatasetFileId, TriplestoreDataset triplestoreDataset) {
    // Get RDF dataset file
    final RdfDatasetFile rdfDatasetFile = this.rdfDatasetFileService.findOne(rdfDatasetFileId);
    try {
      final Project project = this.projectService.findOne(rdfDatasetFile.getSourceDatasetFile().getDataset().getProjectId());
      if (!project.isOpen()) {
        throw new SolidifyCheckingException(this.messageService.get("ingest.project.closed"));
      }
      switch (rdfDatasetFile.getStatusImport()) {
        case TO_IMPORT, TO_REIMPORT -> this.processStartImportRdfDatasetFile(rdfDatasetFile);
        case IMPORTING -> this.processImportingRdfDatasetFile(rdfDatasetFile, triplestoreDataset);
        case INDEXING -> this.processIndexingRdfDatasetFile(rdfDatasetFile, project);
        case TO_REPLACE -> this.processStartReplaceRdfDatasetFile(rdfDatasetFile);
        case REPLACING -> this.processReplacingRdfDatasetFile(rdfDatasetFile, triplestoreDataset);
        case TO_REMOVE -> this.processStartRemoveRdfDatasetFile(rdfDatasetFile);
        case REMOVING -> this.processRemovingRdfDatasetFile(rdfDatasetFile, triplestoreDataset, project.isAccessPublic());
        default -> throw new SolidifyRuntimeException(
                this.messageService.get("ingest.error.status", new Object[] { rdfDatasetFile.getStatusImport() }));
      }

    } catch (final SolidifyCheckingException e) {
      log.error("Error in checking RDF: " + e.getMessage(), e);
      rdfDatasetFile
              .setErrorStatusWithMessage(this.messageService.get("ingest.error.check", new Object[] { e.getMessage() }));
    } catch (final SolidifyProcessingException e) {
      log.error("Error in processing RDF: " + e.getMessage(), e);
      rdfDatasetFile
              .setErrorStatusWithMessage(this.messageService.get("ingest.error.process", new Object[] { e.getMessage() }));
    } catch (final Exception e) {
      log.error("Error in RDF storage: " + e.getMessage(), e);
      rdfDatasetFile.setErrorStatusWithMessage(this.messageService.get("ingest.error", new Object[] { e.getMessage() }));
    }

    return this.rdfDatasetFileService.save(rdfDatasetFile);
  }

  private void processStartImportRdfDatasetFile(RdfDatasetFile rdfDatasetFile) {
    // Start import or re-import
    rdfDatasetFile.setStatusImport(RdfDatasetFile.RdfDatasetFileStatus.IMPORTING);
  }

  private void processStartReplaceRdfDatasetFile(RdfDatasetFile rdfDatasetFile) {
    // Start Replace
    rdfDatasetFile.setStatusImport(RdfDatasetFile.RdfDatasetFileStatus.REPLACING);
  }

  private void processReplacingRdfDatasetFile(RdfDatasetFile rdfDatasetFile, TriplestoreDataset triplestoreDataset) {
    org.springframework.core.io.Resource rdfFile = new FileSystemResource(Paths.get(rdfDatasetFile.getFilePath()));
    this.triplestoreResourceService.replaceDatasetContent(triplestoreDataset.getName(), rdfFile);
    // Re-Importing all the other RdfDatasetFile from the same TriplestoreDataset imported as not imported
    this.rdfDatasetFileService.reimportOtherRdfDatasetFiles(rdfDatasetFile.getTriplestoreDatasetId());
    // Re-Importing Research Data files for the same triplestore dataset
    SolidifyEventPublisher.getPublisher().publishEvent(new ResearchDataFileReloadMessage(rdfDatasetFile.getProjectId()));
    // Update status
    rdfDatasetFile.setStatusImport(RdfDatasetFile.RdfDatasetFileStatus.INDEXING);
  }

  private void processStartRemoveRdfDatasetFile(RdfDatasetFile rdfDatasetFile) {
    // Start remove
    rdfDatasetFile.setStatusImport(RdfDatasetFile.RdfDatasetFileStatus.REMOVING);
  }

  private void processRemovingRdfDatasetFile(RdfDatasetFile rdfDatasetFile, TriplestoreDataset triplestoreDataset, boolean publicProject) {
    this.triplestoreResourceService.deleteDatasetContent(triplestoreDataset.getName());
    // Re-Importing all the other RdfDatasetFile from the same TriplestoreDataset already imported
    this.rdfDatasetFileService.reimportOtherRdfDatasetFiles(rdfDatasetFile.getTriplestoreDatasetId());
    // Re-Importing Research Data files for the same triplestore dataset
    SolidifyEventPublisher.getPublisher().publishEvent(new ResearchDataFileReloadMessage(rdfDatasetFile.getProjectId()));
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);

    this.deleteFromIndex(ResourceName.PRIVATE_INDEX, rdfDatasetFile.getResId(), pageable);
    if (publicProject) {
      this.deleteFromIndex(ResourceName.PUBLIC_INDEX, rdfDatasetFile.getResId(), pageable);
    }
    rdfDatasetFile.setStatusImport(RdfDatasetFile.RdfDatasetFileStatus.NOT_IMPORTED);
  }

  private void deleteFromIndex(String indexName, String rdfDatasetFileResId, Pageable pageable) {
    // Search each reference of RdfDatasetFile in the index and delete each of them
    String query = "sourceId:" + rdfDatasetFileResId;
    RestCollection<ResearchObjectMetadata> listIndex = this.indexingService.getIndexMetadataList(indexName, query, null,
            pageable);
    for (ResearchObjectMetadata index : listIndex.getData()) {
      this.indexingService.deleteIndexMetadata(indexName, index.getResId());
    }
  }

  private void processImportingRdfDatasetFile(RdfDatasetFile rdfDatasetFile, TriplestoreDataset triplestoreDataset) {
    // Add RDF metadata
    org.springframework.core.io.Resource rdfFile = new FileSystemResource(Paths.get(rdfDatasetFile.getFilePath()));
    this.triplestoreResourceService.addDatasetContent(triplestoreDataset.getName(), rdfFile);
    rdfDatasetFile.setTriplestoreDatasetId(triplestoreDataset.getResId());
    rdfDatasetFile.setStatusImport(RdfDatasetFile.RdfDatasetFileStatus.INDEXING);
  }

  private void processIndexingRdfDatasetFile(RdfDatasetFile rdfDatasetFile, Project project) throws JsonProcessingException, JAXBException {
    List<ResearchObjectType> researchObjectTypeList = this.projectService.getAllResearchObjectTypes(project.getResId());
    if (!researchObjectTypeList.isEmpty()) {
      List<String> rdfTypeList = new ArrayList<>();
      Model model = RdfTool.getRdfModel(Paths.get(rdfDatasetFile.getFilePath()));
      // Find active RDF Type
      final Query localQuery = SparqlQueryTool.queryToListRdfTypes(SparqlBuilderTool.QUERY_VARIABLE);
      try (QueryExecution localExecution = QueryExecutionFactory.create(localQuery, model)) {
        ResultSet resultSet = localExecution.execSelect();
        while (resultSet.hasNext()) {
          QuerySolution solution = resultSet.nextSolution();
          rdfTypeList.add(solution.get(SparqlBuilderTool.QUERY_VARIABLE).toString());
        }
      }
      // For each Research Object Type
      for (ResearchObjectType researchObjectType : researchObjectTypeList) {
        final ObjectType objectType = this.getObjectType(project, researchObjectType);
        if (rdfTypeList.stream().anyMatch(r -> r.equals(researchObjectType.getRdfTypeUri()))) {
          this.listSubjects(rdfDatasetFile.getResId(), objectType, rdfDatasetFile.getLastUpdate().getWhen(), project, researchObjectType, model);
        }
      }

    }
    // Update status
    rdfDatasetFile.setStatusImport(RdfDatasetFile.RdfDatasetFileStatus.IMPORTED);
  }

  private ObjectType getObjectType(Project project, ResearchObjectType researchObjectType) {
    if (project.getIiifManifestResearchObjectType() != null
            && project.getIiifManifestResearchObjectType().getResId().equals(researchObjectType.getResId())) {
      return ObjectType.MANIFEST;
    }
    if (project.getResearchDataFileResearchObjectType() != null
            && project.getResearchDataFileResearchObjectType().getResId().equals(researchObjectType.getResId())) {
      return ObjectType.FILE;
    }
    return ObjectType.MANAGED;
  }

  private void listSubjects(String sourceId, ObjectType objectType, OffsetDateTime sourceLastUpdate, Project project,
          ResearchObjectType researchObjectType, Model model)
          throws JsonProcessingException, JAXBException {
    log.info("Indexing {} for {}: in-progress", researchObjectType.getRdfTypeUri(), sourceId);
    final Query listQuery = SparqlQueryTool.queryToListSubjectsOfRdfType(SparqlBuilderTool.SUBJECT_VARIABLE, researchObjectType.getRdfTypeUri());
    long i = 0;
    try (QueryExecution localExecution = QueryExecutionFactory.create(listQuery, model)) {
      ResultSet resultSet = localExecution.execSelect();
      // For each subject
      while (resultSet.hasNext()) {
        i++;
        if (i % 1000 == 0) {
          log.info("Indexing {} for {}: {}", researchObjectType.getRdfTypeUri(), sourceId, i);
        }
        QuerySolution solution = resultSet.nextSolution();
        this.indexSubject(sourceId, objectType, sourceLastUpdate, project, researchObjectType, model,
                solution.getResource(SparqlBuilderTool.SUBJECT_VARIABLE).toString());
      }
    }
    log.info("Indexing {} for {}: completed", researchObjectType.getRdfTypeUri(), sourceId);
  }

  private void indexSubject(String sourceId, ObjectType objectType, OffsetDateTime sourceLastUpdate, Project project,
          ResearchObjectType researchObjectType, Model model, String subject)
          throws JsonProcessingException, JAXBException {
    final Query listQuery = SparqlQueryTool.queryToListSubjectProperties(subject, SparqlBuilderTool.PROPERTY_VARIABLE,
            SparqlBuilderTool.OBJECT_VARIABLE);
    final Map<String, Object> properties = new HashMap<>();
    try (QueryExecution listExecution = QueryExecutionFactory.create(listQuery, model)) {
      ResultSet listResultSet = listExecution.execSelect();
      while (listResultSet.hasNext()) {
        QuerySolution solution = listResultSet.nextSolution();
        properties.put(this.getPropertyName(solution.get(SparqlBuilderTool.PROPERTY_VARIABLE).toString()),
                solution.get(SparqlBuilderTool.OBJECT_VARIABLE).toString());
      }
    }
    final String objectId = this.getHederaId(subject);
    // Index RDF information in index database in private index
    this.indexRdfInfo(ResourceName.PRIVATE_INDEX, this.getPrivateIndex(), objectId, researchObjectType, subject, sourceId, objectType,
            sourceLastUpdate, properties,
            project);

    // Index RDF information in public index if project is public
    if (project.isAccessPublic().booleanValue()) {
      this.indexRdfInfo(ResourceName.PUBLIC_INDEX, this.getPublicIndex(), objectId, researchObjectType, subject, sourceId, objectType,
              sourceLastUpdate, properties,
              project);
    }
    log.debug("ResearchObjectType '{}' '{}' successfully indexed in '{}' & '{}' for project '{}'", researchObjectType.getName(), subject,
            this.getPublicIndex(), this.getPrivateIndex(), project.getName());
  }

  private String getHederaId(String subject) {
    return subject.substring(subject.lastIndexOf("/") + 1);
  }

  private String getPropertyName(String property) {
    if (property.contains("#")) {
      return property.substring(property.lastIndexOf("#") + 1);
    }
    return property.substring(property.lastIndexOf("/") + 1);
  }

  private void indexRdfInfo(String typeIndex, String indexName, String objectId, ResearchObjectType researchObjectType, String subject,
          String sourceId, ObjectType objectType, OffsetDateTime sourceLastUpdate, Map<String, Object> properties, Project project)
          throws JAXBException, JsonProcessingException {
    final String errorPrefix = "ResearchObjectType '" + researchObjectType.getName() + "' '" + subject + "'";
    final String errorSuffix = "'" + typeIndex + "' for project '" + project.getName() + "'";
    try {
      if (this.indexingService.checkMetadata(typeIndex, objectId)
              && !this.indexingService.deleteIndexMetadata(typeIndex, objectId)) {
        throw new SolidifyProcessingException(errorPrefix + " could not be deleted in " + errorSuffix);
      }
      if (!this.indexingService.createIndexMetadata(typeIndex, new ResearchObjectMetadata(
              indexName,
              objectId,
              this.metadataService.getMetadataInJson(
                      project,
                      researchObjectType,
                      objectId,
                      subject,
                      objectType,
                      sourceLastUpdate,
                      sourceId,
                      properties)))) {
        throw new SolidifyProcessingException(errorPrefix + " could not be indexed in " + errorSuffix);
      }
    } catch (RuntimeException e) {
      throw new SolidifyProcessingException(errorPrefix + " could not be processed for indexing in " + errorSuffix, e);
    }
  }

  @Transactional
  public void purgeProjectTriplestore(Project project, TriplestoreDataset triplestoreDataset) {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    // Set filter criteria
    final SourceDataset sourceDatasetFilter = new SourceDataset();
    sourceDatasetFilter.setProject(project);
    // List all source datasets of the project
    for (SourceDataset sd : this.sourceDatasetService.findAll(new SourceDatasetSpecification(sourceDatasetFilter), pageable)) {
      // List all source dataset file of the dataset
      final SourceDatasetFile sourceDatasetFileFilter = new SourceDatasetFile();
      sourceDatasetFileFilter.setDataset(sd);
      for (SourceDatasetFile sdf : this.sourceDatasetFileService.findAll(new SourceDatasetFileSpecification(sourceDatasetFileFilter),
              pageable)) {
        final RdfDatasetFile rdfDatasetFileFilter = new RdfDatasetFile();
        rdfDatasetFileFilter.setSourceDatasetFile(sdf);
        rdfDatasetFileFilter.setTriplestoreDatasetId(triplestoreDataset.getResId());
        rdfDatasetFileFilter.setStatusImport(RdfDatasetFileStatus.IMPORTED);
        for (RdfDatasetFile rdfDatasetFile : this.rdfDatasetFileService.findAll(new RdfDatasetFileSpecification(rdfDatasetFileFilter),
                pageable)) {
          // Reset status
          rdfDatasetFile.setStatusImport(RdfDatasetFileStatus.NOT_IMPORTED);
          this.rdfDatasetFileService.save(rdfDatasetFile);
          log.info("RDF dataset file {}/{}/{} [{}]: {}",
                  rdfDatasetFile.getSourceDatasetFile().getDataset().getName(),
                  rdfDatasetFile.getSourceDatasetFile().getFileName(),
                  rdfDatasetFile.getFileName(),
                  rdfDatasetFile.getResId(),
                  rdfDatasetFile.getStatusImport());
        }
      }
    }
    // Delete triple store
    this.triplestoreResourceService.deleteDatasetContent(triplestoreDataset.getName());
    // Delete index metadata
    this.indexingService.deleteMetadataByProject(ResourceName.PUBLIC_INDEX, project.getShortName());
    this.indexingService.deleteMetadataByProject(ResourceName.PRIVATE_INDEX, project.getShortName());
  }
}
