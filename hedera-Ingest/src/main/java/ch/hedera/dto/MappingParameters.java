/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - MappingParameters.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.dto;

import java.util.ArrayList;
import java.util.List;

import jakarta.validation.constraints.NotNull;

import ch.hedera.model.RdfFormat;

public class MappingParameters {

  public enum MapperEngine {
    NONE, CARML, RML_MAPPER
  }

  @NotNull
  private String rmlFileId;
  private RdfFormat rdfFormat = RdfFormat.TURTLE;

  private List<String> sourceDatasetFileIds = new ArrayList<>();

  public String getRmlFileId() {
    return this.rmlFileId;
  }

  public void setRmlFileId(String rmlFileId) {
    this.rmlFileId = rmlFileId;
  }

  public RdfFormat getRdfFormat() {
    return this.rdfFormat;
  }

  public void setRdfFormat(RdfFormat rdfFormat) {
    this.rdfFormat = rdfFormat;
  }

  public List<String> getSourceDatasetFileIds() {
    return this.sourceDatasetFileIds;
  }

  public void setSourceDatasetFileIds(List<String> ids) {
    this.sourceDatasetFileIds = ids;
  }
}
