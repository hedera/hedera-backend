/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - RdfDatasetFileService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.service.ResourceService;

import ch.hedera.config.HederaProperties;
import ch.hedera.controller.IngestController;
import ch.hedera.message.RdfStorageMessage;
import ch.hedera.model.RdfOperation;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.RdfDatasetFile.RdfDatasetFileStatus;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.repository.RdfDatasetFileRepository;
import ch.hedera.service.rest.fallback.FallbackProjectRemoteResourceService;
import ch.hedera.specification.RdfDatasetFileSpecification;

@Service
@ConditionalOnBean(IngestController.class)
public class RdfDatasetFileService extends ResourceService<RdfDatasetFile> {

  private final HederaProperties configService;
  private final SourceDatasetFileService sourceDatasetFileService;
  private final FallbackProjectRemoteResourceService projectService;

  public RdfDatasetFileService(HederaProperties configService, @Lazy SourceDatasetFileService sourceDatasetFileService,
          FallbackProjectRemoteResourceService projectService) {
    this.configService = configService;
    this.sourceDatasetFileService = sourceDatasetFileService;
    this.projectService = projectService;
  }

  @Override
  public RdfDatasetFileSpecification getSpecification(RdfDatasetFile resource) {
    return new RdfDatasetFileSpecification(resource);
  }

  @Override
  protected RdfDatasetFile afterFind(RdfDatasetFile resource) {
    resource = super.afterFind(resource);
    resource.getSourceDatasetFile().getDataset()
            .setProject(this.projectService.findOneWithCache(resource.getSourceDatasetFile().getDataset().getProjectId()));
    return resource;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void reimportOtherRdfDatasetFiles(String triplestoreDatasetId) {
    List<RdfDatasetFile> rdfDatasetFiles = this.getRdfDatasetFileFromTriplestoreDatasetId(triplestoreDatasetId);
    rdfDatasetFiles.forEach(file -> {
      if (!file.getResId().equals(triplestoreDatasetId) && file.getStatusImport().equals(RdfDatasetFileStatus.IMPORTED)) {
        file.setStatusImport(RdfDatasetFile.RdfDatasetFileStatus.TO_REIMPORT);
        this.save(file);
        this.putInProcessingQueue(file, triplestoreDatasetId, RdfOperation.ADD);
      }
    });
  }

  public boolean putInProcessingQueue(RdfDatasetFile rdfDatasetFile, String triplestoreDatasetId, RdfOperation rdfOperation) {
    if (rdfDatasetFile.isInProgress()) {
      SolidifyEventPublisher.getPublisher()
              .publishEvent(
                      new RdfStorageMessage(rdfDatasetFile.getResId(), triplestoreDatasetId, rdfOperation,
                              rdfDatasetFile.getFileSize() > this.getFileSizeLimitation()));
      return true;
    }
    return false;
  }

  @Override
  public void delete(String id) {
    final RdfDatasetFile rdfDatasetFile = this.findOne(id);
    this.itemRepository.deleteById(id);

    // If the rdfDatasetFile being deleted is the only one coming from the sourceDatasetFile, set the sourceDatasetFile to NOT_TRANSFORMED
    final SourceDatasetFile sourceDatasetFile = rdfDatasetFile.getSourceDatasetFile();
    List<RdfDatasetFile> rdfDatasetFileList = ((RdfDatasetFileRepository) this.itemRepository).findBySourceDatasetFile(sourceDatasetFile);
    if (rdfDatasetFileList.isEmpty()) {
      sourceDatasetFile.setStatus(SourceDatasetFile.SourceDatasetFileStatus.NOT_TRANSFORMED);
      this.sourceDatasetFileService.save(sourceDatasetFile);
    }
  }

  public List<RdfDatasetFile> getRdfDatasetFileFromTriplestoreDatasetId(String tripleStoreDatasetId) {
    return ((RdfDatasetFileRepository) this.itemRepository).findByTriplestoreDatasetId(tripleStoreDatasetId);
  }

  private Long getFileSizeLimitation() {
    return this.configService.getQueue().getSourceDatasetBigFileLimit();
  }

  public List<RdfDatasetFile> findBySourceDatasetFile(SourceDatasetFile sourceDatasetFileId) {
    return ((RdfDatasetFileRepository) this.itemRepository).findBySourceDatasetFile(sourceDatasetFileId);
  }
}
