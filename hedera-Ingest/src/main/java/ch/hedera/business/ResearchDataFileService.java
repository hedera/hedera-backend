/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - ResearchDataFileService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.jena.query.ResultSet;
import org.apache.jena.update.UpdateRequest;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyFileDeleteException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.service.CompositeResourceService;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.IngestController;
import ch.hedera.message.ResearchDataFileMessage;
import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.ResearchDataFile.ResearchDataFileFileStatus;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.repository.ResearchDataFileRepository;
import ch.hedera.repository.SourceDatasetFileRepository;
import ch.hedera.rest.ResourceName;
import ch.hedera.service.rest.fallback.FallbackProjectRemoteResourceService;
import ch.hedera.service.rest.fallback.FallbackResearchObjectTypeRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedResearchObjectMetadataRemoteResourceService;
import ch.hedera.service.storage.ProjectStorage;
import ch.hedera.service.triplestore.TriplestoreResourceService;
import ch.hedera.specification.ResearchDataFileSpecification;
import ch.hedera.tool.HederaValidationTool;
import ch.hedera.util.SparqlQueryTool;
import ch.hedera.util.SparqlUpdateTool;

@Service
@ConditionalOnBean(IngestController.class)
public class ResearchDataFileService extends CompositeResourceService<ResearchDataFile> {

  private final HederaProperties configService;
  private final FallbackProjectRemoteResourceService projectService;
  private final FallbackResearchObjectTypeRemoteResourceService researchObjectTypeService;
  private final SourceDatasetFileRepository sourceDatasetFileRepository;
  private final TriplestoreResourceService triplestoreResourceService;
  private final TrustedResearchObjectMetadataRemoteResourceService indexingService;

  public ResearchDataFileService(
          HederaProperties configService,
          FallbackProjectRemoteResourceService projectService,
          FallbackResearchObjectTypeRemoteResourceService researchObjectTypeService,
          SourceDatasetFileRepository sourceDatasetFileRepository,
          TriplestoreResourceService triplestoreResourceService,
          TrustedResearchObjectMetadataRemoteResourceService indexingService) {
    this.configService = configService;
    this.projectService = projectService;
    this.researchObjectTypeService = researchObjectTypeService;
    this.sourceDatasetFileRepository = sourceDatasetFileRepository;
    this.triplestoreResourceService = triplestoreResourceService;
    this.indexingService = indexingService;
  }

  @Override
  public SolidifySpecification<ResearchDataFile> getSpecification(ResearchDataFile resource) {
    return new ResearchDataFileSpecification(resource);
  }

  public List<ResearchDataFile> findByProjectIdAndStatus(String projectId, ResearchDataFileFileStatus status) {
    return ((ResearchDataFileRepository) this.itemRepository).findByProjectIdAndStatus(projectId, status);
  }

  public long getTotalSizeByProject(String projectId) {
    final Long totalSize = ((ResearchDataFileRepository) this.itemRepository).getTotalSizeByProject(projectId);
    if (totalSize == null) {
      return 0L;
    }
    return totalSize.longValue();
  }

  public List<String> findByProjectIdAndRelativeLocation(String projectId, String relativeLocation) {
    String relativeLocationSubFolder = relativeLocation;
    if (!relativeLocation.endsWith("/")) {
      relativeLocationSubFolder = relativeLocationSubFolder + "/";
    }
    relativeLocationSubFolder = relativeLocationSubFolder + "%";
    return ((ResearchDataFileRepository) this.itemRepository).findResearchDataFileIdByRelativeLocationFolderAndProject(projectId,
            relativeLocation, relativeLocationSubFolder);
  }

  public Optional<ResearchDataFile> findByProjectIdAndRelativeLocationAndFileName(String projectId, String relativeLocation, String fileName) {
    List<ResearchDataFile> existingFiles = this.findByProjectIdAndRelativeLocationAndFileNameCaseInsensitive(projectId, relativeLocation,
            fileName);
    List<ResearchDataFile> matchingFiles = existingFiles.stream()
            .filter(df -> df.getFileName().equals(fileName) && df.getRelativeLocation().equals(relativeLocation)).toList();

    if (matchingFiles.size() == 1) {
      return Optional.of(matchingFiles.get(0));
    } else if (matchingFiles.isEmpty()) {
      return Optional.empty();
    } else {
      throw new SolidifyRuntimeException(
              "Duplicated files found for projectId=" + projectId + ", relativeLocation=" + relativeLocation + ", fileName=" + fileName);
    }
  }

  public ResearchDataFile getResearchDataFile(String projectId, Path path, String location, String researchObjectTypeId, String mimeType,
          String originalFilename) {
    final ResearchDataFile researchDataFile = new ResearchDataFile();
    researchDataFile.setFileName(originalFilename);
    researchDataFile.setFileSize(FileTool.getSize(path));
    researchDataFile.setMimeType(mimeType);
    researchDataFile.setSourcePath(path.toUri());
    researchDataFile.setResearchObjectTypeId(researchObjectTypeId);
    researchDataFile.setAccessibleFrom(DataAccessibilityType.valueOf(location));
    researchDataFile.setProjectId(projectId);
    return researchDataFile;
  }

  public List<ResearchDataFile> findByProjectId(String projectId) {
    return ((ResearchDataFileRepository) this.itemRepository).findByProjectId(projectId);
  }

  @Override
  protected <S extends Resource> RemoteResourceService<S> getResourceService(Class<S> type) {

    if (type == Project.class) {
      return (RemoteResourceService<S>) this.projectService;
    }
    if (type == ResearchObjectType.class) {
      return (RemoteResourceService<S>) this.researchObjectTypeService;

    }
    throw new UnsupportedOperationException("Missing resource service for type " + type.getName());
  }

  @Override
  public void delete(String id) {
    final ResearchDataFile researchDataFile = this.findOne(id);
    if (this.canDeleteResearchDataFile(researchDataFile)) {
      try {
        // Check if the file is still in the temp directory
        String prefix = this.configService.getResearchDataFileLocation() + "/" + researchDataFile.getProject().getProjectId();
        if (researchDataFile.getSourcePath().getPath().startsWith(prefix) && FileTool.checkFile(Paths.get(researchDataFile.getSourcePath()))) {
          FileTool.deleteFile(Paths.get(researchDataFile.getSourcePath()));
        } else {
          final ProjectStorage projectStorage = ProjectStorage.getProjectStorage(this.configService.getStorage(), researchDataFile.getProject());
          projectStorage.deleteResearchDataFile(researchDataFile);
        }
      } catch (IOException e) {
        throw new SolidifyFileDeleteException("Unable to delete files of the ResearchDataFile", e);
      }
      // Delete RDF metadata
      this.deleteResearchDatasetFromFuseki(researchDataFile);

      // Delete private index metadata
      if (this.indexingService.checkMetadata(ResourceName.PRIVATE_INDEX, id)
              && !this.indexingService.deleteIndexMetadata(ResourceName.PRIVATE_INDEX, id)) {
        throw new SolidifyProcessingException("Cannot remove index metadata");
      }

      // Delete restricted index if project is private
      if (researchDataFile.getProject().isAccessPublic().booleanValue()
              && this.indexingService.checkMetadata(ResourceName.PUBLIC_INDEX, id)
              && !this.indexingService.deleteIndexMetadata(ResourceName.PUBLIC_INDEX, id)) {
        throw new SolidifyProcessingException("Cannot remove index metadata");
      }
      // Delete entity
      this.itemRepository.deleteById(id);
    } else {
      throw new SolidifyCheckingException(this.messageService.get("validation.researchDataFile.delete.error.referenced"));
    }
  }

  /**
   * Check that ResearchDataFile is not used in triplestore neither as subject, neither as an object.
   *
   * @param researchDataFile
   * @return
   */
  private boolean canDeleteResearchDataFile(ResearchDataFile researchDataFile) {
    // Check if the research data file is linked to source dataset file
    final List<SourceDatasetFile> sourceDatasetFilelist = this.sourceDatasetFileRepository.findByResearchDataFile(researchDataFile);
    if (!sourceDatasetFilelist.isEmpty()) {
      return false;
    }
    // Check if the research data file has RDF metadata
    if (researchDataFile.getRdfMetadata() == null) {
      return true;
    }
    // If yes, check if the research data file is referenced in RDF triple store
    String hederaId = researchDataFile.getUri().toString();
    ResultSet resultAsSubject = this.triplestoreResourceService.executeQuery(researchDataFile.getProject().getShortName(),
            SparqlQueryTool.queryToFilterByRdfMetadata(hederaId, new ByteArrayInputStream(researchDataFile.getRdfMetadata().getBytes())));
    ResultSet resultAsObject = this.triplestoreResourceService.executeQuery(researchDataFile.getProject().getShortName(),
            SparqlQueryTool.queryAsObject(hederaId));
    return !resultAsSubject.hasNext() && !resultAsObject.hasNext();
  }

  public void deleteResearchDatasetFromFuseki(ResearchDataFile researchDataFile) {
    // check if the file is referenced in RDF
    if (researchDataFile.getRdfMetadata() != null) {
      final String hederaId = researchDataFile.getUri().toString();
      final UpdateRequest updateRequest = SparqlUpdateTool.updateToDeleteRdfMetadata(hederaId,
              new ByteArrayInputStream(researchDataFile.getRdfMetadata().getBytes()));
      this.triplestoreResourceService.executeUpdate(researchDataFile.getProject().getShortName(), updateRequest);
    }
  }

  @Override
  public void validateLinkedResources(ResearchDataFile researchDataFile, BindingResult errors) {
    final Project project = this.checkProject(researchDataFile, errors);
    final ResearchObjectType rot = this.checkResearchObjectType(researchDataFile, errors);
    if (project != null && rot != null) {
      this.checkProjectResearchObjectTypeOfResearchDataFile(rot, project, errors);
    }
  }

  @Override
  public void patchResource(ResearchDataFile researchDataFile, Map<String, Object> updateMap) {
    if (updateMap.containsKey(HederaConstants.RELATIVE_LOCATION_PARAM)
            && !researchDataFile.getRelativeLocation().equals(updateMap.get(HederaConstants.RELATIVE_LOCATION_PARAM))) {
      updateMap.put("status", ResearchDataFile.ResearchDataFileFileStatus.CHANGE_RELATIVE_LOCATION);
    }
    super.patchResource(researchDataFile, updateMap);
  }

  @Override
  public ResearchDataFile save(ResearchDataFile researchDataFile) {
    final ResearchDataFile result = super.save(researchDataFile);

    if (researchDataFile.getStatus().equals(ResearchDataFile.ResearchDataFileFileStatus.CHANGE_RELATIVE_LOCATION)) {
      this.putInProcessingQueue(researchDataFile);
    }
    return result;
  }

  @Override
  protected void validateItemSpecificRules(ResearchDataFile item, BindingResult errors) {
    if (!HederaValidationTool.isValidFileName(item.getFileName())) {
      errors.addError(
              new FieldError(item.getClass().getSimpleName(), "fileName",
                      this.messageService.get("validation.researchDataFile.error.filename",
                              new Object[] { item.getFileName() })));
    }
    if (!HederaValidationTool.isValidRelativeLocation(
            item.getRelativeLocation())) { // check that relative location don't contain space or accented characters.
      errors.addError(
              new FieldError(item.getClass().getSimpleName(), "relativeLocation",
                      this.messageService.get("validation.researchDataFile.error.relativeLocation",
                              new Object[] { item.getFileName() })));
    }
  }

  public boolean putInProcessingQueue(ResearchDataFile researchDataFile) {
    if (researchDataFile.isInProgress()) {
      SolidifyEventPublisher.getPublisher().publishEvent(
              new ResearchDataFileMessage(researchDataFile.getResId(), researchDataFile.getFileSize() > this.getFileSizeLimitation()));
      return true;
    }
    return false;
  }

  private List<ResearchDataFile> findByProjectIdAndRelativeLocationAndFileNameCaseInsensitive(String projectId, String relativeLocation,
          String fileName) {
    return ((ResearchDataFileRepository) this.itemRepository).findByProjectIdAndRelativeLocationAndFileNameIgnoreCase(projectId,
            relativeLocation, fileName);
  }

  private Long getFileSizeLimitation() {
    return this.configService.getQueue().getSourceDatasetBigFileLimit();
  }

  private Project checkProject(ResearchDataFile researchDataFile, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(researchDataFile.getProjectId())) {
      try {
        final Project project = this.projectService.findOne(researchDataFile.getProjectId());
        if (!project.isOpen()) {
          errors.addError(new FieldError(researchDataFile.getClass().getSimpleName(), HederaConstants.PROJECT_ID_FIELD,
                  this.messageService.get("validation.project.closed", new Object[] { researchDataFile.getProjectId() })));
        }
        return project;
      } catch (final SolidifyResourceNotFoundException e) {
        errors.addError(new FieldError(researchDataFile.getClass().getSimpleName(), HederaConstants.PROJECT_ID_FIELD,
                this.messageService.get("validation.resource.notexist", new Object[] { researchDataFile.getProjectId() })));
      }
    }
    return null;
  }

  private ResearchObjectType checkResearchObjectType(ResearchDataFile researchDataFile, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(researchDataFile.getResearchObjectTypeId())) {
      try {
        return this.researchObjectTypeService.findOne(researchDataFile.getResearchObjectTypeId());
      } catch (final SolidifyResourceNotFoundException e) {
        errors.addError(new FieldError(researchDataFile.getClass().getSimpleName(), HederaConstants.RESEARCH_OBJECT_TYPE_ID_FIELD,
                this.messageService.get("validation.resource.notexist", new Object[] { researchDataFile.getResearchObjectTypeId() })));
      }
    }
    return null;
  }

  private void checkProjectResearchObjectTypeOfResearchDataFile(ResearchObjectType researchObjectType, Project project, BindingResult errors) {
    if (!project.getResearchDataFileResearchObjectType().getResId().equals(researchObjectType.getResId())) {
      errors.addError(new FieldError(researchObjectType.getClass().getSimpleName(), HederaConstants.RESEARCH_OBJECT_TYPE_ID_FIELD,
              this.messageService.get("validation.project.researchObjectType",
                      new Object[] { project.getProjectId(), researchObjectType.getResId() })));
    }
  }

  public List<String> getRelativeLocationList(String projectId) {
    final List<String> results = ((ResearchDataFileRepository) this.itemRepository).findDistinctRelativeLocationByProject(projectId);
    return new ArrayList<>(results);
  }

}
