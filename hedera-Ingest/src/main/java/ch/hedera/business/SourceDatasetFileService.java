/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - SourceDatasetFileService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.nio.file.Path;
import java.util.NoSuchElementException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.FileTool;

import ch.hedera.controller.IngestController;
import ch.hedera.model.dto.FileUploadDto;
import ch.hedera.model.ingest.DatasetFileType;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.ResearchDataFile.ResearchDataFileFileStatus;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.service.rest.fallback.FallbackProjectRemoteResourceService;
import ch.hedera.specification.SourceDatasetFileSpecification;

@Service
@ConditionalOnBean(IngestController.class)
public class SourceDatasetFileService extends ResourceService<SourceDatasetFile> {

  private static final String RESEARCH_DATA_FILE_ID = "researchDataFileId";

  private final FallbackProjectRemoteResourceService projectService;
  private final ResearchDataFileService researchDataFileService;
  private final RdfDatasetFileService rdfDatasetFileService;

  public SourceDatasetFileService(FallbackProjectRemoteResourceService projectService, ResearchDataFileService researchDataFileService,
          RdfDatasetFileService rdfDatasetFileService) {
    this.projectService = projectService;
    this.researchDataFileService = researchDataFileService;
    this.rdfDatasetFileService = rdfDatasetFileService;
  }

  @Override
  public SolidifySpecification<SourceDatasetFile> getSpecification(SourceDatasetFile resource) {
    return new SourceDatasetFileSpecification(resource);
  }

  @Override
  protected SourceDatasetFile afterFind(SourceDatasetFile resource) {
    resource = super.afterFind(resource);
    resource.getDataset().setProject(this.projectService.findOneWithCache(resource.getDataset().getProjectId()));
    return resource;
  }

  @Override
  protected void validateItemSpecificRules(SourceDatasetFile item, BindingResult errors) {
    // Check if linked to research data file
    if (item.isLinkToResearchDataFile()) {
      try {
        final ResearchDataFile researchDataFile = this.researchDataFileService.findOne(item.getResearchDataFile().getResId());
        if (!item.getProjectId().equals(researchDataFile.getProjectId())) {
          errors.addError(new FieldError(item.getClass().getSimpleName(), RESEARCH_DATA_FILE_ID,
                  this.messageService.get("validation.sourceDatasetFile.error.researchDataFile.wrongProject",
                          new Object[] { item.getResearchDataFile().getResId(), researchDataFile.getProjectId(), item.getProjectId() })));
        } else if (researchDataFile.getStatus() != ResearchDataFileFileStatus.COMPLETED) {
          errors.addError(new FieldError(item.getClass().getSimpleName(), RESEARCH_DATA_FILE_ID,
                  this.messageService.get("validation.sourceDatasetFile.error.researchDataFile.wrongStatus",
                          new Object[] { item.getResearchDataFile().getResId(), researchDataFile.getStatus() })));
        } else {
          item.setResearchDataFile(researchDataFile);
        }
      } catch (NoSuchElementException e) {
        errors.addError(new FieldError(item.getClass().getSimpleName(), RESEARCH_DATA_FILE_ID,
                this.messageService.get("validation.sourceDatasetFile.error.researchDataFile.notexist",
                        new Object[] { item.getResearchDataFile().getResId() })));
      }
    }
  }

  public SourceDatasetFile getSourceDatasetFile(SourceDataset sourceDataset, String fileType, String filename, String version, Path sourcePath) {
    final SourceDatasetFile sourceDatasetFile = new SourceDatasetFile();
    this.setSourceDatasetFileInfo(sourceDatasetFile, sourceDataset, fileType, filename, version, sourcePath);
    return sourceDatasetFile;
  }

  public SourceDatasetFile getSourceDatasetFile(SourceDataset sourceDataset, String fileType, String filename, String version, Path sourcePath,
          FileUploadDto<SourceDatasetFile> fileUploadDto) {
    final SourceDatasetFile sourceDatasetFile = fileUploadDto.getResourceFile();
    this.setSourceDatasetFileInfo(sourceDatasetFile, sourceDataset, fileType, filename, version, sourcePath);
    return sourceDatasetFile;
  }

  public String generateFilename(String originalFilename, String version) {
    StringBuilder filename = new StringBuilder();
    if (originalFilename.contains("/")) {
      originalFilename = originalFilename.replace("/", "--");
    }
    // File name
    if (originalFilename.contains(".")) {
      filename.append(originalFilename.substring(0, originalFilename.lastIndexOf(".")));
    } else {
      filename.append(originalFilename);
    }
    // Add version
    filename.append("-")
            .append(version);
    // Add extension
    if (originalFilename.contains(".")) {
      filename.append(originalFilename.substring(originalFilename.lastIndexOf(".")));
    }
    return filename.toString();
  }

  private void setSourceDatasetFileInfo(SourceDatasetFile sourceDatasetFile, SourceDataset sourceDataset, String fileType, String filename,
          String version, Path sourcePath) {
    sourceDatasetFile.setSourcePath(sourcePath.toUri());
    sourceDatasetFile.setFileSize(FileTool.getSize(sourcePath));
    sourceDatasetFile.setType(DatasetFileType.valueOf(fileType));
    sourceDatasetFile.setDataset(sourceDataset);
    sourceDatasetFile.setFileName(filename);
    sourceDatasetFile.setVersion(version);
    this.setAuthenticatedUserProperties(sourceDatasetFile);

  }

  @Override
  public void delete(String id) {
    final SourceDatasetFile sourceDatasetFile = this.findOne(id);
    if (!this.canDeleteSourceDatasetFile(sourceDatasetFile)) {
      throw new SolidifyProcessingException("Cannot delete SourceDatasetFile since it has RdfDatasetFile linked to it");
    }
    this.itemRepository.deleteById(id);
  }

  public boolean canDeleteSourceDatasetFile(SourceDatasetFile sourceDatasetFile) {
    return this.rdfDatasetFileService.findBySourceDatasetFile(sourceDatasetFile).isEmpty();
  }

}
