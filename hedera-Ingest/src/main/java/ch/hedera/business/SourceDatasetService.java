/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - SourceDatasetService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.service.CompositeResourceService;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.controller.IngestController;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.settings.Project;
import ch.hedera.service.rest.fallback.FallbackProjectRemoteResourceService;
import ch.hedera.specification.SourceDatasetSpecification;

@Service
@ConditionalOnBean(IngestController.class)
public class SourceDatasetService extends CompositeResourceService<SourceDataset> {

  private final FallbackProjectRemoteResourceService projectService;
  private final SourceDatasetFileService sourceDatasetFileService;

  public SourceDatasetService(FallbackProjectRemoteResourceService projectService, SourceDatasetFileService sourceDatasetFileService) {
    this.projectService = projectService;
    this.sourceDatasetFileService = sourceDatasetFileService;
  }

  @Override
  public SolidifySpecification<SourceDataset> getSpecification(SourceDataset resource) {
    return new SourceDatasetSpecification(resource);
  }

  @Override
  protected <S extends Resource> RemoteResourceService<S> getResourceService(Class<S> type) {

    if (type == Project.class) {
      return (RemoteResourceService<S>) this.projectService;
    }
    throw new UnsupportedOperationException("Missing resource service for type " + type.getName());
  }

  @Override
  public void validateLinkedResources(SourceDataset sourceDataset, BindingResult errors) {
    this.checkProject(sourceDataset, errors);
  }

  private void checkProject(SourceDataset sourceDataset, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(sourceDataset.getProjectId())) {
      try {
        final Project project = this.projectService.findOne(sourceDataset.getProjectId());
        if (!project.isOpen()) {
          errors.addError(new FieldError(sourceDataset.getClass().getSimpleName(), HederaConstants.PROJECT_ID_FIELD,
                  this.messageService.get("validation.project.closed", new Object[] { sourceDataset.getProjectId() })));
        }
      } catch (final SolidifyResourceNotFoundException e) {
        errors.addError(new FieldError(sourceDataset.getClass().getSimpleName(), HederaConstants.PROJECT_ID_FIELD,
                this.messageService.get("validation.resource.notexist", new Object[] { sourceDataset.getProjectId() })));
      }
    }
  }

  @Override
  public void delete(String id) {
    final SourceDataset sourceDataset = this.findOne(id);
    if (sourceDataset.getDataFiles().stream().anyMatch(rdf -> !this.sourceDatasetFileService.canDeleteSourceDatasetFile(rdf))) {
      throw new SolidifyProcessingException("Cannot delete SourceDataset since at least one of its SourceDatasetFile cannot be deleted");
    }
    this.itemRepository.deleteById(id);
  }
}
