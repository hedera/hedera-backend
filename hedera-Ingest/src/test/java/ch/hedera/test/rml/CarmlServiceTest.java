/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - CarmlServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.rml;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import jakarta.xml.bind.JAXBException;

import ch.hedera.service.rml.CarmlService;

@ActiveProfiles({ "rml-carml", "triplestore-inmemory", "sec-noauth" })
@ExtendWith(SpringExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CarmlServiceTest extends RmlMappingServiceTest {

  @BeforeEach
  public void setup() {
    super.setUp();
    // Carml
    this.rmlMappingService = new CarmlService(this.messageService, this.config, this.trustedRmlRemoteResourceService, this.metadataService,
            this.idMappingService);
  }

  @Order(10)
  @ParameterizedTest
  @MethodSource("rmlConversions")
  void applyRml(String ldProjectBaseUri, String filename, String rml) throws IOException {
    this.runRmlApplicationTest(ldProjectBaseUri, filename, rml);
  }

  @Order(20)
  @ParameterizedTest
  @MethodSource("rmlConversions")
  void applyRmlToGenerateRdfDatasetFiles(String projectUri, String filename, String rml) throws IOException {
    this.runRmlApplicationWithSourceDatasetTest(projectUri, filename, rml);
  }

  @Order(30)
  @ParameterizedTest
  @ValueSource(strings = { "geneve.jpg", "MyUNIGE.pdf", "subresources/Moreno_001.xml" })
  void applyRmlToResearchDataFiles(String filename) throws JAXBException, IOException {
    this.runRmlApplicationWithResearchDataFileTest("http://ld.hedera.ch/ludus", filename);
  }

  @Override
  protected List<String> getRmlFiles() {
    return List.of("research-data-file-by-project-json.ttl", "research-data-file-by-project-xml.ttl", "ludus-data.ttl", "pliegos.ttl",
            "rml-io-example.ttl", "characters.ttl");
  }

  @Override
  protected String getRmlFolder() {
    return "carml";
  }

  private static Stream<Arguments> rmlConversions() {
    return Stream.of(
            Arguments.of("http://ld.hedera.ch/ludus", "ludus-data.csv", "ludus-data.ttl"),
            Arguments.of("http://ld.hedera.ch/pliegos", "pliegos.xml", "pliegos.ttl"),
            Arguments.of("http://ld.hedera.ch/pliegos", "research-data-file-wo-rel-location.json", "research-data-file-by-project-json.ttl"),
            Arguments.of("http://ld.hedera.ch/pliegos", "research-data-file-w-rel-location.json", "research-data-file-by-project-json.ttl"),
            Arguments.of("http://ld.hedera.ch/pliegos", "research-data-file-wo-rel-location.xml", "research-data-file-by-project-xml.ttl"),
            Arguments.of("http://ld.hedera.ch/pliegos", "research-data-file-w-rel-location.xml", "research-data-file-by-project-xml.ttl"),
            Arguments.of("http://ld.hedera.ch/pliegos", "research-data-file.xml", "research-data-file-by-project-xml.ttl"),
            Arguments.of("http://example.org/rules/", "characters.json", "characters.ttl"));
  }

}
