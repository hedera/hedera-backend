/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - RmlMapperServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.rml;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.hedera.model.RdfFormat;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.service.rml.RmlMapperService;

@ActiveProfiles({ "rml-rmlmapper", "triplestore-inmemory", "sec-noauth" })
@ExtendWith(SpringExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Disabled("TODO RML Mapper not supported")
class RmlMapperServiceTest extends RmlMappingServiceTest {

  @BeforeEach
  public void setup() {
    super.setUp();
    // RML Mapper
    this.rmlMappingService = new RmlMapperService(this.messageService, this.config, this.trustedRmlRemoteResourceService, this.metadataService,
            this.idMappingService);
  }

  @Order(10)
  @ParameterizedTest
  @MethodSource("rmlConversions")
  void applyRml(String ldProjectBaseUri, String filename, String rml) throws IOException {
    this.runRmlApplicationTest(ldProjectBaseUri, filename, rml);
  }

  @Order(20)
  @ParameterizedTest
  @MethodSource("rmlConversions")
  void applyRmlToGenerateRdfDatasetFiles(String ldProjectBaseUri, String filename, String rml) throws IOException {
    SourceDatasetFile sourceDatasetFile = this.getSourceDataFile(filename);
    assertNotNull(sourceDatasetFile);
    RdfDatasetFile result = this.rmlMappingService.applyRml(sourceDatasetFile.getDataset().getProject(), sourceDatasetFile, rml,
            RdfFormat.TURTLE);
    assertNotNull(result);
  }

  private static Stream<Arguments> rmlConversions() {
    return Stream.of(
            Arguments.of("http://example.org/rules/", "characters.xml", "characters-xml.ttl"),
            Arguments.of("http://example.org/rules/", "characters.tsv", "characters-tsv.ttl"),
            Arguments.of("http://example.org/rules/", "characters.json", "characters-json.ttl"));
  }

  @Override
  protected List<String> getRmlFiles() {
    return List.of("characters-xml.ttl", "characters-tsv.ttl", "characters-json.ttl");
  }

  @Override
  protected String getRmlFolder() {
    return "rml-mapper";
  }

}
