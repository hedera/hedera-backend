/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - RmlMappingServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.rml;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import jakarta.xml.bind.JAXBException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.mock.mockito.MockBean;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.HederaIdentifier;
import ch.hedera.model.ingest.IdMapping;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.repository.HederaIdentifierRepository;
import ch.hedera.repository.IdMappingRepository;
import ch.hedera.service.HederaIdentifierService;
import ch.hedera.service.IdMappingService;
import ch.hedera.service.MetadataService;
import ch.hedera.service.rest.trusted.TrustedRmlRemoteResourceService;
import ch.hedera.service.rml.RmlMappingService;
import ch.hedera.util.RmlTool;

public abstract class RmlMappingServiceTest {

  private static final Logger log = LoggerFactory.getLogger(RmlMappingServiceTest.class);

  @Mock
  protected TrustedRmlRemoteResourceService trustedRmlRemoteResourceService;

  @MockBean(name = "idMappingRepository")
  protected IdMappingRepository idMappingRepository;

  @MockBean(name = "hederaIdentifierRepository")
  protected HederaIdentifierRepository hederaIdentifierRepository;

  @Mock
  protected MessageService messageService;

  protected RmlMappingService rmlMappingService;

  protected HederaProperties config = new HederaProperties(new SolidifyProperties(new GitInfoProperties()));

  protected MetadataService metadataService;

  protected HederaIdentifierService hederaIdentifierService;

  protected IdMappingService idMappingService;

  protected Path sourceFolder = Paths.get("src", "main", "resources", "rml").toAbsolutePath();
  protected Path testFolder = Paths.get("src", "test", "resources").toAbsolutePath();
  protected Path workingFolder = Paths.get("src", "test", "resources", "testing").toAbsolutePath();

  protected RmlMappingServiceTest() {
    this.config.setHome(this.workingFolder.toString());
    if (!FileTool.ensureFolderExists(Paths.get(this.config.getRmlLocation()))) {
      throw new SolidifyRuntimeException("Cannot create " + this.config.getRdfDatasetLocation());
    }
    if (!FileTool.ensureFolderExists(Paths.get(this.config.getRdfDatasetLocation()))) {
      throw new SolidifyRuntimeException("Cannot create " + this.config.getRdfDatasetLocation());
    }
    // Set Access module URL
    this.config.getModule().getAccess().setUrl("https://hedera.unige.ch/access");
    this.config.getModule().getIIIF().setUrl("https://hedera.unige.ch/iiif");
    this.config.getModule().getLinkedData().setUrl("https://hedera.unige.ch/ld");
    this.metadataService = new MetadataService(this.messageService);
  }

  @AfterEach
  public void purgeData() {
    FileTool.deleteFolder(this.workingFolder);
  }

  @BeforeEach
  public void setUp() {
    for (String rml : this.getRmlFiles()) {
      if (FileTool.checkFile(this.sourceFolder.resolve(rml))) {
        when(this.trustedRmlRemoteResourceService.downloadRmlFile(rml)).thenReturn(this.sourceFolder.resolve(rml));
      } else {
        when(this.trustedRmlRemoteResourceService.downloadRmlFile(rml)).thenReturn(this.testFolder.resolve(this.getRmlFolder()).resolve(rml));
      }
    }
    Optional<IdMapping> idMappingOpt = Optional.empty();
    when(this.idMappingRepository.findByBusinessIdAndProjectShortNameAndResearchObjectTypeName(any(String.class), any(String.class),
            any(String.class))).thenReturn(idMappingOpt);
    IdMapping idMapping = new IdMapping();
    idMapping.setBusinessId("businessId");
    idMapping.setProjectShortName("project");
    idMapping.setResearchObjectTypeName("rot");
    idMapping.setHederaId(10L);
    when(this.idMappingRepository.save(any(IdMapping.class))).thenReturn(idMapping);
    HederaIdentifier id = new HederaIdentifier();
    when(this.hederaIdentifierRepository.save(any(HederaIdentifier.class))).thenReturn(id);
    this.hederaIdentifierService = new HederaIdentifierService(this.hederaIdentifierRepository);
    this.idMappingService = new IdMappingService(this.hederaIdentifierService, this.idMappingRepository, this.config);
  }

  protected abstract String getRmlFolder();

  protected abstract List<String> getRmlFiles();

  protected void runRmlApplicationTest(String ldProjectBaseUri, String filename, String rml) throws IOException {
    final Path sourceFile = this.getDataFile(filename);
    final Path rmlFile = this.getDataFile(this.getRmlFolder(), rml);
    final String iiifImageBaseUrl = StringTool.removeTrailing(this.config.getModule().getIIIF().getPublicUrl(), "/");
    final Map<String, String> parametersMap = RmlTool.getPlaceHolderValues(URI.create(ldProjectBaseUri), sourceFile, iiifImageBaseUrl);
    Path result = this.rmlMappingService.applyRml(sourceFile, rmlFile, parametersMap, RdfFormat.TURTLE);
    assertNotNull(result);
    assertTrue(FileTool.checkFile(result));
    assertNotEquals(0L, FileTool.getSize(result));
    log.info(
            "\nResult [{}]:\n===============================================BEGIN===============================================\n{}\n================================================END================================================\n",
            filename,
            FileTool.toString(result));
  }

  protected void runRmlApplicationWithSourceDatasetTest(String projectUri, String filename, String rml) throws IOException {
    SourceDatasetFile sourceDatasetFile = this.getSourceDataFile(filename);
    assertNotNull(sourceDatasetFile);
    RdfDatasetFile result = this.rmlMappingService.applyRml(sourceDatasetFile.getDataset().getProject(), sourceDatasetFile, rml,
            RdfFormat.TURTLE);
    assertNotNull(result);
  }

  protected void runRmlApplicationWithResearchDataFileTest(String projectUri, String filename) throws IOException, JAXBException {
    ResearchDataFile researchDataFile = this.getResearchDataFile(filename);
    assertNotNull(researchDataFile);
    ResearchDataFile researchDataFile2 = this.rmlMappingService.applyRml(researchDataFile.getProject(), researchDataFile, RdfFormat.TURTLE);
    assertNotNull(researchDataFile2);
    assertNotNull(researchDataFile2.getRdfMetadata());
    assertNotNull(researchDataFile2.getUri());
    if (researchDataFile.getAccessibleFrom().equals(DataAccessibilityType.IIIF)) {
      assertTrue(researchDataFile2.getRdfMetadata().contains("iiif"));
    } else {
      assertTrue(researchDataFile2.getRdfMetadata().contains("access"));
    }
    assertFalse(researchDataFile2.getRdfMetadata().contains("%2F"));
    assertFalse(researchDataFile2.getRdfMetadata().contains("%23"));
    assertFalse(researchDataFile2.getRdfMetadata().replace("https://", "").replace("http://", "").contains("//"));
  }

  protected Path getDataFile(String filename) {
    return this.getDataFile(null, filename);
  }

  protected Path getDataFile(String folder, String filename) {
    Path file = this.getFile(folder, filename);
    assertNotNull(file);
    assertTrue(FileTool.checkFile(file));
    return file;
  }

  private Path getFile(String folder, String filename) {
    if (FileTool.checkFile(this.sourceFolder.resolve(filename))) {
      return this.sourceFolder.resolve(filename);
    }
    if (StringTool.isNullOrEmpty(folder)) {
      return this.testFolder.resolve(filename);
    }
    return this.testFolder.resolve(folder).resolve(filename);
  }

  protected SourceDatasetFile getSourceDataFile(String filename) {
    SourceDataset sourceDataset = new SourceDataset();
    sourceDataset.setName(filename);
    sourceDataset.setProject(this.getProject(filename));
    SourceDatasetFile sourceDatasetFile = new SourceDatasetFile(sourceDataset);
    final Path file = this.getDataFile(filename);
    sourceDatasetFile.setFileName(file.getFileName().toString());
    sourceDatasetFile.setFileSize(FileTool.getSize(file));
    sourceDatasetFile.setSourcePath(file.toUri());
    return sourceDatasetFile;
  }

  private Project getProject(String filename, ResearchObjectType rot) {
    Project project = this.getProject(filename);
    project.setResearchDataFileResearchObjectType(rot);
    return project;
  }

  private Project getProject(String filename) {
    Project project = new Project();
    project.setName(filename);
    project.setShortName(filename.substring(0, filename.indexOf(".")));
    project.setOpeningDate(LocalDate.now().minusDays(1));
    project.setAccessPublic(Boolean.TRUE);
    return project;
  }

  protected ResearchDataFile getResearchDataFile(String filename) {
    ResearchObjectType researchObjectType = new ResearchObjectType();
    researchObjectType.setName("test-object");
    researchObjectType.setRdfType(HederaConstants.RESEARCH_OBJECT_TYPE_RDF_TYPE_D1);
    Ontology onto = new Ontology();
    onto.setName("CRMdig");
    onto.setVersion("3.2.1");
    onto.setFormat(RdfFormat.RDF_XML);
    onto.setBaseUri(URI.create("http://www.ics.forth.gr/isl/CRMdig/"));
    researchObjectType.setOntology(onto);
    ResearchDataFile researchDataFile = new ResearchDataFile();
    researchDataFile.getLastUpdate().setWhen(OffsetDateTime.now());
    final Path file = this.getDataFile(filename);
    researchDataFile.setProject(this.getProject(filename, researchObjectType));
    researchDataFile.setResearchObjectType(researchObjectType);
    researchDataFile.setSourcePath(file.toUri());
    researchDataFile.setUri(URI.create(this.getUrl("https://hedera.unige.ch", researchDataFile)));
    if (filename.contains("/")) {
      researchDataFile.setRelativeLocation("/" + filename.substring(0, filename.lastIndexOf("/")));
    } else {
      researchDataFile.setRelativeLocation("/");
    }
    researchDataFile.setFileName(file.getFileName().toString());
    researchDataFile.setFileSize(FileTool.getSize(file));
    researchDataFile.setMimeType(FileTool.getContentType(file).toString());
    HashMap<String, Object> metadata = new HashMap<>();
    if (filename.endsWith("jpg") || filename.endsWith("jpeg") || filename.endsWith("png")) {
      researchDataFile.setAccessibleFrom(DataAccessibilityType.IIIF);
      metadata.put(HederaConstants.FILE_METADATA_WIDTH, 100L);
      metadata.put(HederaConstants.FILE_METADATA_HEIGHT, 120L);
      metadata.put(HederaConstants.FILE_WEB_URL, this.getUrl("https://hedera.unige.ch/iiif", researchDataFile));
    } else {
      researchDataFile.setAccessibleFrom(DataAccessibilityType.TEI);
      metadata.put(HederaConstants.FILE_WEB_URL, this.getUrl("https://hedera.unige.ch/access", researchDataFile));

    }
    researchDataFile.setMetadata(metadata);
    return researchDataFile;
  }

  private String getUrl(String rootUrl, ResearchDataFile researchDataFile) {
    return rootUrl
            + "/" + researchDataFile.getProject().getShortName()
            + "/" + researchDataFile.getResearchObjectType().getName()
            + "/" + researchDataFile.getFileName();
  }

}
