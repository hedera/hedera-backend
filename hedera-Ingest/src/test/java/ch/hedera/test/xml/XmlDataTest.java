/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Ingest - XmlDataTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.xml;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.XMLTool;

import ch.hedera.HederaConstants;

class XmlDataTest {
  private Path testFolder = Paths.get("src", "test", "resources").toAbsolutePath();

  @ParameterizedTest
  @ValueSource(strings = { "hedera-repository-1.0.xsd", "hedera-research-object-1.0.xsd" })
  void wellformedTest(String file) throws IOException {
    final String xml = this.getSchema(file);
    assertDoesNotThrow(() -> XMLTool.wellformed(xml));
  }

  @ParameterizedTest
  @ValueSource(strings = { "research-data-file.xml", "research-data-file-wo-rel-location.xml", "research-data-file-w-rel-location.xml" })
  void validTest(String file) throws IOException {
    final String schema = this.getSchema(HederaConstants.HEDERA_RESEARCH_OBJECT_SCHEMA_1);
    final Path filePath = this.getFilePath(this.testFolder, file);
    assertDoesNotThrow(() -> XMLTool.validate(schema, FileTool.toString(filePath)));
  }

  private String getSchema(String file) throws IOException {
    final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
    final org.springframework.core.io.Resource[] xsd = resolver
            .getResources("classpath*:/" + SolidifyConstants.SCHEMA_HOME + "/" + file);
    if (xsd.length > 0) {
      return FileTool.toString(xsd[0].getInputStream());
    }
    throw new IOException("No schema " + file);
  }

  private Path getFilePath(Path parentPath, String filename) {
    final Path file = parentPath.resolve(filename);
    assertNotNull(file);
    assertTrue(FileTool.checkFile(file));
    return file;
  }

}
