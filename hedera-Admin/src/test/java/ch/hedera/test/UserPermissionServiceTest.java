/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - UserPermissionServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test;

import static ch.hedera.HederaRestFields.APPLICATION_ROLE_FIELD;
import static ch.hedera.HederaRestFields.RES_ID_FIELD;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.hedera.business.UserService;
import ch.hedera.model.security.User;
import ch.hedera.service.security.UserPermissionService;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.model.SolidifyApplicationRole;

@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
public class UserPermissionServiceTest {

  private final static String USER_ID = "user";
  private final static String ADMIN_ID = "admin";
  private final static String ADMIN2_ID = "admin2";
  private final static String ROOT_ID = "root";
  private final static String ROOT2_ID = "root2";
  private final static String USER_ROLE_ID = "USER";
  private final static String ADMIN_ROLE_ID = "ADMIN";
  private final static String ROOT_ROLE_ID = "ROOT";

  private final User user = new User();
  private final User admin = new User();
  private final User admin2 = new User();
  private final User root = new User();
  private final User root2 = new User();

  private final Authentication authentication = Mockito.mock(Authentication.class);
  private final SecurityContext securityContext = Mockito.mock(SecurityContext.class);
  private final Collection<? extends GrantedAuthority> userAuthorithy = Arrays.asList(new SimpleGrantedAuthority(USER_ROLE_ID));
  private final Collection<? extends GrantedAuthority> adminAuthorithy = Arrays.asList(new SimpleGrantedAuthority(ADMIN_ROLE_ID));
  private final Collection<? extends GrantedAuthority> rootAuthorithy = Arrays.asList(new SimpleGrantedAuthority(ROOT_ROLE_ID));
  private final Map<String, Object> mapUserRole = Map.of(APPLICATION_ROLE_FIELD, Map.of(RES_ID_FIELD, USER_ROLE_ID));
  private final Map<String, Object> mapRootRole = Map.of(APPLICATION_ROLE_FIELD, Map.of(RES_ID_FIELD, ROOT_ROLE_ID));

  @Mock
  private UserService userService;

  private UserPermissionService userPermissionService;

  @BeforeAll
  void setUp() {
    this.userPermissionService = new UserPermissionService(this.userService);
    this.user.setResId(USER_ID);
    this.admin.setResId(ADMIN_ID);
    this.admin2.setResId(ADMIN2_ID);
    this.root.setResId(ROOT_ID);
    this.root2.setResId(ROOT2_ID);
    this.user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.USER));
    this.admin.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ADMIN));
    this.admin2.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ADMIN));
    this.root.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ROOT));
    this.root2.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ROOT));
    SecurityContextHolder.setContext(this.securityContext);
    Mockito.when(this.securityContext.getAuthentication()).thenReturn(this.authentication);
    when(this.userService.findOne(USER_ID)).thenReturn(this.user);
    when(this.userService.findOne(ADMIN_ID)).thenReturn(this.admin);
    when(this.userService.findOne(ADMIN2_ID)).thenReturn(this.admin2);
    when(this.userService.findOne(ROOT_ID)).thenReturn(this.root);
    when(this.userService.findOne(ROOT2_ID)).thenReturn(this.root2);
    when(this.userService.containsApplicationRoleId(mapUserRole)).thenReturn(true);
    when(this.userService.containsApplicationRoleId(mapRootRole)).thenReturn(true);
    when(this.userService.getApplicationRoleId(mapUserRole)).thenReturn(USER_ROLE_ID);
    when(this.userService.getApplicationRoleId(mapRootRole)).thenReturn(ROOT_ROLE_ID);
  }

  @Test
  void rootIsAllowedTest() {
    this.setRoot();
    assertTrue(this.userPermissionService.isAllowedToUpdate(ROOT_ID, Map.of()));
  }

  @Test
  void adminIsAllowedTest() {
    this.setAdmin();
    assertTrue(this.userPermissionService.isAllowedToUpdate(ROOT_ID, Map.of()));
  }

  @Test
  void userIsNotAllowedTest() {
    this.setUser();
    assertFalse(this.userPermissionService.isAllowedToUpdate(ROOT_ID, Map.of()));
  }

  @Test
  void rootIsAllowedToDowngradeRootTest() {
    this.setRoot();
    assertTrue(this.userPermissionService.isAllowedToUpdate(ROOT2_ID, this.mapUserRole));
  }

  @Test
  void rootIsNotAllowedToDowngradeHisRoleTest() {
    this.setRoot();
    assertFalse(this.userPermissionService.isAllowedToUpdate(ROOT_ID, this.mapUserRole));
  }

  @Test
  void adminIsNotAllowedToDowngradeRootTest() {
    this.setAdmin();
    assertFalse(this.userPermissionService.isAllowedToUpdate(ROOT_ID, this.mapUserRole));
  }

  @Test
  void rootIsAllowedToUpgradeAdminTest() {
    this.setRoot();
    assertTrue(this.userPermissionService.isAllowedToUpdate(ADMIN_ID, this.mapRootRole));
  }

  @Test
  void adminIsAllowedToDowngradeAdminTest() {
    this.setAdmin();
    assertTrue(this.userPermissionService.isAllowedToUpdate(ADMIN2_ID, this.mapUserRole));
  }

  @Test
  void adminIsNotAllowedToUpgradeAdminTest() {
    this.setAdmin();
    assertFalse(this.userPermissionService.isAllowedToUpdate(ADMIN2_ID, this.mapRootRole));
  }

  @Test
  void adminIsNotAllowedToDowngradeHisRoleTest() {
    this.setAdmin();
    assertFalse(this.userPermissionService.isAllowedToUpdate(ADMIN_ID, this.mapUserRole));
  }

  @Test
  void mapInsteadOfStringTest() {
    this.setAdmin();
    // This is not considered as a tentative of modifying the application role
    assertTrue(this.userPermissionService.isAllowedToUpdate(ADMIN_ID, Map.of(APPLICATION_ROLE_FIELD, Map.of(RES_ID_FIELD, Map.of()))));
  }

  @Test
  void stringInsteadOfMapTest() {
    this.setAdmin();
    // This is not considered as a tentative of modifying the application role
    assertTrue(this.userPermissionService.isAllowedToUpdate(ADMIN_ID, Map.of(APPLICATION_ROLE_FIELD, ROOT_ROLE_ID)));
  }

  @Test
  void nullApplicationRoleTest() {
    // Setting an applicationRole to null must be checked at the validation layer not the permission
    // layer
    this.setAdmin();
    final Map<String, Object> updateMap = new HashMap<>();
    final Map<String, Object> embeddedUpdateMap = new HashMap<>();
    updateMap.put(APPLICATION_ROLE_FIELD, embeddedUpdateMap);
    embeddedUpdateMap.put(RES_ID_FIELD, null);
    assertTrue(this.userPermissionService.isAllowedToUpdate(ADMIN_ID, updateMap));
  }

  private void setUser() {
    Mockito.doReturn(this.userAuthorithy).when(this.authentication).getAuthorities();
    Mockito.doReturn(this.user).when(this.userService).getCurrentUser();
  }

  private void setAdmin() {
    Mockito.doReturn(this.adminAuthorithy).when(this.authentication).getAuthorities();
    Mockito.doReturn(this.admin).when(this.userService).getCurrentUser();
  }

  private void setRoot() {
    Mockito.doReturn(this.rootAuthorithy).when(this.authentication).getAuthorities();
    Mockito.doReturn(this.root).when(this.userService).getCurrentUser();
  }
}
