/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - RdfTypeTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.util.FileTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.model.humanities.RdfFile;
import ch.hedera.util.RdfTool;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlQueryTool;

class RdfTypeTest {

  private static final Logger log = LoggerFactory.getLogger(RdfTypeTest.class);

  protected Path sourceFolder = Paths.get("src", "main", "resources").toAbsolutePath();

  @ParameterizedTest
  @MethodSource("ontologies")
  void listOntologiesTest(String ontoFile, RdfFormat ontoFormat) throws IOException {
    Path onto = this.getFile(ontoFile);
    Model model = RdfTool.getRdfModel(onto, ontoFormat);
    assertNotNull(model);
    List<String> rdfTypes = this.listRdfType(model);
    assertFalse(rdfTypes.isEmpty());
  }

  @ParameterizedTest
  @MethodSource("rdfTypes")
  void checkRdfTypeTest(String rdfType, String ontoBaseUri, String ontoFile, RdfFormat ontoFormat) throws IOException {
    Path onto = this.getFile(ontoFile);
    Model model = RdfTool.getRdfModel(onto, ontoFormat);
    assertNotNull(model);
    final Query classQuery = SparqlQueryTool.queryToGetNumberOfRdfTypes(ontoBaseUri + rdfType, SparqlQueryTool.ONTOLOGY_RDF_CLASS);
    final Query propertyQuery = SparqlQueryTool.queryToGetNumberOfRdfTypes(ontoBaseUri + rdfType, SparqlQueryTool.ONTOLOGY_RDF_PROPERTY);
    final int isClass = this.countTriple(model, classQuery);
    final int isProperty = this.countTriple(model, propertyQuery);
    assertEquals(1, isClass + isProperty);
  }

  @ParameterizedTest
  @MethodSource("rdfTypes")
  void checkRdfTypeWithOntologyTest(String rdfType, String ontoBaseUri, String ontoFile, RdfFormat ontoFormat) throws IOException {
    Path onto = this.getFile(ontoFile);
    Ontology ontology = this.getOntology(ontoBaseUri, ontoFormat, onto);
    assertTrue(RdfTool.isPartOfOntology(rdfType, ontology));
  }

  private Ontology getOntology(String ontoBaseUri, RdfFormat ontoFormat, Path onto) throws IOException {
    Ontology ontology = new Ontology();
    ontology.setName(onto.getFileName().toString());
    ontology.setVersion(ontology.getName());
    ontology.setBaseUri(URI.create(ontoBaseUri));
    ontology.setFormat(ontoFormat);
    RdfFile rdfFile = new RdfFile();
    rdfFile.setFileName(onto.getFileName().toString());
    rdfFile.setFileSize(FileTool.getSize(onto));
    rdfFile.setFileContent(FileTool.getInputStream(onto).readAllBytes());
    ontology.setOntologyFile(rdfFile);
    return ontology;
  }

  private int countTriple(Model model, Query query) {
    try (QueryExecution localExecution = QueryExecutionFactory.create(query, model)) {
      ResultSet resultSet = localExecution.execSelect();
      while (resultSet.hasNext()) {
        QuerySolution solution = resultSet.nextSolution();
        int count = solution.getLiteral(SparqlQueryTool.QUERY_VARIABLE).getInt();
        return count;
      }
    }
    return 0;
  }

  private List<String> listRdfType(Model model) {
    List<String> rdfTypeList = new ArrayList<>();
    final Query localQuery = SparqlQueryTool.queryToListRdfTypes(SparqlBuilderTool.VALUE_VARIABLE);
    try (QueryExecution localExecution = QueryExecutionFactory.create(localQuery, model)) {
      ResultSet resultSet = localExecution.execSelect();
      while (resultSet.hasNext()) {
        QuerySolution solution = resultSet.nextSolution();
        String type = solution.get(SparqlBuilderTool.VALUE_VARIABLE).toString();
        rdfTypeList.add(type);
        log.info("RDF type:{}", type);
        this.listSubjects(model, type);
      }
      return rdfTypeList;
    }
  }

  private void listSubjects(Model model, String rdfTypeUri) {
    final Query listQuery = SparqlQueryTool.queryToListSubjectsOfRdfType(SparqlBuilderTool.SUBJECT_VARIABLE, rdfTypeUri);

    try (QueryExecution localExecution = QueryExecutionFactory.create(listQuery, model)) {
      ResultSet resultSet = localExecution.execSelect();
      // For each subject
      while (resultSet.hasNext()) {
        QuerySolution solution = resultSet.nextSolution();
        String subject = solution.getResource(SparqlBuilderTool.SUBJECT_VARIABLE).toString();
        log.info("   RDF subject:{}", subject);
      }
    }
  }

  private Path getFile(String filename) {
    Path file = this.sourceFolder.resolve(filename);
    assertNotNull(file);
    assertTrue(FileTool.checkFile(file));
    return file;
  }

  private static Stream<Arguments> ontologies() {
    return Stream.of(
            Arguments.of("CIDOC_CRM_v7.1.2.rdf", RdfFormat.RDF_XML),
            Arguments.of("CRMdig_v3.2.1.rdfs", RdfFormat.RDF_XML),
            Arguments.of("EBUCore-1.10.rdf", RdfFormat.RDF_XML),
            Arguments.of("Exif-2003-12.rdf", RdfFormat.RDF_XML),
            Arguments.of("RiC-O_1-0.rdf", RdfFormat.RDF_XML));
  }

  private static Stream<Arguments> rdfTypes() {
    return Stream.of(
            Arguments.of(HederaConstants.RESEARCH_OBJECT_TYPE_RDF_TYPE_D1, HederaConstants.ONTOLOGY_CRM_DIG_BASE_URI, "CRMdig_v3.2.1.rdfs",
                    RdfFormat.RDF_XML),
            Arguments.of("resourceFilename", HederaConstants.ONTOLOGY_EBU_CORE_BASE_URI, "EBUCore-1.10.rdf", RdfFormat.RDF_XML),
            Arguments.of("hasMimeType", HederaConstants.ONTOLOGY_EBU_CORE_BASE_URI, "EBUCore-1.10.rdf", RdfFormat.RDF_XML),
            Arguments.of("fileSize", HederaConstants.ONTOLOGY_EBU_CORE_BASE_URI, "EBUCore-1.10.rdf", RdfFormat.RDF_XML),
            Arguments.of("height", HederaConstants.ONTOLOGY_EXIF_BASE_URI, "Exif-2003-12.rdf", RdfFormat.RDF_XML),
            Arguments.of("width", HederaConstants.ONTOLOGY_EXIF_BASE_URI, "Exif-2003-12.rdf", RdfFormat.RDF_XML),
            Arguments.of("Record", HederaConstants.ONTOLOGY_RIC_O_BASE_URI, "RiC-O_1-0.rdf", RdfFormat.RDF_XML));
  }
}
