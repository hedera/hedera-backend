/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - ProjectService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.util.Map;
import java.util.NoSuchElementException;

import org.apache.jena.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyUnmodifiableException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.service.CompositeResourceService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.config.HederaProperties.Storage;
import ch.hedera.controller.AdminController;
import ch.hedera.model.Access;
import ch.hedera.model.StorageType;
import ch.hedera.model.settings.License;
import ch.hedera.model.settings.Project;
import ch.hedera.model.triplestore.TriplestoreDataset;
import ch.hedera.repository.ProjectRepository;
import ch.hedera.rest.ResourceName;
import ch.hedera.service.rest.trusted.TrustedIdMappingRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedResearchObjectMetadataRemoteResourceService;
import ch.hedera.service.storage.ProjectStorage;
import ch.hedera.specification.ProjectSpecification;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlTool;

@Service
@ConditionalOnBean(AdminController.class)
public class ProjectService extends CompositeResourceService<Project> {
  private static final Logger logger = LoggerFactory.getLogger(ProjectService.class);

  private static final String PROJECT_SHORT_NAME = "shortName";

  private static final String WRONG_SHORT_NAME = "validation.project.shortName.wrong";
  private static final String CLOSING_DATE_SET_WHEN_NO_OPENING_DATE = "validation.dates.closingDateSetWhenNoOpeningDate";
  private static final String OPENING_DATE_NOT_BEFORE_CLOSING_DATE = "validation.dates.openingDateMustBeBeforeClosingDate";

  private final Storage storageConfig;

  private final LicenseService licenseService;

  private final TriplestoreDatasetService triplestoreDatasetService;

  private final ProjectRepository projectRepository;

  private final TrustedIdMappingRemoteResourceService idMappingRemoteResourceService;

  private final TrustedResearchObjectMetadataRemoteResourceService indexingService;

  private final String defaultCopyrightHolder;

  public ProjectService(
          HederaProperties config,
          LicenseService licenseService,
          TriplestoreDatasetService triplestoreDatasetService,
          ProjectRepository projectRepository,
          TrustedIdMappingRemoteResourceService idMappingRemoteResourceService,
          TrustedResearchObjectMetadataRemoteResourceService indexingService) {
    this.storageConfig = config.getStorage();
    this.licenseService = licenseService;
    this.triplestoreDatasetService = triplestoreDatasetService;
    this.projectRepository = projectRepository;
    this.idMappingRemoteResourceService = idMappingRemoteResourceService;
    this.indexingService = indexingService;
    this.defaultCopyrightHolder = config.getParameters().getDefaultCopyrightHolder();
  }

  @Override
  public Project save(Project project) {
    // Create default license
    License defaultLicense = project.getDefaultLicense();
    if (defaultLicense != null) {
      if (defaultLicense.getResId() != null) {
        defaultLicense = this.licenseService.findOne(defaultLicense.getResId());
        project.setDefaultLicense(defaultLicense);
      } else {
        throw new SolidifyValidationException(new ValidationError("Default license resId must not be null"));
      }
    }

    // Add default copyright holder
    if (project.getCopyrightHolder() == null) {
      project.setCopyrightHolder(defaultCopyrightHolder);
    }

    final Project savedProject = super.save(project);

    // Create default TriplestoreDataset
    try {
      if (this.triplestoreDatasetService.findByName(project.getShortName()).isEmpty()) {
        final TriplestoreDataset triplestoreDataset = new TriplestoreDataset();
        triplestoreDataset.setProject(savedProject);
        triplestoreDataset.setResId(project.getResId());
        triplestoreDataset.setName(project.getShortName());
        triplestoreDataset.setDescription("Default triplestore dataset for project " + project.getName());
        triplestoreDataset.setAccess(Access.PUBLIC);
        this.triplestoreDatasetService.save(triplestoreDataset);
      }
    } catch (Exception e) {
      throw new SolidifyProcessingException("Cannot create default triplestore dataset", e);
    }

    return savedProject;
  }

  @Override
  public void delete(String id) {
    logger.info("Start deleting the project : {}", id);
    Project project = this.findOne(id);
    try {
      // Delete triplestore datasets of the project
      for (TriplestoreDataset triplestoreDataset : this.triplestoreDatasetService.findByProject(project)) {
        this.triplestoreDatasetService.delete(triplestoreDataset.getResId());
      }
      // Delete index metadata
      this.indexingService.deleteMetadataByProject(ResourceName.PUBLIC_INDEX, project.getShortName());
      this.indexingService.deleteMetadataByProject(ResourceName.PRIVATE_INDEX, project.getShortName());
      // Delete project storage
      final ProjectStorage projectStorage = ProjectStorage.getProjectStorage(this.storageConfig, project);
      projectStorage.purgeStorageLocation();
      // Delete ID Mapping
      this.idMappingRemoteResourceService.purge(project.getShortName());
    } catch (Exception e) {
      logger.error("Cannot delete triplestore dataset or S3 bucket for project storage: {}", id);
      throw new SolidifyProcessingException("Cannot delete triplestore datasets or S3 bucket for project storage", e);
    }
    // Delete project
    super.delete(id);
  }

  @Override
  public void patchResource(Project project, Map<String, Object> updateMap) {
    if (Boolean.TRUE.equals(project.hasData() && updateMap.containsKey(PROJECT_SHORT_NAME))
            && !project.getShortName().equals(updateMap.get(PROJECT_SHORT_NAME))) {
      throw new SolidifyUnmodifiableException(
              this.messageService.get("validation.resource.unmodifiable.reason",
                      new Object[] { project.getClass().getSimpleName() + ": " + project.getProjectId(), PROJECT_SHORT_NAME }));
    }
    super.patchResource(project, updateMap);
  }

  @Override
  public void validateItemSpecificRules(Project item, BindingResult errors) {

    /*
     * Check short name is different form 'manifest'
     */
    if (ResourceName.MANIFEST.equals(item.getShortName())
            || ResourceName.COLLECTION.equals(item.getShortName())) {

      errors.addError(
              new FieldError(item.getClass().getSimpleName(), PROJECT_SHORT_NAME,
                      this.messageService.get(WRONG_SHORT_NAME, new Object[] { item.getShortName() })));
    }

    /*
     * A closing date can not be set if the opening date is not set
     */
    if (item.getOpeningDate() == null && item.getClosingDate() != null) {
      errors.addError(
              new FieldError(item.getClass().getSimpleName(), "closingDate", this.messageService.get(CLOSING_DATE_SET_WHEN_NO_OPENING_DATE)));
    }

    /*
     * If opening and closing dates are set, the opening date must be before the closing date
     */
    if (item.getOpeningDate() != null && item.getClosingDate() != null && item.getOpeningDate().isAfter(item.getClosingDate())) {

      errors.addError(new FieldError(item.getClass().getSimpleName(), "closingDate",
              this.messageService.get(OPENING_DATE_NOT_BEFORE_CLOSING_DATE, new Object[] { item.getOpeningDate(), item.getClosingDate() })));
    }

    /*
     * If we provide iiif sparql manifest , check that the query is valid.
     */
    if (!StringTool.isNullOrEmpty(item.getIiifManifestSparqlQuery())) {
      try {
        // Check syntax
        SparqlTool.validateSparqlSyntax(item.getIiifManifestSparqlQuery());
        // Check mandatory variables
        final Query iiifManifestQuery = SparqlBuilderTool.createQuery(item.getIiifManifestSparqlQuery());
        for (String variableName : HederaConstants.getIIIFManifestMandatoryVariables()) {
          final String sparqlVariable = "?" + variableName;
          if (!SparqlTool.queryContainsVariable(iiifManifestQuery, sparqlVariable)) {
            errors.addError(
                    new FieldError(item.getClass().getSimpleName(), "iiifManifestSparqlQuery",
                            this.messageService.get("validation.sparql.query.mustContainVariable",
                                    new Object[] { sparqlVariable })));
          }
        }
      } catch (SolidifyRuntimeException e) {
        errors.addError(
                new FieldError(item.getClass().getSimpleName(), "iiifManifestSparqlQuery",
                        this.messageService.get("validation.sparql.query.invalid",
                                new Object[] { e.getMessage() })));
      }
    }

    /*
     * Check that the storage type is enable.
     */

    if (!this.checkStorage(item.getStorageType())) {
      errors.addError(
              new FieldError(item.getClass().getSimpleName(), "storageType", this.messageService.get("validation.project.storageType",
                      new Object[] { item.getStorageType() })));
    }
  }

  @Override
  public ProjectSpecification getSpecification(Project resource) {
    return new ProjectSpecification(resource);
  }

  public Project getByName(String name) {
    Project item = ((ProjectRepository) this.itemRepository).findByName(name)
            .orElseThrow(() -> new NoSuchElementException("No resource with name " + name));
    this.afterFind(item);
    return item;
  }

  public Project getByShortName(String shortName) {
    Project item = ((ProjectRepository) this.itemRepository).findByShortName(shortName)
            .orElseThrow(() -> new NoSuchElementException("No resource with short name " + shortName));
    this.afterFind(item);
    return item;
  }

  public Page<Project> findOpenProjectsByPersonIdAndProjectNameAddingAllPublic(String personId, Project project, Pageable pageable) {
    return this.projectRepository.findAll(ProjectSpecification.getOpenProjectsByPersonIdAndProjectNameAddingAllPublic(personId, project),
            pageable);
  }

  public Page<Project> findOpenProjectsByPersonIdAndProjectName(String personId, Project project, Pageable pageable) {
    return this.projectRepository.findAll(ProjectSpecification.getOpenProjectsByPersonIdAndProjectName(personId, project), pageable);
  }

  public Page<Project> findProjectsByPersonIdAndProjectName(String personId, Project project, Pageable pageable) {
    return this.projectRepository.findAll(ProjectSpecification.getProjectsByPersonIdAndProjectName(personId, project), pageable);
  }

  public Page<Project> findProjectsByPersonIdAndProjectNameAddingAllPublic(String personId, Project project,
          Pageable pageable) {
    return this.projectRepository.findAll(ProjectSpecification.getProjectsByPersonIdAndProjectNameAddingAllPublic(personId, project), pageable);
  }

  private boolean checkStorage(StorageType storageType) {
    try {
      switch (storageType) {
        case FILE -> this.storageConfig.getFileStorage().getUrl();
        case OBJECT -> this.storageConfig.getObjectStorage().getUrl();
        default -> throw new SolidifyRuntimeException("Unknown storage type");
      }
      return true;
    } catch (SolidifyRuntimeException e) {
      return false;
    }
  }
}
