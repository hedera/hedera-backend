/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - PersonService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.PersonWithOrcid;
import ch.unige.solidify.service.CompositeResourceService;
import ch.unige.solidify.service.PersonWithOrcidService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ValidationTool;

import ch.hedera.controller.AdminController;
import ch.hedera.model.security.Role;
import ch.hedera.model.security.User;
import ch.hedera.model.settings.Person;
import ch.hedera.repository.PersonRepository;
import ch.hedera.specification.PersonSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class PersonService extends CompositeResourceService<Person> implements PersonWithOrcidService {

  private final PersonRepository personRepository;
  private final UserService userService;

  public PersonService(UserService userService, PersonRepository personRepository) {
    this.userService = userService;
    this.personRepository = personRepository;
  }

  public Page<Person> searchAllWithUser(String search, Pageable pageable) {
    return this.personRepository.searchAllWithUser(search, pageable);
  }

  public Person getByOrcid(String orcid) {
    Person item = this.personRepository.findByOrcid(orcid)
            .orElseThrow(() -> new NoSuchElementException("No resource with orcid " + orcid));
    this.afterFind(item);
    return item;
  }

  @Override
  public PersonSpecification getSpecification(Person resource) {
    return new PersonSpecification(resource);
  }

  @Override
  public PersonWithOrcid getPerson(Authentication authentication) {
    final String authUserId = authentication.getName();
    final User user = this.userService.findByExternalUid(authUserId);
    if (user != null) {
      return user.getPerson();
    } else {
      throw new SolidifyRuntimeException("User referenced by " + authUserId + " is not in database");
    }
  }

  @Override
  public void validateItemSpecificRules(Person person, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(person.getOrcid()) && !ValidationTool.isValidOrcid(person.getOrcid())) {
      errors.addError(new FieldError(person.getClass().getSimpleName(), "orcid",
              this.messageService.get("validation.orcId.invalid", new Object[] { person.getOrcid() })));
    }
  }

  @Override
  public PersonWithOrcid save(PersonWithOrcid person) {
    if (person instanceof Person p) {
      return super.save(p);
    } else {
      throw new SolidifyRuntimeException("PersonService cannot save instance of " + person.getClass());
    }
  }

  public List<Person> findByProjectAndRoleLevelHierarchy(String projectId, Role role) {
    final PersonSpecification spec = this.getSpecification(new Person());
    spec.setProjectResId(projectId);
    spec.setRole(role);

    return this.findAll(spec, Pageable.unpaged()).toList();
  }

}
