/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - TriplestoreDatasetService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.ResourceService;

import ch.hedera.controller.AdminController;
import ch.hedera.model.settings.Project;
import ch.hedera.model.triplestore.TriplestoreDataset;
import ch.hedera.repository.TriplestoreDatasetRepository;
import ch.hedera.service.triplestore.TriplestoreSettingsService;
import ch.hedera.specification.TriplestoreDatasetSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class TriplestoreDatasetService extends ResourceService<TriplestoreDataset> {

  private final TriplestoreSettingsService triplestoreSettingsService;

  public TriplestoreDatasetService(TriplestoreSettingsService triplestoreSettingsService) {
    super();
    this.triplestoreSettingsService = triplestoreSettingsService;
  }

  public List<TriplestoreDataset> findByProject(Project project) {
    return ((TriplestoreDatasetRepository) this.itemRepository).findByProject(project);
  }

  public Optional<TriplestoreDataset> findByName(String name) {
    return ((TriplestoreDatasetRepository) this.itemRepository).findByName(name);
  }

  @Override
  public TriplestoreDataset save(TriplestoreDataset item) {
    item = super.save(item);
    this.triplestoreSettingsService.createDatasetIfNotExists(item.getName());
    return item;
  }

  @Override
  public void delete(String id) {
    TriplestoreDataset triplestoreDataset = this.findOne(id);
    this.triplestoreSettingsService.deleteDatasetIfExists(triplestoreDataset.getName());
    this.itemRepository.deleteById(id);
  }

  @Override
  public TriplestoreDatasetSpecification getSpecification(TriplestoreDataset resource) {
    return new TriplestoreDatasetSpecification(resource);
  }

}
