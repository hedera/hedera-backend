/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - ResearchObjectTypeService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.controller.AdminController;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.repository.ResearchObjectTypeRepository;
import ch.hedera.specification.ResearchObjectTypeSpecification;
import ch.hedera.util.RdfTool;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlTool;

@Service
@ConditionalOnBean(AdminController.class)
public class ResearchObjectTypeService extends ResourceService<ResearchObjectType> {

  private static final Logger log = LoggerFactory.getLogger(ResearchObjectTypeService.class);

  private OntologyService ontologyService;

  public ResearchObjectTypeService(OntologyService ontologyService) {
    this.ontologyService = ontologyService;
  }

  @Override
  public ResearchObjectTypeSpecification getSpecification(ResearchObjectType resource) {
    return new ResearchObjectTypeSpecification(resource);
  }

  @Override
  protected void validateItemSpecificRules(ResearchObjectType item, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(item.getRdfType())) {
      Ontology ontology = this.ontologyService.findOne(item.getOntology().getResId());
      if (!RdfTool.isPartOfOntology(item.getRdfType(), ontology)) {
        errors.addError(
                new FieldError(item.getClass().getSimpleName(), "rdfType",
                        this.messageService.get("validation.researchObjectType.error.notInOntology",
                                new Object[] { item.getRdfType(), ontology.getName(), ontology.getVersion() })));
      }
    }
    if (!StringTool.isNullOrEmpty(item.getSparqlListQuery())) {
      try {
        SparqlTool.validateSparqlSyntax(item.getSparqlListQuery());
      } catch (SolidifyCheckingException e) {
        errors.addError(
                new FieldError(item.getClass().getSimpleName(), "sparqlListQuery", this.messageService.get("validation.sparql.query.invalid",
                        new Object[] { e.getMessage() })));
      }
    }
    String sparqlDetailQuery = item.getSparqlDetailQuery();
    if (!StringTool.isNullOrEmpty(sparqlDetailQuery)) {
      try {
        SparqlTool.validateSparqlSyntax(sparqlDetailQuery);

        if (!SparqlTool.queryContainsVariable(SparqlBuilderTool.createQuery(sparqlDetailQuery),
                SparqlBuilderTool.SUBJECT_FULL_NAME_VARIABLE)) {
          errors.addError(new FieldError(item.getClass().getSimpleName(), "sparqlDetailQuery",
                  this.messageService.get("validation.sparql.query.mustContainVariable",
                          new Object[] { SparqlBuilderTool.SUBJECT_FULL_NAME_VARIABLE })));
        }

      } catch (SolidifyCheckingException e) {
        errors.addError(
                new FieldError(item.getClass().getSimpleName(), "sparqlDetailQuery", this.messageService.get("validation.sparql.query.invalid",
                        new Object[] { e.getMessage() })));
      }
    }

  }

  public ResearchObjectType getByNameAndOntology(Ontology ontology, String name) {
    ResearchObjectType item = ((ResearchObjectTypeRepository) this.itemRepository).findByOntologyAndName(ontology, name)
            .orElseThrow(() -> new NoSuchElementException(
                    "No resource with name " + name + " for ontology " + ontology.getName() + " (" + ontology.getVersion() + ")"));
    this.afterFind(item);
    return item;
  }

  public void initOptionalData(Ontology cidocCrmOno, Ontology crmDigOnto) {
    try {
      this.createResearchObjectType(HederaConstants.RESEARCH_OBJECT_TYPE_E24, "human-made-thing", "E24_Physical_Human-Made_Thing",
              "Physical Human-Made Thing", cidocCrmOno);
      this.createResearchObjectType(HederaConstants.RESEARCH_OBJECT_TYPE_E22, "human-made-object", "E22_Human-Made_Object", "Human-Made Object",
              cidocCrmOno);
      this.createResearchObjectType(HederaConstants.RESEARCH_OBJECT_TYPE_E41, "appellation", "E41_Appellation", "Appellation", cidocCrmOno);
      this.createResearchObjectType(HederaConstants.RESEARCH_OBJECT_TYPE_E53, "location", "E53_Place", "Place / Location", cidocCrmOno);
      this.createResearchObjectType(HederaConstants.RESEARCH_OBJECT_TYPE_E39, "actor", "E39_Actor", "Actor / Organization", cidocCrmOno);
      this.createResearchObjectType(HederaConstants.RESEARCH_OBJECT_TYPE_E21, "person", "E21_Person", "Person", cidocCrmOno);
      this.createResearchObjectType(HederaConstants.RESEARCH_OBJECT_TYPE_D1, "digitalobject", HederaConstants.RESEARCH_OBJECT_TYPE_RDF_TYPE_D1,
              "Identifiable Immaterial Items", crmDigOnto);
    } catch (Exception e) {
      log.warn("Error when creating an ontology: {}", e.getMessage());
    }
  }

  private void createResearchObjectType(String id, String name, String rdfType, String description, Ontology ontology) {
    if (((ResearchObjectTypeRepository) this.itemRepository).findByOntologyAndName(ontology, name).isEmpty()) {
      ResearchObjectType rot = new ResearchObjectType();
      rot.setResId(id);
      rot.setName(name);
      rot.setRdfType(rdfType);
      rot.setDescription(description);
      if (ontology != null) {
        rot.setOntology(ontology);
      }
      this.save(rot);
      log.info("Research Object Type'{}' created", name);
    }
  }
}
