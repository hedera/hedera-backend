/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - IIIFCollectionSettingsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.util.Optional;

import org.apache.jena.query.Query;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.controller.AdminController;
import ch.hedera.model.settings.IIIFCollectionSettings;
import ch.hedera.repository.IIIFCollectionSettingsRepository;
import ch.hedera.specification.IIIFCollectionSettingsSpecification;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlTool;

@Service
@ConditionalOnBean(AdminController.class)
public class IIIFCollectionSettingsService extends ResourceService<IIIFCollectionSettings> {

  private final IIIFCollectionSettingsRepository iiifCollectionSettingsRepository;

  public IIIFCollectionSettingsService(IIIFCollectionSettingsRepository iiifCollectionSettingsRepository) {
    this.iiifCollectionSettingsRepository = iiifCollectionSettingsRepository;
  }

  public Optional<IIIFCollectionSettings> findByName(String name) {
    return this.iiifCollectionSettingsRepository.findByName(name);
  }

  @Override
  public void validateItemSpecificRules(IIIFCollectionSettings item, BindingResult errors) {

    /*
     * Check short name is different form 'manifest'
     */
    if (HederaConstants.IIIF_PROJECT_DEFAULT_COLLECTION.equals(item.getName())) {

      errors.addError(
              new FieldError(item.getClass().getSimpleName(), "name",
                      this.messageService.get("validation.iiifCollection.name.wrong", new Object[] { item.getName() })));
    }

    /*
     * If we provide iiif sparql manifest , check that the query is valid.
     */
    if (!StringTool.isNullOrEmpty(item.getIiifCollectionSparqlQuery())) {
      try {
        // Check syntax
        SparqlTool.validateSparqlSyntax(item.getIiifCollectionSparqlQuery());
        // Check mandatory variables
        final Query iiifManifestQuery = SparqlBuilderTool.createQuery(item.getIiifCollectionSparqlQuery());
        for (String variableName : HederaConstants.getIIIFManifestMandatoryVariables()) {
          final String sparqlVariable = "?" + variableName;
          if (!SparqlTool.queryContainsVariable(iiifManifestQuery, sparqlVariable)) {
            errors.addError(
                    new FieldError(item.getClass().getSimpleName(), "iiifManifestSparqlQuery",
                            this.messageService.get("validation.sparql.query.mustContainVariable",
                                    new Object[] { sparqlVariable })));
          }
        }
      } catch (SolidifyRuntimeException e) {
        errors.addError(
                new FieldError(item.getClass().getSimpleName(), "iiifManifestSparqlQuery",
                        this.messageService.get("validation.sparql.query.invalid",
                                new Object[] { e.getMessage() })));
      }

    }
  }

  @Override
  public SolidifySpecification<IIIFCollectionSettings> getSpecification(IIIFCollectionSettings resource) {
    return new IIIFCollectionSettingsSpecification(resource);
  }
}
