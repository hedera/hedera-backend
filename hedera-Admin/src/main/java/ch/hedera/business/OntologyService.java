/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - OntologyService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.service.ResourceService;

import ch.hedera.HederaConstants;
import ch.hedera.controller.AdminController;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.repository.OntologyRepository;
import ch.hedera.specification.OntologySpecification;
import ch.hedera.util.RdfTool;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlQueryTool;

@Service
@ConditionalOnBean(AdminController.class)
public class OntologyService extends ResourceService<Ontology> {

  private static final Logger log = LoggerFactory.getLogger(OntologyService.class);

  private static final String NAME_FORMAT_VERSION_ALREADY_EXIST = "validation.ontology.error.nameFormatAndVersionAlreadyExist";

  private static final String WRONG_BASE_URI = "validation.ontology.error.wrongBaseUri";

  private final OntologyRepository ontologyRepository;

  public OntologyService(OntologyRepository ontologyRepository) {
    this.ontologyRepository = ontologyRepository;
  }

  @Override
  public OntologySpecification getSpecification(Ontology resource) {
    return new OntologySpecification(resource);
  }

  @Override
  public void validateItemSpecificRules(Ontology ontology, BindingResult errors) {
    // Check that there is no another ontology with same name, version and format
    Ontology existingOntology = this.findByNameAndVersion(ontology.getName(), ontology.getVersion());
    if (existingOntology != null && !ontology.getResId().equals(existingOntology.getResId())) {
      errors.addError(new FieldError(ontology.getClass().getSimpleName(), "name",
              this.messageService.get(NAME_FORMAT_VERSION_ALREADY_EXIST,
                      new Object[] { ontology.getName(), ontology.getFormat(), ontology.getVersion() })));
    }
    // Check Base Uri
    if (ontology.getBaseUri() != null
            && !ontology.getBaseUri().toString().endsWith("/")
            && !ontology.getBaseUri().toString().endsWith("#")) {
      errors.addError(new FieldError(ontology.getClass().getSimpleName(), "baseUri",
              this.messageService.get(WRONG_BASE_URI)));
    }
    // Check syntax
    if (ontology.getOntologyFile() != null) {
      try {
        RdfTool.wellformed(new ByteArrayInputStream(ontology.getOntologyFile().getFileContent()), ontology.getFormat());
      } catch (SolidifyCheckingException e) {
        errors.addError(
                new FieldError(ontology.getClass().getSimpleName(), "ontologyFile", this.messageService.get("validation.ontology.check.syntax",
                        new Object[] { ontology.getOntologyFile().getFileName(), e.getMessage() })));
      }
    }
  }

  public List<String> listOntologyFormats() {
    final List<String> list = new ArrayList<>();
    for (RdfFormat of : RdfFormat.values()) {
      list.add(of.getName());
    }
    return list;
  }

  public Ontology findByNameAndVersion(String name, String version) {
    return ((OntologyRepository) this.itemRepository).findByNameAndVersion(name, version);
  }

  public List<String> listRdfClasses(String ontologyId) {
    final Ontology ontology = this.findOne(ontologyId);
    return this.listRdfType(ontology, SparqlQueryTool.queryToListRdfClasses(ontology.getBaseUri().toString(), SparqlBuilderTool.VALUE_VARIABLE));
  }

  public List<String> listRdfProperties(String ontologyId) {
    final Ontology ontology = this.findOne(ontologyId);
    return this.listRdfType(ontology,
            SparqlQueryTool.queryToListRdfProperties(ontology.getBaseUri().toString(), SparqlBuilderTool.VALUE_VARIABLE));
  }

  private List<String> listRdfType(Ontology ontology, Query rdfTypeQuery) {
    final Model model = this.loadOntologyDefinition(ontology);
    final List<String> rdfTypeList = new ArrayList<>();
    try (QueryExecution localExecution = QueryExecutionFactory.create(rdfTypeQuery, model)) {
      ResultSet resultSet = localExecution.execSelect();
      while (resultSet.hasNext()) {
        QuerySolution solution = resultSet.nextSolution();
        rdfTypeList.add(solution.get(SparqlBuilderTool.VALUE_VARIABLE).toString());
      }
    }
    return rdfTypeList;
  }

  public String isClassOrProperty(String ontologyId, String rdfType) {
    final Ontology ontology = this.findOne(ontologyId);
    final Model model = this.loadOntologyDefinition(ontology);
    final String rdfTypeUri = ontology.getBaseUri().toString() + rdfType;
    if (RdfTool.isRdfClass(model, rdfTypeUri)) {
      return HederaConstants.ONTOLOGY_RDF_CLASS;
    }
    if (RdfTool.isRdfProperty(model, rdfTypeUri)) {
      return HederaConstants.ONTOLOGY_RDF_PROPERTY;
    }
    return null;
  }

  private Model loadOntologyDefinition(Ontology ontology) {
    return RdfTool.loadModel(new ByteArrayInputStream(ontology.getResourceFile().getFileContent()), ontology.getFormat());
  }

  public void initOptionalData() {
    try {
      this.createOntology(
              HederaConstants.ONTOLOGY_CIDOC_CRM,
              "5.0.4",
              "CIDOC CRM v5.0.4 RDFS Ontology",
              HederaConstants.ONTOLOGY_CIDOC_CRM_BASE_URI,
              RdfFormat.RDF_XML,
              "https://www.cidoc-crm.org/Version/version-5.0.4",
              "CIDOC_CRM_v5.0.4.rdfs",
              MediaType.APPLICATION_XML.toString());
      this.createOntology(
              HederaConstants.ONTOLOGY_CIDOC_CRM,
              "6.2.1",
              "CIDOC CRM v6.2.1 RDFS Ontology",
              HederaConstants.ONTOLOGY_CIDOC_CRM_BASE_URI,
              RdfFormat.RDF_XML,
              "https://www.cidoc-crm.org/Version/version-6.2.1",
              "CIDOC_CRM_v6.2.1.rdf",
              MediaType.APPLICATION_XML.toString());
      this.createOntology(
              HederaConstants.ONTOLOGY_CIDOC_CRM,
              "7.1.2",
              "CIDOC CRM v7.1.2 RDFS Ontology",
              HederaConstants.ONTOLOGY_CIDOC_CRM_BASE_URI,
              RdfFormat.RDF_XML,
              "https://www.cidoc-crm.org/Version/version-7.1.2",
              "CIDOC_CRM_v7.1.2.rdf",
              MediaType.APPLICATION_XML.toString());
      this.createOntology(
              HederaConstants.ONTOLOGY_CRM_DIG,
              "3.2.1",
              "CRMdig v3.2.1 RDFS Ontology",
              "http://www.ics.forth.gr/isl/CRMdig/",
              RdfFormat.RDF_XML,
              HederaConstants.ONTOLOGY_CRM_DIG_BASE_URI,
              "CRMdig_v3.2.1.rdfs",
              MediaType.APPLICATION_XML.toString());
      this.createOntology(
              HederaConstants.ONTOLOGY_RIC_O,
              "1.0",
              "RiC-O v1.0 OWL Ontology",
              HederaConstants.ONTOLOGY_RIC_O_BASE_URI,
              RdfFormat.RDF_XML,
              "https://github.com/ICA-EGAD/RiC-O/tree/v1.0",
              "RiC-O_1-0.rdf",
              MediaType.APPLICATION_XML.toString());
      this.createOntology(
              HederaConstants.ONTOLOGY_EXIF,
              "2003-12",
              "Exif Ontology",
              HederaConstants.ONTOLOGY_EXIF_BASE_URI,
              RdfFormat.RDF_XML,
              "https://www.w3.org/2003/12/exif",
              "Exif-2003-12.rdf",
              MediaType.APPLICATION_XML.toString());
      this.createOntology(
              HederaConstants.ONTOLOGY_EBU_CORE,
              "1.10",
              "EBU Core 1.10 Ontology",
              HederaConstants.ONTOLOGY_EBU_CORE_BASE_URI,
              RdfFormat.RDF_XML,
              "https://www.ebu.ch/metadata/ontologies/ebucore/",
              "EBUCore-1.10.rdf",
              MediaType.APPLICATION_XML.toString());
    } catch (IOException | URISyntaxException e) {
      log.warn("Error when creating an ontology: {}", e.getMessage());
    }
  }

  private void createOntology(String name, String version, String description, String baseUri, RdfFormat format, String url,
          String filename, String mimetype)
          throws IOException, URISyntaxException {
    if (this.ontologyRepository.findByNameAndVersion(name, version) == null) {
      Ontology ontology = new Ontology();
      ontology.setName(name);
      ontology.setDescription(description);
      ontology.setFormat(format);
      ontology.setUrl(new URL(url));
      ontology.setVersion(version);
      ontology.setBaseUri(new URI(baseUri));

      // Add file
      ResourceFile resourceFile = ((ResourceFileInterface) ontology).setNewResourceFile();
      resourceFile.getCreation().setWhen(OffsetDateTime.now());

      // read file
      byte[] fileContent = new ClassPathResource(filename).getInputStream().readAllBytes();
      resourceFile.setFileContent(fileContent);
      resourceFile.setFileSize((long) fileContent.length);
      resourceFile.setFileName(filename);
      resourceFile.setMimeType(mimetype);

      this.save(ontology);
      log.info("Ontology '{}' created", name);
    }
  }

}
