/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - ProjectPersonService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.service.JoinResource3TiersService;
import ch.unige.solidify.specification.Join3TiersSpecification;
import ch.unige.solidify.specification.SolidifySpecification;

import ch.hedera.controller.AdminController;
import ch.hedera.model.display.PersonRoleListDTO;
import ch.hedera.model.display.ProjectPersonRoleDTO;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ProjectPersonRole;
import ch.hedera.specification.PersonSpecification;
import ch.hedera.specification.ProjectPersonRoleSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class ProjectPersonService extends JoinResource3TiersService<Project, Person, Role, ProjectPersonRole> {

  @Override
  public Join3TiersSpecification<ProjectPersonRole> getJoinSpecification(ProjectPersonRole joinResource) {
    return new ProjectPersonRoleSpecification(joinResource);
  }

  @Override
  protected SolidifySpecification<Person> getChildSpecification(Person person) {
    return new PersonSpecification(person);
  }

  @Override
  protected String getChildToRelationPropertyName() {
    return ProjectPersonRole.PERSON_RELATION_PROPERTY_NAME;
  }

  @Override
  public Role getEmptyGrandChildResourceObject() {
    return new Role();
  }

  @Override
  public void setGrandChildResource(ProjectPersonRole joinResource, Role role) {
    joinResource.setRole(role);
  }

  @Override
  public Role getGrandChildResource(ProjectPersonRole joinResource) {
    return joinResource.getRole();
  }

  @Override
  public ProjectPersonRole getEmptyJoinResourceObject() {
    return new ProjectPersonRole();
  }

  @Override
  public Project getEmptyParentResourceObject() {
    return new Project();
  }

  @Override
  public Person getEmptyChildResourceObject() {
    return new Person();
  }

  @Override
  public void setParentResource(ProjectPersonRole joinResource, Project project) {
    joinResource.setProject(project);
  }

  @Override
  public void setChildResource(ProjectPersonRole joinResource, Person person) {
    joinResource.setPerson(person);
  }

  @Override
  public Relation3TiersChildDTO getChildDTO(Person person, List<ProjectPersonRole> joinResources) {
    List<ProjectPersonRoleDTO> list = new ArrayList<>();
    for (ProjectPersonRole projectPersonRole : joinResources) {
      list.add(this.getGrandChildDTO(projectPersonRole));
    }
    return new PersonRoleListDTO(person, list);
  }

  @Override
  public ProjectPersonRoleDTO getGrandChildDTO(ProjectPersonRole joinResource) {
    return new ProjectPersonRoleDTO(joinResource);
  }

  @Override
  public List<String> getDefaultGrandChildIds() {
    return Arrays.asList(Role.VISITOR.getResId());
  }

}
