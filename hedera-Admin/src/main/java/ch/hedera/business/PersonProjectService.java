/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - PersonProjectService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.hedera.controller.AdminController;
import ch.hedera.model.display.ProjectRoleDTO;
import ch.hedera.model.display.ProjectRoleListDTO;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ProjectPersonRole;
import ch.hedera.specification.PersonProjectRoleSpecification;
import ch.hedera.specification.ProjectSpecification;

import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.service.JoinResource3TiersService;
import ch.unige.solidify.specification.Join3TiersSpecification;
import ch.unige.solidify.specification.SolidifySpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class PersonProjectService extends JoinResource3TiersService<Person, Project, Role, ProjectPersonRole> {

  @Override
  public Join3TiersSpecification<ProjectPersonRole> getJoinSpecification(ProjectPersonRole joinResource) {
    return new PersonProjectRoleSpecification(joinResource);
  }

  @Override
  protected SolidifySpecification<Project> getChildSpecification(Project project) {
    return new ProjectSpecification(project);
  }

  @Override
  protected String getChildToRelationPropertyName() {
    return ProjectPersonRole.PROJECT_RELATION_PROPERTY_NAME;
  }

  @Override
  public Role getEmptyGrandChildResourceObject() {
    return new Role();
  }

  @Override
  public void setGrandChildResource(ProjectPersonRole joinResource, Role role) {
    joinResource.setRole(role);
  }

  @Override
  public Role getGrandChildResource(ProjectPersonRole joinResource) {
    return joinResource.getRole();
  }

  @Override
  public ProjectPersonRole getEmptyJoinResourceObject() {
    return new ProjectPersonRole();
  }

  @Override
  public Person getEmptyParentResourceObject() {
    return new Person();
  }

  @Override
  public Project getEmptyChildResourceObject() {
    return new Project();
  }

  @Override
  public void setParentResource(ProjectPersonRole joinResource, Person person) {
    joinResource.setPerson(person);
  }

  @Override
  public void setChildResource(ProjectPersonRole joinResource, Project project) {
    joinResource.setProject(project);
  }

  @Override
  public Relation3TiersChildDTO getChildDTO(Project project, List<ProjectPersonRole> joinResources) {
    List<ProjectRoleDTO> list = new ArrayList<>();
    for (ProjectPersonRole projectPersonRole : joinResources) {
      list.add(this.getGrandChildDTO(projectPersonRole));
    }
    return new ProjectRoleListDTO(project, list);
  }

  @Override
  public ProjectRoleDTO getGrandChildDTO(ProjectPersonRole joinResource) {
    return new ProjectRoleDTO(joinResource);
  }

  @Override
  public List<String> getDefaultGrandChildIds() {
    return Arrays.asList(Role.VISITOR.getResId());
  }

}
