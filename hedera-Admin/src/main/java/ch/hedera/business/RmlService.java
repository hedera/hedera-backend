/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - RmlService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.time.OffsetDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.service.ResourceService;

import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AdminController;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.humanities.Rml;
import ch.hedera.model.humanities.RmlFormat;
import ch.hedera.repository.RmlRepository;
import ch.hedera.specification.RmlSpecification;
import ch.hedera.util.RdfTool;
import ch.hedera.util.RmlTool;

@Service
@ConditionalOnBean(AdminController.class)
public class RmlService extends ResourceService<Rml> {

  private static final String NAME_FORMAT_VERSION_ALREADY_EXIST = "validation.rml.error.nameFormatAndVersionAlreadyExist";
  private static final String RML_XML_MAPPING = "rml_xml_mapping.ttl";
  private static final String RML_CSV_MAPPING = "rml_csv_mapping.ttl";
  private static final String RML_JSON_MAPPING = "rml_json_mapping.ttl";
  private static final String RML_SQL_MAPPING = "rml_sql_mapping.ttl";
  private static final Logger log = LoggerFactory.getLogger(RmlService.class);

  private final String ldModuleUrl;

  public RmlService(HederaProperties config) {
    super();
    this.ldModuleUrl = config.getModule().getLinkedData().getPublicUrl();
  }

  @Override
  public RmlSpecification getSpecification(Rml resource) {
    return new RmlSpecification(resource);
  }

  @Override
  public void validateItemSpecificRules(Rml rml, BindingResult errors) {
    // Check that there is no another rml with same name, version and format
    Rml existingRml = this.findByNameAndFormatAndVersion(rml.getName(), rml.getFormat(), rml.getVersion());
    if (existingRml != null && !rml.getResId().equals(existingRml.getResId())) {
      errors.addError(new FieldError(rml.getClass().getSimpleName(), "name",
              this.messageService.get(NAME_FORMAT_VERSION_ALREADY_EXIST, new Object[] { rml.getName(), rml.getFormat(), rml.getVersion() })));
    }

    try {
      final String rmlWithParameters = RmlTool.replacePlaceHoldersInFile(new ByteArrayInputStream(rml.getRdfFile().getFileContent()),
              RmlTool.getPlaceHolderValues(URI.create(this.ldModuleUrl)));
      RdfTool.wellformed(new ByteArrayInputStream(rmlWithParameters.getBytes()), RdfFormat.TURTLE);
    } catch (SolidifyCheckingException e) {
      errors.addError(
              new FieldError(rml.getClass().getSimpleName(), "rdfFile", this.messageService.get("validation.rml.check.syntax",
                      new Object[] { rml.getRdfFile().getFileName(), e.getMessage() })));
    }

  }

  private Rml findByNameAndFormatAndVersion(String name, RmlFormat format, String version) {
    return ((RmlRepository) this.itemRepository).findByNameAndFormatAndVersion(name, format, version);
  }

  public void initOptionalData() {
    try {
      this.createRml("XML mapping", "RML mapping for XML", RmlFormat.XML, RML_XML_MAPPING);
      this.createRml("CSV mapping", "RML mapping for CSV", RmlFormat.CSV, RML_CSV_MAPPING);
      this.createRml("JSON mapping", "RML mapping for JSON", RmlFormat.JSON, RML_JSON_MAPPING);
      this.createRml("SQL mapping", "RML mapping for SQL", RmlFormat.SQL, RML_SQL_MAPPING);
    } catch (IOException e) {
      log.warn("Error when creating an RML mapping: {}", e.getMessage());
    }
  }

  private void createRml(String name, String description, RmlFormat format, String filename) throws IOException {
    if (((RmlRepository) this.itemRepository).findByNameAndFormatAndVersion(name, format, "1.0") == null) {
      Rml rml = new Rml();
      rml.setName(name);
      rml.setDescription(description);
      rml.setFormat(format);
      rml.setVersion("1.0");

      // Add file
      ResourceFile resourceFile = ((ResourceFileInterface) rml).setNewResourceFile();
      resourceFile.getCreation().setWhen(OffsetDateTime.now());

      // read all bytes and save
      byte[] fileContent = new ClassPathResource(filename).getInputStream().readAllBytes();
      resourceFile.setFileContent(fileContent);
      resourceFile.setFileSize((long) fileContent.length);
      resourceFile.setFileName(filename);
      resourceFile.setMimeType(MediaType.APPLICATION_XML.toString());

      this.save(rml);
      log.info("RML '{}' created", name);
    }
  }
}
