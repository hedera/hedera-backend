/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - ProjectRoleService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.hedera.controller.AdminController;
import ch.hedera.model.display.RolePersonDTO;
import ch.hedera.model.display.RolePersonListDTO;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ProjectPersonRole;
import ch.hedera.specification.ProjectRolePersonSpecification;
import ch.hedera.specification.RoleSpecification;

import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.service.JoinResource3TiersService;
import ch.unige.solidify.specification.Join3TiersSpecification;
import ch.unige.solidify.specification.SolidifySpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class ProjectRoleService extends JoinResource3TiersService<Project, Role, Person, ProjectPersonRole> {

  @Override
  public Join3TiersSpecification<ProjectPersonRole> getJoinSpecification(ProjectPersonRole joinResource) {
    return new ProjectRolePersonSpecification(joinResource);
  }

  @Override
  protected SolidifySpecification<Role> getChildSpecification(Role role) {
    return new RoleSpecification(role);
  }

  @Override
  protected String getChildToRelationPropertyName() {
    return ProjectPersonRole.ROLE_RELATION_PROPERTY_NAME;
  }

  @Override
  public Person getEmptyGrandChildResourceObject() {
    return new Person();
  }

  @Override
  public void setGrandChildResource(ProjectPersonRole joinResource, Person grandChildResource) {
    joinResource.setPerson(grandChildResource);
  }

  @Override
  public Person getGrandChildResource(ProjectPersonRole joinResource) {
    return joinResource.getPerson();
  }

  @Override
  public Relation3TiersChildDTO getChildDTO(Role childItem, List<ProjectPersonRole> joinResources) {
    List<RolePersonDTO> list = new ArrayList<>();
    for (ProjectPersonRole projectPersonRole : joinResources) {
      list.add(this.getGrandChildDTO(projectPersonRole));
    }
    return new RolePersonListDTO(childItem, list);
  }

  @Override
  public RolePersonDTO getGrandChildDTO(ProjectPersonRole joinResource) {
    return new RolePersonDTO(joinResource);
  }

  @Override
  public ProjectPersonRole getEmptyJoinResourceObject() {
    return new ProjectPersonRole();
  }

  @Override
  public Project getEmptyParentResourceObject() {
    return new Project();
  }

  @Override
  public Role getEmptyChildResourceObject() {
    return new Role();
  }

  @Override
  public void setParentResource(ProjectPersonRole joinResource, Project project) {
    joinResource.setProject(project);

  }

  @Override
  public void setChildResource(ProjectPersonRole joinResource, Role role) {
    joinResource.setRole(role);

  }
}
