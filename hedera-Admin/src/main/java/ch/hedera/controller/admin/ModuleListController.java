/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - ModuleListController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.admin;

import java.util.Objects;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.OAIProperties;
import ch.unige.solidify.auth.client.service.AuthorizationClientProperties;
import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.util.StringTool;

import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AdminController;
import ch.hedera.rest.UrlPath;

@EveryonePermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_MODULE)
public class ModuleListController implements ControllerWithHateoasHome {

  private final ModuleList moduleList;

  public ModuleListController(HederaProperties properties, AuthorizationClientProperties authClientProperties, OAIProperties oaiProperties) {
    this.moduleList = new ModuleList(properties, authClientProperties, oaiProperties);
  }

  private class ModuleList extends RepresentationModel<ModuleList> {

    @Schema(description = "The URL of Authorization module.")
    private final String authorization;
    @Schema(description = "The URL of Admin module.")
    private final String admin;
    @Schema(description = "The URL of Ingest module.")
    private final String ingest;
    @Schema(description = "The URL of Access module.")
    private final String access;
    @Schema(description = "The URL of Index module.")
    private final String index;
    @Schema(description = "The URL of OAI-PMH module.")
    private final String oaiInfo;
    @Schema(description = "The URL of OAI-PMH provider.")
    private final String oaiPmh;
    @Schema(description = "The URL of SPARQL proxy.")
    private final String sparql;
    @Schema(description = "The URL of IIIF proxy.")
    private final String iiif;
    @Schema(description = "The URL of Linked Data proxy.")
    private final String linkedData;
    @Schema(description = "The URL of ontology proxy.")
    private final String ontologies;

    public ModuleList(HederaProperties properties, AuthorizationClientProperties authClientProperties, OAIProperties oaiProperties) {
      this.authorization = authClientProperties.getPublicAuthorizationServerUrl();
      this.admin = properties.getModule().getAdmin().getPublicUrl();
      this.ingest = properties.getModule().getIngest().getPublicUrl();
      this.access = properties.getModule().getAccess().getPublicUrl();
      this.index = this.getLinkedModuleUrl(this.admin, IndexConstants.INDEX_MODULE);
      this.oaiInfo = this.getLinkedModuleUrl(this.access, OAIConstants.OAI_MODULE);
      if (StringTool.isNullOrEmpty(oaiProperties.getPublicUrl())) {
        this.oaiPmh = this.oaiInfo + "/" + OAIConstants.OAI_PROVIDER + "/" + OAIConstants.OAI_RESOURCE;
      } else {
        this.oaiPmh = oaiProperties.getPublicUrl();
      }
      this.sparql = properties.getModule().getSparql().getPublicUrl();
      this.iiif = properties.getModule().getIIIF().getPublicUrl();
      this.linkedData = properties.getModule().getLinkedData().getPublicUrl();
      this.ontologies = properties.getModule().getOntologies().getPublicUrl();
    }

    public String getAuthorization() {
      return this.authorization;
    }

    public String getAdmin() {
      return this.admin;
    }

    public String getIngest() {
      return this.ingest;
    }

    public String getAccess() {
      return this.access;
    }

    public String getIndex() {
      return this.index;
    }

    public String getOaiInfo() {
      return this.oaiInfo;
    }

    public String getOaiPmh() {
      return this.oaiPmh;
    }

    public String getSparql() {
      return this.sparql;
    }

    public String getIIIF() {
      return this.iiif;
    }

    public String getLinkedData() {
      return this.linkedData;
    }

    public String getOntologies() {
      return this.ontologies;
    }

    private String getLinkedModuleUrl(String mainModuleUrl, String linkedModuleName) {
      return mainModuleUrl.substring(0, mainModuleUrl.lastIndexOf("/") + 1) + linkedModuleName;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = super.hashCode();
      result = prime * result
              + Objects.hash(this.access, this.admin, this.authorization, this.ingest, this.oaiInfo);
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (!super.equals(obj))
        return false;
      if (this.getClass() != obj.getClass())
        return false;
      ModuleList other = (ModuleList) obj;
      return Objects.equals(this.access, other.access) && Objects.equals(this.admin, other.admin)
              && Objects.equals(this.authorization, other.authorization) && Objects.equals(this.ingest, other.ingest)
              && Objects.equals(this.oaiInfo, other.oaiInfo);
    }
  }

  @GetMapping
  public HttpEntity<ModuleList> moduleList() {
    return new ResponseEntity<>(this.moduleList, HttpStatus.OK);
  }

}
