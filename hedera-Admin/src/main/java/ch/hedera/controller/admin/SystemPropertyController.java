/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - SystemPropertyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.admin;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.business.IndexFieldAliasService;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.model.index.FacetProperties;

import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AdminController;
import ch.hedera.rest.UrlPath;

@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_SYSTEM_PROPERTY)
public class SystemPropertyController implements ControllerWithHateoasHome {

  static final Logger log = LoggerFactory.getLogger(SystemPropertyController.class);

  private final HederaProperties config;

  private final SystemProperties systemProperties;

  private final IndexFieldAliasService indexFieldAliasService;

  private final String indexName;

  public SystemPropertyController(HederaProperties properties, SolidifyProperties solidifyProperties,
          IndexFieldAliasService indexFieldAliasService) {
    this.config = properties;
    this.systemProperties = new SystemProperties(properties, solidifyProperties);
    this.indexFieldAliasService = indexFieldAliasService;
    this.indexName = properties.getIndexing().getIndexName();
  }

  @Schema(description = "A system property is defined by the back-end administrators, for the whole system.")
  private static class SystemProperties extends RepresentationModel<SystemProperties> {

    @Schema(description = "The default license used by the system.")
    private String defaultLicense;

    @Schema(description = "The default copyright holder used by the system.")
    private String defaultCopyrightHolder;

    @Schema(description = "The maximum upload file size, in bytes.")
    private String fileSizeLimit;

    @Schema(description = "The ORCID client ID used by the system.")
    private String orcidClientId;

    @Schema(description = "Describes facet properties used on archives search page.")
    private List<FacetProperties> searchFacets;

    public SystemProperties(HederaProperties properties, SolidifyProperties solidifyProperties) {
      this.defaultLicense = properties.getParameters().getDefaultLicense();
      this.defaultCopyrightHolder = properties.getParameters().getDefaultCopyrightHolder();
      this.fileSizeLimit = String.valueOf(properties.getParameters().getFileSizeLimit().toBytes());
      this.orcidClientId = solidifyProperties.getOrcid().getClientId();
    }

    public String getDefaultLicense() {
      return this.defaultLicense;
    }

    public String getDefaultCopyrightHolder() {
      return this.defaultCopyrightHolder;
    }

    public String getFileSizeLimit() {
      return this.fileSizeLimit;
    }

    public List<FacetProperties> getSearchFacets() {
      return this.searchFacets;
    }

    public String getOrcidClientId() {
      return this.orcidClientId;
    }

    public void setSearchFacets(List<FacetProperties> searchFacets) {
      this.searchFacets = searchFacets;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = super.hashCode();
      result = prime * result + Objects.hash(this.defaultLicense, this.defaultCopyrightHolder, this.fileSizeLimit, this.orcidClientId,
              this.searchFacets);
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (!super.equals(obj))
        return false;
      if (this.getClass() != obj.getClass())
        return false;
      SystemProperties other = (SystemProperties) obj;
      return Objects.equals(this.defaultLicense, other.defaultLicense)
              && Objects.equals(this.defaultCopyrightHolder, other.defaultCopyrightHolder)
              && Objects.equals(this.fileSizeLimit, other.fileSizeLimit)
              && Objects.equals(this.orcidClientId, other.orcidClientId)
              && Objects.equals(this.searchFacets, other.searchFacets);
    }

  }

  @GetMapping
  public HttpEntity<SystemProperties> systemProperties() {
    this.systemProperties.setSearchFacets(this.indexFieldAliasService.getFacetProperties(this.indexName));
    this.systemProperties.removeLinks();
    this.systemProperties.add(linkTo(this.getClass()).withSelfRel());
    return new ResponseEntity<>(this.systemProperties, HttpStatus.OK);
  }
}
