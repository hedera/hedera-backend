/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - RmlController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.admin;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceWithFileController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.hedera.HederaConstants;
import ch.hedera.controller.AdminController;
import ch.hedera.model.humanities.Rml;
import ch.hedera.model.humanities.RmlFormat;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_RML)
public class RmlController extends ResourceWithFileController<Rml> {
  public RmlController() {
  }

  @Override
  public HttpEntity<Rml> create(@RequestBody Rml rml) {
    return super.create(rml);
  }

  @Override
  @UserPermissions
  public HttpEntity<Rml> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  @UserPermissions
  public HttpEntity<RestCollection<Rml>> list(@ModelAttribute Rml search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<Rml> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
  }

  @PostMapping("/" + HederaActionName.UPLOAD_RDF)
  public HttpEntity<Rml> createItemAndUploadFile(@RequestParam("name") String name,
          @RequestParam("description") String description,
          @RequestParam("version") String version,
          @RequestParam("format") String format,
          @RequestParam(HederaConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    Rml rml = new Rml();
    rml.setName(name);
    rml.setDescription(description);
    rml.setVersion(version);
    rml.setFormat(RmlFormat.valueOf(format));
    return super.createItemAndUploadFile(rml, file, mimeType);
  }

  @Override
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.UPLOAD_FILE)
  public HttpEntity<Rml> uploadFile(
          @PathVariable String id,
          @RequestParam(HederaConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(id, file, mimeType);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.DOWNLOAD_RDF)
  @Override
  public HttpEntity<StreamingResponseBody> downloadFile(@PathVariable String id) {
    return super.downloadFile(id);
  }

  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.DELETE_RDF)
  @Override
  public HttpEntity<Rml> deleteFile(@PathVariable String id) {
    return super.deleteFile(id);
  }
}
