/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - RmlProjectController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.admin;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.AssociationController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.hedera.controller.AdminController;
import ch.hedera.model.humanities.Rml;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.ResourceName;
import ch.hedera.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_RML + SolidifyConstants.URL_PARENT_ID + ResourceName.PROJECT)
public class RmlProjectController extends AssociationController<Rml, Project> {

  @Override
  public HttpEntity<List<Project>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.create(parentid, ids);
  }

  @UserPermissions
  @Override
  public HttpEntity<Project> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  @UserPermissions
  public HttpEntity<RestCollection<Project>> list(@PathVariable String parentid, @ModelAttribute Project filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  public HttpEntity<Project> update(@PathVariable String parentid, @PathVariable String id, @RequestBody Project v2) {
    return super.update(parentid, id, v2);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return super.delete(parentid, id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.deleteList(parentid, ids);
  }

  @Override
  protected String getParentFieldName() {
    return "rmls";
  }

  @Override
  public Project getEmptyChildResourceObject() {
    return new Project();
  }

  @Override
  protected boolean addChildOnParent(Rml rml, Project project) {
    return rml.addProject(project);
  }

  @Override
  protected boolean removeChildFromParent(Rml rml, Project project) {
    return rml.removeProject(project);
  }
}
