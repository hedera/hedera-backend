/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - TriplestoreQueriesController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.admin;

import org.apache.jena.query.Query;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;

import ch.hedera.controller.AdminController;
import ch.hedera.model.humanities.RmlFormat;
import ch.hedera.model.triplestore.SparqlResultRow;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.triplestore.TriplestoreResourceService;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlQueryTool;

@RestController
@ConditionalOnBean(AdminController.class)
@AdminPermissions
@RequestMapping(UrlPath.ADMIN_TRIPLESTORE_QUERIES)
public class TriplestoreQueriesController {

  TriplestoreResourceService triplestoreResourceService;

  public TriplestoreQueriesController(TriplestoreResourceService triplestoreResourceService) {
    this.triplestoreResourceService = triplestoreResourceService;
  }

  @PostMapping("/{datasetId}/list")
  public ResponseEntity<RestCollection<SparqlResultRow>> executeQueryForList(@PathVariable String datasetId, @RequestBody String queryStr,
          Pageable pageable) {
    final Query query = SparqlBuilderTool.createQuery(queryStr);
    Page<SparqlResultRow> datasetEntryList = this.triplestoreResourceService.executeQueryForList(datasetId, query, pageable);
    return new ResponseEntity<>(this.toRestCollection(datasetEntryList), HttpStatus.OK);
  }

  @PostMapping("/{datasetId}/detail")
  public ResponseEntity<RestCollection<SparqlResultRow>> executeQueryForDetail(@PathVariable String datasetId,
          @RequestBody String nodeValue) {
    Page<SparqlResultRow> datasetEntryProperties = this.triplestoreResourceService.executeQueryForDetail(datasetId, nodeValue,
            SparqlQueryTool.queryToListAllTriples(SparqlBuilderTool.SUBJECT_VARIABLE, SparqlBuilderTool.PROPERTY_VARIABLE,
                    SparqlBuilderTool.OBJECT_VARIABLE));
    return new ResponseEntity<>(this.toRestCollection(datasetEntryProperties), HttpStatus.OK);
  }

  @PostMapping(value = "/{datasetId}", consumes = "text/plain")
  public ResponseEntity<String> executeQuery(@PathVariable String datasetId, @RequestBody String queryStr,
          @RequestParam(name = "format", defaultValue = "JSON") RmlFormat format) {
    String result;
    final Query query = SparqlBuilderTool.createQuery(queryStr);
    switch (format) {
      case CSV -> result = this.triplestoreResourceService.executeQueryAndReturnCSV(datasetId, query);
      case XML -> result = this.triplestoreResourceService.executeQueryAndReturnXML(datasetId, query);
      default -> result = this.triplestoreResourceService.executeQueryAndReturnJSON(datasetId, query);
    }

    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  private RestCollection<SparqlResultRow> toRestCollection(Page<SparqlResultRow> resultsPage) {
    return new RestCollection<>(resultsPage, resultsPage.getPageable());
  }
}
