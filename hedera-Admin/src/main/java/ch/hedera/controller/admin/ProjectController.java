/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - ProjectController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.admin;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceWithFileController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.NoOnePermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.business.ProjectService;
import ch.hedera.controller.AdminController;
import ch.hedera.model.ResourceIdentifierType;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_PROJECT)
public class ProjectController extends ResourceWithFileController<Project> {

  @Override
  public HttpEntity<Project> create(@RequestBody Project project) {
    return super.create(project);
  }

  @UserPermissions
  @Override
  public HttpEntity<Project> get(@PathVariable String id) {
    return super.get(id);
  }

  @UserPermissions
  @GetMapping(path = SolidifyConstants.URL_ID, params = HederaConstants.IDENTIFIER_TYPE_PARAM)
  public HttpEntity<Project> getById(@PathVariable String id,
          @RequestParam(defaultValue = ResourceIdentifierType.DEFAULT) ResourceIdentifierType identifierType) {
    Project project;
    switch (identifierType) {
      case NAME -> {
        project = ((ProjectService) this.itemService).getByName(id);
        this.addLinks(project);
        return new ResponseEntity<>(project, HttpStatus.OK);
      }
      case SHORT_NANE -> {
        project = ((ProjectService) this.itemService).getByShortName(id);
        this.addLinks(project);
        return new ResponseEntity<>(project, HttpStatus.OK);
      }
      case RES_ID -> {
        return super.get(id);
      }
      default -> {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
      }
    }
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<Project>> list(@ModelAttribute Project search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<Project>> advancedSearch(@ModelAttribute Project resource,
          @RequestParam("search") String search, @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(resource, search, matchtype, pageable);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<Project>> advancedSearch(@RequestBody Project search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(search, matchtype, pageable);
  }

  @PreAuthorize("@projectPermissionService.isAllowedToUpdate(#id)")
  @Override
  public HttpEntity<Project> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  /**
   * Delete a Project.
   *
   * @param id
   * @return HttpStatus.OK (200) in case of delete or HttpStatus.ACCEPTED (202) in case of closing the project to distinguish between both
   * operations
   */
  @PreAuthorize("@projectPermissionService.isAllowed(#id, 'DELETE')")
  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @NoOnePermissions
  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
  }

  @PreAuthorize("@projectPermissionService.isAllowed(#id, 'DELETE')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.CLOSE)
  public ResponseEntity<Project> close(@PathVariable String id, @RequestParam(value = "closingDate", required = false) String closingDate) {
    return this.closeProject(id, closingDate);
  }

  @PreAuthorize("@projectPermissionService.isAllowed(#id, 'UPDATE')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.UPLOAD_LOGO)
  @Override
  public HttpEntity<Project> uploadFile(@PathVariable String id, @RequestParam(HederaConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(id, file, mimeType);
  }

  @UserPermissions
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.DOWNLOAD_LOGO)
  @Override
  public HttpEntity<StreamingResponseBody> downloadFile(@PathVariable String id) {
    return super.downloadFile(id);
  }

  @PreAuthorize("@projectPermissionService.isAllowed(#id, 'UPDATE')")
  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.DELETE_LOGO)
  @Override
  public HttpEntity<Project> deleteFile(@PathVariable String id) {
    return super.deleteFile(id);
  }

  private ResponseEntity<Project> closeProject(String projectId, String closingDate) {
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(StringTool.DATE_FORMAT);
    Project project = this.itemService.findOne(projectId);
    if (project.isOpen()) {
      if (StringTool.isNullOrEmpty(closingDate)) {
        project.setClosingDate(LocalDate.now());
      } else {
        project.setClosingDate(LocalDate.parse(closingDate, formatter));
      }
      project = this.itemService.save(project);
    }
    return new ResponseEntity<>(project, HttpStatus.OK);
  }
}
