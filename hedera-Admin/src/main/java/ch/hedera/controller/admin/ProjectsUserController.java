/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - ProjectsUserController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.admin;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.auth.service.ApplicationRoleListService;
import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Tool;

import ch.hedera.business.PersonProjectService;
import ch.hedera.business.PersonService;
import ch.hedera.business.ProjectService;
import ch.hedera.controller.AdminController;
import ch.hedera.model.dto.AuthorizedProjectDto;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ProjectPersonRole;
import ch.hedera.rest.UrlPath;

@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_AUTHORIZED_PROJECT)
public class ProjectsUserController extends SolidifyController implements ControllerWithHateoasHome, ApplicationRoleListService {
  private static final Logger logger = LoggerFactory.getLogger(ProjectsUserController.class);

  private static final boolean WITHOUT_ALL_PUBLIC = false;

  private final ProjectService projectService;
  private final PersonService personService;
  private final PersonProjectService personProjectService;

  public ProjectsUserController(ProjectService projectService,
          PersonService personService,
          PersonProjectService personProjectService) {
    this.projectService = projectService;
    this.personService = personService;
    this.personProjectService = personProjectService;
  }

  /***
   * This method will return the project passed as parameter if the person has a role in it. If not, an exception will be thrown.
   *
   * @param id
   * @return
   */
  @GetMapping(SolidifyConstants.URL_ID)
  public HttpEntity<AuthorizedProjectDto> getAuthorizedProject(@PathVariable String id) {
    try {
      final List<AuthorizedProjectDto> projectList = this
              .getListAuthorizedProjects(PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE),
                      new Project(), WITHOUT_ALL_PUBLIC)
              .getContent();
      final AuthorizedProjectDto project = projectList.stream()
              .filter(o -> id.equals(o.getResId()))
              .findFirst()
              .orElse(null);

      if (project == null) {
        throw new SolidifyRuntimeException("Could not find authorized project with id " + id + " for current user");
      }
      this.addLinks(project);
      return new ResponseEntity<>(project, HttpStatus.OK);
    } catch (final SolidifyRuntimeException ex) {
      logger.warn(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  /**
   * This method will return all authorized projects: the ones where the person has a role. If the param withAllPublic is true, all the public
   * projects will be added as well even though the person has no role in it. This is used for the access module.
   *
   * @param openOnly if true returns only open projects
   * @return a list of projects on which the authenticated user has permissions
   */
  @GetMapping
  @SuppressWarnings("squid:S4684")
  public HttpEntity<RestCollection<AuthorizedProjectDto>> listAll(
          @RequestParam(value = "openOnly", required = false, defaultValue = "true") boolean openOnly,
          @RequestParam(value = "withAllPublic", required = false, defaultValue = "false") boolean withAllPublic,
          @ModelAttribute Project search, Pageable pageable) {
    try {
      if (openOnly) {
        final LocalDate now = LocalDate.now();
        search.setOpeningDate(now);
        search.setClosingDate(now);
      }
      final Page<AuthorizedProjectDto> projectList = this.getListAuthorizedProjects(pageable, search, withAllPublic);

      final RestCollection<AuthorizedProjectDto> collection = new RestCollection<>(projectList, pageable);
      collection.add(linkTo(this.getClass()).withSelfRel());
      collection.add(Tool.parentLink((linkTo(this.getClass())).toUriComponentsBuilder()).withRel(ActionName.MODULE));

      this.addPageLinks(linkTo(this.getClass()), collection, pageable);

      return new ResponseEntity<>(collection, HttpStatus.OK);

    } catch (final SolidifyRuntimeException ex) {
      logger.warn(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  protected void addLinks(Project project) {
    project.removeLinks();
    project.addLinks(linkTo(this.getClass()), true, true);
  }

  private Page<AuthorizedProjectDto> getListAuthorizedProjects(Pageable pageable, Project search, boolean withAllPublic) {
    Page<AuthorizedProjectDto> listItem;
    if (this.isRootOrTrustedOrAdminRole()) {
      Page<Project> projectList = this.projectService.findAll(this.projectService.getSpecification(search),
              pageable);
      listItem = this.getAuthorizedProjectWithRole(projectList);
    } else if (this.getAuthenticatedUserExternalUid() != null) {
      listItem = this.getProjectsWithPermissionsForAuthenticatedUser(search, withAllPublic, pageable);
    } else {
      throw new SolidifyRuntimeException("This method can not be called with your right.");
    }

    return listItem;
  }

  /**
   * @param withAllPublic all public project should also be added as authorized projects
   * @return all projects on which the authenticated user has permissions
   * @throws SolidifyRuntimeException if the authenticated user is not associated to any Person
   */
  private Page<AuthorizedProjectDto> getProjectsWithPermissionsForAuthenticatedUser(Project search, boolean withAllPublic,
          Pageable pageable) {

    final String personId = this.personService.getPerson(SecurityContextHolder.getContext().getAuthentication()).getResId();
    Page<Project> projectPage;

    if (personId != null) {
      // Filter on project
      if (search.getOpeningDate() == null) {
        if (withAllPublic) {
          // All projects with public ones
          projectPage = this.projectService.findProjectsByPersonIdAndProjectNameAddingAllPublic(personId, search, pageable);
        } else {
          // All projects without public ones
          projectPage = this.projectService.findProjectsByPersonIdAndProjectName(personId, search, pageable);
        }
      } else {
        if (withAllPublic) {
          // All open projects with public ones
          projectPage = this.projectService.findOpenProjectsByPersonIdAndProjectNameAddingAllPublic(personId, search, pageable);
        } else {
          // All open projects
          projectPage = this.projectService.findOpenProjectsByPersonIdAndProjectName(personId, search, pageable);
        }
      }
    } else {
      final String authUserId = this.getAuthenticatedUserExternalUid();
      throw new SolidifyRuntimeException("User '" + authUserId + "' does not correspond to any Person");
    }
    // convert to org unit dto and add the role in each org unit
    return this.getAuthorizedProjectWithRole(projectPage);
  }

  private Page<AuthorizedProjectDto> fromProjectToAuthorizedProject(Page<Project> projectPage) {
    return projectPage.map(AuthorizedProjectDto::fromEntity);
  }

  private Page<AuthorizedProjectDto> getAuthorizedProjectWithRole(Page<Project> projectPage) {
    final String personId = this.personService.getPerson(SecurityContextHolder.getContext().getAuthentication()).getResId();

    // convert to project dto and add the role in each project
    Page<AuthorizedProjectDto> projectDtoList = this.fromProjectToAuthorizedProject(projectPage);

    for (AuthorizedProjectDto projectItem : projectDtoList) {
      List<ProjectPersonRole> projectRoles = this.personProjectService.findAllRelations(personId, projectItem.getResId());
      if (projectRoles.size() == 1) {
        projectItem.setRole(projectRoles.get(0).getRole());
      } else if (projectRoles.size() > 1) {
        throw new SolidifyRuntimeException("Person '" + personId + "' has more than one role on project " + projectItem.getResId());
      }
    }

    return projectDtoList;
  }
}
