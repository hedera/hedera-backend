/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - UserController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.admin;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.hedera.business.UserService;
import ch.hedera.controller.AdminController;
import ch.hedera.model.security.User;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.UrlPath;
import ch.hedera.service.UserSynchronizationService;

@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_USER)
public class UserController extends ResourceController<User> {
  static final Logger log = LoggerFactory.getLogger(UserController.class);

  private final UserSynchronizationService userSynchronizationService;

  public UserController(UserSynchronizationService userSynchronizationService) {
    this.userSynchronizationService = userSynchronizationService;
  }

  @UserPermissions
  @Override
  public HttpEntity<User> get(@PathVariable String id) {
    return super.get(id);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<User>> list(@ModelAttribute User search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<User>> advancedSearch(@ModelAttribute User user,
          @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchType, Pageable pageable) {
    return super.advancedSearch(user, search, matchType, pageable);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<User>> advancedSearch(@RequestBody User search,
          @RequestParam(value = "match", required = false) String matchType, Pageable pageable) {
    return super.advancedSearch(search, matchType, pageable);
  }

  // Change role permissions are checked by the authorization server
  @AdminPermissions
  @Override
  public HttpEntity<User> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    // Get existing user
    final User existingUser = this.itemService.findOne(id);
    if (updateMap == null) {
      return ResponseEntity.badRequest().build();
    }
    // Check if role changed
    String updateApplicationRoleId = ((UserService) this.itemService).getApplicationRoleId(updateMap);
    if (updateApplicationRoleId != null && !existingUser.getApplicationRole().getResId().equals(updateApplicationRoleId)) {
      ((UserService) this.itemService).updateUserRole(existingUser.getExternalUid(), updateApplicationRoleId);
    }
    return super.update(id, updateMap);
  }

  @AdminPermissions
  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @AdminPermissions
  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @UserPermissions
  @GetMapping("/" + HederaActionName.AUTHENTICATED)
  public HttpEntity<User> getAuthenticatedUser() {
    final String externalUid = this.getAuthenticatedUserExternalUid();
    final User user = ((UserService) this.itemService).findByExternalUid(externalUid);
    this.addLinks(user);
    return new ResponseEntity<>(user, HttpStatus.OK);
  }

  @AdminPermissions
  @PostMapping("/" + HederaActionName.REVOKE_ALL_TOKENS + "/" + UrlPath.AUTH_USER_ID)
  public ResponseEntity<Object> revokeAllTokens(@PathVariable String externalUid) {

    final User user = ((UserService) this.itemService).findByExternalUid(externalUid);
    if (user == null) {
      return ResponseEntity.notFound().build();
    }
    ((UserService) this.itemService).revokeAccessAndRefreshTokens(user.getExternalUid());
    return ResponseEntity.ok().build();
  }

  @UserPermissions
  @PostMapping("/" + HederaActionName.REVOKE_MY_TOKENS)
  public ResponseEntity<Object> revokeMyToken() {
    final String externalUid = this.getAuthenticatedUserExternalUid();
    ((UserService) this.itemService).revokeAccessAndRefreshTokens(externalUid);
    return ResponseEntity.ok().build();
  }

  /**
   * For all users:
   * - retrieve application role from authorization server
   * - retrieve user login information from authorization server
   * - synchronize (bi-directional) ORCID with authorization server
   *
   * @return the result of the synchronize action
   */
  @RootPermissions
  @PostMapping(HederaActionName.SYNCHRONIZE)
  public HttpEntity<Result> synchronize() {
    Result result = new Result("synchronize");
    try {
      this.userSynchronizationService.synchronizeUsersWithAuthServer();
      result.setStatus(Result.ActionStatus.EXECUTED);
      result.setMesssage("User information synchronized");
    } catch (RuntimeException e) {
      log.error("In synchronising user information", e);
      result.setStatus(Result.ActionStatus.NOT_EXECUTED);
      result.setMesssage(e.getMessage());
    }
    return this.returnHttpResult(result);
  }

  @TrustedUserPermissions
  @PostMapping("/" + HederaActionName.NEW_USER + SolidifyConstants.URL_EXTERNAL_UID)
  public ResponseEntity<Void> createNewUser(@PathVariable String externalUid) {
    ((UserService) this.itemService).createNewUser(externalUid);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
