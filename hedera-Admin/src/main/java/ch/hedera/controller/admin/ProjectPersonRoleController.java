/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - ProjectPersonRoleController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.admin;

import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.Relation3TiersController;
import ch.unige.solidify.exception.SolidifyUnmodifiableException;
import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.NoOnePermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.hedera.controller.AdminController;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ProjectPersonRole;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ResourceName;
import ch.hedera.rest.UrlPath;

@RestController
@UserPermissions
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_PROJECT + SolidifyConstants.URL_PARENT_ID + ResourceName.PERSON)
public class ProjectPersonRoleController
        extends Relation3TiersController<Project, Person, Role, ProjectPersonRole> {

  @Override
  @PreAuthorize("@projectPermissionService.isAllowed(#parentid, 'UPDATE')")
  public HttpEntity<List<ProjectPersonRole>> create(@PathVariable String parentid, @PathVariable String id,
          @RequestBody String[] ids) {
    if (ids.length > 1) {
      throw new SolidifyUnmodifiableException("Only one role by person by organizational unit is allowed");
    }
    return super.create(parentid, id, ids);
  }

  @Override
  @PreAuthorize("@projectPermissionService.isAllowed(#parentid, 'UPDATE')")
  public HttpEntity<List<ProjectPersonRole>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.create(parentid, ids);
  }

  @Override
  @PreAuthorize("@projectPermissionService.isAllowed(#parentid, 'GET_MEMBER')")
  public HttpEntity<Relation3TiersChildDTO> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  @PreAuthorize("@projectPermissionService.isAllowed(#parentid, 'LIST_MEMBERS')")
  public HttpEntity<RestCollection<Relation3TiersChildDTO>> list(@PathVariable String parentid, @ModelAttribute Person filterPerson,
          Pageable pageable) {
    return super.list(parentid, filterPerson, pageable);
  }

  @Override
  @PreAuthorize("@projectPermissionService.isAllowed(#parentid, 'UPDATE')")
  public HttpEntity<List<ProjectPersonRole>> update(@PathVariable String parentid, @PathVariable String id,
          @RequestBody ProjectPersonRole joinResource) {
    return super.update(parentid, id, joinResource);
  }

  @Override
  @NoOnePermissions
  @PreAuthorize("@projectPermissionService.isAllowed(#parentid, 'UPDATE')")
  public HttpEntity<ProjectPersonRole> update(@PathVariable String parentid, @PathVariable String id,
          @PathVariable String grandChildId, @RequestBody Map<String, Object> joinResource) {
    return super.update(parentid, id, grandChildId, joinResource);
  }

  @Override
  @PreAuthorize("@projectPermissionService.isAllowed(#parentid, 'UPDATE')")
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id, @RequestBody(required = false) String[] ids) {
    return super.delete(parentid, id, ids);
  }

  @Override
  @PreAuthorize("@projectPermissionService.isAllowed(#parentid, 'UPDATE')")
  public ResponseEntity<Void> delete(@PathVariable String parentid, @RequestBody(required = false) String[] ids) {
    return super.delete(parentid, ids);
  }

  @Override
  @PostMapping(SolidifyConstants.URL_ID + "/" + HederaActionName.SET_ROLE)
  @PreAuthorize("@projectPermissionService.isAllowed(#parentid, 'UPDATE')")
  public HttpEntity<HttpStatus> setGrandChildList(@PathVariable String parentid, @PathVariable String id, @RequestBody String[] gdChildIds) {
    return super.setGrandChildList(parentid, id, gdChildIds);
  }

}
