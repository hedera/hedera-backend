/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - ProjectRoleController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.admin;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.Relation3TiersController;
import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.UserPermissions;

import ch.hedera.controller.AdminController;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ProjectPersonRole;
import ch.hedera.rest.ResourceName;
import ch.hedera.rest.UrlPath;

@RestController
@UserPermissions
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_PROJECT + SolidifyConstants.URL_PARENT_ID + ResourceName.ROLE)
public class ProjectRoleController extends
        Relation3TiersController<Project, Role, Person, ProjectPersonRole> {

  @Override
  @PreAuthorize("@projectPermissionService.isAllowed(#parentid, 'LIST_MEMBERS')")
  public HttpEntity<RestCollection<Relation3TiersChildDTO>> list(@PathVariable String parentid, @ModelAttribute Role filterRole,
          Pageable pageable) {
    return super.list(parentid, filterRole, pageable);
  }

  @Override
  @PreAuthorize("@projectPermissionService.isAllowed(#parentid, 'GET_MEMBER')")
  public HttpEntity<Relation3TiersChildDTO> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

}
