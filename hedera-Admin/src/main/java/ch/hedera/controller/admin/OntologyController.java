/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - OntologyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.admin;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceWithFileController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.business.OntologyService;
import ch.hedera.controller.AdminController;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ResourceName;
import ch.hedera.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_ONTOLOGY)
public class OntologyController extends ResourceWithFileController<Ontology> {

  @Override
  public HttpEntity<Ontology> create(@RequestBody Ontology ontology) {
    return super.create(ontology);
  }

  @UserPermissions
  @Override
  public HttpEntity<Ontology> get(@PathVariable String id) {
    return super.get(id);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<Ontology>> list(@ModelAttribute Ontology search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<Ontology> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @EveryonePermissions
  @GetMapping("/" + HederaActionName.LIST_ONTOLOGY_FORMATS)
  public HttpEntity<List<String>> listOntologyFormats() {
    return new ResponseEntity<>(((OntologyService) this.itemService).listOntologyFormats(), HttpStatus.OK);
  }

  @PostMapping("/" + HederaActionName.UPLOAD_ONTOLOGY)
  public HttpEntity<Ontology> createItemAndUploadFile(
          @RequestParam("name") String name,
          @RequestParam("description") String description,
          @RequestParam("version") String version,
          @RequestParam("format") String format,
          @RequestParam(value = "baseUri") String baseUri,
          @RequestParam(value = "url", required = false) String url,
          @RequestParam(HederaConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) throws MalformedURLException, URISyntaxException {
    Ontology ontology = new Ontology();
    ontology.setName(name);
    ontology.setDescription(description);
    ontology.setVersion(version);
    ontology.setBaseUri(new URI(baseUri));
    ontology.setFormat(RdfFormat.valueOf(format));
    if (!StringTool.isNullOrEmpty(url)) {
      ontology.setUrl(new URL(url));
    }
    return super.createItemAndUploadFile(ontology, file, mimeType);
  }

  @Override
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.UPLOAD_FILE)
  public HttpEntity<Ontology> uploadFile(
          @PathVariable String id,
          @RequestParam(HederaConstants.FILE) MultipartFile file,
          @RequestParam(value = SolidifyConstants.MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(id, file, mimeType);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.DOWNLOAD_ONTOLOGY)
  @Override
  public HttpEntity<StreamingResponseBody> downloadFile(@PathVariable String id) {
    return super.downloadFile(id);
  }

  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.DELETE_ONTOLOGY)
  @Override
  public HttpEntity<Ontology> deleteFile(@PathVariable String id) {
    return super.deleteFile(id);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.RDF_CLASS)
  public HttpEntity<List<String>> listRdfClasses(@PathVariable String id) {
    return new ResponseEntity<>(((OntologyService) this.itemService).listRdfClasses(id), HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.RDF_PROPERTY)
  public HttpEntity<List<String>> listRdfProperties(@PathVariable String id) {
    return new ResponseEntity<>(((OntologyService) this.itemService).listRdfProperties(id), HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + HederaActionName.IS_PART_OF + "/{rdfType}")
  public HttpEntity<String> isPartOf(@PathVariable String id, @PathVariable String rdfType) {
    final String type = ((OntologyService) this.itemService).isClassOrProperty(id, rdfType);
    if (StringTool.isNullOrEmpty(type)) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(type, HttpStatus.OK);
  }

}
