/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - TriplestoreSettingsController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller.admin;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.NoSqlResourceController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.service.NoSqlResourceService;

import ch.hedera.controller.AdminController;
import ch.hedera.model.triplestore.DatasetSettings;
import ch.hedera.rest.UrlPath;

@RestController
@ConditionalOnBean(AdminController.class)
@AdminPermissions
@RequestMapping(UrlPath.ADMIN_TRIPLESTORE_SETTINGS)
public class TriplestoreSettingsController extends NoSqlResourceController<DatasetSettings> {

  public TriplestoreSettingsController(NoSqlResourceService<DatasetSettings> triplestoreService) {
    super(triplestoreService);
  }

  @Override
  public HttpEntity<DatasetSettings> create(@RequestBody DatasetSettings datasetInfo) {
    return super.create(datasetInfo);
  }

  @Override
  public HttpEntity<DatasetSettings> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<RestCollection<DatasetSettings>> list(@ModelAttribute DatasetSettings search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }
}
