/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - AdminController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.DependsOn;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.business.IndexFieldAliasService;
import ch.unige.solidify.controller.ModuleController;
import ch.unige.solidify.index.settings.IndexingSettingsService;
import ch.unige.solidify.model.index.IndexSettings;

import ch.hedera.HederaConstants;
import ch.hedera.business.ApplicationRoleService;
import ch.hedera.business.FundingAgencyService;
import ch.hedera.business.InstitutionService;
import ch.hedera.business.LanguageService;
import ch.hedera.business.LicenseService;
import ch.hedera.business.OntologyService;
import ch.hedera.business.ResearchObjectTypeService;
import ch.hedera.business.RmlService;
import ch.hedera.business.RoleService;
import ch.hedera.config.HederaProperties;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.UrlPath;

@RestController
@DependsOn("solidifyEventPublisher")
@ConditionalOnProperty(prefix = "hedera.module.admin", name = "enable")
@RequestMapping(UrlPath.ADMIN)
public class AdminController extends ModuleController {

  AdminController(HederaProperties config,
          LanguageService languageService,
          RoleService roleService,
          LicenseService licenseService,
          FundingAgencyService fundingAgencyService,
          InstitutionService institutionService,
          ApplicationRoleService applicationRoleService,
          OntologyService ontologyService,
          ResearchObjectTypeService researchObjectTypeService,
          RmlService rmlService,
          IndexFieldAliasService indexFieldAliasService,
          IndexingSettingsService<IndexSettings> indexSettingsService) {
    super(ModuleName.ADMIN);

    // Initialize required data
    applicationRoleService.initRequiredData();
    roleService.initRequiredData();

    // Initialize optional data
    if (config.getData().isInit()) {
      languageService.initOptionalData();
      licenseService.initOptionalData();
      fundingAgencyService.initOptionalData();
      institutionService.initOptionalData();
      ontologyService.initOptionalData();
      researchObjectTypeService.initOptionalData(
              ontologyService.findByNameAndVersion(HederaConstants.ONTOLOGY_CIDOC_CRM, "7.1.2"),
              ontologyService.findByNameAndVersion(HederaConstants.ONTOLOGY_CRM_DIG, "3.2.1"));
      rmlService.initOptionalData();
      // Index Field Alias creation
      this.initDataForIndex(indexFieldAliasService, config.getIndexing().getIndexName());
      this.initDataForIndex(indexFieldAliasService, config.getIndexing().getPrivateIndexName());
    }

    if (config.getData().isInitIndexes()) {
      // Index creation
      indexSettingsService.init(config.getIndexing().getIndexDefinitionList());
    }
  }

  private void initDataForIndex(IndexFieldAliasService indexFieldAliasService, String indexName) {
    // Facets for index
    // @formatter:off
    indexFieldAliasService.createIfNotExists(indexName,  10,   HederaConstants.PROJECT_FACET,                  HederaConstants.PROJECT_INDEX_FIELD + HederaConstants.INDEXING_KEYWORD,                  true, true,  1, 10);
    indexFieldAliasService.createIfNotExists(indexName,  20,   HederaConstants.ITEM_TYPE_FACET,                HederaConstants.ITEM_TYPE_INDEX_FIELD+ HederaConstants.INDEXING_KEYWORD,                 true, false, 1, 5);
    indexFieldAliasService.createIfNotExists(indexName,  30,   HederaConstants.RESEARCH_OBJET_TYPE_FACET,      HederaConstants.RESEARCH_OBJET_TYPE_NAME_INDEX_FIELD + HederaConstants.INDEXING_KEYWORD, true, true,  1, 10);
    indexFieldAliasService.createIfNotExists(indexName,  40,   HederaConstants.RESEARCH_OBJET_RDF_TYPE_FACET,  HederaConstants.RESEARCH_OBJET_RDF_TYPE_INDEX_FIELD + HederaConstants.INDEXING_KEYWORD,  true, false, 1, 10);
    indexFieldAliasService.createIfNotExists(indexName,  50,   HederaConstants.OBJECT_TYPE_FACET,              HederaConstants.OBJECT_TYPE_INDEX_FIELD + HederaConstants.INDEXING_KEYWORD,              true, false, 1, 10);
    indexFieldAliasService.createIfNotExists(indexName,  60,   HederaConstants.CONTENT_TYPE_FACET,             HederaConstants.CONTENT_TYPE_INDEX_FIELD + HederaConstants.INDEXING_KEYWORD,             true, false, 4, 10);
    indexFieldAliasService.createIfNotExists(indexName,  70,   HederaConstants.ONTOLOGY_FACET,                 HederaConstants.ONTOLOGY_NAME_INDEX_FIELD + HederaConstants.INDEXING_KEYWORD,            true, false, 4, 10);
    indexFieldAliasService.createIfNotExists(indexName,  80,   HederaConstants.ONTOLOGY_VERSION_FACET,         HederaConstants.ONTOLOGY_VERSION_INDEX_FIELD + HederaConstants.INDEXING_KEYWORD,         true, false, 5, 10);

    indexFieldAliasService.createIfNotExists(indexName,  null, HederaConstants.INDEX_FIELD_URI_SORT_ALIAS,     HederaConstants.INDEX_FIELD_URI_SORT,    false, true, null, null);
    indexFieldAliasService.createIfNotExists(indexName,  null, HederaConstants.INDEX_FIELD_OBJECT_DATE_ALIAS,  HederaConstants.INDEX_FIELD_OBJECT_DATE, false, true, null, null);
    // @formatter:on
  }
}
