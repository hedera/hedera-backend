/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - MonitorService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import org.json.JSONObject;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.client.service.AuthorizationClientProperties;
import ch.unige.solidify.service.FallbackRestClientService;
import ch.unige.solidify.service.MessageService;

import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AdminController;

@Service
@ConditionalOnBean(AdminController.class)
public class MonitorService extends HederaService {

  private final FallbackRestClientService restClientService;
  private final String authServerUrl;
  private final String adminUrl;
  private final String ingestUrl;
  private final String accessUrl;

  public MonitorService(AuthorizationClientProperties authconfig, HederaProperties config, MessageService messageService,
          FallbackRestClientService restClientService) {
    super(messageService);
    this.authServerUrl = authconfig.getAuthorizationServerUrl();
    this.adminUrl = config.getModule().getAdmin().getUrl();
    this.ingestUrl = config.getModule().getIngest().getUrl();
    this.accessUrl = config.getModule().getAccess().getUrl();
    this.restClientService = restClientService;
  }

  public JSONObject getModuleStatuses() {
    final JSONObject moduleStatus = new JSONObject();
    // Modules with unique instance
    moduleStatus.put("auth", this.getModuleStatus(this.authServerUrl));
    moduleStatus.put("admin", this.getModuleStatus(this.adminUrl));
    moduleStatus.put("ingest", this.getModuleStatus(this.ingestUrl));
    moduleStatus.put("access", this.getModuleStatus(this.accessUrl));
    return moduleStatus;
  }

  private JSONObject getModuleStatus(String url) {
    try {
      return new JSONObject(this.restClientService.getResource(url)).put("status", "UP");
    } catch (final Exception e) {
      return new JSONObject().put("status", "DOWN");
    }
  }

}
