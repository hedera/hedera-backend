/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - UserPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.security;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.service.security.SolidifyPermissionService;

import ch.hedera.business.UserService;
import ch.hedera.controller.AdminController;
import ch.hedera.model.security.User;

@Service
@ConditionalOnBean(AdminController.class)
public class UserPermissionService implements SolidifyPermissionService {

  private final UserService userService;

  public UserPermissionService(UserService userService) {
    this.userService = userService;
  }

  public boolean isAllowedToUpdate(String userId, Map<String, Object> updateMap) {
    if (this.isUserRole()) {
      return false;
    }
    if (this.isRootOrTrustedRole()) {
      return !(this.userService.containsApplicationRoleId(updateMap)
              && this.userService.getCurrentUser().getResId().equals(userId));
    }
    if (this.isAdminRole()) {
      final User beforeUpdateUser = this.userService.findOne(userId);
      final String beforeUpdateApplicationRole = beforeUpdateUser.getApplicationRole().getResId();
      final String afterUpdateApplicationRole = this.userService.getApplicationRoleId(updateMap);
      if (!this.userService.containsApplicationRoleId(updateMap)) {
        return true;
      } else if (beforeUpdateApplicationRole.equals(afterUpdateApplicationRole)) {
        return true;
      } else {
        return !(this.userService.getCurrentUser().getResId().equals(userId)
                || AuthApplicationRole.ROOT.getResId().equals(beforeUpdateApplicationRole)
                || AuthApplicationRole.ROOT.getResId().equals(afterUpdateApplicationRole));
      }
    }
    return false;
  }

}
