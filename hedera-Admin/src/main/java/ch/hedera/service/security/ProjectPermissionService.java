/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - ProjectPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.security;

import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.hedera.business.ProjectService;
import ch.hedera.controller.AdminController;
import ch.hedera.controller.HederaControllerAction;
import ch.hedera.model.ProjectAwareResource;
import ch.hedera.model.security.Role;
import ch.hedera.service.rest.trusted.TrustedPersonRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;

@Service
@ConditionalOnBean(AdminController.class)
public class ProjectPermissionService extends AbstractPermissionWithProjectService {

  private final ProjectService projectService;

  private final TrustedProjectRemoteResourceService trustedProjectRemoteResourceService;

  public ProjectPermissionService(ProjectService projectService,
          TrustedPersonRemoteResourceService personRemoteResourceService,
          TrustedProjectRemoteResourceService trustedProjectRemoteResourceService) {
    super(personRemoteResourceService);
    this.projectService = projectService;
    this.trustedProjectRemoteResourceService = trustedProjectRemoteResourceService;
  }

  @Override
  protected boolean isManagerAllowed(ProjectAwareResource existingResource, HederaControllerAction action) {
    return true;
  }

  @Override
  protected boolean isCreatorAllowed(ProjectAwareResource existingResource, HederaControllerAction action) {
    return this.isActionAllowedForEveryMember(action);
  }

  @Override
  protected boolean isVisitorAllowed(ProjectAwareResource existingResource, HederaControllerAction action) {
    return this.isActionAllowedForEveryMember(action);
  }

  private boolean isActionAllowedForEveryMember(HederaControllerAction action) {
    return action == HederaControllerAction.LIST_MEMBERS ||
            action == HederaControllerAction.GET_MEMBER ||
            action == HederaControllerAction.LIST_FORMS ||
            action == HederaControllerAction.GET_FORM ||
            action == HederaControllerAction.GET_CURRENT_FORM;
  }

  @Override
  ProjectAwareResource getExistingResource(String resId) {
    return this.projectService.findOne(resId);
  }

  public boolean isProjectManager(String projectId) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    final String personId = this.getPersonId();
    final Optional<Role> projectRole = this.trustedPersonRemoteResourceService.findProjectRole(personId, projectId);
    return projectRole.map(role -> role.equals(Role.MANAGER)).orElse(false);
  }

  public boolean isCurrentUserProjectMember(String projectId) {
    return this.isCurrentUserProjectMember(projectId, this.getPersonId());
  }

  public boolean isCurrentUserProjectMember(String projectId, String personId) {
    final String callerPersonId = this.getPersonId();
    if (!callerPersonId.equals(personId)) {
      return false;
    }
    return (this.trustedProjectRemoteResourceService.getAuthorizedProject(projectId)).isPresent();
  }

}
