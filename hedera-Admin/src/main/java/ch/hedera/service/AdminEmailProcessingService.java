/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - AdminEmailProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import jakarta.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.EmailService;
import ch.unige.solidify.service.MessageService;

import ch.hedera.HederaConstants;
import ch.hedera.business.PersonService;
import ch.hedera.business.ProjectService;
import ch.hedera.config.HederaProperties;
import ch.hedera.controller.AdminController;
import ch.hedera.message.EmailMessage;

@Service
@ConditionalOnBean(AdminController.class)
@Profile("email-service")
public class AdminEmailProcessingService extends EmailProcessingService {
  private static final Logger log = LoggerFactory.getLogger(AdminEmailProcessingService.class);

  private final ProjectService projectService;
  private final PersonService personService;
  private static final String ORG_UNITS_CONSTANT = "projects";

  protected AdminEmailProcessingService(MessageService messageService, EmailService emailService,
          HederaProperties properties, ProjectService projectService, PersonService personService) {
    super(messageService, emailService, properties);
    this.projectService = projectService;
    this.personService = personService;
  }

  @Override
  public void processEmailMessage(EmailMessage emailMessage) {
    log.info("Reading message by Admin module {}", emailMessage);
    Map<String, Object> parameterList;
    try {
      switch (emailMessage.getTemplate()) {
        case NEW_SUBSCRIPTION, REMOVE_SUBSCRIPTION -> {
          parameterList = this.getEmailDefaultParameters();
          parameterList.put("notificationTypes", emailMessage.getParameters().get(HederaConstants.NOTIFICATION_IDS));
          this.emailService.sendEmailWithTemplate(emailMessage.getTemplate().toString().toLowerCase(),
                  Collections.singletonList(emailMessage.getTo()), emailMessage.getTemplate().getSubject(), parameterList);
        }
        default -> {
        }
      }
    } catch (MessagingException e) {
      log.error("Cannot send email ({}): {}", emailMessage.getTemplate().getSubject(), e.getMessage(), e);
    }
  }

  private void sendEmailWithProjectName(EmailMessage emailMessage) throws MessagingException {
    Map<String, Object> parameterList;

    parameterList = this.getEmailDefaultParameters();
    parameterList.put(ORG_UNITS_CONSTANT, emailMessage.getParameters());
    List<String> projectNames = new ArrayList<>();
    List<String> personNames = new ArrayList<>();
    for (String projecttId : emailMessage.getParameters().keySet()) {
      projectNames.add(this.projectService.findOne(projecttId).getName());
      Map<String, String> personRoleParams = (Map<String, String>) emailMessage.getParameters().get(projecttId);
      personRoleParams.keySet().forEach(pr -> personNames.add(this.personService.findOne(pr).getFullName()));
    }
    parameterList.put("projectNames", projectNames);
    parameterList.put("personNames", personNames);

    this.emailService.sendEmailWithTemplate(emailMessage.getTemplate().toString().toLowerCase(),
            Collections.singletonList(emailMessage.getTo()), emailMessage.getTemplate().getSubject(), parameterList);
  }
}
