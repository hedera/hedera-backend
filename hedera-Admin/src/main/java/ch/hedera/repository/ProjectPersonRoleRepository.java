/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Admin - ProjectPersonRoleRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.JoinRepository;

import ch.hedera.controller.AdminController;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.ProjectPersonRole;

@Repository
@ConditionalOnBean(AdminController.class)
public interface ProjectPersonRoleRepository extends JoinRepository<ProjectPersonRole> {

  @Query("""
          SELECT r FROM Role r JOIN r.projectPersons oupr
          WHERE oupr.compositeKey.project.resId = :projectId
          AND oupr.compositeKey.person.resId = :personId
          """)
  List<Role> findByProjectAndPerson(String projectId, String personId);

}
