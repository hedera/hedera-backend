#!/bin/sh
cd DLCM-IntegrationTests/
mvn -U -s settings.xml -Dspring.application.name=DLCM-IntegrationTestsRunner -Dspring.cloud.config.uri=http://configserver:8887 -Dspring.cloud.config.label=${LABEL} -Dspring.cloud.config.enabled=true -Dspring.cloud.config.fail-fast=true -Dspring.profiles.include=ff-notool,vir-notool verify
sleep 1000000d
