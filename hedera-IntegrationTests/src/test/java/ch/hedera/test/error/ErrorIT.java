/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ErrorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.error;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.util.AbstractRestClientTool;

import ch.hedera.model.settings.Project;
import ch.hedera.rest.ResourceName;

@ActiveProfiles("client-sudo")
@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = { "spring.cloud.bootstrap.enabled=true" })
class ErrorIT {

  private static final Logger log = LoggerFactory.getLogger(ErrorIT.class);

  @Autowired
  protected AbstractRestClientTool restClientTool;

  @Value("${hedera.module.admin.url}")
  private String adminUrl;

  @Test
  void badRequest() {
    final String url = this.adminUrl + "/" + ResourceName.PROJECT;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    assertThrows(HttpClientErrorException.BadRequest.class, () -> restTemplate.postForObject(url, null, String.class));
  }

  @Test
  void forbidden() {
    final String url = this.adminUrl + "/" + ResourceName.PROJECT;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(new Project()), Project.class));
  }

  @Test
  void methodNotAllowed() {
    final RestTemplate restTemplate = this.restClientTool.getClient();
    assertThrows(HttpClientErrorException.MethodNotAllowed.class,
            () -> restTemplate.exchange(this.adminUrl, HttpMethod.PUT, new HttpEntity<>(""), String.class));
  }

  @Test
  void notFound() {
    final String url = this.adminUrl + "doesntexists";
    final RestTemplate restTemplate = this.restClientTool.getClient();
    assertThrows(HttpClientErrorException.NotFound.class, () -> restTemplate.getForObject(url, String.class));
  }

  @Test
  void unauthorized() {
    try {
      final URL url = new URL(this.adminUrl);
      String port = "";
      final int portNumber = url.getPort();
      if (portNumber != -1) {
        port = String.valueOf(portNumber);
      }
      final String urlWithoutUserInfo = url.getProtocol() + "://" + url.getHost() + ":" + port + url.getPath();
      final RestTemplate restTemplate = new RestTemplate();
      assertThrows(HttpClientErrorException.Unauthorized.class, () -> restTemplate.getForObject(urlWithoutUserInfo, String.class));
    } catch (final MalformedURLException e) {
      fail("failed to compute url", e);
    }
  }

}
