/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - InitDataIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.init;

import static ch.hedera.test.HederaTestConstants.THIRD_PARTY_USER_ID;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.model.humanities.Rml;
import ch.hedera.model.humanities.RmlFormat;
import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.DatasetFileType;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.security.Role;
import ch.hedera.model.security.User;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.service.access.IIIFClientService;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.OntologyClientService;
import ch.hedera.service.admin.TriplestoreSettingsClientService;
import ch.hedera.service.admin.UserClientService;
import ch.hedera.service.ingest.RdfDatasetFileClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.HederaTestConstants.ProjectStatus;
import ch.hedera.test.service.PersonITService;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.RmlITService;
import ch.hedera.test.service.SourceDatasetITService;

@Order(Integer.MIN_VALUE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class InitDataIT extends AbstractInitDataIT {

  private final FundingAgencyClientService fundingAgencyClientService;
  private final UserClientService userClientService;
  private final ProjectITService projectITService;
  private final OntologyClientService ontologyClientService;
  private final ResearchObjectTypeITService researchObjectTypeITService;
  private final PersonITService personITService;
  private final RmlITService rmlITService;
  private final SourceDatasetITService sourceDatasetITService;
  private final SourceDatasetClientService sourceDatasetClientService;
  private final RdfDatasetFileClientService rdfDatasetFileClientService;
  private final ResearchDataFileClientService researchDataFileClientService;
  private final TriplestoreSettingsClientService triplestoreSettingsClientService;
  private final IIIFClientService iiifClientService;

  @Autowired
  public InitDataIT(
          Environment env,
          SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyClientService,
          UserClientService userClientService,
          SourceDatasetClientService sourceDatasetClientService,
          RdfDatasetFileClientService rdfDatasetFileClientService,
          ProjectITService projectITService, OntologyClientService ontologyClientService,
          ResearchObjectTypeITService researchObjectTypeITService,
          PersonITService personITService,
          RmlITService rmlITService,
          SourceDatasetITService sourceDatasetITService,
          ResearchDataFileClientService researchDataFileClientService,
          TriplestoreSettingsClientService triplestoreSettingsClientService,
          IIIFClientService iiifClientService) {
    super(env, restClientTool);
    this.fundingAgencyClientService = fundingAgencyClientService;
    this.userClientService = userClientService;
    this.sourceDatasetClientService = sourceDatasetClientService;
    this.rdfDatasetFileClientService = rdfDatasetFileClientService;
    this.projectITService = projectITService;
    this.ontologyClientService = ontologyClientService;
    this.researchObjectTypeITService = researchObjectTypeITService;
    this.personITService = personITService;
    this.rmlITService = rmlITService;
    this.sourceDatasetITService = sourceDatasetITService;
    this.researchDataFileClientService = researchDataFileClientService;
    this.triplestoreSettingsClientService = triplestoreSettingsClientService;
    this.iiifClientService = iiifClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Order(10)
  @Test
  void createUsers() {
    // Make a REST call with test users, in order to present their tokens to the hedera application and create their users
    this.restClientTool.sudoUser();
    this.fundingAgencyClientService.findAll();
    this.restClientTool.exitSudo();

    this.restClientTool.sudoAdmin();
    this.fundingAgencyClientService.findAll();
    this.restClientTool.exitSudo();

    this.restClientTool.sudoRoot();
    this.fundingAgencyClientService.findAll();
    this.restClientTool.exitSudo();

    this.restClientTool.sudoThirdPartyUser();
    this.fundingAgencyClientService.findAll();
    this.restClientTool.exitSudo();

    // Set last login time and IP for testing purposes
    final User user = this.userClientService.findAll().stream().filter(u -> u.getFirstName().contains(THIRD_PARTY_USER_ID)).findFirst()
            .orElseThrow(() -> new NoSuchElementException("Missing test user " + THIRD_PARTY_USER_ID));
    user.setLastLoginTime(OffsetDateTime.now());
    user.setLastLoginIpAddress("8.8.8.8");
    this.userClientService.update(user.getResId(), user);

    List<User> list = this.userClientService.searchByProperties(Map.of("lastName", HederaTestConstants.PERMANENT_TEST_DATA_LABEL));
    assertEquals(4, list.size());
  }

  @Order(20)
  @Test
  void verifyUserPerson() {
    final List<Person> personList = this.personITService.findAll();
    final List<User> userList = this.userService.findAll();
    final List<String> roleIds = new ArrayList<>(
            Arrays.asList(AuthApplicationRole.ADMIN_ID, AuthApplicationRole.USER_ID, AuthApplicationRole.ROOT_ID));
    for (String roleId : roleIds) {
      List<Person> rolePersons = personList.stream()
              .filter(person -> person.getFirstName().toLowerCase()
                      .contains((HederaTestConstants.PERMANENT_TEST_DATA_LABEL + roleId).toLowerCase()))
              .toList();
      List<User> roleUsers = userList.stream()
              .filter(user -> user.getFirstName().toLowerCase().contains((HederaTestConstants.PERMANENT_TEST_DATA_LABEL + roleId).toLowerCase()))
              .toList();
      assertEquals(1, rolePersons.size(), "There must be exactly one " + roleId + " person");
      assertEquals(1, roleUsers.size(), "There must be exactly one " + roleId + " user");
      assertNotNull(roleUsers.get(0).getPerson(), "The role " + roleId + " must have a person connected to its user");
      assertEquals(roleId, roleUsers.get(0).getApplicationRole().getResId(), "User for " + roleId + " has not a " + roleId + " role");
    }
  }

  @Order(30)
  @Test
  void createResearchObjectTypes() {
    Ontology cidocCrmOnto = this.ontologyClientService.searchByProperties(Map.of("name", HederaConstants.ONTOLOGY_CIDOC_CRM, "version", "7.1.2"))
            .get(0);
    assertNotNull(
            this.researchObjectTypeITService.createRemoteResearchObjectType("cidoc-crm-pliego", "pliego", "E22_Human-Made_Object", "Pliego",
                    cidocCrmOnto));

  }

  @Order(40)
  @Test
  void verifyProject() {
    this.initPersonProjectRole();
    this.projectITService.getOrCreatePermanentProjectForNoOne();
    this.projectITService.getSecondPermanentProject();
    this.projectITService.getClosedPermanentProject();
    this.projectITService.getPublicPermanentProject();
    this.projectITService.getPrivatePermanentProject();
    List<Project> list = this.projectITService.findProjects(HederaTestConstants.PERMANENT_TEST_DATA_LABEL);
    assertEquals(8, list.size());
    list.forEach(project -> assertDoesNotThrow(() -> this.triplestoreSettingsClientService.findOne(project.getShortName()),
            String.format("Dataset is missing on triple store for project %s", project.getShortName())));
  }

  @Order(50)
  @Test
  void verifyPermanentRML() {
    this.createRml(HederaTestConstants.DEMO_RML_NAME, HederaTestConstants.DEMO_RML_FILE_TO_UPLOAD);
    this.createRml(HederaTestConstants.TEST_RML_NAME, HederaTestConstants.TEST_RML_FILE_TO_UPLOAD);
    final List<Rml> rmls = this.rmlITService.searchByProperties(Map.of("name", HederaTestConstants.PERMANENT_TEST_DATA_LABEL));
    assertEquals(2, rmls.size());
  }

  private void createRml(String rmlName, String rmlFile) {
    List<Rml> rmls = this.rmlITService
            .searchByProperties(Map.of("name", HederaTestConstants.getNameWithPermanentLabel(rmlName)));
    if (rmls.isEmpty()) {
      // Create a rml
      this.rmlITService.createRemoteRml(HederaTestConstants.getNameWithPermanentLabel(rmlName),
              RmlFormat.XML, "1",
              "RML for testing",
              new ClassPathResource(rmlFile));
    }
  }

  @Order(60)
  @Test
  void verifyPermanentData() {
    final Project publicProject = this.projectITService.getPublicPermanentProject();
    assertNotNull(publicProject);
    this.addProjectData(publicProject);
    final Project privateProject = this.projectITService.getPrivatePermanentProject();
    assertNotNull(privateProject);
    this.addProjectData(privateProject);
  }

  @Order(100)
  @Test
  void refreshIIIFMetadata() {
    this.restClientTool.sudoRoot();
    assertDoesNotThrow(() -> this.iiifClientService.refreshIIIF());
    this.restClientTool.exitSudo();
  }

  private void addProjectData(Project project) {
    // Research Object Type
    final List<String> researchObjetcTypeList = List.of(
            HederaConstants.RESEARCH_OBJECT_TYPE_E22,
            HederaConstants.RESEARCH_OBJECT_TYPE_E24,
            HederaConstants.RESEARCH_OBJECT_TYPE_E21,
            HederaConstants.RESEARCH_OBJECT_TYPE_E53,
            HederaConstants.RESEARCH_OBJECT_TYPE_E41);
    this.projectITService.addResearchObjectType(project.getProjectId(), researchObjetcTypeList);

    // Add rml mapping
    final Rml rmlMapping = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);

    // Create a sourceDataset
    SourceDataset sourceDataset;

    // Check if the sourceDataset exist
    List<SourceDataset> sourceDatasets = this.sourceDatasetClientService
            .searchByProperties(Map.of(
                    "name", HederaTestConstants.SOURCE_DATASET_NAME,
                    HederaConstants.PROJECT_ID_FIELD, project.getResId()));
    if (sourceDatasets.isEmpty()) {
      sourceDataset = this.sourceDatasetClientService.create(this.createSourceDataset(project));
    } else {
      sourceDataset = sourceDatasets.stream().findFirst().get();
    }

    // Get or create sourceDatasetFile
    for (SourceDatasetFile sourceDatasetFile : this.createOrGetSourceDataseFile(sourceDataset)) {
      // Apply rml to generate RdfDatasetFile only if the file has not been transformed
      if (sourceDatasetFile.getStatus() != SourceDatasetFile.SourceDatasetFileStatus.TRANSFORMED) {
        final Result result = this.sourceDatasetClientService.applyRml(sourceDataset.getResId(), sourceDatasetFile.getResId(),
                rmlMapping.getResId());
        assertEquals(ActionStatus.EXECUTED, result.getStatus());
        this.sourceDatasetITService.waitForSourceDatasetFileIsTransformed(sourceDataset.getResId(), sourceDatasetFile.getResId());
      }

      // add rdfFile to triplestore
      RdfDatasetFile rdfDatasetFile1 = this.rdfDatasetFileClientService.findAllWithProjectId(project.getResId()).stream()
              .filter(e -> e.getSourceDatasetFile().getResId().equals(sourceDatasetFile.getResId()))
              .findFirst().get();

      // Add to tripleStore only if not imported
      if (rdfDatasetFile1.getStatusImport() == RdfDatasetFile.RdfDatasetFileStatus.NOT_IMPORTED) {
        this.rdfDatasetFileClientService.addInTripleStore(rdfDatasetFile1.getResId(), project.getProjectId());
      }
    }
    // Upload content
    final List<String> researchDataFiles = List.of("Varios_001_1.jpg", "Varios_001_2.jpg", "Varios_002_1.jpg", "Varios_002_2.jpg",
            HederaTestConstants.FIRST_SOURCE_DATASET_FILE_FILENAME, HederaTestConstants.SECOND_SOURCE_DATASET_FILE_FILENAME);
    for (String researchDataFile : researchDataFiles) {
      List<ResearchDataFile> list = this.researchDataFileClientService
              .searchByProperties(Map.of("fileName", researchDataFile, HederaConstants.PROJECT_ID_FIELD, project.getResId()));
      if (list.isEmpty()) {
        DataAccessibilityType dataType = DataAccessibilityType.IIIF;
        if (researchDataFile.endsWith(".xml")) {
          dataType = DataAccessibilityType.TEI;
        }
        this.researchDataFileClientService.uploadFile(
                new ClassPathResource(researchDataFile),
                project.getProjectId(),
                dataType.toString(),
                "/");
      }

    }
  }

  private void initPersonProjectRole() {
    // Needed to create project for each role as ApplicationRole User
    final List<Role> listRoles = this.roleService.findAll();
    for (final Role role : listRoles) {
      this.projectITService.getOrCreatePermanentProject(ProjectStatus.OPEN, role.getName().toUpperCase(), AuthApplicationRole.USER);
    }
  }

  private Rml addRml(Project project, String rmlName) {
    List<Rml> rmls = this.rmlITService
            .searchByProperties(Map.of("name", HederaTestConstants.getNameWithPermanentLabel(rmlName)));
    final Rml rml = rmls.get(0);

    // Add the Rml to the project
    List<Rml> rmlList = this.projectITService.getRmls(project.getResId());
    Optional<Rml> foundRml = rmlList.stream().filter(r -> r.getResId().equals(rml.getResId())).findFirst();
    if (foundRml.isEmpty()) {
      this.projectITService.addRml(project.getResId(), rml.getResId());
    }
    return rml;
  }

  private SourceDataset createSourceDataset(Project project) {
    return this.sourceDatasetITService.createPermanentLocalSourceDataset(project.getResId());
  }

  private List<SourceDatasetFile> createOrGetSourceDataseFile(SourceDataset sourceDataset) {
    List<SourceDatasetFile> sourceDataFile = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());
    if (sourceDataFile.isEmpty()) {
      this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
              new ClassPathResource("Moreno_001.xml"), DatasetFileType.XML.getName(), "1.0");
      this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
              new ClassPathResource("Moreno_002.xml"), DatasetFileType.XML.getName(), "1.0");
    }
    return this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());
  }
}
