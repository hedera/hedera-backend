/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AbstractIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test;

import static ch.hedera.HederaConstants.NO_REPLY_PREFIX;
import static ch.hedera.test.HederaTestConstants.THIRD_PARTY_USER_ID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.security.User;
import ch.hedera.service.admin.IndexFieldAliasClientService;
import ch.hedera.service.admin.LicenseClientService;
import ch.hedera.service.admin.RoleClientService;
import ch.hedera.service.admin.UserClientService;
import ch.hedera.service.ingest.RdfDatasetFileClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;

@ActiveProfiles("client-sudo")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = { "spring.cloud.bootstrap.enabled=true" })
public abstract class AbstractIT {

  protected final static String NO_ONE = "no_one";

  @Autowired
  protected LicenseClientService licenseService;

  @Autowired
  protected RoleClientService roleService;

  @Autowired
  protected UserClientService userService;

  @Autowired
  protected RdfDatasetFileClientService rdfDatasetFileClientService;

  @Autowired
  protected ResearchDataFileClientService researchDataFileClientService;

  @Autowired
  protected IndexFieldAliasClientService indexFieldAliasService;

  protected SudoRestClientTool restClientTool;

  protected Environment env;

  public AbstractIT(Environment env, SudoRestClientTool restClientTool) {
    this.env = env;
    this.restClientTool = restClientTool;
  }

  @BeforeEach
  protected void setup() {
    this.setUser();
    this.createFixtures();
  }

  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  protected void createFixtures() {
    // Do nothing
  }

  protected void assertEqualsWithoutNanoSeconds(OffsetDateTime time1, OffsetDateTime time2) {
    assertEquals(time1.withNano(0), time2.withNano(0));
  }

  protected void assertEqualsWithoutNanoSeconds(String message, OffsetDateTime time1, OffsetDateTime time2) {
    assertEquals(time1.withNano(0), time2.withNano(0), message);
  }

  public boolean waitForRdfDatasetFileIsProcessed(String projectId, String sourceDatasetFileId) {
    int count = 0;
    List<RdfDatasetFile> rdfFilesWithProjectId;
    do {
      rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(projectId);
      HederaTestConstants.failAfterNAttempt(count, "The RdfDatasetFile is not ready for Project " + projectId);
      count++;
    } while (rdfFilesWithProjectId.stream().noneMatch(e -> e.getSourceDatasetFile().getResId().equals(sourceDatasetFileId)));
    return true;
  }

  protected void clearLicenseFixtures() {
    this.licenseService.findAll().stream()
            .filter(license -> license.getOpenLicenseId().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(license -> this.licenseService.delete(license.getResId()));
  }

  /**
   * Delete fixtures created by tests
   */
  protected abstract void deleteFixtures();

  /***********************************/

  protected User findThirdPartyUser() {
    return this.userService.searchByProperties(Map.of("email", NO_REPLY_PREFIX + THIRD_PARTY_USER_ID + "@unige.ch")).get(0);
  }

  protected List<Map<String, Object>> getValidationErrors(HttpClientErrorException httpClientErrorException) {
    try {
      String responseBody = httpClientErrorException.getResponseBodyAsString();
      Map<String, Object> responseJsonMap = new ObjectMapper().readValue(responseBody, Map.class);
      return (List<Map<String, Object>>) responseJsonMap.get("validationErrors");
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("unable to get validationErrors from HttpClientErrorException", e);
    }
  }

  protected List<String> getFieldValidationErrors(HttpClientErrorException httpClientErrorException, String fieldName) {
    List<Map<String, Object>> validationErrors = this.getValidationErrors(httpClientErrorException);
    if (validationErrors != null) {
      Optional<Map<String, Object>> fieldValidationErrorOpt = validationErrors.stream().filter(map -> map.get("fieldName").equals(fieldName))
              .findFirst();
      if (fieldValidationErrorOpt.isPresent()) {
        return (List<String>) fieldValidationErrorOpt.get().get("errorMessages");
      } else {
        return new ArrayList<>();
      }
    }
    return new ArrayList<>();
  }

  protected boolean fieldHasValidationError(HttpClientErrorException httpClientErrorException, String fieldName) {
    return !this.getFieldValidationErrors(httpClientErrorException, fieldName).isEmpty();
  }

  protected void assertFieldContainsValidationErrorMessage(HttpClientErrorException e, String fieldName, String expectedErrorMessage) {
    List<String> errorMessages = this.getFieldValidationErrors(e, fieldName);
    assertTrue(errorMessages.contains(expectedErrorMessage), errorMessages + " doesn't contain the message '" + expectedErrorMessage + "'");
  }

  protected void assertFieldContainsValidationErrorMessageSubstring(HttpClientErrorException e, String fieldName, String expectedErrorMessage) {
    boolean substringFound = false;
    List<String> errorMessages = this.getFieldValidationErrors(e, fieldName);
    for (String errorMessage : errorMessages) {
      if (errorMessage.contains(expectedErrorMessage)) {
        substringFound = true;
        break;
      }
    }
    assertTrue(substringFound, errorMessages + " doesn't contain the message '" + expectedErrorMessage + "'");
  }

  protected long checkFile(Path file) {
    assertNotNull(file.toFile());
    assertTrue(FileTool.checkFile(file));
    long size = FileTool.getSize(file);
    assertNotEquals(0, size);
    return size;
  }

  protected long checkFile(ClassPathResource classPathResource) {
    long size = 0;
    try {
      size = classPathResource.contentLength();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    assertNotEquals(0, size);
    return size;
  }

  @AfterEach
  public void tearDown() {
    this.deleteFixtures();
    this.restClientTool.exitSudo();
  }
}
