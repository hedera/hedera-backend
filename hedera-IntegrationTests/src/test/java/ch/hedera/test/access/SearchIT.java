/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - SearchIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.access;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.core.util.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.service.access.OAISetClientService;
import ch.hedera.service.access.PublicProjectClientService;
import ch.hedera.service.access.SearchClientService;
import ch.hedera.service.admin.IndexFieldAliasClientService;

public class SearchIT extends AbstractAccessIT {

  private final SearchClientService searchClientService;

  @Autowired
  public SearchIT(Environment env,
          SudoRestClientTool restClientTool,
          OAISetClientService oaiSetService,
          PublicProjectClientService publicProjectService,
          IndexFieldAliasClientService indexFieldAliasService,
          SearchClientService searchClientService) {
    super(env, restClientTool, oaiSetService, publicProjectService, indexFieldAliasService);
    this.searchClientService = searchClientService;
  }

  @Test
  void getResearchObject() {
    this.restClientTool.sudoRoot();
    RestCollection<ResearchObjectMetadata> listResults = this.searchClientService.list();
    Assert.isNonEmpty(listResults);
    this.restClientTool.exitSudo();

    // Get a research object from a public project
    Optional<ResearchObjectMetadata> roPublicOpt = listResults.getData().stream().filter(ro -> (Boolean) ((Map) ro.getMetadata()
            .get("researchProject")).get("accessPublic")).findFirst();
    if (roPublicOpt.isPresent()) {
      ResearchObjectMetadata researchObjectMetadata = this.searchClientService.get(roPublicOpt.get().getResId());
      assertNotNull(researchObjectMetadata);
      assertNotNull(researchObjectMetadata.getMetadata());
      assertTrue((Boolean) ((Map<String, Object>) researchObjectMetadata.getMetadata().get("researchProject")).get("accessPublic"));
    }
  }

  @Test
  void getResearchObjectOfPrivateProjectWithoutRight() {
    this.restClientTool.sudoRoot();
    RestCollection<ResearchObjectMetadata> listResults = this.searchClientService.list();
    Assert.isNonEmpty(listResults);
    this.restClientTool.exitSudo();

    // Get a research object from a public project
    Optional<ResearchObjectMetadata> roPublicOpt = listResults.getData().stream().filter(ro -> ((Map) ro.getMetadata()
            .get("researchProject")).get("accessPublic") == Boolean.FALSE).findFirst();
    if (roPublicOpt.isPresent()) {
      assertThrows(HttpClientErrorException.NotFound.class, () -> this.searchClientService.get(roPublicOpt.get().getResId()));
    }
  }

  @Test
  void listForRootOrTrustedClients() {
    this.restClientTool.sudoRoot();
    RestCollection<ResearchObjectMetadata> listResults = this.searchClientService.list();
    assertNotNull(listResults);
    assertFalse(listResults.getData().isEmpty());
    this.restClientTool.exitSudo();
  }

  @Test
  void listForbiddenForNormalUsers() {
    assertThrows(HttpClientErrorException.Forbidden.class, this.searchClientService::list);
  }

  @Test
  void listForbiddenForAdminUsers() {
    this.restClientTool.sudoAdmin();
    assertThrows(HttpClientErrorException.Forbidden.class, this.searchClientService::list);
    this.restClientTool.exitSudo();
  }
}
