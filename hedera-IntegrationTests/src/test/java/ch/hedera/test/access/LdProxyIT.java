/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - LdProxyIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.access;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.RdfFormat;
import ch.hedera.model.access.PublicProject;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.service.access.LdProxyClientService;
import ch.hedera.service.access.PublicProjectClientService;
import ch.hedera.test.AbstractIT;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.util.RdfTool;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class LdProxyIT extends AbstractIT {

  private final ProjectITService projectITService;
  private final PublicProjectClientService projectClientService;
  private final LdProxyClientService ldProxyClientService;

  @Override
  @BeforeEach
  protected void setup() {
    final Project privateProject = this.projectITService.getPrivatePermanentProject();
    this.restClientTool.sudoAdmin();
    this.projectITService.getPeople(privateProject.getResId())
            .forEach(person -> this.projectITService.removePerson(privateProject.getResId(), person.getResId()));
    this.restClientTool.exitSudo();
  }

  @Autowired
  LdProxyIT(
          Environment env,
          SudoRestClientTool restClientTool,
          ProjectITService projectITService,
          PublicProjectClientService projectClientService,
          LdProxyClientService ldProxyClientService) {
    super(env, restClientTool);
    this.projectITService = projectITService;
    this.projectClientService = projectClientService;
    this.ldProxyClientService = ldProxyClientService;
  }

  @Test
  @Order(10)
  void listProjects() {
    final RestCollection<PublicProject> projects = this.projectClientService.list();
    assertFalse(projects.getData().isEmpty());
    final List<PublicProject> projectList = this.ldProxyClientService.listProjects();
    assertFalse(projectList.isEmpty());
    assertEquals(projects.getData().size(), projectList.size());
    for (PublicProject project : projectList) {
      Project projectFromAdmin = this.projectITService.findOneByShortName(project.getShortName());
      // assertTrue(projectFromAdmin.isAccessPublic());
      PublicProject projectByShortName = this.ldProxyClientService.getProject(project.getShortName());
      assertEquals(project.getShortName(), projectByShortName.getShortName());
      assertEquals(project.getName(), projectByShortName.getName());
      assertEquals(project.getName(), projectFromAdmin.getName());
    }
  }

  @Test
  @Order(20)
  void listResearchObjectTypes() {
    for (PublicProject project : this.ldProxyClientService.listProjects()) {
      final List<String> researchObjectTypes = this.ldProxyClientService.getProjectResearchObjectTypes(project.getShortName());
      assertNotNull(researchObjectTypes);
      for (String researchObjectType : researchObjectTypes) {
        final List<String> researchObjects = this.ldProxyClientService.getProjectResearchObjectsInJson(project.getShortName(),
                researchObjectType);
        assertNotNull(researchObjects);
      }
    }
  }

  @Test
  @Order(30)
  void listResearchObjects() {
    for (PublicProject project : this.ldProxyClientService.listProjects()) {
      for (String researchObjectType : this.ldProxyClientService.getProjectResearchObjectTypes(project.getShortName())) {
        for (String researchObjectUri : this.ldProxyClientService.getProjectResearchObjectsInJson(project.getShortName(),
                researchObjectType)) {
          final String hederaId = researchObjectUri.substring(researchObjectUri.lastIndexOf("/") + 1);
          assertFalse(StringTool.isNullOrEmpty(hederaId));
          final String researchObject = this.ldProxyClientService.getProjectResearchObjectsInJson(project.getShortName(),
                  researchObjectType, hederaId);
          assertFalse(StringTool.isNullOrEmpty(researchObject));
        }
      }
    }
  }

  @Test
  @Order(31)
  void listResearchObjectMetadata() {
    for (PublicProject project : this.ldProxyClientService.listProjects()) {
      List<ResearchObjectType> managedResearchObjectTypes = this.getManagedResearchObjectTypes(project.getShortName());
      for (String researchObjectType : this.ldProxyClientService.getProjectResearchObjectTypes(project.getShortName())) {
        if (managedResearchObjectTypes.stream().filter(rot -> rot.getName().equals(researchObjectType)).count() == 0) {
          // skip unmanaged research object type
          continue;
        }
        for (String researchObjectUri : this.ldProxyClientService.getProjectResearchObjectsInJson(project.getShortName(),
                researchObjectType)) {
          final String hederaId = researchObjectUri.substring(researchObjectUri.lastIndexOf("/") + 1);
          final ResearchObjectMetadata researchObjectMetadata = this.ldProxyClientService.getProjectResearchObject(project.getShortName(),
                  researchObjectType, hederaId);
          assertNotNull(researchObjectMetadata, "HederaId [" + hederaId + "] not found");
        }
      }
    }
  }

  @Test
  @Order(40)
  void listResearchObjectInRdf() {
    for (PublicProject project : this.ldProxyClientService.listProjects()) {
      for (String researchObjectType : this.ldProxyClientService.getProjectResearchObjectTypes(project.getShortName())) {
        for (String researchObjectUri : this.ldProxyClientService.getProjectResearchObjectsInJson(project.getShortName(),
                researchObjectType)) {
          final String hederaId = researchObjectUri.substring(researchObjectUri.lastIndexOf("/") + 1);
          assertFalse(StringTool.isNullOrEmpty(hederaId));
          // JSON-LD
          final String researchObjectInJsonLd = this.ldProxyClientService.getProjectResearchObjectsInJsonLd(project.getShortName(),
                  researchObjectType, hederaId);
          assertFalse(StringTool.isNullOrEmpty(researchObjectInJsonLd));
          assertDoesNotThrow(() -> RdfTool.wellformed(researchObjectInJsonLd, RdfFormat.JSON_LD));
          // JSON XML
          final String researchObjectInJsonXml = this.ldProxyClientService.getProjectResearchObjectsInJsonXml(project.getShortName(),
                  researchObjectType, hederaId);
          assertFalse(StringTool.isNullOrEmpty(researchObjectInJsonXml));
          assertDoesNotThrow(() -> RdfTool.wellformed(researchObjectInJsonXml, RdfFormat.RDF_XML));
          // JSON Turtle
          final String researchObjectInJsonTurtle = this.ldProxyClientService.getProjectResearchObjectsInTurtle(project.getShortName(),
                  researchObjectType, hederaId);
          assertFalse(StringTool.isNullOrEmpty(researchObjectInJsonTurtle));
          assertDoesNotThrow(() -> RdfTool.wellformed(researchObjectInJsonTurtle, RdfFormat.TURTLE));
          // JSON N-Triples
          final String researchObjectInJsonNTriples = this.ldProxyClientService.getProjectResearchObjectsInNTriples(project.getShortName(),
                  researchObjectType, hederaId);
          assertFalse(StringTool.isNullOrEmpty(researchObjectInJsonNTriples));
          assertDoesNotThrow(() -> RdfTool.wellformed(researchObjectInJsonNTriples, RdfFormat.N_TRIPLES));

        }
      }
    }
  }

  private List<ResearchObjectType> getManagedResearchObjectTypes(String projectShortName) {
    final Project project = this.projectITService.findOneByShortName(projectShortName);
    final List<ResearchObjectType> researchObjectTypes = this.projectITService.getResearchObjectTypes(project.getProjectId());
    if (project.getResearchDataFileResearchObjectType() != null) {
      researchObjectTypes.add(project.getResearchDataFileResearchObjectType());
    }
    if (project.getIiifManifestResearchObjectType() != null) {
      researchObjectTypes.add(project.getIiifManifestResearchObjectType());
    }

    return researchObjectTypes;
  }

  @Override
  protected void deleteFixtures() {
    // Nothing to clean
  }
}
