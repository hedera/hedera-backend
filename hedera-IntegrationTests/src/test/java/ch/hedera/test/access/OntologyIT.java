/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - OntologyIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.access;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.apache.logging.log4j.core.config.Order;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.access.PublicOntology;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.rest.ModuleName;
import ch.hedera.service.access.OAISetClientService;
import ch.hedera.service.access.PublicOntologyClientService;
import ch.hedera.service.access.PublicProjectClientService;
import ch.hedera.service.admin.IndexFieldAliasClientService;
import ch.hedera.service.admin.OntologyClientService;
import ch.hedera.test.service.ProjectITService;

@Order(10)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class OntologyIT extends AbstractAccessIT {

  @Value("${hedera.module.access.url}")
  private String accessUrl;

  protected PublicOntologyClientService publicOntologyClientService;
  protected OntologyClientService ontologyClientService;

  @Autowired
  public OntologyIT(Environment env,
          SudoRestClientTool restClientTool,
          OAISetClientService oaiSetService,
          PublicProjectClientService publicProjectService,
          ProjectITService projectITService,
          IndexFieldAliasClientService indexFieldAliasService,
          PublicOntologyClientService publicOntologyClientService,
          OntologyClientService ontologyClientService) {
    super(env, restClientTool, oaiSetService, publicProjectService, indexFieldAliasService);
    this.publicOntologyClientService = publicOntologyClientService;
    this.ontologyClientService = ontologyClientService;
  }

  @Test
  void createOntologyNotAllowed() {
    final String applicationUrl = this.accessUrl.substring(0, this.accessUrl.lastIndexOf("/"));
    final String url = applicationUrl + "/" + ModuleName.ONTOLOGY;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    assertThrows(HttpClientErrorException.MethodNotAllowed.class,
            () -> restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(new PublicOntology()), PublicOntology.class));
  }

  @Test
  void listOntologies() {
    final List<PublicOntology> accessOntologies = this.publicOntologyClientService.list();
    // check number of projects searching all public projects from admin
    this.restClientTool.sudoAdmin();
    final List<Ontology> adminOntologies = this.ontologyClientService.findAll();
    this.restClientTool.exitSudo();
    assertEquals(accessOntologies.size(), adminOntologies.size());
  }

  @Test
  void listExternalOntologies() {
    final List<PublicOntology> accessOntologies = this.publicOntologyClientService.listExternalOntologies();
    // check number of projects searching all public projects from admin
    this.restClientTool.sudoAdmin();
    final List<Ontology> adminOntologies = this.ontologyClientService.listExternalOntologies();
    this.restClientTool.exitSudo();
    assertEquals(accessOntologies.size(), adminOntologies.size());
  }

  @Test
  void listInternalOntologies() {
    final List<PublicOntology> accessOntologies = this.publicOntologyClientService.listInternalOntologies();
    // check number of projects searching all public projects from admin
    this.restClientTool.sudoAdmin();
    final List<Ontology> adminOntologies = this.ontologyClientService.listInternalOntologies();
    this.restClientTool.exitSudo();
    assertEquals(accessOntologies.size(), adminOntologies.size());
  }

  @Test
  void listOntologyDetails() {
    for (PublicOntology onto : this.publicOntologyClientService.list()) {
      final PublicOntology ontoDetail = this.publicOntologyClientService.get(onto.getResId());
      assertEquals(onto.getResId(), ontoDetail.getResId());
      assertEquals(onto.getName(), ontoDetail.getName());
      assertEquals(onto.getVersion(), ontoDetail.getVersion());
      assertEquals(onto.getDescription(), ontoDetail.getDescription());
      assertEquals(onto.getUrl(), ontoDetail.getUrl());
      final String ontoContent = this.publicOntologyClientService.getContent(onto.getResId());
      assertFalse(StringTool.isNullOrEmpty(ontoContent));
    }
  }

}
