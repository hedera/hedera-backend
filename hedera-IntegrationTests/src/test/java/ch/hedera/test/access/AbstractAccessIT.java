/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AbstractAccessIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.access;

import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.service.access.OAISetClientService;
import ch.hedera.service.access.PublicProjectClientService;
import ch.hedera.service.admin.IndexFieldAliasClientService;
import ch.hedera.test.AbstractIT;

public abstract class AbstractAccessIT extends AbstractIT {

  protected OAISetClientService oaiSetService;

  protected PublicProjectClientService publicProjectService;

  protected IndexFieldAliasClientService indexFieldAliasService;

  public AbstractAccessIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OAISetClientService oaiSetService,
          PublicProjectClientService publicProjectService,
          IndexFieldAliasClientService indexFieldAliasService) {
    super(env, restClientTool);
    this.oaiSetService = oaiSetService;
    this.publicProjectService = publicProjectService;
    this.indexFieldAliasService = indexFieldAliasService;
  }

  @Override
  protected void deleteFixtures() {
  }
}
