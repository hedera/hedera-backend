/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ProjectIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.access;

import static java.util.stream.Collectors.groupingBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.core.config.Order;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.access.PublicProject;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.ResourceName;
import ch.hedera.service.access.OAISetClientService;
import ch.hedera.service.access.PublicProjectClientService;
import ch.hedera.service.admin.IndexFieldAliasClientService;
import ch.hedera.test.service.ProjectITService;

@Order(30)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ProjectIT extends AbstractAccessIT {

  @Value("${hedera.module.access.url}")
  private String accessUrl;

  protected ProjectITService projectITService;

  @Autowired
  public ProjectIT(Environment env,
          SudoRestClientTool restClientTool,
          OAISetClientService oaiSetService,
          PublicProjectClientService publicProjectService,
          ProjectITService projectITService,
          IndexFieldAliasClientService indexFieldAliasService) {
    super(env, restClientTool, oaiSetService, publicProjectService, indexFieldAliasService);
    this.projectITService = projectITService;
  }

  @Test
  void createProjectNotAllowed() {
    final String url = this.accessUrl + "/" + ResourceName.PROJECT;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    assertThrows(HttpClientErrorException.MethodNotAllowed.class,
            () -> restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(new PublicProject()), PublicProject.class));
  }

  @Test
  void listProjects() {
    final RestCollection<PublicProject> projects = this.publicProjectService.list();
    // check number of projects searching all public projects from admin
    final List<Project> projectList = this.projectITService.searchByProperties(Map.of("accessPublic", "true"));
    assertEquals(projects.getPage().getTotalItems(), projectList.size());
  }

  @Test
  void listPublicAndAuthorizedProject() {
    final RestCollection<PublicProject> projects = this.publicProjectService.list();

    final List<Project> projectList = this.projectITService.searchByProperties(Map.of("accessPublic", "true"));
    final List<Project> authorizedList = this.projectITService.getAuthorizedProjects();

    List<Project> listCombined = new ArrayList<>(
            Stream.concat(projectList.stream(), authorizedList.stream()).collect(Collectors.toList()));
    List<Project> listWithoutDuplicates = listCombined.stream().collect(groupingBy(Project::getResId)).values().stream()
            .flatMap(values -> values.stream().limit(1)).toList();
    assertEquals(projects.getPage().getTotalItems(), listWithoutDuplicates.size());

  }

  @Test
  void listAllPublicWithoutToken() {
    final RestCollection<PublicProject> projects = this.publicProjectService.listWithoutToken();

    final List<Project> projectList = this.projectITService.searchByProperties(Map.of("accessPublic", "true"));
    assertEquals(projects.getPage().getTotalItems(), projectList.size());
  }

  @Test
  void defaultCopyrightHolder() {
    final Project project = this.projectITService.getPublicPermanentProject();
    assertEquals(HederaConstants.DEFAULT_COPYRIGHT_HOLDER, project.getCopyrightHolder());
  }

}
