/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - SparqlProxyIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.access;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.service.access.SparqlProxyClientService;
import ch.hedera.test.AbstractIT;
import ch.hedera.test.service.PersonITService;
import ch.hedera.test.service.ProjectITService;

class SparqlProxyIT extends AbstractIT {

  private final SparqlProxyClientService sparqlProxyClientService;
  private final ProjectITService projectITService;

  private final PersonITService personITService;

  private static final String SPARQL_QUERY = """
          PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
          PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
          SELECT * WHERE {
            ?sub ?pred ?obj .
          } LIMIT 10""";

  private static final String DELETE_SPARQL_QUERY = """
          DELETE WHERE {
            ?s ?p ?o .
          }""";

  private static final String UPDATE_SPARQL_QUERY = """
          PREFIX foaf: <http://xmlns.com/foaf/0.1/>
          INSERT DATA {
            <http://example.org/person1> foaf:name 'Alice' .
          }""";

  final String FEDERATE_SPARQL_QUERY = """
            PREFIX dbo: <http://dbpedia.org/ontology/>
            PREFIX dbr: <http://dbpedia.org/resource/>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            SELECT ?scientist ?birthPlace ?country
            WHERE {
              SERVICE <%s> {
                ?scientist dbo:birthPlace ?birthPlace .
                ?scientist a dbo:Scientist .
                ?scientist rdfs:label ?scientistName .
                FILTER(LANG(?scientistName) = 'en')
              }
              SERVICE <http://factforge.net/sparql> {
                ?birthPlace dbo:country ?countryURL .
                ?countryURL rdfs:label ?country .
                FILTER(LANG(?country) = 'en')
              }
            }""";

  private final static String SYNTAX_ERROR_QUERY = """
            SELECT * WHEEEEERE {
              ?s ?p ?o .
            }""";

  @Autowired
  SparqlProxyIT(Environment env, SudoRestClientTool restClientTool, SparqlProxyClientService sparqlProxyClientService,
          ProjectITService projectITService, PersonITService personITService) {
    super(env, restClientTool);
    this.sparqlProxyClientService = sparqlProxyClientService;
    this.projectITService = projectITService;
    this.personITService = personITService;
  }

  @Test
  void runQueryOnPublicProject() {
    final Project publicProject = this.projectITService.getPublicPermanentProject();
    String result = this.sparqlProxyClientService.executeQueryWithoutToken(publicProject.getShortName(), SPARQL_QUERY);
    assertNotNull(result);
  }

  @Test
  void runQueryOnPublicProjectAsUrlEncoded() {
    final Project publicProject = this.projectITService.getPublicPermanentProject();
    String result = this.sparqlProxyClientService.executeQueryAsPostUrlEncoded(publicProject.getShortName(), SPARQL_QUERY);
    assertNotNull(result);
  }

  @Test
  void runQueryOnPublicProjectAsGet() {
    final Project publicProject = this.projectITService.getPublicPermanentProject();
    String result = this.sparqlProxyClientService.executeQueryAsGet(publicProject.getShortName(), SPARQL_QUERY);
    assertNotNull(result);
  }

  @Test
  void runQueryOnPrivateProjectAsManager() {
    final Project privateProject = this.projectITService.getPrivatePermanentProject();
    this.restClientTool.sudoAdmin();
    final Person person = this.personITService.getPermanentTestRemotePerson(AuthApplicationRole.USER);
    this.projectITService.enforcePersonHasRoleInProject(privateProject, person, Role.MANAGER_ID);
    this.restClientTool.exitSudo();
    String result = this.sparqlProxyClientService.executeQueryAsPost(privateProject.getShortName(), SPARQL_QUERY);
    assertNotNull(result);
  }

  @Test
  void runQueryOnPrivateProjectAsCreator() {
    final Project privateProject = this.projectITService.getPrivatePermanentProject();
    this.restClientTool.sudoAdmin();
    final Person person = this.personITService.getPermanentTestRemotePerson(AuthApplicationRole.USER);
    this.projectITService.enforcePersonHasRoleInProject(privateProject, person, Role.CREATOR_ID);
    this.restClientTool.exitSudo();
    String result = this.sparqlProxyClientService.executeQueryAsPost(privateProject.getShortName(), SPARQL_QUERY);
    assertNotNull(result);
  }

  @Test
  void runQueryOnPrivateProjectAsVisitor() {
    final Project privateProject = this.projectITService.getPrivatePermanentProject();
    this.restClientTool.sudoAdmin();
    final Person person = this.personITService.getPermanentTestRemotePerson(AuthApplicationRole.USER);
    this.projectITService.enforcePersonHasRoleInProject(privateProject, person, Role.VISITOR_ID);
    this.restClientTool.exitSudo();
    String result = this.sparqlProxyClientService.executeQueryAsPost(privateProject.getShortName(), SPARQL_QUERY);
    assertNotNull(result);
  }

  @Test
  void runQueryOnPrivateProjectWithoutRole() {
    final Project privateProject = this.projectITService.getPrivatePermanentProject();
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.sparqlProxyClientService.executeQueryAsPost(privateProject.getShortName(), SPARQL_QUERY));
  }

  @Test
  void runDeleteQueryOnAnyProject() {
    final Project publicProject = this.projectITService.getPublicPermanentProject();
    assertThrows(HttpClientErrorException.MethodNotAllowed.class,
            () -> this.sparqlProxyClientService.executeQueryAsPost(publicProject.getShortName(), DELETE_SPARQL_QUERY));
  }

  @Test
  void runUpdateQueryOnAnyProject() {
    final Project publicProject = this.projectITService.getPublicPermanentProject();
    assertThrows(HttpClientErrorException.MethodNotAllowed.class,
            () -> this.sparqlProxyClientService.executeQueryAsPost(publicProject.getShortName(), UPDATE_SPARQL_QUERY));
  }

  @Test
  void runFederateQueryOnAnyProject() {
    final Project publicProject = this.projectITService.getPublicPermanentProject();
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.sparqlProxyClientService.executeQueryAsPost(publicProject.getShortName(),
                    FEDERATE_SPARQL_QUERY.formatted(this.sparqlProxyClientService.getResourceUrl())));
  }

  @Test
  void runQueryWithSyntaxError() {
    final Project publicProject = this.projectITService.getPublicPermanentProject();
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.sparqlProxyClientService.executeQueryWithoutToken(publicProject.getShortName(), SYNTAX_ERROR_QUERY));
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoAdmin();
    final Project privateProject = this.projectITService.getPrivatePermanentProject();
    this.projectITService.getPeople(privateProject.getResId())
            .forEach(person -> this.projectITService.removePerson(privateProject.getResId(), person.getResId()));
    this.restClientTool.exitSudo();  }
}
