/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - WebProxyIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.access;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.access.PublicProject;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.model.settings.Project;
import ch.hedera.model.xml.hedera.v1.researchObject.ItemType;
import ch.hedera.service.access.ProjectResearchDataFileClientService;
import ch.hedera.service.access.ProjectResearchObjectClientService;
import ch.hedera.service.access.PublicProjectClientService;
import ch.hedera.test.AbstractIT;
import ch.hedera.test.service.ProjectITService;

class WebProxyIT extends AbstractIT {

  private final ProjectITService projectITService;
  private final PublicProjectClientService projectClientService;
  private final ProjectResearchDataFileClientService researchDataFileClientService;
  private final ProjectResearchObjectClientService researchObjectClientService;

  @Override
  @BeforeEach
  protected void setup() {
    final Project privateProject = this.projectITService.getPrivatePermanentProject();
    this.restClientTool.sudoAdmin();
    this.projectITService.getPeople(privateProject.getResId())
            .forEach(person -> this.projectITService.removePerson(privateProject.getResId(), person.getResId()));
    this.restClientTool.exitSudo();
  }

  @Autowired
  WebProxyIT(
          Environment env,
          SudoRestClientTool restClientTool,
          ProjectITService projectITService,
          PublicProjectClientService projectClientService,
          ProjectResearchDataFileClientService researchDataFileClientService,
          ProjectResearchObjectClientService researchObjectClientService) {
    super(env, restClientTool);
    this.projectITService = projectITService;
    this.projectClientService = projectClientService;
    this.researchDataFileClientService = researchDataFileClientService;
    this.researchObjectClientService = researchObjectClientService;
  }

  @Test
  void browsePublicProjects() {
    final RestCollection<PublicProject> projects = this.projectClientService.list();
    assertFalse(projects.getData().isEmpty());
    for (PublicProject project : projects.getData()) {
      Project projectFromAdmin = this.projectITService.findOneByShortName(project.getShortName());
      if (projectFromAdmin.isAccessPublic()) {
        PublicProject projectByShortName = this.projectClientService.get(project.getShortName());
        assertEquals(project.getShortName(), projectByShortName.getShortName());
        assertEquals(project.getName(), projectByShortName.getName());
        assertEquals(project.getName(), projectFromAdmin.getName());
      }
    }
  }

  @Test
  void browseResearchDataFilesForPublicProject() {
    final Project project = this.projectITService.getPublicPermanentProject();
    assertNotNull(project);
    assertTrue(project.isAccessPublic());
    for (ResearchObjectMetadata researchObjectMetadata : this.researchDataFileClientService.list(project.getShortName()).getData()) {
      // Test data from list
      this.testResearchDataFile(project, ItemType.RESEARCH_DATA_FILE, researchObjectMetadata);
      // Test data from get
      this.testResearchDataFile(project, ItemType.RESEARCH_DATA_FILE,
              this.researchDataFileClientService.get(project.getShortName(), researchObjectMetadata.getHederaId()));
      // Test data download with hederaID
      Path downloadedFileFromHederaId = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), researchObjectMetadata.getFilename() + "_1");
      this.researchDataFileClientService.download(project.getShortName(), researchObjectMetadata.getHederaId(), downloadedFileFromHederaId);
      long sizeFromHederaId = this.checkFile(downloadedFileFromHederaId);
      // Test data download with rel. location + filename
      Path downloadedFileFromRelLocationAndFileName = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER),
              researchObjectMetadata.getFilename() + "_2");
      this.researchDataFileClientService.download(project.getShortName(), researchObjectMetadata.getRelativeLocation(),
              researchObjectMetadata.getFilename(), downloadedFileFromRelLocationAndFileName);
      long sizeFromRelLocationAndFileName = this.checkFile(downloadedFileFromRelLocationAndFileName);
      assertEquals(sizeFromHederaId, sizeFromRelLocationAndFileName);
    }
  }

  @Test
  void browseResearchDataFileForPrivateProject() {
    final Project project = this.projectITService.getPrivatePermanentProject();
    assertNotNull(project);
    assertFalse(project.isAccessPublic());
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.researchDataFileClientService.list(project.getShortName()));
  }

  @Test
  void unableToDownloadResearchDataFileFromPrivateProject() {
    final Project project = this.projectITService.getPrivatePermanentProject();
    assertNotNull(project);
    assertFalse(project.isAccessPublic());

    this.restClientTool.sudoAdmin();
    List<ResearchObjectMetadata> researchObjectMetadataList = this.researchDataFileClientService.list(project.getShortName()).getData();
    assertNotNull(researchObjectMetadataList);
    assertFalse(researchObjectMetadataList.isEmpty());
    this.restClientTool.exitSudo();
    ResearchObjectMetadata researchObjectMetadata = researchObjectMetadataList.get(0);

    Path downloadedFileFromHederaId = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), researchObjectMetadata.getFilename() + "_1");
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.researchDataFileClientService.download(project.getShortName(),
            researchObjectMetadata.getHederaId(), downloadedFileFromHederaId));
  }

  @Test
  void browseResearchObjectsForPublicProject() {
    final Project project = this.projectITService.getPublicPermanentProject();
    assertNotNull(project);
    assertTrue(project.isAccessPublic());
    for (ResearchObjectMetadata researchDataFile : this.researchObjectClientService.list(project.getShortName()).getData()) {
      // Test data from list
      this.testResearchDataFile(project, ItemType.RESEARCH_OBJECT, researchDataFile);
      // Test data from get
      this.testResearchDataFile(project, ItemType.RESEARCH_OBJECT,
              this.researchDataFileClientService.get(project.getShortName(), researchDataFile.getHederaId()));
    }
  }

  @Test
  void browseResearchObjectsForPrivateProject() {
    final Project project = this.projectITService.getPrivatePermanentProject();
    assertNotNull(project);
    assertFalse(project.isAccessPublic());
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.researchObjectClientService.list(project.getShortName()));
  }

  private void testResearchDataFile(Project project, ItemType itemType, ResearchObjectMetadata researchDataFile) {
    assertEquals(project.getResId(), researchDataFile.getProjectId());
    assertEquals(itemType.value(), researchDataFile.getMetadata().get(HederaConstants.ITEM_TYPE_INDEX_FIELD));
  }

  @Override
  protected void deleteFixtures() {
    // Nothing to clean
  }
}
