/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - IIIFProxyIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.access;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.JSONTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.access.IIIFCollectionEntry;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Project;
import ch.hedera.service.access.IIIFClientService;
import ch.hedera.service.access.ProjectResearchDataFileClientService;
import ch.hedera.test.AbstractIT;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.ProjectITService;

class IIIFProxyIT extends AbstractIT {

  private final ProjectITService projectITService;
  private final ProjectResearchDataFileClientService researchDataFileClientService;
  private final IIIFClientService iiifClientService;

  private static final String UNKNOWN_MANIFEST = "254LJK";

  @Autowired
  public IIIFProxyIT(
          Environment env,
          SudoRestClientTool restClientTool,
          ProjectITService projectITService,
          ProjectResearchDataFileClientService researchDataFileClientService,
          IIIFClientService iiifClientService) {
    super(env, restClientTool);
    this.projectITService = projectITService;
    this.researchDataFileClientService = researchDataFileClientService;
    this.iiifClientService = iiifClientService;
  }

  @Test
  void iiifImageWithWrongParametersOnPublicProject() {
    final Project project = this.projectITService.getPublicPermanentProject();
    assertNotNull(project);
    for (ResearchObjectMetadata researchObjectMetadata : this.researchDataFileClientService.list(project.getShortName()).getData()) {
      if (researchObjectMetadata.getAccessibleFrom().equals(DataAccessibilityType.IIIF.toString())) {
        // Download image
        final Path downloadedImage = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER),
                researchObjectMetadata.getFilename() + "_" + researchObjectMetadata.getHederaId());
        final String businessId = researchObjectMetadata.getFullFileName().substring(1);
        assertThrows(HttpClientErrorException.BadRequest.class,
                () -> this.iiifClientService.getImage("3", project.getShortName(), businessId, downloadedImage, "full/full/0/default.jpg"));
      }
    }
  }

  @ParameterizedTest
  @MethodSource("iiifVersions")
  void iiifImageOnPublicProject(String iiifVersion) {
    final Project project = this.projectITService.getPublicPermanentProject();
    assertNotNull(project);
    for (ResearchObjectMetadata researchObjectMetadata : this.researchDataFileClientService.list(project.getShortName()).getData()) {
      if (researchObjectMetadata.getAccessibleFrom().equals(DataAccessibilityType.IIIF.toString())) {
        // Download image
        final Path downloadedImage = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER),
                researchObjectMetadata.getFilename() + "_" + researchObjectMetadata.getHederaId());
        final String businessId = researchObjectMetadata.getFullFileName().substring(1);
        assertDoesNotThrow(() -> this.iiifClientService.getImage(iiifVersion, project.getShortName(), businessId, downloadedImage));
        this.checkFile(downloadedImage);
      }
    }
  }

  @ParameterizedTest
  @MethodSource("iiifVersions")
  void iiifImageInfoOnPublicProject(String iiifVersion) throws IOException {
    final Project project = this.projectITService.getPublicPermanentProject();
    assertNotNull(project);
    for (ResearchObjectMetadata researchObjectMetadata : this.researchDataFileClientService.list(project.getShortName()).getData()) {
      if (researchObjectMetadata.getAccessibleFrom().equals(DataAccessibilityType.IIIF.toString())) {
        final String businessId = researchObjectMetadata.getFullFileName().substring(1);
        // Get image info
        final String iiifSchema = this.getIIIFImageSchema(iiifVersion);
        final String imageInfo = this.iiifClientService.getImageInfo(iiifVersion, project.getShortName(), businessId);
        assertNotNull(imageInfo);
        assertDoesNotThrow(() -> JSONTool.wellformed(imageInfo));
        // TODO: check the JSON schema of IIIF
        assertDoesNotThrow(() -> JSONTool.validate(iiifSchema, imageInfo));
      }
    }
  }

  @Test
  void iiifManifestNotExistingProject() {
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.iiifClientService.getManifests("NO-PROJECT"));
  }

  @Test
  void iiifManifestSparqlEmptyResult() {
    Project publicPermanentProject = this.projectITService.getPublicPermanentProject();
    assertThrows(HttpClientErrorException.NotFound.class,
            () -> this.iiifClientService.getManifest(publicPermanentProject.getShortName(), UNKNOWN_MANIFEST));
  }

  @Test
  void iiifManifestProjectWithoutIIIFSparqlManifestQuery() {
    Project projectWithoutIiifManifest = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, Role.MANAGER_ID, AuthApplicationRole.USER);
    assertThrows(HttpClientErrorException.NotFound.class,
            () -> this.iiifClientService.getManifest(projectWithoutIiifManifest.getShortName(), UNKNOWN_MANIFEST));
  }

  @Test
  void iiifManifestProjectWithoutIiifManifestResearchObjectType() {
    Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, Role.MANAGER_ID, AuthApplicationRole.USER);

    this.restClientTool.sudoAdmin();
    // Set any sparql query as iiif manifest query.
    project.setIiifManifestSparqlQuery("""
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            SELECT ?resource_subject ?img_subject ?img_iiif_url ?img_height ?img_width ?label
            WHERE { ?resource_subject ?pred ?obj . }
            LIMIT 10
            """);
    this.projectITService.update(project.getProjectId(), project);
    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.NotFound.class,
            () -> this.iiifClientService.getManifest(project.getShortName(), UNKNOWN_MANIFEST));

    this.restClientTool.sudoAdmin();
    project.setIiifManifestSparqlQuery(""); // restore original value.
    this.projectITService.update(project.getProjectId(), project);
    this.restClientTool.exitSudo();
  }

  @Test
  void iiifCollectionByProject() {
    final List<Project> projectList = this.projectITService.findAll();
    assertFalse(projectList.stream().filter(project -> project.hasData()).toList().isEmpty(),
            "At least one project with data need to be created");
    for (Project project : this.projectITService.findAll()) {
      if (project.isAccessPublic()) {
        final List<IIIFCollectionEntry> list = this.iiifClientService.getCollections(project.getShortName());
        if (project.hasData() && project.getIiifManifestResearchObjectType() != null) {
          assertTrue(list.size() >= 1, "Error in project: " + project.getShortName());
          assertDoesNotThrow(
                  () -> this.iiifClientService.getCollection(project.getShortName(), HederaConstants.IIIF_PROJECT_DEFAULT_COLLECTION));
        }
      } else {
        // As user => Forbidden
        assertThrows(HttpClientErrorException.Forbidden.class, () -> this.iiifClientService.getCollections(project.getShortName()));
        assertThrows(HttpClientErrorException.Forbidden.class,
                () -> this.iiifClientService.getCollection(project.getShortName(), HederaConstants.IIIF_PROJECT_DEFAULT_COLLECTION));

        // Test as Admin
        this.restClientTool.sudoAdmin();
        assertDoesNotThrow(() -> this.iiifClientService.getCollections(project.getShortName()));
        assertDoesNotThrow(() -> this.iiifClientService.getCollection(project.getShortName(), HederaConstants.IIIF_PROJECT_DEFAULT_COLLECTION));
        this.restClientTool.exitSudo();
      }
    }
  }

  @Override
  protected void deleteFixtures() {

  }

  private static Stream<Arguments> iiifVersions() {
    return Stream.of(
            Arguments.of("2"),
            Arguments.of("3"));
  }

  private String getIIIFImageSchema(String iiifVersion) throws IOException {
    try (InputStream schema = new ClassPathResource("iiif/iiif-image-" + iiifVersion + ".0.json").getInputStream()) {
      return FileTool.toString(schema);
    }
  }

  private String getIIIFPresentationSchema(String iiifVersion) throws IOException {
    try (InputStream schema = new ClassPathResource("iiif/iiif-presentation-" + iiifVersion + ".0.json").getInputStream()) {
      return FileTool.toString(schema);
    }
  }
}
