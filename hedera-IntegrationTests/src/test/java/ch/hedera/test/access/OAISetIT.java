/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - OAISetIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.access;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.apache.logging.log4j.core.config.Order;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.model.oai.OAISet;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.service.access.OAISetClientService;
import ch.hedera.service.access.PublicProjectClientService;
import ch.hedera.service.admin.IndexFieldAliasClientService;
import ch.hedera.test.HederaTestConstants;

@Order(20)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class OAISetIT extends AbstractAccessIT {

  private final static String SPEC_URI = "oaiSet-valid-uri:123";

  @Autowired
  public OAISetIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OAISetClientService oaiSetService,
          PublicProjectClientService publicProjectService,
          IndexFieldAliasClientService indexFieldAliasService) {
    super(env, restClientTool, oaiSetService, publicProjectService, indexFieldAliasService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @AfterEach
  public void cleanOrderFixture() {
    this.oaiSetService.findAll().stream().filter(oaiSet -> oaiSet.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(oaiSet -> this.oaiSetService.delete(oaiSet.getResId()));
  }

  @Test
  void creationTest() {
    // Create
    final OAISet oaiSet1 = new OAISet();
    oaiSet1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("oaiSet"));
    oaiSet1.setSpec(SPEC_URI);
    oaiSet1.setQuery("*");
    final OAISet oaiSet2 = this.oaiSetService.create(oaiSet1);

    // Test the creation return
    assertNotNull(oaiSet2, "Cannot create oaiSet");
    assertEquals(oaiSet1.getName(), oaiSet2.getName(), "Problem with oaiSet name");

    // Test creation
    final OAISet oaiSet3 = this.oaiSetService.findOne(oaiSet2.getResId());
    assertNotNull(oaiSet3, "Cannot find oaiSet");
    assertEquals(oaiSet1.getName(), oaiSet3.getName(), "Problem with oaiSet name");
  }

  @Test
  void creationInvalidSpecTest() {
    // Create
    final OAISet oaiSet1 = new OAISet();
    oaiSet1.setName("DOI Registration");
    oaiSet1.setSpec("invalid URI");
    oaiSet1.setQuery("*");
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.oaiSetService.create(oaiSet1));
  }

  @Test
  void deleteTest() {
    // Create
    OAISet oaiSet = new OAISet();
    oaiSet.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("oaiSet"));
    oaiSet.setSpec(SPEC_URI);
    oaiSet.setQuery("*");
    final OAISet oaiSet2 = this.oaiSetService.create(oaiSet);
    // Test the creation return
    assertNotNull(oaiSet2, "Cannot create oaiSet");

    // Delete the order
    final String resId = oaiSet2.getResId();
    this.oaiSetService.delete(resId);
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.oaiSetService.findOne(resId));
  }

  @Test
  void nameUniquenessTest() {
    // Create
    final OAISet oaiSet1 = new OAISet();
    oaiSet1.setName("DOI Registration");
    oaiSet1.setSpec(SPEC_URI);
    oaiSet1.setQuery("*");
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.oaiSetService.create(oaiSet1));
  }

  @Test
  void specUniquenessTest() {
    // Create
    final OAISet oaiSet1 = new OAISet();
    oaiSet1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("oaiSet"));
    oaiSet1.setSpec(OAIConstants.OAI_DOI_REGISTRATION);
    oaiSet1.setQuery("*");
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.oaiSetService.create(oaiSet1));
  }

  @Test
  void unicityTest() {
    OAISet oaiSet1 = new OAISet();
    oaiSet1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("oaiSet"));
    oaiSet1.setSpec(SPEC_URI);
    oaiSet1.setQuery("*");
    oaiSet1 = this.oaiSetService.create(oaiSet1);

    OAISet oaiSet2 = new OAISet();
    oaiSet2.setName(oaiSet1.getName());
    oaiSet2.setSpec(oaiSet1.getSpec());
    oaiSet2.setQuery(oaiSet1.getQuery());

    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.oaiSetService.create(oaiSet2));
  }

  @Test
  void updateTest() {
    // Create
    final OAISet oaiSet = new OAISet();
    oaiSet.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("oaiSet"));
    oaiSet.setSpec(SPEC_URI);
    oaiSet.setQuery("*");
    final OAISet oaiSet2 = this.oaiSetService.create(oaiSet);
    // Test the creation return
    assertNotNull(oaiSet2, "Cannot create oaiSet");

    // Update the order
    oaiSet2.setDescription(HederaTestConstants.getRandomNameWithTemporaryLabel("oaiSet"));
    final OAISet oaiSet3 = this.oaiSetService.update(oaiSet2.getResId(), oaiSet2);
    assertNotNull(oaiSet3, "Cannot find oaiSet");
    final OAISet oaiSet4 = this.oaiSetService.findOne(oaiSet2.getResId());
    assertNotNull(oaiSet4, "Cannot find oaiSet");

    // Test the update
    assertEquals(oaiSet2.getName(), oaiSet4.getName(), "Problem with oaiSet name");
    assertEquals(oaiSet2.getDescription(), oaiSet4.getDescription(), "Problem with oaiSet description");
    this.assertEqualsWithoutNanoSeconds("Problem with oaiSet creation time", oaiSet2.getCreationTime(), oaiSet4.getCreationTime());

  }
}
