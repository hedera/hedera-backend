/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - SourceDatasetITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.service;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.ingest.SourceDatasetFile.SourceDatasetFileStatus;
import ch.hedera.model.settings.Project;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;

@Service
public class SourceDatasetITService extends ITService {

  private final SourceDatasetClientService sourceDatasetClientService;
  private final ProjectITService projectITService;

  public SourceDatasetITService(SudoRestClientTool restClientTool, SourceDatasetClientService sourceDatasetClientService,
          ProjectITService projectITService) {
    super(restClientTool);
    this.sourceDatasetClientService = sourceDatasetClientService;
    this.projectITService = projectITService;
  }

  public SourceDataset createLocalSourceDatasetForRole(HederaTestConstants.PersistenceMode persistenceMode, String name, String description,
          String role) {
    final SourceDataset sourceDataset = this.createLocalSourceDataset(persistenceMode, name, description);
    this.restClientTool.sudoAdmin();
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, role, AuthApplicationRole.USER);
    this.restClientTool.exitSudo();
    sourceDataset.setProjectId(project.getResId());
    return sourceDataset;
  }

  public SourceDataset createLocalSourceDatasetOnProject(HederaTestConstants.PersistenceMode persistenceMode, String name, String description,
           Project project) {
    final SourceDataset sourceDataset = this.createLocalSourceDataset(persistenceMode, name, description);
    sourceDataset.setProjectId(project.getResId());
    return sourceDataset;
  }

  public SourceDataset createLocalSourceDataset(HederaTestConstants.PersistenceMode persistenceMode, String name, String description) {
    final SourceDataset sourceDataset = new SourceDataset();
    sourceDataset.init();
    sourceDataset.setName(this.createLabel(persistenceMode, name));
    sourceDataset.setDescription(description);
    return sourceDataset;
  }

  public SourceDataset createTemporaryLocalSourceDataset(String projectId, ApplicationRole role) {
    String name = this.getSourceDatasetName(role.getName());
    return this.createTemporaryLocalSourceDataset(projectId, name);
  }

  public SourceDataset createTemporaryLocalSourceDataset(String projectId, String name, boolean changeLabel) {
    final SourceDataset sourceDataset = new SourceDataset();
    if (changeLabel) {
      sourceDataset.setName(this.getSourceDatasetName(name));
    } else {
      sourceDataset.setName(name);
    }
    sourceDataset.setDescription("description " + ThreadLocalRandom.current().nextInt());
    sourceDataset.setProjectId(projectId);
    return sourceDataset;
  }

  public SourceDataset createTemporaryLocalSourceDataset(String projectId, String name) {
    return this.createTemporaryLocalSourceDataset(projectId, name, true);
  }

  public SourceDataset createPermanentLocalSourceDataset(String projectId) {
    String label = this.createLabel(HederaTestConstants.PersistenceMode.PERMANENT, HederaTestConstants.SOURCE_DATASET_NAME);
    final SourceDataset sourceDataset = new SourceDataset();
    sourceDataset.setName(label);
    sourceDataset.setDescription("description " + ThreadLocalRandom.current().nextInt());
    sourceDataset.setProjectId(projectId);
    return sourceDataset;
  }

  private String getSourceDatasetName(String role) {
    String name;
    if (role == null) {
      name = "Project";
    } else {
      name = "Project for " + role.toLowerCase();
    }
    return this.createLabel(HederaTestConstants.PersistenceMode.TEMPORARY, name);
  }

  public boolean waitSourceDatasetIsDeleted(String resId) {
    int count = 0;
    boolean depositFound;
    do {
      try {
        this.sourceDatasetClientService.findOne(resId);
        depositFound = true;
      } catch (HttpClientErrorException.NotFound e) {
        depositFound = false;
      }
      HederaTestConstants.failAfterNAttempt(count, "SourceDataset " + resId + " should have been deleted");
      count++;
    } while (depositFound);

    return true;
  }

  public boolean waitForSourceDatasetFileIsChecked(String sourceDatasetId, String sourceDatasetFileId) {
    return this.waitForSourceDatasetFileHasChangeToStatus(sourceDatasetId, sourceDatasetFileId, SourceDatasetFileStatus.CHECKED);
  }

  public boolean waitForSourceDatasetFileIsTransformed(String sourceDatasetId, String sourceDatasetFileId) {
    return this.waitForSourceDatasetFileHasChangeToStatus(sourceDatasetId, sourceDatasetFileId, SourceDatasetFileStatus.TRANSFORMED);
  }

  public boolean waitForSourceDatasetFileIsInError(String sourceDatasetId, String sourceDatasetFileId) {
    return this.waitForSourceDatasetFileHasChangeToStatus(sourceDatasetId, sourceDatasetFileId, SourceDatasetFileStatus.IN_ERROR);
  }

  private boolean waitForSourceDatasetFileHasChangeToStatus(String sourceDatasetId, String sourceDatasetFileId, SourceDatasetFileStatus status) {
    int count = 0;
    SourceDatasetFile sourceDatasetFile;
    do {
      sourceDatasetFile = this.sourceDatasetClientService.getSourceDataFile(sourceDatasetId, sourceDatasetFileId);
      HederaTestConstants.failAfterNAttempt(count, "The sourceDatasetFile " + sourceDatasetFileId + " is not already in the status " + status);
      count++;

    } while (!sourceDatasetFile.getStatus().equals(status));
    return true;
  }

  private void clearSourceDatasetFixtures() {
    List<String> listSourceDatasetId = this.sourceDatasetClientService.findAll().stream()
            .filter(sourceDataset -> sourceDataset.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .map(sourceDataset -> sourceDataset.getResId())
            .toList();
    this.sourceDatasetClientService.deleteList(listSourceDatasetId.toArray(new String[0]));
  }

  @Override
  public void cleanTestData() {
    this.restClientTool.sudoRoot();
    this.clearSourceDatasetFixtures();
    this.restClientTool.exitSudo();
  }
}
