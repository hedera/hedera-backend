/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - RdfDatasetFileITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.service;

import org.springframework.stereotype.Service;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.service.ingest.RdfDatasetFileClientService;
import ch.hedera.test.HederaTestConstants;

@Service
public class RdfDatasetFileITService extends ITService {
  private final RdfDatasetFileClientService rdfDatasetFileClientService;

  public RdfDatasetFileITService(SudoRestClientTool restClientTool, ProjectITService projectITService,
          RdfDatasetFileClientService rdfDatasetFileClientService) {
    super(restClientTool);
    this.rdfDatasetFileClientService = rdfDatasetFileClientService;
  }

  private void clearRdfDatasetFileITServiceFixtures() {
    this.rdfDatasetFileClientService.findAll().stream()
            .filter(rdfFile -> rdfFile.getSourceDatasetFile().getSourceDataset().getName()
                    .startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(rdfFile -> {
              this.rdfDatasetFileClientService.deleteFromTripleStore(rdfFile.getResId(), rdfFile.getProjectId());
              this.waitForRdfDatasetFileIsNotImportedInFuseki(rdfFile.getResId());
              this.rdfDatasetFileClientService.delete(rdfFile.getResId());
            });
  }

  public boolean waitForRdfDatasetFileIsImportedInFuseki(String rdfFileId) {
    return this.waitForRdfDatasetFileHasChangeToStatus(rdfFileId, RdfDatasetFile.RdfDatasetFileStatus.IMPORTED);
  }

  public boolean waitForRdfDatasetFileIsNotImportedInFuseki(String rdfFileId) {
    return this.waitForRdfDatasetFileHasChangeToStatus(rdfFileId, RdfDatasetFile.RdfDatasetFileStatus.NOT_IMPORTED);
  }

  private boolean waitForRdfDatasetFileHasChangeToStatus(String rdfFileId, RdfDatasetFile.RdfDatasetFileStatus status) {
    int count = 0;
    RdfDatasetFile rdfDatasetFile;
    do {
      rdfDatasetFile = this.rdfDatasetFileClientService.findOne(rdfFileId);
      HederaTestConstants.failAfterNAttempt(count, "The RdfDatasetFile " + rdfFileId + " is not already in the status " + status);
      count++;

    } while (!rdfDatasetFile.getStatusImport().equals(status));
    return true;
  }

  @Override
  public void cleanTestData() {
    this.restClientTool.sudoRoot();
    this.clearRdfDatasetFileITServiceFixtures();
    this.restClientTool.exitSudo();
  }

}
