/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ResearchObjectTypeITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.service.admin.OntologyClientService;
import ch.hedera.service.admin.ResearchObjectTypeClientService;
import ch.hedera.test.HederaTestConstants;

@Service
public class ResearchObjectTypeITService extends ITService {

  public OntologyClientService ontologyClientService;

  public ResearchObjectTypeClientService researchObjectTypeClientService;

  public ResearchObjectTypeITService(
          SudoRestClientTool restClientTool,
          OntologyClientService ontologyClientService,
          ResearchObjectTypeClientService researchObjectTypeClientService) {
    super(restClientTool);
    this.ontologyClientService = ontologyClientService;
    this.researchObjectTypeClientService = researchObjectTypeClientService;
  }

  public ResearchObjectType createRemoteResearchObjectType(ResearchObjectType researchObjectType) {
    return this.researchObjectTypeClientService.create(researchObjectType);
  }

  private void clearResearchObjectFixtures() {

    final List<ResearchObjectType> researchObjectTypes = this.researchObjectTypeClientService.findAll();

    for (final ResearchObjectType researchObjectType : researchObjectTypes) {
      if (researchObjectType.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_NAME)) {
        /*
         * Remove eventual links with projects first
         */
        final List<Project> projects = this.researchObjectTypeClientService.getProjects(researchObjectType.getResId());
        if (projects != null && !projects.isEmpty()) {
          for (final Project po : projects) {
            this.researchObjectTypeClientService.removeProject(researchObjectType.getResId(), po.getResId());
          }
        }
      }
    }
    this.researchObjectTypeClientService.findAll().stream()
            .filter(ro -> ro.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_NAME))
            .forEach(ro -> this.researchObjectTypeClientService.delete(ro.getResId()));
  }

  public ResearchObjectType getResearchObjectTypeForTesting() {
    final Ontology onto = this.ontologyClientService.searchByProperties(Map.of("name", HederaConstants.ONTOLOGY_CRM_DIG, "version", "3.2.1"))
            .get(0);
    return this.createResearchObjectType(
            HederaTestConstants.getRandomNameWithTemporaryName("test"),
            HederaTestConstants.TEST_DATA_DESCRIPTION,
            HederaConstants.RESEARCH_OBJECT_TYPE_RDF_TYPE_D1,
            onto.getResId());
  }

  public ResearchObjectType getResearchObjectTypeForResearchDataFile() {
    return this.researchObjectTypeClientService.findOne(HederaConstants.RESEARCH_OBJECT_TYPE_D1);
  }

  public ResearchObjectType getResearchObjectType(String resId) {
    return this.researchObjectTypeClientService.findOne(resId);
  }

  public ResearchObjectType createResearchObjectType(String name, String description, String rdfType, String ontologyId) {
    final ResearchObjectType rd = new ResearchObjectType();
    rd.setName(name);
    rd.setRdfType(rdfType);
    final Ontology onto = new Ontology();
    onto.setResId(ontologyId);
    rd.setOntology(onto);
    rd.setDescription(description);
    rd.setSparqlListQuery(HederaTestConstants.SPARQL_QUERY);
    rd.setSparqlDetailQuery(HederaTestConstants.SPARQL_QUERY_DETAIL);
    return rd;
  }

  public ResearchObjectType createRemoteResearchObjectType(String id, String name, String rdfType, String description, Ontology ontology) {
    try {
      return this.researchObjectTypeClientService.findOne(id);
    } catch (HttpClientErrorException e) {
      if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
        throw e;
      }
      ResearchObjectType rot = this.createResearchObjectType(name, description, rdfType, ontology.getResId());
      rot.setResId(id);
      return this.researchObjectTypeClientService.create(rot);
    }
  }

  @Override
  public void cleanTestData() {
    this.restClientTool.sudoAdmin();
    this.clearResearchObjectFixtures();
    this.restClientTool.exitSudo();
  }
}
