/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - InstitutionITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;

@Service
public class InstitutionITService extends ITService {

  // Main services
  private final InstitutionClientService institutionClientService;

  public InstitutionITService(SudoRestClientTool restClientTool,
          InstitutionClientService institutionClientService) {
    super(restClientTool);
    this.institutionClientService = institutionClientService;
  }

  public Institution createRemoteInstitution(Institution institution) {
    return this.institutionClientService.create(institution);
  }

  protected void clearInstitutionsFixtures() {

    final List<Institution> institutions = this.institutionClientService.findAll();

    for (final Institution institution : institutions) {

      if (institution.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {

        /*
         * Remove eventual links with people first
         */
        final List<Person> people = this.institutionClientService.getPeople(institution.getResId());
        if (people != null && !people.isEmpty()) {
          for (final Person person : people) {
            this.institutionClientService.removePerson(institution.getResId(), person.getResId());
          }
        }

        /*
         * Remove eventual links with projects first
         */
        final List<Project> units = this.institutionClientService.getProjects(institution.getResId());
        if (units != null && !units.isEmpty()) {
          for (final Project unit : units) {
            this.institutionClientService.removeProject(institution.getResId(), unit.getResId());
          }
        }

        this.institutionClientService.delete(institution.getResId());
      }
    }
  }

  @Override
  public void cleanTestData() {
    this.clearInstitutionsFixtures();
  }

}
