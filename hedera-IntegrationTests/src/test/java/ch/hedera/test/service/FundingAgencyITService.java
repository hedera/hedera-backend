/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - FundingAgencyITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.service;

import java.nio.file.Path;
import java.util.List;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.settings.FundingAgency;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.test.HederaTestConstants;

@Service
public class FundingAgencyITService extends ITService {

  private final FundingAgencyClientService fundingAgencyClientService;

  public FundingAgencyITService(SudoRestClientTool restClientTool, FundingAgencyClientService fundingAgencyClientService) {
    super(restClientTool);
    this.fundingAgencyClientService = fundingAgencyClientService;
  }

  public FundingAgency createRemoteFundingAgency(String acronym, String name) {
    FundingAgency fundingAgency = new FundingAgency();
    fundingAgency.setAcronym(HederaTestConstants.getRandomNameWithTemporaryLabel(acronym));
    fundingAgency.setName(HederaTestConstants.getRandomNameWithTemporaryLabel(name));
    return this.createRemoteFundingAgency(fundingAgency);
  }

  public FundingAgency createRemoteFundingAgency(FundingAgency fundingAgency) {
    return this.fundingAgencyClientService.create(fundingAgency);
  }

  public FundingAgency uploadLogo(String fundingAgencyId, ClassPathResource logoToUpload) {
    this.fundingAgencyClientService.uploadLogo(fundingAgencyId, logoToUpload);
    return this.fundingAgencyClientService.findOne(fundingAgencyId);
  }

  public void downloadLogo(String fundingAgencyId, Path path) {
    this.fundingAgencyClientService.downloadLogo(fundingAgencyId, path);
  }

  public void deleteLogo(String fundingAgencyId) {
    this.fundingAgencyClientService.deleteLogo(fundingAgencyId);
  }

  protected void clearFundingAgencyFixtures() {
    final List<FundingAgency> agencies = this.fundingAgencyClientService.findAll();

    for (final FundingAgency a : agencies) {

      if (a.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {

        /*
         * Remove eventual links with projects first
         */
        final List<Project> child_units = this.fundingAgencyClientService.getProjects(a.getResId());
        if (child_units != null) {
          for (final Project u : child_units) {
            this.fundingAgencyClientService.removeProject(a.getResId(), u.getResId());
          }
        }

        this.fundingAgencyClientService.delete(a.getResId());
      }
    }
  }

  @Override
  public void cleanTestData() {
    this.clearFundingAgencyFixtures();
  }

}
