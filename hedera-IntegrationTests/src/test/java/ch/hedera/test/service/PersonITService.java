/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - PersonITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.service;

import static ch.hedera.HederaConstants.NO_REPLY_PREFIX;
import static ch.unige.solidify.auth.model.AuthTestConstants.FIRST_NAME_TEST_SUFFIX;
import static ch.unige.solidify.auth.model.AuthTestConstants.LAST_NAME_TEST_SUFFIX;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.security.User;
import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.PersonClientService;
import ch.hedera.service.admin.UserClientService;
import ch.hedera.test.HederaTestConstants;

@Service
public class PersonITService extends ITService {

  // Main services
  private final PersonClientService personClientService;
  private final UserClientService userClientService;

  public PersonITService(SudoRestClientTool restClientTool, PersonClientService personClientService, UserClientService userClientService) {
    super(restClientTool);
    this.personClientService = personClientService;
    this.userClientService = userClientService;
  }

  public Person create(Person person) {
    return this.personClientService.create(person);
  }

  public Person update(String personId, Person person) {
    return this.personClientService.update(personId, person);
  }

  public Person findOne(String personId) {
    return this.personClientService.findOne(personId);
  }

  public List<Person> findAll() {
    return this.personClientService.findAll();
  }

  public Person findByOrcid(String orcId) {
    return this.personClientService.findByOrcid(orcId);
  }

  public List<Person> searchByProperties(Map<String, String> properties) {
    return this.personClientService.searchByProperties(properties);
  }

  public List<Person> search(String search, String matchType) {
    return this.personClientService.search(search, matchType);
  }

  public void delete(String personId) {
    this.personClientService.delete(personId);
  }

  public void deleteList(String[] ids) {
    this.personClientService.deleteList(ids);
  }

  public List<Institution> getInstitutions(String personId) {
    return this.personClientService.getInstitutions(personId);
  }

  public void addInstitution(String personId, String institutionId) {
    this.personClientService.addInstitution(personId, institutionId);
  }

  public List<Project> getProjects(String personId) {
    return this.personClientService.getProjects(personId);
  }

  public void addProject(String personId, String projectId) {
    this.personClientService.addProject(personId, projectId);
  }

  public void addProject(String personId, String projectId, String... roleNames) {
    this.personClientService.addProject(personId, projectId, roleNames);
  }

  public void removeProject(String personId, String projectId) {
    this.personClientService.removeProject(personId, projectId);
  }

  public void removeProject(String personId, String projectId, String roleName) {
    this.personClientService.removeProject(personId, projectId, roleName);
  }

  public Person uploadAvatar(String resId, Resource file) {
    return this.personClientService.uploadAvatar(resId, file);
  }

  public void downloadAvatar(String resId, Path path) {
    this.personClientService.downloadAvatar(resId, path);
  }

  public void deleteAvatar(String resId) {
    this.personClientService.deleteAvatar(resId);
  }

  public Person getPermanentTestRemotePerson(ApplicationRole applicationRole) {
    String applicationRoleId = applicationRole.getResId();
    String firstName = HederaTestConstants.getNameWithPermanentLabel(applicationRoleId + FIRST_NAME_TEST_SUFFIX);
    String lastName = HederaTestConstants.getNameWithPermanentLabel(applicationRoleId + LAST_NAME_TEST_SUFFIX);
    List<Person> people = this.personClientService.searchByProperties(Map.of("lastName", lastName, "firstName", firstName));
    for (Person person : people) {
      if (person.getFirstName().equals(firstName) && person.getLastName().equals(lastName)) {
        return person;
      }
    }
    throw new IllegalStateException("Person for role " + applicationRole.getName() + " should have been created during initialization");
  }

  public Person createRemoteTemporaryPerson() {
    final Person person = new Person();
    person.setFirstName(HederaTestConstants.getRandomNameWithTemporaryLabel("Any First name"));
    person.setLastName(HederaTestConstants.getRandomNameWithTemporaryLabel("Any Last name"));
    person.setOrcid(HederaTestConstants.getRandomOrcId());
    return this.create(person);
  }

  private void clearPeopleFixtures() {
    this.personClientService.findAll().stream()
            .filter(person -> person.getLastName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(person -> this.personClientService.delete(person.getResId()));
  }

  public User getUserWithRole(ApplicationRole role) {
    return this.userClientService.searchByProperties(Map.of("email", NO_REPLY_PREFIX + role.getName().toLowerCase() + "@unige.ch")).get(0);
  }

  @Override
  public void cleanTestData() {
    this.clearPeopleFixtures();
  }
}
