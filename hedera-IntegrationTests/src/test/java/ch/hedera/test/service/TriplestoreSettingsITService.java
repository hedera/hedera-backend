/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - TriplestoreSettingsITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.service;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.stereotype.Service;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.triplestore.DatasetSettings;
import ch.hedera.service.admin.ProjectClientService;
import ch.hedera.service.admin.TriplestoreSettingsClientService;
import ch.hedera.test.HederaTestConstants;

@Service
public class TriplestoreSettingsITService extends ITService {

  private final TriplestoreSettingsClientService triplestoreSettingsClientService;
  private final ProjectClientService projectClientService;
  public TriplestoreSettingsITService(SudoRestClientTool restClientTool, TriplestoreSettingsClientService triplestoreSettingsClientService,
          ProjectClientService projectClientService) {
    super(restClientTool);
    this.triplestoreSettingsClientService = triplestoreSettingsClientService;
    this.projectClientService = projectClientService;
  }

  public DatasetSettings create(String resId) {
    return this.triplestoreSettingsClientService.create(resId);
  }

  public DatasetSettings findOne(String resId) {
    return this.triplestoreSettingsClientService.findOne(resId);
  }

  public List<DatasetSettings> findAll() {
    return this.triplestoreSettingsClientService.findAll();
  }

  public void delete(String resId) {
    this.triplestoreSettingsClientService.delete(resId);
  }

  public void delete(List<String> resIds) {
    resIds.forEach(this.triplestoreSettingsClientService::delete);
  }

  /**
   * Return a String that is allowed as dataset name in Fuseki
   *
   * @param name
   * @return
   */
  public String getTemporaryDatasetName(String name) {
    return HederaTestConstants.TEMPORARY_TEST_DATASET_LABEL + name.replace(" ", "-") + ThreadLocalRandom.current().nextInt();
  }

  private void deleteEmptyTemporaryDatasets() {
    this.projectClientService.findAll().stream()
              .filter(project -> project.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL) && !project.hasData())
              .forEach(project ->this.delete(project.getShortName()));
  }

  @Override
  public void cleanTestData() {
    this.restClientTool.sudoAdmin();
    this.deleteEmptyTemporaryDatasets();
    this.restClientTool.exitSudo();
  }
}
