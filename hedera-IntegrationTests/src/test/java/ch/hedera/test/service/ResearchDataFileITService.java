/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ResearchDataFileITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.settings.Project;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.test.HederaTestConstants;

@Service
public class ResearchDataFileITService extends ITService {
  private static final Logger logger = LoggerFactory.getLogger(ResearchDataFileITService.class);

  @Value("${hedera.test.waitToAvoidSpurious403:0}")
  private int waitToAvoidSpurious403;

  private final ProjectITService projectITService;
  private final ResearchDataFileClientService researchDataFileClientService;

  public ResearchDataFileITService(SudoRestClientTool restClientTool, ProjectITService projectITService,
          ResearchDataFileClientService researchDataFileClientService) {
    super(restClientTool);
    this.projectITService = projectITService;
    this.researchDataFileClientService = researchDataFileClientService;
  }

  private void clearResearchDataFileFixtures() {
    int error = 0;
    for (Project project : this.projectITService.findAll()) {
      if (project.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        try {
          List<ResearchDataFile> researchDataFileList = this.researchDataFileClientService.findAllWithProjectId(project.getResId());
          researchDataFileList.forEach(researchDataFile -> this.researchDataFileClientService.delete(researchDataFile.getResId()));
        } catch (RuntimeException e) {
          logger.error("Cannot remove research data file for project: {} ({})", project.getResId(), project.getShortName());
          error++;
        }
      }
    }
    if (error > 0) {
      throw new SolidifyRuntimeException("There are " + error + " error(s) in project research data file clean process");
    }
  }

  public boolean waitAllResearchDataFilesIsProcessed(String projectId, int fileNumber) {
    int count = 0;
    List<ResearchDataFile> fileList = this.researchDataFileClientService.findAllWithProjectId(projectId);

    assertEquals(fileNumber, fileList.size());
    while (!fileList.stream().allMatch(f -> f.getStatus().equals(ResearchDataFile.ResearchDataFileFileStatus.COMPLETED))) {
      String initialMessage = "Project " + projectId + ": research data files are not all in COMPLETED status.";
      StringBuilder builder = new StringBuilder().append(initialMessage);
      for (final ResearchDataFile dataFile : fileList) {
        builder.append("\n Research data file ")
                .append(dataFile.getResId())
                .append(" has status ")
                .append(dataFile.getStatus());
      }
      HederaTestConstants.failAfterNAttempt(count, builder.toString());
      fileList = this.researchDataFileClientService.findAllWithProjectId(projectId);
      count++;
    }
    return true;
  }

  public boolean waitResearchDataFileIsProcessed(String researchDataFileId, ResearchDataFile.ResearchDataFileFileStatus endStatus) {
    int count = 0;
    ResearchDataFile researchDataFile;

    SolidifyTime.waitInSeconds(this.waitToAvoidSpurious403);

    do {
      researchDataFile = this.researchDataFileClientService.findOne(researchDataFileId);
      HederaTestConstants
              .failAfterNAttempt(count,
                      "Research data file " + researchDataFileId + " should have status Completed or InError instead of "
                              + researchDataFile.getStatus());
      count++;
    } while (researchDataFile.getStatus() != ResearchDataFile.ResearchDataFileFileStatus.COMPLETED
            && researchDataFile.getStatus() != ResearchDataFile.ResearchDataFileFileStatus.IN_ERROR);
    return (endStatus == researchDataFile.getStatus());
  }

  @Override
  public void cleanTestData() {
    this.restClientTool.sudoRoot();
    this.clearResearchDataFileFixtures();
    this.restClientTool.exitSudo();
  }
}
