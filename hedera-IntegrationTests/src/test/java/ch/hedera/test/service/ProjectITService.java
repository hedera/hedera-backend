/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ProjectITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.StorageType;
import ch.hedera.model.display.ProjectDTO;
import ch.hedera.model.humanities.Rml;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.FundingAgency;
import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.service.admin.ProjectClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.HederaTestConstants.PersistenceMode;
import ch.hedera.test.HederaTestConstants.ProjectStatus;

@Service
public class ProjectITService extends ITService {
  private static final Logger logger = LoggerFactory.getLogger(ProjectITService.class);

  @Value("${hedera.test.waitToAvoidSpurious403:0}")
  private int waitToAvoidSpurious403;

  // Main services
  private final PersonITService personITService;
  private final ProjectClientService projectClientService;
  private final ResearchObjectTypeITService researchObjectTypeITService;

  public ProjectITService(PersonITService personITService,
          ProjectClientService projectClientService,
          ResearchObjectTypeITService researchObjectTypeITService,
          SudoRestClientTool restClientTool) {
    super(restClientTool);
    // Services
    this.personITService = personITService;
    this.projectClientService = projectClientService;
    this.researchObjectTypeITService = researchObjectTypeITService;
  }

  public Project findOne(String projectId) {
    return this.projectClientService.findOne(projectId);
  }

  public Project findOneByShortName(String projectShortName) {
    return this.projectClientService.findByShortName(projectShortName);
  }

  public List<Project> findAll() {
    return this.projectClientService.findAll();
  }

  public List<Project> searchByProperties(Map<String, String> properties) {
    return this.projectClientService.searchByProperties(properties);
  }

  public Project create(Project project) {
    return this.projectClientService.create(project);
  }

  public void delete(String projectId) {
    this.projectClientService.delete(projectId);
  }

  public Project update(String projectId, Project project) {
    return this.projectClientService.update(projectId, project);
  }

  public Project update(String projectId, Project project, List<String> propertiesToUpdate) {
    return this.projectClientService.update(projectId, project, propertiesToUpdate);
  }

  public Project close(Project project) {
    return this.close(project.getProjectId(), LocalDate.now().minusDays(1).toString());
  }

  public Project close(String projectId, String date) {
    return this.projectClientService.close(projectId, date);
  }

  public void addPerson(String projectId, String personId) {
    this.projectClientService.addPerson(projectId, personId);
  }

  public void removePerson(String projectId, String personId) {
    this.projectClientService.removePerson(projectId, personId);
  }

  public void addPersonRole(String projectId, String personId, String projectRole) {
    this.projectClientService.addPersonRole(projectId, personId, projectRole);
  }

  public Role getPersonRole(String projectId, String personId) {
    return this.projectClientService.getPersonRole(projectId, personId);
  }

  public List<Person> getPeople(String projectId) {
    return this.projectClientService.getPeople(projectId);
  }

  public List<Institution> getInstitutions(String projectId) {
    return this.projectClientService.getInstitutions(projectId);
  }

  public List<Rml> getRmls(String projectId) {
    return this.projectClientService.getRmls(projectId);
  }

  public List<ResearchObjectType> getResearchObjectTypes(String projectId) {
    return this.projectClientService.getResearchObjectTypes(projectId);
  }

  public void addInstitution(String projectId, String institutionId) {
    this.projectClientService.addInstitution(projectId, institutionId);
  }

  public void removeInstitution(String projectId, String institutionId) {
    this.projectClientService.removeInstitution(projectId, institutionId);
  }

  public List<FundingAgency> getFundingAgencies(String projectId) {
    return this.projectClientService.getFundingAgencies(projectId);
  }

  public void addFundingAgency(String projectId, String fundingAgencyId) {
    this.projectClientService.addFundingAgency(projectId, fundingAgencyId);
  }

  public void removeFundingAgency(String projectId, String fundingAgencyId) {
    this.projectClientService.removeFundingAgency(projectId, fundingAgencyId);
  }

  public void addRml(String projectId, String rmlId) {
    this.projectClientService.addRml(projectId, rmlId);
  }

  public void removeRml(String projectId, String rmlId) {
    this.projectClientService.removeRml(projectId, rmlId);
  }

  public void addResearchObjectType(String projectId, String researchObjectId) {
    this.projectClientService.addResearchObjectType(projectId, researchObjectId);
  }

  public void addResearchObjectType(String projetcId, List<String> researchObjectTypeList) {
    List<ResearchObjectType> researchObjectTypeListOfProject = this.getResearchObjectTypes(projetcId);
    for (String researchObjectTypeId : researchObjectTypeList) {
      if (researchObjectTypeListOfProject.stream().filter(rot -> rot.getResId().equals(researchObjectTypeId)).count() == 0) {
        assertDoesNotThrow(() -> this.addResearchObjectType(projetcId, researchObjectTypeId));
      }
    }
  }

  public void removeResearchObjectType(String projectId, String researchObjectId) {
    this.projectClientService.removeResearchObjectType(projectId, researchObjectId);
  }

  public Project uploadLogo(String projectId, Resource logoToUpload) {
    this.projectClientService.uploadLogo(projectId, logoToUpload);
    return this.projectClientService.findOne(projectId);
  }

  public void downloadLogo(String projectId, Path path) {
    this.projectClientService.downloadLogo(projectId, path);
  }

  public void deleteLogo(String projectId) {
    this.projectClientService.deleteLogo(projectId);
  }

  public void enforcePersonHasRoleInProject(Project project, Person person, String projectRole) {
    this.projectClientService.addPersonRole(project.getResId(), person.getResId(), projectRole);
    SolidifyTime.waitInSeconds(this.waitToAvoidSpurious403);
    final Role effectiveProjectRole = this.projectClientService.getPersonRole(project.getResId(), person.getResId());
    if (!effectiveProjectRole.getResId().equals(projectRole)) {
      throw new IllegalStateException("Person " + person.getResId() + " in project " + project.getResId() + " has role "
              + effectiveProjectRole.getResId() + " instead of " + projectRole);
    } else {
      logger.info("Person {} has project role {} on project {}", person.getFullName(), projectRole, project.getResId());
    }
  }

  public Project getSecondPermanentProject() {
    return this.getOrCreatePermanentProject("project-2", "Project 2", true, false);
  }

  public Project getPublicPermanentProject() {
    return this.getOrCreatePermanentProject("public-project", "Public project", true, false);
  }

  public Project getPrivatePermanentProject() {
    return this.getOrCreatePermanentProject("private-project", "Private project", false, false);
  }

  public Project getClosedPermanentProject() {
    return this.getOrCreatePermanentProject("closed-project", "Closed project", true, true);
  }

  private Project getOrCreatePermanentProject(String resId, String name, boolean isPublic, boolean isClosed) {
    String shortName = "perm-" + resId;
    final String projectName = HederaTestConstants.getNameWithPermanentLabel(name);
    final List<Project> projects = this.projectClientService.searchByProperties(Map.of("name", projectName));

    final Optional<Project> optionalProject = projects.stream().filter(item -> item.getName().equals(projectName)).findFirst();
    Project project = null;
    if (optionalProject.isEmpty()) {
      project = new Project();
      project.setResId(resId);
      project.setName(projectName);
      project.setShortName(shortName);
      project.setAccessPublic(isPublic);
      if (isClosed) {
        project.setOpeningDate(LocalDate.now().minusYears(5));
        project.setClosingDate(project.getOpeningDate().plusYears(2));
      }
      project.setUrl(this.generateRandomUrl());
      project.setIiifManifestSparqlQuery(this.getIIIFManifestQuery());
      project.setResearchDataFileResearchObjectType(this.researchObjectTypeITService.getResearchObjectTypeForResearchDataFile());
      // Add research object type for the IIIF manifest
      project.setIiifManifestResearchObjectType(this.researchObjectTypeITService.getResearchObjectType("cidoc-crm-pliego"));
      project = this.projectClientService.create(project);
    } else {
      project = optionalProject.get();
    }
    return project;
  }

  public Project createRemoteTemporaryProject(String acronym, String description) {
    Project project = this.createLocalTemporaryProjectWithoutResearchObjectType(acronym, description, null);
    project.setResearchDataFileResearchObjectType(this.researchObjectTypeITService.getResearchObjectTypeForResearchDataFile());
    return this.create(project);
  }

  public Project createLocalTemporaryProjectWithoutResearchObjectType(String acronym, String description, List<Institution> listInstitutions) {
    // Create a new Project
    ProjectDTO project = new ProjectDTO();
    project.setName(HederaTestConstants.getRandomNameWithTemporaryLabel(description));
    project.setShortName(acronym);
    project.setUrl(this.generateRandomUrl());
    project.setDescription(description);

    if (listInstitutions != null) {
      project.setInstitutions(new ArrayList<>());
      listInstitutions.forEach(project::addInstitution);
    }

    project.setKeywords(Arrays.asList("magnetostatics", "photonics", "oceanography"));
    return project;
  }

  public List<Project> findProjects(String name) {
    return this.projectClientService.searchByProperties(Map.of("name", name));
  }

  public Project createTemporaryProject(ProjectStatus projectStatus, String projectRole, ApplicationRole applicationRole) {
    logger.info("Creating temporary project for project role {}, application role {} and status {}", projectRole, applicationRole.getResId(), projectStatus);
    this.restClientTool.sudoAdmin();
    final Project localProject = this.createLocalProject(PersistenceMode.TEMPORARY, projectStatus, projectRole);
    final Project remoteProject = this.create(localProject);
    logger.info("Project {} created", remoteProject.getResId());
    if (applicationRole != null) {
      final Person person = this.personITService.getPermanentTestRemotePerson(applicationRole);
      if (projectRole != null && !NO_ONE.equals(projectRole)) {
        this.enforcePersonHasRoleInProject(remoteProject, person, projectRole);
      }
    }
    this.restClientTool.exitSudo();
    return remoteProject;
  }

  public Project getPermanentProjectWithRole(String role) {
    this.restClientTool.sudoAdmin();
    Project project = this.getOrCreatePermanentProject(ProjectStatus.OPEN, role,
            AuthApplicationRole.USER);
    this.restClientTool.exitSudo();
    return project;
  }

  public Project getOrCreatePermanentProjectAsManager(ProjectStatus projectStatus) {
    return this.getOrCreatePermanentProject(projectStatus, Role.MANAGER_ID);
  }

  public Project getOrCreatePermanentProjectForNoOne() {
    return this.getOrCreatePermanentProject(ProjectStatus.OPEN, NO_ONE);
  }

  public Project getOrCreatePermanentProject(ProjectStatus projectStatus, String projectRole) {
    return this.getOrCreatePermanentProject(projectStatus, projectRole, AuthApplicationRole.USER);
  }

  public Project getOrCreatePermanentProject(ProjectStatus projectStatus, String projectRole, ApplicationRole applicationRole) {
    return this.getOrCreatePermanentProject(projectStatus, StorageType.FILE, projectRole, applicationRole);
  }

  public Project getOrCreatePermanentProject(ProjectStatus projectStatus, StorageType storageType, String projectRole,
          ApplicationRole applicationRole) {
    return this.getOrCreateProject(PersistenceMode.PERMANENT, projectStatus, storageType, projectRole, applicationRole);
  }

  public Project getOrCreateProject(PersistenceMode persistenceMode, ProjectStatus projectStatus, StorageType storageType, String projectRole,
          ApplicationRole applicationRole) {
    Project project = this.createLocalProject(persistenceMode, projectStatus, projectRole);
    project.setStorageType(storageType);
    final String projectName = project.getName();

    final List<Project> projects = this.projectClientService.searchByProperties(Map.of("name", projectName));

    final Optional<Project> optionalProject = projects.stream().filter(item -> item.getName().equals(projectName)).findFirst();

    this.restClientTool.sudoAdmin();
    if (optionalProject.isPresent()) {
      project = this.projectClientService.update(optionalProject.get().getResId(), project);
    } else {
      project = this.projectClientService.create(project);
    }
    // Add person
    if (applicationRole != null && projectRole != null && !NO_ONE.equals(projectRole)) {
      final Person person = this.personITService.getPermanentTestRemotePerson(applicationRole);
      this.enforcePersonHasRoleInProject(project, person, projectRole);
    }
    this.restClientTool.exitSudo();
    return project;
  }

  public Project createLocalProject(PersistenceMode persistenceMode, ProjectStatus projectStatus, String projectRole) {
    final Project project = new Project();
    if (persistenceMode == PersistenceMode.PERMANENT) {
      project.setResId(this.getProjectId(projectRole));
    }
    project.setName(this.getProjectName(persistenceMode, projectRole));
    project.setShortName(this.getProjectShortName(persistenceMode, projectRole));
    project.setKeywords(Arrays.asList("chromatography", "flocculation", "ultrafiltration"));
    project.setUrl(this.generateRandomUrl());
    switch (projectStatus) {
      case CLOSED -> {
        project.setOpeningDate(LocalDate.now().minusYears(10));
        project.setClosingDate(LocalDate.now().minusYears(5));
      }
      case OPEN -> {
        project.setOpeningDate(LocalDate.now().minusYears(10));
        project.setClosingDate(LocalDate.now().plusYears(5));
      }
    }
    // Add researchDataFileForProject
    project.setResearchDataFileResearchObjectType(this.researchObjectTypeITService.getResearchObjectTypeForResearchDataFile());
    return project;
  }

  public void setRandomUrl(Project project) {
    try {
      project.setUrl(new URL("https://unige.ch/" + ThreadLocalRandom.current().nextInt()));
    } catch (final MalformedURLException e) {
      e.printStackTrace();
    }
  }

  private String getProjectId(String projectRole) {
    String resId;
    if (projectRole == null) {
      resId = "project";
    } else {
      resId = projectRole.toLowerCase() + "-project";
    }
    return resId;
  }

  private String getProjectName(PersistenceMode persistenceMode, String projectRole) {
    String name;
    if (projectRole == null) {
      name = "Project";
    } else {
      name = "Project for " + projectRole.toLowerCase();
    }
    return this.createLabel(persistenceMode, name);
  }

  private String getProjectShortName(PersistenceMode persistenceMode, String projectRole) {
    StringBuilder shortName = new StringBuilder();
    if (persistenceMode.equals(PersistenceMode.PERMANENT)) {
      shortName.append("perm-");
    } else {
      shortName.append("temp-");
    }
    shortName.append("project");
    if (projectRole != null) {
      shortName.append("-").append(projectRole.toLowerCase());
    }
    if (persistenceMode.equals(PersistenceMode.TEMPORARY)) {
      shortName.append("-").append(ThreadLocalRandom.current().nextInt());
    }
    return shortName.toString();
  }

  public Project openProject(Project project) {
    project.setOpeningDate(LocalDate.now().minusDays(5));
    project.setClosingDate(LocalDate.now().plusDays(5));
    this.update(project.getProjectId(), project, List.of("openingDate", "closingDate"));
    return this.findOne(project.getProjectId());
  }

  public Project closeProject(Project project) {
    return this.close(project.getProjectId(), LocalDate.now().minusDays(1).toString());
  }

  public List<Project> getAuthorizedProjects() {
    return this.projectClientService.getAuthorizedProject();
  }

  public void reopenTemporaryProjects() {
    int error = 0;
    for (Project project : this.projectClientService.findAll()) {
      if (project.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        try {
          this.openProject(project);
        } catch (RuntimeException e) {
          logger.error("Cannot open project: {} ({})", project.getResId(), project.getShortName());
          error++;
        }
      }
    }
    if (error > 0) {
      throw new SolidifyRuntimeException("There are " + error + " error(s) in project open process");
    }
  }

  private void clearProjectFixtures() {
    int error = 0;
    for (Project project : this.projectClientService.findAll()) {
      if (project.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        try {
          this.projectClientService.delete(project.getResId());
        } catch (RuntimeException e) {
          logger.error("Cannot delete project: {} ({})", project.getResId(), project.getShortName());
          error++;
        }
      }
    }
    if (error > 0) {
      throw new SolidifyRuntimeException("There are " + error + " error(s) in project clearing");
    }
  }

  @Override
  public void cleanTestData() {
    this.clearProjectFixtures();
  }

  public String getIIIFManifestQuery() {
    final String query = """
            PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
            PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
            PREFIX la: <https://linked.art/ns/terms/>
            PREFIX exif: <https://www.w3.org/2003/12/exif/ns#>
            PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo#>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX owl: <http://www.w3.org/2002/07/owl#>

            SELECT ?resource_subject ?img_subject ?manifest_md_place_name ?manifest_md_publisher ?manifest_md_begin_date ?manifest_md_end_date ?page_id ?label ?img_height ?img_width ?img_iiif_url
            WHERE {
              ?resource_subject a crm:E22_Human-Made_Object .
              ?resource_subject crm:P46_is_composed_of ?page .
              ?page la:digitally_shown_by  ?img_subject .
              ?pn_id crm:P1_identifies ?page .
              ?pn_id rdf:value ?page_id .
              ?img_subject a crmdig:D1_Digital_Object .
              ?img_subject crmdig:L54_is_same-as ?img_iiif_url .
              optional {
                ?resource_subject crm:P1_is_identified_by ?label_appellation .
                ?label_appellation a crm:E41_Appellation .
                ?label_appellation crm:P190_has_symbolic_content ?label
              } .
              optional { ?img_subject exif:height ?img_height ;
                                      exif:width ?img_width }.
              ?production a frbroo:F30_Publication_Event .
              ?production crm:P108_has_produced ?resource_subject .
              ?production crm:P14_carried_out_by ?actor .
              ?production  crm:P4_has_time-span ?time_span .
              ?time_span crm:P82a_begin_of_the_begin ?manifest_md_begin_date .
              optional{
                  ?time_span crm:P82b_end_of_the_begin ?manifest_md_end_date
              }
              optional {
                ?actor crm:P1_is_identified_by [ crm:P190_has_symbolic_content ?manifest_md_publisher ] .
                ?actor crm:P74_has_former_or_current_residence ?place .
                ?place crm:P1_is_identified_by [
                  crm:P190_has_symbolic_content ?manifest_md_place_name
                ]
              }
            }
            ORDER BY ?img_iiif_url
                        """;
    return query;
  }

  public boolean waitForProjectHasData(String projectId, boolean hasData) {
    int count = 0;
    Project project;
    do {
      project = this.projectClientService.findOne(projectId);
      HederaTestConstants.failAfterNAttempt(count, "The project " + projectId + "is has not already data linked to it " + project.hasData());
      count++;

    } while (hasData ? !project.hasData() : project.hasData());
    return true;
  }

}
