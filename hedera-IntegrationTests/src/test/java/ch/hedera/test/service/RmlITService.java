/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - RmlITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.service;

import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.humanities.Rml;
import ch.hedera.model.humanities.RmlFormat;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.test.HederaTestConstants;

@Service
public class RmlITService extends ITService {

  public RmlClientService rmlClientService;

  public RmlITService(SudoRestClientTool restClientTool, RmlClientService rmlClientService) {
    super(restClientTool);
    this.rmlClientService = rmlClientService;
  }

  public Rml createRemoteRml(String name, RmlFormat format, String version, String description, Resource file) {
    Rml rml = new Rml();
    rml.setName(name);
    rml.setFormat(format);
    rml.setDescription(description);
    rml.setVersion(version);

    return this.createRemoteRml(rml, file);
  }

  public Rml createRemoteRml(Rml rml, Resource file) {
    return this.rmlClientService.createAndUploadRdfFile(rml, file);
  }

  private void clearRmlFixtures() {

    final List<Rml> rmls = this.rmlClientService.findAll();

    for (final Rml rml : rmls) {
      if (rml.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        /*
         * Remove eventual links with projects first
         */
        final List<Project> projects = this.rmlClientService.getProjects(rml.getResId());
        if (projects != null && !projects.isEmpty()) {
          for (final Project po : projects) {
            this.rmlClientService.removeProject(rml.getResId(), po.getResId());
          }
        }
      }
    }

    this.rmlClientService.findAll().stream()
            .filter(rml -> rml.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(rml -> this.rmlClientService.delete(rml.getResId()));
  }

  public List<Rml> searchByProperties(Map<String, String> properties) {
    return this.rmlClientService.searchByProperties(properties);
  }

  @Override
  public void cleanTestData() {
    this.restClientTool.sudoAdmin();
    this.clearRmlFixtures();
    this.restClientTool.exitSudo();
  }
}
