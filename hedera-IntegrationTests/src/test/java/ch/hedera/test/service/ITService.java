/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ThreadLocalRandom;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.HederaTestConstants.PersistenceMode;

public abstract class ITService {

  protected final static String NO_ONE = "no-one";

  protected final SudoRestClientTool restClientTool;

  public ITService(SudoRestClientTool restClientTool) {
    this.restClientTool = restClientTool;
  }

  public abstract void cleanTestData();

  protected String createLabel(PersistenceMode persistenceMode, String name) {
    if (persistenceMode.equals(PersistenceMode.PERMANENT)) {
      return HederaTestConstants.getNameWithPermanentLabel(name);
    }
    if (persistenceMode.equals(PersistenceMode.TEMPORARY)) {
      return HederaTestConstants.getRandomNameWithTemporaryLabel(name);
    }
    throw new IllegalStateException("Unmanaged persistence mode " + persistenceMode);
  }

  protected URL generateRandomUrl() {
    URL url = null;
    try {
      url = new URL("https://unige.ch/" + ThreadLocalRandom.current().nextInt());
    } catch (final MalformedURLException e) {
      e.printStackTrace();
    }
    return url;
  }

}
