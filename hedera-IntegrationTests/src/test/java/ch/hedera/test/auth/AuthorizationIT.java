/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AuthorizationIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.auth;

import static ch.hedera.rest.UrlPath.ACCESS;
import static ch.hedera.rest.UrlPath.ACCESS_PROJECT;
import static ch.hedera.rest.UrlPath.ACCESS_RESEARCH_OBJECT;
import static ch.hedera.rest.UrlPath.ADMIN;
import static ch.hedera.rest.UrlPath.ADMIN_AUTHORIZED_PROJECT;
import static ch.hedera.rest.UrlPath.ADMIN_FUNDING_AGENCY;
import static ch.hedera.rest.UrlPath.ADMIN_INSTITUTION;
import static ch.hedera.rest.UrlPath.ADMIN_LANGUAGE;
import static ch.hedera.rest.UrlPath.ADMIN_LICENSE;
import static ch.hedera.rest.UrlPath.ADMIN_LICENSE_IMPORT_FILE;
import static ch.hedera.rest.UrlPath.ADMIN_LICENSE_IMPORT_LIST;
import static ch.hedera.rest.UrlPath.ADMIN_OPEN_LICENSE_IMPORT_FILE;
import static ch.hedera.rest.UrlPath.ADMIN_PERSON;
import static ch.hedera.rest.UrlPath.ADMIN_PROJECT;
import static ch.hedera.rest.UrlPath.ADMIN_ROLE;
import static ch.hedera.rest.UrlPath.ADMIN_USER;
import static ch.hedera.rest.UrlPath.INGEST;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.PATCH;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;

import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.IndexUrlPath;
import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.OAIUrlPath;
import ch.unige.solidify.rest.ActionName;

import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ResourceName;

@Disabled("Run this manually")
@ExtendWith(SpringExtension.class)
@SpringBootTest
class AuthorizationIT {

  @Autowired
  private Environment env;
  private static final Logger log = LoggerFactory.getLogger(AuthorizationIT.class);
  private final static String accessionUrl = "http://localhost:16108/hedera";
  private final static String ingestionUrl = "http://localhost:16106/hedera";
  private final static String administrationUrl = "http://localhost:16105/hedera";
  private String userToken;

  private static final String LAST_ID = "/id";
  private static final String MIDDLE_ID = "/id/";

  private static final List<HttpMethod> httpMethodToTest = Arrays.asList(GET, POST, PUT, PATCH, DELETE);

  @BeforeEach
  public void init() {
    this.userToken = this.env.getProperty("solidify.oauth2.accesstoken");
  }

  @Test
  void testAccessModule() {
    final String[] resourceURLs = {
            ACCESS,

            OAIUrlPath.OAI_PROVIDER,
            OAIUrlPath.OAI_PROVIDER + "/" + OAIConstants.OAI_RESOURCE,
            OAIUrlPath.OAI_PROVIDER + "/" + OAIConstants.OAI_RESOURCE + "/" + ResourceName.XSL,

            OAIUrlPath.OAI_SET,

            ACCESS_RESEARCH_OBJECT,
            ACCESS_RESEARCH_OBJECT + "/" + ActionName.SEARCH,

            ACCESS_PROJECT,
            ACCESS_PROJECT + MIDDLE_ID + HederaActionName.DOWNLOAD_LOGO,

    };
    this.testModule(resourceURLs, accessionUrl);
  }

  @Test
  void testAdminModule() {
    final String[] resourceURLs = {
            ADMIN,

            ADMIN_INSTITUTION,
            ADMIN_INSTITUTION + MIDDLE_ID + ResourceName.PROJECT,
            ADMIN_INSTITUTION + MIDDLE_ID + ResourceName.PROJECT + LAST_ID,
            ADMIN_INSTITUTION + MIDDLE_ID + ResourceName.PERSON,
            ADMIN_INSTITUTION + MIDDLE_ID + ResourceName.PERSON + LAST_ID,

            ADMIN_PERSON,
            ADMIN_PERSON, "/" + HederaActionName.SEARCH_WITH_USER,
            ADMIN_PERSON + MIDDLE_ID + ResourceName.PROJECT,
            ADMIN_PERSON + MIDDLE_ID + ResourceName.PROJECT + LAST_ID,
            ADMIN_PERSON + MIDDLE_ID + ResourceName.INSTITUTION,
            ADMIN_PERSON + MIDDLE_ID + ResourceName.INSTITUTION + LAST_ID,

            ADMIN_PROJECT,
            ADMIN_PROJECT + MIDDLE_ID + ActionName.CLOSE,
            ADMIN_PROJECT + "/" + HederaActionName.UPLOAD_LOGO,
            ADMIN_PROJECT + "/" + HederaActionName.DOWNLOAD_LOGO,
            ADMIN_PROJECT + "/" + HederaActionName.DELETE_LOGO,
            ADMIN_PROJECT + MIDDLE_ID + ResourceName.PERSON,
            ADMIN_PROJECT + MIDDLE_ID + ResourceName.PERSON + LAST_ID,
            ADMIN_PROJECT + MIDDLE_ID + ResourceName.FUNDING_AGENCY,
            ADMIN_PROJECT + MIDDLE_ID + ResourceName.FUNDING_AGENCY + LAST_ID,
            ADMIN_PROJECT + MIDDLE_ID + ResourceName.INSTITUTION,
            ADMIN_PROJECT + MIDDLE_ID + ResourceName.INSTITUTION + LAST_ID,

            ADMIN_AUTHORIZED_PROJECT,
            ADMIN_AUTHORIZED_PROJECT + LAST_ID,

            ADMIN_ROLE,

            ADMIN_LICENSE,
            ADMIN_LICENSE + ADMIN_LICENSE_IMPORT_LIST,
            ADMIN_LICENSE + ADMIN_LICENSE_IMPORT_FILE,
            ADMIN_LICENSE + ADMIN_OPEN_LICENSE_IMPORT_FILE,

            ADMIN_LANGUAGE,

            ADMIN_FUNDING_AGENCY,
            ADMIN_FUNDING_AGENCY + MIDDLE_ID + ResourceName.PROJECT,
            ADMIN_FUNDING_AGENCY + MIDDLE_ID + ResourceName.PROJECT + LAST_ID,

            ADMIN_USER,
            ADMIN_USER + "/" + HederaActionName.AUTHENTICATED,
            ADMIN_USER + "/" + HederaActionName.REVOKE_ALL_TOKENS + LAST_ID,
            // Don't test REVOKE_MY_TOKENS otherwise test user will loose its token
            // ADMIN_USER + "/" + HederaActionName.REVOKE_MY_TOKENS,
            ADMIN_USER + MIDDLE_ID + HederaActionName.UPLOAD_AVATAR,
            ADMIN_USER + MIDDLE_ID + HederaActionName.DOWNLOAD_AVATAR,
            ADMIN_USER + MIDDLE_ID + HederaActionName.DELETE_AVATAR,

    };
    this.testModule(resourceURLs, administrationUrl);
  }

  @Test
  void testIndexModule() {
    final String[] resourceURLs = {
            IndexUrlPath.INDEX,

            IndexUrlPath.INDEX_SETTING,
            IndexUrlPath.INDEX_SETTING + "/" + IndexConstants.DELETE_ALL,

            IndexUrlPath.INDEX_INDEX_FIELD_ALIAS
    };
    this.testModule(resourceURLs, administrationUrl);
  }

  @Test
  void testIngestModule() {
    final String[] resourceURLs = {
            INGEST
    };
    this.testModule(resourceURLs, ingestionUrl);
  }

  private void testModule(String[] resourceURLs, String applicationUrl) {
    final String[] trailingSlashes = { "", "/" };
    ConnectionProvider fixedPool = ConnectionProvider.builder("fixedPool")
            .maxConnections(20000)
            .pendingAcquireTimeout(Duration.ofMinutes(3))
            .build();
    HttpClient httpClient = HttpClient.create(fixedPool);
    final WebClient client = WebClient.builder()
            .baseUrl(applicationUrl)
            .clientConnector(new ReactorClientHttpConnector(httpClient))
            .codecs(clientCodecConfigurer -> clientCodecConfigurer
                    .defaultCodecs()
                    .jackson2JsonEncoder(new Jackson2JsonEncoder(
                            // Allow to send empty Json object for test purposes
                            new ObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false), MediaType.APPLICATION_JSON)))
            .build();
    try {
      final PrintWriter printWriter = new PrintWriter(new FileWriter("authResults.txt", true), true);
      for (final HttpMethod httpMethod : httpMethodToTest) {
        for (String resourceURL : resourceURLs) {
          if (httpMethod.equals(PATCH) || httpMethod.equals(PUT) || httpMethod.equals(DELETE)) {
            resourceURL = resourceURL + LAST_ID;
          }
          final List<ClientResponse> clientResponseList = new ArrayList<>();
          for (final String trailingSlash : trailingSlashes) {
            Object objectToSend = new Object();
            if (resourceURL.equals(ADMIN_LICENSE + ADMIN_LICENSE_IMPORT_LIST)) {
              objectToSend = Collections.emptyList();
            }
            Consumer<HttpHeaders> headersConsumer = headers -> headers.setBearerAuth(this.userToken);
            if ((resourceURL.equals(ADMIN_LICENSE + ADMIN_OPEN_LICENSE_IMPORT_FILE) ||
                    resourceURL.equals(ADMIN_LICENSE + ADMIN_LICENSE_IMPORT_FILE))) {
              headersConsumer = headersConsumer
                      .andThen(headers -> headers.add(HttpHeaders.CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA_VALUE + ";boundary=a"));
              MultipartBodyBuilder builder = new MultipartBodyBuilder();
              Resource image = new ClassPathResource("geneve.jpg");
              builder.part("file", image);
              objectToSend = builder.build();
            }
            final ClientResponse clientResponse;
            if (httpMethod.equals(POST) || httpMethod.equals(PATCH)) {
              clientResponse = this.restCallWithBody(client, httpMethod, resourceURL + trailingSlash, headersConsumer, objectToSend);
            } else {
              clientResponse = this.restCallWithoutBody(client, httpMethod, resourceURL + trailingSlash, headersConsumer);
            }
            clientResponseList.add(clientResponse);
          }
          this.registerResults(clientResponseList, httpMethod, applicationUrl, resourceURL, printWriter);
        }
      }
      printWriter.close();
    } catch (IOException e) {
      log.error("Error when testing application module", e.getMessage());
    }
  }

  private ClientResponse restCallWithBody(WebClient client, HttpMethod httpMethod, String uri, Consumer<HttpHeaders> headersConsumer,
          Object objectToSend) {
    return client.method(httpMethod).uri(uri).headers(headersConsumer).bodyValue(objectToSend).exchange().block();
  }

  private ClientResponse restCallWithoutBody(WebClient client, HttpMethod httpMethod, String uri, Consumer<HttpHeaders> headersConsumer) {
    return client.method(httpMethod).uri(uri).headers(headersConsumer).exchange().block();
  }

  private void registerResults(List<ClientResponse> clientResponseList, HttpMethod httpMethod, String applicationUrl, String resourceURL,
          PrintWriter printWriter) {
    final ClientResponse responseWithoutSlash = clientResponseList.get(0);
    final ClientResponse responseWithSlash = clientResponseList.get(1);
    if (responseWithoutSlash.statusCode().equals(responseWithSlash.statusCode())) {
      String output = httpMethod + " " + applicationUrl + resourceURL + " " + responseWithoutSlash.statusCode();
      System.out.println(output);
      printWriter.println(output);
    } else {
      String output = "WARNING response with and without slash differ\n"
              + httpMethod + " " + applicationUrl + resourceURL + " " + responseWithoutSlash.statusCode() + "\n"
              + httpMethod + " " + applicationUrl + resourceURL + "/ " + responseWithSlash.statusCode();
      System.out.println(output);
      printWriter.println(output);
    }
  }
}
