/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - HederaTestConstants.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.concurrent.ThreadLocalRandom;

import ch.unige.solidify.util.SolidifyTime;

public class HederaTestConstants {
  public final static int TRIES = 120;

  public static final String THIRD_PARTY_USER_ID = "THIRD-PARTY-USER";

  public static final String PERMANENT_TEST_DATA_LABEL = "[Permanent Test Data] ";
  public static final String TEMPORARY_TEST_DATA_LABEL = "[Temporary Test Data] ";
  public static final String TEMPORARY_TEST_DATASET_LABEL = "Temporary-Test-Dataset-";
  public static final String TEMPORARY_TEST_DATA_NAME = "temp-";
  public static final String TEST_DATA_DESCRIPTION = "Description for testing";
  public final static String MISSING_TMP_RDF_DATASET_FILE_MSG = "A temporary test RdfDatasetFile is missing";
  public static final String JAVA_TEMPORARY_FOLDER = "java.io.tmpdir";
  public static final String REFERENCE_FILE_TO_UPLOAD = "bootstrap.yml";
  public static final String DEMO_RML_FILE_TO_UPLOAD = "demo.rml.ttl";
  public static final String DEMO_RML_NAME = "demo";
  public static final String TEST_RML_FILE_TO_UPLOAD = "test.rml.ttl";
  public static final String TEST_RML_NAME = "test";
  public static final String SOURCE_DATASET_NAME = "Source Dataset";
  public static final String SOURCE_DATASET_FILE_TO_UPLOAD = "dataset.csv";

  public static final String RDF_SOURCE_DATASET_FILE_FILENAME = "pliegos/pliegos.nt";
  public static final String FIRST_SOURCE_DATASET_FILE_FILENAME = "Moreno_001.xml";
  public static final String SECOND_SOURCE_DATASET_FILE_FILENAME = "Moreno_002.xml";
  public static final String THIRD_SOURCE_DATASET_FILE_FILENAME = "Moreno_003.xml";

  public static final String RESEARCH_DATAFILE_TO_UPLOAD = "rosario_revue.pdf";
  public static final String ONTOLOGY_FILE_TO_UPLOAD = "CIDOC_CRM_v7.1.2.rdf";
  public static final String ONTOLOGY_FILE_NOT_VALID_TO_UPLOAD = "CIDOC_CRM_v5.1.2.xml";

  public static final String TEST_RES_ID = "test";

  // Test Order - ROLE
  public static final int ROOT_TEST_ORDER = 0;
  public static final int ADMINISTRATOR_TEST_ORDER = 1;
  public static final int MANAGER_TEST_ORDER = 2;
  public static final int CREATOR_TEST_ORDER = 3;
  public static final int VISITOR_TEST_ORDER = 4;
  // Test Order - MODULE
  public static final int ADMIN_TEST_ORDER = 1;
  public static final int INGEST_TEST_ORDER = 100;
  public static final int ACCESS_TEST_ORDER = 200;
  // Test Order - ENTITY
  public static final int INGEST_SOURCE_DATASET_TEST_ORDER = 0;
  public static final int INGEST_RESEARCH_DATA_FILE_TEST_ORDER = 10;
  public static final int INGEST_SOURCE_DATASET_FILE_TEST_ORDER = 20;
  public static final int INGEST_RDF_DATASET_TEST_ORDER = 30;

  // Test Tag
  public static final String AS_ROOT_TAG = "AsRoot";
  public static final String AS_ADMIN_TAG = "AsAdmin";
  public static final String AS_MANAGER_TAG = "AsManager";
  public static final String AS_CREATOR_TAG = "AsCreator";
  public static final String AS_VISITOR_TAG = "AsVisitor";

  public final static String SPARQL_QUERY = "PREFIX foaf:  <http://xmlns.com/foaf/0.1/>\n"
          + "SELECT ?name\n"
          + "WHERE {\n"
          + "    ?person foaf:name ?name .\n"
          + "}";

  public final static String SPARQL_QUERY_DETAIL = "SELECT * WHERE { ?subject ?predicate ?object }";

  public enum ProjectStatus {
    CLOSED, OPEN
  }

  public enum PersistenceMode {
    PERMANENT, TEMPORARY
  }

  public static void failAfterNAttempt(int count, String failMessage) {
    if (count > HederaTestConstants.TRIES) {
      fail(failMessage);
    }
    SolidifyTime.waitInMilliSeconds(200);
  }

  public static String getNameWithPermanentLabel(String name) {
    return PERMANENT_TEST_DATA_LABEL + name;
  }

  public static String getRandomName(String name) {
    return name + ThreadLocalRandom.current().nextInt();
  }

  public static String getRandomNameWithTemporaryLabel(String name) {
    return TEMPORARY_TEST_DATA_LABEL + HederaTestConstants.getRandomName(name);
  }

  public static String getRandomNameWithTemporaryName(String name) {
    return TEMPORARY_TEST_DATA_NAME + HederaTestConstants.getRandomName(name);
  }

  public static String getRandomOrcId() {
    String orcId = "";
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++) {
        orcId = orcId + ThreadLocalRandom.current().nextInt(10);
      }
      orcId = orcId + "-";
    }
    return orcId.substring(0, orcId.length() - 1);
  }
}
