/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - UserAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.model.SolidifyApplicationRole;
import ch.unige.solidify.util.SearchOperation;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.security.User;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.PersonITService;

@Tag(HederaTestConstants.AS_ADMIN_TAG)
class UserAsAdminIT extends AbstractAdminIT {

  private final PersonITService personITService;

  @Autowired
  public UserAsAdminIT(Environment env, SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService, PersonITService personITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
    this.personITService = personITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Disabled("Reactivate this thest, when a decision  about this behavior has been made")
  @Test
  void adminCanSetUserToAdminAndRevert() {
    User user = this.personITService.getUserWithRole(AuthApplicationRole.USER);
    user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ADMIN));
    User updatedUser = this.userService.update(user.getResId(), user);
    assertEquals(AuthApplicationRole.ADMIN, updatedUser.getApplicationRole(), "User must be set to ADMIN");
    // Restore user
    user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.USER));
    updatedUser = this.userService.update(user.getResId(), user);
    assertEquals(AuthApplicationRole.USER, updatedUser.getApplicationRole(), "User must be restored to USER");
  }

  @Test
  void adminCannotSetUserToRoot() {
    try {
      User user = this.personITService.getUserWithRole(AuthApplicationRole.USER);
      user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ROOT));
      this.userService.update(user.getResId(), user);
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void adminCannotSetAdminToRoot() {
    try {
      User user = this.personITService.getUserWithRole(AuthApplicationRole.ADMIN);
      user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ROOT));
      this.userService.update(user.getResId(), user);
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void adminCannotSetRootToAdmin() {
    try {
      User user = this.personITService.getUserWithRole(AuthApplicationRole.ROOT);
      user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ADMIN));
      this.userService.update(user.getResId(), user);
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void adminCannotSetRootToUser() {
    try {
      User user = this.personITService.getUserWithRole(AuthApplicationRole.ROOT);
      user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.USER));
      this.userService.update(user.getResId(), user);
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void searchTest() {
    // Get an user
    User user = this.personITService.getUserWithRole(AuthApplicationRole.USER);

    /*
     * Test case sensitive research REM: as the database stores firstname and lastname in
     * utf8_general_ci, the searchTerm finds results even with wrong case
     */
    String searchTerm = "ser_first"; // substring of firstname
    assertTrue(this.found(String.format("firstName~%s, lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, user));
    searchTerm = "ser_last"; // substring of lastname
    assertTrue(this.found(String.format("firstName~%s, lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, user));

    /*
     * Test case insensitive research
     */
    searchTerm = "ser_first"; // substring of firstname
    assertTrue(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, user));
    searchTerm = "ser_last"; // substring of lastname
    assertTrue(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, user));

    /*
     * Search on both firstName AND lastName (by default on backend)
     */
    assertFalse(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), null, user));

    /*
     * search on not existing term
     */
    searchTerm = "--notexisting--";
    assertFalse(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, user));
  }

  @Test
  void shouldSeeSomeAttribute() {
    User user = this.userService.getAuthenticatedUser();
    assertNotNull(user.getExternalUid());
    assertNotNull(user.getApplicationRole());
  }

  private boolean found(String search, String matchType, User userToFound) {
    final List<User> userList = this.userService.search(search, matchType);
    for (final User u : userList) {
      if (u.getResId().equals(userToFound.getResId())) {
        return true;
      }
    }
    return false;
  }

  @Override
  protected void deleteFixtures() {
    // Do nothing
  }
}
