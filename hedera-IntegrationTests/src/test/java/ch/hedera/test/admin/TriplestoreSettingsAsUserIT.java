/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - TriplestoreSettingsAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.triplestore.DatasetSettings;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.TriplestoreSettingsITService;

@Tag(HederaTestConstants.AS_MANAGER_TAG)
class TriplestoreSettingsAsUserIT extends TriplestoreSettingsIT {

  @Autowired
  public TriplestoreSettingsAsUserIT(Environment env, SudoRestClientTool restClientTool, FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService, GlobalBannerClientService globalBannerService,
          TriplestoreSettingsITService triplestoreSettingsITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService, triplestoreSettingsITService);
  }

  @Test
  void createDataset() {
    String datasetName = this.triplestoreSettingsITService.getTemporaryDatasetName("test dataset");
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.triplestoreSettingsITService.create(datasetName));
  }

  @Test
  void getDataset() {
    this.restClientTool.sudoAdmin();
    String datasetName = this.triplestoreSettingsITService.getTemporaryDatasetName("test dataset");
    DatasetSettings datasetInfo = this.triplestoreSettingsITService.create(datasetName);
    assertNotNull(datasetInfo);
    assertEquals(datasetName, datasetInfo.getResId());
    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.triplestoreSettingsITService.findOne(datasetName));
  }

  @Test
  void listDatasets() {
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.triplestoreSettingsITService.findAll());
  }
}
