/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - InstitutionIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.Person;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.PersonITService;

class InstitutionIT extends AbstractAdminIT {

  private PersonITService personITService;

  @Autowired
  public InstitutionIT(Environment env, SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          PersonITService personITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
    this.personITService = personITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    // Create an institution
    final Institution institution1 = new Institution();
    institution1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Institut Du Du "));
    institution1.setDescription("C'est très joli");
    institution1.setEmailSuffixes(Arrays.asList("test1.ch", "test2.ch"));
    final Institution institution2 = this.institutionService.create(institution1);

    // Test the creation
    assertEquals(institution1.getName(), institution2.getName());
    assertEquals(institution1.getDescription(), institution2.getDescription());
    assertTrue(institution2.getEmailSuffixes().contains("test1.ch"));
    assertTrue(institution2.getEmailSuffixes().contains("test2.ch"));
  }

  @Test
  void creationTestWithCorrectURL() throws MalformedURLException {
    this.creationWithURL("https://wwww.unige.ch");
  }

  @Test
  void creationTestWithMalformedURL() {
    assertThrows(MalformedURLException.class, () -> {
      this.creationWithURL("httppps://www.unige.ch");
    });
  }

  public void creationWithURL(String url) throws MalformedURLException {
    // Create an institution
    final Institution intstitution1 = new Institution();
    intstitution1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Institut von Ürl"));
    intstitution1.setDescription("ça c'est de l'insitut");
    try {
      intstitution1.setUrl(new URL(url));
    } catch (final MalformedURLException e) {
      throw e;
    }
    final Institution institution2 = this.institutionService.create(intstitution1);

    // Test the creation
    assertEquals(intstitution1.getName(), institution2.getName());
    assertEquals(intstitution1.getDescription(), institution2.getDescription());
    assertEquals(intstitution1.getUrl(), institution2.getUrl());
  }

  @Test
  void getTestWithRorId() {
    // Create an institution
    final String name = HederaTestConstants.getRandomNameWithTemporaryLabel("Institution with ROR");
    final String description = "C'est très joli";
    final String rorId = "04v1bf639";
    Institution institution1 = new Institution();
    institution1.setName(name);
    institution1.setDescription(description);
    institution1.setRorId(rorId);
    institution1 = this.institutionService.create(institution1);

    // Test the creation
    final Institution fetchedInstitution = this.institutionService.findOne(institution1.getResId());
    assertEquals(name, fetchedInstitution.getName());
    assertEquals(description, fetchedInstitution.getDescription());
    assertEquals(rorId, fetchedInstitution.getRorId());

    // Get Institution by ROR ID
    final Institution rorInstitution = this.institutionService.findByRorId(institution1.getRorId());
    assertEquals(name, rorInstitution.getName());
    assertEquals(description, rorInstitution.getDescription());
    assertEquals(rorId, rorInstitution.getRorId());

    /*
     * Create a new Institution with a ROR with prefix
     */
    final String name2 = HederaTestConstants.getRandomNameWithTemporaryLabel("Institution with full ROR");
    final String description2 = "C'est très vilain";
    final String shortRorId2 = "04tst0102";
    final String fullRorId2 = "https://ror.org/" + shortRorId2;
    Institution institution2 = new Institution();
    institution2.setName(name2);
    institution2.setDescription(description2);
    institution2.setRorId(fullRorId2);
    institution2 = this.institutionService.create(institution2);

    /*
     * Fetch it from server and test it has been created
     */
    final Institution fetchedInstitution2 = this.institutionService.findOne(institution2.getResId());
    assertNotNull(fetchedInstitution2);
    assertEquals(name2, fetchedInstitution2.getName());
    assertEquals(description2, fetchedInstitution2.getDescription());
    assertEquals(shortRorId2, fetchedInstitution2.getRorId());

    // Get funding agency by short ROR ID
    final Institution rorInstitution2 = this.institutionService.findByRorId(shortRorId2);
    assertNotNull(rorInstitution2);
    assertEquals(name2, rorInstitution2.getName());
    assertEquals(description2, rorInstitution2.getDescription());
    assertEquals(shortRorId2, rorInstitution2.getRorId());
  }

  @Test
  void personTest() {
    // Create an institution
    Institution institution = new Institution();
    institution.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Institut Du Du "));
    institution = this.institutionService.create(institution);

    // Create a person
    Person person = new Person();
    person.setFirstName(HederaTestConstants.getRandomNameWithTemporaryLabel("Jojo "));
    person.setLastName(HederaTestConstants.getRandomNameWithTemporaryLabel("Tutu "));
    person = this.personITService.create(person);

    // Add a person to the institution
    this.institutionService.addPerson(institution.getResId(), person.getResId());
    final List<Person> listPersons = this.institutionService.getPeople(institution.getResId());
    final String resIdPersonToSearch = person.getResId();
    final Optional<Person> optionalPerson = listPersons.stream().filter(p -> p.getResId().equals(resIdPersonToSearch)).findFirst();
    assertTrue(optionalPerson.isPresent());

    // Test that the person is added to the institution
    assertEquals(person.getResId(), optionalPerson.get().getResId());
    assertEquals(person.getFirstName(), optionalPerson.get().getFirstName());
    assertEquals(person.getLastName(), optionalPerson.get().getLastName());
  }

  @Test
  void unicityTest() {
    // Create an institution
    Institution institution1 = new Institution();
    institution1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Institut Du Du "));
    institution1.setDescription("C'est très joli");
    institution1 = this.institutionService.create(institution1);

    // Create a second institution with the same name
    Institution institution2 = new Institution();
    institution2.setName(institution1.getName());
    institution2.setDescription("Wow, such description");

    // Testing unicity of the name
    try {
      this.institutionService.create(institution2);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Unicity constraint not satisfied");
    }

  }

  @Test
  void updateTest() {
    // Create an institution
    Institution institution1 = new Institution();
    institution1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("A"));
    institution1.setDescription("A");
    institution1.setEmailSuffixes(Arrays.asList("test1.ch", "test2.ch"));
    institution1 = this.institutionService.create(institution1);

    // Update the institution
    institution1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("B"));
    institution1.setDescription("B");
    institution1.setEmailSuffixes(Arrays.asList("test1.ch", "test2.ch", "test3.ch"));
    this.institutionService.update(institution1.getResId(), institution1);
    final Institution institution2 = this.institutionService.findOne(institution1.getResId());

    // Test the update
    assertEquals(institution1.getName(), institution2.getName());
    assertEquals(institution1.getDescription(), institution2.getDescription());
    assertTrue(institution2.getEmailSuffixes().contains("test1.ch"));
    assertTrue(institution2.getEmailSuffixes().contains("test2.ch"));
    assertTrue(institution2.getEmailSuffixes().contains("test3.ch"));
    this.assertEqualsWithoutNanoSeconds(institution1.getCreationTime(), institution2.getCreationTime());
  }

  @Test
  void uploadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an institution
     */
    final Institution institution1 = this.createRemoteInstitution("Institution #LOGO");

    /*
     * Add logo
     */
    Institution fetchedInstitution = this.institutionService.uploadLogo(institution1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Institution refetchedInstitution = this.institutionService.findOne(fetchedInstitution.getResId());
    assertEquals(refetchedInstitution.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedInstitution.getResourceFile().getFileSize(), sizeLogo);
  }

  @Test
  void downloadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an institution
     */
    final Institution institution1 = this.createRemoteInstitution("Institution #LOGO");

    /*
     * Add logo
     */
    Institution institution2 = this.institutionService.uploadLogo(institution1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Institution refetchedInstitution = this.institutionService.findOne(institution2.getResId());
    assertEquals(refetchedInstitution.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedInstitution.getResourceFile().getFileSize(), sizeLogo);

    Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.tmp");
    this.institutionService.downloadLogo(refetchedInstitution.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedInstitution.getResourceFile().getFileSize());
  }

  @Test
  void deleteLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an institution
     */
    final Institution institution1 = this.createRemoteInstitution("Institution #LOGO");

    /*
     * Add logo
     */
    Institution fetchedInstitution = this.institutionService.uploadLogo(institution1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    Institution refetchedInstitution = this.institutionService.findOne(fetchedInstitution.getResId());
    assertEquals(refetchedInstitution.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedInstitution.getResourceFile().getFileSize(), sizeLogo);

    this.institutionService.deleteLogo(institution1.getResId());
    refetchedInstitution = this.institutionService.findOne(fetchedInstitution.getResId());
    assertNull(refetchedInstitution.getResourceFile());
  }

  @Override
  protected void deleteFixtures() {
    this.clearInstitutionsFixtures();
  }

  private Institution createRemoteInstitution(String description) {
    // Create a new Institution
    Institution institution = new Institution();
    institution.setName(HederaTestConstants.getRandomNameWithTemporaryLabel(description));
    institution = this.institutionService.create(institution);
    return institution;
  }
}
