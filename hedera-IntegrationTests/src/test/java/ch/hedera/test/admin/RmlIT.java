/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - RmlIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.humanities.Rml;
import ch.hedera.model.humanities.RmlFormat;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.test.HederaTestConstants;

class RmlIT extends AbstractAdminIT {

  private final RmlClientService rmlService;

  @Autowired
  public RmlIT(Environment env, SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          RmlClientService rmlService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
    this.rmlService = rmlService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void testCreate() {
    /*
     * Create a rml
     */
    final String rmlName = HederaTestConstants.getRandomNameWithTemporaryLabel("Rml");
    final String rmlDescription = HederaTestConstants.getRandomNameWithTemporaryLabel("Rml description");

    Rml rml = new Rml();
    rml.setName(rmlName);
    rml.setDescription(rmlDescription);
    rml.setVersion("1");
    rml.setFormat(RmlFormat.SQL);

    Rml rmlRemote = this.rmlService.createAndUploadRdfFile(rml, new ClassPathResource(HederaTestConstants.DEMO_RML_FILE_TO_UPLOAD));

    /*
     * Fetch it from server and test it has been created
     */
    final Rml rmlFetched = this.rmlService.findOne(rmlRemote.getResId());
    assertNotNull(rmlFetched.getResId());
    assertEquals(rmlName, rmlFetched.getName());
    assertEquals(rmlDescription, rmlFetched.getDescription());
  }

  @Test
  void testDelete() {
    final String rmlName = HederaTestConstants.getRandomNameWithTemporaryLabel("Rml");
    final String rmlDescription = HederaTestConstants.getRandomNameWithTemporaryLabel("Rml description");

    Rml rml = new Rml();
    rml.setName(rmlName);
    rml.setDescription(rmlDescription);
    rml.setVersion("1");
    rml.setFormat(RmlFormat.SQL);

    /*
     * Create a rml
     */
    Rml rmlRemote = this.rmlService.createAndUploadRdfFile(rml, new ClassPathResource(HederaTestConstants.DEMO_RML_FILE_TO_UPLOAD));

    /*
     * Fetch it from server and test it has been created
     */
    final Rml rmlFetched = this.rmlService.findOne(rmlRemote.getResId());
    assertNotNull(rmlFetched.getResId());
    assertEquals(rmlName, rmlFetched.getName());
    assertEquals(rmlDescription, rmlFetched.getDescription());

    /**
     * Delete it
     */
    this.rmlService.delete(rmlFetched.getResId());

    /*
     * Fetching again returns nothing
     */
    final String rmlId = rmlRemote.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.rmlService.findOne(rmlId));
  }

  @Test
  void testUpdate() {
    /*
     * Creates a rml
     */
    final String rmlName = HederaTestConstants.getRandomNameWithTemporaryLabel("Rml");
    final String rmlDescription = HederaTestConstants.getRandomNameWithTemporaryLabel("Rml description");
    final String rmlName2 = HederaTestConstants.getRandomNameWithTemporaryLabel("Rml");
    final String rmlDescription2 = HederaTestConstants.getRandomNameWithTemporaryLabel("Rml description");

    Rml rml = new Rml();
    rml.setName(rmlName);
    rml.setDescription(rmlDescription);
    rml.setVersion("1");
    rml.setFormat(RmlFormat.SQL);

    Rml rmlRemote = this.rmlService.createAndUploadRdfFile(rml, new ClassPathResource(HederaTestConstants.DEMO_RML_FILE_TO_UPLOAD));

    /*
     * Fetch it from server and test it has been created
     */
    final Rml rmlFetched = this.rmlService.findOne(rmlRemote.getResId());
    assertNotNull(rmlFetched.getResId());
    assertEquals(rmlName, rmlFetched.getName());
    assertEquals(rmlDescription, rmlFetched.getDescription());

    /*
     * Does the update
     */
    rmlFetched.setName(rmlName2);
    rmlFetched.setDescription(rmlDescription2);
    List<String> propertiesToUpdate = new ArrayList<>();
    propertiesToUpdate.add("name");
    propertiesToUpdate.add("description");
    this.rmlService.update(rmlFetched.getResId(), rmlFetched, propertiesToUpdate);

    this.rmlService.findOne(rmlFetched.getResId());
    assertNotNull(rmlFetched.getResId());
    assertEquals(rmlName2, rmlFetched.getName());
    assertEquals(rmlDescription2, rmlFetched.getDescription());
  }

  protected void clearRmlFixtures() {
    final List<Rml> rmls = this.rmlService.findAll();

    for (final Rml rml : rmls) {
      if (rml.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.rmlService.delete(rml.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearRmlFixtures();
  }

}
