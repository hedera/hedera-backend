/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ProjectAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.humanities.Rml;
import ch.hedera.model.humanities.RmlFormat;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.FundingAgency;
import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.License;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.rest.ResourceName;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.FundingAgencyITService;
import ch.hedera.test.service.InstitutionITService;
import ch.hedera.test.service.PersonITService;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.RmlITService;
import ch.hedera.test.service.TriplestoreSettingsITService;

@Tag(HederaTestConstants.AS_ADMIN_TAG)
class ProjectAsAdminIT extends AbstractProjectIT {

  protected final static String ACRONYM = "swiss-research-data";
  protected final static String NAME = "Swiss Research Data Preservation";

  protected final TriplestoreSettingsITService triplestoreSettingsITService;

  @Autowired
  public ProjectAsAdminIT(Environment env,
          SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          ProjectITService projectITService,
          InstitutionITService institutionITService,
          FundingAgencyITService fundingAgencyITService,
          PersonITService personITService,
          RmlITService rmlITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          TriplestoreSettingsITService TriplestoreSettingsITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService, projectITService, institutionITService,
            fundingAgencyITService, personITService, rmlITService, researchObjectTypeITService);
    this.triplestoreSettingsITService = TriplestoreSettingsITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void addAndRemoveFundingAgency() {
    /*
     * Get test project
     */
    final Project project = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-fa", NAME + " #FUNDING AGENCY");
    List<FundingAgency> fundingAgencies = this.projectITService.getFundingAgencies(project.getResId());
    final int initialState = fundingAgencies.size();

    /*
     * Create a new FundingAgency
     */
    final String fundingAgencyAcronym = HederaTestConstants.getRandomNameWithTemporaryLabel("FA");
    final String fundingAgencyName = HederaTestConstants.getRandomNameWithTemporaryLabel("My FundingAgency");
    FundingAgency new_agency = new FundingAgency();
    new_agency.setAcronym(fundingAgencyAcronym);
    new_agency.setName(fundingAgencyName);
    new_agency = this.fundingAgencyService.create(new_agency);

    /*
     * Add the FundingAgency to the project
     */
    this.projectITService.addFundingAgency(project.getResId(), new_agency.getResId());

    /*
     * Check it has been added
     */
    fundingAgencies = this.projectITService.getFundingAgencies(project.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(initialState + 1, fundingAgencies.size());
    final Optional<FundingAgency> optionalFA = fundingAgencies.stream().filter(fa -> fa.getName().equals(fundingAgencyName)).findFirst();
    assertTrue(optionalFA.isPresent());
    assertEquals(fundingAgencyName, optionalFA.get().getName());

    /*
     * Remove the FundingAgency
     */
    this.projectITService.removeFundingAgency(project.getResId(), new_agency.getResId());

    /*
     * Check it has been removed
     */
    fundingAgencies = this.projectITService.getFundingAgencies(project.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(initialState, fundingAgencies.size());

    /*
     * Create 3 FundingAgencies and link them to the project
     */
    final FundingAgency subFA1 = new FundingAgency();
    subFA1.setAcronym(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub FA 1"));
    subFA1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub FundingAgency 1"));

    final FundingAgency subFA2 = new FundingAgency();
    subFA2.setAcronym(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub FA 2"));
    subFA2.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub FundingAgency 2"));

    final FundingAgency subFA3 = new FundingAgency();
    subFA3.setAcronym(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub FA 3"));
    subFA3.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub FundingAgency 3"));

    final FundingAgency[] subAgencies = new FundingAgency[] { subFA1, subFA2, subFA3 };

    String agencyIdToDelete = null;

    int i = 0;
    for (FundingAgency subAgency : subAgencies) {
      subAgency = this.fundingAgencyService.create(subAgency);
      this.projectITService.addFundingAgency(project.getResId(), subAgency.getResId());

      if (i == 1) {
        agencyIdToDelete = subAgency.getResId();
      }

      i++;
    }

    /*
     * Check they are all linked correctly
     */
    fundingAgencies = this.projectITService.getFundingAgencies(project.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(initialState + 3, fundingAgencies.size());

    /*
     * Delete one specifically and check it is not in the list anymore
     */
    this.projectITService.removeFundingAgency(project.getResId(), agencyIdToDelete);
    fundingAgencies = this.projectITService.getFundingAgencies(project.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(initialState + 2, fundingAgencies.size());
    for (final FundingAgency fetched_agency : fundingAgencies) {
      assertNotEquals(agencyIdToDelete, fetched_agency.getResId());
    }

  }

  @Test
  void addAndRemoveInstitutionTest() {
    // Get test project
    final Project project = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-institution", NAME + " #INSTITUTION");
    List<Institution> list = this.projectITService.getInstitutions(project.getResId());
    final int initialState = list.size();

    // Create an institution
    Institution institution1 = new Institution();
    institution1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("institution "));
    institution1.setDescription("description" + ThreadLocalRandom.current().nextInt());
    institution1 = this.institutionService.create(institution1);

    // Add the institution to the project
    this.projectITService.addInstitution(project.getResId(), institution1.getResId());
    list = this.projectITService.getInstitutions(project.getResId());
    assertEquals(initialState + 1, list.size());
    final String institutionName = institution1.getName();
    final Optional<Institution> optionalInstitution = list.stream().filter(institution -> institution.getName().equals(institutionName))
            .findFirst();
    assertTrue(optionalInstitution.isPresent());

    // Test the institution
    assertEquals(institution1.getResId(), optionalInstitution.get().getResId());
    assertEquals(institution1.getName(), optionalInstitution.get().getName());
    assertEquals(institution1.getDescription(), optionalInstitution.get().getDescription());

    this.projectITService.removeInstitution(project.getResId(), institution1.getResId());
    list = this.projectITService.getInstitutions(project.getResId());
    assertEquals(initialState, list.size());
  }

  @Test
  void addAndRemoveResearchObjectTypeTest() {
    // Get test project
    final Project project = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-rot", NAME + " #ROT");
    List<ResearchObjectType> list = this.projectITService.getResearchObjectTypes(project.getResId());
    final int initialState = list.size();

    // Create an researchObject
    ResearchObjectType researchObjectType1 = this.researchObjectTypeITService.getResearchObjectTypeForTesting();
    researchObjectType1 = this.researchObjectTypeITService.createRemoteResearchObjectType(researchObjectType1);

    // Add the researchObject to the project
    this.projectITService.addResearchObjectType(project.getResId(), researchObjectType1.getResId());
    list = this.projectITService.getResearchObjectTypes(project.getResId());
    assertEquals(initialState + 1, list.size());
    final String researchObjectName = researchObjectType1.getName();
    final Optional<ResearchObjectType> optResearchObject = list.stream().filter(ro -> ro.getName().equals(researchObjectName)).findFirst();
    assertTrue(optResearchObject.isPresent());

    // Test the institution
    assertEquals(researchObjectType1.getResId(), optResearchObject.get().getResId());
    assertEquals(researchObjectType1.getName(), optResearchObject.get().getName());
    assertEquals(researchObjectType1.getDescription(), optResearchObject.get().getDescription());

    this.projectITService.removeResearchObjectType(project.getResId(), researchObjectType1.getResId());
    list = this.projectITService.getResearchObjectTypes(project.getResId());
    assertEquals(initialState, list.size());
  }

  @Test
  void addPersonToTheProjectTest() {
    final Project project = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-person", NAME + " #PERSON");
    final Person person = this.personITService.createRemoteTemporaryPerson();
    final int peopleBeforeAdding = this.projectITService.getPeople(project.getResId()).size();

    // Add the person to the project with default role
    this.projectITService.addPerson(project.getResId(), person.getResId());
    final List<Person> people = this.projectITService.getPeople(project.getResId());

    // If a person is linked to the basic auth account who created the organizational unit
    // then this person is automatically added to the organizational unit.
    // So one or two people should be present in the list
    assertTrue(people.size() == peopleBeforeAdding + 1 || people.size() == peopleBeforeAdding + 2);

    // Test added person on the project
    for (final Person person2 : people) {
      if (person2.getResId().equals(person.getResId())) {
        assertNotNull(person2);
        assertEquals(person.getFirstName(), person2.getFirstName());
        assertEquals(person.getLastName(), person2.getLastName());
        assertEquals(person.getOrcid(), person2.getOrcid());
      }
    }
  }

  @Test
  void addAndRemoveRmlTest() throws IOException {
    // Get test project
    final Project project = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-rml", NAME + " #RML");
    List<Rml> list = this.projectITService.getRmls(project.getResId());
    final int initialState = list.size();

    // Create a rml
    Rml newRml = this.rmlITService.createRemoteRml(HederaTestConstants.getRandomNameWithTemporaryLabel("Rml1"), RmlFormat.CSV, "1",
            "description",
            new ClassPathResource("empty.txt"));

    // Add the Rml to the project
    this.projectITService.addRml(project.getResId(), newRml.getResId());
    list = this.projectITService.getRmls(project.getResId());
    assertEquals(initialState + 1, list.size());
    final String rmlName = newRml.getName();
    final Optional<Rml> optionalRml = list.stream().filter(rml -> rml.getName().equals(rmlName)).findFirst();
    assertTrue(optionalRml.isPresent());

    // Test the rmlResearchOBJECT
    assertEquals(newRml.getResId(), optionalRml.get().getResId());
    assertEquals(newRml.getName(), optionalRml.get().getName());
    assertEquals(newRml.getFormat(), optionalRml.get().getFormat());

    this.projectITService.removeRml(project.getResId(), newRml.getResId());
    list = this.projectITService.getRmls(project.getResId());
    assertEquals(initialState, list.size());
  }

  @Test
  void changeRoleOfAPersonInsideAProjectTest() {
    final Project project = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-role-person", NAME + " #ROLE PERSON");
    final Person person = this.personITService.createRemoteTemporaryPerson();

    // Add the person to the project with default role (VISITOR)
    this.projectITService.addPerson(project.getResId(), person.getResId());
    this.projectITService.removePerson(project.getResId(), person.getResId());

    // Add the person to the project with other roles
    this.projectITService.addPersonRole(project.getResId(), person.getResId(), Role.MANAGER_ID);
    final Role role = this.projectITService.getPersonRole(project.getResId(), person.getResId());
    assertEquals(Role.MANAGER_ID, role.getResId());
  }

  @Test
  void closeTest() {
    Project project = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-close", NAME + " #CLOSE");

    assertTrue(project.isOpen());

    // Close
    final String resId = project.getResId();
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(StringTool.DATE_FORMAT);
    final String closingDate = LocalDate.now().plus(1, ChronoUnit.DAYS).format(formatter);

    final Project closedProject = this.projectITService.close(resId, closingDate);

    // Test close
    assertEquals(closedProject.getClosingDate().toString(), closingDate);
    assertTrue(closedProject.isOpen());

    // Update with incompatible dates
    closedProject.setOpeningDate(LocalDate.now().minusDays(5));
    closedProject.setClosingDate(LocalDate.now().minusDays(7));
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.projectITService.update(resId, closedProject));

    // Update with past dates
    closedProject.setOpeningDate(LocalDate.now().minusDays(5));
    closedProject.setClosingDate(LocalDate.now().minusDays(3));
    this.projectITService.update(resId, closedProject);
    project = this.projectITService.findOne(resId);
    assertFalse(project.isOpen());

    // Update with future dates
    closedProject.setOpeningDate(LocalDate.now().plusDays(5));
    closedProject.setClosingDate(LocalDate.now().plusDays(7));
    this.projectITService.update(resId, closedProject);
    project = this.projectITService.findOne(resId);
    assertFalse(project.isOpen());

    // Update with future dates
    closedProject.setOpeningDate(LocalDate.now().minusDays(1));
    closedProject.setClosingDate(LocalDate.now().plusDays(1));
    this.projectITService.update(resId, closedProject);
    project = this.projectITService.findOne(resId);
    assertTrue(project.isOpen());

    // Update with today dates
    closedProject.setOpeningDate(LocalDate.now());
    closedProject.setClosingDate(LocalDate.now());
    this.projectITService.update(resId, closedProject);
    project = this.projectITService.findOne(resId);
    assertTrue(project.isOpen());

    this.projectITService.delete(project.getResId());
  }

  @Test
  void creationTest() {
    // Create a project
    final Project project = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-create", NAME + " #CREATE");

    final Project project2 = this.projectITService.findOne(project.getResId());

    // Test the creation
    assertEquals(project.getName(), project2.getName());
    assertEquals(project.getDescription(), project2.getDescription());
    assertThat(project.getKeywords(), is(project2.getKeywords()));

    this.projectITService.delete(project.getResId());
  }

  @ParameterizedTest
  @ValueSource(strings = { "log-delivery-march-2020", "abcde-456", "12345678", "ok-1234abcde", "test--nnv--221" })
  void createProjectWithValidShortName(String shortName) {
    assertNotNull(this.projectITService.createRemoteTemporaryProject(shortName, shortName + " #VALID SHORT NAME"));
  }

  @ParameterizedTest
  @ValueSource(strings = { ResourceName.MANIFEST, ResourceName.COLLECTION, "yqmmsljlbrbjywbmseem8xher5ikyzciryumvahknpke745kukbz68ityf87bllaa5O",
          "$abcde123", "1234_abcde", "xn--abcd-jkjk-77", "sthree-1234-abcde", "sthree-configurator-abcd_jkjk-77", "dhhdu887--s3alias",
          "jhdj-ijdij87--ol-s3", "fuhufv//knkn", "DocExampleBucket", "doc-example-bucket-", "DONTWORK", "192.168.5.4" })
  void createProjectWithInvalidShortName(String shortName) {
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.projectITService.createRemoteTemporaryProject(shortName, shortName + " #INVALID SHORT NAME"));
  }

  @Test
  void creationWithOpeningClosingDatesTest() {
    // Create a project
    Project project1 = new Project();
    project1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("project"));
    project1.setShortName(ACRONYM + "-dates");
    this.projectITService.setRandomUrl(project1);
    project1.setDescription("Swiss Research Data Preservation #CREATE");
    project1.setResearchDataFileResearchObjectType(this.researchObjectTypeITService.getResearchObjectTypeForResearchDataFile());

    final LocalDate openingDate = LocalDate.now();
    final LocalDate closingDate = openingDate.plusDays(7);

    project1.setOpeningDate(openingDate);
    project1.setClosingDate(closingDate);

    project1 = this.projectITService.create(project1);
    final Project project2 = this.projectITService.findOne(project1.getResId());

    // Test the creation
    assertEquals(project1.getName(), project2.getName(), "Names must match");
    assertEquals(project1.getDescription(), project2.getDescription(), "Descriptions must match");
    this.assertEqualsWithoutNanoSeconds("Creation times must match", project1.getCreationTime(),
            project2.getCreationTime());
    assertEquals(openingDate, project2.getOpeningDate(), "Opening dates must match");
    assertEquals(closingDate, project2.getClosingDate(), "Closing dates must match");

    this.projectITService.delete(project1.getResId());

  }

  @Test
  void creationWithWrongOpeningClosingDatesTest() {

    // Create a project
    Project project1 = new Project();
    project1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("project"));
    this.projectITService.setRandomUrl(project1);
    project1.setDescription("Swiss Research Data Preservation #CREATE");

    final LocalDate closingDate = LocalDate.now();
    final LocalDate openingDate = closingDate.plusDays(7);
    project1.setOpeningDate(openingDate);
    project1.setClosingDate(closingDate);
    project1.setResearchDataFileResearchObjectType(this.researchObjectTypeITService.getResearchObjectTypeForResearchDataFile());

    boolean success = true;
    try {
      project1 = this.projectITService.create(project1);
    } catch (final Exception e) {
      success = false;
    }
    assertFalse(success);

    final String project1ResId = project1.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.projectITService.findOne(project1ResId));
  }

  @Test
  void defaultLicenseTest() {
    final String projectDefaultLicenseId = "CC-BY-NC-ND-4.0";
    final String projectDefaultLicenseTitle = "Creative Commons Attribution Non Commercial No Derivatives 4.0 International";

    // Test the presence of the system-wide default license on a newly created project
    Project project = this.projectITService.getOrCreatePermanentProjectForNoOne();
    final String projectId = project.getResId();

    // Test the setting of a project specific license
    License newDefaultLicense = new License();
    newDefaultLicense.setResId(projectDefaultLicenseId);
    project.setDefaultLicense(newDefaultLicense);
    this.projectITService.update(projectId, project);
    project = this.projectITService.findOne(projectId);
    assertEquals(projectDefaultLicenseId, project.getDefaultLicense().getResId());
    assertEquals(projectDefaultLicenseTitle, project.getDefaultLicense().getTitle());

    // Test that setting a garbage license title implies 500 internal error
    Project projectWithLicenseTitle = new Project();
    newDefaultLicense.setTitle("KKKKKKKKKK");
    projectWithLicenseTitle.setDefaultLicense(newDefaultLicense);
    assertThrows(HttpServerErrorException.InternalServerError.class,
            () -> this.projectITService.update(projectId, projectWithLicenseTitle, Arrays.asList("defaultLicense.title")));

    // Test that default license has not been updated
    project = this.projectITService.findOne(projectId);
    assertEquals(projectDefaultLicenseId, project.getDefaultLicense().getResId());
    assertEquals(projectDefaultLicenseTitle, project.getDefaultLicense().getTitle());

    // Try to reset the default license
    project.setDefaultLicense(null);
    this.projectITService.update(projectId, project);
    project = this.projectITService.findOne(projectId);
    assertNull(project.getDefaultLicense());

    // Try to add a non-existing license
    newDefaultLicense = new License();
    newDefaultLicense.setResId("KKKKKKKKKK");
    newDefaultLicense.setTitle("KKKKKKKKKKK");
    project.setDefaultLicense(newDefaultLicense);
    final Project projectUpdate = project;
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.projectITService.update(projectId, projectUpdate));
  }

  @Test
  void deleteTest() {
    // Create a new project
    Project project = this.projectITService.createRemoteTemporaryProject(ACRONYM, NAME);

    // Delete the project
    final String resId = project.getResId();
    this.projectITService.delete(resId);

    /*
     * Fetching again returns nothing
     */
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.projectITService.findOne(resId));
  }

  @Test
  void removePersonFromAProjectTest() {
    final Project project = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-rm-person", NAME + " #REMOVE PERSON");
    final Person person = this.personITService.createRemoteTemporaryPerson();

    // Add the person to the project with default role
    this.projectITService.addPerson(project.getResId(), person.getResId());
    List<Person> people = this.projectITService.getPeople(project.getResId());

    final int nbPepole = people.size();
    this.projectITService.removePerson(project.getResId(), person.getResId());
    people = this.projectITService.getPeople(project.getResId());
    assertEquals(nbPepole - 1, people.size());
  }

  @Test
  void unicityTest() {
    // Creating a new Organizational Unit
    Project project1 = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-unique", NAME + " #UNIQUE");
    project1 = this.projectITService.findOne(project1.getResId());

    // Creating a second Organizational unit with the same name as the first's
    Project project2 = new Project();
    this.projectITService.setRandomUrl(project2);
    project2.setDescription("Dufourian Research Data Preservation #CREATE");

    // Testing the unicity of name
    project2.setName(project1.getName());
    project2.setShortName(project1.getShortName() + "-bis");
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.projectITService.create(project2));

    // Testing the unicity of short name
    project2.setName(project1.getName() + "-bis");
    project2.setShortName(project1.getShortName());
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.projectITService.create(project2));
  }

  @Test
  void updateTest() {
    // Create a project
    final Project project1 = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-update", NAME + " #UPDATE");

    // Update the person
    final Project project2 = new Project();
    project2.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("project 2.0"));
    project2.setShortName("project-20");
    this.projectITService.setRandomUrl(project2);
    project2.setDescription("Swiss Research Data Preservation (v2) #UPDATE");
    project2.setResearchDataFileResearchObjectType(this.researchObjectTypeITService.getResearchObjectTypeForResearchDataFile());

    // Update the keywords
    project2.setKeywords(Arrays.asList("astrochemistry ", "immunohistochemistry ", "phytochemistry"));
    final Project project3 = this.projectITService.update(project1.getResId(), project2);

    // Test the update
    assertEquals(project1.getResId(), project3.getResId());
    assertEquals(project2.getDescription(), project3.getDescription());
    assertThat(project3.getKeywords(), is(project2.getKeywords()));

    // Update ShortName
    project3.setShortName("project-20-1");
    final Project project4 = this.projectITService.update(project1.getResId(), project3);
    assertEquals(project1.getResId(), project4.getResId());
    assertEquals(project3.getShortName(), project4.getShortName());

    // Update IIIF Manifest query
    project4.setIiifManifestSparqlQuery(this.projectITService.getIIIFManifestQuery());
    final Project project5 = this.projectITService.update(project1.getResId(), project4);
    assertEquals(project1.getResId(), project5.getResId());
    assertEquals(project3.getShortName(), project5.getShortName());
    assertEquals(this.projectITService.getIIIFManifestQuery(), project5.getIiifManifestSparqlQuery());

    // Update with invalid IIIF Manifest query
    project5.setIiifManifestSparqlQuery("SELECT ?o ?p ?v { ?o ?p }");
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.projectITService.update(project1.getResId(), project5));

    // Update with IIIF Manifest query and missing variables
    project5.setIiifManifestSparqlQuery("SELECT ?o ?p ?v { ?o ?p ?v}");
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.projectITService.update(project1.getResId(), project5));

  }

  @Test
  void uploadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create a project
     */
    final Project project1 = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-add-logo", NAME + " #LOGO ADD");

    /*
     * Add logo
     */
    Project project2 = this.projectITService.uploadLogo(project1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    Project project3 = this.projectITService.findOne(project2.getResId());
    assertEquals(project3.getResourceFile().getFileName(), logoName);
    assertEquals(project3.getResourceFile().getFileSize(), sizeLogo);

    /*
     * Update org unit again
     */
    String newName = HederaTestConstants.getRandomNameWithTemporaryLabel("project");
    project3.setName(newName);
    project3 = this.projectITService.update(project3.getResId(), project3);
    assertEquals(project3.getName(), newName);

  }

  @Test
  void downloadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create a project
     */
    final Project project1 = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-dl-logo", NAME + " #LOGO DOWNLOAD");

    /*
     * Add logo
     */
    Project project2 = this.projectITService.uploadLogo(project1.getResId(), logoToUpload);
    /*
     * Check logo has been uploaded
     */
    final Project refetchedProject = this.projectITService.findOne(project2.getResId());
    assertEquals(refetchedProject.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedProject.getResourceFile().getFileSize(), sizeLogo);

    Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.tmp");
    this.projectITService.downloadLogo(refetchedProject.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedProject.getResourceFile().getFileSize());
  }

  @Test
  void deleteLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create a project
     */
    final Project project1 = this.projectITService.createRemoteTemporaryProject(ACRONYM + "-del-logo", NAME + " #LOGO DELETE");

    /*
     * Add logo
     */
    Project project2 = this.projectITService.uploadLogo(project1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    Project project3 = this.projectITService.findOne(project2.getResId());
    assertEquals(project3.getResourceFile().getFileName(), logoName);
    assertEquals(project3.getResourceFile().getFileSize(), sizeLogo);

    this.projectITService.deleteLogo(project1.getResId());
    project3 = this.projectITService.findOne(project2.getResId());
    assertNull(project3.getResourceFile());
  }

  /*************************************************/

  @Override
  protected void deleteFixtures() {
    this.triplestoreSettingsITService.cleanTestData(); // run first in order to get the temporary project before they got deleted
    this.projectITService.cleanTestData();
    this.fundingAgencyITService.cleanTestData();
    this.institutionITService.cleanTestData();
    this.personITService.cleanTestData();
    this.researchObjectTypeITService.cleanTestData();
    this.rmlITService.cleanTestData();
  }
}
