/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AbstractAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import java.util.List;

import org.springframework.core.env.Environment;

import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.security.User;
import ch.hedera.model.settings.FundingAgency;
import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.License;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.AbstractIT;
import ch.hedera.test.HederaTestConstants;

public abstract class AbstractAdminIT extends AbstractIT {
  
  FundingAgencyClientService fundingAgencyService;

  InstitutionClientService institutionService;

  GlobalBannerClientService globalBannerService;

  public AbstractAdminIT(Environment env,
          SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService) {
    super(env, restClientTool);
    this.fundingAgencyService = fundingAgencyService;
    this.institutionService = institutionService;
    this.globalBannerService = globalBannerService;
  }

  /***********************************/

  protected void clearFundingAgencyFixtures() {
    final List<FundingAgency> agencies = this.fundingAgencyService.findAll();

    for (final FundingAgency a : agencies) {

      if (a.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {

        /*
         * Remove eventual links with projects first
         */
        final List<Project> child_units = this.fundingAgencyService.getProjects(a.getResId());
        if (child_units != null) {
          for (final Project u : child_units) {
            this.fundingAgencyService.removeProject(a.getResId(), u.getResId());
          }
        }

        this.fundingAgencyService.delete(a.getResId());
      }
    }
  }

  protected void clearInstitutionsFixtures() {

    final List<Institution> institutions = this.institutionService.findAll();

    for (final Institution institution : institutions) {

      if (institution.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {

        /*
         * Remove eventual links with people first
         */
        final List<Person> people = this.institutionService.getPeople(institution.getResId());
        if (people != null && !people.isEmpty()) {
          for (final Person person : people) {
            this.institutionService.removePerson(institution.getResId(), person.getResId());
          }
        }

        /*
         * Remove eventual links with organizational units first
         */
        final List<Project> units = this.institutionService.getProjects(institution.getResId());
        if (units != null && !units.isEmpty()) {
          for (final Project unit : units) {
            this.institutionService.removeProject(institution.getResId(), unit.getResId());
          }
        }

        this.institutionService.delete(institution.getResId());
      }
    }
  }

  protected void clearLicenceFixtures() {
    final List<License> licenses = this.licenseService.findAll();

    for (final License license : licenses) {
      if (license.getTitle().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.licenseService.delete(license.getResId());
      }
    }
  }

  protected void clearIndexFieldAliasFixtures() {
    final List<IndexFieldAlias> indexFieldAliases = this.indexFieldAliasService.findAll();

    for (final IndexFieldAlias indexFieldAlias : indexFieldAliases) {
      if (indexFieldAlias.getIndexName().equals(HederaTestConstants.TEST_RES_ID)) {
        this.indexFieldAliasService.delete(indexFieldAlias.getResId());
      }
    }
  }

  /***********************************/

  protected void clearUserFixtures() {
    final List<User> users = this.userService.findAll();

    for (final User u : users) {

      if (u.getFirstName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {

        this.userService.delete(u.getResId());
      }
    }
  }

  protected void clearGlobalBannerFixtures() {
    this.globalBannerService.findAll().stream()
            .filter(globalBanner -> globalBanner.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(globalBanner -> this.globalBannerService.delete(globalBanner.getResId()));
  }
}
