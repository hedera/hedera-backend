/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - IIIFCollectionSettingsIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.settings.IIIFCollectionSettings;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.IIIFCollectionSettingsClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.ProjectITService;

class IIIFCollectionSettingsIT extends AbstractAdminIT {

  private static final String INVALID_MANIFEST_QUERY = "SELECT ?s { ?s a <researchObjectType.rdfType> . }";
  private static final String VALID_MANIFEST_QUERY = "SELECT ?resource_subject ?img_subject ?img_iiif_url ?img_height ?img_width ?label { ?resource_subject a <researchObjectType.rdfType> . }";

  private final IIIFCollectionSettingsClientService iiifCollectionSettingsClientService;
  private final ProjectITService projectITService;

  @Autowired
  public IIIFCollectionSettingsIT(Environment env, SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          IIIFCollectionSettingsClientService iiifCollectionSettingsClientService, ProjectITService projectITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
    this.iiifCollectionSettingsClientService = iiifCollectionSettingsClientService;
    this.projectITService = projectITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void testCreate() {
    final Project project = this.projectITService.createRemoteTemporaryProject("iif-temp-project1", "");
    final IIIFCollectionSettings iiifCollectionSettings = new IIIFCollectionSettings();
    iiifCollectionSettings.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("IIIF_COLLECTION"));
    iiifCollectionSettings.setIiifCollectionSparqlQuery(VALID_MANIFEST_QUERY);
    iiifCollectionSettings.setProject(project);
    IIIFCollectionSettings iiifCollectionSettings1 = this.iiifCollectionSettingsClientService.create(iiifCollectionSettings);
    assertNotNull(iiifCollectionSettings1);
    assertEquals(VALID_MANIFEST_QUERY, iiifCollectionSettings1.getIiifCollectionSparqlQuery());
  }

  @Test
  void testUpdate() {
    final Project project = this.projectITService.createRemoteTemporaryProject("iif-temp-project2", "");
    final IIIFCollectionSettings iiifCollectionSettings = new IIIFCollectionSettings();
    iiifCollectionSettings.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("IIIF_COLLECTION"));
    iiifCollectionSettings.setIiifCollectionSparqlQuery(VALID_MANIFEST_QUERY);
    iiifCollectionSettings.setProject(project);
    IIIFCollectionSettings remote = this.iiifCollectionSettingsClientService.create(iiifCollectionSettings);

    final IIIFCollectionSettings founded = this.iiifCollectionSettingsClientService.findOne(remote.getResId());

    /*
     * Does the update
     */
    final String name2 = HederaTestConstants.getRandomNameWithTemporaryLabel("IIIF_COLLECTION");
    final String sparqlQuery = "SELECT ?resource_subject ?img_subject ?img_iiif_url ?img_height ?img_width ?label WHERE { <http://example.org/book/book1> ?title ?img_subject . }";
    founded.setName(name2);
    founded.setIiifCollectionSparqlQuery(sparqlQuery);
    List<String> propertiesToUpdate = new ArrayList<>();
    propertiesToUpdate.add("name");
    propertiesToUpdate.add("iiifCollectionSparqlQuery");
    this.iiifCollectionSettingsClientService.update(founded.getResId(), founded, propertiesToUpdate);

    final IIIFCollectionSettings fetched = this.iiifCollectionSettingsClientService.findOne(founded.getResId());

    assertEquals(name2, fetched.getName());
    assertEquals(sparqlQuery, fetched.getIiifCollectionSparqlQuery());

    // Invalid query
    final String sparqlQuery1 = "SELECT ?resource_subject ?img_subject ?img_height ?img_width ?label WHERE { <http://example.org/book/book1> ?title ?img_subject . }";
    founded.setIiifCollectionSparqlQuery(sparqlQuery1);
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.iiifCollectionSettingsClientService.update(founded.getResId(), founded, propertiesToUpdate));
  }

  @Test
  void testCreateWithInvalideQuery() {
    final Project project = this.projectITService.createRemoteTemporaryProject("iif-temp-project3", "");
    final IIIFCollectionSettings iiifCollectionSettings = new IIIFCollectionSettings();
    iiifCollectionSettings.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("IIIF_COLLECTION"));
    iiifCollectionSettings.setIiifCollectionSparqlQuery(INVALID_MANIFEST_QUERY);
    iiifCollectionSettings.setProject(project);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.iiifCollectionSettingsClientService.create(iiifCollectionSettings));
  }

  @Test
  void testCreateWithProject() {
    final Project project = this.projectITService.getOrCreatePermanentProjectForNoOne();
    final IIIFCollectionSettings iiifCollectionSettings = new IIIFCollectionSettings();
    iiifCollectionSettings.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("IIIF_COLLECTION"));
    iiifCollectionSettings.setIiifCollectionSparqlQuery(VALID_MANIFEST_QUERY);
    iiifCollectionSettings.setProject(project);

    IIIFCollectionSettings iiifCollectionSettings1 = this.iiifCollectionSettingsClientService.create(iiifCollectionSettings);
    assertNotNull(iiifCollectionSettings1);
    assertEquals(VALID_MANIFEST_QUERY, iiifCollectionSettings1.getIiifCollectionSparqlQuery());
    assertEquals(project.getResId(), iiifCollectionSettings1.getProject().getResId());
    assertEquals(project.getProjectId(), iiifCollectionSettings1.getProject().getProjectId());
  }

  protected void clearIIIFCollectionFixtures() {
    final List<IIIFCollectionSettings> collections = this.iiifCollectionSettingsClientService.findAll();

    for (final IIIFCollectionSettings iiifCollectionSettings : collections) {
      if (iiifCollectionSettings.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.iiifCollectionSettingsClientService.delete(iiifCollectionSettings.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearIIIFCollectionFixtures();
    this.projectITService.cleanTestData();
  }

}
