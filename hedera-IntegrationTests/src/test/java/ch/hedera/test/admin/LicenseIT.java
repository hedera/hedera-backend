/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - LicenseIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.net.URL;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.settings.License;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;

class LicenseIT extends AbstractAdminIT {

  private static final String ALL_IMPORT_FILE_NAME = "import/all.json";
  private static final String CKAN_IMPORT_FILE_NAME = "import/ckan.json";

  private static final String HEDERA_IMPORT_FILE_NAME = "import/hedera.json";

  @Autowired
  public LicenseIT(Environment env, SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    // Create a license
    License expectedLicense = new License();
    expectedLicense.setTitle(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL + "GNU Free Documentation License ");
    final String gnuLicense = "GNU" + ThreadLocalRandom.current().nextInt();
    expectedLicense.setOpenLicenseId(gnuLicense);
    expectedLicense = this.licenseService.create(expectedLicense);
    final License actualLicense = this.licenseService.findOne(expectedLicense.getResId());

    // Test the creation
    assertNotNull(expectedLicense.getResId());
    this.assertsLicense(expectedLicense, actualLicense);

    // Test unique openLicenseId
    final License sameLicense = new License();
    sameLicense.setTitle(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL + "GNU Free Documentation License");
    sameLicense.setOpenLicenseId(gnuLicense);
    try {
      this.licenseService.create(sameLicense);
      fail("Should raise exception: Unique constraint openLicenseId");
    } catch (final Exception e) {
      assertEquals(e.getClass(), HttpClientErrorException.BadRequest.class);
    }
  }

  @Test
  void getTestWithSpdxId() {
    // Create a license
    License expectedLicense = new License();
    expectedLicense.setTitle(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL + "GNU Free Documentation License ");
    final String gnuLicense = "GNU" + ThreadLocalRandom.current().nextInt();
    expectedLicense.setOpenLicenseId(gnuLicense);
    expectedLicense = this.licenseService.create(expectedLicense);
    final License actualLicense = this.licenseService.findOne(expectedLicense.getResId());

    // Test the creation
    assertNotNull(expectedLicense.getResId());
    this.assertsLicense(expectedLicense, actualLicense);

    // Get License by SPDX ID
    License spdxLicense = this.licenseService.findBySpdxId(expectedLicense.getOpenLicenseId());
    // Test the get
    this.assertsLicense(expectedLicense, spdxLicense);
  }

  @Test
  void deleteTest() {
    // Create a new licence
    License expectedLicense = new License();
    expectedLicense.setTitle(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL + "License  " + ThreadLocalRandom.current().nextInt());
    expectedLicense.setOpenLicenseId("License  " + ThreadLocalRandom.current().nextInt());
    expectedLicense = this.licenseService.create(expectedLicense);

    // Delete
    final String resId = expectedLicense.getResId();
    this.licenseService.delete(resId);
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.licenseService.findOne(resId));
  }

  @Test
  void findAllTest() {
    // Create a new licence
    final License expectedLicense = new License();
    expectedLicense.setTitle(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL + " License  " + ThreadLocalRandom.current().nextInt());
    expectedLicense.setOpenLicenseId("License  " + ThreadLocalRandom.current().nextInt());

    this.licenseService.create(expectedLicense);

    // Find all
    final List<License> actualLicenses = this.licenseService.findAll();

    // Test at least
    assertFalse(actualLicenses.isEmpty());
    assertTrue(actualLicenses.size() >= 1);
  }

  @Test
  void importsLicenseTest() {
    // Test import as file hedera format
    String expectedPayload = new JSONObject().put("imported", 15).toString();
    String actualPayload = this.licenseService.importLicenseFile(HEDERA_IMPORT_FILE_NAME).getBody();
    assertEquals(expectedPayload, actualPayload);
  }

  @Test
  void updateTest() throws Exception {
    // Create a licence
    License expectedLicense = new License();
    expectedLicense.setTitle(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL + " A" + ThreadLocalRandom.current().nextInt());
    expectedLicense.setOpenLicenseId("A" + ThreadLocalRandom.current().nextInt());
    expectedLicense = this.licenseService.create(expectedLicense);

    // Update the licence
    expectedLicense.setTitle(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL + "B" + ThreadLocalRandom.current().nextInt());
    final String openLicenseId = "B" + ThreadLocalRandom.current().nextInt();
    expectedLicense.setOpenLicenseId(openLicenseId);
    expectedLicense.setDomainContent(true);
    expectedLicense.setDomainData(true);
    expectedLicense.setDomainSoftware(true);
    expectedLicense.setFamily("a family");
    expectedLicense.setIsGeneric(true);
    expectedLicense.setMaintainer("the GNU");
    expectedLicense.setOdConformance(License.ConformanceStatus.APPROVED);
    expectedLicense.setOsdConformance(License.ConformanceStatus.APPROVED);
    expectedLicense.setStatus(License.LicenseStatus.ACTIVE);
    expectedLicense.setUrl(new URL("http://www.unige.ch"));
    final License actualLicense = this.licenseService.update(expectedLicense.getResId(), expectedLicense);

    // Test the update
    assertNotNull(actualLicense);
    this.assertsLicense(expectedLicense, actualLicense);

    // Test unique openLicenseId
    License sameLicense = new License();
    sameLicense.setTitle(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL + " license with same id");
    sameLicense.setOpenLicenseId("C" + ThreadLocalRandom.current().nextInt());
    sameLicense = this.licenseService.create(sameLicense);
    sameLicense.setOpenLicenseId(openLicenseId);
    try {
      this.licenseService.update(sameLicense.getResId(), sameLicense);
      fail("Should raise exception: Unique constraint openLicenseId");
    } catch (final Exception e) {
      assertEquals(e.getClass(), HttpClientErrorException.BadRequest.class);
    }
  }

  @Override
  protected void clearLicenceFixtures() {
    final List<License> licenses = this.licenseService.findAll();

    for (final License license : licenses) {
      if (license.getTitle().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.licenseService.delete(license.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearLicenceFixtures();
  }

  private void assertsLicense(License expectedLicense, License actualLicense) {
    assertEquals(expectedLicense.getTitle(), actualLicense.getTitle());
    this.assertEqualsWithoutNanoSeconds(expectedLicense.getCreationTime(), actualLicense.getCreationTime());
    assertEquals(expectedLicense.getOpenLicenseId(), actualLicense.getOpenLicenseId());
    assertEquals(expectedLicense.getDomainContent(), actualLicense.getDomainContent());
    assertEquals(expectedLicense.getDomainData(), actualLicense.getDomainData());
    assertEquals(expectedLicense.getDomainSoftware(), actualLicense.getDomainSoftware());
    assertEquals(expectedLicense.getFamily(), actualLicense.getFamily());
    assertEquals(expectedLicense.getIsGeneric(), actualLicense.getIsGeneric());
    assertEquals(expectedLicense.getMaintainer(), actualLicense.getMaintainer());
    assertEquals(expectedLicense.getOdConformance(), actualLicense.getOsdConformance());
    assertEquals(expectedLicense.getOsdConformance(), actualLicense.getOsdConformance());
    assertEquals(expectedLicense.getStatus(), actualLicense.getStatus());
    assertEquals(expectedLicense.getUrl(), actualLicense.getUrl());
    assertEquals(expectedLicense.getOpenLicenseId() + " (" + expectedLicense.getTitle() + ")", actualLicense.getDescription());
  }

}
