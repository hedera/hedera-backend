/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - UserAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static ch.hedera.test.HederaTestConstants.THIRD_PARTY_USER_ID;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.security.User;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.PersonITService;

@Tag(HederaTestConstants.AS_MANAGER_TAG)
class UserAsUserIT extends AbstractAdminIT {

  private final PersonITService personITService;

  @Autowired
  public UserAsUserIT(Environment env, SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService, PersonITService personITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
    this.personITService = personITService;
  }

  @Test
  void userCannotCreateUser() {
    User user = new User();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.userService.create(user));
  }

  @Test
  void userCannotUpdateUser() {
    User user = this.personITService.getUserWithRole(AuthApplicationRole.USER);
    user.setLastName("Grogou");
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.userService.update(user.getResId(), user));
  }

  @Test
  void userCannotDeleteUser() {
    User user = this.personITService.getUserWithRole(AuthApplicationRole.USER);
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.userService.delete(user.getResId()));
  }

  @Test
  void shouldNotSeeSomeAttributeOfOtherUser() {
    User user = this.userService.findAll().stream().filter(u -> u.getFirstName().contains(THIRD_PARTY_USER_ID + "_firstName"))
            .findFirst().orElseThrow();
    assertNull(user.getExternalUid());
    assertNull(user.getLastLoginIpAddress());
    assertNull(user.getLastLoginTime());
    assertNull(user.getApplicationRole());
  }

  @Test
  void shouldSeeSomeAttributeOfMyself() {
    this.restClientTool.sudoThirdPartyUser();
    User user1 = this.userService.getAuthenticatedUser();
    assertNotNull(user1.getExternalUid());
    assertNotNull(user1.getLastLoginIpAddress());
    assertNotNull(user1.getLastLoginTime());
    assertNotNull(user1.getApplicationRole());

    User user2 = this.userService.findAll().stream().filter(u -> u.getFirstName().contains(THIRD_PARTY_USER_ID + "_firstName"))
            .findFirst().orElseThrow();
    assertNotNull(user2.getExternalUid());
    assertNotNull(user2.getLastLoginIpAddress());
    assertNotNull(user2.getLastLoginTime());
    assertNotNull(user2.getApplicationRole());
    this.restClientTool.exitSudo();
  }

  @Override
  protected void deleteFixtures() {
    // Do nothing
  }
}
