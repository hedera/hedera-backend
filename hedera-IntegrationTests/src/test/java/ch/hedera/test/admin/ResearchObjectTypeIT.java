/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ResearchObjectTypeIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.service.admin.ResearchObjectTypeClientService;
import ch.hedera.test.service.ResearchObjectTypeITService;

class ResearchObjectTypeIT extends AbstractAdminIT {
  private final ResearchObjectTypeClientService researchObjectTypeClientService;
  private final ResearchObjectTypeITService researchObjectTypeITService;

  @Autowired
  public ResearchObjectTypeIT(Environment env, SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          ResearchObjectTypeClientService researchObjectTypeClientService,
          ResearchObjectTypeITService researchObjectTypeITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
    this.researchObjectTypeClientService = researchObjectTypeClientService;
    this.researchObjectTypeITService = researchObjectTypeITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    // Create a research object
    final ResearchObjectType researchObjectType1 = this.researchObjectTypeITService.getResearchObjectTypeForTesting();
    final ResearchObjectType createdResearchObjectType = this.researchObjectTypeClientService.create(researchObjectType1);

    // Test the creation
    assertEquals(researchObjectType1.getName(), createdResearchObjectType.getName());
    assertEquals(researchObjectType1.getDescription(), createdResearchObjectType.getDescription());
    assertEquals(researchObjectType1.getSparqlListQuery(), createdResearchObjectType.getSparqlListQuery());
    assertEquals(researchObjectType1.getSparqlDetailQuery(), createdResearchObjectType.getSparqlDetailQuery());
  }

  @Test
  void creationWithoutSparqlDetailTest() {
    // Create with empty SPARQL query for detail
    final ResearchObjectType researchObjectType2 = this.researchObjectTypeITService.getResearchObjectTypeForTesting();
    researchObjectType2.setSparqlDetailQuery(null);
    final ResearchObjectType createdResearchObjectType2 = this.researchObjectTypeClientService.create(researchObjectType2);
    assertEquals(researchObjectType2.getName(), createdResearchObjectType2.getName());
    assertEquals(researchObjectType2.getDescription(), createdResearchObjectType2.getDescription());
    assertEquals(researchObjectType2.getSparqlListQuery(), createdResearchObjectType2.getSparqlListQuery());
    assertNull(createdResearchObjectType2.getSparqlDetailQuery());
  }

  @Test
  void creationWithoutSparqlListTest() {
    // Create with empty SPARQL query for detail
    final ResearchObjectType researchObjectType2 = this.researchObjectTypeITService.getResearchObjectTypeForTesting();
    researchObjectType2.setSparqlListQuery(null);
    final ResearchObjectType createdResearchObjectType2 = this.researchObjectTypeClientService.create(researchObjectType2);
    assertEquals(researchObjectType2.getName(), createdResearchObjectType2.getName());
    assertEquals(researchObjectType2.getDescription(), createdResearchObjectType2.getDescription());
    assertEquals(researchObjectType2.getSparqlDetailQuery(), createdResearchObjectType2.getSparqlDetailQuery());
    assertNull(createdResearchObjectType2.getSparqlListQuery());
  }

  @Test
  void creationWithInvalidSparqlTest() {
    // Try to create with invalid SPARQL query for list
    final ResearchObjectType researchObjectType4 = this.researchObjectTypeITService
            .getResearchObjectTypeForTesting();
    researchObjectType4.setSparqlListQuery(researchObjectType4.getSparqlListQuery() + "INVALID SPARQL QUERY");
    HttpClientErrorException.BadRequest e4 = assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.researchObjectTypeClientService.create(researchObjectType4));
    this.assertFieldContainsValidationErrorMessageSubstring(e4, "sparqlListQuery", "The SPARQL query is invalid:");

    // Try to create with valid SPARQL query for detail, but without ?subject variable
    final ResearchObjectType researchObjectType5 = this.researchObjectTypeITService
            .getResearchObjectTypeForTesting();
    researchObjectType5.setSparqlDetailQuery(researchObjectType5.getSparqlDetailQuery().replace("?subject", "?other"));
    HttpClientErrorException.BadRequest e5 = assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.researchObjectTypeClientService.create(researchObjectType5));
    this.assertFieldContainsValidationErrorMessage(e5, "sparqlDetailQuery", "The SPARQL query must contain a '?subject' variable");

    // Try to create with invalid SPARQL query for detail
    final ResearchObjectType researchObjectType6 = this.researchObjectTypeITService
            .getResearchObjectTypeForTesting();
    researchObjectType6.setSparqlListQuery(researchObjectType6.getSparqlListQuery() + "INVALID SPARQL QUERY");
    HttpClientErrorException.BadRequest e6 = assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.researchObjectTypeClientService.create(researchObjectType6));
    this.assertFieldContainsValidationErrorMessageSubstring(e6, "sparqlListQuery", "The SPARQL query is invalid:");
  }

  @Test
  void deleteTest() {
    // Create a new research object
    ResearchObjectType rd1 = this.researchObjectTypeITService.getResearchObjectTypeForTesting();
    rd1 = this.researchObjectTypeClientService.create(rd1);

    // Delete
    final String resId = rd1.getResId();
    this.researchObjectTypeClientService.delete(resId);
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.researchObjectTypeClientService.findOne(resId));
  }

  @Override
  protected void deleteFixtures() {
    this.researchObjectTypeITService.cleanTestData();
  }

}
