/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - OntologyIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.service.admin.OntologyClientService;
import ch.hedera.test.HederaTestConstants;

class OntologyIT extends AbstractAdminIT {

  protected OntologyClientService ontologyService;

  @Autowired
  public OntologyIT(Environment env, SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          OntologyClientService ontologyService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
    this.ontologyService = ontologyService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void createwithValidFileTest() throws MalformedURLException, URISyntaxException {

    final String ontologyName = HederaTestConstants.getRandomNameWithTemporaryLabel("ontology");

    /*
     * Create a new Ontology
     */
    Ontology ontology = this.createLocalOntology(ontologyName, "1", RdfFormat.RDF_XML);

    ontology = this.ontologyService.createAndUploadOntologyFile(ontology, new ClassPathResource(HederaTestConstants.ONTOLOGY_FILE_TO_UPLOAD));

    /*
     * Fetch it from server and test it has been created
     */
    final Ontology ontologyFetched = this.ontologyService.findOne(ontology.getResId());
    assertNotNull(ontologyFetched.getResId());
    assertEquals(ontologyName, ontologyFetched.getName());
    assertNotNull(ontologyFetched.getOntologyFile());
  }

  @Test
  void createwithNotValidFileTest() throws MalformedURLException, URISyntaxException {

    final String ontologyName = HederaTestConstants.getRandomNameWithTemporaryLabel("ontology");

    /*
     * Create a new Ontology
     */
    Ontology ontology = this.createLocalOntology(ontologyName, "1", RdfFormat.RDF_XML);

    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.ontologyService.createAndUploadOntologyFile(ontology,
            new ClassPathResource(HederaTestConstants.ONTOLOGY_FILE_NOT_VALID_TO_UPLOAD)));
  }

  @Test
  void createTest() throws MalformedURLException, URISyntaxException {

    final String ontologyName = HederaTestConstants.getRandomNameWithTemporaryLabel("ontology");

    /*
     * Create a new Ontology
     */
    Ontology ontology = this.createLocalOntology(ontologyName, "1", RdfFormat.RDF_XML);
    ontology = this.ontologyService.create(ontology);

    /*
     * Fetch it from server and test it has been created
     */
    final Ontology ontologyFetched = this.ontologyService.findOne(ontology.getResId());
    assertNotNull(ontologyFetched.getResId());
    assertEquals(ontologyName, ontologyFetched.getName());
  }

  @Test
  void testDelete() throws MalformedURLException, URISyntaxException {

    final String ontologyName = HederaTestConstants.getRandomNameWithTemporaryLabel("ontology");

    /*
     * Create a new Ontology
     */
    Ontology ontology = this.createLocalOntology(ontologyName, "1", RdfFormat.RDF_XML);
    ontology = this.ontologyService.create(ontology);

    /*
     * Fetch it from server and test it has been created
     */
    final Ontology ontologyFetched = this.ontologyService.findOne(ontology.getResId());
    assertNotNull(ontologyFetched.getResId());

    /*
     * Delete it
     */
    this.ontologyService.delete(ontologyFetched.getResId());

    /*
     * Fetching again returns nothing
     */
    final String OntologyId = ontology.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.ontologyService.findOne(OntologyId));
  }

  @Test
  void testUpdate() throws MalformedURLException, URISyntaxException {

    final String ontologyName = HederaTestConstants.getRandomNameWithTemporaryLabel("ontology");
    final String ontologyName2 = HederaTestConstants.getRandomNameWithTemporaryLabel("ontology2");

    /*
     * Create a new Ontology
     */
    Ontology ontology = this.createLocalOntology(ontologyName, "1", RdfFormat.RDF_XML);
    ontology = this.ontologyService.create(ontology);

    /*
     * Fetch it from server and test it has been created
     */
    final Ontology ontologyFetched = this.ontologyService.findOne(ontology.getResId());
    assertNotNull(ontologyFetched.getResId());

    /*
     * Does the update
     */
    ontologyFetched.setName(ontologyName2);
    ontologyFetched.setVersion("2");
    this.ontologyService.update(ontologyFetched.getResId(), ontologyFetched);

    /*
     * Checks update succeeded
     */
    final Ontology ontologyReFetched = this.ontologyService.findOne(ontologyFetched.getResId());
    assertEquals(ontologyReFetched.getName(), ontologyName2, "names are equal");
    assertEquals("2", ontologyReFetched.getVersion());
  }

  @Test
  void testNameFormatAndVersion() throws MalformedURLException, URISyntaxException {
    final String ontologyName = HederaTestConstants.getRandomNameWithTemporaryLabel("secondOntology");
    final String version = "1.0";
    final RdfFormat format = RdfFormat.RDF_XML;

    /*
     * Create a new Ontology
     */
    Ontology ontology = this.createLocalOntology(ontologyName, version, format);
    ontology = this.ontologyService.create(ontology);

    /*
     * Fetch it from server and test it has been created
     */
    final Ontology ontologyFetched = this.ontologyService.findOne(ontology.getResId());
    assertNotNull(ontologyFetched.getResId());

    /*
     * Create a new Ontology with same values
     */
    Ontology secondOntology = this.createLocalOntology(ontologyName, version, format);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.ontologyService.create(secondOntology));
  }

  @ParameterizedTest
  @MethodSource("ontologies")
  void testRdfTypeLists(String ontologyName, String ontologyVersion, long classNumber, long propertyNumber) {
    final Ontology ontology = this.ontologyService.searchByProperties(Map.of("name", ontologyName, "version", ontologyVersion))
            .get(0);
    assertNotNull(ontology);
    final List<String> classList = this.ontologyService.listRdfClasses(ontology.getResId());
    assertTrue(classList.size() > 0);
    assertEquals(classNumber, classList.size());
    this.checkRdfTypeList(ontology.getBaseUri().toString(), classList, ontology.getResId(), HederaConstants.ONTOLOGY_RDF_CLASS);
    final List<String> propList = this.ontologyService.listRdfProperties(ontology.getResId());
    assertTrue(propList.size() > 0);
    assertEquals(propertyNumber, propList.size());
    this.checkRdfTypeList(ontology.getBaseUri().toString(), propList, ontology.getResId(), HederaConstants.ONTOLOGY_RDF_PROPERTY);
  }

  private void checkRdfTypeList(String baseUri, List<String> typeList, String ontologyId, String rdfType) {
    for (String type : typeList) {
      assertTrue(type.startsWith(baseUri), "Wrong prefix: " + type + " (" + baseUri + ")");
      final String rdfTypeName = this.getRdfTypeName(type);
      assertEquals(rdfType, this.ontologyService.isPartOf(ontologyId, rdfTypeName), "Wrong RDF type: " + rdfTypeName);
    }
  }

  private String getRdfTypeName(String type) {
    String separator = "/";
    if (type.contains("#")) {
      separator = "#";
    }
    return type.substring(type.lastIndexOf(separator) + 1);
  }

  private Ontology createLocalOntology(String ontologyName, String ontologyVersion, RdfFormat ontologyFormat)
          throws URISyntaxException, MalformedURLException {
    Ontology ontology = new Ontology();
    ontology.setDescription("Ontology Description");
    ontology.setName(ontologyName);
    ontology.setVersion(ontologyVersion);
    ontology.setBaseUri(new URI(HederaConstants.ONTOLOGY_CIDOC_CRM_BASE_URI));
    ontology.setFormat(ontologyFormat);
    ontology.setUrl(new URL("http://owl.com"));
    return ontology;
  }

  protected void clearOntologyFixtures() {
    final List<Ontology> ontologies = this.ontologyService.findAll();

    for (final Ontology o : ontologies) {
      if (o.getName().startsWith(HederaTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.ontologyService.delete(o.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearOntologyFixtures();
  }

  private static Stream<Arguments> ontologies() {
    return Stream.of(
            Arguments.of(HederaConstants.ONTOLOGY_CIDOC_CRM, "7.1.2", 76, 309),
            Arguments.of(HederaConstants.ONTOLOGY_CRM_DIG, "3.2.1", 16, 68),
            Arguments.of(HederaConstants.ONTOLOGY_RIC_O, "1.0", 105, 400),
            Arguments.of(HederaConstants.ONTOLOGY_EBU_CORE, "1.10", 225, 751),
            Arguments.of(HederaConstants.ONTOLOGY_EXIF, "2003-12", 1, 160));
  }

}
