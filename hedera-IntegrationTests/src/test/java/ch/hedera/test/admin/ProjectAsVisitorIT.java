/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ProjectAsVisitorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import java.io.IOException;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.security.Role;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.FundingAgencyITService;
import ch.hedera.test.service.InstitutionITService;
import ch.hedera.test.service.PersonITService;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.RmlITService;

@Tag(HederaTestConstants.AS_VISITOR_TAG)
class ProjectAsVisitorIT extends AbstractProjectWithRoleIT {

  @Autowired
  public ProjectAsVisitorIT(Environment env,
          SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          ProjectITService projectITService,
          InstitutionITService institutionITService,
          FundingAgencyITService fundingAgencyITService,
          PersonITService personITService,
          RmlITService rmlITService,
          ResearchObjectTypeITService researchObjectTypeITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService, projectITService, institutionITService,
            fundingAgencyITService, personITService, rmlITService, researchObjectTypeITService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @Override
  protected String role() {
    return Role.VISITOR_ID;
  }

  @Test
  void addAndRemoveInstitutionsTest() {
    this.addAndRemoveInstitutionsShouldFailTest();
  }

  @Test
  void addAndRemoveFundingAgencyTest() {
    this.addAndRemoveFundingAgencyShouldFailTest();
  }

  @Test
  void addAndRemovePersonTest() {
    this.addAndRemovePersonShouldFailTest();
  }

  @Test
  void addAndRemoveRmlTest() throws IOException {
    this.addAndRemoveRmlShouldFailTest();
  }

  @Test
  void addAndRemoveResearchObjectTypeTest() throws IOException {
    this.addAndRemoveResearchObjectTypesShouldFailTest();
  }

  @Test
  void downloadLogoTest() throws IOException {
    this.downloadLogoShouldSucceedTest();
  }

  @Test
  void uploadLogoTest() {
    this.uploadLogoShouldFailTest();
  }

  @Test
  void deleteLogoTest() throws IOException {
    this.deleteLogoShouldFailTest();
  }
}
