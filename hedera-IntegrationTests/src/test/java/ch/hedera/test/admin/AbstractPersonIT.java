/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AbstractPersonIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.security.User;
import ch.hedera.model.settings.Person;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.service.PersonITService;

public abstract class AbstractPersonIT extends AbstractAdminIT {

  protected PersonITService personITService;

  public AbstractPersonIT(Environment env,
          SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          PersonITService personITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
    this.personITService = personITService;
  }

  protected Person getLinkedPersonToUser(User user) {
    return this.personITService.findOne(user.getPerson().getResId());
  }
}
