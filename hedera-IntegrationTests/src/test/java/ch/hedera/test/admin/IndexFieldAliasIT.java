/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - IndexFieldAliasIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.core.JsonProcessingException;

import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.HederaConstants;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;

class IndexFieldAliasIT extends AbstractAdminIT {

  @Autowired
  public IndexFieldAliasIT(Environment env, SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
  }

  private static final Integer FACET_MIN_COUNT = 1;
  private static final Integer FACET_LIMIT = 30;
  private static final Integer FACET_DEFAULT_VISIBLE_VALUES = 10;
  private static final Integer FACET_ORDER = 100;

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    IndexFieldAlias indexFieldAlias = this.getBasicIndexFieldAlias();
    indexFieldAlias = this.indexFieldAliasService.create(indexFieldAlias);

    final IndexFieldAlias currentIndexFieldAlias = this.indexFieldAliasService.findOne(indexFieldAlias.getResId());

    assertEquals(indexFieldAlias.getIndexName(), currentIndexFieldAlias.getIndexName());
    assertEquals(indexFieldAlias.getAlias(), currentIndexFieldAlias.getAlias());
    assertEquals(indexFieldAlias.getField(), currentIndexFieldAlias.getField());
    assertFalse(currentIndexFieldAlias.isFacet());
    assertNull(currentIndexFieldAlias.getFacetDefaultVisibleValues());
    assertNull(currentIndexFieldAlias.getFacetLimit());
    assertNull(currentIndexFieldAlias.getFacetMinCount());
    assertNull(currentIndexFieldAlias.getFacetOrder());
  }

  @Test
  void creationFacetTest() {
    IndexFieldAlias indexFieldAlias = this.getFacetIndexFieldAlias();
    indexFieldAlias = this.indexFieldAliasService.create(indexFieldAlias);

    final IndexFieldAlias currentIndexFieldAlias = this.indexFieldAliasService.findOne(indexFieldAlias.getResId());

    assertEquals(indexFieldAlias.getIndexName(), currentIndexFieldAlias.getIndexName());
    assertEquals(indexFieldAlias.getAlias(), currentIndexFieldAlias.getAlias());
    assertEquals(indexFieldAlias.getField(), currentIndexFieldAlias.getField());
    assertTrue(currentIndexFieldAlias.isFacet());
    assertEquals(FACET_ORDER, indexFieldAlias.getFacetOrder());
    assertEquals(FACET_MIN_COUNT, indexFieldAlias.getFacetMinCount());
    assertEquals(FACET_LIMIT, indexFieldAlias.getFacetLimit());
    assertEquals(FACET_DEFAULT_VISIBLE_VALUES, indexFieldAlias.getFacetDefaultVisibleValues());
  }

  @Test
  void facetDefaultOrderTest() {
    IndexFieldAlias indexFieldAlias1 = this.getFacetIndexFieldAlias();
    this.indexFieldAliasService.create(indexFieldAlias1);

    IndexFieldAlias indexFieldAlias2 = this.getFacetIndexFieldAlias();
    indexFieldAlias2.setFacetOrder(null);
    indexFieldAlias2 = this.indexFieldAliasService.create(indexFieldAlias2);

    final IndexFieldAlias currentIndexFieldAlias = this.indexFieldAliasService.findOne(indexFieldAlias2.getResId());
    assertEquals(currentIndexFieldAlias.getFacetOrder(), new Integer(FACET_ORDER + HederaConstants.ORDER_INCREMENT));
  }

  @Test
  void facetValidationTest() throws JsonProcessingException {
    final IndexFieldAlias indexFieldAlias1 = this.getFacetIndexFieldAlias();
    indexFieldAlias1.setFacetLimit(null);

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.indexFieldAliasService.create(indexFieldAlias1));
    assertTrue(this.getFieldValidationErrors(e, "facetLimit").contains("The value is mandatory if the item is a facet"));

    final IndexFieldAlias indexFieldAlias2 = this.getFacetIndexFieldAlias();
    indexFieldAlias2.setFacetMinCount(null);
    e = assertThrows(HttpClientErrorException.class, () -> this.indexFieldAliasService.create(indexFieldAlias2));
    assertTrue(this.getFieldValidationErrors(e, "facetMinCount").contains("The value is mandatory if the item is a facet"));

    final IndexFieldAlias indexFieldAlias3 = this.getFacetIndexFieldAlias();
    indexFieldAlias3.setFacetLimit(10);
    indexFieldAlias3.setFacetDefaultVisibleValues(20);
    e = assertThrows(HttpClientErrorException.class, () -> this.indexFieldAliasService.create(indexFieldAlias3));
    assertTrue(this.getFieldValidationErrors(e, "facetDefaultVisibleValues")
            .contains("The number of default visible values cannot be greater than the limit number"));

    final IndexFieldAlias indexFieldAlias4 = this.getFacetIndexFieldAlias();
    indexFieldAlias4.setFacetLimit(0);
    indexFieldAlias4.setFacetDefaultVisibleValues(0);
    indexFieldAlias4.setFacetMinCount(0);
    indexFieldAlias4.setFacetOrder(0);

    e = assertThrows(HttpClientErrorException.class, () -> this.indexFieldAliasService.create(indexFieldAlias4));
    assertTrue(this.getFieldValidationErrors(e, "facetLimit").contains("The value must be greater than 0"));
    assertTrue(this.getFieldValidationErrors(e, "facetDefaultVisibleValues").contains("The value must be greater than 0"));
    assertTrue(this.getFieldValidationErrors(e, "facetMinCount").contains("The value must be greater than 0"));
    assertTrue(this.getFieldValidationErrors(e, "facetOrder").contains("The value must be greater than 0"));

    final IndexFieldAlias indexFieldAlias5 = this.getFacetIndexFieldAlias();
    indexFieldAlias5.setFacetLimit(-1);
    indexFieldAlias5.setFacetDefaultVisibleValues(-1);
    indexFieldAlias5.setFacetMinCount(-1);
    indexFieldAlias5.setFacetOrder(-1);
    e = assertThrows(HttpClientErrorException.class, () -> this.indexFieldAliasService.create(indexFieldAlias5));
    assertTrue(this.getFieldValidationErrors(e, "facetLimit").contains("The value must be greater than 0"));
    assertTrue(this.getFieldValidationErrors(e, "facetDefaultVisibleValues").contains("The value must be greater than 0"));
    assertTrue(this.getFieldValidationErrors(e, "facetMinCount").contains("The value must be greater than 0"));
    assertTrue(this.getFieldValidationErrors(e, "facetOrder").contains("The value must be greater than 0"));
  }

  private IndexFieldAlias getBasicIndexFieldAlias() {
    IndexFieldAlias indexFieldAlias = new IndexFieldAlias();
    indexFieldAlias.setIndexName(HederaTestConstants.TEST_RES_ID);
    indexFieldAlias.setAlias(HederaTestConstants.getRandomNameWithTemporaryLabel("alias"));
    indexFieldAlias.setField(HederaTestConstants.getRandomNameWithTemporaryLabel("path.to.value"));
    return indexFieldAlias;
  }

  private IndexFieldAlias getFacetIndexFieldAlias() {
    IndexFieldAlias indexFieldAlias = this.getBasicIndexFieldAlias();
    indexFieldAlias.setFacet(true);
    indexFieldAlias.setFacetMinCount(FACET_MIN_COUNT);
    indexFieldAlias.setFacetLimit(FACET_LIMIT);
    indexFieldAlias.setFacetDefaultVisibleValues(FACET_DEFAULT_VISIBLE_VALUES);
    indexFieldAlias.setFacetOrder(FACET_ORDER);
    return indexFieldAlias;
  }

  @Test
  void deleteTest() {

    IndexFieldAlias indexFieldAlias = this.getBasicIndexFieldAlias();
    indexFieldAlias = this.indexFieldAliasService.create(indexFieldAlias);

    final IndexFieldAlias currentIndexFieldAlias = this.indexFieldAliasService.findOne(indexFieldAlias.getResId());
    assertNotNull(currentIndexFieldAlias);

    final String resId = indexFieldAlias.getResId();
    this.indexFieldAliasService.delete(resId);

    assertThrows(HttpClientErrorException.NotFound.class, () -> this.indexFieldAliasService.findOne(resId));
  }

  @Test
  void findAllTest() {
    IndexFieldAlias indexFieldAlias = this.getBasicIndexFieldAlias();
    this.indexFieldAliasService.create(indexFieldAlias);
    IndexFieldAlias indexFieldAlias2 = this.getBasicIndexFieldAlias();
    this.indexFieldAliasService.create(indexFieldAlias2);

    final List<IndexFieldAlias> indexFieldAliases = this.indexFieldAliasService.findAll();

    assertFalse(indexFieldAliases.isEmpty());
    assertTrue(indexFieldAliases.size() >= 2);
  }

  @Test
  void updateTest() {
    IndexFieldAlias indexFieldAlias = this.getBasicIndexFieldAlias();
    this.indexFieldAliasService.create(indexFieldAlias);
    final IndexFieldAlias currentIndexFieldAlias = this.indexFieldAliasService.findOne(indexFieldAlias.getResId());

    String alias = "updated alias";
    String field = "updated field";
    currentIndexFieldAlias.setAlias(alias);
    currentIndexFieldAlias.setField(field);
    this.indexFieldAliasService.update(currentIndexFieldAlias.getResId(), currentIndexFieldAlias);

    final IndexFieldAlias updatedIndexFieldAlias = this.indexFieldAliasService.findOne(indexFieldAlias.getResId());

    assertEquals(updatedIndexFieldAlias.getIndexName(), currentIndexFieldAlias.getIndexName());
    assertEquals(updatedIndexFieldAlias.getAlias(), alias);
    assertEquals(updatedIndexFieldAlias.getField(), field);

    // facet values reset to NULL if object is not a facet
    IndexFieldAlias facetIndexFieldAlias = this.getFacetIndexFieldAlias();
    this.indexFieldAliasService.create(facetIndexFieldAlias);
    final IndexFieldAlias currentFacetIndexFieldAlias = this.indexFieldAliasService.findOne(facetIndexFieldAlias.getResId());
    assertEquals(FACET_ORDER, facetIndexFieldAlias.getFacetOrder());
    assertEquals(FACET_MIN_COUNT, facetIndexFieldAlias.getFacetMinCount());
    assertEquals(FACET_LIMIT, facetIndexFieldAlias.getFacetLimit());
    assertEquals(FACET_DEFAULT_VISIBLE_VALUES, facetIndexFieldAlias.getFacetDefaultVisibleValues());

    currentFacetIndexFieldAlias.setFacet(false);
    currentFacetIndexFieldAlias.setSystem(false);
    this.indexFieldAliasService.update(currentFacetIndexFieldAlias.getResId(), currentFacetIndexFieldAlias);

    this.indexFieldAliasService.findOne(facetIndexFieldAlias.getResId());
    assertNull(indexFieldAlias.getFacetOrder());
    assertNull(indexFieldAlias.getFacetMinCount());
    assertNull(indexFieldAlias.getFacetLimit());
    assertNull(indexFieldAlias.getFacetDefaultVisibleValues());
  }

  @Override
  protected void deleteFixtures() {
    this.clearIndexFieldAliasFixtures();
  }
}
