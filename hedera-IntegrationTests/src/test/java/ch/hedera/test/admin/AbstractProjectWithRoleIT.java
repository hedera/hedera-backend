/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AbstractProjectWithRoleIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.BeforeAll;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.humanities.Rml;
import ch.hedera.model.humanities.RmlFormat;
import ch.hedera.model.settings.FundingAgency;
import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.HederaTestConstants.ProjectStatus;
import ch.hedera.test.service.FundingAgencyITService;
import ch.hedera.test.service.InstitutionITService;
import ch.hedera.test.service.PersonITService;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.RmlITService;

abstract class AbstractProjectWithRoleIT extends AbstractProjectIT {

  public AbstractProjectWithRoleIT(Environment env,
          SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          ProjectITService projectITService,
          InstitutionITService institutionITService,
          FundingAgencyITService fundingAgencyITService,
          PersonITService personITService,
          RmlITService rmlITService,
          ResearchObjectTypeITService researchObjectTypeITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService, projectITService, institutionITService,
            fundingAgencyITService, personITService, rmlITService, researchObjectTypeITService);
  }

  protected abstract String role();

  @BeforeAll
  void setUp() {
    this.projectITService.getOrCreatePermanentProject(ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoAdmin();
    this.projectITService.cleanTestData();
    this.fundingAgencyITService.cleanTestData();
    this.institutionITService.cleanTestData();
    this.personITService.cleanTestData();
    this.rmlITService.cleanTestData();
    this.researchObjectTypeITService.cleanTestData();
    this.restClientTool.exitSudo();
  }

  public void downloadLogoShouldSucceedTest() throws IOException {
    /*
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());

    /*
     * Add logo
     */
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();
    this.restClientTool.sudoAdmin();
    Project project2 = this.projectITService.uploadLogo(project.getResId(), logoToUpload);
    final Project refetchedProject = this.projectITService.findOne(project2.getResId());
    assertEquals(refetchedProject.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedProject.getResourceFile().getFileSize(), sizeLogo);
    this.restClientTool.exitSudo();

    /*
     * Download logo
     */
    Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.tmp");
    this.projectITService.downloadLogo(refetchedProject.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedProject.getResourceFile().getFileSize());
  }

  public void uploadLogoShouldSucceedTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());

    /*
     * Add logo
     */
    Project project2 = this.projectITService.uploadLogo(project.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    Project project3 = this.projectITService.findOne(project2.getResId());
    assertEquals(project3.getResourceFile().getFileName(), logoName);
    assertEquals(project3.getResourceFile().getFileSize(), sizeLogo);

    /*
     * Update project again
     */
    String newName = HederaTestConstants.getRandomNameWithTemporaryLabel("project");
    project3.setName(newName);
    project3 = this.projectITService.update(project3.getResId(), project3);
    assertEquals(project3.getName(), newName);
  }

  public void uploadLogoShouldFailTest() {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);

    /*
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());

    /*
     * Add logo
     */
    String projectId = project.getResId();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.projectITService.uploadLogo(projectId, logoToUpload));
  }

  public void deleteLogoShouldSucceedTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());

    /*
     * Add logo
     */
    Project project2 = this.projectITService.uploadLogo(project.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    Project project3 = this.projectITService.findOne(project2.getResId());
    assertEquals(project3.getResourceFile().getFileName(), logoName);
    assertEquals(project3.getResourceFile().getFileSize(), sizeLogo);

    /*
     * Delete logo
     */
    this.projectITService.deleteLogo(project.getResId());
    project3 = this.projectITService.findOne(project2.getResId());
    assertNull(project3.getResourceFile());
  }

  public void deleteLogoShouldFailTest() {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);

    /*
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());
    String projectId = project.getResId();

    /*
     * Add logo
     */
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.projectITService.uploadLogo(projectId, logoToUpload));
  }

  protected void addAndRemoveInstitutionsShouldSucceedTest() {
    /*
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());
    List<Institution> list = this.projectITService.getInstitutions(project.getResId());
    final int initialState = list.size();

    /*
     * Create a new Institution
     */
    this.restClientTool.sudoAdmin();
    Institution institution = new Institution();
    institution.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Institution"));
    institution.setDescription("description" + ThreadLocalRandom.current().nextInt());
    institution = this.institutionService.create(institution);
    this.restClientTool.exitSudo();

    final String projectId = project.getResId();
    final String newInstitutionId = institution.getResId();

    /*
     * Add the Institution to the project
     */
    this.projectITService.addInstitution(projectId, newInstitutionId);
    list = this.projectITService.getInstitutions(projectId);
    assertEquals(initialState + 1, list.size());
    final String institutionName = institution.getName();
    final Optional<Institution> optionalInstitution = list.stream().filter(ins -> ins.getName().equals(institutionName)).findFirst();
    assertTrue(optionalInstitution.isPresent());

    /*
     * Check it has been added
     */
    assertEquals(newInstitutionId, optionalInstitution.get().getResId());
    assertEquals(institution.getName(), optionalInstitution.get().getName());
    assertEquals(institution.getDescription(), optionalInstitution.get().getDescription());

    /*
     * Remove the Institution
     */
    this.projectITService.removeInstitution(projectId, newInstitutionId);
    /*
     * Check it has been removed
     */
    list = this.projectITService.getInstitutions(projectId);
    assertEquals(initialState, list.size());

    /*
     * Create 3 Institutions and link them to the project
     */
    Institution institution1 = new Institution();
    institution1.setDescription(HederaTestConstants.getRandomNameWithTemporaryLabel("Institution Sub 1"));
    institution1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Institution Sub 1"));

    Institution institution2 = new Institution();
    institution2.setDescription(HederaTestConstants.getRandomNameWithTemporaryLabel("Institution Sub 2"));
    institution2.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Institution Sub 2"));

    Institution institution3 = new Institution();
    institution3.setDescription(HederaTestConstants.getRandomNameWithTemporaryLabel("Institution Sub 3"));
    institution3.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Institution Sub 3"));

    final Institution[] institutions = new Institution[] { institution1, institution2, institution3 };

    String institutionsToDelete = null;

    int i = 0;
    for (Institution ins : institutions) {
      this.restClientTool.sudoAdmin();
      ins = this.institutionService.create(ins);
      this.restClientTool.exitSudo();
      this.projectITService.addInstitution(projectId, ins.getResId());

      if (i == 1) {
        institutionsToDelete = ins.getResId();
      }

      i++;
    }

    /*
     * Check they are all linked correctly
     */
    list = this.projectITService.getInstitutions(project.getResId());
    assertNotNull(list);
    assertEquals(initialState + 3, list.size());

    /*
     * Delete one specifically and check it is not in the list anymore
     */
    this.projectITService.removeInstitution(project.getResId(), institutionsToDelete);
    list = this.projectITService.getInstitutions(project.getResId());
    assertNotNull(list);
    assertEquals(initialState + 2, list.size());
    for (final Institution fetchedInstitution : list) {
      assertNotEquals(institutionsToDelete, fetchedInstitution.getResId());
    }
  }

  protected void addAndRemoveInstitutionsShouldFailTest() {
    /*
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());

    List<Institution> list = this.projectITService.getInstitutions(project.getResId());
    final int initialState = list.size();

    /*
     * Create a new Institution
     */
    this.restClientTool.sudoAdmin();
    Institution institution = new Institution();
    institution.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Institution"));
    institution.setDescription("description" + ThreadLocalRandom.current().nextInt());
    institution = this.institutionITService.createRemoteInstitution(institution);
    this.restClientTool.exitSudo();

    final String projectId = project.getResId();
    final String newInstitutionId = institution.getResId();

    /*
     * Adding an Institution to the project is forbidden for a creator
     */
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.projectITService.addInstitution(projectId, newInstitutionId));

    /*
     * Add it as ADMIN
     */
    this.restClientTool.sudoAdmin();
    this.projectITService.addInstitution(projectId, newInstitutionId);
    this.restClientTool.exitSudo();

    list = this.projectITService.getInstitutions(project.getResId());
    assertEquals(initialState + 1, list.size());
    final String institutionName = institution.getName();
    final Optional<Institution> optionalInstitution = list.stream().filter(ins -> ins.getName().equals(institutionName)).findFirst();
    assertTrue(optionalInstitution.isPresent());

    /*
     * Check it has been added
     */
    assertEquals(newInstitutionId, optionalInstitution.get().getResId());
    assertEquals(institution.getName(), optionalInstitution.get().getName());
    assertEquals(institution.getDescription(), optionalInstitution.get().getDescription());

    /*
     * Remove the Institution
     */
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.projectITService.removeInstitution(projectId, newInstitutionId));
  }

  void addAndRemoveFundingAgencyShouldSucceedTest() {
    /*
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());
    List<FundingAgency> fundingAgencies = this.projectITService.getFundingAgencies(project.getResId());
    final int initialState = fundingAgencies.size();

    /*
     * Create a new FundingAgency
     */
    this.restClientTool.sudoAdmin();
    FundingAgency newAgency = this.fundingAgencyITService.createRemoteFundingAgency("FA", "My FundingAgency");
    final String fundingAgencyName = newAgency.getName();
    this.restClientTool.exitSudo();

    final String projectId = project.getResId();
    final String newAgencyId = newAgency.getResId();

    /*
     * Add the FundingAgency to the project
     */
    this.projectITService.addFundingAgency(projectId, newAgencyId);

    /*
     * Check it has been added
     */
    fundingAgencies = this.projectITService.getFundingAgencies(projectId);
    assertNotNull(fundingAgencies);
    assertEquals(initialState + 1, fundingAgencies.size());
    final Optional<FundingAgency> optionalFA = fundingAgencies.stream().filter(fa -> fa.getName().equals(fundingAgencyName)).findFirst();
    assertTrue(optionalFA.isPresent());
    assertEquals(fundingAgencyName, optionalFA.get().getName());

    /*
     * Remove the FundingAgency
     */
    this.projectITService.removeFundingAgency(projectId, newAgencyId);

    /*
     * Check it has been removed
     */
    fundingAgencies = this.projectITService.getFundingAgencies(projectId);
    assertNotNull(fundingAgencies);
    assertEquals(initialState, fundingAgencies.size());

    /*
     * Create 3 FundingAgencies and link them to the project
     */
    final FundingAgency subFA1 = new FundingAgency();
    subFA1.setAcronym(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub FA 1"));
    subFA1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub FundingAgency 1"));

    final FundingAgency subFA2 = new FundingAgency();
    subFA2.setAcronym(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub FA 2"));
    subFA2.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub FundingAgency 2"));

    final FundingAgency subFA3 = new FundingAgency();
    subFA3.setAcronym(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub FA 3"));
    subFA3.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub FundingAgency 3"));

    final FundingAgency[] subAgencies = new FundingAgency[] { subFA1, subFA2, subFA3 };

    String agencyIdToDelete = null;

    int i = 0;
    for (FundingAgency subAgency : subAgencies) {
      this.restClientTool.sudoAdmin();
      subAgency = this.fundingAgencyService.create(subAgency);
      this.restClientTool.exitSudo();
      this.projectITService.addFundingAgency(projectId, subAgency.getResId());

      if (i == 1) {
        agencyIdToDelete = subAgency.getResId();
      }

      i++;
    }

    /*
     * Check they are all linked correctly
     */
    fundingAgencies = this.projectITService.getFundingAgencies(projectId);
    assertNotNull(fundingAgencies);
    assertEquals(initialState + 3, fundingAgencies.size());

    /*
     * Delete one specifically and check it is not in the list anymore
     */
    this.projectITService.removeFundingAgency(projectId, agencyIdToDelete);
    fundingAgencies = this.projectITService.getFundingAgencies(projectId);
    assertNotNull(fundingAgencies);
    assertEquals(initialState + 2, fundingAgencies.size());
    for (final FundingAgency fetchedAgency : fundingAgencies) {
      assertNotEquals(agencyIdToDelete, fetchedAgency.getResId());
    }
  }

  protected void addAndRemoveFundingAgencyShouldFailTest() {
    /*
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());

    List<FundingAgency> fundingAgencies = this.projectITService.getFundingAgencies(project.getResId());
    final int initialState = fundingAgencies.size();

    /*
     * Create a new FundingAgency
     */
    this.restClientTool.sudoAdmin();
    FundingAgency newAgency = this.fundingAgencyITService.createRemoteFundingAgency("FA", "My FundingAgency");
    final String fundingAgencyName = newAgency.getName();
    this.restClientTool.exitSudo();

    /*
     * Adding the FundingAgency to the project is forbidden for a creator
     */
    final String projectId = project.getResId();
    final String newAgencyId = newAgency.getResId();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.projectITService.addFundingAgency(projectId, newAgencyId));

    /*
     * Add it as ADMIN
     */
    this.restClientTool.sudoAdmin();
    this.projectITService.addFundingAgency(projectId, newAgencyId);
    this.restClientTool.exitSudo();

    /*
     * Check it has been added
     */
    fundingAgencies = this.projectITService.getFundingAgencies(project.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(initialState + 1, fundingAgencies.size());
    final Optional<FundingAgency> optionalFA = fundingAgencies.stream().filter(fa -> fa.getName().equals(fundingAgencyName)).findFirst();
    assertTrue(optionalFA.isPresent());
    assertEquals(fundingAgencyName, optionalFA.get().getName());

    /*
     * Removing the FundingAgency is forbidden for a creator
     */
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.projectITService.removeFundingAgency(projectId, newAgencyId));
  }

  protected void addAndRemoveRmlShouldSucceedTest() throws IOException {
    /*
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(ProjectStatus.OPEN, this.role());
    List<Rml> rmls = this.projectITService.getRmls(project.getResId());
    final int initialState = rmls.size();

    /*
     * Create a new RMl
     */
    this.restClientTool.sudoAdmin();
    Rml newRml = this.rmlITService.createRemoteRml(HederaTestConstants.getRandomNameWithTemporaryLabel("Rml1"), RmlFormat.CSV, "1",
            "description",
            new ClassPathResource("empty.txt"));
    final String rmlName = newRml.getName();
    this.restClientTool.exitSudo();

    final String projectId = project.getResId();
    final String newRmlId = newRml.getResId();

    /*
     * Add the Rml to the project
     */
    this.projectITService.addRml(projectId, newRmlId);

    /*
     * Check it has been added
     */
    rmls = this.projectITService.getRmls(projectId);
    assertNotNull(rmls);
    assertEquals(initialState + 1, rmls.size());
    final Optional<Rml> optionalRml = rmls.stream().filter(fa -> fa.getName().equals(rmlName)).findFirst();
    assertTrue(optionalRml.isPresent());
    assertEquals(rmlName, optionalRml.get().getName());

    /*
     * Remove the Rml
     */
    this.projectITService.removeRml(projectId, newRmlId);

    /*
     * Check it has been removed
     */
    rmls = this.projectITService.getRmls(projectId);
    assertNotNull(rmls);
    assertEquals(initialState, rmls.size());

    /*
     * Create 3 RMLs and link them to the project
     */
    final Rml rml1 = new Rml();
    rml1.setFormat(RmlFormat.SQL);
    rml1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub RML 1"));
    rml1.setVersion("1");
    rml1.setDescription("description 1");

    final Rml rml2 = new Rml();
    rml2.setFormat(RmlFormat.SQL);
    rml2.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub RML 2"));
    rml2.setVersion("1");
    rml2.setDescription("description 2");

    final Rml rml3 = new Rml();
    rml3.setFormat(RmlFormat.SQL);
    rml3.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("Sub RML 3"));
    rml3.setVersion("1");
    rml3.setDescription("description 3");

    final Rml[] subRmls = new Rml[] { rml1, rml2, rml3 };
    String rmlIdToDelete = null;

    int i = 0;
    for (Rml subRml : subRmls) {
      this.restClientTool.sudoAdmin();
      subRml = this.rmlITService.createRemoteRml(subRml, new ClassPathResource("empty.txt"));
      this.restClientTool.exitSudo();
      this.projectITService.addRml(projectId, subRml.getResId());

      if (i == 1) {
        rmlIdToDelete = subRml.getResId();
      }

      i++;
    }

    /*
     * Check they are all linked correctly
     */
    rmls = this.projectITService.getRmls(projectId);
    assertNotNull(rmls);
    assertEquals(initialState + 3, rmls.size());

    /*
     * Delete one specifically and check it is not in the list anymore
     */
    this.projectITService.removeRml(projectId, rmlIdToDelete);
    rmls = this.projectITService.getRmls(projectId);
    assertNotNull(rmls);
    assertEquals(initialState + 2, rmls.size());
    for (final Rml fetchedRml : subRmls) {
      assertNotEquals(rmlIdToDelete, fetchedRml.getResId());
    }
  }

  protected void addAndRemoveRmlShouldFailTest() throws IOException {
    /*-
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());

    List<Rml> rmls = this.projectITService.getRmls(project.getResId());
    final int initialState = rmls.size();

    /*
     * Create a new Rml
     */
    this.restClientTool.sudoAdmin();
    Rml newRml = this.rmlITService.createRemoteRml(HederaTestConstants.getRandomNameWithTemporaryLabel("Rml1"), RmlFormat.CSV, "1",
            "description", new ClassPathResource("empty.txt"));
    final String rmlName = newRml.getName();
    this.restClientTool.exitSudo();

    /*
     * Adding the Rml to the project is forbidden for a creator
     */
    final String projectId = project.getResId();
    final String newRmlId = newRml.getResId();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.projectITService.addRml(projectId, newRmlId));

    /*
     * Add it as ADMIN
     */
    this.restClientTool.sudoAdmin();
    this.projectITService.addRml(projectId, newRmlId);
    this.restClientTool.exitSudo();

    /*
     * Check it has been added
     */
    rmls = this.projectITService.getRmls(project.getResId());
    assertNotNull(rmls);
    assertEquals(initialState + 1, rmls.size());
    final Optional<Rml> optionalRml = rmls.stream().filter(fa -> fa.getName().equals(rmlName)).findFirst();
    assertTrue(optionalRml.isPresent());
    assertEquals(rmlName, optionalRml.get().getName());

    /*
     * Removing the Rml is forbidden for a creator
     */
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.projectITService.removeRml(projectId, newRmlId));

  }

  protected void addAndRemoveResearchObjectTypesShouldFailTest() throws IOException {
    /*-
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());

    List<ResearchObjectType> researchObjectTypes = this.projectITService.getResearchObjectTypes(project.getResId());
    final int initialState = researchObjectTypes.size();

    /*
     * Create a new ResearchObject
     */
    this.restClientTool.sudoAdmin();
    ResearchObjectType newResearchObjectType = this.researchObjectTypeITService.getResearchObjectTypeForTesting();
    newResearchObjectType = this.researchObjectTypeITService.createRemoteResearchObjectType(newResearchObjectType);
    final String newResearchObjectName = newResearchObjectType.getName();
    this.restClientTool.exitSudo();

    /*
     * Adding the ResearchObject to the project is forbidden
     */
    final String projectId = project.getResId();
    final String newResearchObjectResId = newResearchObjectType.getResId();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.projectITService.addResearchObjectType(projectId, newResearchObjectResId));

    /*
     * Add it as ADMIN
     */
    this.restClientTool.sudoAdmin();
    this.projectITService.addResearchObjectType(projectId, newResearchObjectResId);
    this.restClientTool.exitSudo();

    /*
     * Check it has been added
     */
    researchObjectTypes = this.projectITService.getResearchObjectTypes(project.getResId());
    assertNotNull(researchObjectTypes);
    assertEquals(initialState + 1, researchObjectTypes.size());
    final Optional<ResearchObjectType> optionalResearchObject = researchObjectTypes.stream()
            .filter(ro -> ro.getName().equals(newResearchObjectName))
            .findFirst();
    assertTrue(optionalResearchObject.isPresent());
    assertEquals(newResearchObjectName, optionalResearchObject.get().getName());

    /*
     * Removing the Rml is forbidden for a creator
     */
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.projectITService.removeRml(projectId, newResearchObjectResId));

  }

  protected void addAndRemoveResearchObjectTypesShouldSucceedTest() throws IOException {
    /*
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(ProjectStatus.OPEN, this.role());
    List<ResearchObjectType> researchObjectTypes = this.projectITService.getResearchObjectTypes(project.getResId());
    final int initialState = researchObjectTypes.size();

    /*
     * Create a new ResearchObject
     */
    this.restClientTool.sudoAdmin();
    ResearchObjectType newResearchObjectType = this.researchObjectTypeITService.getResearchObjectTypeForTesting();
    newResearchObjectType = this.researchObjectTypeITService.createRemoteResearchObjectType(newResearchObjectType);
    final String newResearchObjectName = newResearchObjectType.getName();
    this.restClientTool.exitSudo();

    final String projectId = project.getResId();
    final String newResearchObjectResId = newResearchObjectType.getResId();

    /*
     * Add the ResearchObject to the project
     */
    this.projectITService.addResearchObjectType(projectId, newResearchObjectResId);

    /*
     * Check it has been added
     */
    researchObjectTypes = this.projectITService.getResearchObjectTypes(projectId);
    assertNotNull(researchObjectTypes);
    assertEquals(initialState + 1, researchObjectTypes.size());
    final Optional<ResearchObjectType> optionalResearchObject = researchObjectTypes.stream()
            .filter(ro -> ro.getName().equals(newResearchObjectName))
            .findFirst();
    assertTrue(optionalResearchObject.isPresent());
    assertEquals(newResearchObjectName, optionalResearchObject.get().getName());

    /*
     * Remove the ResearchObject
     */
    this.projectITService.removeResearchObjectType(projectId, newResearchObjectResId);

    /*
     * Check it has been removed
     */
    researchObjectTypes = this.projectITService.getResearchObjectTypes(projectId);
    assertNotNull(researchObjectTypes);
    assertEquals(initialState, researchObjectTypes.size());

    /*
     * Create 3 ResearchObject and link them to the project
     */
    final ResearchObjectType researchObjectType1 = this.researchObjectTypeITService.getResearchObjectTypeForTesting();
    final ResearchObjectType researchObjectType2 = this.researchObjectTypeITService.getResearchObjectTypeForTesting();
    final ResearchObjectType researchObjectType3 = this.researchObjectTypeITService.getResearchObjectTypeForTesting();

    final ResearchObjectType[] subRO = new ResearchObjectType[] { researchObjectType1, researchObjectType2, researchObjectType3 };
    String roIdToDelete = null;

    int i = 0;
    for (ResearchObjectType subResearchObjectType : subRO) {
      this.restClientTool.sudoAdmin();
      subResearchObjectType = this.researchObjectTypeITService.createRemoteResearchObjectType(subResearchObjectType);
      this.restClientTool.exitSudo();
      this.projectITService.addResearchObjectType(projectId, subResearchObjectType.getResId());

      if (i == 1) {
        roIdToDelete = subResearchObjectType.getResId();
      }

      i++;
    }

    /*
     * Check they are all linked correctly
     */
    researchObjectTypes = this.projectITService.getResearchObjectTypes(projectId);
    assertNotNull(researchObjectTypes);
    assertEquals(initialState + 3, researchObjectTypes.size());

    /*
     * Delete one specifically and check it is not in the list anymore
     */
    this.projectITService.removeResearchObjectType(projectId, roIdToDelete);
    researchObjectTypes = this.projectITService.getResearchObjectTypes(projectId);
    assertNotNull(researchObjectTypes);
    assertEquals(initialState + 2, researchObjectTypes.size());
    for (final ResearchObjectType fetchedResearchObjectType : researchObjectTypes) {
      assertNotEquals(roIdToDelete, fetchedResearchObjectType.getResId());
    }
  }

  protected void addAndRemovePersonShouldSucceedTest() {
    this.restClientTool.sudoAdmin();
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());
    final Person person = this.personITService.createRemoteTemporaryPerson();
    this.restClientTool.exitSudo();

    final String projectId = project.getResId();
    final String personId = person.getResId();

    final int totalPeopleBeforeAdding = this.projectITService.getPeople(projectId).size();

    // Add the person to the project with default role
    this.projectITService.addPerson(projectId, person.getResId());

    final List<Person> people = this.projectITService.getPeople(projectId);
    Optional<Person> addedPersonOpt = people.stream().filter(p -> p.getResId().equals(personId)).findFirst();

    assertEquals(totalPeopleBeforeAdding + 1, people.size());
    assertTrue(addedPersonOpt.isPresent());
    assertEquals(person.getFirstName(), addedPersonOpt.get().getFirstName());
    assertEquals(person.getLastName(), addedPersonOpt.get().getLastName());
    assertEquals(person.getOrcid(), addedPersonOpt.get().getOrcid());

    // Remove the person from the project
    this.projectITService.removePerson(projectId, personId);

    final List<Person> peopleAfterRemove = this.projectITService.getPeople(projectId);
    Optional<Person> addedPersonAfterRemoveOpt = peopleAfterRemove.stream().filter(p -> p.getResId().equals(personId)).findFirst();
    assertEquals(totalPeopleBeforeAdding, peopleAfterRemove.size());
    assertTrue(addedPersonAfterRemoveOpt.isEmpty());
  }

  protected void addAndRemovePersonShouldFailTest() {
    this.restClientTool.sudoAdmin();
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());
    final Person person = this.personITService.createRemoteTemporaryPerson();
    this.restClientTool.exitSudo();

    final String projectId = project.getResId();
    final String personId = person.getResId();

    final int totalPeopleBeforeAdding = this.projectITService.getPeople(projectId).size();

    // Adding a person to the project is forbidden for creators
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.projectITService.addPerson(projectId, personId));

    /*
     * Add it as ADMIN
     */
    this.restClientTool.sudoAdmin();
    this.projectITService.addPerson(projectId, personId);
    this.restClientTool.exitSudo();

    final List<Person> people = this.projectITService.getPeople(projectId);
    assertEquals(totalPeopleBeforeAdding + 1, people.size());

    // Removing a person from a project is forbidden for creators
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.projectITService.removePerson(projectId, personId));
  }

  protected void createProjectWithoutResearchDataFileResearchObjectTypeShouldFailTest() {
    // Create a project
    Project project1 = new Project();
    project1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("project"));
    this.projectITService.setRandomUrl(project1);
    project1.setDescription("Swiss Research Data Preservation #CREATE");

    final LocalDate closingDate = LocalDate.now();
    final LocalDate openingDate = closingDate.plusDays(7);
    project1.setOpeningDate(openingDate);
    project1.setClosingDate(closingDate);

    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.projectITService.create(project1));
  }
}
