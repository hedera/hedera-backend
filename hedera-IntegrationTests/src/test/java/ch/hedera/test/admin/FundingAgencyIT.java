/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - FundingAgencyIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.settings.FundingAgency;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.HederaTestConstants.ProjectStatus;
import ch.hedera.test.service.ProjectITService;

class FundingAgencyIT extends AbstractAdminIT {

  protected ProjectITService projectITService;

  @Autowired
  public FundingAgencyIT(Environment env, SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          ProjectITService projectITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
    this.projectITService = projectITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void testAddAndRemoveProject() {

    final String fundingAgencyAcronym = HederaTestConstants.getRandomNameWithTemporaryLabel("FA");
    final String fundingAgencyName = HederaTestConstants.getRandomNameWithTemporaryLabel("My FundingAgency with Unit");
    final String projectName = HederaTestConstants.getNameWithPermanentLabel("Project for manager");

    /*
     * Create a new FundingAgency
     */
    FundingAgency newAgency = new FundingAgency();
    newAgency.setAcronym(fundingAgencyAcronym);
    newAgency.setName(fundingAgencyName);
    newAgency = this.fundingAgencyService.create(newAgency);

    /*
     * Create a new project and check number of projects
     */
    final Project newProject = this.projectITService.getOrCreatePermanentProjectAsManager(ProjectStatus.OPEN);
    List<Project> projects = this.fundingAgencyService.getProjects(newAgency.getResId());
    final int sizeBefore = projects.size();

    /*
     * Add the project to the FundingAgency
     */
    this.fundingAgencyService.addProject(newAgency.getResId(), newProject.getResId());

    /*
     * Check it has been added
     */

    projects = this.fundingAgencyService.getProjects(newAgency.getResId());
    assertNotNull(projects);
    assertEquals(sizeBefore + 1, projects.size());
    final Optional<Project> projectOpt = projects.stream().filter(project -> project.getName().equals(newProject.getName())).findFirst();
    assertTrue(projectOpt.isPresent());
    assertEquals(projectName, projectOpt.get().getName());

    /*
     * Remove the project
     */
    this.fundingAgencyService.removeProject(newAgency.getResId(), newProject.getResId());

    /*
     * Check it has been removed
     */
    projects = this.fundingAgencyService.getProjects(newAgency.getResId());
    assertNotNull(projects);
    assertEquals(sizeBefore, projects.size());

    /*
     * Add two organizational unit and link them to the FundingAgency
     */
    final int beforeAddingFundingAgencies = this.fundingAgencyService.getProjects(newAgency.getResId()).size();
    this.fundingAgencyService.addProject(newAgency.getResId(), newProject.getResId());

    final Project project2 = this.projectITService.getSecondPermanentProject();

    this.fundingAgencyService.addProject(newAgency.getResId(), project2.getResId());

    /*
     * Check they are all linked correctly
     */
    projects = this.fundingAgencyService.getProjects(newAgency.getResId());
    assertNotNull(projects);
    assertEquals(beforeAddingFundingAgencies + 2, projects.size());

    /*
     * Delete one specifically and check it is not in the list anymore
     */
    this.fundingAgencyService.removeProject(newAgency.getResId(), project2.getResId());
    projects = this.fundingAgencyService.getProjects(newAgency.getResId());
    assertNotNull(projects);
    assertEquals(beforeAddingFundingAgencies + 1, projects.size());
    for (final Project fetchedProject : projects) {
      assertNotEquals(project2.getResId(), fetchedProject.getResId());
    }
  }

  @Test
  void testCreate() {

    final String agencyAcronym = HederaTestConstants.getRandomNameWithTemporaryLabel("FA");
    final String agencyName = HederaTestConstants.getRandomNameWithTemporaryLabel("My FundingAgency");

    /*
     * Create a new FundingAgency
     */
    FundingAgency newAgency = new FundingAgency();
    newAgency.setAcronym(agencyAcronym);
    newAgency.setName(agencyName);
    newAgency = this.fundingAgencyService.create(newAgency);

    /*
     * Fetch it from server and test it has been created
     */
    final FundingAgency fetchedAgency = this.fundingAgencyService.findOne(newAgency.getResId());
    assertNotNull(fetchedAgency.getResId());
    assertEquals(agencyName, fetchedAgency.getName());
  }

  @Test
  void getTestWithRorId() {
    /*
     * Create a new FundingAgency
     */
    final String agencyAcronym = HederaTestConstants.getRandomNameWithTemporaryLabel("FA");
    final String agencyName = HederaTestConstants.getRandomNameWithTemporaryLabel("My FundingAgency");
    final String rorId = "04p405e02";
    FundingAgency newAgency = new FundingAgency();
    newAgency.setAcronym(agencyAcronym);
    newAgency.setName(agencyName);
    newAgency.setRorId(rorId);
    newAgency = this.fundingAgencyService.create(newAgency);

    /*
     * Fetch it from server and test it has been created
     */
    final FundingAgency fetchedAgency = this.fundingAgencyService.findOne(newAgency.getResId());
    assertNotNull(fetchedAgency);
    assertEquals(agencyAcronym, fetchedAgency.getAcronym());
    assertEquals(agencyName, fetchedAgency.getName());
    assertEquals(rorId, fetchedAgency.getRorId());

    // Get funding agency by ROR ID
    final FundingAgency rorAgency = this.fundingAgencyService.findByRorId(newAgency.getRorId());
    assertNotNull(rorAgency);
    assertEquals(agencyAcronym, rorAgency.getAcronym());
    assertEquals(agencyName, rorAgency.getName());
    assertEquals(rorId, rorAgency.getRorId());

    /*
     * Create a new FundingAgency with a ROR with prefix
     */
    final String agencyAcronym2 = HederaTestConstants.getRandomNameWithTemporaryLabel("FA2");
    final String agencyName2 = HederaTestConstants.getRandomNameWithTemporaryLabel("My FundingAgency2");
    final String shortRorId2 = "04tst0102";
    final String fullRorId2 = "https://ror.org/" + shortRorId2;
    FundingAgency newAgency2 = new FundingAgency();
    newAgency2.setAcronym(agencyAcronym2);
    newAgency2.setName(agencyName2);
    newAgency2.setRorId(fullRorId2);
    newAgency2 = this.fundingAgencyService.create(newAgency2);

    /*
     * Fetch it from server and test it has been created
     */
    final FundingAgency fetchedAgency2 = this.fundingAgencyService.findOne(newAgency2.getResId());
    assertNotNull(fetchedAgency2);
    assertEquals(agencyAcronym2, fetchedAgency2.getAcronym());
    assertEquals(agencyName2, fetchedAgency2.getName());
    assertEquals(shortRorId2, fetchedAgency2.getRorId());

    // Get funding agency by short ROR ID
    final FundingAgency rorAgency2 = this.fundingAgencyService.findByRorId(shortRorId2);
    assertNotNull(rorAgency2);
    assertEquals(agencyAcronym2, rorAgency2.getAcronym());
    assertEquals(agencyName2, rorAgency2.getName());
    assertEquals(shortRorId2, rorAgency2.getRorId());
  }

  @Test
  void testDelete() {

    final String agencyAcronym = HederaTestConstants.getRandomNameWithTemporaryLabel("FA");
    final String agencyName = HederaTestConstants.getRandomNameWithTemporaryLabel("My FundingAgency");

    /*
     * Create a new FundingAgency
     */
    FundingAgency newAgency = new FundingAgency();
    newAgency.setAcronym(agencyAcronym);
    newAgency.setName(agencyName);
    newAgency = this.fundingAgencyService.create(newAgency);

    /*
     * Check it has been created
     */
    final FundingAgency fetchedAgency = this.fundingAgencyService.findOne(newAgency.getResId());
    assertNotNull(fetchedAgency.getResId());

    /*
     * Delete it
     */
    this.fundingAgencyService.delete(fetchedAgency.getResId());

    /*
     * Fetching again returns nothing
     */
    final String newAgencyId = newAgency.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.fundingAgencyService.findOne(newAgencyId));
  }

  @Test
  void testUnicity() {

    final String agencyAcronym = HederaTestConstants.getRandomNameWithTemporaryLabel("FA");
    final String agencyName = HederaTestConstants.getRandomNameWithTemporaryLabel("My FundingAgency");

    // Creating a new agency
    FundingAgency fundingAgency1 = new FundingAgency();
    fundingAgency1.setAcronym(agencyAcronym);
    fundingAgency1.setName(agencyName);
    fundingAgency1 = this.fundingAgencyService.create(fundingAgency1);

    // Creating a second agency with the same name and acronym as the first one
    FundingAgency fundingAgency2 = new FundingAgency();
    fundingAgency2.setAcronym(fundingAgency1.getAcronym());
    fundingAgency2.setName(fundingAgency1.getName());

    // Testing unicity of the name and acronym
    try {
      this.fundingAgencyService.create(fundingAgency2);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Unicity constraint not satisfied");
    }
  }

  @Test
  void testUpdate() {

    final String agencyAcronym1 = HederaTestConstants.getRandomNameWithTemporaryLabel("FA1");
    final String agencyName1 = HederaTestConstants.getRandomNameWithTemporaryLabel("Funding Agency unit");
    final String agencyAcronym2 = HederaTestConstants.getRandomNameWithTemporaryLabel("FA2");
    final String agencyName2 = HederaTestConstants.getRandomNameWithTemporaryLabel("Funding Agency updated");

    FundingAgency newAgency = new FundingAgency();
    newAgency.setAcronym(agencyAcronym1);
    newAgency.setName(agencyName1);
    newAgency = this.fundingAgencyService.create(newAgency);

    /*
     * Checks saving succeeded
     */
    final FundingAgency fetchedAgency = this.fundingAgencyService.findOne(newAgency.getResId());
    assertEquals(fetchedAgency.getName(), agencyName1, "names are equal");

    /*
     * Does the update
     */
    fetchedAgency.setAcronym(agencyAcronym2);
    fetchedAgency.setName(agencyName2);
    this.fundingAgencyService.update(fetchedAgency.getResId(), fetchedAgency);

    /*
     * Checks update succeeded
     */
    final FundingAgency refetchedAgency = this.fundingAgencyService.findOne(fetchedAgency.getResId());
    assertEquals(refetchedAgency.getName(), agencyName2, "names are equal");
    assertEquals(refetchedAgency.getAcronym(), agencyAcronym2, "acronyms are equal");
  }

  @Test
  void uploadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an fundingAgency
     */
    final FundingAgency fundingAgency1 = this.createRemoteFundingAgency("Funding Agency #LOGO");

    /*
     * Add logo
     */
    FundingAgency fetchedFundingAgency = this.fundingAgencyService.uploadLogo(fundingAgency1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final FundingAgency refetchedFundingAgency = this.fundingAgencyService.findOne(fetchedFundingAgency.getResId());
    assertEquals(refetchedFundingAgency.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedFundingAgency.getResourceFile().getFileSize(), sizeLogo);
  }

  @Test
  void downloadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an fundingAgency
     */
    final FundingAgency fundingAgency1 = this.createRemoteFundingAgency("Funding Agency #LOGO");

    /*
     * Add logo
     */
    FundingAgency fundingAgency2 = this.fundingAgencyService.uploadLogo(fundingAgency1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final FundingAgency refetchedFundingAgency = this.fundingAgencyService.findOne(fundingAgency2.getResId());
    assertEquals(refetchedFundingAgency.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedFundingAgency.getResourceFile().getFileSize(), sizeLogo);

    Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.tmp");
    this.fundingAgencyService.downloadLogo(refetchedFundingAgency.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedFundingAgency.getResourceFile().getFileSize());
  }

  @Test
  void deleteLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an fundingAgency
     */
    final FundingAgency fundingAgency1 = this.createRemoteFundingAgency("Funding Agency #LOGO");

    /*
     * Add logo
     */
    FundingAgency fetchedFundingAgency = this.fundingAgencyService.uploadLogo(fundingAgency1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    FundingAgency refetchedFundingAgency = this.fundingAgencyService.findOne(fetchedFundingAgency.getResId());
    assertEquals(refetchedFundingAgency.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedFundingAgency.getResourceFile().getFileSize(), sizeLogo);

    this.fundingAgencyService.deleteLogo(fundingAgency1.getResId());
    refetchedFundingAgency = this.fundingAgencyService.findOne(fetchedFundingAgency.getResId());
    assertNull(refetchedFundingAgency.getResourceFile());
  }

  /*************************************************/

  @Override
  protected void deleteFixtures() {
    this.clearFundingAgencyFixtures();
  }

  private FundingAgency createRemoteFundingAgency(String description) {
    // Create a new FundingAgency
    FundingAgency fundingAgency = new FundingAgency();
    fundingAgency.setName(HederaTestConstants.getRandomNameWithTemporaryLabel(description));
    fundingAgency.setAcronym(HederaTestConstants.getRandomNameWithTemporaryLabel(description));
    fundingAgency = this.fundingAgencyService.create(fundingAgency);
    return fundingAgency;
  }
}
