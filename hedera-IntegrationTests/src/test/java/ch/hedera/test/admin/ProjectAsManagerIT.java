/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ProjectAsManagerIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.FundingAgencyITService;
import ch.hedera.test.service.InstitutionITService;
import ch.hedera.test.service.PersonITService;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.RmlITService;

@Tag(HederaTestConstants.AS_MANAGER_TAG)
class ProjectAsManagerIT extends AbstractProjectWithRoleIT {

  @Autowired
  public ProjectAsManagerIT(Environment env,
          SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          ProjectITService projectITService,
          InstitutionITService institutionITService,
          FundingAgencyITService fundingAgencyITService,
          PersonITService personITService,
          RmlITService rmlITService,
          ResearchObjectTypeITService researchObjectTypeITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService, projectITService, institutionITService,
            fundingAgencyITService, personITService, rmlITService, researchObjectTypeITService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @Override
  protected String role() {
    return Role.MANAGER_ID;
  }

  @Test
  void addAndRemoveInstitutionsTest() {
    this.addAndRemoveInstitutionsShouldSucceedTest();
  }

  @Test
  void addAndRemoveFundingAgencyTest() {
    this.addAndRemoveFundingAgencyShouldSucceedTest();
  }

  @Test
  void addAndRemoveRmlTest() throws IOException {
    this.addAndRemoveRmlShouldSucceedTest();
  }

  @Test
  void addAndRemoveResearchObjectTypeTest() throws IOException {
    this.addAndRemoveResearchObjectTypesShouldSucceedTest();
  }

  @Test
  void addAndRemovePersonTest() {
    this.addAndRemovePersonShouldSucceedTest();
  }

  @Test
  void downloadLogoTest() throws IOException {
    this.downloadLogoShouldSucceedTest();
  }

  @Test
  void uploadLogoTest() throws IOException {
    this.uploadLogoShouldSucceedTest();
  }

  @Test
  void deleteLogoTest() throws IOException {
    this.deleteLogoShouldSucceedTest();
  }

  @Test
  void closeTest() {
    /*
     * Get test project
     */
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());

    // Close
    final String resId = project.getResId();
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(StringTool.DATE_FORMAT);
    final String closingDate = LocalDate.now().plus(1, ChronoUnit.DAYS).format(formatter);

    Project project2 = this.projectITService.close(resId, closingDate);

    // Test close
    assertEquals(project2.getClosingDate().toString(), closingDate);

    this.projectITService.delete(project2.getResId());
  }

  @Test
  public void createProjectWithoutResearchDataFileResearchObjectTypeTest() {
    this.createProjectWithoutResearchDataFileResearchObjectTypeShouldFailTest();
  }
}
