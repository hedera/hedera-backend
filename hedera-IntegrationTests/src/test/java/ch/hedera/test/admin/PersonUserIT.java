/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - PersonUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SearchOperation;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.security.User;
import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.Person;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.service.admin.PersonClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.HederaTestConstants.ProjectStatus;
import ch.hedera.test.service.PersonITService;
import ch.hedera.test.service.ProjectITService;

class PersonUserIT extends AbstractPersonIT {

  private final PersonClientService personClientService;

  protected ProjectITService projectITService;

  @Autowired
  public PersonUserIT(Environment env, SudoRestClientTool restClientTool,
          PersonClientService personClientService,
          ProjectITService projectITService,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          PersonITService personITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService, personITService);
    this.personClientService = personClientService;
    this.projectITService = projectITService;
  }

  @Test
  void creationTest() {
    // Create a person
    Person person1 = this.createTemporaryPerson("Jean-Claude", "Dusse", false);
    person1 = this.personClientService.create(person1);
    final Person person2 = this.personClientService.findOne(person1.getResId());

    // Test the creation
    assertEquals(person1.getFirstName(), person2.getFirstName());
    assertEquals(person1.getLastName(), person2.getLastName());
    this.assertEqualsWithoutNanoSeconds(person1.getCreationTime(), person2.getCreationTime());
    assertNull(person2.getOrcid());
  }

  @Test
  void deleteTest() {
    // Create a person
    Person person1 = this.createTemporaryPerson("Jérôme", "Tarayre", false);
    person1 = this.personClientService.create(person1);

    // Delete the person
    try {
      this.personClientService.delete(person1.getResId());
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void institutionTest() {
    // Create a person
    Person person = this.createTemporaryPerson("Miguel", "Weissmuller", false);
    person = this.personClientService.create(person);

    // Create an institution
    Institution institution1 = new Institution();
    institution1.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("institution"));
    institution1.setDescription(HederaTestConstants.getRandomNameWithTemporaryLabel("description"));
    try {
      this.institutionService.create(institution1);
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void removeProjectFromPersonTest() {

    // Create a person
    Person person = this.createTemporaryPerson("Nathalie", "Morin", false);
    person = this.personClientService.create(person);

    this.projectITService.getOrCreatePermanentProjectAsManager(ProjectStatus.OPEN);
    try {
      this.personClientService.getProjects(person.getResId());
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void searchTest() {
    // Create a person
    Person person = new Person();
    person.setFirstName(HederaTestConstants.getRandomNameWithTemporaryLabel("SearchFirstName"));
    person.setLastName(HederaTestConstants.getRandomNameWithTemporaryLabel("SearchLastName"));
    person = this.personClientService.create(person);

    /*
     * Test case sensitive research REM: as the database stores firstname and lastname in
     * utf8_general_ci, the searchTerm finds results even with wrong case
     */
    String searchTerm = "archfirs"; // substring of firstname
    assertTrue(this.found(String.format("firstName~%s, lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));
    searchTerm = "archlas"; // substring of lastname
    assertTrue(this.found(String.format("firstName~%s, lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));

    /*
     * Test case insensitive research
     */
    searchTerm = "archfirs"; // substring of firstname
    assertTrue(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));
    searchTerm = "archlas"; // substring of lastname
    assertTrue(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));

    /*
     * Search on both firstName AND lastName (by default on backend)
     */
    assertFalse(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), null, person));

    /*
     * search on not existing term
     */
    searchTerm = "--notexisting--";
    assertFalse(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));
  }

  @Test
  void updateTest() {
    // Create a person
    Person person1 = this.createTemporaryPerson("Jérôme", "Tarayre", false);
    person1 = this.personClientService.create(person1);

    // Update the person
    final Person person2 = this.createTemporaryPerson("Robert", "Lespinass", true);
    try {
      this.personClientService.update(person1.getResId(), person2);
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void uploadLogo() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Find the person linked to the user connected
     */
    User myUser = this.userService.getAuthenticatedUser();

    Person myPerson = this.getLinkedPersonToUser(myUser);

    final Person fetchedPerson = this.personClientService.uploadAvatar(myPerson.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Person refetchedPerson = this.personClientService.findOne(fetchedPerson.getResId());
    assertEquals(refetchedPerson.getAvatar().getFileName(), logoName);
    assertEquals(refetchedPerson.getAvatar().getFileSize(), sizeLogo);
  }

  @Test
  void uploadLogoToThirdPartyUser() {
    assertThrows(HttpClientErrorException.Forbidden.class, () -> {
      final String logoName = "geneve.jpg";
      final ClassPathResource logoToUpload = new ClassPathResource(logoName);
      final User thirdPartyUser = this.findThirdPartyUser();

      this.personClientService.uploadAvatar(thirdPartyUser.getPerson().getResId(), logoToUpload);
    });
  }

  @Test
  void downloadLogo() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();
    /*
     * Find the person linked to the user connected
     */
    User myUser = this.userService.getAuthenticatedUser();

    Person myPerson = this.getLinkedPersonToUser(myUser);

    final Person fetchedPerson = this.personClientService.uploadAvatar(myPerson.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Person refetchedPerson = this.personClientService.findOne(fetchedPerson.getResId());
    assertEquals(refetchedPerson.getAvatar().getFileName(), logoName);
    assertEquals(refetchedPerson.getAvatar().getFileSize(), sizeLogo);

    Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.tmp");
    this.personClientService.downloadAvatar(refetchedPerson.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedPerson.getAvatar().getFileSize());
  }

  @Test
  void downloadLogoToThirdPartyUser() {
    final User thirdPartyUser = this.findThirdPartyUser();
    Person thirdPartyPerson = this.getLinkedPersonToUser(thirdPartyUser);

    try {
      Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.tmp");
      this.personClientService.downloadAvatar(thirdPartyPerson.getResId(), path);
      assertNotNull(path.toFile());
      assertEquals(Long.valueOf(path.toFile().length()), thirdPartyPerson.getAvatar().getFileSize());
    } catch (final HttpClientErrorException.NotFound e) {
      assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
    }
  }

  @Test
  void deleteLogo() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    User myUser = this.userService.getAuthenticatedUser();
    Person myPerson = this.getLinkedPersonToUser(myUser);

    this.personClientService.uploadAvatar(myPerson.getResId(), logoToUpload);
    assertNotNull(myPerson.getAvatar());
    assertEquals(myPerson.getAvatar().getFileName(), logoName);
    assertEquals(myPerson.getAvatar().getFileSize(), sizeLogo);
    this.personClientService.deleteAvatar(myPerson.getResId());

    Person person = this.personClientService.findOne(myPerson.getResId());
    assertNull(person.getAvatar());
  }

  @Test
  void deleteLogoToThirdPartyUser() {
    assertThrows(HttpClientErrorException.Forbidden.class, () -> {
      User thirdPartyUser = this.findThirdPartyUser();
      Person thirdPartyPerson = this.getLinkedPersonToUser(thirdPartyUser);

      this.personClientService.deleteAvatar(thirdPartyPerson.getResId());
    });
  }

  @Test
  void getTestWithOrcid() {
    // Create a person
    Person person1 = this.createTemporaryPerson("Bernard", "Morin", true);
    person1 = this.personClientService.create(person1);
    final Person person2 = this.personClientService.findOne(person1.getResId());

    // Test the creation
    assertEquals(person1.getFirstName(), person2.getFirstName());
    assertEquals(person1.getLastName(), person2.getLastName());
    this.assertEqualsWithoutNanoSeconds(person1.getCreationTime(), person2.getCreationTime());
    assertEquals(person1.getOrcid(), person2.getOrcid());

    // Get Person by ORCID
    Person person3 = this.personClientService.findByOrcid(person1.getOrcid());
    // Test the get
    assertEquals(person1.getFirstName(), person3.getFirstName());
    assertEquals(person1.getLastName(), person3.getLastName());
    this.assertEqualsWithoutNanoSeconds(person1.getCreationTime(), person3.getCreationTime());
    assertEquals(person1.getOrcid(), person3.getOrcid());
  }

  @Override
  protected void deleteFixtures() {
    // Do nothing. Delegates to CleanIT
  }

  private Person createTemporaryPerson(String firstName, String lastName, boolean orcid) {
    final Person person = new Person();
    person.setFirstName(HederaTestConstants.getRandomNameWithTemporaryLabel(firstName));
    person.setLastName(HederaTestConstants.getRandomNameWithTemporaryLabel(lastName));
    if (orcid) {
      person.setOrcid(HederaTestConstants.getRandomOrcId());
    }
    return person;
  }

  private boolean found(String search, String matchType, Person personToFound) {
    final List<Person> people = this.personClientService.search(search, matchType);
    for (final Person p : people) {
      if (p.getResId().equals(personToFound.getResId())) {
        return true;
      }
    }
    return false;
  }

}
