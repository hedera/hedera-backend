/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - IndexFieldAliasAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;

class IndexFieldAliasAsUserIT extends AbstractAdminIT {

  @Autowired
  public IndexFieldAliasAsUserIT(Environment env, SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
  }

  @Test
  void creationTest() {
    IndexFieldAlias indexFieldAlias = this.getBasicIndexFieldAlias();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.indexFieldAliasService.create(indexFieldAlias));
  }

  @Test
  void findAllTest() {
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.indexFieldAliasService.findAll());
  }

  private IndexFieldAlias getBasicIndexFieldAlias() {
    IndexFieldAlias indexFieldAlias = new IndexFieldAlias();
    indexFieldAlias.setIndexName(HederaTestConstants.TEST_RES_ID);
    indexFieldAlias.setAlias(HederaTestConstants.getRandomNameWithTemporaryLabel("alias"));
    indexFieldAlias.setField(HederaTestConstants.getRandomNameWithTemporaryLabel("path.to.value"));
    return indexFieldAlias;
  }

  @Override
  protected void deleteFixtures() {
  }
}
