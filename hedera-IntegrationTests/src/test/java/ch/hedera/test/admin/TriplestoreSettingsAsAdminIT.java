/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - TriplestoreSettingsAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.triplestore.DatasetSettings;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.TriplestoreSettingsITService;

@Tag(HederaTestConstants.AS_ADMIN_TAG)
class TriplestoreSettingsAsAdminIT extends TriplestoreSettingsIT {

  @Autowired
  public TriplestoreSettingsAsAdminIT(Environment env, SudoRestClientTool restClientTool, FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService, GlobalBannerClientService globalBannerService,
          TriplestoreSettingsITService triplestoreSettingsITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService, triplestoreSettingsITService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void createAndGetDataset() {
    String datasetName = this.triplestoreSettingsITService.getTemporaryDatasetName("test dataset");
    DatasetSettings datasetSettings = this.triplestoreSettingsITService.create(datasetName);
    assertNotNull(datasetSettings);
    assertEquals(datasetName, datasetSettings.getResId());

    DatasetSettings fetchedDatasetSettings = this.triplestoreSettingsITService.findOne(datasetName);
    assertEquals(datasetName, fetchedDatasetSettings.getResId());
  }

  @Test
  void listDatasets() {

    List<DatasetSettings> datasetSettingsList = this.triplestoreSettingsITService.findAll();
    final String nameStartWith = HederaTestConstants.TEMPORARY_TEST_DATASET_LABEL;
    final long startCount = datasetSettingsList.stream().filter(di -> di.getResId().startsWith(nameStartWith))
            .count();

    String datasetName1 = this.triplestoreSettingsITService.getTemporaryDatasetName("test dataset");
    this.triplestoreSettingsITService.create(datasetName1);

    String datasetName2 = this.triplestoreSettingsITService.getTemporaryDatasetName("test dataset");
    this.triplestoreSettingsITService.create(datasetName2);

    datasetSettingsList = this.triplestoreSettingsITService.findAll();
    assertEquals(startCount + 2,
            datasetSettingsList.stream().filter(di -> di.getResId().startsWith(nameStartWith)).count());
  }
}
