/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - PersonIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SearchOperation;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.service.admin.PersonClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.HederaTestConstants.ProjectStatus;
import ch.hedera.test.service.PersonITService;
import ch.hedera.test.service.ProjectITService;

class PersonIT extends AbstractPersonIT {

  private final PersonClientService personClientService;

  protected ProjectITService projectITService;

  @Autowired
  public PersonIT(Environment env, SudoRestClientTool restClientTool,
          PersonClientService personClientService,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          ProjectITService projectITService,
          PersonITService personITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService, personITService);
    this.personClientService = personClientService;
    this.projectITService = projectITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    // Create a person
    Person person1 = this.createPerson("Jean-Claude", "Dusse", false);
    person1 = this.personITService.create(person1);
    final Person person2 = this.personITService.findOne(person1.getResId());

    // Test the creation
    assertEquals(person1.getFirstName(), person2.getFirstName());
    assertEquals(person1.getLastName(), person2.getLastName());
    this.assertEqualsWithoutNanoSeconds(person1.getCreationTime(), person2.getCreationTime());
    assertNull(person2.getOrcid());
  }

  @Test
  void creationTestEmptyOrcid() {
    Person person1 = this.createPerson("Gilbert", "Seldman", false);
    person1.setOrcid("");
    Person person2 = this.personITService.create(person1);
    assertEquals(person1.getFirstName(), person2.getFirstName(), "First name not set correctly");
    assertEquals(person1.getLastName(), person2.getLastName(), "Last name not set correctly");
    assertEquals(person1.getOrcid(), person2.getOrcid(), "Orcid not set correctly");
  }

  @Test
  void creationTestNullOrcid() {
    Person person1 = this.createPerson("Marius", "Seldman", false);
    person1.setOrcid(null);
    Person person2 = this.personITService.create(person1);
    assertEquals(person1.getFirstName(), person2.getFirstName(), "First name not set correctly");
    assertEquals(person1.getLastName(), person2.getLastName(), "Last name not set correctly");
    assertEquals(person1.getOrcid(), person2.getOrcid(), "Orcid not set correctly");
  }

  @Test
  void creationTestUniqueOrcid() {
    // Create a person
    Person person1 = this.createPerson("Gilbert", "Seldman", true);
    final String orcid = person1.getOrcid();
    person1 = this.personITService.create(person1);

    Person person2 = this.createPerson("Marius", "Franceschini", false);
    person2.setOrcid(orcid);
    try {
      this.personITService.create(person2);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Unicity constraint not satisfied");
    }
  }

  @Test
  void creationTestWithEmptyName() {
    // Create a person
    Person person1 = this.createPerson("Jean-Claude", "Dusse", false);
    person1.setFirstName("");
    try {
      this.personITService.create(person1);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Test
  void creationTestWithOrcid() {
    // Create a person
    Person person1 = this.createPerson("Bernard", "Morin", true);
    person1 = this.personITService.create(person1);
    final Person person2 = this.personITService.findOne(person1.getResId());

    // Test the creation
    assertEquals(person1.getFirstName(), person2.getFirstName());
    assertEquals(person1.getLastName(), person2.getLastName());
    this.assertEqualsWithoutNanoSeconds(person1.getCreationTime(), person2.getCreationTime());
    assertEquals(person1.getOrcid(), person2.getOrcid());
  }

  @Test
  void creationTestWithOrcidX() {
    // Create a person
    Person person1 = this.createPerson("Bernard", "Morin", true);
    person1.setOrcid(person1.getOrcid().substring(0, person1.getOrcid().length() - 1) + "X");
    person1 = this.personITService.create(person1);
    final Person person2 = this.personITService.findOne(person1.getResId());

    // Test the creation
    assertEquals(person1.getFirstName(), person2.getFirstName());
    assertEquals(person1.getLastName(), person2.getLastName());
    this.assertEqualsWithoutNanoSeconds(person1.getCreationTime(), person2.getCreationTime());
    assertEquals(person1.getOrcid(), person2.getOrcid());
  }

  @Test
  void getTestWithOrcid() {
    // Create a person
    Person person1 = this.createPerson("Bernard", "Morin", true);
    person1 = this.personITService.create(person1);
    final Person person2 = this.personITService.findOne(person1.getResId());

    // Test the creation
    assertEquals(person1.getFirstName(), person2.getFirstName());
    assertEquals(person1.getLastName(), person2.getLastName());
    this.assertEqualsWithoutNanoSeconds(person1.getCreationTime(), person2.getCreationTime());
    assertEquals(person1.getOrcid(), person2.getOrcid());

    // Get Person by ORCID
    Person person3 = this.personITService.findByOrcid(person1.getOrcid());
    // Test the get
    assertEquals(person1.getFirstName(), person3.getFirstName());
    assertEquals(person1.getLastName(), person3.getLastName());
    this.assertEqualsWithoutNanoSeconds(person1.getCreationTime(), person3.getCreationTime());
    assertEquals(person1.getOrcid(), person3.getOrcid());
  }

  @Test
  void creationTestWithWrongOrcid() {
    // Create a person
    Person person1 = this.createPerson("Gisèle", "André", false);
    person1.setOrcid("1234");
    try {
      this.personITService.create(person1);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Test
  void deleteListTest() {
    // Create persons
    final int personNumber = 10;
    for (int i = 0; i < personNumber; i++) {
      this.personITService.create(this.createPerson("Jean-Claude " + i, "Dusse", false));
    }
    // Search Persons
    final List<Person> personList = this.personITService.searchByProperties(Map.of("lastName", "Dusse"));
    assertEquals(personList.size(), personNumber);

    // Delete Persons
    final String[] ids = new String[personNumber];
    int i = 0;
    for (final Person person : personList) {
      ids[i] = person.getResId();
      i++;
    }
    this.personITService.deleteList(ids);

    // Check if person deleted
    for (final Person person : personList) {
      assertThrows(HttpClientErrorException.NotFound.class, () -> this.personITService.findOne(person.getResId()), "Person not deleted");
    }
  }

  @Test
  void deleteTest() {
    // Create a person
    Person person1 = this.createPerson("Jérôme", "Tarayre", false);
    person1 = this.personITService.create(person1);

    // Delete Person
    this.personITService.delete(person1.getResId());

    // Check if person deleted
    final String person1Id = person1.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.personITService.findOne(person1Id), "Person not deleted");
  }

  @Test
  void institutionTest() {
    // Create a person
    Person person = this.createPerson("Miguel", "Weissmuller", false);
    person = this.personITService.create(person);

    // Create an institution
    Institution institution1 = new Institution();
    final String institutionName = HederaTestConstants.getRandomNameWithTemporaryLabel("institution");
    institution1.setName(institutionName);
    institution1.setDescription(HederaTestConstants.getRandomNameWithTemporaryLabel("description"));
    institution1 = this.institutionService.create(institution1);

    // Add the person to the institution
    this.personITService.addInstitution(person.getResId(), institution1.getResId());
    final List<Institution> listInstitutions = this.personITService.getInstitutions(person.getResId());
    final Optional<Institution> optionalInstitution = listInstitutions.stream().filter(ins -> ins.getName().equals(institutionName)).findFirst();
    assertTrue(optionalInstitution.isPresent());

    // Test the institution
    assertEquals(institution1.getResId(), optionalInstitution.get().getResId());
    assertEquals(institution1.getName(), optionalInstitution.get().getName());
    assertEquals(institution1.getDescription(), optionalInstitution.get().getDescription());
  }

  @Test
  void removeProjectFromPersonWithDefaultRoleTest() {

    // Create a person
    Person person = this.createPerson("Nathalie", "Morin", false);
    person = this.personITService.create(person);

    // Create a project
    final Project project = this.projectITService.getOrCreatePermanentProjectAsManager(ProjectStatus.OPEN);
    List<Project> projects = this.personITService.getProjects(person.getResId());
    final int sizeBeforeAddingPerson = projects.size();

    // Add the person to the project with default role
    this.personITService.addProject(person.getResId(), project.getResId());
    projects = this.personITService.getProjects(person.getResId());

    assertEquals(sizeBeforeAddingPerson + 1, projects.size());

    final Project expectedProject = projects.get(0);

    // Test added person on the projects
    assertEquals(project.getResId(), expectedProject.getResId());
    assertEquals(project.getDescription(), expectedProject.getDescription());

    // Test remove project from the person
    this.personITService.removeProject(person.getResId(), expectedProject.getResId());
    projects = this.personITService.getProjects(person.getResId());
    assertTrue(projects.isEmpty());
  }

  @Test
  void removeProjectFromPersonTest() {

    // Create a person
    Person person = this.createPerson("Nathalie", "Morin", false);
    person = this.personITService.create(person);

    // Create a project
    final Project project = this.projectITService.getOrCreatePermanentProjectAsManager(ProjectStatus.OPEN);
    List<Project> projects = this.personITService.getProjects(person.getResId());
    final int sizeBeforeAddingPerson = projects.size();

    // Add the person to the projects with others roles
    this.personITService.addProject(person.getResId(), project.getResId(), Role.CREATOR.getResId());
    projects = this.personITService.getProjects(person.getResId());
    assertEquals(sizeBeforeAddingPerson + 1, projects.size());

    final Project expectedProject = projects.get(0);

    // Test added person on the project
    assertEquals(project.getResId(), expectedProject.getResId());
    assertEquals(project.getDescription(), expectedProject.getDescription());

    // Test remove a role from the person in project
    this.personITService.removeProject(person.getResId(), expectedProject.getResId(), Role.CREATOR.getResId());
    projects = this.personITService.getProjects(person.getResId());
    assertTrue(projects.isEmpty());

  }

  @Test
  void searchTest() {
    // Create a person
    Person person = new Person();
    person.setFirstName(HederaTestConstants.getRandomNameWithTemporaryLabel("SearchFirstName"));
    person.setLastName(HederaTestConstants.getRandomNameWithTemporaryLabel("SearchLastName"));
    person = this.personITService.create(person);

    /*
     * Test case sensitive research REM: as the database stores firstname and lastname in
     * utf8_general_ci, the searchTerm finds results even with wrong case
     */
    String searchTerm = "archfirs"; // substring of firstname
    assertTrue(this.found(String.format("firstName~%s, lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));
    searchTerm = "archlas"; // substring of lastname
    assertTrue(this.found(String.format("firstName~%s, lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));

    /*
     * Test case insensitive research
     */
    searchTerm = "archfirs"; // substring of firstname
    assertTrue(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));
    searchTerm = "archlas"; // substring of lastname
    assertTrue(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));

    /*
     * Search on both firstName AND lastName (by default on backend)
     */
    assertFalse(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), null, person));

    /*
     * search on not existing term
     */
    searchTerm = "--notexisting--";
    assertFalse(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));
  }

  @Test
  void updateTest() {
    // Create a person
    Person person1 = this.createPerson("Jérôme", "Tarayre", false);
    person1 = this.personITService.create(person1);

    // Update the person
    final Person person2 = this.createPerson("Robert", "Lespinass", true);
    final Person person3 = this.personITService.update(person1.getResId(), person2);

    // Test the update
    assertEquals(person2.getFirstName(), person3.getFirstName());
    assertEquals(person2.getLastName(), person3.getLastName());
    assertEquals(person2.getOrcid(), person3.getOrcid());
  }

  @Override
  protected void deleteFixtures() {
    // Do nothing. Delegates to CleanIT
  }

  private Person createPerson(String firstName, String lastName, boolean hasOrcid) {
    final Person person = new Person();
    person.setFirstName(HederaTestConstants.getRandomNameWithTemporaryLabel(firstName));
    person.setLastName(HederaTestConstants.getRandomNameWithTemporaryLabel(lastName));
    if (hasOrcid) {
      person.setOrcid(HederaTestConstants.getRandomOrcId());
    }
    return person;
  }

  private boolean found(String search, String matchType, Person personToFound) {
    final List<Person> people = this.personITService.search(search, matchType);
    for (final Person p : people) {
      if (p.getResId().equals(personToFound.getResId())) {
        return true;
      }
    }
    return false;
  }
}
