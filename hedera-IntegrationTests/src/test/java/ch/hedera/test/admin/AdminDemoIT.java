/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AdminDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.admin;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.settings.FundingAgency;
import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.License;
import ch.hedera.model.settings.Person;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.service.PersonITService;

@Order(1)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Disabled("Run this manually")
class AdminDemoIT extends AbstractAdminIT {
  private static final Logger log = LoggerFactory.getLogger(AdminDemoIT.class);

  private PersonITService personITService;

  private final String LICENSE_FOLDER = "license";
  private final String FUNDING_AGENCY_FOLDER = "fundingAgency";
  private final String INSTITUTION_FOLDER = "institution";
  private final String PERSON_FOLDER = "person";

  private final String PNG_FILES = "*.png";

  public final static String[] ORCID_LIST = {
          "0391-2165-8328-7734",
          "1018-8125-5186-0023",
          "0880-4342-1494-606X"
  };

  public AdminDemoIT(Environment env, SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          PersonITService personITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
    this.personITService = personITService;
  }

  @Order(20)
  @Test
  void addLicenseLogo() throws IOException {
    for (Resource file : this.listFiles(this.LICENSE_FOLDER, this.PNG_FILES)) {
      String spdxId = this.getFilename(file);
      log.info("License '{}'", spdxId);
      // Get License by SPDX ID
      License license = this.licenseService.findBySpdxId(spdxId);
      assertNotNull(license, "License not found: " + spdxId);
      this.licenseService.uploadLogo(license.getResId(), file);
    }
  }

  @Order(30)
  @Test
  void addFundingAgencyRorId() {
    for (Entry<String, String> rorInfo : this.getFundingAgencyRorIdList().entrySet()) {
      log.info("Funding agency '{}'", rorInfo.getKey());
      FundingAgency fa = this.fundingAgencyService.findByAcronym(rorInfo.getKey());
      assertNotNull(fa, "Funding agency not found: " + rorInfo.getKey());
      if (StringTool.isNullOrEmpty(fa.getRorId())) {
        fa.setRorId(rorInfo.getValue());
        this.fundingAgencyService.update(fa.getResId(), fa);
      } else {
        log.warn("Funding agency  '{}' not updated", rorInfo.getKey());
      }
    }
  }

  @Order(31)
  @Test
  void addFundingAgencyLogo() throws IOException {
    for (Resource file : this.listFiles(this.FUNDING_AGENCY_FOLDER, this.PNG_FILES)) {
      String rorId = this.getFilename(file);
      log.info("Funding agency '{}'", rorId);
      FundingAgency fa = this.fundingAgencyService.findByRorId(rorId);
      if (fa == null) {
        fa = this.fundingAgencyService.findByAcronym(rorId);
      }
      assertNotNull(fa, "Funding agency not found: " + rorId);
      this.fundingAgencyService.uploadLogo(fa.getResId(), file);
    }
  }

  @Order(40)
  @Test
  void addInstitutionRorId() {
    for (Entry<String, String> rorInfo : this.getInstitutionRorIdList().entrySet()) {
      log.info("Institution '{}'", rorInfo.getKey());
      Institution institution = this.institutionService.findByName(rorInfo.getKey());
      assertNotNull(institution, "Institution not found: " + rorInfo.getKey());
      if (StringTool.isNullOrEmpty(institution.getRorId())) {
        institution.setRorId(rorInfo.getValue());
        this.institutionService.update(institution.getResId(), institution);
      } else {
        log.warn("Institution '{}' not updated", rorInfo.getKey());
      }
    }
  }

  @Order(41)
  @Test
  void addInstitutionLogo() throws IOException {
    for (Resource file : this.listFiles(this.INSTITUTION_FOLDER, this.PNG_FILES)) {
      String rorId = this.getFilename(file);
      log.info("Institution '{}'", rorId);
      Institution institution = this.institutionService.findByRorId(rorId);
      if (institution == null) {
        institution = this.institutionService.findByName(rorId);
      }
      assertNotNull(institution, "Institution not found: " + rorId);
      this.institutionService.uploadLogo(institution.getResId(), file);
    }
  }

  @Order(50)
  @Test
  void createResearchers() throws IOException {
    for (String orcid : ORCID_LIST) {
      if (this.personITService.findByOrcid(orcid) == null) {
        Person researcher = this.personITService.create(this.getResearcher(orcid));
        for (Institution institution : this.getResearcherInstitutions(orcid)) {
          this.personITService.addInstitution(researcher.getResId(), institution.getResId());
        }
      }
    }
    this.addPersonAvatar();
  }

  void addPersonAvatar() throws IOException {
    for (Resource file : this.listFiles(this.PERSON_FOLDER, this.PNG_FILES)) {
      String orcid = this.getFilename(file);
      log.info("Person '{}'", orcid);
      Person person = this.personITService.findByOrcid(orcid);
      assertNotNull(person, "Person not found: " + orcid);
      this.personITService.uploadAvatar(person.getResId(), file);
    }
  }

  private Map<String, String> getFundingAgencyRorIdList() {
    return Map.of(
            "SNF", "00yjd3n13",
            "CTI", "012y9zp98",
            "swissuniversities", "02fsagg23");
  }

  private Map<String, String> getInstitutionRorIdList() {
    Map<String, String> list = new HashMap<>();
    list.put("UNIGE", "01swzsf04");
    list.put("EPFL", "02s376052");
    list.put("ETH", "05a28rw58");
    list.put("UZH", "02crff812");
    list.put("HES-SO", "01xkakk17");
    list.put("ZHAW", "05pmsvm27");
    list.put("HUG", "01m1pv723");
    list.put("IHEID", "007ygn379");
    list.put("UNIL", "019whta54s");
    list.put("CERN", "01ggx4157");
    list.put("UNIFR", "022fs9h90");
    list.put("BFH", "02bnkt322");
    return list;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Override
  protected void deleteFixtures() {
    // do nothing
  }

  private String getFilename(Resource file) {
    return file.getFilename().substring(0, file.getFilename().lastIndexOf("."));
  }

  private Resource[] listFiles(String folder, String filePattern) throws IOException {
    final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
    return resolver.getResources("classpath*:/" + folder + "/" + filePattern);
  }

  private List<Institution> getResearcherInstitutions(String orcid) {
    final List<Institution> institutions = new ArrayList<>();
    if (orcid.equals(ORCID_LIST[0])) {
      // Jean Piaget
      institutions.add(this.institutionService.findByName("UNIGE"));
    } else if (orcid.equals(ORCID_LIST[1])) {
      // Tim Berners-Lee
      institutions.add(this.institutionService.findByName("SWITCHedu-ID"));
      institutions.add(this.institutionService.findByName("CERN"));
    } else if (orcid.equals(ORCID_LIST[2])) {
      // Albert Eisntien
      institutions.add(this.institutionService.findByName("ETH"));
    }

    return institutions;
  }

  private Person getResearcher(String orcid) {
    if (orcid.equals(ORCID_LIST[0])) {
      return this.createPerson("Jean", "Piaget", orcid);
    } else if (orcid.equals(ORCID_LIST[1])) {
      return this.createPerson("Tim", "Berners-Lee", orcid);
    } else if (orcid.equals(ORCID_LIST[2])) {
      return this.createPerson("Albert", "Einstein", orcid);
    }
    return null;
  }

  private Person createPerson(String firstName, String lastName, String orcid) {
    final Person person = new Person();
    person.setFirstName(firstName);
    person.setLastName(lastName);
    if (!StringTool.isNullOrEmpty(orcid)) {
      person.setOrcid(orcid);
    }
    return person;
  }
}
