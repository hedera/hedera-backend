/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AdminDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.demo;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.EnabledIf;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.model.settings.FundingAgency;
import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.License;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.service.admin.LicenseClientService;
import ch.hedera.service.admin.OntologyClientService;
import ch.hedera.service.admin.ResearchObjectTypeClientService;
import ch.hedera.test.service.ProjectITService;

@Order(1)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@EnabledIf("${hedera.demo-config.enabled:false}")
class AdminDemoIT extends AbstractDemoIT {

  private static final Logger log = LoggerFactory.getLogger(AdminDemoIT.class);

  private final static String GYPSO = "Gypso";
  private final static String POESIE_16 = "Poésie du XVIe siècle";
  private final static String ROMA_16 = "Musique du XVIe siècle";
  private final static String ARCHEO = "Archéologie classique";
  private final static String HISTARTDIA = "Diathèque de l'histoire de l'art";
  private final static String NEOTROPICAL_FISH = "Poissons néotropicaux";
  private final static String TURRETTINI = "Turrettini";

  private final static String LICENSE_FOLDER = "license";
  private final static String FUNDING_AGENCY_FOLDER = "fundingAgency";
  private final static String INSTITUTION_FOLDER = "institution";
  private final static String ONTOLOGY_FOLDER = "ontology";

  protected final FundingAgencyClientService fundingAgencyService;
  protected final InstitutionClientService institutionService;
  protected final LicenseClientService licenseService;
  protected final OntologyClientService ontologyClientService;
  protected final ResearchObjectTypeClientService researchObjectTypeService;

  @Autowired
  public AdminDemoIT(Environment env,
          SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          LicenseClientService licenseService,
          OntologyClientService ontologyClientService,
          ResearchObjectTypeClientService researchObjectTypeService,
          ProjectITService projectITService) {
    super(env, restClientTool, projectITService);
    this.fundingAgencyService = fundingAgencyService;
    this.institutionService = institutionService;
    this.licenseService = licenseService;
    this.ontologyClientService = ontologyClientService;
    this.researchObjectTypeService = researchObjectTypeService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Order(10)
  @Test
  void createSwissInstitutions() {
    // Institutions
    assertNotNull(this.createInstitution("UNIGE", "Université de Genève", "https://www.unige.ch", "01swzsf04", "unige.ch"));
    assertNotNull(this.createInstitution("EPFL", "Ecole Polytechnique Fédérale de Lausanne", "https://www.epfl.ch", "02s376052", "epfl.ch"));
    assertNotNull(this.createInstitution("ETH", "ETH Zürich", "https://www.ethz.ch", "05a28rw58", "ethz.ch"));
    assertNotNull(this.createInstitution("UZH", "University of Zurich", "https://www.uzh.ch", "02crff812", "uzh.ch"));
    assertNotNull(
            this.createInstitution("HES-SO", "Haute Ecole Spécialisée de Suisse Occidentale", "https://www.hes-so.ch", "01xkakk17", "hesge.ch",
                    "hes-so.ch"));
    assertNotNull(this.createInstitution("ZHAW", "Zurich University of Applied Sciences", "https://www.zhaw.ch", "05pmsvm27", "zhaw.ch"));
    assertNotNull(this.createInstitution("HUG", "Hôpitaux Universitaires de Genève", "https://www.hug-ge.ch", "01m1pv723", "hcuge.ch"));
    assertNotNull(
            this.createInstitution("IHEID", "Graduate Institute of International & Development Studies", "https://www.graduateinstitute.ch",
                    "007ygn379", "graduateinstitute.ch"));
    assertNotNull(this.createInstitution("SWITCHedu-ID", "SWITCH edu-ID", "https://www.switch.ch/edu-id", null, "eduid.ch"));
    assertNotNull(this.createInstitution("UNIL", "Université de Lausanne", "https://www.unil.ch", "019whta54", "unil.ch"));
    assertNotNull(this.createInstitution("CERN", "European Organization for Nuclear Research", "https://home.cern", "01ggx4157", "cern.ch"));
    assertNotNull(this.createInstitution("UNIFR", "Université de Fribourg", "https://www.unifr.ch", "022fs9h90", "unifr.ch"));
    assertNotNull(this.createInstitution("BFH", "Bern University of Applied Sciences", "https://www.bfh.ch", "02bnkt322", "bfh.ch"));
    // Funding Agencies
    assertNotNull(this.createFundingAgency("swissuniversities", "swissuniversities", "https://www.swissuniversities.ch", "02fsagg23"));
    assertNotNull(this.createFundingAgency("SNF", "Swiss National Science Foundation", "https://www.snf.ch", "00yjd3n13"));
    assertNotNull(this.createFundingAgency("CTI", "Commission for Technology and Innovation", "https://www.kti.admin.ch/", "012y9zp98"));
  }

  @Order(20)
  @Test
  void addLicenseLogo() throws IOException {
    for (Resource file : this.listFiles(LICENSE_FOLDER, "/", PNG_FILES)) {
      String spdxId = this.getFilename(file);
      log.info("Add license logo '{}'", spdxId);
      // Get License by SPDX ID
      License license = this.licenseService.findBySpdxId(spdxId);
      assertNotNull(license, "License not found: " + spdxId);
      this.licenseService.uploadLogo(license.getResId(), file);
    }
  }

  @Order(30)
  @Test
  void addFundingAgencyRorId() {
    for (Entry<String, String> rorInfo : this.getFundingAgencyRorIdList().entrySet()) {
      FundingAgency fa = null;
      try {
        fa = this.fundingAgencyService.findByAcronym(rorInfo.getKey());
      } catch (HttpClientErrorException e) {
        if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
          throw e;
        }
        assertNotNull(fa, "Funding agency not found: " + rorInfo.getKey());
      }
      if (StringTool.isNullOrEmpty(fa.getRorId())) {
        log.info("Adding funding agency ROR ID '{}'", rorInfo.getKey());
        fa.setRorId(rorInfo.getValue());
        this.fundingAgencyService.update(fa.getResId(), fa);
      } else {
        log.warn("Funding agency '{}' ROR ID not updated", rorInfo.getKey());
      }
    }
  }

  @Order(31)
  @Test
  void addFundingAgencyLogo() throws IOException {
    for (Resource file : this.listFiles(FUNDING_AGENCY_FOLDER, "/", PNG_FILES)) {
      String rorId = this.getFilename(file);
      log.info("Add funding agency logo '{}'", rorId);
      FundingAgency fa = null;
      try {
        fa = this.fundingAgencyService.findByRorId(rorId);
      } catch (HttpClientErrorException e) {
        if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
          throw e;
        }
        fa = this.fundingAgencyService.findByAcronym(rorId);
      }
      assertNotNull(this.fundingAgencyService.uploadLogo(fa.getResId(), file));
    }
  }

  @Order(40)
  @Test
  void addInstitutionRorId() {
    for (Entry<String, String> rorInfo : this.getInstitutionRorIdList().entrySet()) {
      Institution institution = null;
      try {
        institution = this.institutionService.findByName(rorInfo.getKey());
      } catch (HttpClientErrorException e) {
        if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
          throw e;
        }
        assertNotNull(institution, "Institution not found: " + rorInfo.getKey());
      }
      if (StringTool.isNullOrEmpty(institution.getRorId())) {
        log.info("Adding institution ROR ID '{}'", rorInfo.getKey());
        institution.setRorId(rorInfo.getValue());
        this.institutionService.update(institution.getResId(), institution);
      } else {
        log.warn("Institution '{}' ROR ID not updated", rorInfo.getKey());
      }
    }
  }

  @Order(41)
  @Test
  void addInstitutionLogo() throws IOException {
    for (Resource file : this.listFiles(INSTITUTION_FOLDER, "/", PNG_FILES)) {
      String rorId = this.getFilename(file);
      Institution institution = null;
      log.info("Adding institution logo '{}'", rorId);
      try {
        institution = this.institutionService.findByRorId(rorId);
      } catch (HttpClientErrorException e) {
        if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
          throw e;
        }
        institution = this.institutionService.findByName(rorId);
      }
      assertNotNull(this.institutionService.uploadLogo(institution.getResId(), file));
    }
  }

  @Order(50)
  @Tag(AbstractDemoIT.PROJECT_GYPSO)
  @Tag(AbstractDemoIT.PROJECT_ROMA_16)
  @Tag(AbstractDemoIT.PROJECT_POESIE_16)
  @Tag(AbstractDemoIT.PROJECT_ARCHEO)
  @Tag(AbstractDemoIT.PROJECT_HISTARTDIA)
  @Tag(AbstractDemoIT.PROJECT_NEOTROPICAL_FISH)
  @Tag(AbstractDemoIT.PROJECT_ADAM)
  @Tag(AbstractDemoIT.PROJECT_TURRETTINI)
  @Tag(AbstractDemoIT.PROJECT_BIBANT)
  @Tag(AbstractDemoIT.PROJECT_TROCHIN)
  @Test
  void createOntologies() throws URISyntaxException, IOException {
    assertNotNull(this.createOntology("adam", "Adam", "1.0", "Ontologie pour l'application Adam",
            "https://ld.hedera.unige.ch/ontologies/adam#",
            "https://ld.hedera.unige.ch/ontologies/adam",
            RdfFormat.RDF_XML, false, this.listFiles(ONTOLOGY_FOLDER, "/", "adam-onto.rdf")[0]));
    assertNotNull(this.createOntology("archeo", ARCHEO, "1.0", "Images archéologie classique",
            "https://ld.hedera.unige.ch/ontologies/archeo#",
            "https://ld.hedera.unige.ch/ontologies/archeo",
            RdfFormat.RDF_XML, false, this.listFiles(ONTOLOGY_FOLDER, "/", "archeo-onto.rdf")[0]));
    assertNotNull(this.createOntology("bibant", "BIBANT", "1.0", "Bibliographie anthropologie",
            "https://ld.hedera.unige.ch/ontologies/bibant#",
            "https://ld.hedera.unige.ch/ontologies/bibant",
            RdfFormat.RDF_XML, false, this.listFiles(ONTOLOGY_FOLDER, "/", "bibant-onto.rdf")[0]));
    assertNotNull(this.createOntology("gypso", GYPSO, "1.0", "Fiches de la gypsothèque d'archéologie classique",
            "https://ld.hedera.unige.ch/ontologies/gypso#",
            "https://ld.hedera.unige.ch/ontologies/gypso",
            RdfFormat.RDF_XML, false, this.listFiles(ONTOLOGY_FOLDER, "/", "gypso-onto.rdf")[0]));
    assertNotNull(this.createOntology("histartdia", HISTARTDIA, "1.0",
            "Fiches techniques décrivant les images de la diathèque de l'Histoire de l'Art",
            "https://ld.hedera.unige.ch/ontologies/histartdia#",
            "https://ld.hedera.unige.ch/ontologies/histartdia",
            RdfFormat.RDF_XML, false, this.listFiles(ONTOLOGY_FOLDER, "/", "histartdia-onto.rdf")[0]));
    assertNotNull(this.createOntology("neotropfish", NEOTROPICAL_FISH, "1.0", "Poissons néotropicaux (mesures et profil génétique)",
            "https://ld.hedera.unige.ch/ontologies/neotropicalfish#",
            "https://ld.hedera.unige.ch/ontologies/neotropicalfish",
            RdfFormat.RDF_XML, false, this.listFiles(ONTOLOGY_FOLDER, "/", "neotropicalfish-onto.rdf")[0]));
    assertNotNull(this.createOntology("poesie1600", POESIE_16, "1.0", "Poésie du XVIe siècle",
            "https://ld.hedera.unige.ch/ontologies/poesie1600#",
            "https://ld.hedera.unige.ch/ontologies/poesie1600",
            RdfFormat.RDF_XML, false, this.listFiles(ONTOLOGY_FOLDER, "/", "poesie1600-onto.rdf")[0]));
    assertNotNull(this.createOntology("roma1600", ROMA_16, "1.0", "Musique du XVIe siècle",
            "https://ld.hedera.unige.ch/ontologies/roma1600#",
            "https://ld.hedera.unige.ch/ontologies/roma1600",
            RdfFormat.RDF_XML, false, this.listFiles(ONTOLOGY_FOLDER, "/", "roma1600-onto.rdf")[0]));
    assertNotNull(this.createOntology("tronchin", "Tronchin", "1.0", "Documents des archives Tronchin",
            "https://ld.hedera.unige.ch/ontologies/tronchin#",
            "https://ld.hedera.unige.ch/ontologies/tronchin",
            RdfFormat.RDF_XML, false, this.listFiles(ONTOLOGY_FOLDER, "/", "tronchin-onto.rdf")[0]));
    assertNotNull(this.createOntology("turrettini", TURRETTINI, "1.0", "Correspondance de J-A Turrettini",
            "https://ld.hedera.unige.ch/ontologies/turrettini#",
            "https://ld.hedera.unige.ch/ontologies/turrettini",
            RdfFormat.RDF_XML, false, this.listFiles(ONTOLOGY_FOLDER, "/", "turrettini-onto.rdf")[0]));
  }

  @Order(60)
  @Tag(AbstractDemoIT.PROJECT_PLIEGOS)
  @Tag(AbstractDemoIT.PROJECT_LUDUS)
  @Tag(AbstractDemoIT.PROJECT_PIAGET)
  @Tag(AbstractDemoIT.PROJECT_GYPSO)
  @Tag(AbstractDemoIT.PROJECT_ROMA_16)
  @Tag(AbstractDemoIT.PROJECT_POESIE_16)
  @Tag(AbstractDemoIT.PROJECT_ARCHEO)
  @Tag(AbstractDemoIT.PROJECT_HISTARTDIA)
  @Tag(AbstractDemoIT.PROJECT_NEOTROPICAL_FISH)
  @Tag(AbstractDemoIT.PROJECT_TURRETTINI)
  @Test
  void createResearchObjectTypes() {
    // CIDOC-CRM
    final Ontology cidocCrmOnto = this.ontologyClientService
            .searchByProperties(Map.of("name", HederaConstants.ONTOLOGY_CIDOC_CRM, "version", "7.1.2"))
            .get(0);
    assertNotNull(
            this.createResearchObjectType("cidoc-crm-e5-event", "event", "E5_Event", "Event", cidocCrmOnto));
    assertNotNull(
            this.createResearchObjectType("cidoc-crm-pliego", "pliego", "E22_Human-Made_Object", "Pliego", cidocCrmOnto));
    // RiC-O
    final Ontology ricoOnto = this.ontologyClientService.searchByProperties(Map.of("name", HederaConstants.ONTOLOGY_RIC_O, "version", "1.0"))
            .get(0);
    assertNotNull(
            this.createResearchObjectType("ric-o-record-resource", "record-resource", "RecordResource", "Record Resource", ricoOnto));
    assertNotNull(
            this.createResearchObjectType("ric-o-record-set", "record-set", "RecordSet", "Record Set", ricoOnto));
    assertNotNull(
            this.createResearchObjectType("ric-o-record", "record", "Record", "Record", ricoOnto));
    assertNotNull(
            this.createResearchObjectType("ric-o-record-part", "record-part", "RecordPart", "Record Part", ricoOnto));
    // Gypso
    final Ontology gypsoOnto = this.ontologyClientService.searchByProperties(Map.of("name", GYPSO, "version", "1.0"))
            .get(0);
    assertNotNull(
            this.createResearchObjectType("gypso-file", "file", "File", "Gypso file", gypsoOnto));
    assertNotNull(
            this.createResearchObjectType("gypso-material", "material", "Material", "Gypso material", gypsoOnto));
    assertNotNull(
            this.createResearchObjectType("gypso-museum", "museum", "Museum", "Gypso museum", gypsoOnto));
    assertNotNull(
            this.createResearchObjectType("gypso-style", "style", "Style", "Gypso style", gypsoOnto));
    assertNotNull(
            this.createResearchObjectType("gypso-author", "author", "Author", "Gypso author", gypsoOnto));
    assertNotNull(
            this.createResearchObjectType("gypso-localisation", "localisation", "Localisation", "Gypso localisation", gypsoOnto));
    assertNotNull(
            this.createResearchObjectType("gypso-technic", "technic", "Technic", "Gypso technic", gypsoOnto));
    assertNotNull(
            this.createResearchObjectType("gypso-specsheet", "specsheet", "SpecSheet", "Gypso specification sheet", gypsoOnto));
    assertNotNull(
            this.createResearchObjectType("gypso-conservationstate", "conservationstate", "ConservationState", "Gypso conservation state",
                    gypsoOnto));
    // Poésie XVIe
    final Ontology poesieOnto = this.ontologyClientService.searchByProperties(Map.of("name", POESIE_16, "version", "1.0"))
            .get(0);
    assertNotNull(
            this.createResearchObjectType("poesie16-file", "file", "File", "Fichier pour la poésie du XVIe", poesieOnto));
    assertNotNull(
            this.createResearchObjectType("poesie16-edition", "edition", "Edition", "Edition pour la poésie du XVIe", poesieOnto));
    assertNotNull(
            this.createResearchObjectType("poesie16-person", "personne", "Personne", "Personne pour la poésie du XVIe",
                    poesieOnto));
    assertNotNull(
            this.createResearchObjectType("poesie16-lieu", "lieu", "Lieu", "Lieu pour la poésie du XVIe", poesieOnto));
    // Musique XVIe
    final Ontology romaOnto = this.ontologyClientService.searchByProperties(Map.of("name", ROMA_16, "version", "1.0"))
            .get(0);
    assertNotNull(
            this.createResearchObjectType("roma16-file", "file", "File", "Fichier pour la musique du XVIe", romaOnto));
    assertNotNull(
            this.createResearchObjectType("roma16-source", "source", "Source", "Source pour la musique du XVIe", romaOnto));
    assertNotNull(
            this.createResearchObjectType("roma16-musicien", "musicien", "Musicien", "Musicien pour la musique du XVIe", romaOnto));
    assertNotNull(
            this.createResearchObjectType("roma16-lieu", "lieu", "Lieu", "Lieu pour la musique du XVIe", romaOnto));
    assertNotNull(
            this.createResearchObjectType("roma16-music-event", "evenementmusical", "EvenementMusical", "Fichier pour la musique du XVIe",
                    romaOnto));
    // Archeo
    final Ontology archeoOnto = this.ontologyClientService.searchByProperties(Map.of("name", ARCHEO, "version", "1.0"))
            .get(0);
    assertNotNull(
            this.createResearchObjectType("archeo-file", "file", "File", "Fichier pour l'archéologie classique", archeoOnto));
    assertNotNull(
            this.createResearchObjectType("archeo-image", "image", "Image", "Image pour l'archéologie classique", archeoOnto));
    assertNotNull(
            this.createResearchObjectType("archeo-artisan", "artisan", "Artisan", "Artisan pour l'archéologie classique", archeoOnto));
    assertNotNull(
            this.createResearchObjectType("archeo-epoque", "epoque", "Epoque", "Epoque pour l'archéologie classique", archeoOnto));
    assertNotNull(
            this.createResearchObjectType("archeo-iconographie", "iconographie", "Iconographie", "Iconographie pour l'archéologie classique",
                    archeoOnto));
    assertNotNull(
            this.createResearchObjectType("archeo-materiau", "materiau", "Materiau", "Materiau pour l'archéologie classique", archeoOnto));
    assertNotNull(
            this.createResearchObjectType("archeo-metier", "metier", "Metier", "Metier pour l'archéologie classique", archeoOnto));
    assertNotNull(
            this.createResearchObjectType("archeo-region", "region", "Region", "Region pour l'archéologie classique", archeoOnto));
    assertNotNull(
            this.createResearchObjectType("archeo-sujet", "sujet", "Sujet", "Sujet pour l'archéologie classique", archeoOnto));
    assertNotNull(
            this.createResearchObjectType("archeo-technique", "technique", "Technique", "Technique pour l'archéologie classique", archeoOnto));
    assertNotNull(
            this.createResearchObjectType("archeo-type-objet", "typeobjet", "TypeObjet", "TypeObjet pour l'archéologie classique", archeoOnto));

    // Histoire de l'art
    final Ontology histartdiaOnto = this.ontologyClientService.searchByProperties(Map.of("name", HISTARTDIA, "version", "1.0"))
            .get(0);
    assertNotNull(
            this.createResearchObjectType("histartdia-file", "file", "File", "Fichier pour l'histoire de l'art", histartdiaOnto));
    assertNotNull(
            this.createResearchObjectType("histartdia-fiche", "fiche", "Fiche", "Fiche pour l'histoire de l'art", histartdiaOnto));
    assertNotNull(
            this.createResearchObjectType("histartdia-acquereur", "acquereur", "Acquereur", "Acquereur pour l'histoire de l'art",
                    histartdiaOnto));
    assertNotNull(
            this.createResearchObjectType("histartdia-auteur", "auteur", "Auteur", "Auteur pour l'histoire de l'art", histartdiaOnto));
    assertNotNull(
            this.createResearchObjectType("histartdia-localisation", "localisation", "Localisation", "Lieu pour l'histoire de l'art",
                    histartdiaOnto));
    // Poissons néotropicaux
    final Ontology neotropfishOnto = this.ontologyClientService.searchByProperties(Map.of("name", NEOTROPICAL_FISH, "version", "1.0"))
            .get(0);
    assertNotNull(
            this.createResearchObjectType("neotropfish-genetic-file", "geneticfile", "GeneticFile",
                    "Fichier génétique pour les poissons néotropicaux",
                    neotropfishOnto));
    // Trurrettini
    final Ontology turrettiniOnto = this.ontologyClientService.searchByProperties(Map.of("name", TURRETTINI, "version", "1.0"))
            .get(0);
    assertNotNull(
            this.createResearchObjectType("turrettini-file", "file", "File", "Fichier pour Turrettini", turrettiniOnto));
    assertNotNull(
            this.createResearchObjectType("turrettini-letter", "letter", "Letter", "Lettre pour Turrettini", turrettiniOnto));
    assertNotNull(
            this.createResearchObjectType("turrettini-book", "book", "Book", "Livre pour Turrettini", turrettiniOnto));
    assertNotNull(
            this.createResearchObjectType("turrettini-person", "person", "Person", "Personne pour Turrettini", turrettiniOnto));
    assertNotNull(
            this.createResearchObjectType("turrettini-theme", "theme", "Theme", "Thème pour Turrettini", turrettiniOnto));

  }

  private Ontology createOntology(String ontoId, String name, String version, String description, String ontoBaseUri, String ontoUrl,
          RdfFormat rdfFormat,
          boolean external, Resource ontoFile) throws URISyntaxException, MalformedURLException {
    final List<Ontology> ontoList = this.ontologyClientService.searchByProperties(Map.of("name", name, "version", version));
    if (!ontoList.isEmpty()) {
      return ontoList.get(0);
    }
    log.info("Creating ontology '{}' ({})", name, version);
    Ontology onto = new Ontology();
    if (!StringTool.isNullOrEmpty(ontoId)) {
      onto.setResId(ontoId);
    }
    onto.setName(name);
    onto.setVersion(version);
    onto.setDescription(description);
    onto.setBaseUri(new URI(ontoBaseUri));
    if (!StringTool.isNullOrEmpty(ontoUrl)) {
      onto.setUrl(new URL(ontoUrl));
    }
    onto.setFormat(rdfFormat);
    onto.setExternal(external);
    final Ontology createdOnto = this.ontologyClientService.create(onto);
    return this.ontologyClientService.uploadOntologyFile(createdOnto.getResId(), ontoFile);
  }

  private ResearchObjectType createResearchObjectType(String id, String name, String rdfType, String description, Ontology ontology) {
    try {
      return this.researchObjectTypeService.findOne(id);
    } catch (HttpClientErrorException e) {
      if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
        throw e;
      }
      log.info("Creating research object type '{}'", name);
      ResearchObjectType rot = new ResearchObjectType();
      rot.setResId(id);
      rot.setName(name);
      rot.setRdfType(rdfType);
      rot.setDescription(description);
      if (ontology != null) {
        rot.setOntology(ontology);
      }
      return this.researchObjectTypeService.create(rot);
    }
  }

  private Map<String, String> getFundingAgencyRorIdList() {
    Map<String, String> list = new HashMap<>();
    list.put("SNF", "00yjd3n13");
    list.put("CTI", "012y9zp98");
    list.put("swissuniversities", "02fsagg23");
    return list;
  }

  private Map<String, String> getInstitutionRorIdList() {
    Map<String, String> list = new HashMap<>();
    list.put("UNIGE", "01swzsf04");
    list.put("EPFL", "02s376052");
    list.put("ETH", "05a28rw58");
    list.put("UZH", "02crff812");
    list.put("HES-SO", "01xkakk17");
    list.put("ZHAW", "05pmsvm27");
    list.put("HUG", "01m1pv723");
    list.put("IHEID", "007ygn379");
    list.put("UNIL", "019whta54");
    list.put("CERN", "01ggx4157");
    list.put("UNIFR", "022fs9h90");
    list.put("BFH", "02bnkt322");
    return list;
  }

  private Institution createInstitution(String name, String description, String webSite, String rorId,
          String... emailSuffixes) {
    try {
      return this.institutionService.findByName(name);
    } catch (HttpClientErrorException e) {
      if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
        throw e;
      }
      log.info("Creating instituion '{}'", name);
      Institution institution = new Institution();
      institution.setName(name);
      institution.setDescription(description);
      // URL
      if (!StringTool.isNullOrEmpty(webSite)) {
        try {
          institution.setUrl(new URL(webSite));
        } catch (MalformedURLException ex) {
          log.warn("Error in creating institution: URL {} is malformed", webSite);
        }
      }
      // ROR ID
      if (!StringTool.isNullOrEmpty(rorId)) {
        institution.setRorId(rorId);
      }
      // Email suffuxes
      if (!StringTool.isNullOrEmpty(emailSuffixes[0])) {
        for (String emailSuffix : emailSuffixes) {
          institution.getEmailSuffixes().add(emailSuffix);
        }
      }
      return this.institutionService.create(institution);
    }
  }

  private FundingAgency createFundingAgency(String acronym, String name, String url, String rorId) {
    try {
      return this.fundingAgencyService.findByAcronym(acronym);
    } catch (HttpClientErrorException e) {
      if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
        throw e;
      }
      log.info("Creating funding agency '{}'", acronym);
      FundingAgency fa = new FundingAgency();
      fa.setAcronym(acronym);
      fa.setName(name);
      if (!StringTool.isNullOrEmpty(url)) {
        try {
          fa.setUrl(new URL(url));
        } catch (MalformedURLException ex) {
          log.warn("Error in creating funding agency: URL {} is malformed", url);
        }
      }
      if (!StringTool.isNullOrEmpty(rorId)) {
        fa.setRorId(rorId);
      }
      return this.fundingAgencyService.create(fa);
    }
  }

}
