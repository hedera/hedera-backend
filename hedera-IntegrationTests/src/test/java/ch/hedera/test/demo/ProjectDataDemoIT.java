/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ProjectDataDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.humanities.Rml;
import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.DatasetFileType;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.RdfDatasetFile.RdfDatasetFileStatus;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.ingest.SourceDatasetFile.SourceDatasetFileStatus;
import ch.hedera.service.ingest.RdfDatasetFileClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.SourceDatasetITService;

@Order(3)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@EnabledIf("${hedera.demo-config.enabled:false}")
class ProjectDataDemoIT extends AbstractDemoIT {

  private final static String DEMO_DATASET = "Demo";
  private final static String PILOT_DATASET = "Pilot";

  protected final SourceDatasetITService sourceDatasetITService;
  protected final RdfDatasetFileITService rdfDatasetFileITService;

  protected final ResearchDataFileClientService researchDataFileClientService;
  protected final SourceDatasetClientService sourceDatasetClientService;
  protected final RdfDatasetFileClientService rdfDatasetFileClientService;

  @Autowired
  public ProjectDataDemoIT(Environment env,
          SudoRestClientTool restClientTool,
          ProjectITService projectITService,
          SourceDatasetITService sourceDatasetITService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          SourceDatasetClientService sourceDatasetClientService,
          RdfDatasetFileClientService rdfDatasetFileClientService) {
    super(env, restClientTool, projectITService);
    this.sourceDatasetITService = sourceDatasetITService;
    this.rdfDatasetFileITService = rdfDatasetFileITService;
    this.researchDataFileClientService = researchDataFileClientService;
    this.sourceDatasetClientService = sourceDatasetClientService;
    this.rdfDatasetFileClientService = rdfDatasetFileClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Order(10)
  @Tag(AbstractDemoIT.PROJECT_PLIEGOS)
  @Test
  void createPliegosResearchDataFiles() throws IOException {
    List<ResearchDataFile> researchDataFiles = this.researchDataFileClientService.findAllWithProjectId(PROJECT_PLIEGOS);
    assertNotNull(researchDataFiles);
    // Add TEI
    this.addResearchDataFiles(PROJECT_PLIEGOS, PROJECT_PLIEGOS, "/varios/tei", "*.xml", DataAccessibilityType.TEI, researchDataFiles);
    // Add Varios images
    this.addResearchDataFiles(PROJECT_PLIEGOS, PROJECT_PLIEGOS, "/varios", "*.jpg", DataAccessibilityType.IIIF, researchDataFiles);
    // Check
    researchDataFiles = this.researchDataFileClientService.findAllWithProjectId(PROJECT_PLIEGOS);
    assertTrue(researchDataFiles.size() >= 16);
  }

  @Order(12)
  @Tag(AbstractDemoIT.PROJECT_PLIEGOS)
  @Test
  void createPliegosSourceDataset() {
    List<SourceDataset> sourceDatasets = this.sourceDatasetClientService.findAllWithProjectId(PROJECT_PLIEGOS);
    // Source Dataset
    Optional<SourceDataset> existingDatasetOpt = sourceDatasets.stream().filter(d -> d.getName().equals(DEMO_DATASET)).findAny();
    SourceDataset existingDataset = null;
    if (existingDatasetOpt.isPresent()) {
      existingDataset = existingDatasetOpt.get();
    } else {
      final SourceDataset dataset = new SourceDataset();
      dataset.setProjectId(PROJECT_PLIEGOS);
      dataset.setName(DEMO_DATASET);
      existingDataset = this.sourceDatasetClientService.create(dataset);
    }
    // Source data files
    final String datasetId = existingDataset.getResId();
    List<SourceDatasetFile> sourceDatasetFiles = this.sourceDatasetClientService.getSourceDataFiles(datasetId);
    List<ResearchDataFile> researchDataFiles = this.researchDataFileClientService.findAllWithProjectId(PROJECT_PLIEGOS);
    assertNotNull(researchDataFiles);
    this.addResearchDataFilesAsSourceDatasetFile(existingDataset, DEMO_DATASET.toLowerCase(), DatasetFileType.XML, DataAccessibilityType.TEI,
            sourceDatasetFiles, researchDataFiles);
    sourceDatasetFiles = this.sourceDatasetClientService.getSourceDataFiles(datasetId);
    assertTrue(sourceDatasetFiles.size() >= 4);
    final Rml rml = this.projectITService.getRmls(PROJECT_PLIEGOS).get(0);
    for (SourceDatasetFile sourceDatasetFile : sourceDatasetFiles) {
      if (sourceDatasetFile.getStatus() == SourceDatasetFileStatus.NOT_TRANSFORMED) {
        final Result result = this.sourceDatasetClientService.applyRml(datasetId, sourceDatasetFile.getResId(), rml.getResId());
        assertEquals(ActionStatus.EXECUTED, result.getStatus());
        this.sourceDatasetITService.waitForSourceDatasetFileIsTransformed(datasetId, sourceDatasetFile.getResId());
      }
    }
    // RDF Dataset files
    List<RdfDatasetFile> rdfDatasetFiles = this.rdfDatasetFileClientService.findAllWithProjectId(PROJECT_PLIEGOS);
    assertTrue(rdfDatasetFiles.size() >= 4);
    for (RdfDatasetFile rdfDatasetFile : rdfDatasetFiles) {
      if (rdfDatasetFile.getStatusImport() == RdfDatasetFileStatus.NOT_IMPORTED) {
        final Result result = this.rdfDatasetFileClientService.addInTripleStore(rdfDatasetFile.getResId(), PROJECT_PLIEGOS);
        assertEquals(ActionStatus.EXECUTED, result.getStatus());
        this.rdfDatasetFileITService.waitForRdfDatasetFileIsImportedInFuseki(rdfDatasetFile.getResId());
      }
    }
    // Check
    sourceDatasets = this.sourceDatasetClientService.findAllWithProjectId(PROJECT_PLIEGOS);
    assertTrue(sourceDatasets.size() >= 1);
  }

  @Order(20)
  @Tag(AbstractDemoIT.PROJECT_LUDUS)
  @Test
  void createLudusResearchDataFiles() throws IOException {
    List<ResearchDataFile> researchDataFiles = this.researchDataFileClientService.findAllWithProjectId(PROJECT_LUDUS);
    assertNotNull(researchDataFiles);
    for (String imagePattern : List.of("*.jpg", "*.gif", "*.bmp", "*.tif", "*.png", "*.pdf")) {
      // Add images
      this.addResearchDataFiles(PROJECT_LUDUS, PROJECT_LUDUS, "/", imagePattern, DataAccessibilityType.IIIF, researchDataFiles);
    }
    researchDataFiles = this.researchDataFileClientService.findAllWithProjectId(PROJECT_LUDUS);
    assertTrue(researchDataFiles.size() >= 7);
  }

  @Order(22)
  @Tag(AbstractDemoIT.PROJECT_LUDUS)
  @Test
  void createLudusSourceDataset() throws IOException {
    List<SourceDataset> sourceDatasets = this.sourceDatasetClientService.findAllWithProjectId(PROJECT_LUDUS);
    // Source Dataset
    Optional<SourceDataset> existingDatasetOpt = sourceDatasets.stream().filter(d -> d.getName().equals(DEMO_DATASET)).findAny();
    SourceDataset existingDataset = null;
    if (existingDatasetOpt.isPresent()) {
      existingDataset = existingDatasetOpt.get();
    } else {
      final SourceDataset dataset = new SourceDataset();
      dataset.setProjectId(PROJECT_LUDUS);
      dataset.setName(DEMO_DATASET);
      existingDataset = this.sourceDatasetClientService.create(dataset);
    }
    // Source data files
    final String datasetId = existingDataset.getResId();
    List<SourceDatasetFile> sourceDatasetFiles = this.sourceDatasetClientService.getSourceDataFiles(datasetId);
    this.addSourceDatasetFiles(existingDataset.getResId(), PROJECT_LUDUS, "*.csv", DatasetFileType.CSV, DEMO_DATASET.toLowerCase(),
            sourceDatasetFiles);
    sourceDatasetFiles = this.sourceDatasetClientService.getSourceDataFiles(datasetId);
    assertTrue(sourceDatasetFiles.size() >= 1);
    final Rml rml = this.projectITService.getRmls(PROJECT_LUDUS).get(0);
    for (SourceDatasetFile sourceDatasetFile : sourceDatasetFiles) {
      if (sourceDatasetFile.getStatus() == SourceDatasetFileStatus.NOT_TRANSFORMED) {
        final Result result = this.sourceDatasetClientService.applyRml(datasetId, sourceDatasetFile.getResId(), rml.getResId());
        assertEquals(ActionStatus.EXECUTED, result.getStatus());
        this.sourceDatasetITService.waitForSourceDatasetFileIsTransformed(datasetId, sourceDatasetFile.getResId());
      }
    }
  }

  @Order(32)
  @Tag(AbstractDemoIT.PROJECT_PIAGET)
  @Test
  void createPiagetSourceDataset() throws IOException {
    List<SourceDataset> sourceDatasets = this.sourceDatasetClientService.findAllWithProjectId(PROJECT_PIAGET);
    // Source Dataset
    Optional<SourceDataset> existingDatasetOpt = sourceDatasets.stream().filter(d -> d.getName().equals(DEMO_DATASET)).findAny();
    SourceDataset existingDataset = null;
    if (existingDatasetOpt.isPresent()) {
      existingDataset = existingDatasetOpt.get();
    } else {
      final SourceDataset dataset = new SourceDataset();
      dataset.setProjectId(PROJECT_PIAGET);
      dataset.setName(DEMO_DATASET);
      existingDataset = this.sourceDatasetClientService.create(dataset);
    }
    // Source data files
    final String datasetId = existingDataset.getResId();
    List<SourceDatasetFile> sourceDatasetFiles = this.sourceDatasetClientService.getSourceDataFiles(datasetId);
    this.addSourceDatasetFiles(existingDataset.getResId(), PROJECT_PIAGET, "*.json", DatasetFileType.JSON, DEMO_DATASET.toLowerCase(),
            sourceDatasetFiles);
    sourceDatasetFiles = this.sourceDatasetClientService.getSourceDataFiles(datasetId);
    assertTrue(sourceDatasetFiles.size() >= 2);
  }

  @Order(40)
  @Tag(AbstractDemoIT.PROJECT_GYPSO)
  @Test
  void createGypsoSourceDataset() throws IOException {
    List<SourceDataset> sourceDatasets = this.sourceDatasetClientService.findAllWithProjectId(PROJECT_GYPSO);
    // Source Dataset
    Optional<SourceDataset> existingDatasetOpt = sourceDatasets.stream().filter(d -> d.getName().equals(DEMO_DATASET)).findAny();
    SourceDataset existingDataset = null;
    if (existingDatasetOpt.isPresent()) {
      existingDataset = existingDatasetOpt.get();
    } else {
      final SourceDataset dataset = new SourceDataset();
      dataset.setProjectId(PROJECT_GYPSO);
      dataset.setName(DEMO_DATASET);
      existingDataset = this.sourceDatasetClientService.create(dataset);
    }
    // Source data files
    final String datasetId = existingDataset.getResId();
    List<SourceDatasetFile> sourceDatasetFiles = this.sourceDatasetClientService.getSourceDataFiles(datasetId);
    this.addSourceDatasetFiles(existingDataset.getResId(), PROJECT_GYPSO, "*.nt", DatasetFileType.RDF, DEMO_DATASET.toLowerCase(),
            sourceDatasetFiles);
    sourceDatasetFiles = this.sourceDatasetClientService.getSourceDataFiles(datasetId);
    assertTrue(sourceDatasetFiles.size() >= 1);
    for (SourceDatasetFile sourceDatasetFile : sourceDatasetFiles) {
      if (sourceDatasetFile.getStatus() == SourceDatasetFileStatus.NOT_TRANSFORMED) {
        final Result result = this.sourceDatasetClientService.transformRdf(datasetId, sourceDatasetFile.getResId());
        assertEquals(ActionStatus.EXECUTED, result.getStatus());
        this.sourceDatasetITService.waitForSourceDatasetFileIsTransformed(datasetId, sourceDatasetFile.getResId());
      }
    }
  }

  @Order(41)
  @Tag(AbstractDemoIT.PROJECT_GYPSO)
  @Test
  void createGypsoResearchDataFiles() throws IOException {
    List<ResearchDataFile> researchDataFiles = this.researchDataFileClientService.findAllWithProjectId(PROJECT_GYPSO);
    assertNotNull(researchDataFiles);
    // Add images
    this.addResearchDataFiles(PROJECT_GYPSO, PROJECT_GYPSO, "/", "ug*", DataAccessibilityType.IIIF, researchDataFiles);
    // Add images
    researchDataFiles = this.researchDataFileClientService.findAllWithProjectId(PROJECT_GYPSO);
    assertTrue(researchDataFiles.size() >= 21);
  }

  private void addResearchDataFiles(String projectId, String projectFolder, String relativeLocation, String filePattern,
          DataAccessibilityType accessibilityType,
          List<ResearchDataFile> researchDataFiles) throws IOException {
    for (Resource dataFile : this.listFiles(projectFolder, relativeLocation, filePattern))
      if (researchDataFiles
              .stream()
              .filter(f -> f.getRelativeLocation().equals(relativeLocation) && f.getFileName().equals(dataFile.getFilename()))
              .count() == 0) {
        this.researchDataFileClientService.uploadFile(dataFile, projectId, accessibilityType.toString(), relativeLocation);
      }
  }

  private void addSourceDatasetFiles(String datasetId, String projectFolder, String filePattern, DatasetFileType datasetFileType, String version,
          List<SourceDatasetFile> sourceDatasetFiles) throws IOException {
    for (Resource sourceFile : this.listFiles(projectFolder, "/", filePattern))
      if (sourceDatasetFiles.stream()
              .filter(f -> f.getFileName().equals(sourceFile.getFilename())
                      && f.getVersion().equals(version)
                      && f.getType().equals(datasetFileType))
              .count() == 0) {
        this.sourceDatasetClientService.uploadFile(datasetId, sourceFile, datasetFileType.toString(), version);
      }
  }

  private void addResearchDataFilesAsSourceDatasetFile(SourceDataset dataset, String version, DatasetFileType datasetFileType,
          DataAccessibilityType accessibleFrom, List<SourceDatasetFile> sourceDatasetFiles, List<ResearchDataFile> researchDataFiles) {
    for (ResearchDataFile researchDataFile : researchDataFiles)
      if (researchDataFile.getAccessibleFrom() == accessibleFrom) {
        if (sourceDatasetFiles.stream()
                .filter(f -> f.getFileName().equals(researchDataFile.getFileName())
                        && f.getVersion().equals(version)
                        && f.getType().equals(datasetFileType))
                .count() == 0) {
          SourceDatasetFile localSourceDatasetFile = new SourceDatasetFile();
          localSourceDatasetFile.setDataset(dataset);
          localSourceDatasetFile.setVersion(version);
          localSourceDatasetFile.setType(datasetFileType);
          localSourceDatasetFile.setResearchDataFile(researchDataFile);

          this.sourceDatasetClientService.createSourceDataFile(dataset.getResId(), localSourceDatasetFile);
        }
      }
  }

}
