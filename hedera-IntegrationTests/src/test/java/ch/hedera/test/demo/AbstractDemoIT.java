/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AbstractDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.demo;

import java.io.IOException;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.test.AbstractIT;
import ch.hedera.test.service.ProjectITService;

public abstract class AbstractDemoIT extends AbstractIT {

  protected final static String PNG_FILES = "*.png";
  protected final static String SVG_FILES = "*.svg";
  protected final static String JPG_FILES = "*.jpg";

  // Project IDs
  public final static String PROJECT_PLIEGOS = "pliegos";
  public final static String PROJECT_VISUAL_CONTAGIONS = "visualcontagions";
  public final static String PROJECT_PIAGET = "piaget";
  public final static String PROJECT_BODMER = "bodmer";
  public final static String PROJECT_MAH = "mah";
  public final static String PROJECT_LUDUS = "ludus";
  // Web PL/SQL project
  public final static String PROJECT_GYPSO = "gypso";
  public final static String PROJECT_ROMA_16 = "roma1600";
  public final static String PROJECT_POESIE_16 = "poesie1600";
  public final static String PROJECT_ARCHEO = "archeo";
  public final static String PROJECT_HISTARTDIA = "histartdia";
  public final static String PROJECT_NEOTROPICAL_FISH = "neotropfish";
  public final static String PROJECT_ADAM = "adam";
  public final static String PROJECT_TURRETTINI = "turrettini";
  public final static String PROJECT_BIBANT = "bibant";
  public final static String PROJECT_TROCHIN = "tronchin";

  protected final ProjectITService projectITService;

  public AbstractDemoIT(Environment env,
          SudoRestClientTool restClientTool,
          ProjectITService projectITService) {
    super(env, restClientTool);
    this.projectITService = projectITService;
  }

  @Override
  protected void deleteFixtures() {
    // delete nothing
  }

  protected String getFilename(Resource file) {
    return file.getFilename().substring(0, file.getFilename().lastIndexOf('.'));
  }

  protected Resource[] listFiles(String folder, String relativeLocation, String filePattern) throws IOException {
    final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
    if (relativeLocation.equals("/")) {
      return resolver.getResources("classpath*:/" + folder + "/" + filePattern);
    } else {
      return resolver.getResources("classpath*:/" + folder + relativeLocation + "/" + filePattern);
    }
  }
}
