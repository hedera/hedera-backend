/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ProjectDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.demo;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.EnabledIf;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.humanities.RdfFile;
import ch.hedera.model.humanities.Rml;
import ch.hedera.model.humanities.RmlFormat;
import ch.hedera.model.settings.IIIFCollectionSettings;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.service.admin.IIIFCollectionSettingsClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.service.admin.LicenseClientService;
import ch.hedera.service.admin.ResearchObjectTypeClientService;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.test.service.ProjectITService;

@Order(2)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@EnabledIf("${hedera.demo-config.enabled:false}")
class ProjectDemoIT extends AbstractDemoIT {

  private static final Logger log = LoggerFactory.getLogger(ProjectDemoIT.class);

  private final static String PROJECT_FOLDER = "project";

  protected InstitutionClientService institutionService;
  protected LicenseClientService licenseService;
  protected ResearchObjectTypeClientService researchObjectTypeService;
  protected RmlClientService rmlclientService;
  protected IIIFCollectionSettingsClientService iiifCollectionSettingsClientService;

  @Autowired
  public ProjectDemoIT(Environment env,
          SudoRestClientTool restClientTool,
          InstitutionClientService institutionService,
          LicenseClientService licenseService,
          ResearchObjectTypeClientService researchObjectTypeService,
          RmlClientService rmlclientService,
          IIIFCollectionSettingsClientService iiifCollectionSettingsClientService,
          ProjectITService projectITService) {
    super(env, restClientTool, projectITService);
    this.institutionService = institutionService;
    this.licenseService = licenseService;
    this.researchObjectTypeService = researchObjectTypeService;
    this.rmlclientService = rmlclientService;
    this.iiifCollectionSettingsClientService = iiifCollectionSettingsClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Order(10)
  @Tag(AbstractDemoIT.PROJECT_PLIEGOS)
  @Tag(AbstractDemoIT.PROJECT_LUDUS)
  @Tag(AbstractDemoIT.PROJECT_PIAGET)
  @Tag(AbstractDemoIT.PROJECT_GYPSO)
  @Tag(AbstractDemoIT.PROJECT_ROMA_16)
  @Tag(AbstractDemoIT.PROJECT_POESIE_16)
  @Tag(AbstractDemoIT.PROJECT_ARCHEO)
  @Tag(AbstractDemoIT.PROJECT_HISTARTDIA)
  @Tag(AbstractDemoIT.PROJECT_NEOTROPICAL_FISH)
  @Tag(AbstractDemoIT.PROJECT_ADAM)
  @Tag(AbstractDemoIT.PROJECT_TURRETTINI)
  @Tag(AbstractDemoIT.PROJECT_BIBANT)
  @Tag(AbstractDemoIT.PROJECT_TROCHIN)
  @Test
  void createProjects() {
    assertNotNull(this.createProject(PROJECT_PLIEGOS, "Pliegos", "Untangling the cordel", "https://desenrollandoelcordel.unige.ch",
            HederaConstants.RESEARCH_OBJECT_TYPE_D1));
    assertNotNull(this.createProject(PROJECT_VISUAL_CONTAGIONS, "Visual Contagions", "Art, Images, and Globalisation",
            "https://www.unige.ch/visualcontagions/", HederaConstants.RESEARCH_OBJECT_TYPE_D1));
    assertNotNull(this.createProject(PROJECT_PIAGET, "Piaget", "Archives Jean Piaget", "https://archivespiaget.ch/",
            HederaConstants.RESEARCH_OBJECT_TYPE_D1));
    assertNotNull(this.createProject(PROJECT_BODMER, "Bodmer Lab", "Une bibliothèque numérique de la littérature mondiale",
            "https://bodmerlab.unige.ch", HederaConstants.RESEARCH_OBJECT_TYPE_D1));
    assertNotNull(this.createProject(PROJECT_MAH, "MAH", "Musée d'Art et d'Histoire", "https://www.mahmah.ch/",
            HederaConstants.RESEARCH_OBJECT_TYPE_D1));
    assertNotNull(this.createProject(PROJECT_LUDUS, "Ludus", "Ludus Data", null, HederaConstants.RESEARCH_OBJECT_TYPE_D1));
    assertNotNull(this.createProject(PROJECT_GYPSO, "Gypso", "Moulages Gypso", null, "gypso-file"));
    assertNotNull(
            this.createProject(PROJECT_ROMA_16, "Musique du XVIe", "Musique du XVIe sicècle", null, "roma16-file"));
    assertNotNull(
            this.createProject(PROJECT_POESIE_16, "Poésie du XVIe", "Poésie du XVIe siècle", null, "poesie16-file"));
    assertNotNull(
            this.createProject(PROJECT_ARCHEO, "Archéologie classique", "Diathèque de l'archéologie classique", null,
                    "archeo-file"));
    assertNotNull(
            this.createProject(PROJECT_HISTARTDIA, "Histoire de l'art", "Diathèque de l'histoire de l'art", null,
                    "histartdia-file"));
    assertNotNull(
            this.createProject(PROJECT_NEOTROPICAL_FISH, "Poissons néotropicaux", "Poissons néotropicaux", null,
                    "neotropfish-genetic-file"));
    assertNotNull(
            this.createProject(PROJECT_ADAM, "Adam", "Application Adam", null, null));
    assertNotNull(
            this.createProject(PROJECT_TURRETTINI, "Jean Alphonse Turrettini",
                    """
                            Jean Alphonse Turrettini (13 août 1671 - 1er mai 1737 (à 65 ans), né et mort à Genève est un théologien.

                            Issu d'une famille de Lucques, qui avait quitté l'Italie pour exercer librement la religion réformée, il est le fils de François Turrettini, pasteur et professeur de théologie à Genève. Il étudia à Genève et, après avoir visité la Hollande, la France et l'Angleterre, il se consacra au ministère évangélique. Reçu en 1693 dans la « Vénérable Compagnie des Pasteurs » de Genève, il devint pasteur de la congrégation italienne. Il fut nommé en 1697 professeur d'histoire ecclésiastique à Genève, puis professeur de théologie en 1705.

                            https://fr.wikipedia.org/wiki/Jean-Alphonse_Turrettini
                                                        """,
                    null, "turrettini-file"));
    assertNotNull(
            this.createProject(PROJECT_BIBANT, "BIBANT", "Bibliographie anthropologie", null, null));
    assertNotNull(
            this.createProject(PROJECT_TROCHIN, "François Tronchin",
                    """
                            François Tronchin est un avocat au Conseil de Genève, écrivain, mécène et collectionneur genevois né en 1704 et décédé en 1798. Il est cousin du célèbre médecin Théodore Tronchin.

                            Il est l'un des premiers collectionneurs-marchands. Il constitue, avec les conseils de son ami Jean-Étienne Liotard, une importante collection de tableaux hollandais, allemands, flamand (comme Nicolaes Berchem, Philips Wouwerman, J. Both...) et italiens dont une grande partie fut vendue à Catherine II en 1770 et sont actuellement exposés au Musée de l'Ermitage.

                            https://fr.wikipedia.org/wiki/Fran%C3%A7ois_Tronchin
                                                        """,
                    null, null));
  }

  @Order(20)
  @Tag(AbstractDemoIT.PROJECT_PLIEGOS)
  @Tag(AbstractDemoIT.PROJECT_LUDUS)
  @Tag(AbstractDemoIT.PROJECT_PIAGET)
  @Tag(AbstractDemoIT.PROJECT_GYPSO)
  @Test
  void addProjectLogo() throws IOException {
    for (String imagePattern : List.of(PNG_FILES, SVG_FILES, JPG_FILES)) {
      for (Resource file : this.listFiles(PROJECT_FOLDER, "/", imagePattern)) {
        String projectId = this.getFilename(file);
        log.info("Add logo for project '{}'", projectId);
        // Get License by Project ID
        Project project = this.projectITService.findOne(projectId);
        assertNotNull(project, "Project not found: " + projectId);
        this.projectITService.uploadLogo(project.getResId(), file);
      }
    }
  }

  @Order(25)
  @Tag(AbstractDemoIT.PROJECT_GYPSO)
  @Test
  void createIIIFCollections() {
    assertNotNull(this.createIIIFCollection(PROJECT_GYPSO, "collection-with-file", "Collection for objects with file (only)",
            this.getGypsoManifestWithFileQuery()));
  }

  @Order(30)
  @Tag(AbstractDemoIT.PROJECT_PLIEGOS)
  @Test
  void updatePliegosProject() throws IOException {
    // Update project
    Project project = this.projectITService.findOne(PROJECT_PLIEGOS);
    project.setIiifManifestResearchObjectType(this.researchObjectTypeService.findOne("cidoc-crm-pliego"));
    project.setIiifManifestSparqlQuery(this.getPliegosManifestQuery());
    assertDoesNotThrow(() -> this.projectITService.update(PROJECT_PLIEGOS, project));
    // Research Object Type
    final List<String> researchObjetcTypeList = List.of(
            HederaConstants.RESEARCH_OBJECT_TYPE_E22,
            HederaConstants.RESEARCH_OBJECT_TYPE_E24,
            HederaConstants.RESEARCH_OBJECT_TYPE_E21,
            HederaConstants.RESEARCH_OBJECT_TYPE_E53,
            HederaConstants.RESEARCH_OBJECT_TYPE_E41);
    this.checkProjectResearchObjectType(PROJECT_PLIEGOS, researchObjetcTypeList);
    // RML
    final Rml pliegosRml = this.getRml("pliegos", "Pliegos mapping", "1.0", RmlFormat.XML, "pliegos/pliegos.rml.ttl");
    List<Rml> rmlListOfProject = this.projectITService.getRmls(PROJECT_PLIEGOS);
    if (rmlListOfProject.stream().filter(rml -> rml.getResId().equals(pliegosRml.getResId())).count() == 0) {
      assertDoesNotThrow(() -> this.projectITService.addRml(PROJECT_PLIEGOS, pliegosRml.getResId()));
    }
  }

  @Order(31)
  @Tag(AbstractDemoIT.PROJECT_PIAGET)
  @Test
  void updatePiagetProject() throws IOException {
    // Research Object Type
    final List<String> researchObjetcTypeList = List.of(
            "ric-o-record-resource",
            HederaConstants.RESEARCH_OBJECT_TYPE_E21);
    this.checkProjectResearchObjectType(PROJECT_PIAGET, researchObjetcTypeList);
    // RMLs
    final Rml authorPersonRml = this.getRml("author-person", "Author mapping (person)", "1.0", RmlFormat.JSON,
            "piaget/piaget-authors-person.rml.ttl");
    final Rml authorOtherRml = this.getRml("author-other", "Author mapping (other)", "1.0", RmlFormat.JSON,
            "piaget/piaget-authors-other.rml.ttl");
    List<Rml> rmlListOfProject = this.projectITService.getRmls(PROJECT_PIAGET);
    if (rmlListOfProject.stream().filter(rml -> rml.getResId().equals(authorPersonRml.getResId())).count() == 0) {
      assertDoesNotThrow(() -> this.projectITService.addRml(PROJECT_PIAGET, authorPersonRml.getResId()));
    }
    if (rmlListOfProject.stream().filter(rml -> rml.getResId().equals(authorOtherRml.getResId())).count() == 0) {
      assertDoesNotThrow(() -> this.projectITService.addRml(PROJECT_PIAGET, authorOtherRml.getResId()));
    }
  }

  @Order(32)
  @Tag(AbstractDemoIT.PROJECT_LUDUS)
  @Test
  void updateLudusProject() throws IOException {
    // Update project
    Project project = this.projectITService.findOne(PROJECT_LUDUS);
    project.setIiifManifestResearchObjectType(this.researchObjectTypeService.findOne("roma16-source"));
    project.setIiifManifestSparqlQuery(this.getLudusManifestQuery());
    assertDoesNotThrow(() -> this.projectITService.update(PROJECT_LUDUS, project));
    // Research Object Type
    final List<String> researchObjetcTypeList = List.of(
            "roma16-musicien",
            "roma16-lieu",
            "roma16-music-event");
    this.checkProjectResearchObjectType(PROJECT_LUDUS, researchObjetcTypeList);
    // RML
    final Rml ludusRml = this.getRml("ludus", "Ludus mapping", "1.0", RmlFormat.CSV, "ludus/ludus.rml.ttl");
    List<Rml> rmlListOfProject = this.projectITService.getRmls(PROJECT_LUDUS);
    if (rmlListOfProject.stream().filter(rml -> rml.getResId().equals(ludusRml.getResId())).count() == 0) {
      assertDoesNotThrow(() -> this.projectITService.addRml(PROJECT_LUDUS, ludusRml.getResId()));
    }
  }

  @Order(33)
  @Tag(AbstractDemoIT.PROJECT_GYPSO)
  @Test
  void updateGypsoProject() {
    // Update project
    Project project = this.projectITService.findOne(PROJECT_GYPSO);
    project.setIiifManifestResearchObjectType(this.researchObjectTypeService.findOne("gypso-specsheet"));
    project.setIiifManifestSparqlQuery(this.getGypsoManifestQuery());
    assertDoesNotThrow(() -> this.projectITService.update(PROJECT_GYPSO, project));
    // Research Object Type
    final List<String> researchObjetcTypeList = List.of(
            "gypso-material",
            "gypso-museum",
            "gypso-style",
            "gypso-author",
            "gypso-technic");
    this.checkProjectResearchObjectType(PROJECT_GYPSO, researchObjetcTypeList);
  }

  @Order(34)
  @Tag(AbstractDemoIT.PROJECT_POESIE_16)
  @Test
  void updatePoesie16Project() {
    // Update project
    Project project = this.projectITService.findOne(PROJECT_POESIE_16);
    project.setIiifManifestResearchObjectType(this.researchObjectTypeService.findOne("poesie16-edition"));
    project.setIiifManifestSparqlQuery(this.getPoesie16ManifestQuery());
    assertDoesNotThrow(() -> this.projectITService.update(PROJECT_POESIE_16, project));
    // Research Object Type
    final List<String> researchObjetcTypeList = List.of(
            "poesie16-person",
            "poesie16-lieu");
    this.checkProjectResearchObjectType(PROJECT_POESIE_16, researchObjetcTypeList);
  }

  @Order(35)
  @Tag(AbstractDemoIT.PROJECT_ROMA_16)
  @Test
  void updateRoma16Project() {
    // Update project
    Project project = this.projectITService.findOne(PROJECT_ROMA_16);
    project.setIiifManifestResearchObjectType(this.researchObjectTypeService.findOne("roma16-source"));
    project.setIiifManifestSparqlQuery(this.getRoma16ManifestQuery());
    assertDoesNotThrow(() -> this.projectITService.update(PROJECT_ROMA_16, project));
    // Research Object Type
    final List<String> researchObjetcTypeList = List.of(
            "roma16-musicien",
            "roma16-lieu",
            "roma16-music-event");
    this.checkProjectResearchObjectType(PROJECT_ROMA_16, researchObjetcTypeList);
  }

  @Order(36)
  @Tag(AbstractDemoIT.PROJECT_ARCHEO)
  @Test
  void updateArcheoProject() {
    // Update project
    Project project = this.projectITService.findOne(PROJECT_ARCHEO);
    project.setIiifManifestResearchObjectType(this.researchObjectTypeService.findOne("archeo-image"));
    project.setIiifManifestSparqlQuery(this.getArcheoManifestQuery());
    assertDoesNotThrow(() -> this.projectITService.update(PROJECT_ARCHEO, project));
    // Research Object Type
    final List<String> researchObjetcTypeList = List.of(
            "archeo-artisan",
            "archeo-epoque",
            "archeo-iconographie",
            "archeo-materiau",
            "archeo-metier",
            "archeo-region",
            "archeo-sujet",
            "archeo-technique",
            "archeo-type-objet");
    this.checkProjectResearchObjectType(PROJECT_ARCHEO, researchObjetcTypeList);
  }

  @Order(37)
  @Tag(AbstractDemoIT.PROJECT_HISTARTDIA)
  @Test
  void updateHistArtDiaProject() {
    // Update project
    Project project = this.projectITService.findOne(PROJECT_HISTARTDIA);
    project.setIiifManifestResearchObjectType(this.researchObjectTypeService.findOne("histartdia-fiche"));
    project.setIiifManifestSparqlQuery(this.getHistArtDiaManifestQuery());
    assertDoesNotThrow(() -> this.projectITService.update(PROJECT_HISTARTDIA, project));
    // Research Object Type
    final List<String> researchObjetcTypeList = List.of(
            "histartdia-acquereur",
            "histartdia-auteur",
            "histartdia-localisation");
    this.checkProjectResearchObjectType(PROJECT_HISTARTDIA, researchObjetcTypeList);
  }

  @Order(37)
  @Tag(AbstractDemoIT.PROJECT_TURRETTINI)
  @Test
  void updateTurrettiniProject() {
    // Update project
    Project project = this.projectITService.findOne(PROJECT_TURRETTINI);
    project.setIiifManifestResearchObjectType(this.researchObjectTypeService.findOne("turrettini-letter"));
    project.setIiifManifestSparqlQuery(this.getTurrettiniManifestQuery());
    assertDoesNotThrow(() -> this.projectITService.update(PROJECT_TURRETTINI, project));
    // Research Object Type
    final List<String> researchObjetcTypeList = List.of(
            "turrettini-book",
            "turrettini-person",
            "turrettini-theme");
    this.checkProjectResearchObjectType(PROJECT_TURRETTINI, researchObjetcTypeList);
  }

  @Order(37)
  @Tag(AbstractDemoIT.PROJECT_MAH)
  @Test
  void updateMAHProject() {
    // Update project
    Project project = this.projectITService.findOne(PROJECT_MAH);
    project.setIiifManifestResearchObjectType(this.researchObjectTypeService.findOne("cidoc-crm-e24-default"));
    project.setIiifManifestSparqlQuery(this.getMAHManifestQuery());
    assertDoesNotThrow(() -> this.projectITService.update(PROJECT_MAH, project));
    // Research Object Type
    final List<String> researchObjetcTypeList = List.of();
    this.checkProjectResearchObjectType(PROJECT_MAH, researchObjetcTypeList);
  }

  @Order(40)
  @Tag(AbstractDemoIT.PROJECT_VISUAL_CONTAGIONS)
  @Test
  void updateVisualContagionsProject() {
    // Update project
    Project project = this.projectITService.findOne(PROJECT_VISUAL_CONTAGIONS);
    project.setIiifManifestResearchObjectType(this.researchObjectTypeService.findOne("cidoc-crm-e24-default"));
    project.setIiifManifestSparqlQuery(this.getVisualContagionsManifestQuery());
    assertDoesNotThrow(() -> this.projectITService.update(PROJECT_VISUAL_CONTAGIONS, project));
    // Research Object Type
    final List<String> researchObjetcTypeList = List.of();
    this.checkProjectResearchObjectType(PROJECT_VISUAL_CONTAGIONS, researchObjetcTypeList);
  }

  private Project createProject(String id, String name, String description, String url, String researchObjectTypeForFileId) {
    try {
      return this.projectITService.findOne(id);
    } catch (HttpClientErrorException e) {
      if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
        throw e;
      }
      Project project = new Project();
      project.setResId(id);
      project.setShortName(id);
      project.setName(name);
      project.setDescription(description);
      project.setDefaultLicense(this.licenseService.findBySpdxId("CC-BY-4.0"));
      if (!StringTool.isNullOrEmpty(researchObjectTypeForFileId)) {
        project.setResearchDataFileResearchObjectType(this.researchObjectTypeService.findOne(researchObjectTypeForFileId));
      }
      // URL
      if (!StringTool.isNullOrEmpty(url)) {
        try {
          project.setUrl(new URL(url));
        } catch (MalformedURLException ex) {
          log.warn("Error in creating institution: URL {} is malformed", url);
        }
      }
      // Create project
      project = this.projectITService.create(project);
      // Institution
      this.projectITService.addInstitution(project.getResId(), this.institutionService.findByName("UNIGE").getResId());
      log.info("Project '{}' created", project.getResId());
      return project;
    }
  }

  private IIIFCollectionSettings createIIIFCollection(String projectId, String collectionName, String collectionDesc, String collectionQuery) {
    final Project project;
    try {
      project = this.projectITService.findOne(projectId);
    } catch (HttpClientErrorException e) {
      throw e;
    }
    List<IIIFCollectionSettings> list = this.iiifCollectionSettingsClientService
            .searchByProperties(Map.of("name", collectionName, "project.resId", project.getResId()));
    if (!list.isEmpty()) {
      return list.get(0);
    }
    IIIFCollectionSettings iiifCollection = new IIIFCollectionSettings();
    iiifCollection.setProject(project);
    iiifCollection.setName(collectionName);
    iiifCollection.setDescription(collectionDesc);
    iiifCollection.setIiifCollectionSparqlQuery(collectionQuery);
    iiifCollection = this.iiifCollectionSettingsClientService.create(iiifCollection);
    log.info("IIIF Collection '{}' created", iiifCollection.getResId());
    return iiifCollection;
  }

  private void checkProjectResearchObjectType(String projetcId, List<String> researchObjectTypeList) {
    List<ResearchObjectType> researchObjectTypeListOfProject = this.projectITService.getResearchObjectTypes(projetcId);
    for (String researchObjectTypeId : researchObjectTypeList) {
      if (researchObjectTypeListOfProject.stream().filter(rot -> rot.getResId().equals(researchObjectTypeId)).count() == 0) {
        assertDoesNotThrow(() -> this.projectITService.addResearchObjectType(projetcId, researchObjectTypeId));
      }
    }
  }

  private Rml getRml(String name, String description, String version, RmlFormat format, String filename) throws IOException {
    List<Rml> rmlList = this.rmlclientService.searchByProperties(Map.of("name", name, "version", version));
    if (!rmlList.isEmpty()) {
      return rmlList.get(0);
    }
    final Rml rml = new Rml();
    rml.setName(name);
    rml.setDescription(description);
    rml.setVersion(version);
    rml.setFormat(format);
    new RdfFile();
    String[] path = filename.split("/");
    assertEquals(2, path.length);
    Resource file = this.listFiles(path[0], "/", path[1])[0];
    return this.rmlclientService.createAndUploadRdfFile(rml, file);
  }

  private String getPliegosManifestQuery() {
    final String query = """
            PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
            PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
            PREFIX la: <https://linked.art/ns/terms/>
            PREFIX exif: <https://www.w3.org/2003/12/exif/ns#>
            PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo#>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX owl: <http://www.w3.org/2002/07/owl#>

            SELECT ?resource_subject ?img_subject ?manifest_md_place_name ?manifest_md_publisher ?manifest_md_begin_date ?manifest_md_end_date ?page_id ?label ?img_height ?img_width ?img_iiif_url
            WHERE {
              ?resource_subject a crm:E22_Human-Made_Object .
              ?resource_subject crm:P46_is_composed_of ?page .
              ?page la:digitally_shown_by  ?img_subject .
              ?pn_id crm:P1_identifies ?page .
              ?pn_id rdf:value ?page_id .
              ?img_subject a crmdig:D1_Digital_Object .
              ?img_subject crmdig:L54_is_same-as ?img_iiif_url .
              optional {
                ?resource_subject crm:P1_is_identified_by ?label_appellation .
                ?label_appellation a crm:E41_Appellation .
                ?label_appellation crm:P190_has_symbolic_content ?label
              } .
              optional { ?img_subject exif:height ?img_height ;
                                      exif:width ?img_width }.
              ?production a frbroo:F30_Publication_Event .
              ?production crm:P108_has_produced ?resource_subject .
              ?production crm:P14_carried_out_by ?actor .
              ?production  crm:P4_has_time-span ?time_span .
              ?time_span crm:P82a_begin_of_the_begin ?manifest_md_begin_date .
              optional{
                  ?time_span crm:P82b_end_of_the_begin ?manifest_md_end_date
              }
              optional {
                ?actor crm:P1_is_identified_by [ crm:P190_has_symbolic_content ?manifest_md_publisher ] .
                ?actor crm:P74_has_former_or_current_residence ?place .
                ?place crm:P1_is_identified_by [
                  crm:P190_has_symbolic_content ?manifest_md_place_name
                ]
              }
            }
            ORDER BY ?img_iiif_url
            """;
    return query;
  }

  private String getGypsoManifestQuery() {
    final String query = """
            PREFIX gypso: <https://ld.hedera.unige.ch/ontologies/gypso#>
            PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
            PREFIX exif: <https://www.w3.org/2003/12/exif/ns#>

            SELECT ?resource_subject ?img_subject ?img_iiif_url ?location_name ?label ?img_height ?img_width
            WHERE {
              ?resource_subject a gypso:SpecSheet .
              ?resource_subject gypso:name ?label .
              OPTIONAL {
                ?resource_subject gypso:containsFile ?img_subject .
                ?img_subject a gypso:File.
                ?img_subject crmdig:L54_is_same-as ?img_iiif_url .
                FILTER CONTAINS( STR(?img_iiif_url), "iiif") .
                ?img_subject exif:height ?img_height .
                ?img_subject exif:width ?img_width .
              }
              OPTIONAL {
                ?resource_subject gypso:inMuseum ?location .
                ?location gypso:name ?location_name
              }
            }
            ORDER BY ?img_iiif_url
            """;
    return query;
  }

  private String getLudusManifestQuery() {
    final String query = """
            PREFIX lud: <https://ld.hedera.unige.ch/ontologies/ludusdata#>
            PREFIX exif: <https://www.w3.org/2003/12/exif/ns#>

            SELECT ?resource_subject ?img_subject ?begin_date ?end_date ?location_name ?label ?img_iiif_url ?img_height ?img_width
            WHERE {
              ?resource_subject a lud:media .
              ?resource_subject lud:contains_file ?img_subject .
              ?resource_subject lud:has_title ?label .
              ?img_subject a lud:file .
              OPTIONAL {
                ?img_subject exif:height ?img_height .
                ?img_subject exif:width ?img_width .
              }
              OPTIONAL { ?resource_subject lud:has_date ?ludus_data
                    OPTIONAL {?ludus_data lud:has_beginning ?begin_date } .
                    OPTIONAL {?ludus_data lud:has_end ?end_date } .
              }
              OPTIONAL {
                ?resource_subject lud:has_location ?location .
                ?location lud:has_name ?location_name
              }
            }
            ORDER BY ?img_iiif_url
            """;
    return query;
  }

  private String getPoesie16ManifestQuery() {
    final String query = """
            PREFIX poesie1600: <https://ld.hedera.unige.ch/ontologies/poesie1600#>
            PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
            PREFIX exif: <https://www.w3.org/2003/12/exif/ns#>

            SELECT ?resource_subject ?img_subject ?img_iiif_url ?img_height ?img_width ?date ?location_name ?transcription ?label
            WHERE {
              ?resource_subject a poesie1600:Edition .
              ?resource_subject poesie1600:titreEdition ?label .
              ?resource_subject poesie1600:containsFile ?img_subject .
              OPTIONAL { ?resource_subject poesie1600:transcription ?transcription } .
              OPTIONAL { ?resource_subject poesie1600:lieu ?location_name } .
              ?img_subject a poesie1600:File .
              ?img_subject crmdig:L54_is_same-as ?img_iiif_url .
              FILTER CONTAINS( STR(?img_iiif_url), "iiif") .
              ?img_subject exif:width ?img_width .
              ?img_subject exif:height ?img_height .
              OPTIONAL { ?resource_subject poesie1600:dateEdition ?date } .

            }
            ORDER BY ?img_iiif_url
            """;
    return query;
  }

  private String getRoma16ManifestQuery() {
    final String query = """
            PREFIX roma1600: <https://ld.hedera.unige.ch/ontologies/roma1600#>
            PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
            PREFIX exif: <https://www.w3.org/2003/12/exif/ns#>

            SELECT ?resource_subject ?img_subject ?img_iiif_url ?date ?label ?img_height ?img_width ?titre_note
            WHERE {
              ?resource_subject a roma1600:Source .
              ?piece roma1600:aSource ?resource_subject .
              ?fiche roma1600:aPieceManuscrite ?piece .
              ?fiche roma1600:containsFile ?img_subject .
              ?img_subject a roma1600:File .
              ?img_subject crmdig:L54_is_same-as ?img_iiif_url .
              FILTER CONTAINS( STR(?img_iiif_url), "iiif") .
              ?img_subject exif:height ?img_height .
              ?img_subject exif:width ?img_width .
              OPTIONAL { ?resource_subject roma1600:datation ?date} .
              OPTIONAL { ?piece roma1600:titreNote ?titre_note} .
              ?resource_subject roma1600:titreSource ?label
            }
            ORDER BY ?img_iiif_url
            """;
    return query;
  }

  private String getArcheoManifestQuery() {
    final String query = """
            PREFIX archeo: <https://ld.hedera.unige.ch/ontologies/archeo#>
            PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
            PREFIX exif: <https://www.w3.org/2003/12/exif/ns#>

            SELECT ?resource_subject ?img_subject ?img_iiif_url ?img_height ?img_width ?date ?location_name ?description ?label
            WHERE {
              ?resource_subject a archeo:Image .
              ?resource_subject archeo:containsFile ?img_subject .
              ?img_subject a archeo:File .
              ?img_subject crmdig:L54_is_same-as ?img_iiif_url .
              FILTER CONTAINS( STR(?img_iiif_url), "iiif") .
              ?img_subject exif:width ?img_width .
              ?img_subject exif:height ?img_height .
              ?resource_subject archeo:description ?description .
              OPTIONAL {
                  ?resource_subject archeo:lienLocalisation ?location .
                  ?location a archeo:Localisation .
                OPTIONAL {
                  ?location archeo:nomLieu ?location_name .
                }
              }
              OPTIONAL {
                ?resource_subject archeo:lienChrono ?chrono .
                OPTIONAL {
                ?chrono archeo:datation ?date .
                }
              }
              ?resource_subject archeo:nomImage ?label
            }
            ORDER BY ?img_iiif_url
            """;
    return query;
  }

  private String getHistArtDiaManifestQuery() {
    final String query = """
            PREFIX histartdia: <https://ld.hedera.unige.ch/ontologies/histartdia#>
            PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
            PREFIX exif: <https://www.w3.org/2003/12/exif/ns#>

            SELECT ?resource_subject ?img_subject ?img_iiif_url ?img_height ?img_width ?date  ?location_name ?description ?label
            WHERE {
              ?resource_subject a histartdia:Fiche .
              ?resource_subject histartdia:titre ?label .
              ?resource_subject histartdia:description ?description .
              ?resource_subject histartdia:containsFile ?img_subject .
              ?img_subject crmdig:L54_is_same-as ?img_iiif_url .
              FILTER CONTAINS( STR(?img_iiif_url), "iiif") .
              ?img_subject exif:height ?img_height .
              ?img_subject exif:width ?img_width .
              OPTIONAL { ?resource_subject histartdia:estDans ?location . } .
              OPTIONAL { ?location a histartdia:Localisation .
              ?location histartdia:lieu ?location_name } .
              ?img_subject a histartdia:File .
              OPTIONAL { ?resource_subject histartdia:datation ?date }
            }
            ORDER BY ?img_iiif_url
            """;
    return query;
  }

  private String getTurrettiniManifestQuery() {
    final String query = """
            PREFIX turrettini: <https://ld.hedera.unige.ch/ontologies/turrettini#>
            PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
            PREFIX exif: <https://www.w3.org/2003/12/exif/ns#>

            SELECT ?resource_subject ?img_subject ?img_iiif_url ?letter_date ?location_name ?label ?img_height ?img_width
            WHERE {
              ?resource_subject a turrettini:Letter .
              ?resource_subject turrettini:description ?label .
              ?resource_subject turrettini:containsBinaryFile ?img_subject .
              ?img_subject a turrettini:File .
              ?img_subject crmdig:L54_is_same-as ?img_iiif_url .
              FILTER CONTAINS( STR(?img_iiif_url), "iiif") .
              ?img_subject exif:height ?img_height .
              ?img_subject exif:width ?img_width .
              OPTIONAL { ?resource_subject turrettini:letterDate ?letter_date } .
              OPTIONAL {
                ?resource_subject turrettini:conservationLocation ?location .
                OPTIONAL { ?location turrettini:locationName ?location_name }
              }
            }
            ORDER BY ?img_iiif_url
            """;
    return query;
  }

  private String getVisualContagionsManifestQuery() {
    final String query = """
            PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
            PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
            PREFIX la: <https://linked.art/ns/terms/>
            PREFIX exif: <https://www.w3.org/2003/12/exif/ns#>
            PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo#>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

            SELECT ?resource_subject ?img_subject ?img_iiif_url ?place_name ?begin_date ?end_date ?label ?img_height ?img_width ?page_id ?country_name ?journal_type ?journal_title ?notice
            WHERE {
              ?resource_subject a crm:E24_Physical_Human-Made_Thing .
              ?resource_subject crm:P46_is_composed_of [
                  crm:P1_is_identified_by [rdf:value ?page_id];
                  crm:P129i_is_subject_of [
                    exif:width ?img_width;
                    exif:height ?img_height
                ];
              ] .
              ?resource_subject crm:P128_carries [
                  la:digitally_shown_by ?img_subject
              ] .
              ?img_subject a crmdig:D1_Digital_Object .
              ?production a frbroo:F30_Publication_Event .
              ?production crm:P108_has_produced ?resource_subject .
              optional { ?resource_subject crm:P1_is_identified_by [ crm:P190_has_symbolic_content ?label ] }
              ?production crm:P14_carried_out_by ?actor .
              ?production frbroo:R23_created_a_realization_of ?serial_work
              OPTIONAL { ?serial_work crm:P1_is_identified_by [ a crm:E41_Appellation; crm:P190_has_symbolic_content ?journal_title] }
              OPTIONAL { ?serial_work  crm:P1_is_identified_by [ a crm:E42_Identifier; rdf:value ?notice]      }
              OPTIONAL { ?serial_work    crm:P2_has_type [rdf:value ?journal_type]   }
              OPTIONAL { ?production crm:P4_has_time-span [ crm:P82a_begin_of_the_begin ?begin_date ]; }
              OPTIONAL {
                ?production crm:P4_has_time-span [ crm:P82b_end_of_the_begin ?end_date ];  }
                ?actor crm:P74_has_former_or_current_residence ?place .
                OPTIONAL { ?place crm:P1_is_identified_by [crm:P190_has_symbolic_content ?place_name]}
                OPTIONAL {?place crm:P89_falls_within [ crm:P1_is_identified_by [crm:P190_has_symbolic_content ?country_name ]]
              }
            }
            ORDER BY ?img_iiif_url
            """;
    return query;
  }

  private String getMAHManifestQuery() {
    final String query = """
            PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
            PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
            PREFIX vir: <https://ncarboni.github.io/vir/>
            PREFIX la: <https://linked.art/ns/terms/>
            PREFIX exif: <https://www.w3.org/2003/12/exif/ns#>
            PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo#>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

            SELECT ?resource_subject ?img_subject ?img_iiif_url ?img_height ?img_width ?begin_date ?end_date ?label ?notice ?denomination ?material ?inventory_nbr ?img_filename ?mention_obl ?copyright ?access_right ?dwd_id ?dplace_name ?dcountry_name ?cwd_id ?cplace_name ?ccountry_name  ?artist_name ?artist_wd ?artist_ulan ?death_place_name ?death_country_name ?death_wd_id ?death_date ?birth_place_name ?birth_country_name ?birth_wd_id ?birth_date ?diameter ?dim_h ?dim_w ?dim_d ?weight ?tag ?type_oeuvre ?img_width ?img_height
            WHERE {
              ?resource_subject a crm:E24_Physical_Human-Made_Thing .
              ?resource_subject crm:P2_has_type [crm:P190_has_symbolic_content ?type_oeuvre] .
              ?resource_subject crm:P128_carries [
                  la:digitally_shown_by ?img_subject
              ] .
              OPTIONAL { ?img_subject exif:height ?img_height ;
                                      exif:width ?img_width }.
              ?img_subject a crmdig:D1_Digital_Object .
              ?img_subject rdf:value ?img_filename .
              ?resource_subject crm:P108i_was_produced_by ?production .
              ?production a crm:E12_Production .

              ?resource_subject crm:P1_is_identified_by [ a crm:E42_Identifier; crm:P190_has_symbolic_content ?inventory_nbr ] .
              OPTIONAL {?resource_subject crm:P1_is_identified_by [ a crm:E41_Appellation; crm:P190_has_symbolic_content ?label ] .}
              OPTIONAL { ?resource_subject crm:P129i_is_subject_of [rdf:value ?denomination]}
              ?resource_subject crm:P45_consists_of [crm:P190_has_symbolic_content ?material] .

              ?resource_subject crm:P8i_witnessed ?activity
              OPTIONAL {
                ?activity crm:P7_took_place_at ?dplace .
                ?dplace crm:P1_is_identified_by [crm:P190_has_symbolic_content ?dplace_name] .
                ?dplace crm:P89_falls_within [crm:P1_is_identified_by [crm:P190_has_symbolic_content ?dcountry_name]] .
                ?dplace crmdig:L54_is_same-as ?dwd_id
              }

              OPTIONAL {
                ?production crm:P7_took_place_at ?cplace .
                ?cplace crm:P1_is_identified_by [crm:P190_has_symbolic_content ?cplace_name] .
                ?cplace crm:P89_falls_within [crm:P1_is_identified_by [crm:P190_has_symbolic_content ?ccountry_name]] .
                ?cplace crmdig:L54_is_same-as ?cwd_id
              }

              ?resource_subject crm:P62_depicts [crm:P105_right_held_by [
                    crm:P3_has_note ?mention_obl;
                    crm:P75i_is_possessed_by [crm:P190_has_symbolic_content ?copyright];
                    crm:P2_has_type [crm:P190_has_symbolic_content ?access_right]
                    ]
              ] .

              OPTIONAL {?production  crm:P4_has_time-span ?timespan } .
              OPTIONAL {?timespan crm:P82a_begin_of_the_begin ?begin_date} .
              optional {?timespan crm:P82b_end_of_the_begin ?end_date} .
              ?production crm:P1_is_identified_by [ crm:P190_has_symbolic_content ?notice ] .

             OPTIONAL {
                ?production crm:P14_carried_out_by ?artist .
                   ?artist crm:P1_is_identified_by [ crm:P190_has_symbolic_content ?artist_name ] .
                OPTIONAL{?artist crmdig:L54_is_same-as ?artist_wd . filter(regex(str(?artist_wd), "wikidata", "i"))}
                OPTIONAL{?artist crmdig:L54_is_same-as ?artist_ulan . filter(regex(str(?artist_ulan), "getty", "i"))}
                OPTIONAL{?artist crm:P100i_die_in ?death .
                  ?death crm:P7_took_place_at[
                   crm:P1_is_identified_by [crm:P190_has_symbolic_content ?death_place_name];
                   crm:P89_falls_within [crm:P1_is_identified_by [crm:P190_has_symbolic_content ?death_country_name]];
                   crmdig:L54_is_same-as ?death_wd_id
                  ] .
                  ?death crm:P4_has_time-span [ crm:P82a_begin_of_the_begin ?death_date]
                }
                 OPTIONAL{?artist crm:P89i_was_born ?birth .
                  ?birth crm:P7_took_place_at[
                     crm:P1_is_identified_by [crm:P190_has_symbolic_content ?birth_place_name];
                     crm:P89_falls_within [crm:P1_is_identified_by [crm:P190_has_symbolic_content ?birth_country_name]];
                     crmdig:L54_is_same-as ?birth_wd_id
                    ] .
                  ?birth crm:P4_has_time-span [ crm:P82a_begin_of_the_begin ?birth_date]
                }
             }

              OPTIONAL{?resource_subject crm:P43_has_dimension [crm:P2_has_type <http://vocab.getty.edu/aat/300055624>; crm:P90_has_value ?diameter ]}
              OPTIONAL{?resource_subject crm:P43_has_dimension [crm:P2_has_type <http://vocab.getty.edu/aat/300072633>; crm:P90_has_value ?dim_d ]}
              OPTIONAL{?resource_subject crm:P43_has_dimension [crm:P2_has_type <http://vocab.getty.edu/aat/300055644>; crm:P90_has_value ?dim_h ]}
              OPTIONAL{?resource_subject crm:P43_has_dimension [crm:P2_has_type <http://vocab.getty.edu/aat/300055647>; crm:P90_has_value ?dim_w ]}
              OPTIONAL{?resource_subject crm:P43_has_dimension [crm:P2_has_type <http://vocab.getty.edu/aat/300254280>; crm:P90_has_value ?weight ]}
            }
            ORDER BY ?img_iiif_url
            """;
    return query;
  }

  private String getGypsoManifestWithFileQuery() {
    final String query = """
            PREFIX gypso: <https://ld.hedera.unige.ch/ontologies/gypso#>
            PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
            PREFIX exif: <https://www.w3.org/2003/12/exif/ns#>

            SELECT ?resource_subject ?img_subject ?img_iiif_url  ?location_name ?label ?img_height ?img_width
            WHERE {
              ?resource_subject a gypso:SpecSheet .
              ?resource_subject gypso:name ?label .
              ?resource_subject gypso:containsFile ?img_subject .
              ?img_subject a gypso:File.
              ?img_subject crmdig:L54_is_same-as ?img_iiif_url .
              FILTER CONTAINS( STR(?img_iiif_url), "iiif") .
              ?img_subject exif:height ?img_height .
              ?img_subject exif:width ?img_width .
              OPTIONAL {
                ?resource_subject gypso:inMuseum ?location .
                ?location gypso:name ?location_name
              }
            }
            ORDER BY ?img_iiif_url
            """;
    return query;
  }
}
