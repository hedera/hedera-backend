/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - CleanIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.clean;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.service.admin.FundingAgencyClientService;
import ch.hedera.service.admin.GlobalBannerClientService;
import ch.hedera.service.admin.InstitutionClientService;
import ch.hedera.test.admin.AbstractAdminIT;
import ch.hedera.test.service.PersonITService;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;

@Order(Integer.MAX_VALUE)
class CleanIT extends AbstractAdminIT {

  protected PersonITService personITService;

  protected ProjectITService projectITService;

  protected ResearchDataFileITService researchDataFileITService;

  protected ResearchObjectTypeITService researchObjectTypeITService;

  @Autowired
  public CleanIT(Environment env, SudoRestClientTool restClientTool,
          FundingAgencyClientService fundingAgencyService,
          InstitutionClientService institutionService,
          GlobalBannerClientService globalBannerService,
          PersonITService personITService,
          ProjectITService projectITService) {
    super(env, restClientTool, fundingAgencyService, institutionService, globalBannerService);
    this.personITService = personITService;
    this.projectITService = projectITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void cleanTest() {
    // Need to have at least one test
    assertTrue(true);
  }

  @Override
  protected void deleteFixtures() {
    this.clearLicenseFixtures();
    this.personITService.cleanTestData();
    this.clearInstitutionsFixtures();
  }
}
