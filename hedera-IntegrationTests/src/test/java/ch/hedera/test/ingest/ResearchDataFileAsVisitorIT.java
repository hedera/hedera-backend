/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ResearchDataFileAsVisitorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static ch.hedera.test.HederaTestConstants.REFERENCE_FILE_TO_UPLOAD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.core.JsonProcessingException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.ResearchDataFile.ResearchDataFileFileStatus;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;

@Order(HederaTestConstants.INGEST_TEST_ORDER
        + HederaTestConstants.INGEST_RESEARCH_DATA_FILE_TEST_ORDER
        + HederaTestConstants.VISITOR_TEST_ORDER)
@Tag(HederaTestConstants.AS_VISITOR_TAG)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ResearchDataFileAsVisitorIT extends AbstractResearchDataFileAsCreatorIT {
  @Autowired
  public ResearchDataFileAsVisitorIT(Environment env, SudoRestClientTool restClientTool,
          ProjectITService projectITService, SourceDatasetITService sourceDatasetITService,
          SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
  }

  @Override
  protected String role() {
    return Role.VISITOR_ID;
  }

  @Override
  @Test
  void uploadResearchDataFileWithSameNameTest() {
    this.uploadResearchDataFileWithSameNameTest(HttpClientErrorException.Forbidden.class);
  }

  @Override
  @Test
  void uploadResearchDataFileTest() {
    this.restClientTool.sudoAdmin();
    Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, this.role(),
            AuthApplicationRole.USER);
    this.restClientTool.exitSudo();

    // Visitor test
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.researchDataFileClientService.uploadFile(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
                    project.getResId(), DataAccessibilityType.IIIF.getName(), null));
  }

  @Override
  @Test
  void downloadTest() throws IOException {
    this.restClientTool.sudoAdmin();

    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, this.role(),
            AuthApplicationRole.USER);

    final ClassPathResource fileToUpload = new ClassPathResource(REFERENCE_FILE_TO_UPLOAD);
    final Long expectedFileSize = this.checkFile(fileToUpload);

    // Create a researchDataFileCreated
    final ResearchDataFile researchDataFileCreated = this.uploadAndWaitResearchDataFileProcessing(
            fileToUpload,
            project.getResId(),
            DataAccessibilityType.IIIF,
            null,
            ResearchDataFileFileStatus.COMPLETED);

    this.restClientTool.exitSudo();

    // Visitor test
    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFileCreated.getResId());

    assertEquals(researchDataFileCreated.getProjectId(), researchDataFileFound.getProjectId());

    // Download the file
    Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.tmp");
    this.researchDataFileClientService.downloadResearchDataFile(researchDataFileFound.getResId(), path);

    assertEquals(expectedFileSize, path.toFile().length());
  }

  @Override
  @Test
  void deleteTest() {
    this.restClientTool.sudoAdmin();

    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, this.role(),
            AuthApplicationRole.USER);

    // Create a researchDataFileCreated
    final ResearchDataFile researchDataFileCreated = this.uploadAndWaitResearchDataFileProcessing(
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(),
            DataAccessibilityType.IIIF,
            null,
            ResearchDataFileFileStatus.COMPLETED);

    this.restClientTool.exitSudo();

    // Visitor test
    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFileCreated.getResId());

    assertEquals(researchDataFileCreated.getProjectId(), researchDataFileFound.getProjectId());
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.researchDataFileClientService.delete(researchDataFileFound.getResId()));
  }

  @Override
  @ParameterizedTest
  @MethodSource("zipFiles")
  void deleteListTest(String zipFile, int fileNumber) {
    this.restClientTool.sudoAdmin();
    Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, this.role(),
            AuthApplicationRole.USER);

    List<ResearchDataFile> result = this.researchDataFileClientService.uploadZipFile(new ClassPathResource(zipFile), project.getResId(),
            DataAccessibilityType.IIIF.getName());
    assertEquals(fileNumber, result.size());
    assertTrue(this.researchDataFileITService.waitAllResearchDataFilesIsProcessed(project.getResId(), fileNumber));

    this.restClientTool.exitSudo();
    // Now we list all the files and delete all of them
    final List<ResearchDataFile> researchDataFileList = this.researchDataFileClientService.findAllWithProjectId(project.getResId());
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.researchDataFileClientService.deleteList(
            researchDataFileList.stream().map(ResearchDataFile::getResId).toArray(String[]::new)));
  }

  @Override
  @Test
  void deleteFolderTest() throws JsonProcessingException {
    this.restClientTool.sudoAdmin();
    Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, this.role(),
            AuthApplicationRole.USER);

    // Create a researchDataFile
    final ResearchDataFile researchDataFile1 = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(), DataAccessibilityType.IIIF, null, ResearchDataFileFileStatus.COMPLETED);
    final ResearchDataFile researchDataFile2 = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource("geneve.jpg"),
            project.getResId(), DataAccessibilityType.IIIF, null, ResearchDataFileFileStatus.COMPLETED);

    ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFile1.getResId());
    ResearchDataFile researchDataFileFound2 = this.researchDataFileClientService.findOne(researchDataFile2.getResId());

    researchDataFileFound.setRelativeLocation("/other");
    ResearchDataFile researchDataFileUpdated = this.researchDataFileClientService.update(researchDataFileFound.getResId(),
            researchDataFileFound);

    String folderName = "/other";
    researchDataFileFound2.setRelativeLocation(folderName);
    ResearchDataFile researchDataFileUpdated2 = this.researchDataFileClientService.update(researchDataFileFound2.getResId(),
            researchDataFileFound2);
    this.researchDataFileITService.waitResearchDataFileIsProcessed(researchDataFileUpdated.getResId(), ResearchDataFileFileStatus.COMPLETED);
    this.researchDataFileITService.waitResearchDataFileIsProcessed(researchDataFileUpdated2.getResId(), ResearchDataFileFileStatus.COMPLETED);

    this.restClientTool.exitSudo();

    List<String> listFolders = this.researchDataFileClientService.listFolders(project.getResId());
    assertEquals(listFolders.size(), 1);
    assertTrue(listFolders.contains(folderName));

    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.researchDataFileClientService.deleteFolder(project.getResId(), folderName));
  }

  @Override
  @Test
  void listFolderTest() throws JsonProcessingException {
    this.restClientTool.sudoAdmin();
    Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, this.role(),
            AuthApplicationRole.USER);

    // Create a researchDataFile
    final ResearchDataFile researchDataFile1 = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(), DataAccessibilityType.WEB, null, ResearchDataFileFileStatus.COMPLETED);
    final ResearchDataFile researchDataFile2 = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource("geneve.jpg"),
            project.getResId(), DataAccessibilityType.IIIF, null, ResearchDataFileFileStatus.COMPLETED);

    ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFile1.getResId());
    ResearchDataFile researchDataFileFound2 = this.researchDataFileClientService.findOne(researchDataFile2.getResId());

    researchDataFileFound.setRelativeLocation("/other");
    ResearchDataFile researchDataFileUpdated = this.researchDataFileClientService.update(researchDataFileFound.getResId(),
            researchDataFileFound);

    researchDataFileFound2.setRelativeLocation("/another");
    ResearchDataFile researchDataFileUpdated2 = this.researchDataFileClientService.update(researchDataFileFound2.getResId(),
            researchDataFileFound2);

    this.researchDataFileITService.waitResearchDataFileIsProcessed(researchDataFileUpdated.getResId(), ResearchDataFileFileStatus.COMPLETED);
    this.researchDataFileITService.waitResearchDataFileIsProcessed(researchDataFileUpdated2.getResId(), ResearchDataFileFileStatus.COMPLETED);

    this.restClientTool.exitSudo();

    assertEquals(researchDataFile1.getProjectId(), researchDataFileUpdated.getProjectId());
    assertEquals(new ClassPathResource("/other"), new ClassPathResource(researchDataFileUpdated.getRelativeLocation()));
    assertEquals(researchDataFile1.getProjectId(), researchDataFileUpdated2.getProjectId());
    assertEquals(new ClassPathResource("/another"), new ClassPathResource(researchDataFileUpdated2.getRelativeLocation()));

    List<String> listFolders = this.researchDataFileClientService.listFolders(project.getResId());
    assertEquals(listFolders.size(), 2);
    assertTrue(listFolders.contains("/other"));
    assertTrue(listFolders.contains("/another"));
  }

  @Override
  @ParameterizedTest
  @MethodSource("zipFiles")
  void reloadTest(String zipFile, int fileNumber) {
    this.restClientTool.sudoAdmin();
    Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, this.role(),
            AuthApplicationRole.USER);

    List<ResearchDataFile> result = this.researchDataFileClientService.uploadZipFile(new ClassPathResource(zipFile), project.getResId(),
            DataAccessibilityType.IIIF.getName());
    // Wait 1s for cache
    SolidifyTime.waitInSeconds(fileNumber);

    assertEquals(fileNumber, result.size());
    assertTrue(this.researchDataFileITService.waitAllResearchDataFilesIsProcessed(project.getResId(), fileNumber));
    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.researchDataFileClientService.reload(project.getResId()));

  }

  @Override
  @ParameterizedTest
  @MethodSource("zipFiles")
  void uploadResearchDataFilesInZipTest(String zipFile, int fileNumber) {
    this.restClientTool.sudoAdmin();
    Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, this.role(),
            AuthApplicationRole.USER);
    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.researchDataFileClientService.uploadZipFile(new ClassPathResource(zipFile), project.getResId(),
                    DataAccessibilityType.IIIF.getName()));

  }

  @Override
  @Test
  void updateTest() {
    this.restClientTool.sudoAdmin();

    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, this.role(),
            AuthApplicationRole.USER);

    // Create a researchDataFileCreated
    final ResearchDataFile researchDataFileCreated = this.uploadAndWaitResearchDataFileProcessing(
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(),
            DataAccessibilityType.IIIF,
            null,
            ResearchDataFileFileStatus.COMPLETED);

    this.restClientTool.exitSudo();

    // Visitor test
    ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFileCreated.getResId());

    assertEquals(researchDataFileCreated.getProjectId(), researchDataFileFound.getProjectId());

    researchDataFileFound.setAccessibleFrom(DataAccessibilityType.TEI);
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.researchDataFileClientService.update(researchDataFileFound.getResId(),
                    researchDataFileFound));

  }

  @Test
  void getAnyResearchDataFileTest() {
    this.restClientTool.sudoAdmin();

    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, this.role(),
            AuthApplicationRole.USER);

    // Create a researchDataFileCreated
    final ResearchDataFile researchDataFileCreated = this.uploadAndWaitResearchDataFileProcessing(
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(),
            DataAccessibilityType.IIIF,
            null,
            ResearchDataFileFileStatus.COMPLETED);

    this.restClientTool.exitSudo();

    // Visitor test
    ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFileCreated.getResId());

    assertEquals(researchDataFileCreated.getProjectId(), researchDataFileFound.getProjectId());
  }
}
