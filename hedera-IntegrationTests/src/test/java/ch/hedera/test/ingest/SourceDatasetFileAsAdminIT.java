/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - SourceDatasetFileAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static ch.hedera.test.HederaTestConstants.REFERENCE_FILE_TO_UPLOAD;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.ingest.DatasetFileType;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;

@Order(HederaTestConstants.INGEST_TEST_ORDER
        + HederaTestConstants.INGEST_SOURCE_DATASET_FILE_TEST_ORDER
        + HederaTestConstants.ADMINISTRATOR_TEST_ORDER)
@Tag(HederaTestConstants.AS_ADMIN_TAG)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SourceDatasetFileAsAdminIT extends AbstractIngestIT {

  @Autowired
  public SourceDatasetFileAsAdminIT(Environment env, SudoRestClientTool restClientTool,
          ProjectITService projectITService, SourceDatasetITService sourceDatasetITService,
          SourceDatasetClientService sourceDatasetClientService, ResearchDataFileITService researchDataFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  protected ApplicationRole applicationRole() {
    return AuthApplicationRole.ADMIN;
  }

  @ParameterizedTest
  @ValueSource(strings = {
          HederaTestConstants.REFERENCE_FILE_TO_UPLOAD,
          "file-without-extention" })
  void uploadSourceDatasetFileTest(String fileToUpload) {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project);

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
            new ClassPathResource(fileToUpload), DatasetFileType.XML.getName(), "1.0");

    final List<SourceDatasetFile> list = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());

    // Test the upload
    assertEquals(1, list.size());
    this.testSourceDatasetFileResult(sourceDataset, sourceDatasetFile, fileToUpload, "1.0", DatasetFileType.XML, list.get(0));
  }

  @ParameterizedTest
  @MethodSource("zipFiles")
  void uploadSourceDatasetFilesInZipTest(String zipFile, int fileNumber) {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project);

    List<SourceDatasetFile> result = this.sourceDatasetClientService.uploadZipFile(sourceDataset.getResId(),
            new ClassPathResource(zipFile), DatasetFileType.XML.getName(), "1.0");
    assertEquals(fileNumber, result.size());

    final List<SourceDatasetFile> list = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());
    assertEquals(fileNumber, list.size());
  }

  @Test
  void uploadSourceDatasetFileWithSameNameAndNewVersionTest() {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project);

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0");

    final List<SourceDatasetFile> list = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());

    // Test the upload
    assertEquals(1, list.size());
    this.testSourceDatasetFileResult(sourceDataset, sourceDatasetFile, REFERENCE_FILE_TO_UPLOAD, "1.0", DatasetFileType.XML, list.get(0));

    final SourceDatasetFile sourceDatasetFile2 = this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "2.0");
    final List<SourceDatasetFile> list2 = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());
    assertEquals(2, list2.size());
    this.testSourceDatasetFileResult(sourceDataset, sourceDatasetFile, REFERENCE_FILE_TO_UPLOAD, "1.0", DatasetFileType.XML, list2.get(0));
    this.testSourceDatasetFileResult(sourceDataset, sourceDatasetFile2, REFERENCE_FILE_TO_UPLOAD, "2.0", DatasetFileType.XML, list2.get(1));
  }

  @Test
  void uploadSourceDatasetFileWithSameNameAndSameVersionTest() {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project);

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0");

    final List<SourceDatasetFile> list = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());

    // Test the upload
    assertEquals(1, list.size());
    this.testSourceDatasetFileResult(sourceDataset, sourceDatasetFile, REFERENCE_FILE_TO_UPLOAD, "1.0", DatasetFileType.XML, list.get(0));

    // Upload in the same DatasetSource a file with same name
    assertThrows(HttpServerErrorException.InternalServerError.class,
            () -> this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
                    new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0"));
  }

  @Test
  void createWithResearchDataFileTest() {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Get Research Data File
    final ResearchDataFile researchDataFile = this.getOrUploadResearchDataFile(project);
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());
    // SourceDataFile with ResearchDataFile
    final SourceDatasetFile sourceDatasetFile = this.createSourceDatasetFileWithResearchDataFile(sourceDataset, researchDataFile, "1.0",
            DatasetFileType.XML);

    final List<SourceDatasetFile> sourceDatasetFilelist = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());

    // Test the upload
    assertEquals(1, sourceDatasetFilelist.size());
    this.testSourceDatasetFileResult(sourceDataset, sourceDatasetFile, HederaTestConstants.FIRST_SOURCE_DATASET_FILE_FILENAME, "1.0",
            DatasetFileType.XML,
            sourceDatasetFilelist.get(0));
    // Test if the research data file cannot be deleted
    assertThrows(HttpServerErrorException.InternalServerError.class,
            () -> this.researchDataFileClientService.delete(researchDataFile.getResId()));

    // Delete source dataset file
    this.sourceDatasetClientService.removeSourceDatasetFile(sourceDataset.getResId(), sourceDatasetFile.getResId());
    final List<SourceDatasetFile> list2 = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());
    assertTrue(list2.isEmpty());

    // Delete research data file
    assertDoesNotThrow(() -> this.researchDataFileClientService.delete(researchDataFile.getResId()));
  }

  @Test
  void createWithResearchDataFilesTest() {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Get Research Data File
    final ResearchDataFile researchDataFile = this.getOrUploadResearchDataFile(project);
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());
    this.createSourceDatasetFileWithResearchDataFile(sourceDataset, researchDataFile, "1.0", DatasetFileType.XML);
    this.createSourceDatasetFileWithResearchDataFile(sourceDataset, researchDataFile, "2.0", DatasetFileType.XML);

    final List<SourceDatasetFile> sourceDatasetFilelist = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());

    // Test the upload
    assertEquals(2, sourceDatasetFilelist.size());
    // Test if the research data file cannot be deleted
    assertThrows(HttpServerErrorException.InternalServerError.class,
            () -> this.researchDataFileClientService.delete(researchDataFile.getResId()));
  }

  @Test
  void downloadTest() throws IOException {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project);

    final ClassPathResource fileToUpload = new ClassPathResource(REFERENCE_FILE_TO_UPLOAD);
    final Long expectedFileSize = this.checkFile(fileToUpload);

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
            fileToUpload, DatasetFileType.XML.getName(), "1.0");

    // Download the file
    Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.tmp");
    this.sourceDatasetClientService.downloadSourceDatasetFile(sourceDataset.getResId(),
            sourceDatasetFile.getResId(), path);

    assertEquals(expectedFileSize, path.toFile().length());
  }

  @Test
  void deleteTest() {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project);

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0");

    final List<SourceDatasetFile> list = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());

    // Test the upload
    assertEquals(1, list.size());
    this.testSourceDatasetFileResult(sourceDataset, sourceDatasetFile, REFERENCE_FILE_TO_UPLOAD, "1.0", DatasetFileType.XML, list.get(0));

    this.sourceDatasetClientService.removeSourceDatasetFile(sourceDataset.getResId(), sourceDatasetFile.getResId());
    final List<SourceDatasetFile> list2 = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());
    assertTrue(list2.isEmpty());
  }

  @Test
  void updateTest() {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project);

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0");

    final List<SourceDatasetFile> list = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());

    // Test the upload
    assertEquals(1, list.size());
    this.testSourceDatasetFileResult(sourceDataset, sourceDatasetFile, REFERENCE_FILE_TO_UPLOAD, "1.0", DatasetFileType.XML, list.get(0));

    // Update version of that SourceDatasetFile
    this.sourceDatasetClientService.updateSourceDatasetFile(sourceDataset.getResId(), sourceDatasetFile.getResId(),
            Map.of("version", "2.0"));

    final List<SourceDatasetFile> list2 = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());

    // Test the upload
    assertEquals(1, list2.size());
    this.testSourceDatasetFileResult(sourceDataset, sourceDatasetFile, REFERENCE_FILE_TO_UPLOAD, "2.0", DatasetFileType.XML, list2.get(0));
  }

  @Test
  void advancedSearchTest() {
    final String anotherFileToUpload = "LICENSE.txt";

    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project);

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0");

    final SourceDatasetFile sourceDatasetFile2 = this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
            new ClassPathResource(anotherFileToUpload), DatasetFileType.XML.getName(), "2.0");

    assertTrue(this.isSourceDataFilePresent("i-version:1.0", null, sourceDataset.getResId(), sourceDatasetFile));
    assertTrue(this.isSourceDataFilePresent("i-version:2.0", null, sourceDataset.getResId(), sourceDatasetFile2));
  }

  @Test
  void closedProjectTest() {
    Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    project = this.projectITService.openProject(project);
    assertTrue(project.isOpen());
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project);
    this.projectITService.waitForProjectHasData(project.getResId(), true);
    // Close project
    project = this.projectITService.close(project);
    assertFalse(project.isOpen());
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0"));
  }

  private SourceDataset createSourceDataset(Project project) {
    final SourceDataset sourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(),
            this.applicationRole());
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);

    // Test the creation
    assertEquals(sourceDataset.getName(), sourceDatasetCreated.getName());

    final SourceDataset sourceDatasetFound = this.sourceDatasetClientService.findOne(sourceDatasetCreated.getResId());

    assertEquals(sourceDataset.getName(), sourceDatasetFound.getName());
    return sourceDatasetFound;
  }

  private boolean isSourceDataFilePresent(String search, String matchType, String sourceDatasetId, SourceDatasetFile sourceDatasetFile) {
    final List<SourceDatasetFile> sourceDatasetFileList = this.sourceDatasetClientService.search(sourceDatasetId, search, matchType);
    for (final SourceDatasetFile sdf : sourceDatasetFileList) {
      if (sdf.getResId().equals(sourceDatasetFile.getResId())) {
        return true;
      }
    }
    return false;
  }

}
