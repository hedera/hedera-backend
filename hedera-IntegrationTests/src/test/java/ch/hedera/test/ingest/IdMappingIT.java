/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - IdMappingIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static ch.hedera.test.HederaTestConstants.REFERENCE_FILE_TO_UPLOAD;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;

public class IdMappingIT extends AbstractIngestIT {

  @Autowired
  public IdMappingIT(Environment env, SudoRestClientTool restClientTool,
          ProjectITService projectITService, SourceDatasetITService sourceDatasetITService,
          SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void deleteTest() {
    // Create project
    Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, null, AuthApplicationRole.ROOT);

    String projectShortName = project.getShortName();
    final String projectId = project.getResId();
    // Create a researchDataFile
    final ResearchDataFile researchDataFile = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(), DataAccessibilityType.IIIF, null, ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);

    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFile.getResId());

    String hederaId = researchDataFileFound.getHederaId();
    assertNotNull(hederaId);

    this.researchDataFileClientService.delete(researchDataFileFound.getResId());

    // Check if RdfDatasetFile is deleted
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.researchDataFileClientService.findOne(researchDataFileFound.getResId()),
            "ResearchDataFile not deleted");

    this.projectITService.waitForProjectHasData(projectId, false);

    this.projectITService.delete(projectId);
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.projectITService.findOne(projectId),
            "Project not deleted");

    SolidifyTime.waitInSeconds(5);

    // Create new project
    project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, null, AuthApplicationRole.ROOT);
    project.setShortName(projectShortName);

    final String projectId2 = project.getResId();

    project = this.projectITService.update(projectId2, project);

    // Create a researchDataFile
    final ResearchDataFile researchDataFile2 = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            projectId2, DataAccessibilityType.IIIF, null, ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);

    final ResearchDataFile researchDataFileFound2 = this.researchDataFileClientService.findOne(researchDataFile2.getResId());

    assertNotEquals(hederaId, researchDataFileFound2.getHederaId());

    this.researchDataFileClientService.delete(researchDataFileFound2.getResId());

    // Check if RdfDatasetFile is deleted
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.researchDataFileClientService.findOne(researchDataFileFound2.getResId()),
            "ResearchDataFile not deleted");

    this.projectITService.waitForProjectHasData(projectId2, false);

    this.projectITService.delete(projectId2);
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.projectITService.findOne(projectId2),
            "Project not deleted");
  }
}
