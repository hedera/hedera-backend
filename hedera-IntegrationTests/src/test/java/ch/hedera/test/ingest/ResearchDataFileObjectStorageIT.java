/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ResearchDataFileObjectStorageIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.ResearchDataFile.ResearchDataFileFileStatus;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;

class ResearchDataFileObjectStorageIT extends AbstractIngestIT {

  @Autowired
  public ResearchDataFileObjectStorageIT(Environment env, SudoRestClientTool restClientTool, ProjectITService projectITService,
          SourceDatasetITService sourceDatasetITService, SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService, ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService, ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  protected ApplicationRole applicationRole() {
    return AuthApplicationRole.ADMIN;
  }

  @Test
  void createAndDeleteResearchDataFileTest() {

    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, Role.MANAGER_ID,
            this.applicationRole());

    String fileToUpload = "geneve.jpg";
    // Create a researchDataFile
    String relativeLocation = "/";

    final ResearchDataFile researchDataFile = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(fileToUpload),
            project.getResId(), DataAccessibilityType.IIIF, relativeLocation, ResearchDataFileFileStatus.COMPLETED);

    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFile.getResId());

    assertEquals(researchDataFileFound.getResId(), researchDataFile.getResId());
    assertEquals(DataAccessibilityType.IIIF.getName(), researchDataFileFound.getAccessibleFrom().getName());
    assertNotNull(researchDataFileFound.getMetadata());
    assertNotNull(researchDataFileFound.getRdfMetadata());

    // Download the researchDataFile from S3 storage
    final ClassPathResource fileUploaded = new ClassPathResource(fileToUpload);
    final Long expectedFileSize = this.checkFile(fileUploaded);

    Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.tmp");
    this.researchDataFileClientService.downloadResearchDataFile(researchDataFileFound.getResId(), path);

    assertEquals(expectedFileSize, path.toFile().length());

    // Delete the research data file
    this.researchDataFileClientService.delete(researchDataFileFound.getResId());

    // Wait for JMS queue to be treated
    SolidifyTime.waitOneSecond();

    // Download again with rising exception to test that it was correctly deleted
    assertThrows(HttpClientErrorException.NotFound.class,
            () -> this.researchDataFileClientService.downloadResearchDataFile(researchDataFileFound.getResId(), path));

    // Delete project
    this.projectITService.delete(project.getResId());

  }

  @Override
  protected void deleteFixtures() {
    super.deleteFixtures();
  }

}
