/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - ResearchDataFileAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static ch.hedera.model.ResourceIdentifierType.ACRONYM;
import static ch.hedera.test.HederaTestConstants.REFERENCE_FILE_TO_UPLOAD;
import static ch.hedera.test.HederaTestConstants.RESEARCH_DATAFILE_TO_UPLOAD;
import static ch.hedera.test.HederaTestConstants.SOURCE_DATASET_FILE_TO_UPLOAD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.fasterxml.jackson.core.JsonProcessingException;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.humanities.Rml;
import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.DatasetFileType;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.ResearchDataFile.ResearchDataFileFileStatus;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.model.triplestore.SparqlResultRow;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.admin.TriplestoreQueriesClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.HederaTestConstants.ProjectStatus;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.RmlITService;
import ch.hedera.test.service.SourceDatasetITService;

@Order(HederaTestConstants.INGEST_TEST_ORDER + HederaTestConstants.INGEST_RESEARCH_DATA_FILE_TEST_ORDER
        + HederaTestConstants.ADMINISTRATOR_TEST_ORDER)
@Tag(HederaTestConstants.AS_ADMIN_TAG)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ResearchDataFileAsAdminIT extends AbstractResearchDataFileBaseIT {

  private final TriplestoreQueriesClientService triplestoreQueriesClientService;

  private final RmlITService rmlITService;

  @Autowired
  public ResearchDataFileAsAdminIT(Environment env, SudoRestClientTool restClientTool, ProjectITService projectITService,
          SourceDatasetITService sourceDatasetITService,
          SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService,
          RmlITService rmlITService,
          TriplestoreQueriesClientService triplestoreQueriesClientService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
    this.rmlITService = rmlITService;
    this.triplestoreQueriesClientService = triplestoreQueriesClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  protected ApplicationRole applicationRole() {
    return AuthApplicationRole.ADMIN;
  }

  @ParameterizedTest
  @ValueSource(strings = { HederaTestConstants.REFERENCE_FILE_TO_UPLOAD, "geneve.jpg", "MyUNIGE.pdf", "sub_folder/geneve.jpg",
          "sub_folder/MyUNIGE.pdf", "gen.eve.jpg", "geneve(w-extension).jpg", "geneve-wo-extension", "geneve(wo)extension" })
  void uploadResearchDataFileTest(String fileToUpload) {
    Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a researchDataFile
    String relativeLocation = "/";
    if (fileToUpload.contains("/")) {
      relativeLocation += fileToUpload.substring(0, fileToUpload.lastIndexOf("/"));
    }

    final ResearchDataFile researchDataFile = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(fileToUpload),
            project.getResId(), DataAccessibilityType.IIIF, relativeLocation, ResearchDataFileFileStatus.COMPLETED);

    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFile.getResId());

    // Test the upload
    this.testResult(project, researchDataFile, researchDataFileFound, fileToUpload);
  }

  @ParameterizedTest
  @MethodSource("zipFiles")
  void uploadResearchDataFilesInZipTest(String zipFile, int fileNumber) {
    Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    this.uploadResearchDataFilesFromZipTest(project, zipFile, fileNumber);
  }

  @ParameterizedTest
  @MethodSource("zipFiles")
  void reloadTest(String zipFile, int fileNumber) {
    Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    this.reloadTest(project, zipFile, fileNumber);
  }

  @Test
  void uploadResearchDataFileWithSameNameTest() {
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a researchDataFile
    final ResearchDataFile researchDataFile = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(), DataAccessibilityType.IIIF, null, ResearchDataFileFileStatus.COMPLETED);

    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFile.getResId());

    // Test the upload
    this.testResult(project, researchDataFile, researchDataFileFound, REFERENCE_FILE_TO_UPLOAD);

    // Upload in the same ResearchDataFile a file with same name
    assertThrows(HttpServerErrorException.InternalServerError.class,
            () -> this.researchDataFileClientService.uploadFile(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), project.getResId(),
                    DataAccessibilityType.IIIF.getName(), null));
  }

  @Test
  void downloadTest() {
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    this.downloadTest(project);
  }

  @Test
  void closedProjectTest() {
    final Project project = this.projectITService.getClosedPermanentProject();
    assertFalse(project.isOpen());
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), project.getResId(),
                    DataAccessibilityType.IIIF, null, ResearchDataFileFileStatus.COMPLETED));
  }

  @Test
  void deleteProjectTest() {
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a researchDataFileCreated
    final ResearchDataFile researchDataFileCreated = this.uploadAndWaitResearchDataFileProcessing(
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), project.getResId(), DataAccessibilityType.IIIF, null,
            ResearchDataFileFileStatus.COMPLETED);

    // Wait one second before testing creation
    SolidifyTime.waitOneSecond();

    final Project projectWithData = this.projectITService.openProject(project);
    this.checkProject(project.getResId(), true, ProjectStatus.OPEN, projectWithData);

    // Close project
    final Project closedProject = this.projectITService.close(project.getResId(), LocalDate.now().minusDays(1).toString());
    this.checkProject(project.getResId(), true, ProjectStatus.CLOSED, closedProject);

    // Try to delete the research data file
    final String resId = researchDataFileCreated.getResId();
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.researchDataFileClientService.delete(resId));

    // Re-open project
    final Project openProject = this.projectITService.openProject(project);
    this.checkProject(project.getResId(), true, ProjectStatus.OPEN, openProject);

    // Delete the research data file
    this.researchDataFileClientService.delete(resId);
    // Wait 1s for cache
    SolidifyTime.waitOneSecond();

    final Project emptyProject = this.projectITService.findOne(project.getResId());
    this.checkProject(project.getResId(), false, ProjectStatus.OPEN, emptyProject);

    // Delete project
    this.projectITService.delete(project.getResId());
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.projectITService.findOne(project.getResId()));
  }

  @Test
  void deleteTest() {
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a researchDataFileCreated
    final ResearchDataFile researchDataFileCreated = this.uploadAndWaitResearchDataFileProcessing(
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), project.getResId(), DataAccessibilityType.IIIF, null,
            ResearchDataFileFileStatus.COMPLETED);

    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFileCreated.getResId());

    assertEquals(researchDataFileCreated.getProjectId(), researchDataFileFound.getProjectId());
    this.researchDataFileClientService.delete(researchDataFileFound.getResId());

    assertThrows(HttpClientErrorException.NotFound.class, () -> this.researchDataFileClientService.findOne(researchDataFileFound.getResId()));
  }

  @Test
  void deleteFromFusekiTest() {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a researchDataFileCreated
    final ResearchDataFile researchDataFileCreated = this.uploadAndWaitResearchDataFileProcessing(
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), project.getResId(), DataAccessibilityType.IIIF, null,
            ResearchDataFileFileStatus.COMPLETED);

    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFileCreated.getResId());

    assertEquals(researchDataFileCreated.getProjectId(), researchDataFileFound.getProjectId());

    this.researchDataFileITService.waitResearchDataFileIsProcessed(researchDataFileFound.getResId(),
            ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);

    String query = "select ?s ?p ?s { <" + researchDataFileFound.getUri().toString() + "> ?p ?o . }";

    // Check that there are triples from this researchDataFile in fuseki
    List<SparqlResultRow> resultQuery = this.triplestoreQueriesClientService.executeQueryToDataset(project.getShortName(), query);
    assertFalse(resultQuery.isEmpty());

    this.researchDataFileClientService.delete(researchDataFileFound.getResId());

    assertThrows(HttpClientErrorException.NotFound.class, () -> this.researchDataFileClientService.findOne(researchDataFileFound.getResId()));

    // Check that there are no triple from this researchDataFile in fuseki
    resultQuery = this.triplestoreQueriesClientService.executeQueryToDataset(project.getShortName(), query);
    assertTrue(resultQuery.isEmpty());
  }

  @Test
  void listFolderTest() throws JsonProcessingException {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    this.listFolderTest(project);
  }

  @Test
  void deleteFolderTest() throws JsonProcessingException {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    this.deleteFolderTest(project);
  }

  @ParameterizedTest
  @MethodSource("zipFiles")
  void deleteListTest(String zipFile, int fileNumber) {
    Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    this.deleteTest(project);
  }

  @Test
  void cannotDeleteAResearchDataFileReferenceTest() {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Check before if there is already a research data file linked to this project
    Optional<ResearchDataFile> researchDataFileOptional = this.researchDataFileClientService.findAllWithProjectId(project.getResId()).stream()
            .filter(rdf -> rdf.getFileName().equals(RESEARCH_DATAFILE_TO_UPLOAD)).findFirst();

    ResearchDataFile researchDataFileCreated = researchDataFileOptional.orElseGet(
            () -> this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(RESEARCH_DATAFILE_TO_UPLOAD), project.getResId(),
                    DataAccessibilityType.IIIF, null, ResearchDataFileFileStatus.COMPLETED));

    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFileCreated.getResId());

    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());

    // Add a RML to project
    final Rml rml = this.addRml(project, HederaTestConstants.TEST_RML_NAME);
    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
            new ClassPathResource(SOURCE_DATASET_FILE_TO_UPLOAD), DatasetFileType.CSV.getName(), "1.0");

    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRmlApplication(sourceDataset.getResId(), sourceDatasetFile.getResId(), rml.getResId());

    // add rdfFile to triplestore
    RdfDatasetFile rdfDatasetFile = this.rdfDatasetFileClientService.findAllWithProjectId(project.getResId()).stream()
            .filter(e -> e.getSourceDatasetFile().getResId().equals(sourceDatasetFile.getResId())).findFirst().orElse(null);

    // Add to tripleStore only if not imported
    if (rdfDatasetFile != null && rdfDatasetFile.getStatusImport() == RdfDatasetFile.RdfDatasetFileStatus.NOT_IMPORTED) {
      this.rdfDatasetFileClientService.addInTripleStore(rdfDatasetFile.getResId(), project.getProjectId());
      this.rdfDatasetFileITService.waitForRdfDatasetFileIsImportedInFuseki(rdfDatasetFile.getResId());

      // Unable to delete a research data file referenced.
      assertThrows(HttpServerErrorException.InternalServerError.class,
              () -> this.researchDataFileClientService.delete(researchDataFileFound.getResId()));

      // To be able to delete later
      this.rdfDatasetFileClientService.deleteFromTripleStore(rdfDatasetFile.getResId(), project.getProjectId());
      this.rdfDatasetFileITService.waitForRdfDatasetFileIsNotImportedInFuseki(rdfDatasetFile.getResId());

      // Once the reques to delete from fuseki is made, it could take some time to really delete them from fuseki.
      SolidifyTime.waitInSeconds(2);

      this.researchDataFileClientService.delete(researchDataFileFound.getResId());

      // Unable to delete a research data file referenced.
      assertThrows(HttpClientErrorException.NotFound.class,
              () -> this.researchDataFileClientService.findOne(researchDataFileFound.getResId()));
    }
  }

  @Test
  void cannotUploadAResearchDataFileWithoutResearchObjectTypeDefinedByProjectTest() {
    final Project project = this.projectITService.createLocalTemporaryProjectWithoutResearchObjectType(
            ACRONYM.name().toLowerCase() + "-cannot-upload", "Cannot upload", null);
    this.projectITService.create(project);

    // Create a researchDataFileCreated
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.researchDataFileClientService.uploadFile(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), project.getResId(),
                    DataAccessibilityType.IIIF.getName(), null));
  }

  @Test
  void fileIsDeletedWhenErrorUploadingResearchDataFileTest() {
    final Project project = this.projectITService.createLocalTemporaryProjectWithoutResearchObjectType(
            ACRONYM.name().toLowerCase() + "-error-uploading", "Error uploading", null);
    this.projectITService.create(project);

    // Create a researchDataFileCreated
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.researchDataFileClientService.uploadFile(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), project.getResId(),
                    DataAccessibilityType.IIIF.getName(), null));

    final ResearchObjectType researchObjectType = this.researchObjectTypeITService.getResearchObjectTypeForResearchDataFile();
    project.setResearchDataFileResearchObjectType(researchObjectType);
    this.projectITService.update(project.getResId(), project);

    final ResearchDataFile researchDataFile = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(), DataAccessibilityType.IIIF, null, ResearchDataFileFileStatus.COMPLETED);

    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFile.getResId());

    // Test the upload
    this.testResult(project, researchDataFile, researchDataFileFound, REFERENCE_FILE_TO_UPLOAD);
  }

  @Test
  void updateTest() {
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    this.updateTest(project);
  }

  @ParameterizedTest
  @MethodSource("relativeLocations")
  void updateRelativeLocationTest(String targetFolder, String fileName) {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a researchDataFile
    final ResearchDataFile researchDataFileCreated = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(fileName),
            project.getResId(), DataAccessibilityType.IIIF, null, ResearchDataFileFileStatus.COMPLETED);

    ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFileCreated.getResId());

    researchDataFileFound.setRelativeLocation(targetFolder);
    ResearchDataFile researchDataFileUpdated = this.researchDataFileClientService.update(researchDataFileFound.getResId(),
            researchDataFileFound);

    this.researchDataFileITService.waitResearchDataFileIsProcessed(researchDataFileUpdated.getResId(), ResearchDataFileFileStatus.COMPLETED);

    assertEquals(researchDataFileCreated.getProjectId(), researchDataFileUpdated.getProjectId());
    assertEquals(new ClassPathResource("/" + targetFolder), new ClassPathResource(researchDataFileUpdated.getRelativeLocation()));
  }

  @ParameterizedTest
  @MethodSource("wrongRelativeLocations")
  void wrongFilenameAndRelativeLocationTest(String targetFolder, String fileName) {
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a researchDataFile
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(fileName),
            project.getResId(), DataAccessibilityType.IIIF, targetFolder, ResearchDataFileFileStatus.COMPLETED));

  }

  private void testResult(Project project, ResearchDataFile researchDataFile1, ResearchDataFile researchDataFile2, String fileName) {
    assertEquals(researchDataFile1.getResId(), researchDataFile2.getResId());
    assertEquals(project.getResId(), researchDataFile2.getProjectId());
    assertEquals(project.getResearchDataFileResearchObjectType().getResId(), researchDataFile2.getResearchObjectTypeId());
    assertEquals(DataAccessibilityType.IIIF.getName(), researchDataFile2.getAccessibleFrom().getName());
    assertTrue(fileName.endsWith(researchDataFile2.getFileName()));
    assertNull(researchDataFile1.getMetadata());
    assertNull(researchDataFile1.getRdfMetadata());
    assertNotNull(researchDataFile2.getMetadata());
    assertNotNull(researchDataFile2.getRdfMetadata());
  }

  private static Stream<Arguments> relativeLocations() {
    return Stream.of(arguments("folder", "bootstrap.yml"), arguments("folderA/folderB", "bootstrap.yml"),
            arguments("other_folderwithplus", "bootstrap.yml"), arguments("yet_other-folder", "gen-eve.jpg"),
            arguments("fold.er", "geneve.jpg"), arguments("fo(ld)er", "geneve.jpg"));
  }

  private static Stream<Arguments> wrongRelativeLocations() {
    return Stream.of(arguments("other folderéwithplus", "genève.jpg"), arguments("folder_plus", "geneve université.jpg"),
            arguments("fo[ld]er", "geneve.jpg"));
  }

  @Override
  protected void deleteFixtures() {
    super.deleteFixtures();
    this.rmlITService.cleanTestData();
  }

}
