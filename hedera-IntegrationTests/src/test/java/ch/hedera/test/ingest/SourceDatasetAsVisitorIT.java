/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - SourceDatasetAsVisitorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;

@Order(HederaTestConstants.INGEST_TEST_ORDER
        + HederaTestConstants.INGEST_SOURCE_DATASET_TEST_ORDER
        + HederaTestConstants.VISITOR_TEST_ORDER)
@Tag(HederaTestConstants.AS_VISITOR_TAG)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SourceDatasetAsVisitorIT extends AbstractDatasetWithRoleIT {
  @Autowired
  public SourceDatasetAsVisitorIT(Environment env, SudoRestClientTool restClientTool,
          ProjectITService projectITService, SourceDatasetITService sourceDatasetITService,
          SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
  }

  @Override
  protected String role() {
    return Role.VISITOR_ID;
  }

  @Test
  void creationTest() {
    final String name = "SourceDataset for creation test";

    // Create a deposit
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.sourceDatasetClientService.create(sourceDataset));
  }

  @Test
  void canListOnlyAuthorizedProjectIdTest() {
    final Project project = this.projectITService.getOrCreatePermanentProject(
            HederaTestConstants.ProjectStatus.OPEN, Role.VISITOR_ID, null);
    assertDoesNotThrow(() -> this.sourceDatasetClientService.findAllWithProjectId(project.getProjectId()));
  }

  @Test
  void cannotListInNonAuthorizedProjectIdTest() {
    this.restClientTool.sudoAdmin();
    final List<Project> projects = this.projectITService.searchByProperties(Map.of("name", NO_ONE));
    this.restClientTool.exitSudo();
    if (!projects.isEmpty()) {
      assertThrows(HttpServerErrorException.InternalServerError.class,
              () -> this.sourceDatasetClientService.findAllWithProjectId(projects.get(0).getProjectId()));
    }
  }

  @Test
  void cannotListAllWithoutProjectIdTest() {
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.sourceDatasetClientService.findAll());
  }

  @Test
  void deleteTest() {
    this.restClientTool.sudoAdmin();
    final String name = "SourceDataset for creation test";

    // Create a deposit
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);

    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.sourceDatasetClientService.delete(sourceDatasetCreated.getResId()));
  }

  @Test
  void updateTest() {
    this.restClientTool.sudoAdmin();
    final String name = "SourceDataset for creation test";

    // Create a deposit
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);

    this.restClientTool.exitSudo();

    sourceDatasetCreated.setName("updated by visitor");
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.sourceDatasetClientService.update(sourceDatasetCreated.getResId(), sourceDatasetCreated));
  }

  @Test
  void getAnySourceDatasetFromItsOwnOrganizationalUnitTest() {
    this.restClientTool.sudoAdmin();
    final String name = "SourceDataset for creation test";

    // Create a deposit
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);
    SolidifyTime.waitInSeconds(this.waitToAvoidSpurious403);

    this.restClientTool.exitSudo();

    assertDoesNotThrow(() -> this.sourceDatasetClientService.findOne(sourceDatasetCreated.getResId()));
  }

  @Test
  void cannotAnySourceDatasetTest() {
    this.restClientTool.sudoAdmin();
    final Project project = this.projectITService.getOrCreatePermanentProjectForNoOne();

    // Create a sourceDataset
    final SourceDataset localSourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(), this.role());
    final SourceDataset remoteSourceDataset = this.sourceDatasetClientService.create(localSourceDataset);

    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.sourceDatasetClientService.findOne(remoteSourceDataset.getResId()));
  }

}
