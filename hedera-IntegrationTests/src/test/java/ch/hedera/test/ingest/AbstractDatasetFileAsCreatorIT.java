/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AbstractDatasetFileAsCreatorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static ch.hedera.test.HederaTestConstants.REFERENCE_FILE_TO_UPLOAD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.ingest.DatasetFileType;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;

public abstract class AbstractDatasetFileAsCreatorIT extends AbstractDatasetWithRoleIT {

  public AbstractDatasetFileAsCreatorIT(Environment env,
          SudoRestClientTool restClientTool,
          ProjectITService projectITService,
          SourceDatasetITService sourceDatasetITService,
          SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
  }

  @Test
  void uploadSourceDatasetFileTest() {
    final String name = "SourceDatasetFile upload test";

    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);
    SolidifyTime.waitInSeconds(this.waitToAvoidSpurious403);

    // Test the creation
    assertEquals(sourceDataset.getName(), sourceDatasetCreated.getName());

    final SourceDataset sourceDatasetFound = this.sourceDatasetClientService.findOne(sourceDatasetCreated.getResId());

    assertEquals(sourceDataset.getName(), sourceDatasetFound.getName());

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDatasetCreated.getResId(),
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0");

    final List<SourceDatasetFile> list = this.sourceDatasetClientService.getSourceDataFiles(sourceDatasetCreated.getResId());

    // Test test upload
    assertEquals(1, list.size());
    this.testSourceDatasetFileResult(sourceDatasetFound, sourceDatasetFile, REFERENCE_FILE_TO_UPLOAD, "1.0", DatasetFileType.XML, list.get(0));
  }

  @Test
  void uploadSourceDatasetFileWithSameNameTest() {
    final String name = "SourceDatasetFile upload test with same file name";

    // Create a SourceDataset
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);
    SolidifyTime.waitInSeconds(this.waitToAvoidSpurious403);

    // Test the creation
    assertEquals(sourceDataset.getName(), sourceDatasetCreated.getName());

    final SourceDataset sourceDatasetFound = this.sourceDatasetClientService.findOne(sourceDatasetCreated.getResId());

    assertEquals(sourceDataset.getName(), sourceDatasetFound.getName());

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDatasetCreated.getResId(),
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0");

    final List<SourceDatasetFile> list = this.sourceDatasetClientService.getSourceDataFiles(sourceDatasetCreated.getResId());

    // Test the upload
    assertEquals(1, list.size());
    this.testSourceDatasetFileResult(sourceDatasetFound, sourceDatasetFile, REFERENCE_FILE_TO_UPLOAD, "1.0", DatasetFileType.XML, list.get(0));

    // Upload in the same DatasetSource a file with same name
    assertThrows(HttpServerErrorException.InternalServerError.class,
            () -> this.sourceDatasetClientService.uploadFile(sourceDatasetCreated.getResId(),
                    new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0"));
  }

  @Test
  void downloadTest() {
    final String name = "SourceDatasetFile upload test with same file name";

    // Create a SourceDataSet
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);
    SolidifyTime.waitInSeconds(this.waitToAvoidSpurious403);

    // Test the creation
    assertEquals(sourceDataset.getName(), sourceDatasetCreated.getName());

    final SourceDataset sourceDatasetFound = this.sourceDatasetClientService.findOne(sourceDatasetCreated.getResId());

    assertEquals(sourceDataset.getName(), sourceDatasetFound.getName());

    final ClassPathResource fileToUpload = new ClassPathResource(REFERENCE_FILE_TO_UPLOAD);
    final Long expectedFileSize = this.checkFile(fileToUpload);

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDatasetCreated.getResId(),
            fileToUpload, DatasetFileType.XML.getName(), "1.0");

    // Download the file
    Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.tmp");
    this.sourceDatasetClientService.downloadSourceDatasetFile(sourceDatasetCreated.getResId(),
            sourceDatasetFile.getResId(), path);

    assertEquals(expectedFileSize, path.toFile().length());
  }

  @Test
  void deleteTest() {
    final String fileToUpload = "bootstrap.yml";

    final String name = "SourceDatasetFile delete SourceDatasetFile";

    // Create a SourceDataset
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);
    SolidifyTime.waitInSeconds(this.waitToAvoidSpurious403);

    // Test the creation
    assertEquals(sourceDataset.getName(), sourceDatasetCreated.getName());

    final SourceDataset sourceDatasetFound = this.sourceDatasetClientService.findOne(sourceDatasetCreated.getResId());

    assertEquals(sourceDataset.getName(), sourceDatasetFound.getName());

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDatasetCreated.getResId(),
            new ClassPathResource(fileToUpload), DatasetFileType.XML.getName(), "1.0");

    final List<SourceDatasetFile> list = this.sourceDatasetClientService.getSourceDataFiles(sourceDatasetCreated.getResId());

    // Test the upload
    assertEquals(1, list.size());
    this.testSourceDatasetFileResult(sourceDatasetFound, sourceDatasetFile, REFERENCE_FILE_TO_UPLOAD, "1.0", DatasetFileType.XML, list.get(0));

    this.sourceDatasetClientService.removeSourceDatasetFile(sourceDataset.getResId(), sourceDatasetFile.getResId());

    final List<SourceDatasetFile> list2 = this.sourceDatasetClientService.getSourceDataFiles(sourceDatasetCreated.getResId());
    assertTrue(list2.isEmpty());
  }

  @Test
  void updateTest() {
    final String name = "SourceDatasetFile update SourceDatasetFile version";

    // Create a SourceDataset
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);
    SolidifyTime.waitInSeconds(this.waitToAvoidSpurious403);

    // Test the creation
    assertEquals(sourceDataset.getName(), sourceDatasetCreated.getName());

    final SourceDataset sourceDatasetFound = this.sourceDatasetClientService.findOne(sourceDatasetCreated.getResId());

    assertEquals(sourceDataset.getName(), sourceDatasetFound.getName());

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDatasetCreated.getResId(),
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0");

    final List<SourceDatasetFile> list = this.sourceDatasetClientService.getSourceDataFiles(sourceDatasetCreated.getResId());

    // Test the upload
    assertEquals(1, list.size());
    this.testSourceDatasetFileResult(sourceDatasetFound, sourceDatasetFile, REFERENCE_FILE_TO_UPLOAD, "1.0", DatasetFileType.XML, list.get(0));

    // Update version of that SourceDatasetFile
    final SourceDatasetFile sourceDatasetFile2 = this.sourceDatasetClientService.updateSourceDatasetFile(sourceDatasetCreated.getResId(),
            sourceDatasetFile.getResId(),
            Map.of("version", "2.0"));

    final List<SourceDatasetFile> list2 = this.sourceDatasetClientService.getSourceDataFiles(sourceDatasetCreated.getResId());

    // Test the upload
    assertEquals(1, list.size());
    this.testSourceDatasetFileResult(sourceDatasetFound, sourceDatasetFile2, REFERENCE_FILE_TO_UPLOAD, "2.0", DatasetFileType.XML, list2.get(0));
  }

  @Test
  void cannotListAllWithoutProjectIdTest() {
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.sourceDatasetClientService.findAll());
  }

  @Test
  void cannotListInNonAuthorizedProjectTest() {
    this.restClientTool.sudoAdmin();
    final List<Project> projectList = this.projectITService.searchByProperties(Map.of("name", AuthApplicationRole.ROOT_ID));
    this.restClientTool.exitSudo();

    if (!projectList.isEmpty()) {
      assertThrows(HttpServerErrorException.InternalServerError.class,
              () -> this.sourceDatasetClientService.findAllWithProjectId(projectList.get(0).getResId()));
    }
  }

}
