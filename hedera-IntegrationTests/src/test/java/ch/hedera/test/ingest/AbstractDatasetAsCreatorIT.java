/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AbstractDatasetAsCreatorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;

abstract public class AbstractDatasetAsCreatorIT extends AbstractDatasetWithRoleIT {

  public AbstractDatasetAsCreatorIT(Environment env, SudoRestClientTool restClientTool,
          ProjectITService projectITService, SourceDatasetITService sourceDatasetITService,
          SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
  }

  @Test
  void findAllTest() {
    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, Role.MANAGER_ID,
            AuthApplicationRole.USER);

    // Create SourceDataset
    final String name1 = "SourceDataset findAll test";
    final SourceDataset sourceDataset1 = this.sourceDatasetITService.createLocalSourceDatasetOnProject(
            HederaTestConstants.PersistenceMode.TEMPORARY,
            name1, DEFAULT_DESCRIPTION, project);
    this.sourceDatasetITService.createLocalSourceDatasetOnProject(HederaTestConstants.PersistenceMode.TEMPORARY,
            name1, DEFAULT_DESCRIPTION, project);
    final SourceDataset createdSourceDataset1 = this.sourceDatasetClientService.create(sourceDataset1);
    final String tmpName1 = createdSourceDataset1.getName();
    SolidifyTime.waitInSeconds(this.waitToAvoidSpurious403);

    final String name2 = "SourceDataset2 findAll test";
    final SourceDataset sourceDataset2 = this.sourceDatasetITService.createLocalSourceDatasetOnProject(
            HederaTestConstants.PersistenceMode.TEMPORARY,
            name2, DEFAULT_DESCRIPTION, project);
    final SourceDataset createdSourceDataset2 = this.sourceDatasetClientService.create(sourceDataset2);
    final String tmpName2 = createdSourceDataset2.getName();
    SolidifyTime.waitInSeconds(this.waitToAvoidSpurious403);

    List<SourceDataset> sourceDatasetList = this.sourceDatasetClientService.searchByProperties(
            Map.of(HederaConstants.PROJECT_ID_FIELD, project.getResId()));
    assertTrue(sourceDatasetList.stream().anyMatch(so -> so.getName().equals(tmpName1)));
    assertTrue(sourceDatasetList.stream().anyMatch(so -> so.getName().equals(tmpName2)));

    // Filter by name
    sourceDatasetList = this.sourceDatasetClientService.searchByProperties(
            Map.of("name", name1, HederaConstants.PROJECT_ID_FIELD, project.getResId()));
    assertTrue(sourceDatasetList.stream().anyMatch(so -> so.getName().equals(tmpName1)));
    assertFalse(sourceDatasetList.stream().anyMatch(so -> so.getName().equals(tmpName2)));

  }

  @Test
  void creationTest() {
    final String name = "SourceDataset for creation test";

    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, Role.MANAGER_ID,
            AuthApplicationRole.USER);

    // Create a source dataset
    final SourceDataset sourceDataset = this.sourceDatasetITService.createLocalSourceDatasetOnProject(
            HederaTestConstants.PersistenceMode.TEMPORARY,
            name, DEFAULT_DESCRIPTION, project);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);
    SolidifyTime.waitInSeconds(this.waitToAvoidSpurious403);

    // Test the creation
    assertEquals(sourceDataset.getName(), sourceDatasetCreated.getName());

    final SourceDataset sourceDatasetFound = this.sourceDatasetClientService.findOne(sourceDatasetCreated.getResId());

    assertEquals(sourceDataset.getName(), sourceDatasetFound.getName());

    // Check that the SourceDataset has been created
    final List<SourceDataset> deposits = this.sourceDatasetClientService.searchByProperties(Map.of("name", sourceDataset.getName(),
            HederaConstants.PROJECT_ID_FIELD, project.getResId()));
    assertEquals(1, deposits.size());
  }

  @Test
  void deleteTest() {
    final String name = "SourceDataset for deleted test";

    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    this.sourceDatasetClientService.create(sourceDataset);
    SolidifyTime.waitInSeconds(this.waitToAvoidSpurious403);

    // Delete the SourceDataset
    final String resId = sourceDataset.getResId();
    this.sourceDatasetClientService.delete(resId);

    // Wait one second to let asynchronous deletion complete before to check
    SolidifyTime.waitOneSecond();

    assertThrows(HttpClientErrorException.NotFound.class, () -> this.sourceDatasetClientService.findOne(resId));
  }

  @Test
  void updateTest() {
    final String name = "SourceDataset for updated test";

    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);
    SolidifyTime.waitInSeconds(this.waitToAvoidSpurious403);

    // Update the deposit
    sourceDatasetCreated.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("B"));
    final SourceDataset actualSourceDataset = this.sourceDatasetClientService.update(sourceDatasetCreated.getResId(), sourceDatasetCreated);
    SourceDataset updatedSourceDataset = this.sourceDatasetClientService.findOne(actualSourceDataset.getResId());

    // Test the update
    assertEquals(sourceDatasetCreated.getName(), updatedSourceDataset.getName());
    this.assertEqualsWithoutNanoSeconds(sourceDatasetCreated.getCreationTime(), updatedSourceDataset.getCreationTime());
    assertNotNull(updatedSourceDataset);
  }

}
