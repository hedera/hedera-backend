/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - SourceDatasetFileAsVisitorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static ch.hedera.test.HederaTestConstants.REFERENCE_FILE_TO_UPLOAD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.ingest.DatasetFileType;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.security.Role;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;

@Order(HederaTestConstants.INGEST_TEST_ORDER
        + HederaTestConstants.INGEST_SOURCE_DATASET_FILE_TEST_ORDER
        + HederaTestConstants.VISITOR_TEST_ORDER)
@Tag(HederaTestConstants.AS_VISITOR_TAG)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SourceDatasetFileAsVisitorIT extends AbstractDatasetWithRoleIT {
  @Autowired
  public SourceDatasetFileAsVisitorIT(Environment env, SudoRestClientTool restClientTool,
          ProjectITService projectITService, SourceDatasetITService sourceDatasetITService,
          SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
  }

  @Override
  protected String role() {
    return Role.VISITOR_ID;
  }

  @Test
  void uploadSourceDataFileTest() {
    this.restClientTool.sudoAdmin();

    final String name = "SourceDatasetFile upload test";

    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);

    // Test the creation
    assertEquals(sourceDataset.getName(), sourceDatasetCreated.getName());

    final SourceDataset sourceDatasetFound = this.sourceDatasetClientService.findOne(sourceDatasetCreated.getResId());

    assertEquals(sourceDataset.getName(), sourceDatasetFound.getName());
    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.sourceDatasetClientService.uploadFile(sourceDatasetCreated.getResId(),
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0"));
  }

  @Test
  void downloadFileTest() throws IOException {
    this.restClientTool.sudoAdmin();

    final String name = "SourceDatasetFile upload test with same file name";

    // Create a SourceDataSet
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);

    // Test the creation
    assertEquals(sourceDataset.getName(), sourceDatasetCreated.getName());

    final SourceDataset sourceDatasetFound = this.sourceDatasetClientService.findOne(sourceDatasetCreated.getResId());

    assertEquals(sourceDataset.getName(), sourceDatasetFound.getName());

    final ClassPathResource fileToUpload = new ClassPathResource(REFERENCE_FILE_TO_UPLOAD);
    final Long expectedFileSize = this.checkFile(fileToUpload);

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDatasetCreated.getResId(),
            fileToUpload, DatasetFileType.XML.getName(), "1.0");

    this.restClientTool.exitSudo();

    // Download the file
    Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.tmp");
    this.sourceDatasetClientService.downloadSourceDatasetFile(sourceDatasetCreated.getResId(),
            sourceDatasetFile.getResId(), path);

    assertEquals(expectedFileSize, path.toFile().length());
  }

  @Test
  void updateTest() {
    this.restClientTool.sudoAdmin();

    final String name = "SourceDatasetFile update SourceDatasetFile version";

    // Create a SourceDataset
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);

    // Test the creation
    assertEquals(sourceDataset.getName(), sourceDatasetCreated.getName());

    final SourceDataset sourceDatasetFound = this.sourceDatasetClientService.findOne(sourceDatasetCreated.getResId());

    assertEquals(sourceDataset.getName(), sourceDatasetFound.getName());

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.uploadFile(sourceDatasetCreated.getResId(),
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0");

    final List<SourceDatasetFile> list = this.sourceDatasetClientService.getSourceDataFiles(sourceDatasetCreated.getResId());

    // Test the upload
    assertEquals(1, list.size());
    assertEquals(list.get(0).getResId(), sourceDatasetFile.getResId());
    assertEquals(list.get(0).getVersion(), "1.0");

    this.restClientTool.exitSudo();

    // Update version of that SourceDatasetFile
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.sourceDatasetClientService.updateSourceDatasetFile(sourceDatasetCreated.getResId(), sourceDatasetFile.getResId(),
                    Map.of("version", "2.0")));
  }

  @Test
  void deleteTest() {
    this.restClientTool.sudoAdmin();
    final String name = "SourceDatasetFile upload test";

    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);

    // Test the creation
    assertEquals(sourceDataset.getName(), sourceDatasetCreated.getName());

    final SourceDataset sourceDatasetFound = this.sourceDatasetClientService.findOne(sourceDatasetCreated.getResId());

    assertEquals(sourceDataset.getName(), sourceDatasetFound.getName());
    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.sourceDatasetClientService.uploadFile(sourceDatasetCreated.getResId(),
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD), DatasetFileType.XML.getName(), "1.0"));
  }

  @Test
  void getAnySourceDataFileTest() {
    this.restClientTool.sudoAdmin();
    final String name = "SourceDatasetFile upload test with same file name";

    // Create a SourceDataSet
    final SourceDataset sourceDataset = this.createLocalSourceDatasetForCurrentRole(name, DEFAULT_DESCRIPTION);
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);

    // Test the creation
    assertEquals(sourceDataset.getName(), sourceDatasetCreated.getName());

    final SourceDataset sourceDatasetFound = this.sourceDatasetClientService.findOne(sourceDatasetCreated.getResId());

    assertEquals(sourceDataset.getName(), sourceDatasetFound.getName());

    this.sourceDatasetClientService.uploadFile(sourceDatasetCreated.getResId(), new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            DatasetFileType.XML.getName(), "1.0");

    this.restClientTool.exitSudo();

    List<SourceDatasetFile> sourceDataFileList = this.sourceDatasetClientService.getSourceDataFiles(sourceDatasetCreated.getResId());

    assertFalse(sourceDataFileList.isEmpty());
    assertEquals(1, sourceDataFileList.size());
  }
}
