/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AbstractIngestIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.jena.graph.Node;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.junit.jupiter.params.provider.Arguments;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.humanities.Rml;
import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.DatasetFileType;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.ResearchDataFile.ResearchDataFileFileStatus;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.AbstractIT;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.HederaTestConstants.ProjectStatus;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;
import ch.hedera.util.RdfTool;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlQueryTool;

public class AbstractIngestIT extends AbstractIT {

  @Value("${hedera.test.waitToAvoidSpurious403:0}")
  protected int waitToAvoidSpurious403;

  protected final static long RDF_FOR_FIRST_DATASET_FILE_SIZE = 6920;
  protected final static long RDF_FOR_SECOND_DATASET_FILE_SIZE = 6885;

  protected ProjectITService projectITService;
  protected SourceDatasetITService sourceDatasetITService;
  protected SourceDatasetClientService sourceDatasetClientService;
  protected ResearchDataFileITService researchDataFileITService;
  protected ResearchDataFileClientService researchDataFileClientService;
  protected RdfDatasetFileITService rdfDatasetFileITService;
  protected ResearchObjectTypeITService researchObjectTypeITService;
  protected RmlClientService rmlClientService;

  public AbstractIngestIT(Environment env, SudoRestClientTool restClientTool, ProjectITService projectITService,
          SourceDatasetITService sourceDatasetITService, SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService, ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService, ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService) {
    super(env, restClientTool);
    this.projectITService = projectITService;
    this.sourceDatasetITService = sourceDatasetITService;
    this.sourceDatasetClientService = sourceDatasetClientService;
    this.researchDataFileClientService = researchDataFileClientService;
    this.researchDataFileITService = researchDataFileITService;
    this.rdfDatasetFileITService = rdfDatasetFileITService;
    this.researchObjectTypeITService = researchObjectTypeITService;
    this.rmlClientService = rmlClientService;
  }

  protected void testSourceDatasetFileResult(SourceDataset sourceDataset, SourceDatasetFile sourceDatasetFile, String filename, String version,
          DatasetFileType fileType, SourceDatasetFile sourceDatasetFileToTest) {
    assertEquals(sourceDataset.getResId(), sourceDatasetFileToTest.getSourceDataset().getResId());
    assertEquals(sourceDatasetFile.getResId(), sourceDatasetFileToTest.getResId());
    assertEquals(version, sourceDatasetFileToTest.getVersion());
    assertEquals(filename, sourceDatasetFileToTest.getFileName());
    assertEquals(fileType, sourceDatasetFileToTest.getType());
  }

  protected void executeAndTestRmlApplication(String sourceDatasetId, String sourceDatasetFileId, String rmlId) {
    // Apply rml to generate RdfDatasetFile
    final Result result = this.sourceDatasetClientService.applyRml(sourceDatasetId, sourceDatasetFileId, rmlId);
    assertEquals(ActionStatus.EXECUTED, result.getStatus());
    // Wait until SourceDatasetFile is transformed
    this.sourceDatasetITService.waitForSourceDatasetFileIsTransformed(sourceDatasetId, sourceDatasetFileId);
  }

  protected void executeRmlApplicationWithError(String sourceDatasetId, String sourceDatasetFileId, String rmlId) {
    // Apply rml to generate RdfDatasetFile
    final Result result = this.sourceDatasetClientService.applyRml(sourceDatasetId, sourceDatasetFileId, rmlId);
    assertEquals(ActionStatus.EXECUTED, result.getStatus());
    // Wait until SourceDatasetFile is transformed
    this.sourceDatasetITService.waitForSourceDatasetFileIsInError(sourceDatasetId, sourceDatasetFileId);
  }

  protected void executeAndTestRdfTransformation(String sourceDatasetId, String sourceDatasetFileId) {
    // trasnform RDf to generate RdfDatasetFile
    final Result result = this.sourceDatasetClientService.transformRdf(sourceDatasetId, sourceDatasetFileId);
    assertEquals(ActionStatus.EXECUTED, result.getStatus());
    // Wait until SourceDatasetFile is transformed
    this.sourceDatasetITService.waitForSourceDatasetFileIsTransformed(sourceDatasetId, sourceDatasetFileId);
  }

  protected void executeAndTestFileCheck(String sourceDatasetId, String sourceDatasetFileId) {
    // trasnform RDf to generate RdfDatasetFile
    final Result result = this.sourceDatasetClientService.check(sourceDatasetId, sourceDatasetFileId);
    assertEquals(ActionStatus.EXECUTED, result.getStatus());
    // Wait until SourceDatasetFile is checked
    this.sourceDatasetITService.waitForSourceDatasetFileIsChecked(sourceDatasetId, sourceDatasetFileId);
  }

  protected void checkProject(String projectId, boolean hasData, ProjectStatus projectStatus, Project newProject) {
    assertNotNull(newProject, "Project object NULL");
    assertEquals(projectId, newProject.getResId(), "Wrong project ID");
    assertEquals(hasData, newProject.hasData(), "Different flag 'hasData'");
    if (projectStatus.equals(ProjectStatus.OPEN)) {
      assertTrue(newProject.isOpen(), "Project must be open");
    } else {
      assertFalse(newProject.isOpen(), "Project must be closed");
    }
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.projectITService.reopenTemporaryProjects();
    // Wait 1s for cache
    SolidifyTime.waitOneSecond();
    this.rdfDatasetFileITService.cleanTestData();
    this.sourceDatasetITService.cleanTestData();
    this.researchDataFileITService.cleanTestData();
    // Wait 1s for cache
    SolidifyTime.waitOneSecond();
    this.projectITService.cleanTestData();
    this.researchObjectTypeITService.cleanTestData();
    this.restClientTool.exitSudo();
  }

  protected Rml addRml(Project project, String rmlName) {
    List<Rml> rmls = this.rmlClientService.searchByProperties(
            Map.of("name", HederaTestConstants.getNameWithPermanentLabel(rmlName)));
    assertFalse(rmls.isEmpty(), "Cannot find RML: " + rmlName);
    final Rml demoRml = rmls.get(0);

    // Add the Rml to the project
    List<Rml> rmlList = this.projectITService.getRmls(project.getResId());
    Optional<Rml> foundRml = rmlList.stream().filter(rml -> rml.getResId().equals(demoRml.getResId())).findFirst();
    if (foundRml.isEmpty()) {
      this.projectITService.addRml(project.getResId(), demoRml.getResId());
    }
    return demoRml;
  }

  protected SourceDataset createSourceDataset(Project project, ApplicationRole role) {
    final SourceDataset sourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(),
            role);
    return this.sourceDatasetClientService.create(sourceDataset);
  }

  protected SourceDatasetFile createSourceDatasetFile(SourceDataset sourceDataset, String fileName, DatasetFileType datasetFileType) {
    return this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
            new ClassPathResource(fileName), datasetFileType.getName(), "1.0");
  }

  protected SourceDatasetFile uploadFirstSourceDataset(SourceDataset sourceDataset) {
    return this.sourceDatasetClientService.uploadFile(sourceDataset.getResId(),
            new ClassPathResource(HederaTestConstants.FIRST_SOURCE_DATASET_FILE_FILENAME), DatasetFileType.XML.getName(), "1.0");
  }

  protected ResearchDataFile uploadAndWaitResearchDataFileProcessing(
          Resource fileResource,
          String projectId,
          DataAccessibilityType dataAccessibilityType,
          String relativeLocation,
          ResearchDataFile.ResearchDataFileFileStatus expectedStatus) {
    final ResearchDataFile researchDataFile = this.researchDataFileClientService.uploadFile(fileResource, projectId,
            dataAccessibilityType.getName(), relativeLocation);
    assertTrue(this.researchDataFileITService.waitResearchDataFileIsProcessed(researchDataFile.getResId(), expectedStatus));
    return researchDataFile;
  }

  protected SourceDatasetFile getOrCreateSourceDatasetFile(SourceDataset sourceDataset, String fileName, DatasetFileType datasetFileType) {
    List<SourceDatasetFile> sourceDatasetFileList = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());
    Optional<SourceDatasetFile> sourceDatasetFileOptional = sourceDatasetFileList.stream().filter(sdf -> sdf.getFileName().equals(fileName))
            .findFirst();
    return sourceDatasetFileOptional.orElseGet(() -> this.createSourceDatasetFile(sourceDataset, fileName, datasetFileType));
  }

  protected void checkFirstRdfDatasetFile(Path path) {
    Model rdfModel = RdfTool.getRdfModel(path);

    List<String> types = new ArrayList<>();
    final Query typesListQuery = SparqlQueryTool.queryToListRdfTypes("?t");
    try (QueryExecution localExecution = QueryExecutionFactory.create(typesListQuery, rdfModel)) {
      ResultSet resultSet = localExecution.execSelect();
      while (resultSet.hasNext()) {
        QuerySolution solution = resultSet.nextSolution();
        types.add(solution.getResource("?t").toString());
      }
      assertFalse(types.isEmpty());
      assertTrue(types.contains("http://www.cidoc-crm.org/cidoc-crm/E21_Person"));
    }

    List<Map<String, String>> allTriples = new ArrayList<>();
    final Query allTriplesQuery = SparqlQueryTool.queryToListAllTriples(
            SparqlBuilderTool.SUBJECT_FULL_NAME_VARIABLE,
            SparqlBuilderTool.PREDICATE_FULL_NAME_VARIABLE,
            SparqlBuilderTool.OBJECT_FULL_NAME_VARIABLE);
    try (QueryExecution localExecution = QueryExecutionFactory.create(allTriplesQuery, rdfModel)) {
      ResultSet resultSet = localExecution.execSelect();
      while (resultSet.hasNext()) {
        QuerySolution solution = resultSet.nextSolution();
        Map<String, String> tripleMap = new HashMap<>();
        Node subjectNode = solution.get(SparqlBuilderTool.SUBJECT_FULL_NAME_VARIABLE).asNode();
        Node predicateNode = solution.get(SparqlBuilderTool.PREDICATE_FULL_NAME_VARIABLE).asNode();
        Node objectNode = solution.get(SparqlBuilderTool.OBJECT_FULL_NAME_VARIABLE).asNode();

        tripleMap.put("subject", this.getNodeValue(subjectNode));
        tripleMap.put("predicate", this.getNodeValue(predicateNode));
        tripleMap.put("object", this.getNodeValue(objectNode));
        allTriples.add(tripleMap);
      }
      assertFalse(allTriples.isEmpty());
      assertTrue(allTriples.stream().anyMatch(tripleMap -> tripleMap.get("object").equals("Moreno_001_1.jpg")));
      assertTrue(allTriples.stream().anyMatch(tripleMap -> tripleMap.get("object").equals("Testamento de D. Guindo y Pascual Cerezo")));
      assertTrue(allTriples.stream().anyMatch(tripleMap -> tripleMap.get("object").equals("José María Moreno")));
    }
  }

  protected ResearchDataFile getOrUploadResearchDataFile(Project project) {
    List<ResearchDataFile> researchDataFilelist = this.researchDataFileClientService
            .searchByProperties(Map.of("fileName", HederaTestConstants.FIRST_SOURCE_DATASET_FILE_FILENAME, HederaConstants.PROJECT_ID_FIELD,
                    project.getResId()));
    if (researchDataFilelist.isEmpty()) {
      // Create Research Data File
      return this.uploadAndWaitResearchDataFileProcessing(
              new ClassPathResource(HederaTestConstants.FIRST_SOURCE_DATASET_FILE_FILENAME),
              project.getResId(),
              DataAccessibilityType.TEI,
              "/",
              ResearchDataFileFileStatus.COMPLETED);
    }
    return researchDataFilelist.get(0);
  }

  protected SourceDatasetFile createSourceDatasetFileWithResearchDataFile(SourceDataset sourceDataset, ResearchDataFile researchDataFile,
          String version, DatasetFileType dataType) {
    SourceDatasetFile localSourceDatasetFile = new SourceDatasetFile();
    localSourceDatasetFile.setDataset(sourceDataset);
    localSourceDatasetFile.setVersion(version);
    localSourceDatasetFile.setType(dataType);
    localSourceDatasetFile.setResearchDataFile(researchDataFile);

    final SourceDatasetFile sourceDatasetFile = this.sourceDatasetClientService.createSourceDataFile(sourceDataset.getResId(),
            localSourceDatasetFile);
    assertNotNull(sourceDatasetFile);
    return sourceDatasetFile;
  }

  private String getNodeValue(Node node) {
    if (node.isURI() || node.isBlank()) {
      return node.toString();
    } else if (node.isLiteral()) {
      return node.getLiteral().toString(false);
    }
    throw new SolidifyRuntimeException("Node type not supported");
  }

  protected static Stream<Arguments> zipFiles() {
    return Stream.of(
            arguments("researchDataFiles-wo-folder.zip", 4),
            arguments("researchDataFiles-w-folders.zip", 7),
            arguments("researchDataFiles-w-folders2.zip", 7));
  }

}
