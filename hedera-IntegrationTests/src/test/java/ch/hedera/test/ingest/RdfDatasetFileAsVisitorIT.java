/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - RdfDatasetFileAsVisitorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static ch.hedera.test.HederaTestConstants.MISSING_TMP_RDF_DATASET_FILE_MSG;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.RdfFormat;
import ch.hedera.model.humanities.Rml;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.ingest.IIIFRefreshService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;

@Order(HederaTestConstants.INGEST_TEST_ORDER
        + HederaTestConstants.INGEST_RDF_DATASET_TEST_ORDER
        + HederaTestConstants.VISITOR_TEST_ORDER)
@Tag(HederaTestConstants.AS_VISITOR_TAG)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class RdfDatasetFileAsVisitorIT extends AbstractDatasetWithRoleIT {

  protected final IIIFRefreshService iiifRefreshService;

  @Autowired
  public RdfDatasetFileAsVisitorIT(Environment env, SudoRestClientTool restClientTool, ProjectITService projectITService,
          SourceDatasetITService sourceDatasetITService,
          SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService,
          IIIFRefreshService iiifRefreshService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
    this.rmlClientService = rmlClientService;
    this.iiifRefreshService = iiifRefreshService;
  }

  @Override
  protected String role() {
    return Role.VISITOR_ID;
  }

  @Test
  void findAllTest() {
    this.restClientTool.sudoAdmin();
    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, this.role(),
            AuthApplicationRole.USER);
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(), this.role());
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);

    // Associate Project with Permanent RML
    final Rml ludusRml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);

    // Upload Ludus data sample
    final SourceDatasetFile sourceDatasetFile = this.uploadFirstSourceDataset(sourceDatasetCreated);

    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRmlApplication(sourceDatasetCreated.getResId(), sourceDatasetFile.getResId(), ludusRml.getResId());

    // remove the rml after apply
    this.projectITService.removeRml(project.getResId(), ludusRml.getResId());
    this.restClientTool.exitSudo();

    // List RdfDatasetFiles
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    assertEquals(1, rdfFilesWithProjectId.size());
  }

  @Test
  void downloadTest() {
    this.restClientTool.sudoAdmin();
    // Create project
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(), this.role());
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);

    // Associate Project with Permanent RML
    final Rml ludusRml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);

    // Upload Ludus data sample
    final SourceDatasetFile sourceDatasetFile = this.uploadFirstSourceDataset(sourceDatasetCreated);

    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRmlApplication(sourceDatasetCreated.getResId(), sourceDatasetFile.getResId(), ludusRml.getResId());

    // remove the rml after apply
    this.projectITService.removeRml(project.getResId(), ludusRml.getResId());
    this.restClientTool.exitSudo();

    // get RdfDatasetFile
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    RdfDatasetFile rdfFile = rdfFilesWithProjectId.stream()
            .filter(e -> e.getSourceDatasetFile().getResId().equals(sourceDatasetFile.getResId()))
            .findFirst()
            .orElseThrow(() -> new IllegalStateException(MISSING_TMP_RDF_DATASET_FILE_MSG));

    if (rdfFile != null) {
      // Download the file
      Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.ttl");
      this.rdfDatasetFileClientService.downloadRdfDatasetFile(rdfFile.getResId(), path);
      this.checkFirstRdfDatasetFile(path);
    }
  }

  @Test
  void updateTest() {
    this.restClientTool.sudoAdmin();
    // Create project
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);

    // Create a sourceDataset
    final SourceDataset sourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(), this.role());
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);

    // Associate Project with Permanent RML
    final Rml ludusRml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);

    // Upload Ludus data sample
    final SourceDatasetFile sourceDatasetFile = this.uploadFirstSourceDataset(sourceDatasetCreated);

    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRmlApplication(sourceDatasetCreated.getResId(), sourceDatasetFile.getResId(), ludusRml.getResId());

    // remove the rml after apply
    this.projectITService.removeRml(project.getResId(), ludusRml.getResId());
    this.restClientTool.exitSudo();

    // get RdfDatasetFile
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    RdfDatasetFile rdfFile = rdfFilesWithProjectId.stream()
            .filter(e -> e.getSourceDatasetFile().getResId().equals(sourceDatasetFile.getResId()))
            .findFirst()
            .orElseThrow(() -> new IllegalStateException(MISSING_TMP_RDF_DATASET_FILE_MSG));

    /*
     * Does the update
     */
    rdfFile.setRdfFormat(RdfFormat.N_QUADS);
    List<String> propertiesToUpdate = new ArrayList<>();
    propertiesToUpdate.add("rdfFormat");

    // Check if RdfDatasetFile is deleted
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.rdfDatasetFileClientService.update(rdfFile.getResId(), rdfFile, propertiesToUpdate),
            "RdfDatasetFile was updated even if it was not permitted !!");
  }

  @Test
  void deleteTest() {
    this.restClientTool.sudoAdmin();
    // Create project
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);

    // Create a sourceDataset
    final SourceDataset sourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(), this.role());
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);

    // Associate Project with Permanent RML
    final Rml ludusRml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);

    // Upload Ludus data sample
    final SourceDatasetFile sourceDatasetFile = this.uploadFirstSourceDataset(sourceDatasetCreated);

    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRmlApplication(sourceDatasetCreated.getResId(), sourceDatasetFile.getResId(), ludusRml.getResId());

    // remove the rml after apply
    this.projectITService.removeRml(project.getResId(), ludusRml.getResId());
    this.restClientTool.exitSudo();

    // get RdfDatasetFile
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    RdfDatasetFile rdfFile = rdfFilesWithProjectId.stream()
            .filter(e -> e.getSourceDatasetFile().getResId().equals(sourceDatasetFile.getResId()))
            .findFirst()
            .orElseThrow(() -> new IllegalStateException(MISSING_TMP_RDF_DATASET_FILE_MSG));

    // Check if RdfDatasetFile is deleted
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.rdfDatasetFileClientService.delete(rdfFile.getResId()),
            "RdfDatasetFile was deleted even if it was not permitted !!");
  }

  @Test
  void deleteListTest() {
    this.restClientTool.sudoAdmin();
    // Create project
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);

    // Create a sourceDataset
    final SourceDataset sourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(), this.role());
    final SourceDataset sourceDatasetCreated = this.sourceDatasetClientService.create(sourceDataset);

    // Associate Project with Permanent RML
    final Rml ludusRml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);

    // Upload Ludus data sample
    final SourceDatasetFile sourceDatasetFile = this.uploadFirstSourceDataset(sourceDatasetCreated);

    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRmlApplication(sourceDatasetCreated.getResId(), sourceDatasetFile.getResId(), ludusRml.getResId());

    // remove the rml after apply
    this.projectITService.removeRml(project.getResId(), ludusRml.getResId());
    this.restClientTool.exitSudo();

    // get RdfDatasetFile
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    rdfFilesWithProjectId.stream()
            .filter(e -> e.getSourceDatasetFile().getResId().equals(sourceDatasetFile.getResId()))
            .findFirst()
            .orElseThrow(() -> new IllegalStateException(MISSING_TMP_RDF_DATASET_FILE_MSG));

    // Delete List
    final String[] ids = new String[rdfFilesWithProjectId.size()];
    int i = 0;
    for (final RdfDatasetFile rdfDatasetFile : rdfFilesWithProjectId) {
      ids[i] = rdfDatasetFile.getResId();
      i++;
    }
    // Check if RdfDatasetFiles were deleted
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.rdfDatasetFileClientService.deleteList(ids),
            "RdfDatasetFile was deleted even if it was not permitted !!");
  }

  @Test
  void iiifRefreshTest() {
    // Create project
    final Project project = this.projectITService.getOrCreatePermanentProject(HederaTestConstants.ProjectStatus.OPEN, this.role());
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.iiifRefreshService.refreshIIIF(project.getProjectId()));
  }
}
