/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - RdfDatasetFileAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static ch.hedera.test.HederaTestConstants.MISSING_TMP_RDF_DATASET_FILE_MSG;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.RdfFormat;
import ch.hedera.model.humanities.Rml;
import ch.hedera.model.ingest.DatasetFileType;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.settings.Project;
import ch.hedera.model.triplestore.SparqlResultRow;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.admin.TriplestoreQueriesClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;

@Order(HederaTestConstants.INGEST_TEST_ORDER
        + HederaTestConstants.INGEST_RDF_DATASET_TEST_ORDER
        + HederaTestConstants.ADMINISTRATOR_TEST_ORDER)
@Tag(HederaTestConstants.AS_ADMIN_TAG)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class RdfDatasetFileAsAdminIT extends AbstractIngestIT {

  private final TriplestoreQueriesClientService triplestoreQueriesClientService;

  @Autowired
  public RdfDatasetFileAsAdminIT(Environment env, SudoRestClientTool restClientTool, ProjectITService projectITService,
          SourceDatasetITService sourceDatasetITService,
          SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService,
          TriplestoreQueriesClientService triplestoreQueriesClientService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
    this.triplestoreQueriesClientService = triplestoreQueriesClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  protected ApplicationRole applicationRole() {
    return AuthApplicationRole.ADMIN;
  }

  @Test
  void createTest() {
    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());
    // Associate Project with Permanent RML
    final Rml rml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);
    // Upload Ludus data sample
    final SourceDatasetFile sourceDatasetFile = this.uploadFirstSourceDataset(sourceDataset);
    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRmlApplication(sourceDataset.getResId(), sourceDatasetFile.getResId(), rml.getResId());
    // remove the rml after apply
    this.projectITService.removeRml(project.getResId(), rml.getResId());
    // List RdfDatasetFiles
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    assertEquals(1, rdfFilesWithProjectId.size());
  }

  @Test
  void createWithWrongRmlTest() {
    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());
    // Associate Project with Permanent RML
    final Rml rml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);
    // remove the rml after apply
    this.projectITService.removeRml(project.getResId(), rml.getResId());
    // Upload Ludus data sample
    final SourceDatasetFile sourceDatasetFile = this.uploadFirstSourceDataset(sourceDataset);
    // Apply rml to generate RdfDatasetFile
    this.executeRmlApplicationWithError(sourceDataset.getResId(), sourceDatasetFile.getResId(), rml.getResId());
    // List RdfDatasetFiles
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    assertEquals(0, rdfFilesWithProjectId.size());
  }

  @Test
  void createRdfTest() {
    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());
    // Upload plieogos data sample
    final SourceDatasetFile sourceDatasetFile = this.createSourceDatasetFile(sourceDataset,
            HederaTestConstants.RDF_SOURCE_DATASET_FILE_FILENAME, DatasetFileType.RDF);
    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRdfTransformation(sourceDataset.getResId(), sourceDatasetFile.getResId());
    // List RdfDatasetFiles
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    assertEquals(1, rdfFilesWithProjectId.size());
  }

  @Test
  void createWithResearchDataFileTest() {
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    // Associate Project with Permanent RML
    final Rml rml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);
    // Get Research Data File
    final ResearchDataFile researchDataFile = this.getOrUploadResearchDataFile(project);
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());
    // SourceDataFile with ResearchDataFile
    final SourceDatasetFile sourceDatasetFile = this.createSourceDatasetFileWithResearchDataFile(sourceDataset, researchDataFile, "1.0",
            DatasetFileType.XML);
    final List<SourceDatasetFile> sourceDatasetFilelist = this.sourceDatasetClientService.getSourceDataFiles(sourceDataset.getResId());
    assertEquals(1, sourceDatasetFilelist.size());
    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRmlApplication(sourceDataset.getResId(), sourceDatasetFile.getResId(), rml.getResId());
    // remove the rml after apply
    this.projectITService.removeRml(project.getResId(), rml.getResId());
    // List RdfDatasetFiles
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    assertEquals(1, rdfFilesWithProjectId.size());
  }

  @Test
  void createMultipleRdfAtOnceTest() {
    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());
    // Upload pliegos data sample
    final SourceDatasetFile sourceDatasetFile = this.createSourceDatasetFile(sourceDataset,
            HederaTestConstants.RDF_SOURCE_DATASET_FILE_FILENAME, DatasetFileType.RDF);
    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRdfTransformation(sourceDataset.getResId(), sourceDatasetFile.getResId());
    // List RdfDatasetFiles
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    assertEquals(1, rdfFilesWithProjectId.size());
  }

  @Test
  void checkTest() {
    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());
    // Upload Ludus data sample
    final SourceDatasetFile sourceDatasetFile = this.uploadFirstSourceDataset(sourceDataset);
    // Apply rml to generate RdfDatasetFile
    this.executeAndTestFileCheck(sourceDataset.getResId(), sourceDatasetFile.getResId());
    // List RdfDatasetFiles
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    assertEquals(0, rdfFilesWithProjectId.size());
  }

  @Test
  void checkWithResearchDataFileTest() {
    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    // Get Research Data File
    final ResearchDataFile researchDataFile = this.getOrUploadResearchDataFile(project);
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());
    // SourceDataFile with ResearchDataFile
    final SourceDatasetFile sourceDatasetFile = this.createSourceDatasetFileWithResearchDataFile(sourceDataset, researchDataFile, "1.0",
            DatasetFileType.XML);
    // Apply rml to generate RdfDatasetFile
    this.executeAndTestFileCheck(sourceDataset.getResId(), sourceDatasetFile.getResId());
    // List RdfDatasetFiles
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    assertEquals(0, rdfFilesWithProjectId.size());
  }

  @Test
  void downloadTest() {
    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());
    // Associate Project with Permanent RML
    final Rml rml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);
    // Upload Ludus data sample
    final SourceDatasetFile sourceDatasetFile = this.uploadFirstSourceDataset(sourceDataset);
    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRmlApplication(sourceDataset.getResId(), sourceDatasetFile.getResId(), rml.getResId());
    // remove the rml after apply
    this.projectITService.removeRml(project.getResId(), rml.getResId());
    // get RdfDatasetFile
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    RdfDatasetFile rdfFile = rdfFilesWithProjectId.stream()
            .filter(e -> e.getSourceDatasetFile().getResId().equals(sourceDatasetFile.getResId()))
            .findFirst()
            .orElseThrow(() -> new IllegalStateException(MISSING_TMP_RDF_DATASET_FILE_MSG));

    if (rdfFile != null) {
      // Download the file
      Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.ttl");
      this.rdfDatasetFileClientService.downloadRdfDatasetFile(rdfFile.getResId(), path);
      this.checkFirstRdfDatasetFile(path);
    }
  }

  @Test
  void deleteTest() {
    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());
    // Associate Project with Permanent RML
    final Rml rml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);
    // Upload Ludus data sample
    final SourceDatasetFile sourceDatasetFile = this.uploadFirstSourceDataset(sourceDataset);

    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRmlApplication(sourceDataset.getResId(), sourceDatasetFile.getResId(), rml.getResId());

    // remove the rml after apply
    this.projectITService.removeRml(project.getResId(), rml.getResId());
    // get RdfDatasetFile
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    RdfDatasetFile rdfFile = rdfFilesWithProjectId.stream()
            .filter(e -> e.getSourceDatasetFile().getResId().equals(sourceDatasetFile.getResId()))
            .findFirst()
            .orElseThrow(() -> new IllegalStateException(MISSING_TMP_RDF_DATASET_FILE_MSG));

    this.rdfDatasetFileClientService.delete(rdfFile.getResId());
    // Check if RdfDatasetFile is deleted
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.rdfDatasetFileClientService.findOne(rdfFile.getResId()),
            "RdfDatasetFile not deleted");
  }

  @Test
  void updateTest() {
    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());
    // Associate Project with Permanent RML
    final Rml rml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);
    // Upload Ludus data sample
    final SourceDatasetFile sourceDatasetFile = this.uploadFirstSourceDataset(sourceDataset);

    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRmlApplication(sourceDataset.getResId(), sourceDatasetFile.getResId(), rml.getResId());
    // remove the rml after apply
    this.projectITService.removeRml(project.getResId(), rml.getResId());
    // get RdfDatasetFile
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    RdfDatasetFile rdfFile = rdfFilesWithProjectId.stream()
            .filter(e -> e.getSourceDatasetFile().getResId().equals(sourceDatasetFile.getResId()))
            .findFirst()
            .orElseThrow(() -> new IllegalStateException(MISSING_TMP_RDF_DATASET_FILE_MSG));

    /*
     * Does the update
     */
    rdfFile.setRdfFormat(RdfFormat.N_QUADS);
    List<String> propertiesToUpdate = new ArrayList<>();
    propertiesToUpdate.add("rdfFormat");
    this.rdfDatasetFileClientService.update(rdfFile.getResId(), rdfFile, propertiesToUpdate);

    RdfDatasetFile rdfDatasetFileUpdated = this.rdfDatasetFileClientService.findOne(rdfFile.getResId());

    // Test the upload
    assertNotNull(rdfDatasetFileUpdated);
    assertEquals(RdfFormat.N_QUADS, rdfDatasetFileUpdated.getRdfFormat());
  }

  @Test
  void closedProjectTest() {
    Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());
    project = this.projectITService.openProject(project);
    assertTrue(project.isOpen());
    // Create a sourceDataset
    final SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());
    // Associate Project with Permanent RML
    final Rml rml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);
    // Upload Ludus data sample
    final SourceDatasetFile sourceDatasetFile = this.uploadFirstSourceDataset(sourceDataset);
    // Close project
    project = this.projectITService.close(project);
    assertFalse(project.isOpen());
    // Apply rml to generate RdfDatasetFile
    final Result result = this.sourceDatasetClientService.applyRml(sourceDataset.getResId(), sourceDatasetFile.getResId(), rml.getResId());
    assertEquals(ActionStatus.NON_APPLICABLE, result.getStatus());
  }

  @Test
  void cannotDeleteRdfDatasetFileAlreadyImportedTest() {
    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a sourceDataset
    SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());

    // Associate Project with Permanent RML
    final Rml rml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);

    // Upload Ludus data sample
    final SourceDatasetFile sourceDatasetFile = this.uploadFirstSourceDataset(sourceDataset);

    // Apply rml to generate RdfDatasetFile
    this.executeAndTestRmlApplication(sourceDataset.getResId(), sourceDatasetFile.getResId(), rml.getResId());

    // remove the rml after apply
    this.projectITService.removeRml(project.getResId(), rml.getResId());
    // get RdfDatasetFile
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());
    String finalSourceDatasetFileId = sourceDatasetFile.getResId();
    RdfDatasetFile rdfFile = rdfFilesWithProjectId.stream()
            .filter(e -> e.getSourceDatasetFile().getResId().equals(finalSourceDatasetFileId))
            .findFirst()
            .orElse(null);

    // Add to tripleStore only if not imported
    if (rdfFile != null && rdfFile.getStatusImport().equals(RdfDatasetFile.RdfDatasetFileStatus.NOT_IMPORTED)) {
      this.rdfDatasetFileClientService.addInTripleStore(rdfFile.getResId(), project.getProjectId());
      this.rdfDatasetFileITService.waitForRdfDatasetFileIsImportedInFuseki(rdfFile.getResId());
    }

    // Check that RdfDatasetFile cannot be deleted
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.rdfDatasetFileClientService.delete(rdfFile.getResId()),
            "RdfDatasetFile cannot deleted");

    // Delete from fuseki the second RdfDatasetFile
    this.rdfDatasetFileClientService.deleteFromTripleStore(rdfFile.getResId(), project.getProjectId());
    this.rdfDatasetFileITService.waitForRdfDatasetFileIsNotImportedInFuseki(rdfFile.getResId());

    this.rdfDatasetFileClientService.delete(rdfFile.getResId());
  }

  @Test
  void deleteRdfFromFusekiTest() throws IOException {
    // Create project
    final Project project = this.projectITService.createTemporaryProject(HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a sourceDataset
    SourceDataset sourceDataset = this.createSourceDataset(project, this.applicationRole());

    // Associate Project with Permanent RML
    final Rml rml = this.addRml(project, HederaTestConstants.DEMO_RML_NAME);

    SourceDatasetFile firstSourceDatasetFile = this.getOrCreateSourceDatasetFile(sourceDataset,
            HederaTestConstants.FIRST_SOURCE_DATASET_FILE_FILENAME, DatasetFileType.XML);
    SourceDatasetFile secondSourceDatasetFile = this.getOrCreateSourceDatasetFile(sourceDataset,
            HederaTestConstants.SECOND_SOURCE_DATASET_FILE_FILENAME, DatasetFileType.XML);

    // Apply rml to generate RdfDatasetFile for the first SourceDatasetFile
    this.executeAndTestRmlApplication(sourceDataset.getResId(), firstSourceDatasetFile.getResId(), rml.getResId());

    // Apply rml to generate RdfDatasetFile for the second SourceDatasetFile
    this.executeAndTestRmlApplication(sourceDataset.getResId(), secondSourceDatasetFile.getResId(), rml.getResId());

    // get RdfDatasetFile
    List<RdfDatasetFile> rdfFilesWithProjectId = this.rdfDatasetFileClientService.findAllWithProjectId(project.getProjectId());

    // Since there could be more created from other test and those two were also created previously
    assertTrue(rdfFilesWithProjectId.size() >= 2);

    // Import first SourceDatasetFile into fuseki
    String finalFirstSourceDatasetFileId = firstSourceDatasetFile.getResId();
    RdfDatasetFile firstRdfFile = rdfFilesWithProjectId.stream()
            .filter(e -> e.getSourceDatasetFile().getResId().equals(finalFirstSourceDatasetFileId))
            .findFirst()
            .orElse(null);
    // Add to tripleStore only if not imported
    if (firstRdfFile != null && firstRdfFile.getStatusImport().equals(RdfDatasetFile.RdfDatasetFileStatus.NOT_IMPORTED)) {
      this.rdfDatasetFileClientService.addInTripleStore(firstRdfFile.getResId(), project.getProjectId());
      this.rdfDatasetFileITService.waitForRdfDatasetFileIsImportedInFuseki(firstRdfFile.getResId());
    }

    // Import second SourceDatasetFile into fuseki
    String finalSecondSourceDatasetFileId = secondSourceDatasetFile.getResId();
    RdfDatasetFile secondRdfFile = rdfFilesWithProjectId.stream()
            .filter(e -> e.getSourceDatasetFile().getResId().equals(finalSecondSourceDatasetFileId))
            .findFirst()
            .orElse(null);
    // Add to tripleStore only if not imported
    if (secondRdfFile != null && secondRdfFile.getStatusImport().equals(RdfDatasetFile.RdfDatasetFileStatus.NOT_IMPORTED)) {
      this.rdfDatasetFileClientService.addInTripleStore(secondRdfFile.getResId(), project.getProjectId());
      this.rdfDatasetFileITService.waitForRdfDatasetFileIsImportedInFuseki(secondRdfFile.getResId());
    }

    // Download the file
    Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "rdf_download.ttl");
    this.rdfDatasetFileClientService.downloadRdfDatasetFile(secondRdfFile.getResId(), path);

    // Read first line of the rdf data and check if th triple exists still in triplestore
    BufferedReader br = new BufferedReader(new FileReader(path.toString()));
    String subject = br.readLine();
    br.close();

    String query = "select ?s ?p ?s { " + subject + "?p ?o . }";

    // Search a triplet that appears only at second sourceDataFile and check that it really exist in triplestore
    List<SparqlResultRow> resultQuery = this.triplestoreQueriesClientService.executeQueryToDataset(project.getShortName(), query);
    assertFalse(resultQuery.isEmpty());

    // Delete from fuseki the second RdfDatasetFile
    this.rdfDatasetFileClientService.deleteFromTripleStore(secondRdfFile.getResId(), project.getProjectId());
    this.rdfDatasetFileITService.waitForRdfDatasetFileIsNotImportedInFuseki(secondRdfFile.getResId());
    this.rdfDatasetFileITService.waitForRdfDatasetFileIsImportedInFuseki(firstRdfFile.getResId());

    // Check that the second one is not imported in fuseki
    RdfDatasetFile secondRdfDatasetFile = this.rdfDatasetFileClientService.findOne(secondRdfFile.getResId());
    assertEquals(RdfDatasetFile.RdfDatasetFileStatus.NOT_IMPORTED, secondRdfDatasetFile.getStatusImport());

    // Check that the first one is still imported in fuseki
    RdfDatasetFile firstRdfDatasetFile = this.rdfDatasetFileClientService.findOne(firstRdfFile.getResId());
    assertEquals(RdfDatasetFile.RdfDatasetFileStatus.IMPORTED, firstRdfDatasetFile.getStatusImport());

    // Search in fuseki by a triplet that appears only at second sourceDataFile which is based on its name (Moreno_002) and check it does not
    // exist anymore
    resultQuery = this.triplestoreQueriesClientService.executeQueryToDataset(project.getShortName(), query);
    assertTrue(resultQuery.isEmpty());

    // Delete from fuseki the first RdfDatasetFile
    this.rdfDatasetFileClientService.deleteFromTripleStore(firstRdfDatasetFile.getResId(), project.getProjectId());
    this.rdfDatasetFileITService.waitForRdfDatasetFileIsNotImportedInFuseki(firstRdfDatasetFile.getResId());

    this.rdfDatasetFileClientService.delete(firstRdfDatasetFile.getResId());
    this.rdfDatasetFileClientService.delete(secondRdfDatasetFile.getResId());

  }
}
