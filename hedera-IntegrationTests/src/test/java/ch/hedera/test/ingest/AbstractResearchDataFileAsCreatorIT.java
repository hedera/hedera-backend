/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AbstractResearchDataFileAsCreatorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static ch.hedera.test.HederaTestConstants.REFERENCE_FILE_TO_UPLOAD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpServerErrorException;

import com.fasterxml.jackson.core.JsonProcessingException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.ResearchDataFile.ResearchDataFileFileStatus;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;

public abstract class AbstractResearchDataFileAsCreatorIT extends AbstractDatasetWithRoleIT {

  public AbstractResearchDataFileAsCreatorIT(Environment env,
          SudoRestClientTool restClientTool,
          ProjectITService projectITService,
          SourceDatasetITService sourceDatasetITService,
          SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
  }

  @Test
  void uploadResearchDataFileTest() {
    this.restClientTool.sudoAdmin();
    Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);
    this.restClientTool.exitSudo();

    // Create a researchDataFile
    final ResearchDataFile researchDataFile = this.uploadAndWaitResearchDataFileProcessing(
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(),
            DataAccessibilityType.IIIF,
            null,
            ResearchDataFileFileStatus.COMPLETED);

    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFile.getResId());

    // Test the upload
    assertEquals(researchDataFileFound.getResId(), researchDataFile.getResId());
    assertEquals(researchDataFileFound.getProjectId(), project.getResId());
    assertEquals(researchDataFileFound.getResearchObjectTypeId(), project.getResearchDataFileResearchObjectType().getResId());
    assertEquals(researchDataFileFound.getAccessibleFrom().getName(), DataAccessibilityType.IIIF.getName());
    assertEquals(REFERENCE_FILE_TO_UPLOAD, researchDataFileFound.getFileName());
  }

  @Test
  void uploadResearchDataFileWithSameNameTest() {
    this.uploadResearchDataFileWithSameNameTest(HttpServerErrorException.InternalServerError.class);
  }

  @ParameterizedTest
  @MethodSource("zipFiles")
  void deleteListTest(String zipFile, int fileNumber) {
    this.restClientTool.sudoAdmin();
    Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);
    this.restClientTool.exitSudo();

    this.deleteListTest(project, zipFile, fileNumber);
  }

  @Test
  void listFolderTest() throws JsonProcessingException {
    this.restClientTool.sudoAdmin();
    Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);
    this.restClientTool.exitSudo();

    this.listFolderTest(project);
  }

  @Test
  void deleteFolderTest() throws JsonProcessingException {
    this.restClientTool.sudoAdmin();
    Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);
    this.restClientTool.exitSudo();

    this.deleteFolderTest(project);
  }

  @ParameterizedTest
  @MethodSource("zipFiles")
  void reloadTest(String zipFile, int fileNumber) {
    this.restClientTool.sudoAdmin();
    Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);
    this.restClientTool.exitSudo();

    this.reloadTest(project, zipFile, fileNumber);
  }

  @ParameterizedTest
  @MethodSource("zipFiles")
  void uploadResearchDataFilesInZipTest(String zipFile, int fileNumber) {
    this.restClientTool.sudoAdmin();
    Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);
    this.restClientTool.exitSudo();

    this.uploadResearchDataFilesFromZipTest(project, zipFile, fileNumber);
  }

  @Test
  void downloadTest() throws IOException {
    this.restClientTool.sudoAdmin();
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);
    this.restClientTool.exitSudo();

    this.downloadTest(project);

  }

  @Test
  void deleteTest() {
    this.restClientTool.sudoAdmin();
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);
    this.restClientTool.exitSudo();

    this.deleteTest(project);
  }

  @Test
  void updateTest() {
    this.restClientTool.sudoAdmin();
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);
    this.restClientTool.exitSudo();

    this.updateTest(project);

  }

  protected void uploadResearchDataFileWithSameNameTest(Class<? extends Exception> expectedException) {
    this.restClientTool.sudoAdmin();
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, this.role(), AuthApplicationRole.USER);

    // Create a researchDataFile
    final ResearchDataFile researchDataFile = this.uploadAndWaitResearchDataFileProcessing(
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(),
            DataAccessibilityType.IIIF,
            null,
            ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);

    this.restClientTool.exitSudo();

    // Start test
    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFile.getResId());

    // Test the upload
    assertEquals(researchDataFileFound.getResId(), researchDataFile.getResId());
    assertEquals(researchDataFileFound.getProjectId(), project.getResId());
    assertEquals(researchDataFileFound.getResearchObjectTypeId(), project.getResearchDataFileResearchObjectType().getResId());
    assertEquals(researchDataFileFound.getAccessibleFrom().getName(), DataAccessibilityType.IIIF.getName());
    assertEquals(REFERENCE_FILE_TO_UPLOAD, researchDataFileFound.getFileName());

    // Upload in the same ResearchDataFile a file with same name
    assertThrows(expectedException, () -> this.researchDataFileClientService.uploadFile(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(), DataAccessibilityType.IIIF.getName(), null));
  }
}
