/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AbstractSourceDatasetForAdminOrRootIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static ch.unige.solidify.SolidifyConstants.DB_DEFAULT_STRING_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.HederaTestConstants;
import ch.hedera.test.HederaTestConstants.ProjectStatus;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;

public abstract class AbstractSourceDatasetForAdminOrRootIT extends AbstractIngestIT {
  public AbstractSourceDatasetForAdminOrRootIT(Environment env,
          SudoRestClientTool restClientTool, ProjectITService projectITService,
          SourceDatasetITService sourceDatasetITService,
          SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
  }

  protected abstract ApplicationRole applicationRole();

  @Test
  void creationTest() {
    final Project project = this.projectITService.getOrCreatePermanentProjectForNoOne();

    // Create a sourceDataset
    final SourceDataset localSourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(),
            this.applicationRole());
    final SourceDataset remoteSourceDataset = this.sourceDatasetClientService.create(localSourceDataset);

    // Test the creation
    assertEquals(localSourceDataset.getName(), remoteSourceDataset.getName());
    assertEquals(localSourceDataset.getDescription(), remoteSourceDataset.getDescription());

    // Wait one second before testing creation
    SolidifyTime.waitOneSecond();

    // Check that the dataset has been created
    final List<SourceDataset> sourceDatasets = this.sourceDatasetClientService.searchByProperties(Map.of("name", remoteSourceDataset.getName(),
            HederaConstants.PROJECT_ID_FIELD, project.getResId()));
    assertEquals(1, sourceDatasets.size());
  }

  @Test
  void cannotCreateTwoSourceDatasetWithSameNameTest() {
    final Project project = this.projectITService.getOrCreatePermanentProjectForNoOne();

    String name = HederaTestConstants.getRandomNameWithTemporaryLabel("Source Dataset");

    // Create a sourceDataset
    final SourceDataset localSourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(), name, false);
    final SourceDataset remoteSourceDataset = this.sourceDatasetClientService.create(localSourceDataset);

    // Test the creation
    assertEquals(localSourceDataset.getName(), remoteSourceDataset.getName());
    assertEquals(localSourceDataset.getDescription(), remoteSourceDataset.getDescription());

    // Wait one second before testing creation
    SolidifyTime.waitOneSecond();

    // Check that the dataset has been created
    final List<SourceDataset> sourceDatasets = this.sourceDatasetClientService.searchByProperties(Map.of("name", remoteSourceDataset.getName(),
            HederaConstants.PROJECT_ID_FIELD, project.getResId()));
    assertEquals(1, sourceDatasets.size());

    // Create a sourceDataset that generates an exception
    final SourceDataset localSourceDataset2 = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(),
            name, false);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.sourceDatasetClientService.create(localSourceDataset2));

  }

  @Test
  void updateTest() {
    final Project project = this.projectITService.getOrCreatePermanentProjectForNoOne();

    String name = "SourceDataset";

    // Create a sourceDataset
    final SourceDataset localSourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(), name);
    final SourceDataset remoteSourceDataset = this.sourceDatasetClientService.create(localSourceDataset);

    // Test the creation
    assertEquals(localSourceDataset.getName(), remoteSourceDataset.getName());
    assertEquals(localSourceDataset.getDescription(), remoteSourceDataset.getDescription());

    // Wait one second before testing creation
    SolidifyTime.waitOneSecond();

    // Check that the dataset has been created
    final List<SourceDataset> sourceDatasets = this.sourceDatasetClientService.searchByProperties(Map.of("name", remoteSourceDataset.getName(),
            HederaConstants.PROJECT_ID_FIELD, project.getResId()));
    assertEquals(1, sourceDatasets.size());

    remoteSourceDataset.setName(HederaTestConstants.getRandomNameWithTemporaryLabel("SourceDataset update"));
    this.sourceDatasetClientService.update(remoteSourceDataset.getResId(), remoteSourceDataset);
    final SourceDataset updatedSourceDataset = this.sourceDatasetClientService.findOne(remoteSourceDataset.getResId());

    // Test the update
    assertEquals(remoteSourceDataset.getName(), updatedSourceDataset.getName());
    this.assertEqualsWithoutNanoSeconds(remoteSourceDataset.getCreationTime(), updatedSourceDataset.getCreationTime());
    assertNotNull(updatedSourceDataset);
  }

  @Test
  void cannotUpdateNameAlreadyUsedTest() {
    final Project project = this.projectITService.getOrCreatePermanentProjectForNoOne();

    final String name = "Source Dataset";

    // Create a sourceDataset
    final SourceDataset localSourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(), name);
    final SourceDataset remoteSourceDataset = this.sourceDatasetClientService.create(localSourceDataset);

    // Test the creation
    assertEquals(localSourceDataset.getName(), remoteSourceDataset.getName());
    assertEquals(localSourceDataset.getDescription(), remoteSourceDataset.getDescription());

    // Wait one second before testing creation
    SolidifyTime.waitOneSecond();

    // Check that the dataset has been created
    final List<SourceDataset> sourceDatasets = this.sourceDatasetClientService.searchByProperties(Map.of("name", remoteSourceDataset.getName(),
            HederaConstants.PROJECT_ID_FIELD, project.getResId()));
    assertEquals(1, sourceDatasets.size());

    final String secondName = "Yet another datasourceset";

    // Create a sourceDataset that generates an exception
    final SourceDataset localSourceDataset2 = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(), secondName);
    SourceDataset remoteSourceDataset2 = this.sourceDatasetClientService.create(localSourceDataset2);

    // Test the creation
    assertEquals(localSourceDataset2.getName(), remoteSourceDataset2.getName());
    assertEquals(localSourceDataset2.getDescription(), remoteSourceDataset2.getDescription());

    remoteSourceDataset2.setName(remoteSourceDataset.getName());
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.sourceDatasetClientService.update(remoteSourceDataset2.getResId(), remoteSourceDataset2));

  }

  @Test
  void tooLongTitleTest() {
    final Project project = this.projectITService.getOrCreatePermanentProjectForNoOne();

    final int maxTitleLength = DB_DEFAULT_STRING_LENGTH;
    final String randomString = new Random().ints(5, 0, 10).mapToObj(String::valueOf).collect(Collectors.joining());
    final String tooLongTitle = randomString + "X".repeat(maxTitleLength - randomString.length() + 1);

    // Create a source dataset with the maximum name length
    final SourceDataset localSourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(), tooLongTitle);
    localSourceDataset.setName(localSourceDataset.getName().substring(0, maxTitleLength));
    this.sourceDatasetClientService.create(localSourceDataset);

    // Try to create a source dataset with a name length exceeding the maximum limit
    final SourceDataset localSourceDataset2 = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(), tooLongTitle);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.sourceDatasetClientService.create(localSourceDataset2));
  }

  @Test
  void deleteTest() {
    final Project project = this.projectITService.getOrCreatePermanentProjectForNoOne();

    // Create a dataset
    final SourceDataset localSourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(),
            this.applicationRole());
    final SourceDataset remoteSourceDataset = this.sourceDatasetClientService.create(localSourceDataset);

    // Test the creation
    assertEquals(localSourceDataset.getName(), remoteSourceDataset.getName());
    assertEquals(localSourceDataset.getDescription(), remoteSourceDataset.getDescription());

    // Wait one second before testing creation
    SolidifyTime.waitOneSecond();

    // Delete the dataset
    final String resId = remoteSourceDataset.getResId();
    this.sourceDatasetClientService.delete(resId);

    assertTrue(this.sourceDatasetITService.waitSourceDatasetIsDeleted(resId));
  }

  @Test
  void closedProjectTest() {
    final Project project = this.projectITService.getClosedPermanentProject();
    assertFalse(project.isOpen());
    final SourceDataset localSourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(),
            this.applicationRole());
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.sourceDatasetClientService.create(localSourceDataset));
  }

  @Test
  void deleteProjectTest() {
    final Project project = this.projectITService.createTemporaryProject(
            HederaTestConstants.ProjectStatus.OPEN, null, this.applicationRole());

    // Create a dataset
    final SourceDataset localSourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(),
            this.applicationRole());
    final SourceDataset remoteSourceDataset = this.sourceDatasetClientService.create(localSourceDataset);

    // Test the creation
    assertEquals(localSourceDataset.getName(), remoteSourceDataset.getName());
    assertEquals(localSourceDataset.getDescription(), remoteSourceDataset.getDescription());

    // Wait one second before testing creation
    SolidifyTime.waitOneSecond();

    final Project projectWithData = this.projectITService.openProject(project);
    this.checkProject(project.getResId(), true, ProjectStatus.OPEN, projectWithData);

    // Close project
    final Project closedProject = this.projectITService.close(project.getResId(), LocalDate.now().minusDays(1).toString());
    this.checkProject(project.getResId(), true, ProjectStatus.CLOSED, closedProject);

    // Try to delete the dataset
    final String resId = remoteSourceDataset.getResId();
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.sourceDatasetClientService.delete(resId));

    // Re-open project
    final Project openProject = this.projectITService.openProject(project);
    this.checkProject(project.getResId(), true, ProjectStatus.OPEN, openProject);

    // Try to delete project
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.projectITService.delete(project.getResId()));

    // Try to update project ShortName
    project.setShortName("temp-project-forbidden");
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.projectITService.update(project.getResId(), project));

    // Delete the dataset
    this.sourceDatasetClientService.delete(resId);
    assertTrue(this.sourceDatasetITService.waitSourceDatasetIsDeleted(resId));

    // Wait one second before testing creation
    SolidifyTime.waitOneSecond();

    final Project emptyProject = this.projectITService.findOne(project.getResId());
    this.checkProject(project.getResId(), false, ProjectStatus.OPEN, emptyProject);

    // Delete project
    this.projectITService.delete(project.getResId());
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.projectITService.findOne(project.getResId()));
  }

  @Test
  void advancedSearchTest() {
    final String name = "SourceDataset for updated test";
    final Project project = this.projectITService.getOrCreatePermanentProjectForNoOne();

    final SourceDataset sourceDataset = this.sourceDatasetITService.createTemporaryLocalSourceDataset(project.getResId(),
            this.applicationRole());
    this.sourceDatasetClientService.create(sourceDataset);

    final String search = "i-name~updated";
    List<SourceDataset> sourceDatasets = this.sourceDatasetClientService.search(search, "any");
    sourceDatasets.forEach(d -> assertTrue(d.getName().contains(name)));
  }

}
