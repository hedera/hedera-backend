/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Integration Tests - AbstractResearchDataFileBaseIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.ingest;

import static ch.hedera.test.HederaTestConstants.JAVA_TEMPORARY_FOLDER;
import static ch.hedera.test.HederaTestConstants.REFERENCE_FILE_TO_UPLOAD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.core.JsonProcessingException;

import ch.unige.solidify.rest.Result;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.settings.Project;
import ch.hedera.service.admin.RmlClientService;
import ch.hedera.service.ingest.ResearchDataFileClientService;
import ch.hedera.service.ingest.SourceDatasetClientService;
import ch.hedera.test.service.ProjectITService;
import ch.hedera.test.service.RdfDatasetFileITService;
import ch.hedera.test.service.ResearchDataFileITService;
import ch.hedera.test.service.ResearchObjectTypeITService;
import ch.hedera.test.service.SourceDatasetITService;

abstract public class AbstractResearchDataFileBaseIT extends AbstractIngestIT {

  private static final String FILE_NAME = "geneve.jpg";
  private static final String FOLDER1 = "/other";
  private static final String FOLDER2 = "/another";

  public AbstractResearchDataFileBaseIT(Environment env, SudoRestClientTool restClientTool,
          ProjectITService projectITService, SourceDatasetITService sourceDatasetITService,
          SourceDatasetClientService sourceDatasetClientService,
          ResearchDataFileITService researchDataFileITService,
          ResearchDataFileClientService researchDataFileClientService,
          RdfDatasetFileITService rdfDatasetFileITService,
          ResearchObjectTypeITService researchObjectTypeITService,
          RmlClientService rmlClientService) {
    super(env, restClientTool, projectITService, sourceDatasetITService, sourceDatasetClientService, researchDataFileITService,
            researchDataFileClientService, rdfDatasetFileITService, researchObjectTypeITService, rmlClientService);
  }

  public void listFolderTest(Project project) throws JsonProcessingException {

    // Create a researchDataFile
    final ResearchDataFile researchDataFile1 = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(), DataAccessibilityType.IIIF, null, ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);
    final ResearchDataFile researchDataFile2 = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(FILE_NAME),
            project.getResId(), DataAccessibilityType.IIIF, null, ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);

    ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFile1.getResId());
    ResearchDataFile researchDataFileFound2 = this.researchDataFileClientService.findOne(researchDataFile2.getResId());

    researchDataFileFound.setRelativeLocation(FOLDER1);
    ResearchDataFile researchDataFileUpdated = this.researchDataFileClientService.update(researchDataFileFound.getResId(),
            researchDataFileFound);

    researchDataFileFound2.setRelativeLocation(FOLDER2);
    ResearchDataFile researchDataFileUpdated2 = this.researchDataFileClientService.update(researchDataFileFound2.getResId(),
            researchDataFileFound2);

    this.researchDataFileITService.waitResearchDataFileIsProcessed(researchDataFileUpdated.getResId(),
            ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);
    this.researchDataFileITService.waitResearchDataFileIsProcessed(researchDataFileUpdated2.getResId(),
            ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);

    assertEquals(researchDataFile1.getProjectId(), researchDataFileUpdated.getProjectId());
    assertEquals(new ClassPathResource(FOLDER1), new ClassPathResource(researchDataFileUpdated.getRelativeLocation()));
    assertEquals(researchDataFile1.getProjectId(), researchDataFileUpdated2.getProjectId());
    assertEquals(new ClassPathResource(FOLDER2), new ClassPathResource(researchDataFileUpdated2.getRelativeLocation()));

    List<String> listFolders = this.researchDataFileClientService.listFolders(project.getResId());
    assertEquals(listFolders.size(), 2);
    assertTrue(listFolders.contains(FOLDER1));
    assertTrue(listFolders.contains(FOLDER2));
  }

  void deleteFolderTest(Project project) throws JsonProcessingException {

    // Create a researchDataFile
    final ResearchDataFile researchDataFile1 = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(), DataAccessibilityType.IIIF, null, ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);
    final ResearchDataFile researchDataFile2 = this.uploadAndWaitResearchDataFileProcessing(new ClassPathResource(FILE_NAME),
            project.getResId(), DataAccessibilityType.IIIF, null, ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);

    ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFile1.getResId());
    ResearchDataFile researchDataFileFound2 = this.researchDataFileClientService.findOne(researchDataFile2.getResId());

    researchDataFileFound.setRelativeLocation(FOLDER1);
    ResearchDataFile researchDataFileUpdated = this.researchDataFileClientService.update(researchDataFileFound.getResId(),
            researchDataFileFound);

    researchDataFileFound2.setRelativeLocation(FOLDER1);
    ResearchDataFile researchDataFileUpdated2 = this.researchDataFileClientService.update(researchDataFileFound2.getResId(),
            researchDataFileFound2);
    this.researchDataFileITService.waitResearchDataFileIsProcessed(researchDataFileUpdated.getResId(),
            ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);
    this.researchDataFileITService.waitResearchDataFileIsProcessed(researchDataFileUpdated2.getResId(),
            ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);

    List<String> listFolders = this.researchDataFileClientService.listFolders(project.getResId());
    assertEquals(listFolders.size(), 1);
    assertTrue(listFolders.contains(FOLDER1));

    this.researchDataFileClientService.deleteFolder(project.getResId(), FOLDER1);

    listFolders = this.researchDataFileClientService.listFolders(project.getResId());
    assertEquals(listFolders.size(), 0);
  }

  public void reloadTest(Project project, String zipFile, int fileNumber) {
    List<ResearchDataFile> result = this.researchDataFileClientService.uploadZipFile(new ClassPathResource(zipFile), project.getResId(),
            DataAccessibilityType.IIIF.getName());
    // Wait 1s for JMS message is sent/processed
    SolidifyTime.waitInSeconds(fileNumber);

    assertEquals(fileNumber, result.size());
    assertTrue(this.researchDataFileITService.waitAllResearchDataFilesIsProcessed(project.getResId(), fileNumber));

    final Result resultAction = this.researchDataFileClientService.reload(project.getResId());
    assertNotNull(resultAction);
    assertEquals(Result.ActionStatus.EXECUTED, resultAction.getStatus());
    // Wait 1s for JMS message is sent/processed
    SolidifyTime.waitInSeconds(fileNumber);
    assertTrue(this.researchDataFileITService.waitAllResearchDataFilesIsProcessed(project.getResId(), fileNumber));
  }

  public void uploadResearchDataFilesFromZipTest(Project project, String zipFile, int fileNumber) {
    List<ResearchDataFile> result = this.researchDataFileClientService.uploadZipFile(new ClassPathResource(zipFile), project.getResId(),
            DataAccessibilityType.IIIF.getName());

    assertEquals(fileNumber, result.size());
    assertTrue(this.researchDataFileITService.waitAllResearchDataFilesIsProcessed(project.getResId(), fileNumber));
  }

  public void downloadTest(Project project) {
    final ClassPathResource fileToUpload = new ClassPathResource(REFERENCE_FILE_TO_UPLOAD);
    final Long expectedFileSize = this.checkFile(fileToUpload);

    // Create a researchDataFileCreated
    final ResearchDataFile researchDataFileCreated = this.uploadAndWaitResearchDataFileProcessing(
            fileToUpload,
            project.getResId(),
            DataAccessibilityType.IIIF,
            null,
            ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);

    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFileCreated.getResId());

    assertEquals(researchDataFileCreated.getProjectId(), researchDataFileFound.getProjectId());

    // Download the file
    Path path = Paths.get(System.getProperty(JAVA_TEMPORARY_FOLDER), "download.tmp");
    this.researchDataFileClientService.downloadResearchDataFile(researchDataFileFound.getResId(), path);

    assertEquals(expectedFileSize, path.toFile().length());
  }

  public void deleteTest(Project project) {
    // Create a researchDataFileCreated
    final ResearchDataFile researchDataFileCreated = this.uploadAndWaitResearchDataFileProcessing(
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(),
            DataAccessibilityType.IIIF,
            null,
            ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);

    final ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFileCreated.getResId());

    assertEquals(researchDataFileCreated.getProjectId(), researchDataFileFound.getProjectId());
    this.researchDataFileClientService.delete(researchDataFileFound.getResId());

    assertThrows(HttpClientErrorException.NotFound.class, () -> this.researchDataFileClientService.findOne(researchDataFileFound.getResId()));
  }

  public void updateTest(Project project) {
    // Create a researchDataFileCreated
    final ResearchDataFile researchDataFileCreated = this.uploadAndWaitResearchDataFileProcessing(
            new ClassPathResource(REFERENCE_FILE_TO_UPLOAD),
            project.getResId(),
            DataAccessibilityType.IIIF,
            null,
            ResearchDataFile.ResearchDataFileFileStatus.COMPLETED);

    ResearchDataFile researchDataFileFound = this.researchDataFileClientService.findOne(researchDataFileCreated.getResId());

    assertEquals(researchDataFileCreated.getProjectId(), researchDataFileFound.getProjectId());

    researchDataFileFound.setAccessibleFrom(DataAccessibilityType.TEI);
    ResearchDataFile researchDataFileUpdated = this.researchDataFileClientService.update(researchDataFileFound.getResId(),
            researchDataFileFound);

    assertEquals(researchDataFileCreated.getProjectId(), researchDataFileUpdated.getProjectId());
    assertEquals(DataAccessibilityType.TEI, researchDataFileUpdated.getAccessibleFrom());
  }

  public void deleteListTest(Project project, String zipFile, int fileNumber) {
    List<ResearchDataFile> result = this.researchDataFileClientService.uploadZipFile(new ClassPathResource(zipFile), project.getResId(),
            DataAccessibilityType.IIIF.getName());
    assertEquals(fileNumber, result.size());
    assertTrue(this.researchDataFileITService.waitAllResearchDataFilesIsProcessed(project.getResId(), fileNumber));

    // Now we list all the files and delete all of them
    List<ResearchDataFile> researchDataFileList = this.researchDataFileClientService.findAllWithProjectId(project.getResId());
    this.researchDataFileClientService.deleteList(researchDataFileList.stream().map(ResearchDataFile::getResId).toArray(String[]::new));

    // Check that they are deleted
    researchDataFileList = this.researchDataFileClientService.findAllWithProjectId(project.getResId());
    assertTrue(researchDataFileList.isEmpty());
  }

}
