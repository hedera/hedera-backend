/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - SchemaController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.security.EveryonePermissions;

import ch.hedera.HederaConstants;
import ch.hedera.rest.ResourceName;

@EveryonePermissions
@RestController
@RequestMapping("/" + ResourceName.SCHEMA)
public class SchemaController extends SolidifyController {

  @GetMapping
  public HttpEntity<StreamingResponseBody> schema() {
    final ClassPathResource xsd = new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + HederaConstants.HEDERA_RESEARCH_OBJECT_SCHEMA_1);
    try {
      final InputStream is = xsd.getInputStream();
      return this.buildDownloadResponseEntity(is, xsd.getFilename(), SolidifyConstants.XML_MIME_TYPE, xsd.contentLength());
    } catch (final IOException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

}
