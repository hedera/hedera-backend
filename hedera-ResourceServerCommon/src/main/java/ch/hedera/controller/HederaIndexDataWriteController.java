/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - HederaIndexDataWriteController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller;

import ch.unige.solidify.controller.index.IndexDataWriteController;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.service.IndexFieldAliasInfoProvider;

import ch.hedera.model.index.ResearchObjectMetadata;

public abstract class HederaIndexDataWriteController<T extends IndexMetadata> extends IndexDataWriteController<ResearchObjectMetadata> {

  private final String indexName;

  public HederaIndexDataWriteController(IndexingService<ResearchObjectMetadata> indexResourceService,
          IndexFieldAliasInfoProvider indexFieldAliasInfoProvider, String indexName) {
    super(indexResourceService, indexFieldAliasInfoProvider);
    this.indexName = indexName;
  }

  @Override
  protected String getIndex() {
    return this.indexName;
  }

}
