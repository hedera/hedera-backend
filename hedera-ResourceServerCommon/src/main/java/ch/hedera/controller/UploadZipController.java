/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - UploadZipController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.ZipTool;
import ch.unige.solidify.validation.ValidationError;

public interface UploadZipController {
  default List<Path> uploadZip(String tmpFolder, MultipartFile zipFile, String extractFolder, MessageService messageService) throws IOException {
    // Check temporary folder
    final Path tmpFolderPath = Paths.get(tmpFolder);
    if (!FileTool.ensureFolderExists(tmpFolderPath)) {
      throw new SolidifyRuntimeException(messageService.get("message.upload.folder.notcreated", new Object[] { tmpFolder }));
    }

    // Copy zip file to temporary folder
    if (zipFile.getOriginalFilename() == null) {
      throw new SolidifyValidationException(new ValidationError("Missing original file name"));
    }
    Files.copy(zipFile.getInputStream(), tmpFolderPath.resolve(zipFile.getOriginalFilename()));

    final String zipFilePathString = tmpFolder + File.separator + zipFile.getOriginalFilename();
    final Path zipFilePath = Paths.get(zipFilePathString);
    final Path extractFolderPath = Paths.get(extractFolder);

    // Ensure extract folder exists
    if (!FileTool.ensureFolderExists(extractFolderPath)) {
      FileTool.deleteFile(zipFilePath);
      throw new IOException(messageService.get("message.upload.folder.notexist", new Object[] { extractFolderPath }));
    }

    // Extract zip file
    final List<Path> extractedFiles;
    final ZipTool zip = new ZipTool(zipFilePathString);
    try {
      extractedFiles = zip.unzipFilesAndReturnList(extractFolderPath);
    } catch (IOException e) {
      FileTool.deleteFile(zipFilePath);
      throw new IllegalStateException(messageService.get("message.upload.zip.invalid"));
    }

    // Delete zip file
    FileTool.deleteFile(zipFilePath);

    return extractedFiles;
  }
}
