/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - HederaControllerAction.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.controller;

public enum HederaControllerAction {
  APPROVE, APPROVE_DISPOSAL, CHECK, CHECK_COMPLIANCE, CREATE, DELETE, DELETE_FILE, DELETE_FOLDER, DOWNLOAD, DOWNLOAD_ARCHIVE, DOWNLOAD_FILE,
  DOWNLOAD_SIP_FILE, ENABLE_REVISION, EXTEND_RETENTION, GET, GET_AIP, GET_CURRENT_FORM, GET_FILE, GET_FORM, GET_MEMBER, HISTORY, LIST,
  LIST_FILES, LIST_FORMS, LIST_MEMBERS, PREPARE_DOWNLOAD_ARCHIVE, START_METADATA_EDITING, REINDEX, REJECT, RESERVE_DOI, RESUME, RESUME_FILE, RELOAD,
  SEND_FILES, CANCEL_METADATA_EDITING, SUBMIT_FOR_APPROVAL, UPDATE, UPDATE_FILE, UPLOAD_FILE, VALIDATE_FILE, UPLOAD_RESEARCH_DATA_FILE,
  ADD_DATASET, REPLACE_DATASET, DELETE_DATASET, EXEC_QUERY
}
