/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - HederationPermissionsFilter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.business;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.Resource;

import ch.hedera.model.settings.Project;
import ch.hedera.service.rest.propagate.PropagateProjectRemoteResourceService;
import ch.hedera.specification.FilterableByProjectsSpecification;

@Service
public class HederationPermissionsFilter<T extends Resource> implements SpecificationPermissionsFilter<T> {

  @Autowired
  protected PropagateProjectRemoteResourceService projectResourceService;

  @Override
  public Specification<T> addPermissionsFilter(Specification<T> spec, Principal principal) {

    if (spec instanceof FilterableByProjectsSpecification) {
      spec = this.addProjectsFilter((FilterableByProjectsSpecification<T>) spec, principal);
    }

    return spec;
  }

  protected Specification<T> addProjectsFilter(FilterableByProjectsSpecification<T> spec, Principal principal) {

    List<Project> projects = this.projectResourceService.getAuthorizedProjects(principal);

    if (projects == null) {
      projects = new ArrayList<>();
    }

    spec.setProjects(projects);

    return (Specification<T>) spec;
  }
}
