/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - ResearchObjectMetadataService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.index;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.IndexProperties;
import ch.unige.solidify.index.ElasticsearchClientProvider;
import ch.unige.solidify.index.indexing.elasticsearch.ElasticsearchService;

import ch.hedera.model.index.ResearchObjectMetadata;

@Profile("index-elasticsearch")
@Service
public class ResearchObjectMetadataService extends ElasticsearchService<ResearchObjectMetadata> {
  public ResearchObjectMetadataService(
          IndexProperties indexProperties,
          ElasticsearchClientProvider elasticsearchClientProvider) {
    super(indexProperties, elasticsearchClientProvider);
  }

  @Override
  protected ResearchObjectMetadata getMetadataObject() {
    return new ResearchObjectMetadata();
  }

  @Override
  public String getIndexName(String indexName) {
    return indexName;
  }

}
