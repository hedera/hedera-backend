/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - HistoryService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import java.time.OffsetDateTime;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.service.MessageService;

import ch.hedera.model.PackageStatus;
import ch.hedera.model.StatusHistory;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.model.security.User;
import ch.hedera.repository.HistoryRepository;
import ch.hedera.service.rest.fallback.FallbackUserRemoteResourceService;

@Service
public class HistoryService extends HederaService {
  private static final Logger log = LoggerFactory.getLogger(HistoryService.class);

  private final HistoryRepository historyRepository;
  private final FallbackUserRemoteResourceService userResourceService;

  public HistoryService(MessageService messageService,
          HistoryRepository historyRepository, FallbackUserRemoteResourceService userResourceService) {
    super(messageService);
    this.historyRepository = historyRepository;
    this.userResourceService = userResourceService;
  }

  public Page<StatusHistory> findByResId(String resId, Pageable pageable) {
    final Page<StatusHistory> listItem = this.historyRepository.findByResId(resId, pageable);

    listItem.forEach(statusHistory -> {
      final User user = this.userResourceService.findByExternalUid(statusHistory.getCreatedBy());
      if (user != null && user.getPerson() != null) {
        statusHistory.setCreatorName(user.getPerson().getFullName());
      }
    });

    return listItem;
  }

  public List<StatusHistory> getHistory(String resId) {
    return this.historyRepository.findByResId(resId);
  }

  @EventListener
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void saveEvent(StatusHistory event) {
    if (log.isTraceEnabled()) {
      log.trace("Received StatusHistory event {}", event);
    }
    // Read last event
    final List<StatusHistory> last = this.historyRepository.findLast(event.getResId());
    if (last.isEmpty() || !last.get(0).getStatus().equals(event.getStatus())) {
      // Add new event
      this.checkEvent(event);
      this.historyRepository.save(event);
    }
  }

  public void checkLastEvents(String resId, String errorMessage, String... statusList) {
    // Sort history by date
    final List<StatusHistory> sortedList = this.sortStatusList(resId);

    for (int i = 0; i < statusList.length; i++) {
      // Check status
      if (!sortedList.get(sortedList.size() - 1 - i).getStatus().equals(statusList[i])) {
        throw new SolidifyCheckingException(errorMessage);
      }
    }
  }

  public String getLastStatus(String resId) {
    return this.getLastStatusEvent(resId).getStatus();
  }

  public String getStatusBeforeError(String resId, String errorStatus) {
    // Sort history by date
    final List<StatusHistory> sortedList = this.sortStatusList(resId);

    // Check if error
    if (sortedList.size() < 2 || !sortedList.get(sortedList.size() - 1).getStatus().equals(errorStatus)) {
      throw new SolidifyCheckingException("Entity " + resId + " not in error");
    }
    return sortedList.get(sortedList.size() - 2).getStatus();
  }

  public OffsetDateTime getLastCompletedUpdate(String resId) {
    final OffsetDateTime lastEvent = this.getLastStatusEvent(resId).getChangeTime();
    final OffsetDateTime lastEdition = this.findLastStatusTimeBefore(resId, PackageStatus.EDITING_METADATA.toString(), lastEvent);
    final OffsetDateTime firstCompleted = this.findFirstStatusTime(resId, PackageStatus.COMPLETED.toString());

    if (lastEdition == null) {
      return firstCompleted;
    } else {
      return this.findFirstStatusTimeAfter(resId, PackageStatus.COMPLETED.toString(), lastEdition);
    }
  }

  public StatusHistory findLastCompletedOrCleaned(String resId) {
    final List<StatusHistory> statusList = this.historyRepository.findLastCompletedOrCleanedStatus(resId);
    if (statusList.isEmpty()) {
      return null;
    } else {
      return statusList.get(0);
    }
  }

  private OffsetDateTime findLastStatusTime(String resId, String status) {
    List<StatusHistory> statusHistoryList = this.historyRepository.findLastWithStatus(resId, status);
    if (statusHistoryList.isEmpty()) {
      return null;
    } else {
      return statusHistoryList.get(0).getChangeTime();
    }
  }

  private OffsetDateTime findFirstStatusTime(String resId, String status) {
    List<StatusHistory> statusHistoryList = this.historyRepository.findFirstWithStatus(resId, status);
    if (statusHistoryList.isEmpty()) {
      return null;
    } else {
      return statusHistoryList.get(0).getChangeTime();
    }
  }

  private OffsetDateTime findFirstStatusTimeAfter(String resId, String status, OffsetDateTime after) {
    List<StatusHistory> statusHistoryList = this.historyRepository.findFirstWithStatusAfter(resId, status, after);
    if (statusHistoryList.isEmpty()) {
      return null;
    } else {
      return statusHistoryList.get(0).getChangeTime();
    }
  }

  private OffsetDateTime findLastStatusTimeBefore(String resId, String status, OffsetDateTime before) {
    List<StatusHistory> statusHistoryList = this.historyRepository.findLastWithStatusBefore(resId, status, before);
    if (statusHistoryList.isEmpty()) {
      return null;
    } else {
      return statusHistoryList.get(0).getChangeTime();
    }
  }

  private StatusHistory getLastStatusEvent(String resId) {
    // Sort history by date
    final List<StatusHistory> sortedList = this.sortStatusList(resId);
    return sortedList.get(sortedList.size() - 1);
  }

  private List<StatusHistory> sortStatusList(String resId) {
    return this.getHistory(resId).stream()
            .sorted(Comparator.comparing(StatusHistory::getSeq))
            .toList();
  }

  private void checkEvent(StatusHistory event) {
    if (event.getDescription() == null) {
      String msg = "status.history";
      if (event.getType().equals(ResearchDataFile.class.getSimpleName())) {
        msg = "ingest.researchDataFile.status." + event.getStatus().toLowerCase();
      } else if (event.getType().equals(SourceDataset.class.getSimpleName())) {
        msg = "ingest.sourceDataset.status." + event.getStatus().toLowerCase();
      } else if (event.getType().equals(SourceDatasetFile.class.getSimpleName())) {
        msg = "ingest.sourceDatasetFile.status." + event.getStatus().toLowerCase();
      } else if (event.getType().equals(RdfDatasetFile.class.getSimpleName())) {
        msg = "ingest.rdfDatasetFile.status." + event.getStatus().toLowerCase();
      }
      event.setDescription(this.messageService.get(msg));
    }
  }

}
