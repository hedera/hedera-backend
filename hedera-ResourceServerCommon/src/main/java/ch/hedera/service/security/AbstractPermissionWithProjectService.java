/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - AbstractPermissionWithProjectService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.security;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.security.SolidifyPermissionService;
import ch.unige.solidify.util.StringTool;

import ch.hedera.controller.HederaControllerAction;
import ch.hedera.model.ProjectAwareResource;
import ch.hedera.model.security.Role;
import ch.hedera.service.rest.abstractservice.PersonRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedPersonRemoteResourceService;

public abstract class AbstractPermissionWithProjectService implements SolidifyPermissionService {

  private static final Logger logger = LoggerFactory.getLogger(AbstractPermissionWithProjectService.class);

  protected final PersonRemoteResourceService trustedPersonRemoteResourceService;

  protected AbstractPermissionWithProjectService(TrustedPersonRemoteResourceService trustedPersonRemoteResourceService) {
    this.trustedPersonRemoteResourceService = trustedPersonRemoteResourceService;
  }

  public boolean isAllowed(String targetResId, String actionString) {

    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    String personId = this.getPersonId();
    ProjectAwareResource existingResource = this.getExistingResource(targetResId);
    if (existingResource == null) {
      throw new NoSuchElementException(targetResId);
    }
    HederaControllerAction action = this.getControllerAction(actionString);
    return this.isAllowedToPerformActionOnResource(personId, existingResource, action);

  }

  public boolean isAllowedToCreate(ProjectAwareResource resource) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    String personId = this.getPersonId();
    return this.isAllowedToPerformActionOnResource(personId, resource, HederaControllerAction.CREATE);
  }

  public boolean isAllowedToUpdate(String targetResId) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    String personId = this.getPersonId();
    ProjectAwareResource existingResource = this.getExistingResource(targetResId);
    return this.isAllowedToPerformActionOnResource(personId, existingResource, HederaControllerAction.UPDATE);
  }

  boolean isVisitorAllowed(ProjectAwareResource existingResource, HederaControllerAction action) {
    return false;
  }

  boolean isCreatorAllowed(ProjectAwareResource existingResource, HederaControllerAction action) {
    return false;
  }

  boolean isManagerAllowed(ProjectAwareResource existingResource, HederaControllerAction action) {
    return false;
  }

  String getResourceSimpleName(ProjectAwareResource existingResource) {

    if (existingResource != null) {
      return existingResource.getClass().getSimpleName();
    } else {
      throw new SolidifyRuntimeException("error while checking user permissions on organizational unit: no given resource to check");
    }
  }

  boolean checkActionByRole(Role role, ProjectAwareResource existingResource, HederaControllerAction action) {

    /*
     * Check role specific permissions
     */
    return switch (role.getResId()) {
      case Role.MANAGER_ID -> this.isManagerAllowed(existingResource, action);
      case Role.CREATOR_ID -> this.isCreatorAllowed(existingResource, action);
      case Role.VISITOR_ID -> this.isVisitorAllowed(existingResource, action);
      default -> false;
    };
  }

  String getProjectIdToCheckPermission(ProjectAwareResource existingResource) {
    if (existingResource != null) {
      return existingResource.getProjectId();
    } else {
      throw new SolidifyRuntimeException("error while checking user permissions on project: no given resource to check");
    }
  }

  String getPersonId() {
    return this.trustedPersonRemoteResourceService.getLinkedPersonId(this.getAuthentication());
  }

  Authentication getAuthentication() {
    return SecurityContextHolder.getContext().getAuthentication();
  }

  HederaControllerAction getControllerAction(String actionString) {
    try {
      return HederaControllerAction.valueOf(actionString);
    } catch (final IllegalArgumentException ex) {
      final String errorMessage = "Unable to check for access permission: the action name '" + actionString + "' is wrong";
      logger.error(errorMessage);
      throw new SolidifyRuntimeException(errorMessage);
    }
  }

  boolean isRoleAllowed(Role role, ProjectAwareResource existingResource, HederaControllerAction action) {

    /*
     * All roles can perform these actions
     */
    if (action == HederaControllerAction.GET ||
            action == HederaControllerAction.GET_FILE ||
            action == HederaControllerAction.LIST_FILES ||
            action == HederaControllerAction.DOWNLOAD_FILE ||
            action == HederaControllerAction.HISTORY) {
      return true;
    }
    return this.checkActionByRole(role, existingResource, action);
  }

  protected boolean isAllowedToPerformActionOnResource(String personId, ProjectAwareResource existingResource,
          HederaControllerAction action) {

    try {

      final String projectId = this.getProjectIdToCheckPermission(existingResource);
      final String resourceSimpleName = this.getResourceSimpleName(existingResource);
      String existingResourceId = null;
      if (existingResource != null) {
        existingResourceId = existingResource.getResId();
      }

      if (StringTool.isNullOrEmpty(projectId)) {
        logger.error("Unable to check permissions on {}: projectId is not set", resourceSimpleName,
                new SolidifyRuntimeException("Unable to check permissions"));
        return false;
      }

      if (StringTool.isNullOrEmpty(personId)) {
        logger.warn("Unable to check permissions on {} ({}): personId is not set", resourceSimpleName, existingResourceId);
        return false;
      }

      final Optional<Role> roleOpt = this.trustedPersonRemoteResourceService.findProjectRole(personId, projectId);

      if (roleOpt.isEmpty()) {
        logger.warn("The person '{}' does not have any role on project '{}'", personId, projectId);
        return false;
      }

      if (this.isRoleAllowed(roleOpt.get(), existingResource, action)) {
        logger.trace("The person '{}' with role '{}' is allowed to perform the action '{}' on {} for project '{}'", personId,
                roleOpt.get().getName(), action, resourceSimpleName, projectId);
        return true;
      }

      logger.warn("The person '{}' is not allowed to perform the action '{}' on {} '{}' for project '{}'", personId, action, resourceSimpleName,
              existingResourceId, projectId);
      return false;

    } catch (final RuntimeException e) {
      logger.error("Error in checking permissions", e);
      return false;
    }
  }

  public boolean isAllowedToDeleteList(String[] listIds, String actionFromController) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    for (final String id : listIds) {
      if (!this.isAllowed(id, actionFromController)) {
        return false;
      }
    }
    return true;
  }

  abstract ProjectAwareResource getExistingResource(String resId);

}
