/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - ProjectDataPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.security;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.util.StringTool;

import ch.hedera.controller.HederaControllerAction;
import ch.hedera.model.ProjectAwareResource;
import ch.hedera.model.dto.AuthorizedProjectDto;
import ch.hedera.model.security.Role;
import ch.hedera.service.rest.propagate.PropagateProjectRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedPersonRemoteResourceService;

public abstract class ProjectDataPermissionService<T extends Resource & ProjectAwareResource> extends AbstractPermissionWithProjectService {
  private static final Logger log = LoggerFactory.getLogger(ProjectDataPermissionService.class);
  private final PropagateProjectRemoteResourceService propagateRemoteProjectService;

  protected ProjectDataPermissionService(TrustedPersonRemoteResourceService trustedPersonRemoteResourceService,
          PropagateProjectRemoteResourceService propagateRemoteProjectService) {
    super(trustedPersonRemoteResourceService);
    this.propagateRemoteProjectService = propagateRemoteProjectService;
  }

  public boolean isAllowed(T item, String actionFromController) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }

    HederaControllerAction action = this.getControllerAction(actionFromController);
    final String personId = this.getPersonId();
    return this.isAllowedToPerformActionOnResource(personId, item, action);
  }

  public boolean isAllowedToListWithProjectId(T item, String actionFromController) {
    final String projectId = item.getProjectId();
    return this.isAllowedToListWithProjectId(projectId, actionFromController);
  }

  public boolean isAllowedToListWithProjectId(String projectId, String actionFromController) {
    HederaControllerAction action = this.getControllerAction(actionFromController);
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    if (!StringTool.isNullOrEmpty(projectId) && action == HederaControllerAction.LIST) {
      Optional<AuthorizedProjectDto> authorizedProject = this.propagateRemoteProjectService.getAuthorizedProject(projectId);
      if (authorizedProject.isEmpty()) {
        log.error("Unable to check permissions: you are not associate to a project",
                new SolidifyRuntimeException("You are not associate to a project"));
        return false;
      }
      AuthorizedProjectDto authorizedProjectDTO = authorizedProject.get();
      return authorizedProjectDTO.getRole() != null && authorizedProjectDTO.getRole().getLevel() <= Role.VISITOR.getLevel();
    }
    return false;
  }

  @Override
  protected boolean isManagerAllowed(ProjectAwareResource existingResource, HederaControllerAction action) {
    return true;
  }

  @Override
  protected boolean isCreatorAllowed(ProjectAwareResource existingResource, HederaControllerAction action) {
    return true;
  }

  @Override
  protected boolean isVisitorAllowed(ProjectAwareResource existingResource, HederaControllerAction action) {
    return this.isActionAllowedForEveryMember(action);
  }

  protected boolean isActionAllowedForEveryMember(HederaControllerAction action) {
    return action == HederaControllerAction.GET ||
            action == HederaControllerAction.GET_FILE ||
            action == HederaControllerAction.LIST ||
            action == HederaControllerAction.LIST_FILES ||
            action == HederaControllerAction.DOWNLOAD_FILE ||
            action == HederaControllerAction.HISTORY;
  }

  protected boolean isActionAllowedForEveryOne(HederaControllerAction action) {
    return this.isActionAllowedForEveryMember(action);
  }
}
