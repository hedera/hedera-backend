/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - ResearchDataFilePermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.security;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.StringTool;

import ch.hedera.controller.HederaControllerAction;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Project;
import ch.hedera.service.rest.propagate.PropagateProjectRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedPersonRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedProjectRemoteResourceService;
import ch.hedera.service.rest.trusted.TrustedResearchDataFileRemoteResourceService;

@Service
public class ResearchDataFilePermissionService extends ProjectDataPermissionService<ResearchDataFile> {

  private static final Logger logger = LoggerFactory.getLogger(ResearchDataFilePermissionService.class);
  private final TrustedResearchDataFileRemoteResourceService researchDataFileService;
  private final TrustedProjectRemoteResourceService trustedProjectRemoteService;

  public ResearchDataFilePermissionService(TrustedPersonRemoteResourceService personRemoteResourceService,
          PropagateProjectRemoteResourceService propagateProjectRemoteResourceService,
          TrustedProjectRemoteResourceService trustedProjectRemoteService,
          TrustedResearchDataFileRemoteResourceService researchDataFileService) {
    super(personRemoteResourceService, propagateProjectRemoteResourceService);
    this.researchDataFileService = researchDataFileService;
    this.trustedProjectRemoteService = trustedProjectRemoteService;
  }

  public boolean isAllowedByProject(String projectId, String actionString) {

    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }

    String personId = this.getPersonId();
    HederaControllerAction action = this.getControllerAction(actionString);

    if (StringTool.isNullOrEmpty(projectId)) {
      logger.error("Unable to check permissions for {}: projectId is not set", personId);
      return false;
    }
    if (StringTool.isNullOrEmpty(personId)) {
      logger.warn("Unable to check permissions on {}: personId is not set", projectId);
      return false;
    }

    // Check if project is public for allowing all actions of every member, if not, the role of the person has to be checked
    Project project = this.trustedProjectRemoteService.getProject(projectId);
    if (this.isActionAllowedForEveryOne(action) && project.isAccessPublic()) {
      return true;
    }

    final Optional<Role> roleOpt = this.trustedPersonRemoteResourceService.findProjectRole(personId, projectId);

    if (roleOpt.isEmpty()) {
      logger.warn("The person '{}' does not have any role on project '{}'", personId, projectId);
      return false;
    }

    // If project is private, actions for every member are allowed only if the person has a role
    if (this.isActionAllowedForEveryMember(action) && !project.isAccessPublic().booleanValue()) {
      return true;
    }

    if (this.isActionAllowedForRoleCreatorOrSuperior(action) && roleOpt.get().getLevel() <= Role.CREATOR.getLevel()) {
      logger.trace("The person '{}' with role '{}' is allowed to perform the action '{}' for project '{}'", personId,
              roleOpt.get().getName(), action, projectId);
      return true;
    }
    return false;
  }

  public boolean isAllowedByProjectName(String projectShortName, String actionString) {
    Project project = this.trustedProjectRemoteService.getProjectByShortNameOrById(projectShortName);
    if (project == null) {
      logger.error("Enable to find project with short name {}", projectShortName);
      throw new NoSuchElementException(projectShortName);
    }
    if (Boolean.TRUE.equals(project.isAccessPublic())) {
      return true;
    } else {
      return this.isAllowedByProject(project.getProjectId(), actionString);
    }
  }

  private boolean isActionAllowedForRoleCreatorOrSuperior(HederaControllerAction action) {
    return action == HederaControllerAction.DOWNLOAD ||
            action == HederaControllerAction.UPLOAD_RESEARCH_DATA_FILE ||
            action == HederaControllerAction.DELETE_FOLDER ||
            action == HederaControllerAction.RELOAD;
  }

  @Override
  ResearchDataFile getExistingResource(String resId) {
    return this.researchDataFileService.findOne(resId);
  }

}
