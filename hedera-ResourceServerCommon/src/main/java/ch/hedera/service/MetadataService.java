/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - MetadataService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.time.OffsetDateTime;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.XMLTool;

import ch.hedera.HederaConstants;
import ch.hedera.HederaXmlNamespacePrefixMapper;
import ch.hedera.adapter.ItemTypeJsonSerializer;
import ch.hedera.adapter.MetadataListJsonSerializer;
import ch.hedera.adapter.ObjectTypeJsonSerializer;
import ch.hedera.model.settings.Project;
import ch.hedera.model.xml.hedera.v1.researchObject.BaseContext;
import ch.hedera.model.xml.hedera.v1.researchObject.ItemType;
import ch.hedera.model.xml.hedera.v1.researchObject.Metadata;
import ch.hedera.model.xml.hedera.v1.researchObject.MetadataList;
import ch.hedera.model.xml.hedera.v1.researchObject.ObjectType;
import ch.hedera.model.xml.hedera.v1.researchObject.Ontology;
import ch.hedera.model.xml.hedera.v1.researchObject.ResearchDataFile;
import ch.hedera.model.xml.hedera.v1.researchObject.ResearchItem;
import ch.hedera.model.xml.hedera.v1.researchObject.ResearchObject;
import ch.hedera.model.xml.hedera.v1.researchObject.ResearchObjectType;
import ch.hedera.model.xml.hedera.v1.researchObject.ResearchProject;

@Service
public class MetadataService extends HederaService {

  private final JAXBContext jaxbContext;

  private final String researchObjectSchema;

  public MetadataService(MessageService messageService) {
    super(messageService);
    // JAXB context
    try {
      final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
      final org.springframework.core.io.Resource[] xsd = resolver
              .getResources("classpath*:/" + SolidifyConstants.SCHEMA_HOME + "/" + HederaConstants.HEDERA_RESEARCH_OBJECT_SCHEMA_1);
      if (xsd.length > 0) {
        this.researchObjectSchema = FileTool.toString(xsd[0].getInputStream());
      } else {
        throw new IOException("No schema " + SolidifyConstants.SCHEMA_HOME + "/" + HederaConstants.HEDERA_RESEARCH_OBJECT_SCHEMA_1);
      }
      this.jaxbContext = JAXBContext.newInstance(ResearchObject.class, ResearchDataFile.class);
    } catch (final JAXBException | IOException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  public String getMetadataInXml(ch.hedera.model.ingest.ResearchDataFile researchDataFile) throws JAXBException {
    return this.toXml(this.getMetadataForIndexing(researchDataFile));
  }

  public String getMetadataInJson(ch.hedera.model.ingest.ResearchDataFile researchDataFile) throws JAXBException, JsonProcessingException {
    return this.toJson(this.getMetadataForIndexing(researchDataFile));
  }

  public ResearchDataFile getMetadataForIndexing(ch.hedera.model.ingest.ResearchDataFile researchDataFile) throws JAXBException {
    final ResearchDataFile xmlResearchDataFile = this.createResearchDataFile(researchDataFile);
    // Add metadata in xml
    this.addXmlMetadata(xmlResearchDataFile, this.toXml(xmlResearchDataFile));
    return xmlResearchDataFile;
  }

  public String getMetadataInXml(Project project, ch.hedera.model.settings.ResearchObjectType researchObjectType,
          String objectId, String subject, ObjectType objectType, OffsetDateTime objectDate, String sourceId, Map<String, Object> properties)
          throws JAXBException {
    return this
            .toXml(this.getMetadataForIndexing(project, researchObjectType, objectId, subject, objectType, objectDate, sourceId, properties));
  }

  public String getMetadataInJson(Project project, ch.hedera.model.settings.ResearchObjectType researchObjectType,
          String objectId, String subject, ObjectType objectType, OffsetDateTime objectDate, String sourceId, Map<String, Object> properties)
          throws JsonProcessingException, JAXBException {
    return this
            .toJson(this.getMetadataForIndexing(project, researchObjectType, objectId, subject, objectType, objectDate, sourceId, properties));
  }

  public ResearchObject getMetadataForIndexing(Project project, ch.hedera.model.settings.ResearchObjectType researchObjectType,
          String objectId, String subject, ObjectType objectType, OffsetDateTime objectDate, String sourceId, Map<String, Object> properties)
          throws JAXBException {
    final ResearchObject xmlResearchObject = this.createResearchObject(project, researchObjectType, objectId, subject, objectType, objectDate,
            sourceId, properties);
    // Add metadata in xml
    this.addXmlMetadata(xmlResearchObject, this.toXml(xmlResearchObject));
    return xmlResearchObject;
  }

  private void addXmlMetadata(ResearchItem researchItem, String xmlMetadata) {
    final Metadata metadata = new Metadata();
    metadata.setKey(HederaConstants.RESEARCH_OBJET_XML_INDEX_FIELD);
    metadata.setValue(xmlMetadata);
    researchItem.getMetadataList().getMetadata().add(metadata);
  }

  private ResearchDataFile createResearchDataFile(ch.hedera.model.ingest.ResearchDataFile researchDataFile) {
    final ResearchDataFile xmlResearchDataFile = new ResearchDataFile();
    xmlResearchDataFile.setItemType(ItemType.RESEARCH_DATA_FILE);
    if (researchDataFile.getUri() != null) {
      xmlResearchDataFile.setHederaId(researchDataFile.getHederaId());
      xmlResearchDataFile.setUri(researchDataFile.getUri().toString());
    } else {
      xmlResearchDataFile.setHederaId(researchDataFile.getResId());
      xmlResearchDataFile.setUri(researchDataFile.getSourcePath().toString());
    }
    xmlResearchDataFile.setObjectDate(researchDataFile.getLastUpdate().getWhen());
    xmlResearchDataFile.setFileName(researchDataFile.getFileName());
    xmlResearchDataFile.setRelativeLocation(researchDataFile.getRelativeLocation());
    xmlResearchDataFile.setAccessibleFrom(researchDataFile.getAccessibleFrom().toString());
    xmlResearchDataFile.setFileSize(researchDataFile.getFileSize());
    xmlResearchDataFile.setMimeType(researchDataFile.getMimeType());
    xmlResearchDataFile.setResearchProject(this.createResearchProject(researchDataFile.getProject()));
    xmlResearchDataFile.setResearchObjectType(this.createResearchObjectType(researchDataFile.getResearchObjectType()));
    xmlResearchDataFile.setMetadataList(new MetadataList());
    this.addMetadata(xmlResearchDataFile, researchDataFile.getMetadata());
    return xmlResearchDataFile;
  }

  public ResearchItem getResearchItem(String stringObject) throws JAXBException {
    return (ResearchItem) this.jaxbContext.createUnmarshaller().unmarshal(new StringReader(stringObject));
  }

  private String toXml(ResearchItem xmlResearchItem) throws JAXBException {
    StringWriter sw = new StringWriter();
    final Marshaller marshaller = this.jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    marshaller.setProperty(OAIConstants.NAMESPACE_MAPPER_PROPERTY, new HederaXmlNamespacePrefixMapper());
    marshaller.marshal(xmlResearchItem, sw);
    final String xml = sw.toString();
    XMLTool.validate(this.researchObjectSchema, xml);
    return xml;
  }

  private String toJson(ResearchItem xmlResearchItem) throws JsonProcessingException {
    final ObjectMapper mapper = new ObjectMapper();
    // Date
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.registerModule(new JavaTimeModule());
    // Metadata
    SimpleModule metadataModule = new SimpleModule();
    metadataModule.addSerializer(new MetadataListJsonSerializer());
    metadataModule.addSerializer(new ItemTypeJsonSerializer());
    metadataModule.addSerializer(new ObjectTypeJsonSerializer());
    mapper.registerModule(metadataModule);
    // Convert in JSON
    return mapper.writeValueAsString(xmlResearchItem);
  }

  private ResearchObject createResearchObject(Project project, ch.hedera.model.settings.ResearchObjectType researchObjectType, String hederaId,
          String objectUri, ObjectType objectType, OffsetDateTime objectDate, String sourceId, Map<String, Object> properties) {
    final ResearchObject xmlResearchObject = new ResearchObject();
    xmlResearchObject.setHederaId(hederaId);
    xmlResearchObject.setItemType(ItemType.RESEARCH_OBJECT);
    xmlResearchObject.setObjectType(objectType);
    xmlResearchObject.setUri(objectUri);
    xmlResearchObject.setSourceId(sourceId);
    xmlResearchObject.setObjectDate(objectDate);
    xmlResearchObject.setResearchProject(this.createResearchProject(project));
    xmlResearchObject.setResearchObjectType(this.createResearchObjectType(researchObjectType));
    xmlResearchObject.setMetadataList(new MetadataList());
    this.addMetadata(xmlResearchObject, properties);
    return xmlResearchObject;
  }

  private void addMetadata(ResearchItem xmlResearchItem, Map<String, Object> properties) {
    for (Entry<String, Object> property : properties.entrySet()) {
      final Metadata metadata = new Metadata();
      metadata.setKey(property.getKey());
      metadata.setValue(property.getValue().toString());
      xmlResearchItem.getMetadataList().getMetadata().add(metadata);
    }
  }

  private ResearchObjectType createResearchObjectType(ch.hedera.model.settings.ResearchObjectType researchObjectType) {
    final ResearchObjectType xmlRot = new ResearchObjectType();
    xmlRot.setName(researchObjectType.getName());
    xmlRot.setRdfType(researchObjectType.getRdfType());
    xmlRot.setDescription(researchObjectType.getDescription());
    xmlRot.setOntology(this.createOntology(researchObjectType.getOntology()));
    return xmlRot;
  }

  private Ontology createOntology(ch.hedera.model.humanities.Ontology ontology) {
    final Ontology xmlOnto = new Ontology();
    xmlOnto.setName(ontology.getName());
    xmlOnto.setVersion(ontology.getVersion());
    xmlOnto.setDescription(ontology.getDescription());
    xmlOnto.setFormat(ontology.getFormat().toString());
    xmlOnto.setBaseUri(ontology.getBaseUri().toString());
    xmlOnto.setBaseUriContext(
            this.createBaseUriContext("https".equals(ontology.getBaseUri().getScheme()), ontology.getBaseUriContext()));
    if (ontology.getUrl() != null) {
      xmlOnto.setUrl(ontology.getUrl().toString());
    }
    return xmlOnto;
  }

  private BaseContext createBaseUriContext(boolean https, String baseUriContext) {
    final BaseContext baseContext = new BaseContext();
    if (https) {
      baseContext.setProtocol("https");
    } else {
      baseContext.setProtocol("http");
    }
    baseContext.setValue(baseUriContext);
    return baseContext;
  }

  private ResearchProject createResearchProject(Project project) {
    final ResearchProject xmlProject = new ResearchProject();
    xmlProject.setName(project.getName());
    xmlProject.setDescription(project.getDescription());
    xmlProject.setShortName(project.getShortName());
    xmlProject.setOpeningDate(project.getOpeningDate());
    xmlProject.setClosingDate(project.getClosingDate());
    xmlProject.setId(project.getResId());
    xmlProject.setAccessPublic(project.isAccessPublic());
    if (project.getUrl() != null) {
      xmlProject.setUrl(project.getUrl().toString());
    }
    if (project.getKeywords() != null && !project.getKeywords().isEmpty()) {
      for (String keyword : project.getKeywords()) {
        xmlProject.getKeywords().add(keyword);
      }
    }
    return xmlProject;
  }
}
