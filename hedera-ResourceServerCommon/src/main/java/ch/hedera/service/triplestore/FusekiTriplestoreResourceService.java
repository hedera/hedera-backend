/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - FusekiTriplestoreResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.triplestore;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.jena.atlas.lib.StrUtils;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionRemote;
import org.apache.jena.rdfconnection.RDFConnectionRemoteBuilder;
import org.apache.jena.sparql.exec.http.QueryExecutionHTTP;
import org.apache.jena.sparql.exec.http.UpdateExecutionHTTP;
import org.apache.jena.update.UpdateExecution;
import org.apache.jena.update.UpdateRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.exception.SolidifyProcessingException;

import ch.hedera.config.HederaProperties;
import ch.hedera.model.triplestore.SparqlResultRow;
import ch.hedera.model.triplestore.SparqlResultValue;
import ch.hedera.util.SparqlQueryTool;
import ch.hedera.util.SparqlUpdateTool;

@Service
@Profile("triplestore-fuseki")
public class FusekiTriplestoreResourceService extends TriplestoreResourceService {
  private static final Logger log = LoggerFactory.getLogger(FusekiTriplestoreResourceService.class);

  private static final String TIMEOUT = "timeout";

  private final String triplestoreEndpoint;
  private final String queryTimeout;
  private final int maxDetailProperties;
  private final TriplestoreSettingsService triplestoreSettingsService;
  private final RestTemplate restClient;

  public FusekiTriplestoreResourceService(HederaProperties config, TriplestoreSettingsService triplestoreSettingsService) {
    super(config);
    this.triplestoreSettingsService = triplestoreSettingsService;
    this.triplestoreEndpoint = config.getTriplestore().getUrl();
    this.queryTimeout = String.valueOf(config.getTriplestore().getFusekiQueryTimeout());
    this.maxDetailProperties = config.getTriplestore().getMaxDetailProperties();

    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
    String login = config.getTriplestore().getLogin();
    String password = config.getTriplestore().getPassword();
    this.restClient = restTemplateBuilder.rootUri(config.getTriplestore().getUrl()).basicAuthentication(login, password).build();
  }

  @Override
  public ResponseEntity<String> addDatasetContent(String datasetName, Resource rdfFile) {

    // Create dataset if it doesn't exist yet
    this.triplestoreSettingsService.createDatasetIfNotExists(datasetName);

    // Upload RDF content
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);
    MultiValueMap<String, Object> values = new LinkedMultiValueMap<>();
    values.add("file", rdfFile);
    HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(values, headers);
    return this.restClient.postForEntity("/" + this.addDatasetPrefix(datasetName) + "/data", requestEntity, String.class);
  }

  @Override
  public ResponseEntity<String> replaceDatasetContent(String datasetName, Resource rdfFile) {
    this.triplestoreSettingsService.deleteDatasetIfExists(datasetName);
    return this.addDatasetContent(datasetName, rdfFile);
  }

  @Override
  public void deleteDatasetContent(String datasetName) {
    final UpdateRequest updateRequest = SparqlUpdateTool.updateToDelete();
    this.executeUpdate(datasetName, updateRequest);
  }

  @Override
  public Page<SparqlResultRow> executeQueryForList(String datasetName, Query sparqlQuery, Pageable pageable) {
    Query paginatedQuery = this.completeQueryPagination(sparqlQuery, pageable);
    List<SparqlResultRow> datasetEntriesList = new ArrayList<>();
    RDFConnectionRemoteBuilder builder = this.getRDFConnectionBuilder(datasetName);
    try (RDFConnection conn = builder.build()) {
      conn.queryResultSet(paginatedQuery, resultSet -> {
        while (resultSet.hasNext()) {
          QuerySolution qs = resultSet.next();
          SparqlResultRow datasetEntry = new SparqlResultRow();
          Iterator<String> iterator = qs.varNames();
          while (iterator.hasNext()) {
            String varName = iterator.next();
            SparqlResultValue rdfNode = this.toRdfNode(qs.get(varName).asNode());
            datasetEntry.addRdfNode(varName, rdfNode);
          }
          datasetEntriesList.add(datasetEntry);
        }
      });
      long totalItems = this.getListTotalElement(datasetName, sparqlQuery);
      return new PageImpl<>(datasetEntriesList, pageable, totalItems);
    }
  }

  @Override
  public Page<SparqlResultRow> executeQueryForDetail(String datasetName, String nodeValue, Query sparqlQuery) {
    sparqlQuery = this.setSubjectVariableValue(sparqlQuery, nodeValue);
    Pageable pageable = PageRequest.of(0, this.maxDetailProperties);
    return this.executeQueryForList(
            datasetName, sparqlQuery, pageable);
  }

  @Override
  public String executeQueryAndReturnXML(String datasetName, Query sparqlQuery) {
    StringBuilder sb = new StringBuilder();
    RDFConnectionRemoteBuilder builder = this.getRDFConnectionBuilder(datasetName);
    try (RDFConnection conn = builder.build()) {
      conn.queryResultSet(sparqlQuery, resultSet -> sb.append(ResultSetFormatter.asXMLString(resultSet)));
      return sb.toString();
    }
  }

  @Override
  public String executeQueryAndReturnCSV(String datasetName, Query sparqlQuery) {
    final StringBuilder sb = new StringBuilder();
    final RDFConnectionRemoteBuilder builder = this.getRDFConnectionBuilder(datasetName);
    try (RDFConnection conn = builder.build()) {
      conn.queryResultSet(sparqlQuery, resultSet -> {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        ResultSetFormatter.outputAsCSV(out, resultSet);
        sb.append(StrUtils.fromUTF8bytes(out.toByteArray()));
      });
      return sb.toString();
    } catch (RuntimeException e) {
      throw new SolidifyProcessingException("SPARQL query failed", e);
    }
  }

  @Override
  public String executeQueryAndReturnJSON(String datasetName, Query sparqlQuery) {
    StringBuilder sb = new StringBuilder();
    RDFConnectionRemoteBuilder builder = this.getRDFConnectionBuilder(datasetName);
    try (RDFConnection conn = builder.build()) {
      conn.queryResultSet(sparqlQuery, resultSet -> {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ResultSetFormatter.outputAsJSON(out, resultSet);
        sb.append(StrUtils.fromUTF8bytes(out.toByteArray()));
      });

      return sb.toString();
    }
  }

  @Override
  public void executeUpdate(String datasetName, UpdateRequest updateRequest) {
    UpdateExecution uExec = UpdateExecutionHTTP.create()
            .endpoint(this.triplestoreEndpoint + "/" + this.addDatasetPrefix(datasetName))
            .update(updateRequest)
            .param(TIMEOUT, this.queryTimeout)
            .build();
    uExec.execute();
  }

  @Override
  public ResultSet executeQuery(String datasetName, Query sparqlQuery) {
    Objects.requireNonNull(datasetName, "datasetName cannot be null");
    Objects.requireNonNull(sparqlQuery, "sparqlQuery cannot be null");
    Objects.requireNonNull(this.triplestoreEndpoint, "triplestoreEndpoint cannot be null");
    final Query query = QueryFactory.create(sparqlQuery);
    final String endpointUrl = this.triplestoreEndpoint + "/" + this.addDatasetPrefix(datasetName);
    try (QueryExecution qExec = QueryExecutionHTTP.create()
            .endpoint(endpointUrl)
            .query(query)
            .param(TIMEOUT, this.queryTimeout)
            .build()) {
      return qExec.execSelect();
    } catch (RuntimeException e) {
      throw new SolidifyProcessingException("SPARQL query on " + endpointUrl + " failed", e);
    }
  }

  @Override
  public Model executeQueryToGetModel(String datasetName, Query sparqlQuery) {
    Query query = QueryFactory.create(sparqlQuery);
    try (QueryExecution qExec = QueryExecutionHTTP.create()
            .endpoint(this.triplestoreEndpoint + "/" + this.addDatasetPrefix(datasetName)) // add the prefix before running the query
            .query(query)
            .param(TIMEOUT, this.queryTimeout)
            .build()) {
      return qExec.execConstruct();
    } catch (RuntimeException e) {
      throw new SolidifyProcessingException("SPARQL query failed", e);
    }
  }

  private RDFConnectionRemoteBuilder getRDFConnectionBuilder(String datasetName) {
    if (log.isDebugEnabled()) {
      log.debug("destination {}", this.triplestoreEndpoint);
      log.debug("queryEndpoint {}", this.addDatasetPrefix(datasetName));
    }
    return RDFConnectionRemote.newBuilder()
            .destination(this.triplestoreEndpoint)
            .queryEndpoint(this.addDatasetPrefix(datasetName))
            .updateEndpoint(null)
            .gspEndpoint(null);
  }

  private long getListTotalElement(String datasetName, Query sparqlQuery) {
    Query queryTotalCount = SparqlQueryTool.queryToCount(sparqlQuery, "?count");
    ResultSet resultSet = this.executeQuery(datasetName, queryTotalCount);
    QuerySolution qs = resultSet.next();
    return qs.getLiteral("count").getLong();
  }

}
