/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - TriplestoreSettingsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.triplestore;

import ch.unige.solidify.service.NoSqlResourceService;
import ch.unige.solidify.util.StringTool;

import ch.hedera.config.HederaProperties;
import ch.hedera.model.triplestore.DatasetSettings;

public abstract class TriplestoreSettingsService extends NoSqlResourceService<DatasetSettings> {

  protected String datasetPrefix;

  protected TriplestoreSettingsService(HederaProperties config) {
    this.needPrefixToBeSet(config.getTriplestore().getDatasetPrefix());
    this.datasetPrefix = config.getTriplestore().getDatasetPrefix();
  }

  public abstract DatasetSettings createDatasetIfNotExists(String datasetId);

  public abstract boolean deleteDatasetIfExists(String datasetId);

  protected void needPrefixToBeSet(String propertyName) {
    if (StringTool.isNullOrEmpty(propertyName)) {
      throw new IllegalStateException("Property 'hedera.triplestore.datasetPrefix' must be set in config");
    }
  }
}
