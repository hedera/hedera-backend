/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - InMemoryTriplestoreSettingsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.triplestore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.hedera.config.HederaProperties;
import ch.hedera.model.triplestore.DatasetSettings;

@Service
@Profile("triplestore-inmemory")
public class InMemoryTriplestoreSettingsService extends TriplestoreSettingsService {

  private Map<String, DatasetSettings> datasetSettingsMap = new HashMap<>();

  protected InMemoryTriplestoreSettingsService(HederaProperties config) {
    super(config);
  }

  @Override
  public DatasetSettings findOne(String id) {
    return this.datasetSettingsMap.get(id);
  }

  @Override
  public List<DatasetSettings> findAll(DatasetSettings search) {
    return List.copyOf(this.datasetSettingsMap.values());
  }

  @Override
  public DatasetSettings save(DatasetSettings datasetSettings) {
    return this.datasetSettingsMap.put(datasetSettings.getResId(), datasetSettings);
  }

  @Override
  public DatasetSettings update(DatasetSettings datasetSettings) {
    return this.datasetSettingsMap.put(datasetSettings.getResId(), datasetSettings);
  }

  @Override
  public boolean delete(DatasetSettings datasetSettings) {
    return this.datasetSettingsMap.remove(datasetSettings.getResId()) != null;
  }

  @Override
  public DatasetSettings createDatasetIfNotExists(String datasetId) {
    if (!this.datasetSettingsMap.containsKey(datasetId)) {
      DatasetSettings datasetSettings = new DatasetSettings();
      datasetSettings.setResId(datasetId);
      return this.save(datasetSettings);
    } else {
      return this.datasetSettingsMap.get(datasetId);
    }
  }

  @Override
  public boolean deleteDatasetIfExists(String datasetId) {
    if (this.findOne(datasetId) != null) {
      this.datasetSettingsMap.remove(datasetId);
      return true;
    }
    return false;
  }
}
