/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - TriplestoreResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.triplestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.engine.binding.Binding;
import org.apache.jena.update.UpdateRequest;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;

import ch.hedera.config.HederaProperties;
import ch.hedera.model.triplestore.SparqlResultRow;
import ch.hedera.model.triplestore.SparqlResultValue;
import ch.hedera.util.SparqlBuilderTool;
import ch.hedera.util.SparqlTool;

public abstract class TriplestoreResourceService {

  protected String datasetPrefix;

  protected TriplestoreResourceService(HederaProperties config) {
    this.needPrefixToBeSet(config.getTriplestore().getDatasetPrefix());
    this.datasetPrefix = config.getTriplestore().getDatasetPrefix();
  }

  public abstract ResponseEntity<String> addDatasetContent(String id, Resource rdfFile);

  public abstract ResponseEntity<String> replaceDatasetContent(String id, Resource rdfFile);

  public abstract void deleteDatasetContent(String id);

  public abstract void executeUpdate(String datasetName, UpdateRequest updateRequest);

  public abstract Page<SparqlResultRow> executeQueryForList(String datasetId, Query query, Pageable pageable);

  public abstract Page<SparqlResultRow> executeQueryForDetail(String datasetId, String nodeValue, Query query);

  public abstract String executeQueryAndReturnXML(String datasetId, Query query);

  public abstract String executeQueryAndReturnCSV(String datasetId, Query query);

  public abstract String executeQueryAndReturnJSON(String datasetId, Query query);

  public abstract ResultSet executeQuery(String datasetId, Query query);

  public abstract Model executeQueryToGetModel(String datasetId, Query query);

  protected String addDatasetPrefix(String dataset) {
    return this.datasetPrefix + dataset;
  }

  protected Query completeQueryPagination(Query sparqlQuery, Pageable pageable) {
    final Query paginatedSparqlQuery = sparqlQuery.cloneQuery();
    if (pageable.getSort().isSorted()) {
      Sort.Order order = pageable.getSort().iterator().next();
      String sortProperty = order.getProperty();
      int sortDirection = order.getDirection().isAscending() ? Query.ORDER_ASCENDING : Query.ORDER_DESCENDING;
      paginatedSparqlQuery.addOrderBy(sortProperty, sortDirection);
    }

    if (pageable.isPaged()) {
      paginatedSparqlQuery.setLimit(pageable.getPageSize());
      paginatedSparqlQuery.setOffset(pageable.getOffset());
    }

    return paginatedSparqlQuery;
  }

  protected SparqlResultValue toRdfNode(Node node) {
    if (node.isURI()) {
      return new SparqlResultValue(SparqlResultValue.NodeType.URI, node.toString());
    } else if (node.isLiteral()) {
      return new SparqlResultValue(SparqlResultValue.NodeType.LITERAL, node.getLiteral().toString(false));
    } else if (node.isBlank()) {
      return new SparqlResultValue(SparqlResultValue.NodeType.BLANK, node.toString());
    }
    throw new SolidifyRuntimeException("Node type not supported");
  }

  /**
   * Add a VALUES clause to the SPARQL query such as: VALUES ?subject { <http://lod.hedera.ch/xxxx> } The SPARQL query to complete must
   * contain a ?subject variable to be able to do the binding.
   *
   * @param sparqlQuery The SPARQL query to complete
   * @param researchObjectId The ?subject value to bind (e.g. "<http://lod.hedera.ch/xxxx>")
   * @return
   */
  protected Query setSubjectVariableValue(Query sparqlQuery, String researchObjectId) {
    if (SparqlTool.queryContainsVariable(sparqlQuery, SparqlBuilderTool.SUBJECT_FULL_NAME_VARIABLE)) {
      final String variableName = SparqlBuilderTool.getVariableName(SparqlBuilderTool.SUBJECT_FULL_NAME_VARIABLE);
      final Query query = QueryFactory.create(sparqlQuery);
      List<Var> vars = query.getProjectVars();
      Optional<Var> subjectVarOpt = vars.stream().filter(v -> v.getVarName().equals(variableName))
              .findFirst();

      List<Var> valuesVars = new ArrayList<>();
      List<Binding> valuesBindings = new ArrayList<>();
      if (subjectVarOpt.isPresent()) {
        valuesVars.add(subjectVarOpt.get());
        Binding binding = Binding.builder().add(subjectVarOpt.get(), NodeFactory.createURI(researchObjectId)).build();
        valuesBindings.add(binding);
        query.setValuesDataBlock(valuesVars, valuesBindings);
      }
      return query;
    } else {
      throw new SolidifyRuntimeException("SPARQL query for detail must contain a ?subject variable");
    }
  }

  protected void needPrefixToBeSet(String propertyName) {
    if (StringTool.isNullOrEmpty(propertyName)) {
      throw new IllegalStateException("Property 'hedera.triplestore.datasetPrefix' must be set in config");
    }
  }
}
