/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - InMemoryTriplestoreResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.triplestore;

import org.apache.jena.query.Query;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.update.UpdateRequest;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import ch.hedera.config.HederaProperties;
import ch.hedera.model.triplestore.SparqlResultRow;

@Service
@Profile("triplestore-inmemory")
public class InMemoryTriplestoreResourceService extends TriplestoreResourceService {

  public InMemoryTriplestoreResourceService(HederaProperties config) {
    super(config);
  }

  @Override
  public ResponseEntity<String> addDatasetContent(String id, Resource rdfFile) {
    return null;
  }

  @Override
  public ResponseEntity<String> replaceDatasetContent(String id, Resource rdfFile) {
    return null;
  }

  @Override
  public void deleteDatasetContent(String id) {
    // Do nothing
  }

  @Override
  public void executeUpdate(String datasetName, UpdateRequest updateRequest) {
    // Do nothing
  }

  @Override
  public Page<SparqlResultRow> executeQueryForList(String datasetId, Query query, Pageable pageable) {
    return null;
  }

  @Override
  public Page<SparqlResultRow> executeQueryForDetail(String datasetId, String nodeValue, Query query) {
    return null;
  }

  @Override
  public String executeQueryAndReturnXML(String datasetId, Query query) {
    return null;
  }

  @Override
  public String executeQueryAndReturnCSV(String datasetId, Query query) {
    return null;
  }

  @Override
  public String executeQueryAndReturnJSON(String datasetId, Query query) {
    return null;
  }

  @Override
  public ResultSet executeQuery(String datasetId, Query query) {
    return null;
  }

  @Override
  public Model executeQueryToGetModel(String datasetId, Query query) {
    return null;
  }

}
