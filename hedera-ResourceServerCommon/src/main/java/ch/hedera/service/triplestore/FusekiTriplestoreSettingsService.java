/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - FusekiTriplestoreSettingsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.triplestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.util.StringTool;

import ch.hedera.config.HederaProperties;
import ch.hedera.model.triplestore.DatasetSettings;

@Service
@Profile("triplestore-fuseki")
public class FusekiTriplestoreSettingsService extends TriplestoreSettingsService {
  private static final Logger logger = LoggerFactory.getLogger(FusekiTriplestoreSettingsService.class);
  private static final String DB_TYPE = "tdb2";
  private final RestTemplate restClient;

  public FusekiTriplestoreSettingsService(HederaProperties config) {
    super(config);
    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
    String login = config.getTriplestore().getLogin();
    String password = config.getTriplestore().getPassword();
    this.restClient = restTemplateBuilder.rootUri(config.getTriplestore().getUrl()).basicAuthentication(login, password).build();
  }

  @Override
  public DatasetSettings findOne(String id) {
    final String datasetName = this.addDatasetPrefix(id);
    Map<String, Object> map = this.restClient.getForObject("/$/stats/" + datasetName, Map.class);
    if (map == null) {
      return null;
    }
    Map<String, Object> datasetSettingsMap = (Map<String, Object>) ((Map<String, Object>) map.get("datasets")).get("/" + datasetName);
    DatasetSettings datasetSettings = new DatasetSettings();
    datasetSettings.setResId(id);
    datasetSettings.setSettings(datasetSettingsMap);
    return datasetSettings;
  }

  @Override
  public List<DatasetSettings> findAll(DatasetSettings search) {
    List<DatasetSettings> datasetsList = new ArrayList<>();
    Map<String, Object> serverResponse = this.restClient.getForObject("/$/server", Map.class);
    if (serverResponse == null) {
      return datasetsList;
    }
    for (Map<String, Object> datasetSettingsMap : (List<Map<String, Object>>) serverResponse.get("datasets")) {
      String internalDatasetName = datasetSettingsMap.get("ds.name").toString();
      if (internalDatasetName.startsWith("/" + this.datasetPrefix)) {
        DatasetSettings datasetSettings = new DatasetSettings();
        datasetSettings.setResId(StringTool.removePrefix(internalDatasetName, "/" + this.datasetPrefix));
        datasetSettings.setSettings(datasetSettingsMap);
        datasetsList.add(datasetSettings);
      }
    }
    return datasetsList;
  }

  @Override
  public DatasetSettings save(DatasetSettings datasetSettings) {
    final String datasetName = this.addDatasetPrefix(datasetSettings.getResId());

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
    map.add("dbName", datasetName);
    map.add("dbType", DB_TYPE);

    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

    logger.info("Add content for dataset : {}", datasetName);
    this.restClient.postForLocation("/$/datasets", request);

    return this.findOne(datasetSettings.getResId());
  }

  @Override
  public DatasetSettings update(DatasetSettings datasetSettings) {
    throw new NotImplementedException();
  }

  @Override
  public boolean delete(DatasetSettings datasetSettings) {
    final String datasetName = this.addDatasetPrefix(datasetSettings.getResId());
    logger.info("Delete dataset : {}", datasetName);
    this.restClient.delete("/$/datasets/" + datasetName);
    return true;
  }

  @Override
  public DatasetSettings createDatasetIfNotExists(String datasetId) {
    try {
      return this.findOne(datasetId);
    } catch (HttpClientErrorException.NotFound e) {
      DatasetSettings datasetSettings = new DatasetSettings();
      datasetSettings.setResId(datasetId);
      return this.save(datasetSettings);
    }
  }

  @Override
  public boolean deleteDatasetIfExists(String datasetId) {
    try {
      this.findOne(datasetId);
      DatasetSettings datasetSettings = new DatasetSettings();
      datasetSettings.setResId(datasetId);
      return this.delete(datasetSettings);
    } catch (HttpClientErrorException.NotFound e) {
      return false;
    }
  }

  /**
   * Add prefix to the project shortname to create/target triple store
   *
   * @param dataset
   * @return
   */
  private String addDatasetPrefix(String dataset) {
    return this.datasetPrefix + dataset;
  }
}
