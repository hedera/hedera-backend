/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - HederaUserRestCreatorService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.client.service.UserCreatorService;
import ch.unige.solidify.service.TrustedRestClientService;

import ch.hedera.config.HederaProperties;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ResourceName;

@Service
public class HederaUserRestCreatorService implements UserCreatorService {

  private final String adminUrl;
  private final TrustedRestClientService restClientService;

  public HederaUserRestCreatorService(HederaProperties properties, TrustedRestClientService restClientService) {
    this.restClientService = restClientService;
    this.adminUrl = properties.getModule().getAdmin().getUrl();
  }

  @Override
  @Cacheable(HederaCacheNames.NEW_USER)
  public String createNewUser(String externalUid) {
    final String url = this.adminUrl + "/" + ResourceName.USER + "/" + HederaActionName.NEW_USER + "/" + externalUid;
    this.restClientService.postResourceAction(url);
    return externalUid;
  }
}
