/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - TrustedRmlRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rest.trusted;

import java.nio.file.Path;

import org.springframework.stereotype.Service;

import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.service.TrustedRestClientService;

import ch.hedera.config.HederaProperties;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ResourceName;
import ch.hedera.service.rest.abstractservice.RmlRemoteResourceService;

@Service
public class TrustedRmlRemoteResourceService extends RmlRemoteResourceService {

  public TrustedRmlRemoteResourceService(HederaProperties config, TrustedRestClientService restClientService) {
    super(config, restClientService);

  }

  public Path downloadRmlFile(String rmlFileId) {
    final String url = this.adminUrl + "/" + ResourceName.RML + "/" + rmlFileId + "/" + HederaActionName.DOWNLOAD_RDF;
    final Path destinationPath = Path.of(this.rmlLocation, rmlFileId);
    this.restClientService.downloadContent(url, destinationPath, TokenUsage.WITH_TOKEN);
    return destinationPath;
  }
}
