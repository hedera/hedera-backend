/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - ResearchDataFileRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rest.abstractservice;

import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.hedera.config.HederaProperties;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.rest.ResourceName;

public abstract class ResearchDataFileRemoteResourceService extends RemoteResourceService<ResearchDataFile> {
  private final String ingestModuleUrl;

  protected ResearchDataFileRemoteResourceService(HederaProperties properties, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.ingestModuleUrl = properties.getModule().getIngest().getUrl();
  }

  @Override
  protected Class<ResearchDataFile> getResourceClass() {
    return ResearchDataFile.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.ingestModuleUrl + "/" + ResourceName.RESEARCH_DATA_FILE;
  }

}
