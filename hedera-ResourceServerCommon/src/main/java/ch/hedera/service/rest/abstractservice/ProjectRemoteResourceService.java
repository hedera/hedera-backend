/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - ProjectRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rest.abstractservice;

import java.nio.file.Path;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.StringTool;

import ch.hedera.config.HederaProperties;
import ch.hedera.model.ResourceIdentifierType;
import ch.hedera.model.display.PersonRoleListDTO;
import ch.hedera.model.dto.AuthorizedProjectDto;
import ch.hedera.model.humanities.Rml;
import ch.hedera.model.settings.FundingAgency;
import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ResourceName;
import ch.hedera.service.CacheService;
import ch.hedera.service.HederaCacheNames;

public abstract class ProjectRemoteResourceService extends RemoteResourceService<Project> {

  private static final Logger log = LoggerFactory.getLogger(ProjectRemoteResourceService.class);

  private final CacheService cacheService;

  private final String adminUrl;

  protected ProjectRemoteResourceService(HederaProperties config, SolidifyRestClientService restClientService,
          CacheService cacheService) {
    super(restClientService);
    this.cacheService = cacheService;
    this.adminUrl = config.getModule().getAdmin().getUrl();
  }

  public RestCollection<Project> getProjectList(String queryString, Pageable pageable) {
    final StringBuilder url = new StringBuilder(this.adminUrl);
    url.append("/");
    url.append(ResourceName.PROJECT);
    if (!StringTool.isNullOrEmpty(queryString)) {
      url.append("?");
      url.append(queryString);
    }
    String jsonString = this.restClientService.getResource(url.toString(), pageable);
    return new RestCollection<>(jsonString, Project.class);
  }

  public RestCollection<Project> getAuthorizedProjectList(String queryString, Pageable pageable) {
    final StringBuilder url = new StringBuilder(this.adminUrl);
    url.append("/");
    url.append(ResourceName.AUTHORIZED_PROJECT);
    url.append("?withAllPublic=true");
    if (!queryString.contains("openingDate") && !queryString.contains("closingDate")) {
      url.append("&openOnly=false");
    }
    if (!StringTool.isNullOrEmpty(queryString)) {
      url.append("&");
      url.append(queryString);
    }
    String jsonString = this.restClientService.getResource(url.toString(), pageable);
    return new RestCollection<>(jsonString, Project.class);
  }

  public Project getProject(String resId) {
    this.restClientService.checkResId(resId);
    String url = this.getResourceUrl() + "/" + resId;
    return this.restClientService.getResource(url, Project.class);
  }

  public Project getProjectByShortNameOrById(String id) {
    Project project = null;
    try {
      project = this.getProjectByShortNameWithCache(id);
    } catch (SolidifyResourceNotFoundException e) {
      project = this.findOneWithCache(id);
    }
    return project;
  }

  @Cacheable(HederaCacheNames.PROJECT_BY_SHORT_NAME)
  public Project getProjectByShortNameWithCache(String shortName) {
    String url = this.getResourceUrl() + "/" + shortName + "?identifierType=" + ResourceIdentifierType.SHORT_NANE.name();
    return this.restClientService.getResource(url, Project.class);
  }

  public Project updateProject(String projectId, Project project) {
    String url = this.getResourceUrl() + "/" + projectId;
    return this.restClientService.patchResource(url, project, Project.class);
  }

  public Project updateProject(String projectId, Map<String, Object> propertiesToUpdate) {
    String url = this.getResourceUrl() + "/" + projectId;
    return this.restClientService.patchResource(url, propertiesToUpdate, Project.class);
  }

  public void downloadProjectLogo(String resId, Path path) {
    String url = this.getResourceUrl() + "/" + resId + "/" + HederaActionName.DOWNLOAD_LOGO;
    this.restClientService.downloadContent(url, path, TokenUsage.WITH_TOKEN);
  }

  public Optional<AuthorizedProjectDto> getAuthorizedProject(String projectId) {
    String url = this.adminUrl + "/" + ResourceName.AUTHORIZED_PROJECT + "/" + projectId;
    return Optional.of(this.restClientService.getResource(url, AuthorizedProjectDto.class));
  }

  public List<Project> getAuthorizedProjects(Principal principal) {

    List<Project> cachedAuthorizedProjects = null;
    String cacheKey = null;

    try {
      cacheKey = principal.getName();
      cachedAuthorizedProjects = this.cacheService.getCachedAuthorizedProjects(cacheKey);
    } catch (final SolidifyRuntimeException e) {
      log.debug("Error getting authorized project", e);
    }

    if (cachedAuthorizedProjects != null && !cachedAuthorizedProjects.isEmpty()) {

      return cachedAuthorizedProjects;

    } else {

      final String url = this.adminUrl + "/" + ResourceName.AUTHORIZED_PROJECT;

      try {
        final List<Project> units = new ArrayList<>();
        Pageable page = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
        RestCollection<Project> projects;
        // List all projects
        do {
          projects = this.restClientService.getResourceList(url, Project.class, page);
          page = page.next();
          for (final Project unit : projects.getData()) {
            units.add(unit);
          }
        } while (projects.getPage().hasNext());

        this.cacheService.storeAuthorizedProjects(cacheKey, units);

        return units;

      } catch (final SolidifyRuntimeException e) {
        log.error("Error getting resource class", e);
      }
      return Collections.emptyList();
    }
  }

  public List<Institution> getInstitutions(String projectId) {
    return this.restClientService.getAllResources(
            this.adminUrl
                    + "/" + ResourceName.PROJECT
                    + "/" + projectId
                    + "/" + ResourceName.INSTITUTION,
            Institution.class);
  }

  public List<FundingAgency> getFundingAgencies(String projectId) {
    return this.restClientService.getAllResources(
            this.adminUrl
                    + "/" + ResourceName.PROJECT
                    + "/" + projectId
                    + "/" + ResourceName.FUNDING_AGENCY,
            FundingAgency.class);
  }

  public List<Rml> getRmlFiles(String projectId) {
    return this.restClientService.getAllResources(
            this.adminUrl
                    + "/" + ResourceName.PROJECT
                    + "/" + projectId
                    + "/" + ResourceName.RML,
            Rml.class);
  }

  public Rml getRmlFile(String projectId, String rmlId) {
    return this.restClientService.getResource(
            this.adminUrl
                    + "/" + ResourceName.PROJECT
                    + "/" + projectId
                    + "/" + ResourceName.RML
                    + "/" + rmlId,
            Rml.class);
  }

  public List<ResearchObjectType> getResearchObjectTypes(String projectId) {
    return this.restClientService.getAllResources(
            this.adminUrl
                    + "/" + ResourceName.PROJECT
                    + "/" + projectId
                    + "/" + ResourceName.RESEARCH_OBJECT_TYPE,
            ResearchObjectType.class);
  }

  public List<ResearchObjectType> getAllResearchObjectTypes(String projectId) {
    List<ResearchObjectType> list = this.getResearchObjectTypes(projectId);
    Project project = this.getProject(projectId);
    if (project.getResearchDataFileResearchObjectType() != null) {
      list.add(project.getResearchDataFileResearchObjectType());
    }
    if ((project.getIiifManifestResearchObjectType()) != null) {
      list.add(project.getIiifManifestResearchObjectType());
    }
    return list;
  }

  public ResearchObjectType getResearchObjectType(String projectId, String researchObjectTypeId) {
    return this.restClientService.getResource(
            this.adminUrl
                    + "/" + ResourceName.PROJECT
                    + "/" + projectId
                    + "/" + ResourceName.RESEARCH_OBJECT_TYPE
                    + "/" + researchObjectTypeId,
            ResearchObjectType.class);
  }

  public List<PersonRoleListDTO> getMembers(String projectId) {
    return this.restClientService.getAllResources(this.adminUrl + "/" + ResourceName.PROJECT + "/" + projectId + "/" + ResourceName.PERSON,
            PersonRoleListDTO.class);
  }

  @Override
  protected Class<Project> getResourceClass() {
    return Project.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminUrl + "/" + ResourceName.PROJECT;
  }

}
