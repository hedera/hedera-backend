/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - OntologyRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rest.abstractservice;

import java.nio.file.Path;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.StringTool;

import ch.hedera.config.HederaProperties;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ResourceName;

public abstract class OntologyRemoteResourceService extends RemoteResourceService<Ontology> {

  private final String adminUrl;
  private final String ontologyLocation;

  protected OntologyRemoteResourceService(HederaProperties config, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.adminUrl = config.getModule().getAdmin().getUrl();
    this.ontologyLocation = config.getOntologyLocation();
  }

  @Override
  protected Class<Ontology> getResourceClass() {
    return Ontology.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminUrl + "/" + ResourceName.ONTOLOGY;
  }

  public RestCollection<Ontology> getList(String queryString, Pageable pageable) {
    final StringBuilder url = new StringBuilder(this.getResourceUrl());
    if (!StringTool.isNullOrEmpty(queryString)) {
      url.append("?");
      url.append(queryString);
    }
    String jsonString = this.restClientService.getResource(url.toString(), pageable);
    return new RestCollection<>(jsonString, Ontology.class);
  }

  public void downloadOntology(String resId) {
    final String url = this.getResourceUrl() + "/" + resId + "/" + HederaActionName.DOWNLOAD_ONTOLOGY;
    final Path destinationPath = Path.of(this.ontologyLocation, resId);
    this.restClientService.downloadContent(url, destinationPath, TokenUsage.WITH_TOKEN);
  }
}
