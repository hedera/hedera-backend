/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - IIIFRefreshRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rest.abstractservice;

import ch.unige.solidify.service.SolidifyRestClientService;

import ch.hedera.config.HederaProperties;
import ch.hedera.rest.HederaActionName;

public abstract class IIIFRefreshRemoteResourceService {

  protected final SolidifyRestClientService restClientService;

  private final String accessUrl;

  protected IIIFRefreshRemoteResourceService(HederaProperties properties, SolidifyRestClientService restClientService) {
    this.restClientService = restClientService;
    this.accessUrl = properties.getModule().getAccess().getUrl();
  }

  public void refreshAll() {
    final String url = this.accessUrl
            + "/" + HederaActionName.IIIF_REFRESH;
    this.restClientService.postResource(url, Void.class);
  }

  public void refresh(String projectShortName) {
    final String url = this.accessUrl
            + "/" + HederaActionName.IIIF_REFRESH
            + "/" + projectShortName;
    this.restClientService.postResource(url, Void.class);
  }
}
