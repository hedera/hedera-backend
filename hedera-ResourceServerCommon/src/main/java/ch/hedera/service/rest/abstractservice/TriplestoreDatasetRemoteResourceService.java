/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - TriplestoreDatasetRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rest.abstractservice;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.hedera.config.HederaProperties;
import ch.hedera.model.settings.Project;
import ch.hedera.model.triplestore.TriplestoreDataset;
import ch.hedera.rest.ResourceName;

public abstract class TriplestoreDatasetRemoteResourceService extends RemoteResourceService<TriplestoreDataset> {
  private final String adminUrl;

  protected TriplestoreDatasetRemoteResourceService(HederaProperties config, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.adminUrl = config.getModule().getAdmin().getUrl();
  }

  public List<TriplestoreDataset> findByProject(Project project) {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    final String url = this.getResourceUrl() + "?project.resId=" + project.getProjectId();
    return this.restClientService.getResourceList(url, TriplestoreDataset.class, pageable).getData();
  }

  @Override
  protected Class<TriplestoreDataset> getResourceClass() {
    return TriplestoreDataset.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminUrl + "/" + ResourceName.TRIPLESTORE_DATASET;
  }
}
