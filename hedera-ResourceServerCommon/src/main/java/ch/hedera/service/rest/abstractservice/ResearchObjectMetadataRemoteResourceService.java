/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - ResearchObjectMetadataRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rest.abstractservice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchConditionType;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.service.rest.abstractservice.IndexMetadataRemoteResourceService;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.config.HederaProperties;
import ch.hedera.model.index.ResearchObjectMetadata;

public abstract class ResearchObjectMetadataRemoteResourceService
        extends IndexMetadataRemoteResourceService<String, ResearchObjectMetadata> {

  private static final Logger log = LoggerFactory.getLogger(ResearchObjectMetadataRemoteResourceService.class);

  private final HederaProperties config;

  protected ResearchObjectMetadataRemoteResourceService(HederaProperties config, SolidifyRestClientService restClientService) {
    super(ResearchObjectMetadata.class, restClientService);
    this.config = config;
  }

  public ResearchObjectMetadata getMetadata(String indexName, String aipId) {
    return this.getIndexMetadata(indexName, aipId);
  }

  public RestCollection<ResearchObjectMetadata> getMetadataList(String indexName, Map<String, Object> parameters,
          Pageable pageable) {
    String url = this.buildUrl(indexName, parameters);
    return this.getIndexMetadataListWithUrl(url, pageable);
  }

  public RestCollection<ResearchObjectMetadata> getMetadataList(String indexName, Pageable pageable) {
    return this.getMetadataList(indexName, new HashMap<>(), pageable);
  }

  public RestCollection<ResearchObjectMetadata> getMetadataList(String indexName, String query, String field, String from,
          String to, String set, String projectId, Pageable pageable) {

    HashMap<String, Object> parameters = this.createParameterMap(query, field, from, to, set);
    parameters.put(HederaConstants.SEARCH_PROJECT, projectId);

    return this.getMetadataList(indexName, parameters, pageable);
  }

  public RestCollection<ResearchObjectMetadata> getMetadataListWithConditions(String indexName, String query, String conditions,
          Pageable pageable) {
    return this.getIndexMetadataListWithConditions(indexName, query, conditions, pageable);
  }

  public RestCollection<ResearchObjectMetadata> getMetadataList(String indexName, String query, String set, Pageable pageable) {
    return this.getIndexMetadataList(indexName, query, set, pageable);
  }

  public boolean deleteMetadataByProject(String indexName, String projectShortName) {
    try {
      List<SearchCondition> query = List
              .of(new SearchCondition(SearchConditionType.TERM, "researchProject.shortName", projectShortName));

      final long deleted = this.restClientService.postObject(this.getIndexUrl(indexName) + "/" + ActionName.DELETE_BY_QUERY, query, Long.class);
      log.info("Metadata deleted in {} for project '{}': {}", indexName, projectShortName, deleted);
    } catch (SolidifyResourceNotFoundException | SolidifyRestException e) {
      log.error("Error in deleting metadata in {} for project '{}'", indexName, projectShortName, e);
      return false;
    }
    return true;
  }

  @Override
  protected String buildUrl(String indexName, Map<String, Object> parameters) {
    String url = this.getIndexUrl(indexName) + "/" + ActionName.SEARCH;
    UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);

    if (parameters != null) {

      this.completeUri(uriBuilder, parameters);

      if (!StringTool.isNullOrEmpty((String) parameters.get(HederaConstants.SEARCH_PROJECT))) {
        uriBuilder.queryParam(HederaConstants.SEARCH_PROJECT, parameters.get(HederaConstants.SEARCH_PROJECT));
      }
    }

    return uriBuilder.toUriString();
  }

  public boolean checkMetadata(String indexName, String resId) {
    return this.headIndex(indexName, resId) == HttpStatus.OK;
  }

  // Build index web service URL
  @Override
  protected String getIndexUrl(String indexName) {
    return this.getIndexModuleUrl() + "/" + indexName;
  }

  private HttpStatusCode headIndex(String indexName, String resId) {
    String url = this.getIndexUrl(indexName) + "/" + resId;
    return this.restClientService.headResource(url);
  }

  protected String getIndexModuleUrl() {
    return this.config.getModule().getIngest().getUrl();
  }

}
