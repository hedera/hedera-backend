/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - IIIFCollectionSettingsRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.rest.abstractservice;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.hedera.config.HederaProperties;
import ch.hedera.model.settings.IIIFCollectionSettings;
import ch.hedera.rest.ResourceName;

public abstract class IIIFCollectionSettingsRemoteResourceService extends RemoteResourceService<IIIFCollectionSettings> {

  private final String adminUrl;

  protected IIIFCollectionSettingsRemoteResourceService(HederaProperties properties, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.adminUrl = properties.getModule().getAdmin().getUrl();
  }

  public List<IIIFCollectionSettings> getIIIFCollectionSettings(String projectId) {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    String url = this.adminUrl + "/" + ResourceName.IIIF_COLLECTION_CONFIG + "?project.resId=" + projectId;
    return this.restClientService.getResourceList(url, IIIFCollectionSettings.class, pageable).getData();
  }

  public Optional<IIIFCollectionSettings> getIIIFCollectionSettings(String projectId, String collectionName) {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    String url = this.adminUrl + "/" + ResourceName.IIIF_COLLECTION_CONFIG + "?project.resId=" + projectId + "&name=" + collectionName;
    List<IIIFCollectionSettings> iiifCollectionSettingsList = this.restClientService.getResourceList(url, IIIFCollectionSettings.class, pageable)
            .getData();
    if (iiifCollectionSettingsList.size() == 1) {
      return Optional.of(iiifCollectionSettingsList.get(0));
    } else if (iiifCollectionSettingsList.isEmpty()) {
      return Optional.empty();
    } else {
      throw new SolidifyRuntimeException("There is multiple IIIFCollectionSettings for project " + projectId +
              " and collection name " + collectionName);
    }
  }

  @Override
  protected Class<IIIFCollectionSettings> getResourceClass() {
    return IIIFCollectionSettings.class;
  }
}
