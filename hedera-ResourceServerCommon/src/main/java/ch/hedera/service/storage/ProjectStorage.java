/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - ProjectStorage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.storage;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import ch.unige.solidify.exception.SolidifyProcessingException;

import ch.hedera.config.HederaProperties.Storage;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.settings.Project;

public abstract class ProjectStorage {

  public abstract URI getStorageLocation(ResearchDataFile researchDatafile) throws URISyntaxException;

  public abstract void purgeStorageLocation();

  public abstract void storeResearchDataFile(ResearchDataFile researchDatafile);

  public abstract InputStream getResearchDataFile(ResearchDataFile researchDatafile) throws IOException;

  public abstract void deleteResearchDataFile(ResearchDataFile researchDatafile) throws IOException;

  public static ProjectStorage getProjectStorage(Storage storageConfig, Project project) {
    return switch (project.getStorageType()) {
      case FILE -> new FileProjectStorage(storageConfig, project);
      case OBJECT -> new ObjectProjectStorage(storageConfig, project);
      default -> throw new SolidifyProcessingException("Wrong project storage type: " + project.getStorageType());
    };
  }

}
