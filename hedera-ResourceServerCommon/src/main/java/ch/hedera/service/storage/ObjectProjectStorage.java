/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - ObjectProjectStorage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.storage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3Configuration;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.Delete;
import software.amazon.awssdk.services.s3.model.DeleteBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectsRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.GetUrlRequest;
import software.amazon.awssdk.services.s3.model.HeadBucketRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;
import software.amazon.awssdk.services.s3.model.ObjectIdentifier;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.S3Exception;
import software.amazon.awssdk.services.s3.model.S3Object;

import ch.unige.solidify.exception.SolidifyProcessingException;

import ch.hedera.config.HederaProperties.ObjectStorage;
import ch.hedera.config.HederaProperties.Storage;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.settings.Project;

public class ObjectProjectStorage extends ProjectStorage {
  private static final Logger logger = LoggerFactory.getLogger(ObjectProjectStorage.class);

  private final String projectBucket;
  private final S3Client s3Client;

  protected ObjectProjectStorage(Storage storageConfig, Project project) {
    this.projectBucket = project.getShortName();
    this.s3Client = this.getNewS3Client(storageConfig.getObjectStorage());
    this.createS3BucketIfNotExist(project.getShortName());
  }

  @Override
  public void storeResearchDataFile(ResearchDataFile researchDatafile) {
    Map<String, String> s3md = new HashMap<>();
    for (final Entry<String, Object> metadata : researchDatafile.getMetadata().entrySet()) {
      s3md.put(metadata.getKey(), metadata.getValue().toString());
    }

    PutObjectRequest request = PutObjectRequest.builder()
            .bucket(this.projectBucket)
            .contentType(researchDatafile.getContentType())
            .contentLength(researchDatafile.getFileSize())
            .metadata(s3md)
            .key(researchDatafile.getRelativePath())
            .build();
    try {
      this.s3Client.putObject(request, Paths.get(researchDatafile.getSourcePath()));
    } catch (AwsServiceException | SdkClientException e) {
      throw new SolidifyProcessingException("Error in storing research data file in S3 container (" + researchDatafile.getResId() + ")", e);
    }
  }

  @Override
  public InputStream getResearchDataFile(ResearchDataFile researchDatafile) throws IOException {

    GetObjectRequest objectRequest = GetObjectRequest.builder()
            .bucket(this.projectBucket)
            .key(researchDatafile.getRelativePath())
            .build();

    ResponseBytes<GetObjectResponse> responseResponseBytes = this.s3Client.getObjectAsBytes(objectRequest);
    byte[] data = responseResponseBytes.asByteArray();
    logger.debug("Successfully obtained researchDataFile: {}, from an S3 bucket", researchDatafile.getFileName());

    return new ByteArrayInputStream(data);
  }

  @Override
  public void deleteResearchDataFile(ResearchDataFile researchDatafile) {
    logger.debug("deleting S3 object: {} from bucket: {}", researchDatafile.getFileName(), this.projectBucket);
    DeleteObjectRequest deleteObjectRequest = DeleteObjectRequest.builder()
            .bucket(this.projectBucket)
            .key(researchDatafile.getRelativePath())
            .build();
    this.s3Client.deleteObject(deleteObjectRequest);
  }

  @Override
  public URI getStorageLocation(ResearchDataFile researchDatafile) throws URISyntaxException {
    GetUrlRequest request = GetUrlRequest.builder().bucket(this.projectBucket).key(researchDatafile.getRelativePath()).build();
    return this.s3Client.utilities().getUrl(request).toURI();
  }

  @Override
  public void purgeStorageLocation() {
    ListObjectsV2Request listObjectsV2Request = ListObjectsV2Request.builder()
            .bucket(this.projectBucket)
            .build();
    ListObjectsV2Response listObjectsV2Response = this.s3Client.listObjectsV2(listObjectsV2Request);
    List<ObjectIdentifier> listObjects = new ArrayList<>();
    for (S3Object s3Object : listObjectsV2Response.contents()) {
      if (s3Object.size() > 0) { // there is a file
        throw new SolidifyProcessingException("The bucket " + this.projectBucket + " you tried to delete is not empty");
      } else {
        listObjects.add(ObjectIdentifier.builder().key(s3Object.key()).build());
      }
    }

    if (!listObjects.isEmpty()) {
      // delete all empty folders
      this.deleteObjects(listObjects);
    }

    // delete the bucket
    this.deleteBucket(this.projectBucket);
  }

  private S3Client getNewS3Client(ObjectStorage objectStorageConfig) {

    AwsBasicCredentials awsCredentials = AwsBasicCredentials.create(objectStorageConfig.getAccessKey(), objectStorageConfig.getSecretKey());

    S3Configuration s3Config = S3Configuration.builder()
            .pathStyleAccessEnabled(objectStorageConfig.getIsPathStyleAccess())
            .build();

    return S3Client.builder()
            .credentialsProvider(StaticCredentialsProvider.create(awsCredentials))
            .endpointOverride(URI.create(objectStorageConfig.getUrl()))
            .region(Region.of("GLOBAL"))
            .serviceConfiguration(s3Config)
            .build();
  }

  private void createS3BucketIfNotExist(String bucketName) {
    if (!this.doesBucketExist(bucketName)) {
      CreateBucketRequest bucketRequest = CreateBucketRequest.builder()
              .bucket(bucketName)
              .build();

      this.s3Client.createBucket(bucketRequest);
      logger.info("S3 Bucket :{} , was created successfully.", bucketName);
    }
  }

  private boolean doesBucketExist(String bucketName) {
    HeadBucketRequest headBucketRequest = HeadBucketRequest.builder()
            .bucket(bucketName)
            .build();
    try {
      this.s3Client.headBucket(headBucketRequest);
      logger.debug("S3 Bucket :{} , exist already.", bucketName);
      return true;
    } catch (NoSuchBucketException e) {
      logger.debug("S3 Bucket :{} , doesn't exist.", bucketName);
      return false;
    }
  }

  private void deleteBucket(String bucketName) {
    try {
      this.s3Client.deleteBucket(DeleteBucketRequest.builder()
              .bucket(bucketName)
              .build());
    } catch (S3Exception exc) {
      throw new SolidifyProcessingException(
              "Error deleting S3 bucket: " + bucketName + ", this bucket you tried to delete is not empty. ", exc);
    } catch (AwsServiceException e) {
      throw new SolidifyProcessingException("Error deleting S3 bucket: " + bucketName + ", call transmitted but couldn't be processed.", e);
    } catch (SdkClientException ex) {
      throw new SolidifyProcessingException(
              "Error deleting S3 bucket: " + bucketName + ", S3 couldn't be contacted or client can't parse response. ", ex);
    }
  }

  private void deleteObjects(List<ObjectIdentifier> keys) {
    // Delete multiple objects in one request.
    Delete del = Delete.builder()
            .objects(keys)
            .build();

    DeleteObjectsRequest multiObjectDeleteRequest = DeleteObjectsRequest.builder()
            .bucket(this.projectBucket)
            .delete(del)
            .build();

    this.s3Client.deleteObjects(multiObjectDeleteRequest);
  }

}
