/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - FileProjectStorage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.storage;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.FileTool;

import ch.hedera.config.HederaProperties.Storage;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.settings.Project;

public class FileProjectStorage extends ProjectStorage {

  protected final Path projectLocation;

  protected FileProjectStorage(Storage storageConfig, Project project) {
    this.projectLocation = Paths.get(URI.create(storageConfig.getFileStorage().getUrl())).resolve(project.getShortName());
    this.checkConfiguration();
  }

  protected void checkConfiguration() {
    if (!FileTool.checkFile(this.projectLocation.getParent())) {
      throw new SolidifyRuntimeException("Location (" + this.projectLocation.getParent() + ") does not exist");
    }
    if (!FileTool.ensureFolderExists(this.projectLocation)) {
      throw new SolidifyRuntimeException("Location (" + this.projectLocation + ") does not exist");
    }
  }

  @Override
  public void storeResearchDataFile(ResearchDataFile researchDataFile) {
    try {
      if (!FileTool.copyFile(Paths.get(researchDataFile.getSourcePath()), this.getStoragePath(researchDataFile))) {
        throw new SolidifyProcessingException("Cannot copy research data file '" + researchDataFile.getResId() + "'");
      }
    } catch (final IOException e) {
      throw new SolidifyProcessingException("Cannot store research data file '" + researchDataFile.getResId() + "'", e);
    }
  }

  @Override
  public InputStream getResearchDataFile(ResearchDataFile researchDataFile) throws IOException {
        final Path file = this.projectLocation.resolve(researchDataFile.getRelativePath());
        return new BufferedInputStream(FileTool.getInputStream(file));
  }

  @Override
  public URI getStorageLocation(ResearchDataFile researchDataFile) throws URISyntaxException {
    return this.getStoragePath(researchDataFile).toUri();
  }

  @Override
  public void deleteResearchDataFile(ResearchDataFile researchDataFile) throws IOException {
    FileTool.deleteFile(Paths.get(researchDataFile.getSourcePath()));
  }

  private Path getStoragePath(ResearchDataFile researchDataFile) {
    return Paths.get(this.projectLocation.toString(), researchDataFile.getRelativeLocation(), researchDataFile.getFileName());
  }

  @Override
  public void purgeStorageLocation() {
    // Check there is no more file in the project folder
    final List<Path> projectFiles = FileTool.scanFolder(this.projectLocation, Files::isRegularFile);
    if (!projectFiles.isEmpty()) {
      throw new SolidifyCheckingException("The project storage is not empty.");
    }
    // Delete the project folder
    FileTool.deleteRecursivelyEmptyDirectories(this.projectLocation);
  }

}
