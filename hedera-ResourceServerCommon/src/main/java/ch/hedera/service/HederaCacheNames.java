/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - HederaCacheNames.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import ch.unige.solidify.rest.CacheNames;

public class HederaCacheNames extends CacheNames {
  public static final String PROJECT_ROLE = "ProjectRole";
  public static final String PROJECT_OBJECT_TYPES = "ProjectObjectTypes";
  public static final String PROJECT_BY_SHORT_NAME = "ProjectByShortName";
  public static final String RESEARCH_OBJECT_TYPE_BY_NAME = "ResearchObjectTypeByName";
  public static final String SEARCH = "search";
  public static final String SUB_RESOURCES = "SubResources";
  public static final String USER_EXTERNAL_UID = "UserExternalUid";
  public static final String NEW_USER = "newUser";
  public static final String ROR_INFO = "rorInfo";
}
