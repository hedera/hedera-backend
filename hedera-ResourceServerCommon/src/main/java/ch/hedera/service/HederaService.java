/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - HederaService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.logging.LogLevel;

import ch.unige.solidify.exception.SolidifyFileDeleteException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

public abstract class HederaService {

  protected static final String LOG_OBJECT_WITH_STATUS_TEMPLATE = "{} '{}' ({}) (status: {}): {}";

  private static final Logger log = LoggerFactory.getLogger(HederaService.class);

  protected final MessageService messageService;

  protected HederaService(MessageService messageService) {
    this.messageService = messageService;
  }

  protected <T extends Resource> boolean belongsToList(T item, List<T> list) {
    return list.stream().anyMatch(o -> item.getResId().equals(o.getResId()));
  }

  protected void deleteFile(URI fileToDelete) {
    try {
      FileTool.deleteFile(fileToDelete);
    } catch (final IOException e) {
      throw new SolidifyFileDeleteException(fileToDelete.toString(), e);
    }
  }

  protected void logObjectWithStatusMessage(LogLevel logLevel, String className, String resId, String status, String name, String message,
          Exception... exceptions) {
    switch (logLevel) {
      case TRACE -> log.trace(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message);
      case DEBUG -> log.debug(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message);
      case WARN -> log.warn(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message);
      case ERROR -> {
        Exception e = (exceptions.length > 0) ? exceptions[0] : null;
        log.error(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message, e);
      }
      default -> log.info(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message);
    }
  }
}
