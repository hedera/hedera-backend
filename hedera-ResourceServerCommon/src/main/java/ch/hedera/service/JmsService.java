/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - JmsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.service.MessageService;

import ch.hedera.config.HederaProperties;
import ch.hedera.message.EmailMessage;
import ch.hedera.message.IIIFMessage;
import ch.hedera.message.IdMappingMessage;
import ch.hedera.message.ProjectMessage;
import ch.hedera.message.RdfStorageMessage;
import ch.hedera.message.ResearchDataFileMessage;
import ch.hedera.message.RmlConversionMessage;

@Service
public class JmsService extends HederaService {
  private static final Logger log = LoggerFactory.getLogger(JmsService.class);

  private final String projectQueue;
  private final String rmlConversionQueue;
  private final String researchDataFileQueue;
  private final String rdfStorageQueue;
  private final String idMappingQueue;
  private final String iiifQueue;

  private final String cacheTopic;
  private final String emailsTopic;

  private JmsTemplate queueJmsTemplate;

  private JmsTemplate topicJmsTemplate;

  public JmsService(JmsTemplate queueJmsTemplate, @Qualifier("topicJmsTemplate") JmsTemplate topicJmsTemplate, HederaProperties properties,
          MessageService messageService) {
    super(messageService);
    this.queueJmsTemplate = queueJmsTemplate;
    this.topicJmsTemplate = topicJmsTemplate;
    this.projectQueue = properties.getQueue().getProject();
    this.rmlConversionQueue = properties.getQueue().getRmlConversion();
    this.researchDataFileQueue = properties.getQueue().getResearchDataFile();
    this.rdfStorageQueue = properties.getQueue().getRdfStorage();
    this.cacheTopic = properties.getTopic().getCache();
    this.emailsTopic = properties.getTopic().getEmails();
    this.idMappingQueue = properties.getQueue().getIdMapping();
    this.iiifQueue = properties.getQueue().getIiif();
  }

  private void convertAndSendToQueue(String queue, Object event) {
    if (log.isTraceEnabled()) {
      log.trace("Sending message to queue {} => {}", queue, event);
    }
    this.queueJmsTemplate.convertAndSend(queue, event);
  }

  private void convertAndSendToTopic(String topic, Object event) {
    if (log.isTraceEnabled()) {
      log.trace("Sending message to topic {} => {}", topic, event);
    }
    this.topicJmsTemplate.convertAndSend(topic, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(CacheMessage event) {
    this.convertAndSendToTopic(this.cacheTopic, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(ProjectMessage event) {
    this.convertAndSendToQueue(this.projectQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(IdMappingMessage event) {
    this.convertAndSendToQueue(this.idMappingQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(IIIFMessage event) {
    this.convertAndSendToQueue(this.iiifQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(EmailMessage event) {
    this.convertAndSendToTopic(this.emailsTopic, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(RmlConversionMessage event) {
    this.convertAndSendToQueue(this.rmlConversionQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(ResearchDataFileMessage event) {
    this.convertAndSendToQueue(this.researchDataFileQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(RdfStorageMessage event) {
    this.convertAndSendToQueue(this.rdfStorageQueue, event);
  }
}
