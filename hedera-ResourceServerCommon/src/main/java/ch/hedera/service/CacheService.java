/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - CacheService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.IndexCacheNames;
import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.message.ResourceCacheMessage;
import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.rest.CacheNames;
import ch.unige.solidify.service.AbstractCacheService;
import ch.unige.solidify.util.StringTool;

import ch.hedera.config.HederaProperties;
import ch.hedera.message.cache.ProjectPersonRoleCacheMessage;
import ch.hedera.model.security.User;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;

@Service
public class CacheService extends AbstractCacheService {

  private static final Logger logger = LoggerFactory.getLogger(CacheService.class);

  private static final String CACHE_CLEARED = "cache cleared";

  /**
   * Autowiring itself allows cache to work on a method inside its own class
   */
  @Autowired
  @Lazy
  protected CacheService self;

  private final Map<String, List<Project>> cachedProjects = new ConcurrentHashMap<>();

  private final Map<String, Date> cachedProjectDates = new ConcurrentHashMap<>();

  @Autowired
  private HederaProperties config;

  /**
   * Clear entry in ProjectRoles cache identified by a personId and an organizational unit id
   *
   * @param personId
   * @param projectId
   */
  @CacheEvict(cacheNames = HederaCacheNames.PROJECT_ROLE)
  public void cleanCacheProjectRole(String personId, String projectId) {
    CacheService.logger
            .trace("'ProjectRoles' cache cleared for: personId={}, projectId={}", personId, projectId);
    this.cachedProjects.clear();
    this.cachedProjectDates.clear();
  }

  @CacheEvict(cacheNames = HederaCacheNames.PROJECT_OBJECT_TYPES, allEntries = true)
  public void cleanCacheProjectObjectTypes() {
    CacheService.logger.trace("'" + HederaCacheNames.PROJECT_OBJECT_TYPES + "' " + CACHE_CLEARED);
  }

  @CacheEvict(cacheNames = HederaCacheNames.PROJECT_BY_SHORT_NAME, allEntries = true)
  public void cleanCacheProjectByShortName() {
    CacheService.logger.trace("'" + HederaCacheNames.PROJECT_BY_SHORT_NAME + "' " + CACHE_CLEARED);
  }

  @CacheEvict(cacheNames = HederaCacheNames.RESEARCH_OBJECT_TYPE_BY_NAME, allEntries = true)
  public void cleanCacheResearchObjectTypeByName() {
    CacheService.logger.trace("'" + HederaCacheNames.RESEARCH_OBJECT_TYPE_BY_NAME + "' " + CACHE_CLEARED);
  }

  /**
   * Clear entry in RESTResources cache identified by its resId
   *
   * @param resId
   */
  @CacheEvict(cacheNames = { CacheNames.RESOURCES, HederaCacheNames.SUB_RESOURCES })
  public void cleanCacheResources(String resId) {
    CacheService.logger.trace("'Resources' cache cleared for resId: {}", resId);
  }

  /**
   * Clear entry in search cache identified by query
   */
  @CacheEvict(cacheNames = HederaCacheNames.SEARCH, allEntries = true)
  public void cleanCacheSearch() {
    CacheService.logger.trace("'search' cache cleared ");
  }

  /**
   * Clear entry in UserExternalUid cache identified by externalUid
   *
   * @param externalUid
   */
  @CacheEvict(cacheNames = HederaCacheNames.USER_EXTERNAL_UID)
  public void cleanCacheUserExternalUid(String externalUid) {
    CacheService.logger.trace("'UserExternalUid' cache cleared for externalUid: {}", externalUid);
  }

  @CacheEvict(value = IndexCacheNames.FACET_REQUESTS, allEntries = true)
  public void evictCachedFacets() {
    CacheService.logger.trace("Facets removed from cache");
  }

  @CacheEvict(value = IndexCacheNames.INDEX_FIELD_ALIASES, allEntries = true)
  public void evictCachedIndexFieldAliases() {
    CacheService.logger.trace("IndexFieldAliases removed from cache");
  }

  public List<Project> getCachedAuthorizedProjects(String cacheKey) {

    if (!StringTool.isNullOrEmpty(cacheKey)) {
      final Date cacheDate = this.cachedProjectDates.get(cacheKey);

      if (cacheDate != null && (System.currentTimeMillis() - cacheDate.getTime()) < (this.config.getParameters().getAuthorizedUnitsCacheTime()
              * 1000)) {
        /*
         * projects taken from the cache
         */
        CacheService.logger.trace("organizational units taken from cache for {}", cacheKey);

        return this.cachedProjects.get(cacheKey);
      } else {
        /*
         * projects have not been cached yet or cache is too old
         */
        this.cachedProjects.remove(cacheKey);
        return Collections.emptyList();
      }
    } else {
      return Collections.emptyList();
    }
  }

  @JmsListener(destination = "${hedera.topic.cache}", containerFactory = "topicListenerFactory")
  public void processCacheMessage(CacheMessage message) {

    if (message instanceof ResourceCacheMessage resourceCacheMessage) {

      this.self.cleanCacheResources(resourceCacheMessage.getResId());

      if (message.hasOtherProperty("search")) {
        this.self.cleanCacheSearch();
      }

      if (((ResourceCacheMessage) message).getResourceClass() == User.class) {
        this.self.cleanCacheUserExternalUid(message.getOtherProperty("externalUid").toString());
      } else if (((ResourceCacheMessage) message).getResourceClass() == ResearchObjectType.class) {
        this.self.cleanCacheProjectObjectTypes();
      } else if (((ResourceCacheMessage) message).getResourceClass() == Project.class) {
        this.self.cleanCacheProjectByShortName();
      } else if (((ResourceCacheMessage) message).getResourceClass() == IndexFieldAlias.class) {
        this.self.evictCachedFacets();
        this.self.evictCachedIndexFieldAliases();
      }

    } else if (message instanceof ProjectPersonRoleCacheMessage projectPersonRoleCacheMessage) {
      this.self.cleanCacheProjectRole(projectPersonRoleCacheMessage.getPersonId(),
              ((ProjectPersonRoleCacheMessage) message).getPersonId());
    }
  }

  public void storeAuthorizedProjects(String cacheKey, List<Project> projects) {
    if (!StringTool.isNullOrEmpty(cacheKey)) {
      CacheService.logger.trace("organizational units put in cache for {}", cacheKey);
      this.cachedProjects.put(cacheKey, projects);
      this.cachedProjectDates.put(cacheKey, new Date());
    }
  }

}
