/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Resource Server Common - MetadataServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.test.metadata;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.xml.bind.JAXBException;

import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.humanities.Ontology;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.model.ingest.DataAccessibilityType;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.model.xml.hedera.v1.researchObject.ObjectType;
import ch.hedera.service.MetadataService;

public class MetadataServiceTest {

  protected Path sourceFolder = Paths.get("src", "test", "resources").toAbsolutePath();

  @Mock
  protected MessageService messageService;

  protected MetadataService metadataService = new MetadataService(this.messageService);

  @Test
  void researchObjectTest() throws JsonProcessingException, JAXBException {
    final String objectId = StringTool.generateResId();
    final String subject = "https://hedera.unige.ch/project/type/subject";
    final OffsetDateTime sourceLastUpdate = OffsetDateTime.now();
    final String sourceId = StringTool.generateResId();
    final String json = this.metadataService.getMetadataInJson(
            this.createProject(),
            this.createResearchObjectType(),
            objectId,
            subject,
            ObjectType.MANAGED,
            sourceLastUpdate,
            sourceId,
            this.createProperties());
    assertNotNull(json);
    ResearchObjectMetadata researchObjectMetadata = new ResearchObjectMetadata("public", objectId, json);
    assertNotNull(researchObjectMetadata);
  }

  @Test
  void researchDataFileTest() throws JsonProcessingException, JAXBException {
    final ResearchDataFile researchDataFile = this.createResarchDataFile();
    final String json = this.metadataService.getMetadataInJson(researchDataFile);
    assertNotNull(json);
    ResearchObjectMetadata researchObjectMetadata = new ResearchObjectMetadata("public", researchDataFile.getResId(), json);
    assertNotNull(researchObjectMetadata);
  }

  private ResearchDataFile createResarchDataFile() {
    ResearchDataFile researchDataFile = new ResearchDataFile();
    researchDataFile.setResearchObjectType(this.createResearchObjectType());
    researchDataFile.setProject(this.createProject());
    researchDataFile.getLastUpdate().setWhen(OffsetDateTime.now());
    researchDataFile.setAccessibleFrom(DataAccessibilityType.IIIF);
    researchDataFile.setFileName("research-data-file.jpg");
    researchDataFile.setSourcePath(this.getDataFile("ludus-data.ttl").toUri());
    researchDataFile.setRelativeLocation("/");
    researchDataFile.setMimeType("image/jpeg");
    researchDataFile.setFileSize(1000L);
    researchDataFile.setMetadata(this.createProperties());
    return researchDataFile;
  }

  private Map<String, Object> createProperties() {
    Map<String, Object> properties = new HashMap<>();
    properties.put(HederaConstants.FILE_METADATA_WIDTH, 1200);
    properties.put(HederaConstants.FILE_METADATA_HEIGHT, 100);
    return properties;
  }

  protected Path getDataFile(String filename) {
    final Path file = this.sourceFolder.resolve(filename);
    assertNotNull(file);
    assertTrue(FileTool.checkFile(file));
    return file;
  }

  private ResearchObjectType createResearchObjectType() {
    ResearchObjectType rot = new ResearchObjectType();
    rot.setName("researchj-object-type");
    rot.setRdfType("rdf-type");
    rot.setOntology(this.createOntology());
    return rot;
  }

  private Ontology createOntology() {
    Ontology onto = new Ontology();
    onto.setName("ontology");
    onto.setVersion("1.0");
    onto.setFormat(RdfFormat.RDF_XML);
    onto.setBaseUri(URI.create("https://ontology.herea.unige,ch"));
    return onto;
  }

  private Project createProject() {
    Project p = new Project();
    p.setName("project");
    p.setShortName("project");
    p.setResearchDataFileResearchObjectType(this.createResearchObjectType());
    p.setOpeningDate(LocalDate.now());
    p.setAccessPublic(Boolean.TRUE);
    return p;
  }

}
