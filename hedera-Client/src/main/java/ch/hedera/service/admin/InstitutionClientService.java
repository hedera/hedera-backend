/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - InstitutionClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.admin;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;

import ch.hedera.model.ResourceIdentifierType;
import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class InstitutionClientService extends ResourceClientService<Institution> {

  public InstitutionClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.INSTITUTION,
            Institution.class,
            restClientTool,
            env);
  }

  public void addPerson(String institutionId, String personId) {
    this.addItem(institutionId, ResourceName.PERSON, personId, Person.class);
  }

  public void addProject(String institutionId, String projectId) {
    this.addItem(institutionId, ResourceName.PROJECT, projectId, Project.class);
  }

  @Override
  public List<Institution> findAll() {
    return this.findAll(Institution.class);
  }

  public List<Project> getProjects(String institutionId) {
    return this.findAllLinkedResources(institutionId, ResourceName.PROJECT, Project.class);
  }

  public Project getProject(String institutionId, String projectId) {
    for (Project project : this.findAllLinkedResources(institutionId, ResourceName.PROJECT, Project.class)) {
      if (projectId.equals(project.getResId())) {
        return project;
      }
    }
    return null;
  }

  public List<Person> getPeople(String institutionId) {
    return this.findAllLinkedResources(institutionId, ResourceName.PERSON, Person.class);
  }

  public void removeProject(String institutionId, String projectId) {
    this.removeItem(institutionId, ResourceName.PROJECT, projectId);
  }

  public void removePerson(String institutionId, String personId) {
    this.removeItem(institutionId, ResourceName.PERSON, personId);
  }

  @Override
  public List<Institution> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Institution.class);
  }

  public Institution uploadLogo(String resId, Resource file) {
    return this.uploadFile(resId, HederaActionName.UPLOAD_LOGO, file);
  }

  public void downloadLogo(String resId, Path path) {
    this.downloadFile(resId, HederaActionName.DOWNLOAD_LOGO, path);
  }

  public void deleteLogo(String resId) {
    this.deleteAction(resId, HederaActionName.DELETE_LOGO);
  }

  public Institution findByRorId(String rorId) {
    return this.findByIdentifierType(this.getResourceUrl(), ResourceIdentifierType.ROR_ID, rorId, Institution.class);
  }

  public Institution findByName(String name) {
    return this.findByIdentifierType(this.getResourceUrl(), ResourceIdentifierType.NAME, name, Institution.class);
  }
}
