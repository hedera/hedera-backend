/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - TriplestoreQueriesClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.admin;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;

import ch.hedera.model.triplestore.SparqlResultRow;
import ch.hedera.model.triplestore.TriplestoreDataset;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class TriplestoreQueriesClientService extends ResourceClientService<TriplestoreDataset> {

  protected TriplestoreQueriesClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, ResourceName.TRIPLESTORE, TriplestoreDataset.class,
            restClientTool, env);
  }

  public List<SparqlResultRow> executeQueryToDataset(String datasetId, String query) {
    final String url = this.getResourceUrl()
            + "/" + ResourceName.QUERY
            + "/" + datasetId
            + "/list";

    RestTemplate restTemplate = this.restClientTool.getClient();
    String jsonString = restTemplate.postForObject(url, query, String.class);

    return CollectionTool.getList(jsonString, SparqlResultRow.class);

  }

  @Override
  public List<TriplestoreDataset> findAll() {
    return Collections.emptyList();
  }

  @Override
  public List<TriplestoreDataset> searchByProperties(Map<String, String> properties) {
    return Collections.emptyList();
  }

}
