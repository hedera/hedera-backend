/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - FundingAgencyClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.admin;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;

import ch.hedera.model.ResourceIdentifierType;
import ch.hedera.model.settings.FundingAgency;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class FundingAgencyClientService extends ResourceClientService<FundingAgency> {

  public FundingAgencyClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.FUNDING_AGENCY,
            FundingAgency.class,
            restClientTool,
            env);
  }

  public void addProject(String fundingAgencyId, String unitId) {
    this.addItem(fundingAgencyId, ResourceName.PROJECT, unitId, Project.class);
  }

  @Override
  public List<FundingAgency> findAll() {
    return this.findAll(FundingAgency.class);
  }

  public List<Project> getProjects(String fundingAgencyId) {
    return this.findAllLinkedResources(fundingAgencyId, ResourceName.PROJECT, Project.class);
  }

  public void removeProject(String fundingAgencyId, String projectId) {
    this.removeItem(fundingAgencyId, ResourceName.PROJECT, projectId);
  }

  @Override
  public List<FundingAgency> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, FundingAgency.class);
  }

  public FundingAgency uploadLogo(String resId, Resource file) {
    return this.uploadFile(resId, HederaActionName.UPLOAD_LOGO, file);
  }

  public void downloadLogo(String resId, Path path) {
    this.downloadFile(resId, HederaActionName.DOWNLOAD_LOGO, path);
  }

  public void deleteLogo(String resId) {
    this.deleteAction(resId, HederaActionName.DELETE_LOGO);
  }

  public FundingAgency findByRorId(String rorId) {
    return this.findByIdentifierType(this.getResourceUrl(), ResourceIdentifierType.ROR_ID, rorId, FundingAgency.class);
  }

  public FundingAgency findByAcronym(String acronym) {
    return this.findByIdentifierType(this.getResourceUrl(), ResourceIdentifierType.ACRONYM, acronym, FundingAgency.class);
  }
}
