/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - LicenseClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.admin;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;

import ch.hedera.model.ResourceIdentifierType;
import ch.hedera.model.settings.License;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;
import ch.hedera.rest.UrlPath;

@Service
public class LicenseClientService extends ResourceClientService<License> {
  private static final Logger log = LoggerFactory.getLogger(LicenseClientService.class);

  public LicenseClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.LICENSE,
            License.class,
            restClientTool,
            env);
  }

  public HttpEntity<String> createLicenses(List<License> licences) {
    final String url = this.getResourceUrl() + UrlPath.ADMIN_LICENSE_IMPORT_LIST;
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    final String response = restTemplate.postForObject(url, licences, String.class);
    return new ResponseEntity<>(response, HttpStatus.CREATED);
  }

  @Override
  public List<License> findAll() {
    return this.findAll(License.class);
  }

  public HttpEntity<String> importLicenseFile(String fileName) {
    final String url = this.getResourceUrl() + UrlPath.ADMIN_LICENSE_IMPORT_FILE;
    return this.importFile(url, fileName);
  }

  public HttpEntity<String> importOpenLicenseFile(String fileName) {
    final String url = this.getResourceUrl() + UrlPath.ADMIN_OPEN_LICENSE_IMPORT_FILE;
    return this.importFile(url, fileName);
  }

  @Override
  public List<License> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, License.class);
  }

  private HttpEntity<String> importFile(String url, String filename) {
    String response;
    try {
      final HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.MULTIPART_FORM_DATA);
      final LinkedMultiValueMap<String, Object> multipartMap = new LinkedMultiValueMap<>();
      multipartMap.add("file", new ClassPathResource(filename));
      final HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(multipartMap, headers);
      final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
      response = restTemplate.postForObject(url, requestEntity, String.class);
      return new ResponseEntity<>(response, HttpStatus.CREATED);
    } catch (final HttpStatusCodeException e) {
      log.error("Error importing file " + filename + " with url " + url, e);
      response = "Error : " + e.getStatusCode() + "|" + e.getStatusText();
    }
    return new HttpEntity<>(response);
  }

  public License findBySpdxId(String spdxId) {
    return this.findByIdentifierType(this.getResourceUrl(), ResourceIdentifierType.SPDX_ID, spdxId, License.class);
  }

  public License uploadLogo(String resId, Resource file) {
    return this.uploadFile(resId, HederaActionName.UPLOAD_LOGO, file);
  }
}
