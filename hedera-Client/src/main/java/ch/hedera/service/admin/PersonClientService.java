/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - PersonClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.admin;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;

import ch.hedera.model.ResourceIdentifierType;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class PersonClientService extends ResourceClientService<Person> {

  public PersonClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.PERSON,
            Person.class,
            restClientTool,
            env);
  }

  public void addInstitution(String personId, String institutionId) {
    this.addItem(personId, ResourceName.INSTITUTION, institutionId, Institution.class);
  }

  public void addProject(String personResId, String projectId) {
    this.addItem(personResId, ResourceName.PROJECT, projectId, Project.class);
  }

  public void addProject(String personId, String projectId, String... roleNames) {
    this.addRelationItem(personId, ResourceName.PROJECT + "/" + projectId, Role.class, roleNames);
  }

  @Override
  public List<Person> findAll() {
    return this.findAll(Person.class);
  }

  public List<Institution> getInstitutions(String personId) {
    return this.findAllLinkedResources(personId, ResourceName.INSTITUTION, Institution.class);
  }

  public List<Project> getProjects(String personId) {
    return this.findAllLinkedResources(personId, ResourceName.PROJECT, Project.class);
  }

  public void removeProject(String personId, String projectId) {
    this.removeItem(personId, ResourceName.PROJECT, projectId);
  }

  public void removeProject(String personId, String projectId, String roleName) {
    this.removeItem(personId, ResourceName.PROJECT, projectId, roleName);
  }

  public List<Person> searchWithUser(String search) {
    String url = this.getResourceUrl() + "/" + HederaActionName.SEARCH_WITH_USER + "?search=" + search;
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, Person.class);
  }

  public List<Person> search(String search, String matchType) {
    return this.search(search, matchType, Person.class);
  }

  @Override
  public List<Person> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Person.class);
  }

  public Person uploadAvatar(String resId, Resource file) {
    return this.uploadFile(resId, HederaActionName.UPLOAD_AVATAR, file);
  }

  public void downloadAvatar(String resId, Path path) {
    this.downloadFile(resId, HederaActionName.DOWNLOAD_AVATAR, path);
  }

  public void deleteAvatar(String resId) {
    this.deleteAction(resId, HederaActionName.DELETE_AVATAR);
  }

  public Person findByOrcid(String orcid) {
    return this.findByIdentifierType(this.getResourceUrl(), ResourceIdentifierType.ORCID, orcid, Person.class);
  }
}
