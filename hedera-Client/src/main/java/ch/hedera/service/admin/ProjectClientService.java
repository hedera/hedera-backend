/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - ProjectClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.admin;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;

import ch.hedera.model.ResourceIdentifierType;
import ch.hedera.model.display.PersonRole;
import ch.hedera.model.humanities.Rml;
import ch.hedera.model.security.Role;
import ch.hedera.model.settings.FundingAgency;
import ch.hedera.model.settings.Institution;
import ch.hedera.model.settings.Person;
import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ProjectPersonRole;
import ch.hedera.model.settings.ProjectPersonRoleKey;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class ProjectClientService extends ResourceClientService<Project> {

  public ProjectClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.PROJECT,
            Project.class,
            restClientTool,
            env);
  }

  public void addFundingAgency(String projectId, String fundingAgencyId) {
    this.addItem(projectId, ResourceName.FUNDING_AGENCY, fundingAgencyId, FundingAgency.class);
  }

  public void addInstitution(String projectId, String institutionId) {
    this.addItem(projectId, ResourceName.INSTITUTION, institutionId, Institution.class);
  }

  public void addRml(String projectId, String rmlId) {
    this.addItem(projectId, ResourceName.RML, rmlId, Rml.class);
  }

  public void addResearchObjectType(String projectId, String researchObjectId) {
    this.addItem(projectId, ResourceName.RESEARCH_OBJECT_TYPE, researchObjectId, ResearchObjectType.class);
  }

  public void addPerson(String projectId, String personId) {
    this.addItem(projectId, ResourceName.PERSON, personId, Person.class);
  }

  public void addPersonRole(String projectId, String personId, String roleName) {
    this.addRelationItem(projectId, ResourceName.PERSON + "/" + personId, Role.class, roleName);
  }

  public void updatePersonRole(String orgUnitId, String personId, String roleName) {
    ProjectPersonRole joinResource = new ProjectPersonRole();
    ProjectPersonRoleKey compositeKey = new ProjectPersonRoleKey();
    Role role = new Role();
    role.setName(roleName);
    compositeKey.setRole(role);
    joinResource.setCompositeKey(compositeKey);
    this.updateRelationItem(orgUnitId, ResourceName.PERSON + "/" + personId, Role.class, joinResource);
  }

  public Project close(String projectId, String date) {
    final String url = this.getResourceUrl() + "/" + projectId + "/" + ActionName.CLOSE + "?closingDate=" + date;

    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());

    return restTemplate.postForObject(url, null, Project.class);
  }

  @Override
  public List<Project> findAll() {
    return this.findAll(Project.class);
  }

  public List<FundingAgency> getFundingAgencies(String projectId) {
    return this.findAllLinkedResources(projectId, ResourceName.FUNDING_AGENCY, FundingAgency.class);
  }

  public List<Institution> getInstitutions(String projectId) {
    return this.findAllLinkedResources(projectId, ResourceName.INSTITUTION, Institution.class);
  }

  public List<Rml> getRmls(String projectId) {
    return this.findAllLinkedResources(projectId, ResourceName.RML, Rml.class);
  }

  public Rml getRmls(String projectId, String rmlId) {
    return this.findOneLinkedResource(projectId, ResourceName.RML, Rml.class, rmlId);
  }

  public List<ResearchObjectType> getResearchObjectTypes(String projectId) {
    return this.findAllLinkedResources(projectId, ResourceName.RESEARCH_OBJECT_TYPE, ResearchObjectType.class);
  }

  public ResearchObjectType getResearchObjectType(String projectId, String researchObjectTypeId) {
    return this.findOneLinkedResource(projectId, ResourceName.RESEARCH_OBJECT_TYPE, ResearchObjectType.class, researchObjectTypeId);
  }

  public List<Person> getPeople(String projectId) {
    return this.findAllLinkedResources(projectId, ResourceName.PERSON, Person.class);
  }

  public Role getPersonRole(String projectId, String personId) {
    for (PersonRole personRole : this.findAllLinkedResources(projectId, ResourceName.PERSON, PersonRole.class)) {
      if (personId.equals(personRole.getResId())) {
        final List<Role> roleList = personRole.getItems();
        if (roleList.size() == 1) {
          return personRole.getItems().get(0);
        } else if (roleList.size() > 1) {
          throw new IllegalStateException("Person " + personId + " has multiple role in project " + projectId);
        }
      }
    }
    return null;
  }

  public void removeFundingAgency(String projectId, String fundingAgencyId) {
    this.removeItem(projectId, ResourceName.FUNDING_AGENCY, fundingAgencyId);
  }

  public void removeInstitution(String projectId, String institutionId) {
    this.removeItem(projectId, ResourceName.INSTITUTION, institutionId);
  }

  public void removePerson(String projectId, String personId) {
    this.removeItem(projectId, ResourceName.PERSON, personId);
  }

  public void removePerson(String projectId, String personId, String roleName) {
    this.removeItem(projectId, ResourceName.PERSON, personId, roleName);
  }

  public void removeRml(String projectId, String rmlId) {
    this.removeItem(projectId, ResourceName.RML, rmlId);
  }

  public void removeResearchObjectType(String projectId, String researchObjectId) {
    this.removeItem(projectId, ResourceName.RESEARCH_OBJECT_TYPE, researchObjectId);
  }

  @Override
  public List<Project> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Project.class);
  }

  public Project findByName(String name) {
    return this.findByIdentifierType(this.getResourceUrl(), ResourceIdentifierType.NAME, name, Project.class);
  }

  public Project findByShortName(String shortName) {
    return this.findByIdentifierType(this.getResourceUrl(), ResourceIdentifierType.SHORT_NANE, shortName, Project.class);
  }

  public Project uploadLogo(String resId, Resource file) {
    return this.uploadFile(resId, HederaActionName.UPLOAD_LOGO, file);
  }

  public void downloadLogo(String resId, Path path) {
    this.downloadFile(resId, HederaActionName.DOWNLOAD_LOGO, path);
  }

  public void deleteLogo(String resId) {
    this.deleteAction(resId, HederaActionName.DELETE_LOGO);
  }

  public List<Project> getAuthorizedProject() {
    String url = this.getBaseUrl() + "/" + ResourceName.AUTHORIZED_PROJECT;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, Project.class);
  }
}
