/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - OntologyClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.admin;

import java.util.List;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;

import ch.hedera.model.humanities.Ontology;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class OntologyClientService extends ResourceClientService<Ontology> {

  public OntologyClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.ONTOLOGY,
            Ontology.class,
            restClientTool,
            env);
  }

  @Override
  public List<Ontology> findAll() {
    return this.findAll(Ontology.class);
  }

  @Override
  public List<Ontology> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Ontology.class);
  }

  public List<Ontology> listExternalOntologies() {
    return this.searchByProperties(Map.of("external", "true"));
  }

  public List<Ontology> listInternalOntologies() {
    return this.searchByProperties(Map.of("external", "false"));
  }

  public Ontology uploadOntologyFile(String resId, Resource file) {
    return this.uploadFile(resId, HederaActionName.UPLOAD_FILE, file);
  }

  public Ontology createAndUploadOntologyFile(Ontology ontology, org.springframework.core.io.Resource file) {
    final MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);

    parameters.add(SolidifyConstants.FILE_PARAM, file);
    parameters.add("name", ontology.getName());
    parameters.add("version", ontology.getVersion());
    parameters.add("format", ontology.getFormat().name());
    parameters.add("baseUri", ontology.getBaseUri().toString());
    if (ontology.getDescription() != null) {
      parameters.add("description", ontology.getDescription());
    }
    if (ontology.getUrl() != null) {
      parameters.add("url", ontology.getUrl().toString());
    }

    final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(parameters, headers);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(this.getResourceUrl() + "/" + HederaActionName.UPLOAD_ONTOLOGY, requestEntity, Ontology.class);
  }

  public List<String> listRdfClasses(String ontologyId) {
    return this.listRdfType(ontologyId, ResourceName.RDF_CLASS);
  }

  public List<String> listRdfProperties(String ontologyId) {
    return this.listRdfType(ontologyId, ResourceName.RDF_PROPERTY);
  }

  public String isPartOf(String ontologyId, String rdfType) {
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(
            this.getResourceUrl()
                    + "/" + ontologyId
                    + "/" + HederaActionName.IS_PART_OF
                    + "/" + rdfType,
            null,
            String.class);
  }

  private List<String> listRdfType(String ontologyId, String resources) {
    final RestTemplate restTemplate = this.restClientTool.getClient();
    final ResponseEntity<List<String>> response = restTemplate.exchange(
            this.getResourceUrl()
                    + "/" + ontologyId
                    + "/" + resources,
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<String>>() {
            });
    return response.getBody();
  }
}
