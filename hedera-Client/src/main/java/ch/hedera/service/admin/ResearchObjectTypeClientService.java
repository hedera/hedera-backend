/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - ResearchObjectTypeClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.admin;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;

import ch.hedera.model.settings.Project;
import ch.hedera.model.settings.ResearchObjectType;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class ResearchObjectTypeClientService extends ResourceClientService<ResearchObjectType> {
  public ResearchObjectTypeClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.RESEARCH_OBJECT_TYPE,
            ResearchObjectType.class,
            restClientTool,
            env);
  }

  @Override
  public List<ResearchObjectType> findAll() {
    return this.findAll(ResearchObjectType.class);
  }

  @Override
  public List<ResearchObjectType> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, ResearchObjectType.class);
  }

  public List<Project> getProjects(String researchObjectId) {
    return this.findAllLinkedResources(researchObjectId, ResourceName.PROJECT, Project.class);
  }

  public void removeProject(String researchObjectId, String projectId) {
    this.removeItem(researchObjectId, ResourceName.PROJECT, projectId);
  }

}
