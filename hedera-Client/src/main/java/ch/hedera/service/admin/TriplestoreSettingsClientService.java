/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - TriplestoreSettingsClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.NoSqlResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;

import ch.hedera.model.triplestore.DatasetSettings;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class TriplestoreSettingsClientService extends NoSqlResourceClientService<DatasetSettings> {

  public TriplestoreSettingsClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.TRIPLESTORE + "/" + ResourceName.SETTINGS,
            DatasetSettings.class,
            restClientTool,
            env);
  }

  public DatasetSettings create(String resId) {
    String url = this.getResourceUrl();
    RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    DatasetSettings datasetSettings = new DatasetSettings();
    datasetSettings.setResId(resId);
    return restTemplate.postForObject(url, datasetSettings, this.getResourceClass());
  }

  public DatasetSettings findOne(String resId) {
    String url = this.getResourceUrl() + "/{resId}";
    Map<String, String> params = new HashMap<>();
    params.put("resId", resId);
    return this.getObjectFromUrl(url, params);
  }

  public List<DatasetSettings> findAll() {
    String url = this.getResourceUrl() + "?size=2000";
    RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, this.getResourceClass());
  }

  public void delete(String resId) {
    String url = this.getResourceUrl() + "/{resId}";
    Map<String, String> params = new HashMap<>();
    params.put("resId", resId);
    RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    restTemplate.delete(url, params);
  }
}
