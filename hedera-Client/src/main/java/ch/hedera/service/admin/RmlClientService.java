/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - RmlClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.admin;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;

import ch.hedera.model.humanities.Rml;
import ch.hedera.model.settings.Project;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class RmlClientService extends ResourceClientService<Rml> {
  public RmlClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.RML,
            Rml.class,
            restClientTool,
            env);
  }

  @Override
  public List<Rml> findAll() {
    return this.findAll(Rml.class);
  }

  @Override
  public List<Rml> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Rml.class);
  }

  public Rml createAndUploadRdfFile(Rml rml, org.springframework.core.io.Resource file) {
    final MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);

    parameters.add(SolidifyConstants.FILE_PARAM, file);
    parameters.add("name", rml.getName());
    parameters.add("version", rml.getVersion());
    parameters.add("format", rml.getFormat().getName());
    parameters.add("description", rml.getDescription());

    final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(parameters, headers);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(this.getResourceUrl() + "/" + HederaActionName.UPLOAD_RDF, requestEntity, Rml.class);
  }

  public List<Project> getProjects(String rmlId) {
    return this.findAllLinkedResources(rmlId, ResourceName.PROJECT, Project.class);
  }

  public void removeProject(String rmlId, String projectId) {
    this.removeItem(rmlId, ResourceName.PROJECT, projectId);
  }
}
