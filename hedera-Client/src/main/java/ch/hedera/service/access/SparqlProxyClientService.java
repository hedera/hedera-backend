/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - SparqlProxyClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.access;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.util.AbstractRestClientTool;

import ch.hedera.rest.ModuleName;

@Service
public class SparqlProxyClientService extends HederaClientService {

  protected SparqlProxyClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(restClientTool, env);
  }

  public String executeQueryAsPost(String projectId, String request) {
    final String url = this.getResourceUrl() + "/" + projectId;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(url, request, String.class);
  }

  public String executeQueryAsPostUrlEncoded(String projectId, String request) {
    final String url = this.getResourceUrl() + "/" + projectId;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    final HttpHeaders headers = new HttpHeaders();
    final MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    map.add("query", request);
    return restTemplate.postForObject(url, new HttpEntity<>(map, headers), String.class);
  }

  public String executeQueryAsGet(String projectId, String request) {
    final String url = this.getResourceUrl() + "/" + projectId + "?query={query}";
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.getForObject(url, String.class, request);
  }

  public String executeQueryWithoutToken(String projectId, String request) {
    final String url = this.getResourceUrl() + "/" + projectId;
    final RestTemplate restTemplate = new RestTemplateBuilder().build();
    return restTemplate.postForObject(url, request, String.class);
  }

  @Override
  public String getResourceUrl() {
    return this.getBaseUrl() + "/" + ModuleName.SPARQL;
  }

}
