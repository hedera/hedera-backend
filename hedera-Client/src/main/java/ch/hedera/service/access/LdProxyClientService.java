/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - LdProxyClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.access;

import java.util.List;

import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;

import ch.hedera.HederaConstants;
import ch.hedera.model.access.PublicProject;
import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.model.triplestore.SparqlResultRow;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class LdProxyClientService extends HederaClientService {

  protected LdProxyClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(restClientTool, env);
  }

  public List<PublicProject> listProjects() {
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    final String jsonResult = restTemplate.getForObject(this.getResourceUrl() + "?size=" + RestCollectionPage.MAX_SIZE_PAGE, String.class);
    return CollectionTool.getList(jsonResult, PublicProject.class);
  }

  public PublicProject getProject(String projectShortName) {
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    return restTemplate.getForObject(this.getResourceUrl() + "/" + projectShortName + "/" + ResourceName.DETAIL, PublicProject.class);
  }

  public List<String> getProjectResearchObjectTypes(String projectShortName) {
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    final String jsonResult = restTemplate
            .getForObject(this.getResourceUrl() + "/" + projectShortName + "/" + ResourceName.RESEARCH_OBJECT_TYPE, String.class);
    return this.getList(jsonResult, String.class);
  }

  public List<SparqlResultRow> getProjectResearchObjects(String projectShortName, String researchObjectType) {
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    final String jsonResult = restTemplate
            .getForObject(this.getResourceUrl() + "/" + projectShortName + "/" + researchObjectType, String.class);
    return CollectionTool.getList(jsonResult, SparqlResultRow.class);
  }

  public List<String> getProjectResearchObjectsInJson(String projectShortName, String researchObjectType) {
    final String jsonResult = this.getForObjectWithContentType(this.getResourceUrl() + "/" + projectShortName + "/" + researchObjectType,
            MediaType.APPLICATION_JSON);
    return this.getList(jsonResult, String.class);
  }

  public ResearchObjectMetadata getProjectResearchObject(String projectShortName, String researchObjectType, String hederaId) {
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    return restTemplate
            .getForObject(this.getResourceUrl() + "/" + projectShortName + "/" + researchObjectType + "/" + hederaId,
                    ResearchObjectMetadata.class);
  }

  public String getProjectResearchObjectsInJson(String projectShortName, String researchObjectType, String hederaId) {
    return this.getForObjectWithContentType(
            this.getResourceUrl() + "/" + projectShortName + "/" + researchObjectType + "/" + hederaId,
            MediaType.APPLICATION_JSON);
  }

  public String getProjectResearchObjectsInJsonLd(String projectShortName, String researchObjectType, String hederaId) {
    return this.getForObjectWithContentType(
            this.getResourceUrl() + "/" + projectShortName + "/" + researchObjectType + "/" + hederaId,
            MediaType.parseMediaType(HederaConstants.APPLICATION_JSON_LD_MIME_TYPE));
  }

  public String getProjectResearchObjectsInJsonXml(String projectShortName, String researchObjectType, String hederaId) {
    return this.getForObjectWithContentType(
            this.getResourceUrl() + "/" + projectShortName + "/" + researchObjectType + "/" + hederaId,
            MediaType.parseMediaType(HederaConstants.APPLICATION_RDF_XML_MIME_TYPE));
  }

  public String getProjectResearchObjectsInTurtle(String projectShortName, String researchObjectType, String hederaId) {
    return this.getForObjectWithContentType(
            this.getResourceUrl() + "/" + projectShortName + "/" + researchObjectType + "/" + hederaId,
            MediaType.parseMediaType(HederaConstants.TEXT_TURTLE_MIME_TYPE));
  }

  public String getProjectResearchObjectsInNTriples(String projectShortName, String researchObjectType, String hederaId) {
    return this.getForObjectWithContentType(
            this.getResourceUrl() + "/" + projectShortName + "/" + researchObjectType + "/" + hederaId,
            MediaType.parseMediaType(HederaConstants.APPLICATION_N_TRIPLES_MIME_TYPE));
  }

  @Override
  public String getResourceUrl() {
    String url = this.getBaseUrl();
    return url + "/" + ModuleName.LINKED_DATA;
  }
}
