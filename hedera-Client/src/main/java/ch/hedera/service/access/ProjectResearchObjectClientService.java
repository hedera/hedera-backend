/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - ProjectResearchObjectClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.access;

import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.NoSqlResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;

import ch.hedera.model.index.ResearchObjectMetadata;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class ProjectResearchObjectClientService extends NoSqlResourceClientService<ResearchObjectMetadata> {

  public ProjectResearchObjectClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(
            SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ACCESS,
            ResourceName.PROJECT,
            ResearchObjectMetadata.class,
            restClientTool,
            env);
  }

  public RestCollection<ResearchObjectMetadata> list(String projectShortName) {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    final String jsonResult = this.getResources(this.getResourceUrl() + "/" + projectShortName + "/" + ResourceName.RESEARCH_OBJECT, pageable);
    return this.deserializeResponse(jsonResult);
  }

  public ResearchObjectMetadata get(String projectShortName, String hederaId) {
    final String url = this.getResourceUrl() + "/" + projectShortName + "/" + ResourceName.RESEARCH_OBJECT + "/" + hederaId;
    return this.getObjectFromUrl(url);
  }

}
