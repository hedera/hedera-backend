/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - PublicProjectClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.access;

import java.util.List;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.NoSqlResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;

import ch.hedera.model.access.PublicProject;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class PublicProjectClientService extends NoSqlResourceClientService<PublicProject> {

  public PublicProjectClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(
            SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ACCESS,
            ResourceName.PROJECT,
            PublicProject.class,
            restClientTool,
            env);
  }

  public RestCollection<PublicProject> list() {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    final String jsonResult = this.getResources(this.getResourceUrl(), pageable);
    return this.deserializeResponse(jsonResult);
  }

  public PublicProject get(String projectShortName) {
    final String url = this.getResourceUrl() + "/" + projectShortName;
    return this.getObjectFromUrl(url);
  }

  public RestCollection<PublicProject> listWithoutToken() {
    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();

    RestTemplate restTemplate = restTemplateBuilder.build();
    String jsonResult = restTemplate.getForObject(this.getResourceUrl(), String.class);
    final List<PublicProject> list = CollectionTool.getList(jsonResult, PublicProject.class);
    final RestCollectionPage page = CollectionTool.getPage(jsonResult);
    return new RestCollection<>(list, page);
  }

}
