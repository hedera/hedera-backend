/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - IIIFClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.access;

import static ch.hedera.HederaConstants.IIIF_IMAGE_INFO;

import java.net.URI;
import java.nio.file.Path;
import java.util.List;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;

import ch.hedera.model.access.IIIFCollectionEntry;
import ch.hedera.model.access.IIIFManifestEntry;
import ch.hedera.model.iiif.IIIFCollection;
import ch.hedera.model.iiif.IIIFManifest;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class IIIFClientService extends HederaClientService {

  private static final String IIIF_IMAGE_PARAMETERS = "full/max/0/default.jpg";

  protected IIIFClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(restClientTool, env);
  }

  public void getImage(String iiifProtocol, String projectShortName, String businessId, Path imagePath) {
    this.getImage(iiifProtocol, projectShortName, businessId, imagePath, IIIF_IMAGE_PARAMETERS);
  }

  public void getImage(String iiifProtocol, String projectShortName, String businessId, Path imagePath, String iiifParameters) {
    final String imageUrl = this.getResourceUrl()
            + "/" + iiifProtocol
            + "/" + projectShortName
            + "/" + businessId
            + "/" + iiifParameters;
    this.restClientTool.downloadContent(URI.create(imageUrl), imagePath, TokenUsage.WITH_TOKEN);
  }

  public String getImageInfo(String iiifProtocol, String projectShortName, String businessId) {
    final String imageUrl = this.getResourceUrl()
            + "/" + iiifProtocol
            + "/" + projectShortName
            + "/" + businessId
            + "/" + IIIF_IMAGE_INFO;
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    return restTemplate.getForObject(imageUrl, String.class);
  }

  public IIIFManifest getManifest(String projectShortName, String businessId) {
    final String manifestUrl = this.getResourceUrl()
            + "/" + ResourceName.MANIFEST
            + "/" + projectShortName
            + "/" + businessId;
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    return restTemplate.getForObject(manifestUrl, IIIFManifest.class);
  }

  public List<IIIFManifestEntry> getManifests(String projectShortName) {
    final String manifestUrl = this.getResourceUrl()
            + "/" + ResourceName.MANIFEST
            + "/" + projectShortName;
    return this.listAll(manifestUrl, IIIFManifestEntry.class);
  }

  public List<IIIFCollectionEntry> getCollections(String projectShortName) {
    final String collectionUrl = this.getResourceUrl()
            + "/" + ResourceName.COLLECTION
            + "/" + projectShortName;
    return this.listAll(collectionUrl, IIIFCollectionEntry.class);
  }

  public IIIFCollection getCollection(String projectShortName, String collectionName) {
    final String collectionUrl = this.getResourceUrl()
            + "/" + ResourceName.COLLECTION
            + "/" + projectShortName
            + "/" + collectionName;
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    return restTemplate.getForObject(collectionUrl, IIIFCollection.class);
  }

  @Override
  public String getResourceUrl() {
    String url = this.getBaseUrl();
    return url + "/" + ModuleName.IIIF;
  }

  public void refreshIIIF() {
    final String url = this.getBaseUrl() + "/" + ModuleName.ACCESS + "/" + HederaActionName.IIIF_REFRESH;
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    restTemplate.postForLocation(url, null);
  }

  public void refreshIIIF(String projectShortName) {
    final String url = this.getBaseUrl() + "/" + ModuleName.ACCESS + "/" + HederaActionName.IIIF_REFRESH + "/" + projectShortName;
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    restTemplate.postForLocation(url, null);
  }

  private <T> List<T> listAll(String url, Class<T> itemClass) {
    final String urlWithPage = this.getResourceUrl() + "?size=" + RestCollectionPage.MAX_SIZE_PAGE;
    final RestTemplate restTemplate = this.restClientTool.getClient(urlWithPage);
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, itemClass);
  }
}
