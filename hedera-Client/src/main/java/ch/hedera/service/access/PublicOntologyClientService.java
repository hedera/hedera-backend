/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - PublicOntologyClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.access;

import java.util.List;

import org.springframework.core.env.Environment;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;

import ch.hedera.model.access.PublicOntology;
import ch.hedera.rest.ModuleName;

@Service
public class PublicOntologyClientService extends HederaClientService {

  protected PublicOntologyClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(restClientTool, env);
  }

  public List<PublicOntology> list() {
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    final String jsonResult = restTemplate.getForObject(this.getResourceUrl() + "?size=" + RestCollectionPage.MAX_SIZE_PAGE, String.class);
    return CollectionTool.getList(jsonResult, PublicOntology.class);
  }

  public List<PublicOntology> listExternalOntologies() {
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    final String jsonResult = restTemplate.getForObject(this.getResourceUrl() + "?size=" + RestCollectionPage.MAX_SIZE_PAGE + "&external=true",
            String.class);
    return CollectionTool.getList(jsonResult, PublicOntology.class);
  }

  public List<PublicOntology> listInternalOntologies() {
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    final String jsonResult = restTemplate.getForObject(this.getResourceUrl() + "?size=" + RestCollectionPage.MAX_SIZE_PAGE + "&external=false",
            String.class);
    return CollectionTool.getList(jsonResult, PublicOntology.class);
  }

  public PublicOntology get(String ontologyId) {
    final String url = this.getResourceUrl() + "/" + ontologyId;
    final String result = this.getForObjectWithContentType(url, MediaType.parseMediaType(MediaTypes.HAL_JSON_VALUE));
    return this.get(result, PublicOntology.class);

  }

  public String getContent(String ontologyId) {
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    final String url = this.getResourceUrl() + "/" + ontologyId;
    return restTemplate.getForObject(url, String.class);
  }

  @Override
  public String getResourceUrl() {
    String url = this.getBaseUrl();
    return url + "/" + ModuleName.ONTOLOGY;
  }

}
