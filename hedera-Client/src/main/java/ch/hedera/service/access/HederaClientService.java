/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - HederaClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.access;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.util.AbstractRestClientTool;

import ch.hedera.rest.ModuleName;

public abstract class HederaClientService {

  protected final AbstractRestClientTool restClientTool;
  protected final Environment env;

  protected HederaClientService(AbstractRestClientTool restClientTool, Environment env) {
    this.restClientTool = restClientTool;
    this.env = env;
  }

  public abstract String getResourceUrl();

  protected String getForObjectWithContentType(String url, MediaType contentType) {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(contentType);
    HttpEntity<Void> requestEntity = new HttpEntity<>(headers);
    final RestTemplate restTemplate = this.restClientTool.getClient(url);
    return restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class).getBody();
  }

  protected <T> List<T> getList(String jsonString, Class<T> itemClass) {
    List<T> result = new ArrayList<>();
    final ObjectMapper mapper = this.getObjectMapper(false);

    try {
      mapper.readTree(jsonString);
      final JavaType listItemType = mapper.getTypeFactory().constructParametricType(List.class,
              itemClass);
      result = mapper.readValue(jsonString, listItemType);
    } catch (final IOException e) {
      throw new SolidifyProcessingException("Error in building list", e);
    }
    return result;
  }

  protected <T> T get(String jsonString, Class<T> itemClass) {
    T result = null;
    final ObjectMapper mapper = this.getObjectMapper(false);
    try {
      result = mapper.readValue(jsonString, itemClass);
    } catch (final IOException e) {
      throw new SolidifyProcessingException("Error in building list", e);
    }
    return result;
  }

  private ObjectMapper getObjectMapper(boolean failOnUnknown) {
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new com.fasterxml.jackson.datatype.jsr310.JavaTimeModule());
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, failOnUnknown);
    return mapper;
  }

  protected String getBaseUrl() {
    String propertyName = SolidifyClientApplication.getApplicationPrefix() + ".module." + ModuleName.ACCESS + ".url";
    String url = this.env.getProperty(propertyName);
    if (url == null) {
      throw new IllegalStateException("Missing property: " + propertyName);
    }
    return url.substring(0, url.lastIndexOf('/'));
  }
}
