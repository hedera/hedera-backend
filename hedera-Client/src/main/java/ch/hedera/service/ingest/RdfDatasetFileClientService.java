/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - RdfDatasetFileClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.ingest;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;

import ch.hedera.HederaRestFields;
import ch.hedera.model.ingest.RdfDatasetFile;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class RdfDatasetFileClientService extends ResourceClientService<RdfDatasetFile> {

  public RdfDatasetFileClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.INGEST, ResourceName.RDF_DATASET_FILE, RdfDatasetFile.class,
            restClientTool, env);
  }

  @Override
  public List<RdfDatasetFile> findAll() {
    return this.findAll(RdfDatasetFile.class);
  }

  public List<RdfDatasetFile> findAllWithProjectId(String projectId) {
    final String url = this.getResourceUrl() + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE
            + "&" + HederaRestFields.PROJECT_ID_FIELD + "=" + projectId;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, RdfDatasetFile.class);
  }

  @Override
  public List<RdfDatasetFile> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, RdfDatasetFile.class);
  }

  public void downloadRdfDatasetFile(String rdfDatasetFileId, Path path) {
    final String url = this.getResourceUrl()
            + "/" + rdfDatasetFileId
            + "/" + ActionName.DOWNLOAD;
    this.download(url, path);
  }

  public Result addInTripleStore(String rdfDatasetFileId, String triplestoreDatasetId) {
    return this.addOrReplaceOrDeleteTripleStore(rdfDatasetFileId, triplestoreDatasetId, HederaActionName.ADD_DATASET);
  }

  public Result replaceInTripleStore(String rdfDatasetFileId, String triplestoreDatasetId) {
    return this.addOrReplaceOrDeleteTripleStore(rdfDatasetFileId, triplestoreDatasetId, HederaActionName.REPLACE_DATASET);
  }

  public Result deleteFromTripleStore(String rdfDatasetFileId, String triplestoreDatasetId) {
    return this.addOrReplaceOrDeleteTripleStore(rdfDatasetFileId, triplestoreDatasetId, HederaActionName.DELETE_DATASET);
  }

  private Result addOrReplaceOrDeleteTripleStore(String rdfDatasetFileId, String triplestoreDatasetId, String action) {
    final String url = this.getResourceUrl()
            + "/" + rdfDatasetFileId
            + "/" + action
            + "/" + triplestoreDatasetId;

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(null, headers);
    RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(url, requestEntity, Result.class);
  }

}
