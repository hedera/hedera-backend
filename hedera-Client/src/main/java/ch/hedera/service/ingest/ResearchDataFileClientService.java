/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - ResearchDataFileClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.ingest;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.HederaRestFields;
import ch.hedera.model.ingest.ResearchDataFile;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class ResearchDataFileClientService extends ResourceClientService<ResearchDataFile> {

  public ResearchDataFileClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.INGEST, ResourceName.RESEARCH_DATA_FILE, ResearchDataFile.class,
            restClientTool, env);
  }

  @Override
  public List<ResearchDataFile> findAll() {
    return this.findAll(ResearchDataFile.class);
  }

  @Override
  public List<ResearchDataFile> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, ResearchDataFile.class);
  }

  public Result reload(String projectId) {
    final String url = this.getResourceUrl() + "/" + HederaActionName.RELOAD
            + "?" + HederaRestFields.PROJECT_ID_FIELD + "=" + projectId;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    return restTemplate.postForObject(url, null, Result.class);
  }

  public List<ResearchDataFile> findAllWithProjectId(String projectId) {
    final String url = this.getResourceUrl() + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE
            + "&" + HederaRestFields.PROJECT_ID_FIELD + "=" + projectId;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, ResearchDataFile.class);
  }

  public ResearchDataFile uploadFile(Resource resource, String projectId, String accessibleFrom, String relativeLocation) {
    final String url = this.getResourceUrl() + "/" + ActionName.UPLOAD;

    final MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
    map.add(HederaConstants.ACCESSIBLE_FROM_PARAM, accessibleFrom);
    map.add("projectId", projectId);
    if (!StringTool.isNullOrEmpty(relativeLocation)) {
      map.add(HederaConstants.RELATIVE_LOCATION_PARAM, relativeLocation);
    }

    return this.uploadFile(url, resource, ResearchDataFile.class, map);
  }

  public List<ResearchDataFile> uploadZipFile(Resource resource, String projectId, String accessibleFrom) {
    final String url = this.getResourceUrl() + "/" + HederaActionName.UPLOAD_ZIP;

    final MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
    map.add(HederaConstants.ACCESSIBLE_FROM_PARAM, accessibleFrom);
    map.add("projectId", projectId);

    return this.uploadFile(url, resource, List.class, map);
  }

  public void downloadResearchDataFile(String resId, Path path) {
    final String url = this.getResourceUrl()
            + "/" + resId
            + "/" + ActionName.DOWNLOAD;
    this.download(url, path);
  }

  public List<String> listFolders(String projectId) throws JsonProcessingException {
    final String url = this.getResourceUrl() + "/" + HederaActionName.LIST_FOLDERS
            + "?" + HederaRestFields.PROJECT_ID_FIELD + "=" + projectId;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return new ObjectMapper().readValue(jsonString, List.class); // list of object
  }

  public void deleteFolder(String projectId, String folderName) {
    final String url = this.getResourceUrl() + "/" + HederaActionName.DELETE_FOLDER
            + "?" + HederaRestFields.PROJECT_ID_FIELD + "=" + projectId
            + "&" + HederaRestFields.RELATIVE_LOCATION_FIELD + "=" + folderName;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    restTemplate.postForObject(url, null, Void.class);
  }

}
