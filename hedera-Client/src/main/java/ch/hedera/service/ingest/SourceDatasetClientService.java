/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - SourceDatasetClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.ingest;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.StringTool;

import ch.hedera.HederaConstants;
import ch.hedera.HederaRestFields;
import ch.hedera.model.RdfFormat;
import ch.hedera.model.ingest.SourceDataset;
import ch.hedera.model.ingest.SourceDatasetFile;
import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;
import ch.hedera.rest.ResourceName;

@Service
public class SourceDatasetClientService extends ResourceClientService<SourceDataset> {

  public SourceDatasetClientService(AbstractRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.INGEST, ResourceName.SOURCE_DATASET, SourceDataset.class,
            restClientTool, env);
  }

  @Override
  public List<SourceDataset> findAll() {
    return this.findAll(SourceDataset.class);
  }

  public List<SourceDataset> findAllWithProjectId(String projectId) {
    final String url = this.getResourceUrl() + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE
            + "&" + HederaRestFields.PROJECT_ID_FIELD + "=" + projectId;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, SourceDataset.class);
  }

  @Override
  public List<SourceDataset> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, SourceDataset.class);
  }

  public List<SourceDataset> search(String search, String matchType) {
    String url = this.getResourceUrl() + "/search?search=" + search;
    if (!StringTool.isNullOrEmpty(matchType)) {
      url += "&match=" + matchType;
    }
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, SourceDataset.class);
  }

  public List<SourceDatasetFile> search(String sourceDatasetId, String search, String matchType) {
    String url = this.getResourceUrl() + "/" + sourceDatasetId + "/" + ResourceName.SOURCE_DATASET_FILE + "/search?search=" + search;
    if (!StringTool.isNullOrEmpty(matchType)) {
      url += "&match=" + matchType;
    }
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, SourceDatasetFile.class);
  }

  public SourceDatasetFile uploadFile(String resId, Resource resource, String datasetFileType, String version) {
    final String url = this.getResourceUrl() + "/" + resId + "/" + ActionName.UPLOAD;

    final MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
    map.add(HederaConstants.DATASET_FILE_TYPE, datasetFileType);
    map.add(HederaConstants.VERSION, version);

    return this.uploadFile(url, resource, SourceDatasetFile.class, map);
  }

  public List<SourceDatasetFile> uploadZipFile(String resId, Resource resource, String datasetFileType, String version) {
    final String url = this.getResourceUrl() + "/" + resId + "/" + HederaActionName.UPLOAD_ZIP;

    final MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
    map.add(HederaConstants.DATASET_FILE_TYPE, datasetFileType);
    map.add(HederaConstants.VERSION, version);

    return this.uploadFile(url, resource, List.class, map);
  }

  public SourceDatasetFile createSourceDataFile(String sourceDatasetId, SourceDatasetFile sourceDatasetFile) {
    return this.addItem(sourceDatasetId, ResourceName.SOURCE_DATASET_FILE, sourceDatasetFile, SourceDatasetFile.class);
  }

  public List<SourceDatasetFile> getSourceDataFiles(String sourceDatasetId) {
    return this.findAllLinkedResources(sourceDatasetId, ResourceName.SOURCE_DATASET_FILE, SourceDatasetFile.class);
  }

  public SourceDatasetFile getSourceDataFile(String sourceDatasetId, String sourceDatasetFileId) {
    return this.findOneLinkedResource(sourceDatasetId, ResourceName.SOURCE_DATASET_FILE, SourceDatasetFile.class, sourceDatasetFileId);
  }

  public void downloadSourceDatasetFile(String sourceDatasetId, String sourceDatasetFileId, Path path) {
    final String url = this.getResourceUrl()
            + "/" + sourceDatasetId
            + "/" + ResourceName.SOURCE_DATASET_FILE
            + "/" + sourceDatasetFileId
            + "/" + ActionName.DOWNLOAD;
    this.download(url, path);
  }

  public SourceDatasetFile updateSourceDatasetFile(String sourceDatasetId, String sourceDatasetFileId, Map<String, Object> mapProperties) {
    return this.updateSubItem(sourceDatasetId, ResourceName.SOURCE_DATASET_FILE, SourceDatasetFile.class, sourceDatasetFileId, mapProperties);
  }

  public void removeSourceDatasetFile(String sourceDatasetIf, String sourceDatasetFileId) {
    this.removeItem(sourceDatasetIf, ResourceName.SOURCE_DATASET_FILE, sourceDatasetFileId);
  }

  public Result applyRml(String sourceDatasetId, String sourceDatasetFileId, String rmlFileId) {
    final String url = this.getResourceUrl()
            + "/" + sourceDatasetId
            + "/" + ResourceName.SOURCE_DATASET_FILE
            + "/" + sourceDatasetFileId
            + "/" + HederaActionName.APPLY_RML;

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    String parameters = buildRmlMappingParameters(Collections.emptyList(), rmlFileId);

    HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity(parameters, headers);
    RestTemplate restTemplate = this.restClientTool.getClient();

    return restTemplate.postForObject(url, requestEntity, Result.class);
  }

  public void applyRmlAll(String sourceDatasetId, List<String> sourceDatasetFileIds, String rmlFileId) {
    final String url = this.getResourceUrl()
            + "/" + sourceDatasetId
            + "/" + ResourceName.SOURCE_DATASET_FILE
            + "/" + HederaActionName.APPLY_RML;

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    String parameters = buildRmlMappingParameters(sourceDatasetFileIds, rmlFileId);

    HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity(parameters, headers);
    RestTemplate restTemplate = this.restClientTool.getClient();

    restTemplate.postForObject(url, requestEntity, Void.class);
  }

  public Result check(String sourceDatasetId, String sourceDatasetFileId) {
    final String url = this.getResourceUrl()
            + "/" + sourceDatasetId
            + "/" + ResourceName.SOURCE_DATASET_FILE
            + "/" + sourceDatasetFileId
            + "/" + HederaActionName.CHECK;

    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    return restTemplate.postForObject(url, null, Result.class);
  }

  public Result transformRdf(String sourceDatasetId, String sourceDatasetFileId) {
    final String url = this.getResourceUrl()
            + "/" + sourceDatasetId
            + "/" + ResourceName.SOURCE_DATASET_FILE
            + "/" + sourceDatasetFileId
            + "/" + HederaActionName.TRANSFORM_RDF;

    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    return restTemplate.postForObject(url, null, Result.class);
  }

  private static String buildRmlMappingParameters(List<String> sourceDatasetFileIds, String rmlFileId) {
    StringBuilder params = new StringBuilder();
    params.append("{\"rmlFileId\":\"" + rmlFileId
            + "\", \"rdfFormat\": \"" + RdfFormat.TURTLE + "\", "
            + "\"sourceDatasetFileIds\":[");
    for (int i = 0; i < sourceDatasetFileIds.size(); i++) {
      // if last element don't add comma at the end
      if (i == sourceDatasetFileIds.size() - 1) {
        params.append("\"" + sourceDatasetFileIds.get(i) + "\"");
      } else {
        params.append("\"" + sourceDatasetFileIds.get(i) + "\",");
      }
    }
    params.append("]}");
    return params.toString();
  }
}
