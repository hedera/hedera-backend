/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Client - IIIFRefreshService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera.service.ingest;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.util.AbstractRestClientTool;

import ch.hedera.rest.HederaActionName;
import ch.hedera.rest.ModuleName;

@Service
public class IIIFRefreshService {

  protected final AbstractRestClientTool restClientTool;
  protected final Environment env;

  public IIIFRefreshService(AbstractRestClientTool restClientTool, Environment env) {
    this.restClientTool = restClientTool;
    this.env = env;
  }

  public void refreshIIIF(String projectId) {
    final String url = this.getResourceUrl()
            + "/" + ModuleName.INGEST
            + "/" + HederaActionName.IIIF_REFRESH
            + "/" + projectId;
    final RestTemplate restTemplate = this.restClientTool.getClient(url);
    restTemplate.postForLocation(url, null);
  }

  public String getResourceUrl() {
    String propertyName = SolidifyClientApplication.getApplicationPrefix() + ".module." + ModuleName.INGEST + ".url";
    String url = this.env.getProperty(propertyName);
    if (url == null) {
      throw new IllegalStateException("Missing property: " + propertyName);
    }
    return url.substring(0, url.lastIndexOf('/'));
  }
}
