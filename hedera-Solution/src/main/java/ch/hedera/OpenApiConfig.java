/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Solution - OpenApiConfig.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.hedera;

import org.springdoc.core.models.GroupedOpenApi;
import org.springdoc.core.properties.SwaggerUiOAuthProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.auth.client.service.AuthorizationClientProperties;
import ch.unige.solidify.config.SolidifyOpenApiConfig;

import ch.hedera.config.HederaRepositoryDescription;
import ch.hedera.rest.ModuleName;

@Configuration
public class OpenApiConfig extends SolidifyOpenApiConfig {

  private final HederaRepositoryDescription repository;
  private final AuthorizationClientProperties authProperties;

  OpenApiConfig(HederaRepositoryDescription repository,
          AuthorizationClientProperties authProperties,
          SwaggerUiOAuthProperties swaggerUiOAuthProperties) {
    super(swaggerUiOAuthProperties);
    this.repository = repository;
    this.authProperties = authProperties;
  }

  @Override
  protected String getAuthorizationServerUrl() {
    return this.authProperties.getPublicAuthorizationServerUrl();
  }

  @Override
  protected String getApplicationName() {
    return this.repository.getLongName();
  }

  @Override
  protected String getApplicationDescription() {
    return this.repository.getDescription();
  }

  @Override
  protected String getApplicationVersion() {
    return this.repository.getVersion();
  }

  @Override
  protected String getApplicationScope() {
    return "app-hedera";
  }

  @Override
  protected String getContactEmail() {
    return this.repository.getEmail();
  }

  @Override
  protected String getApplicationTagName() {
    return HederaConstants.HEDERA.toLowerCase();
  }

  @Bean
  @ConditionalOnProperty(prefix = "hedera.module.admin", name = "enable")
  public GroupedOpenApi adminOpenApi() {
    return this.moduleGroup(ModuleName.ADMIN);
  }

  @Bean
  @ConditionalOnProperty(prefix = "hedera.module.ingest", name = "enable")
  public GroupedOpenApi ingestOpenApi() {
    return this.moduleGroup(ModuleName.INGEST);
  }

  @Bean
  @ConditionalOnProperty(prefix = "hedera.module.access", name = "enable")
  public GroupedOpenApi accessOpenApi() {
    return this.moduleGroup(ModuleName.ACCESS);
  }

  @Bean
  @ConditionalOnProperty(prefix = "hedera.module.access", name = "enable")
  public GroupedOpenApi iiifOpenApi() {
    return this.moduleGroup(ModuleName.IIIF);
  }

  @Bean
  @ConditionalOnProperty(prefix = "hedera.module.access", name = "enable")
  public GroupedOpenApi sparqlOpenApi() {
    return this.moduleGroup(ModuleName.SPARQL);
  }

  @Bean
  @ConditionalOnProperty(prefix = "hedera.module.access", name = "enable")
  public GroupedOpenApi ldOpenApi() {
    return this.moduleGroup(ModuleName.LINKED_DATA);
  }

  @Bean
  @ConditionalOnProperty(prefix = "hedera.module.access", name = "enable")
  public GroupedOpenApi ontologiesOpenApi() {
    return this.moduleGroup(ModuleName.ONTOLOGY);
  }

  @Bean
  @ConditionalOnProperty(prefix = "hedera.module.access", name = "enable")
  public GroupedOpenApi oaiOpenApi() {
    return this.moduleGroup(OAIConstants.OAI_MODULE);
  }

  @Bean
  @ConditionalOnProperty(prefix = "hedera.module.access", name = "enable")
  public GroupedOpenApi indexOpenApi() {
    return this.moduleGroup(IndexConstants.INDEX_MODULE);
  }
}
