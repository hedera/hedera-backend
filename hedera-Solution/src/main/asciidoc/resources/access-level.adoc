[cols="^,^,3,options="header"]
|===
|Icon|Access Level|Description

|image:access-level-public.svg[Public Level,height=100]
|{set:cellbgcolor:#18BB9C} [white]#`*PUBLIC*`#
|{set:cellbgcolor:#FFFFFF} The archive is accessible to everyone +
=> Open access.


|image:access-level-restricted.svg[Restricted Level,height=100]
|{set:cellbgcolor:#F39C11} [white]#`*RESTRICTED*`#
|{set:cellbgcolor:#FFFFFF} The archive is accessible to team members (i.e., Org. Unit) +
=> Trusted parties.

|image:access-level-closed.svg[Closed Level,height=100]
|{set:cellbgcolor:#F44336} [white]#`*CLOSED*`#
|{set:cellbgcolor:#FFFFFF} The archive is accessible case by case thank to access control list (ACL) +
=> Individuals.

|===
