[[hedera-tools-guide]]
= hedera Tools Guide
:doctype: book
:icons: font
:source-highlighter: highlightjs
:toc: left
:toclevels: 4
:sectlinks:
:sectnums:

hedera Tools *v{version}*, _{buildDate}_

image:hedera-logo.png[hedera]

The current documentation is available in link:hedera-ToolsGuide.html[HTML] or link:hedera-ToolsGuide.pdf[PDF].


[[hedera-tools-functions]]
== Tool Options

=== `project-check` function
This function allows to check if the structure of import folder is correct. 

It will simulate the project import.

=== `project-import` function
This function allows to import project and all associated data:

. Create the project if not exists
. Create RML files if not exist and associate to the project
. Import Dataset files
. Import Research data files 

=== `source-dataset-import` function
This function is equivalent to the step 4 of `project-import` function.

=== `research-data-file-import` function
This function is equivalent to the step 5 of `project-import` function.

[[hedera-tools-installation]]
== Installation

[[hedera-tools-get]]
=== Get the tool
* Download the hedera tool package: link:packages/hedera-Tools-{version}.jar[hedera-Tools-{version}.jar]
* Define the properties file

.hedera-tools.properties
[source]
----
###################################################
# Folder where the data to import or to check are #
###################################################
hedera.import=<Must be defined>  <1>
hedera.project=<Must be defined> <2>
  
#############################################################################
# Profile to determine which function to run                                #
#  - project-check             = To check the folder structure              #
#  - project-import            = To import projects & associated data       #
#  - source-dataset-import     = To import source datasets of a project     #
#  - research-data-file-import = To import research data files of a project #
#############################################################################
spring.profiles.active=<Must be defined> // <3>
# Examples:
#  spring.profiles.active=project-import
#  spring.profiles.active=source-dataset-import,research-data-file-import
spring.profiles.include[0]=basic-log
spring.profiles.include[1]=client-standalone
 
# hedera modules
hedera.module.admin.url=<hedera Admin module URL: must be defined>
# Example: hedera.module.admin.url=https://sandbox.hedera.ch/administration/admin
hedera.module.ingest.url=<hedera Ingest module URL: must be defined>
# Example: hedera.module.ingest.url=https://sandbox.hedera.ch/ingestion/ingest
 
spring.cloud.config.enabled=false
spring.cloud.bootstrap.enabled=false
 
# Client should not instantiate JDBC / Hibernate / JPA layer
spring.autoconfigure.exclude[0]=org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
spring.autoconfigure.exclude[1]=org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
spring.autoconfigure.exclude[2]=org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration
spring.autoconfigure.exclude[3]=org.springframework.boot.autoconfigure.data.web.SpringDataWebAutoConfiguration
----
<1> Parameters to define the input folder: _project folder_ for   `project-check` or `project-import`, _source dataset folder_ for  `source-dataset-import`, _research data file folder_ for `research-data-file-import` 
<2> Project identifier (could be ID or short name), mandatory for `source-dataset-import`/ `research-data-file-import`
<3> Profiles to run tools: `project-check` / `project-import` / `source-dataset-import`/ `research-data-file-import`

[[hedera-tools-organize]]
=== Organize the data
The structure of the _input_ folder must be like this: 

* `<Project container> /  <Project container>.png` for the project logo (support `png`, `jpg` and `jpeg`)
* `<Project container> /  rml  / Your RML files`
* `<Project container> /  source-datasets / Your datasets / <CSV | JSON | XML | RDF> / Your source dataset files`
* `<Project container> /  research-data-files / <IIIF | IIIF_MANIFEST | TEI | WEB> / Your folder / Your sub-folder / Your research data files`


[[hedera-tools-run]]
== Execution
To run the tool:

* Set the environment variable to define OAuth2 access token

`export OAUTH2_TOKEN=<__OAuth2 Access Token__>`

* Run the following command

`java -Dspring.cloud.bootstrap.enabled=true -Dspring.cloud.bootstrap.location=<__properties file__> -Dsolidify.oauth2.accesstoken=$OAUTH2_TOKEN -jar hedera-Tools-{version}.jar >>hedera-tools.log 2>&1`
